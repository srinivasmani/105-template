<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css"></link>

<div id="options-table" style="border:1px solid black;margin-top:6px;padding:10px;display:block;">
<input type="hidden" name="optionDescription" id="optionDescription" value="" />
<input type="hidden" name="lastoptionDiv" id="lastoptionDiv" value="" />
	<div class="sideheading">
		<label>Option Type</label> <br /> 
		<select id="optionType" name="optionType">
			<option id="1" selected="selected">Square</option>
			<option id="2">Circle</option>
			<option id="3">None</option>
		</select>
	</div>
	<div class="sideheading" id="optionComp">
		<label>Option Name</label> <br /> <input type="text" value="${optionSelect}"
			id="optionName" name="optionName"  maxlength="20"/>
	</div>
	<div class="sideheading" id="compOptionDesc">
		<div class="checkboxcntDiv" id = "checkboxcnt_1">
			<img src="../images/checkbox.png" class="imageOptionType" id="image_1">
			<input type="text" value="" id="optionDesc_1" name="optionDesc_1" size="9" class="compOptDesc" style="width:127px;" />
			<span><input type="button" name="add_1" value="" id="add_1" class="add"/>
			<input type="button" name="remove_1" value="" id="remove_1" class="delete"/></span>
		</div>
	</div>
	<div class="sideheading" style="text-align: center;">
		<input class="btn_forright" type="button" value="Ok" id="rightOkCompOption" /> 
		<input class="btn_forright" type="button" value="Cancel" id="rightCancelCompOption" />
	</div>
</div>
