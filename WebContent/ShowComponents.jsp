<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<style>
div#image { 
    position:relative;
    width:400px; height:300px; /* width accommodates scrollbar on right */
    overflow:auto; /* for non-javascript */
    }
    .imgSet{
     	float: left;
     	width: 30%;";
    }
</style>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/jquery-editTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<link rel="stylesheet" type="text/css" href="../css/pagination.css"></link>
<input type = "text" id = "place" class = "place" name = "place" value = "${type}" style="display: none;"/>
<div id="image" style="width: 100%;margin-top:10px;">
<%! int j=0; %>
		<display:table name="${componentsList}" id="component" sort="list" pagesize="6" requestURI="./getComponents.do" >
		<%j++; %>
			<display:column>
			<c:if test="${component.templateComponent.status.statusId != 3}">
				<div id="compboxesdiv<%=j%>" style="width:165px;text-align:left;">
						<div class="imgSet imgSetD">
							<%-- <input type="text" value="${component.componentName}" name="compName_<%=j%>" id="compName_<%=j%>">--%>
							<label>${component.componentName}</label>
							<input type="hidden" value="${component.componentId}" name="compId_<%=j%>" id="compId_<%=j%>">
							<input type="button" class="deleteComponent btn-txt" value="Delete" id="deleteLink_<%=j%>">
						</div>
				</div>
			</c:if>	
			</display:column>
			 <%@include file="./displayTagProperties.jsp" %>
		</display:table>
</div>
<script type="text/javascript">

jQuery(document).ready(function(){
	jQuery(".deleteComponent").click(function(){
		var r = confirm("Are you sure you want to delete?");
		if (r==true){
		var idNum = ($(this).attr('id')).split("_");
		var compIdDiv = "compId_"+idNum[1];
		//alert("compIdDiv : "+compIdDiv);
		var pageId="./deleteComponent.do?cid="+jQuery("#"+compIdDiv).val();
		//alert("pageId : "+pageId);
		$(this).attr('href',"#");
			 jQuery.ajax({   
			   url :pageId,   
			   type :"GET",
			   dataType : "html", 
			   success : function(data) {
				 jQuery("#compBox").html(data);
			   },  
			   error : function(xmlhttp, error_msg){
			   }
  			 });
		}
		});

	jQuery("#firstPage a").click(function(){
		var pageId=$(this).attr('href');
		$(this).attr('href',"#");
			 jQuery.ajax({   
			   url :pageId,
			   type :"POST",
			   dataType : "html",   
			   success : function(data) {
				 jQuery("#image").html(data);
			   },  
			   error : function(xmlhttp, error_msg) {
			   }
  			 });
			
		});
});
</script>