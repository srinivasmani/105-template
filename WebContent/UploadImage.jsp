<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet" type="text/css" href="../css/pagination.css">
<style>
.grayBox1 {
	top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    opacity: 0.8;
    z-index: 1001;
     display: none;
    position: fixed;
    background-color: black;
    filter:alpha(opacity=80);
}
.box_content1 {
	left: 25%;
    right: 90%;
    z-index: 1002;
    padding: 16px;
    overflow: auto;
    position: fixed;
    margin-top: -65px;
    height: auto !important;
	width: 620px !important;
    border: 8px solid #ACACAC !important;
    background: none repeat scroll 0 0 #FFFFFF;
}
</style>
<input type="hidden" id=reqHeight name="reqHeight" />
<input type="hidden" id="reqWidth" name="reqWidth" />
<div id="grayBG" class="grayBox1"></div>
	<div id="centerImagePop" class="box_content" style="display: none;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadLocalImage" action="./editTemplate2.do" method="POST" id="uploadLocalImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				    <input type="hidden" id="center" name="center" value="center">
				    <input type="hidden" id="centerimageHeight" name="centerimageHeight" value="">
				    <input type="hidden" id="centerimageWidth" name="centerimageWidth" value="">
					<span><input name="fileImage" type="file" id="localImage" value="${logoImage}" /></span>
					<c:choose>
						<c:when test="${template.templateImage ne null and template.templateImage ne '' and user.userRole eq 'admin'}">
							<span id="delBtn">
								<input type="button" value="Delete" id="deleteButton"/>
							</span>
						</c:when>
						<c:when test="${userForm.formImage ne null and userForm.formImage ne '' and user.userRole eq 'user'}">
							<span id="delBtn">
								<input type="button" value="Delete" id="deleteButton"/>
							</span>
						</c:when>
						<c:otherwise>
							<span id="delBtn"></span>
						</c:otherwise>
					</c:choose>
					<span id="saveBtn"></span>
				</div>
			</form>
		</p>
		<form name="uploadRImage" action="./browseImage.do?type=center" method="post" id="uploadRImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browse" onclick="repositoryImageSelectionPop('center'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="repositoryPopUp" class="box_content1" style="display: none;background: #fff;">
					<span style="float: right; cursor: pointer;margin-top: -2px;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="lbox" style="margin-top:10px;"></div>
					</p>
				</div>
			</div>
		</form>
	</div>
	<%--for 104 centerimage popup --%> 
	<div id="centerImagePop104" class="box_content" style="display: none;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadLocalImage104" action="./editTemplate2.do" method="POST" id="uploadLocalImage104" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				    <input type="hidden" id="center104" name="center104" value="center104">
				    <input type="hidden" id="centerimageHeight104" name="centerimageHeight104" value="">
				    <input type="hidden" id="centerimageWidth104" name="centerimageWidth104" value="">
					<span><input name="fileImage" type="file" id="localImage104" value="${logoImage}" /></span>
					<c:choose>
						<c:when test="${template.templateImage ne null and template.templateImage ne '' and user.userRole eq 'admin'}">
							<span id="delBtn104">
								<input type="button" value="Delete" id="deleteButton104"/>
							</span>
						</c:when>
						<c:when test="${userForm.formImage ne null and userForm.formImage ne '' and user.userRole eq 'user'}">
							<span id="delBtn104">
								<input type="button" value="Delete" id="deleteButton104"/>
							</span>
						</c:when>
						<c:otherwise>
							<span id="delBtn104"></span>
						</c:otherwise>
					</c:choose>
					<span id="saveBtn104"></span>
				</div>
			</form>
		</p>
		<form name="uploadRImage104" action="./browseImage.do?type=center104" method="post" id="uploadRImage104" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browse104" onclick="repositoryImageSelectionPop('center104'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="repositoryPopUp104" class="box_content1" style="display: none;background: #fff;">
					<span style="float: right; cursor: pointer;margin-top: -2px;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="lbox104" style="margin-top:10px;"></div>
					</p>
				</div>
			</div>
		</form>
	</div>
	<div id="leftLogoImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadLeftLogoLocalImage" action="./editTemplate2.do" method="POST" id="uploadLeftLogoLocalImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				     <input type="hidden" id="leftLogo" name="leftLogo" value="leftLogo">
				    <input type="hidden" id="leftLogoimageHeight" name="leftLogoimageHeight" value="">
				    <input type="hidden" id="leftLogoimageWidth" name="leftLogoimageWidth" value="">
					<span><input name="fileImage" type="file" id="localLeftLogoImage" value="${logoImage}" /></span>
					<c:choose>
						<c:when test="${userForm.formLeftImage ne null and userForm.formLeftImage ne ''}">
							<span id="delBtn">
								<input type="button" value="Delete" id="deleteLeftButton"/>
							</span>
						</c:when>
						<c:otherwise>
							<span id="delBtnLeft"></span>
						</c:otherwise>
					</c:choose>
				</div>
			</form>
		</p>
		<form name="uploadRLeftLogoImage" action="./browseImage.do?type=leftLogo" method="post" id="uploadRLeftLogoImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseLeftLogo" onclick="repositoryImageSelectionPop('leftLogo'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="leftLogoRepositoryPopUp" class="box_content1" style="display: none;background: #fff;">
					<span style="float: right;cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="leftLogobox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="leftLogoImagePop104" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadLeftLogoLocalImage104" action="./editTemplate2.do" method="POST" id="uploadLeftLogoLocalImage104" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				     <input type="hidden" id="leftLogo104" name="leftLogo104" value="leftLogo104">
				    <input type="hidden" id="leftLogoimageHeight104" name="leftLogoimageHeight104" value="">
				    <input type="hidden" id="leftLogoimageWidth104" name="leftLogoimageWidth104" value="">
					<span><input name="fileImage" type="file" id="localLeftLogoImage104" value="${logoImage}" /></span>
					<c:choose>
						<c:when test="${userForm.formLeftImage ne null and userForm.formLeftImage ne ''}">
							<span id="delBtnLeft104">
								<input type="button" value="Delete" id="deleteLeftButton104"/>
							</span>
						</c:when>
						<c:otherwise>
							<span id="delBtnLeft104"></span>
						</c:otherwise>
					</c:choose>
				</div>
			</form>
		</p>
		<form name="uploadRLeftLogoImage104" action="./browseImage.do?type=leftLogo104" method="post" id="uploadRLeftLogoImage104" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseLeftLogo104" onclick="repositoryImageSelectionPop('leftLogo104'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="leftLogoRepositoryPopUp104" class="box_content1" style="display: none;background: #fff;">
					<span style="float: right;cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="leftLogobox104"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="rightLogoImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadRightLogoLocalImage" action="./editTemplate2.do" method="POST" id="uploadRightLogoLocalImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				<input type="hidden" id="rightLogo" name="rightLogo" value="rightLogo">
				<input type="hidden" id="rightLogoimageHeight" name="rightLogoimageHeight" value="">
				    <input type="hidden" id="rightLogoimageWidth" name="rightLogoimageWidth" value="">
					<span><input name="fileImage" type="file" id="localRightLogoImage" value="${logoImage}" /></span>
					<c:choose>
						<c:when test="${userForm.formRightImage ne null and userForm.formRightImage ne ''}">
							<span id="delBtn">
								<input type="button" value="Delete" id="deleteRightButton"/>
							</span>
						</c:when>
						<c:otherwise>
							<span id="delBtnRight"></span>
						</c:otherwise>
					</c:choose>
				</div>
			</form>
		</p>
		<form name="uploadRRightLogoImage" action="./browseImage.do?type=rightLogo" method="post" id="uploadRRightLogoImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseRightLogo" onclick="repositoryImageSelectionPop('rightLogo'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="rightLogoRepositoryPopUp" class="box_content1" style="display: none;background: #fff; ">
					<span style="float: right;cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="rightLogobox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<%--Right Optional Logo for template 104 --%>
	<div id="rightLogoImagePop104" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadRightLogoLocalImage104" action="./editTemplate2.do" method="POST" id="uploadRightLogoLocalImage104" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				<input type="hidden" id="rightLogo104" name="rightLogo104" value="rightLogo104">
				<input type="hidden" id="rightLogoimageHeight104" name="rightLogoimageHeight104" value="">
				    <input type="hidden" id="rightLogoimageWidth104" name="rightLogoimageWidth104" value="">
					<span><input name="fileImage" type="file" id="localRightLogoImage104" value="${logoImage}" /></span>
					<c:choose>
						<c:when test="${userForm.formRightImage ne null and userForm.formRightImage ne ''}">
							<span id="delBtnRight104">
								<input type="button" value="Delete" id="deleteRightButton104"/>
							</span>
						</c:when>
						<c:otherwise>
							<span id="delBtnRight104"></span>
						</c:otherwise>
					</c:choose>
					<span id="saveBtn104"></span>
				</div>
			</form>
		</p>
		<form name="uploadRRightLogoImage104" action="./browseImage.do?type=rightLogo104" method="post" id="uploadRRightLogoImage104" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseRightLogo104" onclick="repositoryImageSelectionPop('rightLogo104'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="rightLogoRepositoryPopUp104" class="box_content1" style="display: none;background: #fff; ">
					<span style="float: right;cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="rightLogobox104"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="leftImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadLeftImage" action="./editTemplate2.do" method="POST" id="uploadLeftImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				<input type="hidden" id="left" name="left" value="left">
				<input type="hidden" id="leftimageHeight" name="leftimageHeight" value="">
				    <input type="hidden" id="leftimageWidth" name="leftimageWidth" value="">
					<span><input name="fileImage" type="file" id="leftImage" value="${logoImage}" /></span>					
				</div>
			</form>
		</p>
		<form name="uploadRLeftImage" action="./browseImage.do?type=left" method="post" id="uploadRLeftImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseLeft" onclick="repositoryImageSelectionPop('left'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="leftRepositoryPopUp" class="box_content1" style="display: none;background: #fff; height: 57%; width: 40%;">
					<span style="float: right;cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="leftBox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="bottomLeftImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
	
			<form name="uploadBottomLeftImage" action="./editTemplate2.do" method="POST" id="uploadBottomLeftImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				    <input type="hidden" id="bottomLeft" name="bottomLeft" value="bottomLeft">
					<input type="hidden" id="bottomLeftimageHeight" name="bottomLeftimageHeight" value="">
				    <input type="hidden" id="bottomLeftimageWidth" name="bottomLeftimageWidth" value="">
					<span><input name="fileImage" type="file" id="bottomLeftImage" value="${logoImage}" /></span>					
				</div>
			</form>
		</p>
		<form name="uploadRBottomLeftImage" action="./browseImage.do?type=bottomLeft" method="post" id="uploadRBottomLeftImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseBottomLeft" onclick="repositoryImageSelectionPop('bottomLeft'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="bottomLeftRepositoryPopUp" class="box_content1" style="display: none;background: #fff; height: 57%; width: 40%;">
					<span style="float: right; cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="bottomLeftBox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="bottomRightImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadBottomRightImage" action="./editTemplate2.do" method="POST" id="uploadBottomRightImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				 <input type="hidden" id="bottomRight" name="bottomRight" value="bottomRight">
				<input type="hidden" id="bottomRightimageHeight" name="bottomRightimageHeight" value="">
				    <input type="hidden" id="bottomRightimageWidth" name="bottomRightimageWidth" value="">
					<span><input name="fileImage" type="file" id="bottomRightImage" value="${logoImage}" /></span>					
				</div>
			</form>
		</p>
		<form name="uploadRBottomRightImage" action="./browseImage.do?type=bottomRight" method="post" id="uploadRBottomRightImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseBottomRight" onclick="repositoryImageSelectionPop('bottomRight'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="bottomRightRepositoryPopUp" class="box_content1" style="display: none;background: #fff; height: 57%; width: 40%;">
					<span style="float: right; cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="bottomRightBox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="bottomLastImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadBottomLastImage" action="./editTemplate2.do" method="POST" id="uploadBottomLastImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				    <input type="hidden" id="bottomLast" name="type" value="bottomLast">
					<input type="hidden" id="bottomLastimageHeight" name="bottomLastimageHeight" value="">
				    <input type="hidden" id="bottomLastimageWidth" name="bottomLastimageWidth" value="">
					<span><input name="fileImage" type="file" id="bottomLastImage" value="${logoImage}" /></span>					
				</div>
			</form>
		</p>
		<form name="uploadRBottomLastImage" action="./browseImage.do?type=bottomLast" method="post" id="uploadRBottomLastImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseBottomLast" onclick="repositoryImageSelectionPop('bottomLast'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="bottomLastRepositoryPopUp" class="box_content1" style="display: none;background: #fff; height: 57%; width: 40%;">
					<span style="float: right; cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="bottomLastBox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="bottomMidImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadBottomMidImage" action="./editTemplate2.do" method="POST" id="uploadBottomMidImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				    <input type="hidden" id="bottomMid" name="type" value="bottomMid">
					<input type="hidden" id="bottomMidimageHeight" name="bottomMidimageHeight" value="">
				    <input type="hidden" id="bottomMidimageWidth" name="bottomMidimageWidth" value="">
					<span><input name="fileImage" type="file" id="bottomMidImage" value="${logoImage}" /></span>					
				</div>
			</form>
		</p>
		<form name="uploadRBottomMidImage" action="./browseImage.do?type=bottomMid" method="post" id="uploadRBottomMidImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseBottomMid" onclick="repositoryImageSelectionPop('bottomMid'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="bottomMidRepositoryPopUp" class="box_content1" style="display: none;background: #fff; height: 57%; width: 40%;">
					<span style="float: right; cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="bottomMidBox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="bottomCenterImagePop" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
		<div style="background: #fff; width: 100%;">
		<div onclick="centerImageSelectionPop('close'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadBottomCenterImage" action="./editTemplate2.do" method="POST" id="uploadBottomCenterImage" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				    <input type="hidden" id="bottomCenter" name="type" value="bottomCenter">
					<input type="hidden" id="bottomCenterimageHeight" name="bottomCenterimageHeight" value="">
				    <input type="hidden" id="bottomCenterimageWidth" name="bottomCenterimageWidth" value="">
					<span><input name="fileImage" type="file" id="bottomCenterImage" value="${logoImage}" /></span>					
				</div>
			</form>
		</p>
		<form name="uploadRBottomCenterImage" action="./browseImage.do?type=bottomCenter" method="post" id="uploadRBottomCenterImage" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browseBottomCenter" onclick="repositoryImageSelectionPop('bottomCenter'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="bottomCenterRepositoryPopUp" class="box_content1" style="display: none;background: #fff; height: 57%; width: 40%;">
					<span style="float: right;cursor: pointer;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close')"></span>
					<p>
						<div id="bottomCenterBox"></div>
					</p>
				</div>
			</div>
		</form>
		</div>
	</div>
                                
                                
                                
<!--  for 105    -->
                                
        <div id="centerImagePop105" class="box_content" style="display: none;">
		<div onclick="centerImageSelectionPop('close105'); return false;" style="cursor: pointer;" align="right">
			<img src="../images/icon-delete.gif">
		</div>
		<p>
			<form name="uploadLocalImage105" action="./editTemplate2.do" method="POST" id="uploadLocalImage105" enctype="multipart/form-data">
				<div class="sideheading">
					<label><nobr>Upload your own image :</nobr> </label>
				</div>
				<div>
				    <input type="hidden" id="center105" name="center105" value="center105">
				    <input type="hidden" id="centerimageHeight105" name="centerimageHeight105" value="">
				    <input type="hidden" id="centerimageWidth105" name="centerimageWidth105" value="">
					<span><input name="fileImage" type="file" id="localImage105" value="${logoImage}" /></span>
					<c:choose>
						<c:when test="${template.templateImage ne null and template.templateImage ne '' and user.userRole eq 'admin'}">
							<span id="delBtn105">
								<input type="button" value="Delete" id="deleteButton105"/>
							</span>
						</c:when>
						<c:when test="${userForm.formImage ne null and userForm.formImage ne '' and user.userRole eq 'user'}">
							<span id="delBtn105">
								<input type="button" value="Delete" id="deleteButton105"/>
							</span>
						</c:when>
						<c:otherwise>
							<span id="delBtn105"></span>
						</c:otherwise>
					</c:choose>
					<span id="saveBtn105"></span>
				</div>
			</form>
		</p>
		<form name="uploadRImage105" action="./browseImage.do?type=center105" method="post" id="uploadRImage105" enctype="multipart/form-data">
			<div class="sideheading">
				<label><br/><nobr>Choose from existing images :</nobr></label>
			</div>
			<div>
				<input type="text" readonly="readonly" style="height: 18px;" id="selectImg">
				<input type="submit" value="Browse..." id="browse105" onclick="repositoryImageSelectionPop('center105'); return false;" style="margin-left: -3px; width: 74px;">
				<div id="grayBG" class="grayBox1"></div>
				<div id="repositoryPopUp105" class="box_content1" style="display: none;background: #fff;">
					<span style="float: right; cursor: pointer;margin-top: -2px;"><img src="../images/icon-delete.gif" onclick="repositoryImageSelectionPop('close105')"></span>
					<p>
						<div id="lbox105" style="margin-top:10px;"></div>
					</p>
				</div>
			</div>
		</form>
	</div>