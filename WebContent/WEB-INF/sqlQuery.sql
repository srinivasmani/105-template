--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.1
-- Dumped by pg_dump version 9.1.1
-- Started on 2011-12-26 00:18:38 IST

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1971 (class 1262 OID 16384)
-- Dependencies: 1970
-- Name: MightyIFS; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE "MightyIFS" IS 'Mighty Inspection Form Site application database.';


--
-- TOC entry 6 (class 2615 OID 16385)
-- Name: MightyIFS; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "MightyIFS";


ALTER SCHEMA "MightyIFS" OWNER TO postgres;

--
-- TOC entry 170 (class 3079 OID 11680)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1972 (class 0 OID 0)
-- Dependencies: 170
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

SET search_path = "MightyIFS", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 163 (class 1259 OID 16396)
-- Dependencies: 6
-- Name: COMPONENTS; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "COMPONENTS" (
    "COMPONENT_ID" integer NOT NULL,
    "COMPONENT_NAME" character(50) NOT NULL,
    "CREATED_DATE" date NOT NULL,
    "CREATED_BY" character(50) NOT NULL,
    "UPDATED_DATE" date NOT NULL,
    "UPDATED_BY" character(50)
);


ALTER TABLE "MightyIFS"."COMPONENTS" OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 24596)
-- Dependencies: 6
-- Name: DOCUMENT; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "DOCUMENT" (
    "DOCUMENT_ID" integer NOT NULL,
    "DOCUMENT_NAME" character(100) NOT NULL,
    "DOCUMENT_DESCRIPTION" character(2000),
    "DOCUMENT_FILE" bytea
);


ALTER TABLE "MightyIFS"."DOCUMENT" OWNER TO postgres;

--
-- TOC entry 169 (class 1259 OID 24627)
-- Dependencies: 6
-- Name: OPTIONS; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "OPTIONS" (
    "OPTION_ID" integer NOT NULL,
    "OPTIONS_NAME" character(50) NOT NULL,
    "OPTION_DESCRIPTION" character(500)
);

ALTER TABLE "MightyIFS"."OPTIONS" OWNER TO postgres;

--
-- TOC entry 167 (class 1259 OID 24614)
-- Dependencies: 6
-- Name: POSITION; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "POSITION" (
    "POSITION_ID" integer NOT NULL,
    "POSITION_DESCRIPTION" character(500),
    "POSITION_NAME" character(100) NOT NULL
);


ALTER TABLE "MightyIFS"."POSITION" OWNER TO postgres;

--
-- TOC entry 165 (class 1259 OID 24591)
-- Dependencies: 6
-- Name: STATUS; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "STATUS" (
    "STATUS_ID" integer NOT NULL,
    "STATUS_DESCRIPTION" character(50)
);


ALTER TABLE "MightyIFS"."STATUS" OWNER TO postgres;

--
-- TOC entry 1973 (class 0 OID 0)
-- Dependencies: 165
-- Name: TABLE "STATUS"; Type: COMMENT; Schema: MightyIFS; Owner: postgres
--

COMMENT ON TABLE "STATUS" IS 'THIS TABLE STORES THE STATUS INFORMATION. THIS IS A MASTER TABLE FOR STATUS.';


--
-- TOC entry 168 (class 1259 OID 24622)
-- Dependencies: 6
-- Name: SUB_COMPONENTS; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "SUB_COMPONENTS" (
    "SUB_COMPONENT_ID" integer NOT NULL,
    "COMPONENT_ID" integer NOT NULL,
    "LABEL" character(100) NOT NULL,
    "OPTION_ID" integer NOT NULL,
    "MODIFIED_DATE" date,
    "MODIFIED_BY" integer,
    "CREATED_DATE" date,
    "CREATED_BY" integer
);


ALTER TABLE "MightyIFS"."SUB_COMPONENTS" OWNER TO postgres;

--
-- TOC entry 161 (class 1259 OID 16386)
-- Dependencies: 6
-- Name: TEMPLATE; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "TEMPLATE" (
    "TEMPLATE_ID" integer NOT NULL,
    "TEMPLATE_NAME" character(50) NOT NULL,
    "CREATED_DATE" date NOT NULL,
    "CREATED_BY" character(50) NOT NULL,
    "UPDATED_DATE" date NOT NULL,
    "UPDATED_BY" character(50) NOT NULL
);


ALTER TABLE "MightyIFS"."TEMPLATE" OWNER TO postgres;

--
-- TOC entry 1974 (class 0 OID 0)
-- Dependencies: 161
-- Name: TABLE "TEMPLATE"; Type: COMMENT; Schema: MightyIFS; Owner: postgres
--

COMMENT ON TABLE "TEMPLATE" IS 'Templates information is stored in this table.';


--
-- TOC entry 164 (class 1259 OID 24576)
-- Dependencies: 6
-- Name: TEMPLATE_COMPONENTS; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "TEMPLATE_COMPONENTS" (
    "TEMPLATE_ID" integer NOT NULL,
    "COPONENT_ID" integer NOT NULL
);


ALTER TABLE "MightyIFS"."TEMPLATE_COMPONENTS" OWNER TO postgres;

--
-- TOC entry 1975 (class 0 OID 0)
-- Dependencies: 164
-- Name: TABLE "TEMPLATE_COMPONENTS"; Type: COMMENT; Schema: MightyIFS; Owner: postgres
--

COMMENT ON TABLE "TEMPLATE_COMPONENTS" IS 'STORES THE COMPONENTS FOR THE TEMPLATES.';


--
-- TOC entry 162 (class 1259 OID 16391)
-- Dependencies: 6
-- Name: USER_FORMS; Type: TABLE; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE TABLE "USER_FORMS" (
    "FORM_ID" integer NOT NULL,
    "TEMPLATE_ID" integer NOT NULL,
    "USER_ID" integer NOT NULL,
    "CREATED_DATE" date NOT NULL,
    "CREATED_BY" character(50) NOT NULL,
    "UPDATED_DATE" date NOT NULL,
    "UPDATED_BY" character(50),
    "COMPONENT_ID" integer,
    "POSITION_ID" integer,
    "STATUS_ID" integer NOT NULL,
    "DOCUMENT_ID" integer
);


ALTER TABLE "MightyIFS"."USER_FORMS" OWNER TO postgres;

--
-- TOC entry 1961 (class 0 OID 16396)
-- Dependencies: 163
-- Data for Name: COMPONENTS; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "COMPONENTS" ("COMPONENT_ID", "COMPONENT_NAME", "CREATED_DATE", "CREATED_BY", "UPDATED_DATE", "UPDATED_BY") FROM stdin;
\.


--
-- TOC entry 1964 (class 0 OID 24596)
-- Dependencies: 166
-- Data for Name: DOCUMENT; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "DOCUMENT" ("DOCUMENT_ID", "DOCUMENT_NAME", "DOCUMENT_DESCRIPTION", "DOCUMENT_FILE") FROM stdin;
\.


--
-- TOC entry 1967 (class 0 OID 24627)
-- Dependencies: 169
-- Data for Name: OPTIONS; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "OPTIONS" ("OPTION_ID", "OPTIONS_NAME", "OPTION_DESCRIPTION") FROM stdin;
\.


--
-- TOC entry 1965 (class 0 OID 24614)
-- Dependencies: 167
-- Data for Name: POSITION; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "POSITION" ("POSITION_ID", "POSITION_DESCRIPTION", "POSITION_NAME") FROM stdin;
\.


--
-- TOC entry 1963 (class 0 OID 24591)
-- Dependencies: 165
-- Data for Name: STATUS; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "STATUS" ("STATUS_ID", "STATUS_DESCRIPTION") FROM stdin;
\.


--
-- TOC entry 1966 (class 0 OID 24622)
-- Dependencies: 168
-- Data for Name: SUB_COMPONENTS; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "SUB_COMPONENTS" ("SUB_COMPONENT_ID", "COMPONENT_ID", "LABEL", "OPTION_ID", "MODIFIED_DATE", "MODIFIED_BY", "CREATED_DATE", "CREATED_BY") FROM stdin;
\.


--
-- TOC entry 1959 (class 0 OID 16386)
-- Dependencies: 161
-- Data for Name: TEMPLATE; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "TEMPLATE" ("TEMPLATE_ID", "TEMPLATE_NAME", "CREATED_DATE", "CREATED_BY", "UPDATED_DATE", "UPDATED_BY") FROM stdin;
\.


--
-- TOC entry 1962 (class 0 OID 24576)
-- Dependencies: 164
-- Data for Name: TEMPLATE_COMPONENTS; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "TEMPLATE_COMPONENTS" ("TEMPLATE_ID", "COPONENT_ID") FROM stdin;
\.


--
-- TOC entry 1960 (class 0 OID 16391)
-- Dependencies: 162
-- Data for Name: USER_FORMS; Type: TABLE DATA; Schema: MightyIFS; Owner: postgres
--

COPY "USER_FORMS" ("FORM_ID", "TEMPLATE_ID", "USER_ID", "CREATED_DATE", "CREATED_BY", "UPDATED_DATE", "UPDATED_BY", "COMPONENT_ID", "POSITION_ID", "STATUS_ID", "DOCUMENT_ID") FROM stdin;
\.


--
-- TOC entry 1939 (class 2606 OID 24603)
-- Dependencies: 166 166
-- Name: DOCUMENT_PK; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "DOCUMENT"
    ADD CONSTRAINT "DOCUMENT_PK" PRIMARY KEY ("DOCUMENT_ID");


--
-- TOC entry 1916 (class 2606 OID 16395)
-- Dependencies: 162 162
-- Name: FORM_PRIMARY_ID; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "USER_FORMS"
    ADD CONSTRAINT "FORM_PRIMARY_ID" PRIMARY KEY ("FORM_ID");


--
-- TOC entry 1949 (class 2606 OID 24634)
-- Dependencies: 169 169
-- Name: OPTION_ID_INDEX; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "OPTIONS"
    ADD CONSTRAINT "OPTION_ID_INDEX" PRIMARY KEY ("OPTION_ID");


--
-- TOC entry 1941 (class 2606 OID 24621)
-- Dependencies: 167 167
-- Name: POSITION_ID_INDEX; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "POSITION"
    ADD CONSTRAINT "POSITION_ID_INDEX" PRIMARY KEY ("POSITION_ID");


--
-- TOC entry 1929 (class 2606 OID 16400)
-- Dependencies: 163 163
-- Name: PRIMARY_COMPONENT_ID; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "COMPONENTS"
    ADD CONSTRAINT "PRIMARY_COMPONENT_ID" PRIMARY KEY ("COMPONENT_ID");


--
-- TOC entry 1936 (class 2606 OID 24595)
-- Dependencies: 165 165
-- Name: STATUS_PK; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "STATUS"
    ADD CONSTRAINT "STATUS_PK" PRIMARY KEY ("STATUS_ID");


--
-- TOC entry 1945 (class 2606 OID 24626)
-- Dependencies: 168 168 168
-- Name: SUB_COMP_PK; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SUB_COMPONENTS"
    ADD CONSTRAINT "SUB_COMP_PK" PRIMARY KEY ("SUB_COMPONENT_ID", "COMPONENT_ID");


--
-- TOC entry 1932 (class 2606 OID 24580)
-- Dependencies: 164 164 164
-- Name: TEMPLATE_COMPONENT_ID; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "TEMPLATE_COMPONENTS"
    ADD CONSTRAINT "TEMPLATE_COMPONENT_ID" PRIMARY KEY ("TEMPLATE_ID", "COPONENT_ID");


--
-- TOC entry 1914 (class 2606 OID 16390)
-- Dependencies: 161 161
-- Name: TEMPLATE_PRIMARY; Type: CONSTRAINT; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "TEMPLATE"
    ADD CONSTRAINT "TEMPLATE_PRIMARY" PRIMARY KEY ("TEMPLATE_ID");


--
-- TOC entry 1926 (class 1259 OID 24604)
-- Dependencies: 163
-- Name: COMPONENT_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "COMPONENT_ID_INDEX" ON "COMPONENTS" USING btree ("COMPONENT_ID");


--
-- TOC entry 1927 (class 1259 OID 24605)
-- Dependencies: 163
-- Name: COMPONENT_NAME_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "COMPONENT_NAME_INDEX" ON "COMPONENTS" USING btree ("COMPONENT_NAME");


--
-- TOC entry 1937 (class 1259 OID 24606)
-- Dependencies: 166
-- Name: DOCUMENT_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "DOCUMENT_ID_INDEX" ON "DOCUMENT" USING btree ("DOCUMENT_ID");


--
-- TOC entry 1934 (class 1259 OID 24607)
-- Dependencies: 165
-- Name: STATUS_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX "STATUS_ID_INDEX" ON "STATUS" USING btree ("STATUS_ID");


--
-- TOC entry 1942 (class 1259 OID 24648)
-- Dependencies: 168
-- Name: SUBCOMP_COMP_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX "SUBCOMP_COMP_ID_INDEX" ON "SUB_COMPONENTS" USING btree ("COMPONENT_ID");

ALTER TABLE "SUB_COMPONENTS" CLUSTER ON "SUBCOMP_COMP_ID_INDEX";


--
-- TOC entry 1943 (class 1259 OID 24647)
-- Dependencies: 168
-- Name: SUB_COMP_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX "SUB_COMP_ID_INDEX" ON "SUB_COMPONENTS" USING btree ("SUB_COMPONENT_ID");


--
-- TOC entry 1930 (class 1259 OID 24610)
-- Dependencies: 164
-- Name: TEMAPLATE_ID_COMP_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "TEMAPLATE_ID_COMP_INDEX" ON "TEMPLATE_COMPONENTS" USING btree ("COPONENT_ID");


--
-- TOC entry 1933 (class 1259 OID 24609)
-- Dependencies: 164
-- Name: TEMPLATE_COMPONENT_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "TEMPLATE_COMPONENT_ID_INDEX" ON "TEMPLATE_COMPONENTS" USING btree ("TEMPLATE_ID");

ALTER TABLE "TEMPLATE_COMPONENTS" CLUSTER ON "TEMPLATE_COMPONENT_ID_INDEX";


--
-- TOC entry 1917 (class 1259 OID 16407)
-- Dependencies: 162
-- Name: TEMPLATE_ID; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "TEMPLATE_ID" ON "USER_FORMS" USING btree ("TEMPLATE_ID");


--
-- TOC entry 1912 (class 1259 OID 24608)
-- Dependencies: 161
-- Name: TEMPLATE_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX "TEMPLATE_ID_INDEX" ON "TEMPLATE" USING btree ("TEMPLATE_ID");


--
-- TOC entry 1918 (class 1259 OID 24612)
-- Dependencies: 162
-- Name: USERFORM_TEMPLATE_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX "USERFORM_TEMPLATE_ID_INDEX" ON "USER_FORMS" USING btree ("TEMPLATE_ID");


--
-- TOC entry 1919 (class 1259 OID 24611)
-- Dependencies: 162
-- Name: USER_FORM_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX "USER_FORM_ID_INDEX" ON "USER_FORMS" USING btree ("FORM_ID");


--
-- TOC entry 1920 (class 1259 OID 24613)
-- Dependencies: 162
-- Name: USER_FORM_USER_ID_INDEX; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX "USER_FORM_USER_ID_INDEX" ON "USER_FORMS" USING btree ("USER_ID");

ALTER TABLE "USER_FORMS" CLUSTER ON "USER_FORM_USER_ID_INDEX";


--
-- TOC entry 1921 (class 1259 OID 24654)
-- Dependencies: 162
-- Name: fki_COMPONENT_COMP_FK; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "fki_COMPONENT_COMP_FK" ON "USER_FORMS" USING btree ("COMPONENT_ID");


--
-- TOC entry 1922 (class 1259 OID 24672)
-- Dependencies: 162
-- Name: fki_DOCUMENT_USERFORM; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "fki_DOCUMENT_USERFORM" ON "USER_FORMS" USING btree ("DOCUMENT_ID");


--
-- TOC entry 1923 (class 1259 OID 24666)
-- Dependencies: 162
-- Name: fki_STATUS_USERFORM; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "fki_STATUS_USERFORM" ON "USER_FORMS" USING btree ("STATUS_ID");


--
-- TOC entry 1946 (class 1259 OID 24640)
-- Dependencies: 168
-- Name: fki_SUB_COMPONENT_OPTION_PK; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "fki_SUB_COMPONENT_OPTION_PK" ON "SUB_COMPONENTS" USING btree ("OPTION_ID");


--
-- TOC entry 1947 (class 1259 OID 24646)
-- Dependencies: 168
-- Name: fki_SUB_COMP_COMP; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "fki_SUB_COMP_COMP" ON "SUB_COMPONENTS" USING btree ("COMPONENT_ID");


--
-- TOC entry 1924 (class 1259 OID 24660)
-- Dependencies: 162
-- Name: fki_USERFORM_POS_FK; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "fki_USERFORM_POS_FK" ON "USER_FORMS" USING btree ("POSITION_ID");


--
-- TOC entry 1925 (class 1259 OID 16406)
-- Dependencies: 162
-- Name: fki_USERFORM_TEMPLATE_FOREIGHNKEY; Type: INDEX; Schema: MightyIFS; Owner: postgres; Tablespace: 
--

CREATE INDEX "fki_USERFORM_TEMPLATE_FOREIGHNKEY" ON "USER_FORMS" USING btree ("TEMPLATE_ID");


--
-- TOC entry 1951 (class 2606 OID 24649)
-- Dependencies: 162 1928 163
-- Name: COMPONENT_COMP_FK; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "USER_FORMS"
    ADD CONSTRAINT "COMPONENT_COMP_FK" FOREIGN KEY ("COMPONENT_ID") REFERENCES "COMPONENTS"("COMPONENT_ID");


--
-- TOC entry 1954 (class 2606 OID 24667)
-- Dependencies: 166 162 1938
-- Name: DOCUMENT_USERFORM; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "USER_FORMS"
    ADD CONSTRAINT "DOCUMENT_USERFORM" FOREIGN KEY ("DOCUMENT_ID") REFERENCES "DOCUMENT"("DOCUMENT_ID");


--
-- TOC entry 1953 (class 2606 OID 24661)
-- Dependencies: 165 162 1935
-- Name: STATUS_USERFORM; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "USER_FORMS"
    ADD CONSTRAINT "STATUS_USERFORM" FOREIGN KEY ("STATUS_ID") REFERENCES "STATUS"("STATUS_ID");


--
-- TOC entry 1957 (class 2606 OID 24635)
-- Dependencies: 1948 168 169
-- Name: SUB_COMPONENT_OPTION_PK; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "SUB_COMPONENTS"
    ADD CONSTRAINT "SUB_COMPONENT_OPTION_PK" FOREIGN KEY ("OPTION_ID") REFERENCES "OPTIONS"("OPTION_ID");


--
-- TOC entry 1958 (class 2606 OID 24641)
-- Dependencies: 1928 168 163
-- Name: SUB_COMP_COMP; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "SUB_COMPONENTS"
    ADD CONSTRAINT "SUB_COMP_COMP" FOREIGN KEY ("COMPONENT_ID") REFERENCES "COMPONENTS"("COMPONENT_ID");


--
-- TOC entry 1955 (class 2606 OID 24581)
-- Dependencies: 1913 161 164
-- Name: TEMPLATE_COMPONENT_TAMPLATE_FK; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "TEMPLATE_COMPONENTS"
    ADD CONSTRAINT "TEMPLATE_COMPONENT_TAMPLATE_FK" FOREIGN KEY ("TEMPLATE_ID") REFERENCES "TEMPLATE"("TEMPLATE_ID");


--
-- TOC entry 1956 (class 2606 OID 24586)
-- Dependencies: 164 163 1928
-- Name: TMEPLATE_COMPONENT_COMPONENT_FK; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "TEMPLATE_COMPONENTS"
    ADD CONSTRAINT "TMEPLATE_COMPONENT_COMPONENT_FK" FOREIGN KEY ("COPONENT_ID") REFERENCES "COMPONENTS"("COMPONENT_ID");


--
-- TOC entry 1952 (class 2606 OID 24655)
-- Dependencies: 1940 162 167
-- Name: USERFORM_POS_FK; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "USER_FORMS"
    ADD CONSTRAINT "USERFORM_POS_FK" FOREIGN KEY ("POSITION_ID") REFERENCES "POSITION"("POSITION_ID");


--
-- TOC entry 1950 (class 2606 OID 16401)
-- Dependencies: 1913 162 161
-- Name: USERFORM_TEMPLATE_FOREIGHNKEY; Type: FK CONSTRAINT; Schema: MightyIFS; Owner: postgres
--

ALTER TABLE ONLY "USER_FORMS"
    ADD CONSTRAINT "USERFORM_TEMPLATE_FOREIGHNKEY" FOREIGN KEY ("TEMPLATE_ID") REFERENCES "TEMPLATE"("TEMPLATE_ID");


-- Completed on 2011-12-26 00:18:38 IST

--
-- PostgreSQL database dump complete
--
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--                            DB Created. - 26 Dec 2011.
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
    
   --------update template 3 img type---05-03-2012-------
   UPDATE template SET  image_type='square'  WHERE template_id=3; 
   UPDATE template SET  image_type='square'  WHERE template_id=4; 
    
    