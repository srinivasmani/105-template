<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
 <%@ include file="/header.jsp"%>
<meta http-equiv="content-language" content="en-GB" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script type="text/javascript" src="../js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<style>
.logo1 {
	float: left;
	height: 0;
	margin-right: 3px;
	margin-top: -2px;
}
.logo2 {
    float: right;
    height: 0;
    margin-right: 40px;
    margin-top: -113px;
}
.leftOptImage {
	border: 1px solid black;
	font-size: 12px;
	height: 2.54cm !important;
	width: 2.54cm !important;
}
.rghtOptImage {
    border: 1px solid black;
    font-size: 12px;
    height: 2.54cm !important;
    margin-top: 0px;
    width: 2.54cm !important;
}
.leftOptImageNoBorder {
	border: 1px solid black;
	font-size: 12px;
	height: 2.54cm !important;
	width: 2.54cm !important;
}
.rghtOptImageNoBorder {
	border: 1px solid black;
	font-size: 12px;
	height: 2.54cm !important;
	width: 2.54cm !important;
}
.img {
    border: 1px solid black;
    font-size: 28px;
    height: 93px !important;
    width: 300px !important;
}
.imgNoBorder {
    border: 1px solid black;
    font-size: 28px;
    height: 2.54cm !important;
    width: 10.16cm !important;
}
.divTable1 .editme1 {
    position: relative !important;
    top: 12px !important;
    white-space: nowrap !important;
}
#msg {
    font-size: 10pt;
    letter-spacing: 0.5px;
    margin-left: 0;
    margin-top: 12%;
}
</style>
		<div class="container">
	<%-- <%@ include file="WebContent/WEB-INF/userNavigation.jsp" %>  --%>
		<div class="header"></div>
		<div class="centercontainer">
		<div class="nav">
			<ul id="headerList">
				<li><a href="./navigateTemplateView.do" class="notSelected" id="tem1" onclick="launchWindow('#dialog');">Template 101</a>
				</li>
				<li><a href="./navigateTemplateIfs.do" class="selected" id="tem2" onclick="launchWindow('#dialog');">Template 102</a>
				</li>
				<li><a href="./navigateTemplate103.do" class="notSelected" id="tem3" onclick="launchWindow('#dialog');">Template 103</a>
				</li>
				<li><a href="./navigateTemplate104.do" class="notSelected" id="tem4" onclick="launchWindow('#dialog');">Template 104</a>
				</li>
			</ul>
		</div>
		<div style="clear: both"></div>
		<div class="innercontainer" id="innercontainer">
			<%String getSVG = (String) request.getAttribute("imgType");
				if ( (getSVG != null) && (getSVG.equalsIgnoreCase("circle"))) {
			%>
			<% getSVG = "<div class='svgTable' ><img height='41' width='41' alt='' src='../images/green_circle.png'/>"+
			"<img height='41' width='41' alt='' src='../images/Yellow_circle.png'/>"+
			"<img height='41' width='41' alt='' src='../images/red_circle.png'/></div>"; 
			}else{%>
			<% getSVG = "<div class='svgTable' ><img height='41' width='41' alt='' src='../images/green.PNG'/>"+
			"<img height='41' width='41' alt='' src='../images/yellow.PNG'/>"+
			"<img height='41' width='41' alt='' src='../images/red.PNG'/></div>"; }%>
			
			<!-- Complete Width and Height -->
			<div id="templateSize">
				<!-- User Logo -->
				<div class="divLogoTab" style="width:21.59cm;;">
					<div class="divLogoRow">
						<div class="logo1">
							<div class="leftOptImage">
									<a href="javascript: void(0);"></a>
								</div>
						</div>
						<div class="imglogo">
							<div align="center">
									<c:if test="${template.templateImage != null && template.templateImage !=''}">
									<div class="imgNoBorder" id="img">
										<img src="/ImageLibrary/${template.templateImage}" style="margin-top:-1px;margin-left:-1px;width:10.22cm;height:2.60cm;"/>
										</div>
									</c:if>
									<c:if test="${template.templateImage == null || template.templateImage ==''}">
									<div class="img" id="img">
										<p id="msg">(Your logo here)</p>
									</div>
									</c:if>
								
							</div>
						</div>						
						<div class="logo2">
							<div class="rghtOptImage">
									<a href="javascript: void(0);"></a>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Page Heading -->
				<div id="pageHeading">
					<p id="heading">
						<label class="text_label" id="lblText">
							<nobr>
								<c:out value="${template.headerText}"/>
							</nobr>
						</label>
					</p>
					<!--<div class="edit"></div>-->																
					<div class="clear"></div>
				</div>

				<!-- Form 1 -->
				<div class="divTable">
				
					<div class="divRow">
						<div class="divCell" id="customer">
							<p class="inspec_formp" id="customerP" onclick="setVal(this.id,'divTxt_cutomer','txtVal_customer');">
								<c:out value="${template.components[0].componentName}"/>
							</p>														
						</div>						
					</div>
				</div>
				<div class="clear"></div>

				<!-- ATTENTION -->
				<div class="selTable">
					<div class="selRow">
						<div class="selCol">
							<div class="selCell">														
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<img  src="../images/green_circle.png" class="selcimg">
									</c:when>
									<c:otherwise>
										<img height='41' width='41' alt='' src='../images/green.PNG'/>
									</c:otherwise>
								</c:choose>
								<span>
									<p class="editme1" id="attentionP" onclick="setVal(this.id,'divTxt_attention','txtVal_attention');">
										<c:out value="${template.components[1].componentName}"/>
									</p>									
								</span>
								<br /> <input type="image" class="color" name="clr" id="inp1"
									style="display: none; margin-left: 11px; margin-top: -5px;"
									src="../images/images.png">
							</div>
						</div>
						
						<div class="selCol">
							<div class="selCell">								
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<img   src="../images/Yellow_circle.png" class="selcimg">
									</c:when>
									<c:otherwise>
										<img height='41' width='41' alt='' src='../images/yellow.PNG'/>
									</c:otherwise>
								</c:choose>
								<span id="nobr">
									<p class="editme1" id="fAttentionP" onclick="setVal(this.id,'divTxt_fAttention','txtVal_fAttention');">
										<c:out value="${template.components[2].componentName}"/>
									</p>																		
								</span>
								<br /> <input type="image" class="color" name="clr" id="inp2"
									style="display: none; margin-left: 11px; margin-top: -5px;"
									src="../images/images.png">
							</div>
						</div>
						
						<div class="selCol">
							<div class="selCell">
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<img   src="../images/red_circle.png" class="selcimg">
									</c:when>
									<c:otherwise>
										<img height='41' width='41' alt='' src='../images/red.PNG'/>
									</c:otherwise>
								</c:choose>
								<span id="nobr">
									<p class="editme1" id="iAttentionP" onclick="setVal(this.id,'divTxt_iAttention','txtVal_iAttention');">
										<c:out value="${template.components[3].componentName}"/>																	
									</p>																		
								</span>
								<br /> <input type="image" class="color" name="clr" id="inp3"
									style="display: none; margin-left: 11px; margin-top: -5px;"
									src="../images/images.png">
							</div>
						</div>
						
					</div>
				</div>

				<!-- Items And Conditions -->
				<div class="divTable1">
					<div id="div1">
						<div class="divRow ">
							<div class="divCell1 rowheading" id="content">
								<p class="editme1"  id="itemsP" onclick="setVal(this.id,'divTxt_items','txtVal_items');">
									<c:out value="${template.components[4].componentName}"/>
								</p>								
							</div>
							
							<div class="divCell1 rowheading" id="content">
								<p class="editme1" id="conditionP" onclick="setVal(this.id,'divTxt_condition','txtVal_condition');">
									<c:out value="${template.components[5].componentName}"/>
								</p>								
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" id="lightsP" onclick="setVal(this.id,'divTxt_lights','txtVal_lights');">
									<c:out value="${template.components[6].componentName}"/>									
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" id="wiperP" onclick="setVal(this.id,'divTxt_wiper','txtVal_wiper');">
									<c:out value="${template.components[7].componentName}"/>								
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" id="windP" onclick="setVal(this.id,'divTxt_wind','txtVal_wind');">
									<c:out value="${template.components[8].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_engine','txtVal_engine');" id="engineP">
									<c:out value="${template.components[9].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_oilLevel','txtVal_oilLevel');" id="oilLevelP">
									<c:out value="${template.components[10].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_oilCondition','txtVal_oilCondition');" id="oilConditionP">
									<c:out value="${template.components[11].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_brakeFluid','txtVal_brakeFluid');" id="brakeFluidP">
									<c:out value="${template.components[12].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_powerFluid','txtVal_powerFluid');" id="powerFluidP">
									<c:out value="${template.components[13].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_washerFluid','txtVal_washerFluid');" id="washerFluidP">
									<c:out value="${template.components[14].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_trmFluid','txtVal_trmFluid');" id="trmFluidP">
									<c:out value="${template.components[15].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_belt','txtVal_belt');" id="beltP">
									<c:out value="${template.components[16].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_hoses','txtVal_hoses');" id="hosesP">
									<c:out value="${template.components[17].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_coolant','txtVal_coolant');" id="coolantP">
									<c:out value="${template.components[18].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_airFilter','txtVal_airFilter');" id="airFilterP">
									<c:out value="${template.components[19].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
					</div>
					
					
					<div id="div2">
						<div class="divRow">
							<div class="divCell1 rowheading" id="content">
								<p class="editme1" id="items1P" onclick="setVal(this.id,'divTxt_items1','txtVal_items1');">
									<b><c:out value="${template.components[20].componentName}"/> </b>
								</p>															
							</div>
							<div class="divCell1 rowheading" id="content">
								<p class="editme1" id="condition1P" onclick="setVal(this.id,'divTxt_condition1','txtVal_condition1');">
									<c:out value="${template.components[21].componentName}"/>
								</p>								
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_cabinFilter','txtVal_cabinFilter');" id="cabinFilterP">
									<c:out value="${template.components[22].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_fuelSystem','txtVal_fuelSystem');" id="fuelSystemP">
									<c:out value="${template.components[23].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_battery','txtVal_battery');" id="batteryP">
									<c:out value="${template.components[24].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_sparkPlug','txtVal_sparkPlug');" id="sparkPlugP">
									<c:out value="${template.components[25].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_fuel','txtVal_fuel');" id="fuelP">
									<c:out value="${template.components[26].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_fluid','txtVal_fluid');" id="fluidP">
									<c:out value="${template.components[27].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_tirePressure','txtVal_tirePressure');" id="tirePressureP">
									<c:out value="${template.components[28].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_tireRotation','txtVal_tireRotation');" id="tireRotationP">
									<c:out value="${template.components[29].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_tireCondition','txtVal_tireCondition');" id="tireConditionP">
									<c:out value="${template.components[30].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_alignment','txtVal_alignment');" id="alignmentP">								
									<c:out value="${template.components[31].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_shocks','txtVal_shocks');" id="shocksP">
									<c:out value="${template.components[32].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_boots','txtVal_boots');" id="bootsP">
									<c:out value="${template.components[33].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_exhaust','txtVal_exhaust');" id="exhaustP">
									<c:out value="${template.components[34].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p class="editme1" onclick="setVal(this.id,'divTxt_others','txtVal_others');" id="othersP">
									<c:out value="${template.components[35].componentName}"/>
								</p>								
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
					</div>
				</div>
<div class="clear"></div>
				<!-- Form 2 -->
			</div>
			<div class="clear"></div>
			<div class=" bottomtext">
				<span id="cmt" style="float:left;">Comments:</span> <span style="border-bottom:1px solid #000; display:block; float:right;width:760px;margin-top:15px;"></span>
			  <br /><span  style="border-bottom:1px solid #000; display:block; clear:left;width:100%;position:relative;top:10px;"></span>
				<br /><span  style="border-bottom:1px solid #000; display:block;clear:left;width:100%;margin-top:10px;"></span>
				<br /><span id="cmt" style="float:left;clear:left;">Inspected by: </span> <span style="border-bottom:1px solid #000; display:block; float:left;width:38%;margin-top:15px;margin-left:1%;margin-right:1%;"></span><span style="width:20%; flioat:left;">Date:</span>
 <span style="border-bottom:1px solid #000; display:block; float:right;width:44%;margin-top:15px;"></span>
				</div>
		
			<div class="containerbtm" style="margin: 3% 3% 3% 35%;">
				<a style="margin-right:5px;" href="./editNavTemplateIfs.do" class="EditBtn btn-txt">Edit Template
				<span  style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
				</a>
       			<a href="./GenerateTemplate2Pdf.do?templateId=${template.templateId}" target="_newtab" class="PdfBtn btn-txt">Generate PDF
       			<span  style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
       			</a>

			</div>
		
		<div class="clear"></div>
	</div>
	
</div>
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>