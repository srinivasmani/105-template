<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

 <c:choose>
   <c:when test="${user.userRole  eq 'admin'}">
 	<ul>
		<li><a href="./navigateTemplateView.do">Template 101</a>
		</li>
		<li><a href="./navigateTemplateIfs.do">Template 102</a>
		</li>
		<li><a href="./navigateTemplate103.do">Template 103</a>
		</li>
		<li><a href="./navigateTemplate104.do" class="selected">Template 104</a>
		</li>
		<li><a href="./addImageToLibrary.do" >Add image </a>
		</li>
	</ul>
	</c:when>
	<c:otherwise>
		<ul>
				<li><a href="./navigateTemplateView.do">Template 101</a>
				</li>
				<li><a href="./getUserComponentIfs.do" class="selected">Template 102</a>				
				</li>
				<li><a href="./navigateTemplate103.do">Template 103</a>
				</li>
				<li><a href="./createUserComponentForm104.do">Template 104</a>
				</li>
		</ul>
	</c:otherwise>
 </c:choose>