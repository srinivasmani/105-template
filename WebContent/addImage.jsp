<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<title>Add image to Image Library</title>
<!-- Style Sheet -->
<link rel="stylesheet" type="text/css" href="../css/style-edit.css"></link>
<link type="text/css" rel="stylesheet" href="../css/jquery.miniColors.css" />
<link type="text/css" rel="stylesheet" href="../css/pagination.css" />
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<script type="text/javascript" src="../js/popup.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/jquery-editTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery-adminEditTemplate.js"></script>
<script type="text/javascript" src="../js/jquery-addOptions.js"></script>
<script type="text/javascript" src="../js/jquery.miniColors.js"></script>
<script type="text/javascript" src="../js/jquery.mini.color.js"></script>
<style>
.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity = 80);
	display: none;
}

.box_content {
	position: fixed;
	top: 10%;
	left: 28%;
	/*height: 30%;*/
	right: 30%;
	/*width: 43%;*/
	width:575px;
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	
	background: none repeat scroll 0 0 #FFFFFF;
	border: 8px solid #ACACAC;
}
#inpDefault{
	width: 20px;
	height: 20px;
	background-color: #045fb4;
}

#imgLogo {
    border: 1px solid black;
    height: 600px;
   
    margin-top: 10px;
    padding: 0;
    text-align: center;
    width: 600px;
    margin-left:auto;
    margin-right:auto;
}
#item .odd, #item .even {
border: 1px solid #ACACAC !important;
margin-right:5px;
margin-bottom:5px;
}
.selectLink {
    left: 0px !important;
    position: relative;
}
</style>
<div class="container">
	<!--Header starts-->
	
	<div class="header"></div>
	<%@ include file="WEB-INF/header/userNavigation.jsp"%>
	<div id="centercontainer" class="centercontainer">
		<div class="nav">
			 <%@ include file="WEB-INF/header/headerLink5.jsp"%>
		</div>
		<div class="innercontainer">
		<div id="loaderImg"></div>
			<div >
				<div align="left" style=" margin-left: 10px; margin-top: 0px;" >
					<form name="uploadLocalImage" action="./editTemplate2.do" method="post" id="uploadLocalImage" enctype="multipart/form-data">
						<div class="sideheading">
							<span style="float:left;">
								<label><nobr>Upload your own image to Image Library :</nobr> </label>
								<input name="fileImage" type="file" id="lbImage" value="${logoImage}" />
							</span>
							<span id="saveBtn" style="float:left;margin-left:5px;"></span>
						</div>
					</form>
				</div>
			</div>
			<!-- not showing preview   -->
			
			<div id="imgLogo" align="left" class="imgLogo" style="display: none;" >
					<c:choose>
						<c:when test="${template.templateImage eq null || template.templateImage eq ''}">
							<p id="msg"> image preview</p>
						</c:when>
						<c:otherwise>
							<img src="/ImageLibrary/${template.templateImage}"  style="max-width:380px; max-height:100px;">
						</c:otherwise>
					</c:choose>
			</div>
		</div>
	</div>
</div>
