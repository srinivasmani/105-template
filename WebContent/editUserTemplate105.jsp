<!DOCTYPE html>
<html>
    <head>
        <%@ page import="java.io.*,java.util.*" language="java"%>
        <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
        <%@ include file="header.jsp"%>
        <%@ page language="java"%>
        <%@include file="UploadImage.jsp" %>
        <title>User Edit Template 5</title>
<!--        <script type="text/javascript" src="../editor/tinymce/tinymce.min.js"></script>-->
        <script type="text/javascript" src="../editor/ckeditor/ckeditor/ckeditor.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/stylesheet.css"></link>
        <script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
        <script type="text/javascript" src="../js/jquery.form.js"></script>
        <script type="text/javascript" src="../js/Jquery-Validation.js"></script>
        <script type="text/javascript" src="../js/Template105.js"></script>
        <script type="text/javascript" src="../js/jquery-userEditTemplate.js"></script>
        <script type="text/javascript" src="../js/popup.js"></script>
        <script type="text/javascript" src="../js/html2canvas.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/popUp.css">
        <style>
            .grayBox {
                position: fixed;
                top: 0%;
                left: 0%;
                width: 100%;
                height: 100%;
                background-color: black;
                z-index: 1001;
                -moz-opacity: 0.8;
                opacity: .80;
                filter: alpha(opacity = 80);
                display: none;
            }

            .box_content {
                position: fixed;
                left: 25%;
                right: 30%;
                width: 575px;
                /*height: 30%;*/
                padding: 16px;
                z-index: 1002;
                overflow: auto;
                border: 8px solid #ACACAC;
                background: none repeat scroll 0 0 #FFFFFF;
                border: 8px solid #ACACAC;
            }
            #img {
                position:relative;
                float:none;
            }
            #img #text {
                left: 74px;
                position: absolute;
                top: 7px;
                width: 300px; 
            }
            #repositoryPopUp{
                height:auto !important;
                border:8px solid #acacac !important;
                width:625px !important;
                margin-top:-65px;
            }
            #item .odd, #item .even {
                border: 1px solid #ACACAC !important;
                margin-right:5px;
                margin-bottom:5px;
            }
            .selectLink {
                left: 36px !important;
                position: relative;
            }
           
            #commentDiv {
                font-family: 'MyriadProRegular', Sans-Serif;
                font-size: 11pt;
                letter-spacing: 0.5px;
                width: 900px;
            }
            #msg {
                font-size: 10pt;
                letter-spacing: 0.5px;
                margin-left: 25%;
                margin-top: 25%;
            }
            .imgLogo {
                border: 1px solid black;
                height: 200px;
                margin-top: 0;
                padding: 0;
                text-align: center;
                width: 393px;
            }

            .customer-details {
                font-family: 'MyriadProRegular';
                font-size: 11pt;
                list-style: none outside none;
                margin-top: 5px;
                width: 362px;
            }

            .customer-details li {
                border-bottom: 1px solid #333;
                padding:15px 0 3px;
                display: block;
            }

            #customer {
                margin-left: 0;
            }

            #templateSize.template-105 {
                //margin-left: 48px;
            }

            #templateSize.template-105 .divLogoTab{
                border-spacing: 0;
                float: left;
                margin-left: 10px;
                width: 354px;
            }
            #templateSize.template-105 .divLogoTab .imgLogo{
                float: left;
                height: 250px !important;
                width: 350px !important;
                margin-left: 24px !important;
                margin-top: 25px !important;
            }
            #templateSize.template-105 .divLogoTab .imgLogo #msg {
                margin-left: 25% !important;
                margin-top: 30% !important;
            }
            #templateSize.template-105 #customer.divCell  {
                margin-right: 7px;
            }
            #templateSize.template-105 #customer.divCell .customer-details li{
                padding: 2px 0 3px;
                margin-bottom: 13px;
            }
            #templateSize.template-105 .inspectionleftwrap {
                width: 392px;
            }
            #templateSize.template-105 .divTable { margin-top: 0px;}
            #templateSize.template-105 .inspectionTxt4 {
                font-size: 11.5pt;
            }
            #templateSize.commentsClass {
                font-family: 'MyriadProRegular';
                font-size: 13pt;
                height: 1px;
                margin-left: 8px;
                padding-top: 5px;
            }
            #component39 {
                height: auto;
                font-family: 'MyriadProRegular';
                font-size: 10pt !important;
                font-style: normal;
                color:#000000 !important;
                padding-bottom: 0px;
                text-align: left;
                width: 18px !important;
                font-weight: bold;
            }
            #component45, #component46 {
                height: auto;
                font-family: 'MyriadProRegular';
                font-size: 10pt !important;
                font-style: normal;
                color:#000000 !important;
                padding-bottom: 0px;
                text-align: left;
                width: 18px !important;
                font-weight: bold;
                left: -4px;
                margin-top: 5px;
            }
            h2.titleFont {
                padding-bottom: 0;
            }
            #component50, #component51, #component52, #component53 {
                height: auto;
                font-family: 'MyriadProRegular';
                font-size: 8pt !important;
                width: 56px !important;
                font-weight: bold;
                font-style: italic;
                text-align: left;
                left: -4px;
                position: relative;
            }
            #component56, #component57 {
                font-size: 9pt !important;
                font-style: italic;
                height: 36px;
                line-height: 9px;
                overflow: hidden;
                width: 144px;
                text-align: left;
                font-weight: bold;
            }
            #component59, #component60, #component61, #component62 {
                width: 18px !important;
                height: auto !important;
                font-style: normal !important;
                font-family: 'MyriadProRegular' !important;
                font-size: 11pt !important;
            }
            .edit-icon-10 {
                position: absolute;
                right: 0;
                top: 8px;
                cursor: pointer;
            }  
            #component5 {
                color: #000000;
                font-family: 'MyriadProRegular';
                font-size: 7pt !important;
            }
            #component47, #component48{
                font-size: 10pt !important;
            }
            
            #component26,#component27,#component28{
                 font-size: 10pt;
                 font-style: oblique;
            }
           
            #component29,#component30,#component31,#component32,#component33,#component34,#component35,#component36 {
                font-style: normal;
                font-size: 10pt !important;
                font-family: Arial;
            }
                 
            #logoText{
                border: 1px solid black;
                height: 280px;
                width: 395px;
                font-size:100%;
                float: none;
            }
            
            #logoText h1 {
                float: none;
                display: block;
                font-size: 32px;
                font-weight: bold;                
            }          
            #logoText h2 {
                float: none;
                display: block;
                font-size: 24px;
                font-weight: bold;                
            }          
            #logoText h3 {
                float: none;
                display: block;
                font-size: 18.7px;
                font-weight: bold;                
            }          
            #logoText h4 {
                float: none;
                display: block;
                font-size: 16px;
                font-weight: bold;                
            }          
            #logoText h5 {
                float: none;
                display: block;
                font-size: 13px;
                font-weight: bold;                
            }          
            #logoText h6 {
                float: none;
                display: block;
                font-size: 10.7px;
                font-weight: bold;                
            }          

            #logoText p {
                float: none;
                display: block;
                font-size: 16px;                
            }
            
            #logoText ol {
                float: none;                
                font-size: 16px;
                list-style-type: decimal;
                padding-left: 40px;
            }
            
            #logoText ul {
                float: none;                
                font-size: 16px;
                list-style-type: disc;
                padding-left: 40px;
            }
            
            #logoText li {
                float: none;                
                font-size: 16px;                
            }
            .itb-list {
                padding-left: 20px;
                padding-bottom: 5px;
                font-size: 16px;
            }
            .itb-list-item {
                white-space: nowrap;
            }
            .itb-list-item-content {
                white-space: normal;
            }
            .itb-list-item-prefix,
            .itb-list-item-content {
                display: inline-block;
                font-size: 16px;                
            }
            .itb-list-item-prefix {
                float: left;
            }
            .itb-super{
                font-size: 13px;
                vertical-align: top;                
                line-height: 0;
            }
            itb-sub{
                font-size: 13px;
                vertical-align: sub;
                line-height: 0;
            }
            
            
            .smallGreenCircle, .smallRedCircle, .smallYellowCircle {
                width: 22px;
                height: 22px;
            }
            
            
            .inspection_bg .rightHeadings{
                position: relative;
            }
            .inspection_bg .rightHeadings span.th {
                width: 100%;
                float: none;
            }
            .inspection_bg .rightHeadings span.handSymbol {
                position: absolute;
                right: 5px;
                top: 8px;
                
            }
            
            #msgText{
                border: 1px solid black;
                height: 230px;
                width: 350px;
            }
            
        </style>
        <script type="text/javascript">
                                            $('document').ready(function() {
                                                
                                                if ($("input[disabled='disabled']")) {
                                                    $("input[disabled='disabled']").each(function() {
                                                        $(this).attr('readonly', 'readonly');
                                                    });
                                                    $("input[disabled='disabled']").each(function() {
                                                        $(this).removeAttr('disabled');
                                                    });

                                                }
                                                if ($("textarea[disabled='disabled']")) {
                                                    $("textarea[disabled='disabled']").each(function() {
                                                        $(this).attr('readonly', 'readonly');
                                                    });
                                                    $("textarea[disabled='disabled']").each(function() {
                                                        $(this).removeAttr('disabled');
                                                    });

                                                }

                                                $('textarea[maxlength]').live('keypress blur', function() {
                                                    // Store the maxlength and value of the field.
                                                    var maxlength = $(this).attr('maxlength');
                                                    var val = $(this).val();

                                                    // Trim the field if it has content over the maxlength.
                                                    if (val.length > maxlength) {
                                                        $(this).val(val.slice(0, maxlength));
                                                    }
                                                });
                                            });
                                            
                                           $(document).ready(function(){
                                               
                                               //$('#iframelogotext').contents().find('html').html(jQuery("div#logoText").html());
                                                $('#logoText').contents().find('div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,label,fieldset,input,p,blockquote').css({"margin-top":"1px","margin-bottom":"0px"});
                                                $('#logoTextId').val($('#logoText').html());
                                                generateCanvas();                                                
                                            }); 
                                            
                                            function renderSubSuperScripts(){
                                                
                                                $('sup', $('#logoText')).each(function () {                                                    
                                                    var replica = $("<span class='itb-super'></span>");
                                                    replica.addClass($(this).attr("class"));
                                                    replica.attr("style",$(this).attr("style"));
                                                    replica.html($(this).html());
                                                    $(this).replaceWith(replica);
                                                });
                                            }
                                            
                                            function renderLists(){
                                                $('ul', $('#logoText')).each(function () {
                                                    var replica = $("<div class='itb-list itb-unordered-list'></div>");
                                                    replica.addClass($(this).attr("class"));
                                                    replica.attr("style",$(this).attr("style"));
                                                    
                                                    $(this).find("li").each(function() {
                                                        var item = $("<div class='itb-list-item'></div>");
                                                        item.addClass($(this).attr("class"));
                                                        item.html("<div class='itb-list-item-prefix'>&bullet;&nbsp;&nbsp;&nbsp;</div>" +
                                                                  "<div class='itb-list-item-content'>" + $(this).html() + "</div>");

                                                        replica.append(item);
                                                    });
                                                    $(this).replaceWith(replica);                                                    
                                                });                                                
                                                $('ol', $('#logoText')).each(function () {                                                    
                                                    var replica = $("<div class='itb-list itb-ordered-list'></div>");
                                                    replica.addClass($(this).attr("class"));
                                                    replica.attr("style",$(this).attr("style"));

                                                    $(this).find("li").each(function() {
                                                        var item = $("<div class='itb-list-item'></div>");
                                                        item.addClass($(this).attr("class"));
                                                        item.html(listItemText(this) + $(this).html());

                                                        item.html("<div class='itb-list-item-prefix'>" + listItemText(this) + "</div>" +
                                                                  "<div class='itb-list-item-content'>" + $(this).html() + "</div>");

                                                        replica.append(item);
                                                    });

                                                    $(this).replaceWith(replica);
                                                    //$(document.body).append(replica);
                                                });
                                                
                                            }
                                            
                                            function listItemText(element) {
                                                var currentIndex = elementIndex(element), text;
                                                text = currentIndex;
                                                return text + ".&nbsp;&nbsp;";
                                            };
                                            function elementIndex(el) {
                                                var i = -1,
                                                count = 1,
                                                childs = el.parentNode.childNodes;

                                                if (el.parentNode) {
                                                    while(childs[++i] !== el) {
                                                        if (childs[i].nodeType === 1) {
                                                            count++;
                                                        }
                                                    }
                                                    return count;
                                                } else {
                                                    return -1;
                                                }
                                            };
//                                           tinymce.init({
//                                                    selector:"textarea#logoEditorTiny",
//                                                    menubar:false,
//                                                    statusbar: false,
//                                                    theme: "modern",
//                                                    height: 205,
//                                                    plugins:[ "textcolor"],
//                                                    toolbar1: "styleselect | bold underline italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent| forecolor backcolor emoticons | undo redo | fontselect fontsizeselect",
//                                                    image_advtab: false,
//                                                    style_formats : [
//                                                            {title : 'Line height 5px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '5px'}},
//                                                            {title : 'Line height 10px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '10px'}},
//                                                            {title : 'Line height 12px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '12px'}},
//                                                            {title : 'Line height 14px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '14px'}},
//                                                            {title : 'Line height 16px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '16px'}},
//                                                            {title : 'Line height 18px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '18px'}},
//                                                            {title : 'Line height 20px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '20px'}},
//                                                            {title : 'Line height 24px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '24px'}},
//                                                            {title : 'Line height 26px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '26px'}},
//                                                            {title : 'Line height 28px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '28px'}},
//                                                            {title : 'Line height 30px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '30px'}},
//                                                            {title : 'Line height 40px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '40px'}},
//                                                            {title : 'Line height 50px', selector : 'p,div,h1,h2,h3,h4,h5,h6', styles: {lineHeight: '50px'}}
//                                                    ],
//                                                    templates: [
//                                                        {title: 'Test template 1', content: 'Test 1'},
//                                                        {title: 'Test template 2', content: 'Test 2'}
//                                                    ]		
//                                            });
        
                                        </script>
    </head>
    <body>
        <div class="container" style="padding:0px;">
            <%@ include file="WEB-INF/header/userNavigation.jsp"%>
            <div id="centercontainer" class="centercontainer">
                <div class="nav">
                    <ul>
                        <c:if test="${userForm.status != 'default'}">
                            <li><a href="javascript:void(0)" class="selected" id="formNameTab">${userForm.formName}</a></li>
                            </c:if>
                            <c:if test="${userForm.status == 'default'}">
                            <li><a href="./navigateTemplateView.do" onclick="launchWindow('#dialog');">Template 101</a>
                            </li>
                            <li><a href="./getUserComponentIfs.do" onclick="launchWindow('#dialog');">Template 102</a>
                            </li>
                            <li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a>
                            </li>
                            <li><a href="./createUserComponentForm104.do" onclick="launchWindow('#dialog');">Template 104</a>
                            </li>
                            <li><a href="./createUserComponentForm105.do" class="selected" onclick="launchWindow('#dialog');">Template 105</a>
                            </li>
                        </c:if>
                    </ul>
                </div>
                <%String getSVG = (String) request.getAttribute("imgType");

                    String green = "";
                    String yellow = "";
                    String red = "";
                    String greenSmall = "";
                    String yellowSmall = "";
                    String redSmall = "";
                    if ((getSVG != null) && (getSVG.equalsIgnoreCase("circle"))) {
                %>
                <% getSVG = "<b class='greenCircle'></b><b class='yellowCircle'></b><b class='redCircle'></b>";
                    green = "<b class='greenCircle'></b>";
                    yellow = "<b class='yellowCircle'></b>";
                    red = "<b class='redCircle'></b>";

                    greenSmall = "<b class='smallGreenCircle'></b>";
                    yellowSmall = "<b class='smallYellowCircle'></b>";
                    redSmall = "<b class='smallRedCircle'></b>";
                } else {%>
                <% getSVG = "<b class='green'></b><b class='yellow'></b><b class='red'></b>";
                        green = "<b class='green'></b>";
                        yellow = "<b class='yellow'></b>";
                        red = "<b class='red' ></b>";
                        greenSmall = "<b class='smallGreen' style='width:20px; height:20px;'></b>";
                        yellowSmall = "<b class='smallYellow' style='width:20px; height:20px;'></b>";
                        redSmall = "<b class='smallRed' style='width:20px; height:20px;'></b>";
                    }%>
                    
                <form id="updateAdminTemplate105" name="updateAdminTemplate105" action="./updateUserFormByFormId105.do" method="post">
                    <input type="hidden" id="imgType" value="${imgType}">
                    <input type = "hidden" name = "saveImage" id="saveImage105" value = "${userForm.formImage}"> 
                    

                    <div style="clear: both"></div>
                    <div class="innercontainer">
                        <div style="text-align:center; padding-top:10px; padding-bottom:10px;">
                            <span class="circlesquare">Form Name</span> : <input type="text" name="formName" id="formName" maxlength="20" value="${userForm.formName}"/>
                        </div>
                        <div id="templateSize" class="template-105">
                            <input type="hidden" name="formId" value="${userForm.userFormId}"/>
                            
                            <!--Header-->
                            <div id="pageHeading">
                                <p id="heading"  class="text_label4">
                                    
                                    <label id="lblText" class="text_label4">
                                        <span id="cmt" style="float:left;clear:left;">
                                            <input type="text" class="inputBorder"  name="headerText" id="headerText"  value="${userForm.headerText}"  onkeypress="return limitOfCharHeader(event, this.id, 25)" 
                                                   size="100" disabled="disabled" maxlength="30" style="width: 620px; height: 30px; font-weight: bold; text-align: center;text-transform: uppercase;" onblur="removeEditHeader('headerText', 25)"  />
                                        </span> 
                                    </label>            
                                    <span style="float: right;" class="handSymbol">
                                        <img src="../images/ieditover.PNG"  onclick="editTextHeader('headerText')" id="editButtonheaderText"/>
                                        <img src="../images/ieditOk.PNG"  onclick="removeEditHeader('headerText', 25)" id="okButtonHeader" style="display: none;"  />
                                    </span>
                                </p>
                                <div class="clear"></div>
                            </div>
                           
                                                   
                            <!--Logo-->                            
                            <div class="divLogoTab">
                                <div class="divLogoRow">
                                    <div class="yourlogo">
                                        <input type="hidden" id="logoTextId" name="logoText" />
                                        <input type="hidden" id="html2canvasImg" name="html2canvasImg" />
                                        <c:choose>
                                            <c:when test="${userForm.formImage!=null && userForm.formImage !=''}">
                                                <div id="imgLogo" align="center" class="imgLogo">
                                                    <img src="/ImageLibrary/${userForm.formImage}" 
                                                         style="margin:0; max-width:350px; max-height:250px;">
                                                </div>
                                                <div id="logoText" style="display: none;"></div>
                                                <textarea id="logoEditorTiny" style="display: none;">
                                                    <p><c:out value="${userForm.logoText}" escapeXml="false"/></p>
                                                </textarea>
                                                <div id="msgText" align="center" style="display: none;">
                                                    <p id="msg">(Your logo here)<br/>Dimension:[350 x 250]pixels</p>
                                                </div>
                                            </c:when>
                                            <c:when test="${userForm.logoText!=null && userForm.logoText !=''}">
                                                <div id="imgLogo" align="center" class="imgLogo" style="display: none;"></div>
                                                <textarea id="logoEditorTiny" rows="30" cols="40" style="display: none;">
                                                    <c:out value="${userForm.logoText}" escapeXml="false"/>
                                                </textarea>
                                                <div id="logoText" style="width:385px;height:280px;padding:3px;"><span style="font-size:100%;"><c:out value="${userForm.logoText}" escapeXml="false"/></span></div>
<!--                                                <iframe id="iframelogotext" style="display: none;">                                                
                                                    
                                                </iframe>-->
                                                <div id="msgText" align="center" style="display: none;">
                                                    <p id="msg">(Your logo here)<br/>Dimension:[350 x 250]pixels</p>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div id="imgLogo" align="center" class="imgLogo" style="display: none;"></div>
                                                <textarea id="logoEditorTiny" rows="25" cols="50" style="display: none;">
                                                    <c:out value="${userForm.logoText}" escapeXml="false"/>
                                                </textarea>
                                                <div id="logoText" style="display: none;"></div>
                                                <div id="msgText" align="center">
                                                    <p id="msg">(Your logo here)<br/>Dimension:[350 x 250]pixels</p>
                                                </div>
<!--                                                <iframe id="iframelogotext" style="display: none;">                                                
                                                    
                                                </iframe>-->
                                            </c:otherwise>
                                        </c:choose>
                                        
                                        <span style="float: right;" class="handSymbol">
                                            <img src="../images/ieditover.PNG"  onclick="showEditor('logoEditorTiny','${pageContext.request.contextPath}')" id="editButtonLogoText" />
                                            <img src="../images/ieditOk.PNG"  onclick="hideEditor('logoEditorTiny')" id="okButtonLogo" style="display: none;"  />
                                        </span> 
                                                
<!--                                        <div style="width:300px;margin:0 auto;clear:both;text-align:center;">
                                            <a href="javascript:void(0);" id="centerImageClick" onclick="centerImageSelectionPop('center105', 350, 250);
                                         return false;" style="height: 0;font-family: 'MyriadProRegular';font-size: 11pt;">Click to upload logo</a>&nbsp;&nbsp;<span class="imgDimension">[350 x 250]</span>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            
                            
                            <!--Name-->
                            <div class="divTable" style="width:390px; float: right;clear:none;">
                                <div class="divRow">
                                    <div id="customer" class="divCell">
                                        <div id="customerPDiv" style="border: 0px solid #000;position: relative;">
                                            <c:if test="${newUserFormComponents[0].status.statusId == 1}">
                                                <c:set var="componentDesc" value=""/>
                                                <c:if test="${newUserFormComponents[0].status.statusId == 1}">
                                                    <c:set var="componentDesc" value="${newUserFormComponents[0].component.componentName}"/>
                                                </c:if>
                                                <input type="hidden" name="componentId0" value="${newUserFormComponents[0].component.componentId}" />
                                                <input type="hidden" name="component0" id="component0"  value="" />
                                                <c:set var="cname" value=" "/>
                                                <p  id="customerP" class="inspec_formp moreWidth" style="width:366px; display: none;">
                                                    <span style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">
                                                        <ul class="customer-details">
                                                            <c:forEach var="cnameSplit" items="${fn:split(newUserFormComponents[0].component.componentName, '_')}" varStatus="stat">
                                                                <li>
                                                                    <input type="hidden" name="component_0${stat.count}" value="${cnameSplit}" />
                                                                    <input type="text" class="inputBorder"  name="component_0${stat.count}" id="component_0${stat.count}"  
                                                       value="${cnameSplit}"  disabled="disabled"  style="width:360px;display:block;padding:5px 0; " 
                                                       onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" size="35" maxlength="35"
                                                       onfocus="var val=this.value; this.value=''; this.value= val;" />
                                                                </li>
                                                            </c:forEach>
                                                        </ul>
                                                    </span>
                                                </p> 
                                                <span class="edit-icon-10">
                                                    <img src="../images/ieditover.PNG"  onclick="editTextCustomer('component')" id="editButton0"/>
                                                    <img src="../images/ieditOk.PNG"  onclick="removeEditCustomer('component', 250)" id="okButton0" style="display: none;"  />
                                                </span>      
                                            </c:if>				 
                                        </div>											
                                    </div>						
                                </div>
                            </div>

                            <div class="clear"></div>
                            
                            <!--Checked-Future-Immediate-->
                            <div class="selTable">
                                <div style="padding:15px 25px;">
	
                                    <%
                                        String getSVG1 = (String)request.getAttribute("imgType");
                                        String circle = "";
                                        String square = "";
                                        if (getSVG1.equalsIgnoreCase("circle")) {
                                            circle = "checked";
                                        } else {
                                            square = "checked";
                                        }
                                    %>
                                    <span class="circleSuare">
                                    <input type="radio" name="imgType" value="circle" <%= circle%> /> Circle
                                    </span>
                                    <span class="circleSuare">
                                        <input type="radio" name="imgType" value="square"  <%= square%> /> Square
                                    </span>
                                </div>
                                    
                                <div class="selRow">
                                    <div class="selCol selCol3" style="margin-right:25px;">
                                        <div class="selCell">														
                                            <span><b class="green"></b></span>
                                            <span class="floatLeft">
                                                <span id="divTxt_lights"  >
                                                    <span class="inspectionTxt4 leftAlignTxt width4">
                                                        <input type="hidden" name="componentId1" value="${newUserFormComponents[1].component.componentId}" />
                                                        <input type="text"    class="inputBorder"  name="component1" id="component1"  value="${newUserFormComponents[1].component.componentName}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
                                                               size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component1',20)"    />
                                                    </span>
                                                </span>    
                                                <span style="float: right;" class="handSymbol">
                                                    <img src="../images/ieditover.PNG"  onclick="editText('component1')" id="editButton1"/>
                                                    <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component1',20)"   id="okButton1" style="display: none;"  />
                                                </span>        
                                            </span>
                                        </div>
                                    </div>

                                    <div class="selCol selCol3" style="margin-right:40px;">
                                        <div class="selCell">								
                                            <span><b class="yellow"></b></span>
                                            <span class="floatLeft">
                                                <span id="divTxt_lights"  >
                                                    <span class="inspectionTxt4 leftAlignTxt width4">
                                                        <input type="hidden" name="componentId2" value="${newUserFormComponents[2].component.componentId}" />
                                                        <input type="text"    class="inputBorder"  name="component2" id="component2"  value="${newUserFormComponents[2].component.componentName}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
                                                               size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component2',20)"    />
                                                    </span>
                                                </span>    
                                                <span style="float: right;" class="handSymbol">
                                                    <img src="../images/ieditover.PNG"  onclick="editText('component2')" id="editButton2"/>
                                                    <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component2',20)"   id="okButton2" style="display: none;"  />
                                                </span>        
                                            </span>
                                        </div>
                                    </div>
                                    <div class="selCol selCol3" style="margin-right:0px;">
                                        <div class="selCell">
                                            <span><b class="red"></b></span>
                                            <span class="floatLeft">
                                                <span id="divTxt_lights"  >
                                                    <span class="inspectionTxt4 leftAlignTxt width4">
                                                        <input type="hidden" name="componentId3" value="${newUserFormComponents[3].component.componentId}" />
                                                        <input type="text"    class="inputBorder"  name="component3" id="component3"  value="${newUserFormComponents[3].component.componentName}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
                                                               size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component3',20)"    />
                                                    </span>
                                                </span>    
                                                <span style="float: right;" class="handSymbol">
                                                    <img src="../images/ieditover.PNG"  onclick="editText('component3')" id="editButton3"/>
                                                    <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component3',20)"   id="okButton3" style="display: none;"  />
                                                </span>        
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clear"></div>

                                        <div class="divTable1 paddingLeft" style="padding-left: 0;">
                                            <div  class="inspectionleftwrap">
                                                <div class="inspection_bg">
                                                   <!--Car-->
                                                   <div class="inspectionTable">

                                                       <div class="clear row1 row1Title greyBg">
                                                           <span align="center" class="th">
                                                               <input type="hidden" name="componentId4" value="${newUserFormComponents[4].component.componentId}" />
                                                               <input type="text" class="inputBorder componentHeader"  name="component4" id="component4"  
                                                                      value="${newUserFormComponents[4].component.componentName}"  onkeypress="return limitOfCharForMandatory(event,this.id, 40)" 
                                                                      maxlength="30" disabled="disabled" size="40" onblur="removeEditMandatory('component4', 40)"/>
                                                           </span>
                                                           <span class="EditBtnNew handSymbol" style="float: right;">
                                                               <img src="../images/ieditover.PNG"  onclick="editText('component4')" id="editButton4"/>
                                                               <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component4',40)" id="okButton4" style="display: none;"  />
                                                           </span>
                                                       </div>

                                                       <div class="clear row1" style="text-align:center;padding:3px;">
                                                           <span class="smallheading" style="float:left; width:150px;display:block; text-align:center; margin-left:105px;">
                                                               <input type="hidden" name="componentId5" value="${newUserFormComponents[5].component.componentId}" />
                                                               <input type="text" class="inputBorder"  name="component5" id="component5"  
                                                                      value="${newUserFormComponents[5].component.componentName}"  onkeypress="return limitOfCharForMandatory(event,this.id, 40)" 
                                                                      disabled="disabled" size="40" onblur="removeEditMandatory('component5', 40)" style="width: 200px;"/>
                                                           </span>
                                                           <span class="EditBtnNew handSymbol" style="float: right; right: 22px; top: 0;">
                                                               <img src="../images/ieditover.PNG"  onclick="editText('component5')" id="editButton5"/>
                                                               <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component5',40)" id="okButton5" style="display: none;"  />
                                                           </span>
                                                       </div>               
                                                       <input type="hidden" name="componentId70" value="${newUserFormComponents[70].component.componentId}" />               
                                                       <div class="clear row1" id="img">
                                                           <div  id="imgLeft" align="center" class="imgLeft" style="height:210px;margin-top:12px;">
                                                               <span class="alignCenter" style="display:block; height:210px;margin-top:12px;">
                                                                   <img src="/ImageLibrary/${newUserFormComponents[70].component.componentName}" alt=" " style="max-height: 192px;max-width:244px;" />
                                                               </span>
                                                           </div>
                                                           <div id="text" style="width:375px; text-align:center; top:0px;left:0px;">
                                                               <a href="javascript:void(0);" id="leftImageClick" onclick="centerImageSelectionPop('left',244,192); return false;">Change Image</a>[245 x 190]
                                                           </div>
                                                       </div>

                                                       <c:forEach var="i" begin="6" end="9" step="1" varStatus ="status">
                                                           <div class="clear row1">
                                                               <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                                   <span class="inspectionTxt4 leftAlignTxt">
                                                                       <input type="hidden" name="componentId${i}" value="${newUserFormComponents[i].component.componentId}" />
                                                                       <input type="text" class="inputBorder"  name="component${i}" id="component${i}"  
                                                                              value="${newUserFormComponents[i].component.componentName}"  onkeypress="return limitnofotext(event,this.id,35)" 
                                                                              size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component${i}', 35)"/>
                                                                   </span>
                                                                   <span class="EditBtnNew handSymbol">
                                                                       <img src="../images/ieditover.PNG"  onclick="editText('component${i}')" id="editButton${i}"/>
                                                                       <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component${i}',40)" id="okButton${i}" style="display: none;"  />
                                                                   </span>
                                                               </c:if>
                                                               <span class="floatRightTxt">
                                                                   <b class="greenCircle" >&nbsp;</b>
                                                                   <b class="yellowCircle" >&nbsp;</b>
                                                                   <b class="redCircle" >&nbsp;</b>
                                                               </span>
                                                           </div>
                                                       </c:forEach> 
                                                   </div> 
                                                   
                                                    <!--Underhood-->        
                                        <div class="inspectionTable">
                                            <div class="clear row1 row1Title greyBg">
                                                <c:if test="${newUserFormComponents[10].status.statusId == 1}">
                                                    <span align="center" class="th">
                                                        <input type="hidden" name="componentId10" value="${newUserFormComponents[10].component.componentId}" />
                                                        <input type="text" class="inputBorder"  name="component10" id="component10"  value="${newUserFormComponents[10].component.componentName}"  
                                                               onkeypress="return limitOfCharForMandatory(event,this.id,35)" size="29" disabled="disabled" maxlength="35" 
                                                               onblur="removeEditMandatory('component10', 35)" />
                                                    </span>
                                                    <span class="EditBtnNew handSymbol" style="float: right;">
                                                        <img src="../images/ieditover.PNG"  onclick="editText('component10')" id="editButton10"/>
                                                        <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component10',40)" id="okButton10" style="display: none;"  />
                                                    </span>
                                                </c:if>
                                            </div>

                                            <c:forEach var="i" begin="11" end="17" step="1" varStatus ="status">
                                                <div class="clear row1">
                                                    <c:if test="${newUserFormComponents[10].status.statusId == 1}">
                                                        <span class="inspectionTxt4 leftAlignTxt">
                                                            <input type="hidden" name="componentId${i}" value="${newUserFormComponents[i].component.componentId}" />
                                                            <input type="text" class="inputBorder"  name="component${i}" id="component${i}"  
                                                                   value="${newUserFormComponents[i].component.componentName}"  onkeypress="return limitnofotext(event,this.id,35)" 
                                                                   size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component${i}', 35)" style="width: 255px !important;"/>
                                                        </span>
                                                        <span class="EditBtnNew handSymbol">
                                                            <img src="../images/ieditover.PNG"  onclick="editText('component${i}')" id="editButton${i}"/>
                                                            <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component${i}',40)" id="okButton${i}" style="display: none;"  />
                                                        </span>
                                                    </c:if>
                                                    <span class="floatRightTxt">
                                                        <b class="greenCircle" >&nbsp;</b>
                                                        <b class="yellowCircle" >&nbsp;</b>
                                                        <b class="redCircle" >&nbsp;</b>
                                                    </span>
                                                </div>

                                            </c:forEach> 
                                        </div>           
                                                                       
                                        <!--Under Vehicle-->
                                        <div class="inspectionTable">
                                            <div class="clear row1 row1Title greyBg">
                                                <c:if test="${newUserFormComponents[18].status.statusId == 1}">
                                                    <span align="center" class="th">
                                                        <input type="hidden" name="componentId18" value="${newUserFormComponents[18].component.componentId}" />
                                                        <input type="text" class="inputBorder"  name="component18" id="component18"  value="${newUserFormComponents[18].component.componentName}"  
                                                               onkeypress="return limitOfCharForMandatory(event,this.id,35)" size="29" disabled="disabled" maxlength="35" 
                                                               onblur="removeEditMandatory('component18', 35)" />
                                                    </span>
                                                    <span class="EditBtnNew handSymbol" style="float: right;">
                                                        <img src="../images/ieditover.PNG"  onclick="editText('component18')" id="editButton18"/>
                                                        <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component18',40)" id="okButton18" style="display: none;"  />
                                                    </span>
                                                </c:if>
                                            </div>

                                            <c:forEach var="i" begin="19" end="23" step="1" varStatus ="status">
                                                <div class="clear row1">
                                                    <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                           <input type="hidden" name="componentId${i}" value="${newUserFormComponents[i].component.componentId}" />
                                                            <c:choose>
                                                                <c:when test="${i == 19}">
                                                                    <span class="inspectionTxtNew" style="width: 263px;">
                                                                    <textarea rows="2" cols="15" class="inputBorder" id="component19" name="component19" onkeypress="return limitnofotext(event,this.id,60)" 
								      disabled="disabled" style="height:36px; margin-bottom: 6px;width:255px;" maxlength="60">${newUserFormComponents[19].component.componentName}</textarea>
                                                                  </span>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <span class="inspectionTxt4 leftAlignTxt">
                                                                    <input type="text" class="inputBorder"  name="component${i}" id="component${i}"  
                                                                   value="${newUserFormComponents[i].component.componentName}"  onkeypress="return limitnofotext(event,this.id,50)" 
                                                                   size="50" disabled="disabled" maxlength="50" onblur="removeEdit('component${i}', 50)"/>
                                                                    </span>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        
                                                        <span class="EditBtnNew handSymbol">
                                                            <img src="../images/ieditover.PNG"  onclick="editText('component${i}')" id="editButton${i}"/>
                                                            <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component${i}',40)" id="okButton${i}" style="display: none;"  />
                                                        </span>
                                                    </c:if>
                                                    <span class="floatRightTxt">
                                                        <b class="greenCircle" >&nbsp;</b>
                                                        <b class="yellowCircle" >&nbsp;</b>
                                                        <b class="redCircle" >&nbsp;</b>
                                                    </span>
                                                </div>

                                            </c:forEach> 
                                        </div>                               
                                        <div class="clear"></div>                               
                                        </div>
                                </div>                               
                                
                                                               
                                                               
                                                               
                                <div  class="inspectionrightwrap">
                                    <div class="inspection_bg">

                                    <!--Tires-->
                                        <div class="inspectionTable" id="tiresBlock" style="border-bottom:1px solid #000;">
                                            <div class="clear row1Title greyBg rightHeadings">
                                                <c:if test="${newUserFormComponents[24].status.statusId == 1}">
                                                    <span align="center" class="th">
                                                        <input type="hidden" name="componentId24" value="${newUserFormComponents[24].component.componentId}" />
                                                        <input type="text" class="inputBorder componentHeader"  name="component24" id="component24"  value="${newUserFormComponents[24].component.componentName}"  onkeypress="return limitOfCharForMandatory(event, this.id, 40)" 
                                                               size="40" disabled="disabled"  maxlength="40" />
                                                    </span>
                                                    <span class="EditBtnNew handSymbol">
                                                        <img src="../images/ieditover.PNG"  onclick="editMultipleText(24, 53)" id="editButton24"/>
                                                        <img src="../images/ieditOk.PNG"  onclick="removeMultipleEdit(24, 53)" id="okButton24" style="display: none;"  />
                                                    </span>
                                                </c:if>
                                            </div>

                                            <div class="clear row1" style="border-bottom:0px;">
                                                <div style="padding:0px; border:0px;height:100px" >
                                                    <div class="bordernone interior_inspec">
                                                        <div class="alignCenter clear paddingBottom" style="width:360px;">
                                                            <c:if test="${newUserFormComponents[25].status.statusId == 1}">
                                                                <span class="inspectionTxtNew" style="top:0px; width: 380px;">
                                                                <h2 class="noInspec">
                                                                    <input type="hidden" name="componentId25" value="${newUserFormComponents[25].component.componentId}" />
                                                                    <input type="text" class="inputBorder" style="text-align: center; font-family: 'MyriadProBold';"  name="component25" id="component25"  value="${newUserFormComponents[25].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 50)" 
                                                                           size="40" disabled="disabled" maxlength="50"/>
                                                                </h2>
                                                            </span>
                                                            </c:if>
                                                        </div>


                                                        <div class="clear paddingBottom" style="width:385px; height:28px; padding-bottom: 2px;">
                                                            <c:if test="${newUserFormComponents[26].status.statusId == 1}">
                                                                <span style="width:139px; float:left;"><%=green%>

                                                                    <span class="fontF4 ">
                                                                        <input type="hidden" name="componentId26" value="${newUserFormComponents[26].component.componentId}" /> 
                                                                        <input type="text" class="inputBorder"  name="component26" id="component26"  value='${newUserFormComponents[26].component.componentName}'  onkeypress="return limitnofotext(event, this.id, 17)" 
                                                                               size="15" disabled="disabled"  maxlength="17" style="width:107px; font-family: 'MyriadProBold'; font-size: 10pt !important;"/>
                                                                    </span>

                                                                </span>
                                                            </c:if>
                                                            <c:if test="${newUserFormComponents[27].status.statusId == 1}">
                                                                <span style="width:124px; float:left;"><%=yellow%>
                                                                    <span class="fontF4 ">
                                                                        <input type="hidden" name="componentId27" value="${newUserFormComponents[27].component.componentId}" /> 
                                                                        <input type="text" class="inputBorder"  name="component27" id="component27"  value='${newUserFormComponents[27].component.componentName}'  onkeypress="return limitnofotext(event, this.id, 16)" 
                                                                               size="15" disabled="disabled"  maxlength="16" style="width:90px; font-size: 10pt !important;"/>
                                                                    </span>
                                                                </span>
                                                            </c:if>
                                                            <c:if test="${newUserFormComponents[28].status.statusId == 1}">
                                                                <span style="width:119px; float:left;"><%=red%>
                                                                    <span class="fontF4 ">
                                                                        <input type="hidden" name="componentId28" value="${newUserFormComponents[28].component.componentId}" /> 
                                                                        <input type="text" class="inputBorder"  name="component28" id="component28"  value='${newUserFormComponents[28].component.componentName}'  onkeypress="return limitnofotext(event, this.id, 16)" 
                                                                               size="15" disabled="disabled"  maxlength="16" style="width:88px;font-family: 'MyriadProBold';font-size: 11pt;font-weight: normal;font-style: oblique; font-size: 10pt !important;"/>
                                                                    </span>
                                                                </span>
                                                            </c:if>

                                                        </div>



                                                        <div class="clear">
                                                            <div class="alignCenter" style="width:375px;">
                                                                <div class="bordernone interior_inspec interior_inspecLeft" style="width: 155px;">
                                                                    <span class="txt_bold" style="float:left;margin-right:4px;width:18px;">
                                                                        <c:choose>
                                                                            <c:when test="${newUserFormComponents[29].status.statusId == 1 && newUserFormComponents[30].status.statusId == 1}">
                                                                                <strong>

                                                                                    <input type="hidden" name="componentId29" value="${newUserFormComponents[29].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component29" id="component29"  value="${newUserFormComponents[29].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" style="width: 19px;" />

                                                                                </strong>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                &nbsp;
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </span>
                                                                    <c:if test="${newUserFormComponents[30].status.statusId == 1 && newUserFormComponents[29].status.statusId == 1}">
                                                                        <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                        <span class="txt_bold" style="float:left;">
                                                                            <strong>

                                                                                <input type="hidden" name="componentId30" value="${newUserFormComponents[30].component.componentId}" /> 
                                                                                <input type="text" class="inputBorder tireComponentHeader"  name="component30" id="component30"  value="${newUserFormComponents[30].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 10)" 
                                                                                                   size="10" disabled="disabled"  maxlength="10" style="width:58px;"/>

                                                                            </strong>
                                                                        </span>
                                                                    </c:if>

                                                                </div>
                                                                <div class="bordernone interior_inspec interior_inspecRight" style="width: 155px;">
                                                                    <span class="txt_bold"  style="float:left;margin-right:4px;width:18px;">
                                                                        <c:choose>
                                                                            <c:when test="${newUserFormComponents[31].status.statusId == 1 && newUserFormComponents[32].status.statusId == 1}">
                                                                                <strong>

                                                                                    <input type="hidden" name="componentId31" value="${newUserFormComponents[31].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component31" id="component31"  value="${newUserFormComponents[31].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" style="width: 20px;"/>

                                                                                </strong>   
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                &nbsp;
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </span>
                                                                    <c:if test="${newUserFormComponents[32].status.statusId == 1 && newUserFormComponents[31].status.statusId == 1}">
                                                                        <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                        <span class="txt_bold"  style="float:left;">
                                                                            <strong>

                                                                                <input type="hidden" name="componentId32" value="${newUserFormComponents[32].component.componentId}" /> 
                                                                                <input type="text" class="inputBorder tireComponentHeader"  name="component32" id="component32"  value="${newUserFormComponents[32].component.componentName}"  onkeypress="return limitnofotext(event, this.id,10)" 
                                                                                           size="10" disabled="disabled"  maxlength="10" style="width:58px;"/>

                                                                            </strong>
                                                                        </span>
                                                                    </c:if>
                                                                </div>

                                                                <div class="bordernone interior_inspec interior_inspecLeft" style="width: 155px;">

                                                                    <span class="txt_bold" style="float:left;margin-right:4px;width:18px;">
                                                                        <c:choose>
                                                                            <c:when test="${newUserFormComponents[33].status.statusId == 1 && newUserFormComponents[34].status.statusId == 1}">
                                                                                <strong>

                                                                                    <input type="hidden" name="componentId33" value="${newUserFormComponents[33].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component33" id="component33"  value="${newUserFormComponents[33].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" />

                                                                                </strong>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                &nbsp;
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </span>
                                                                    <c:if test="${newUserFormComponents[34].status.statusId == 1 && newUserFormComponents[33].status.statusId == 1}">
                                                                        <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                        <span class="txt_bold"  style="float:left;">
                                                                            <strong>

                                                                                <input type="hidden" name="componentId34" value="${newUserFormComponents[34].component.componentId}" /> 
                                                                                <input type="text" class="inputBorder tireComponentHeader"  name="component34" id="component34"  value="${newUserFormComponents[34].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 10)" 
                                                                                           size="10" disabled="disabled"  maxlength="10" style="width:58px;"/>

                                                                            </strong>
                                                                        </span>
                                                                    </c:if>

                                                                </div>
                                                                <div class="bordernone interior_inspec interior_inspecRight" style="width: 155px;">
                                                                    <span class="txt_bold"  style="float:left;margin-right:4px;width:18px;">
                                                                        <c:choose>
                                                                            <c:when test="${newUserFormComponents[35].status.statusId == 1 && newUserFormComponents[36].status.statusId == 1}">
                                                                                <strong>

                                                                                    <input type="hidden" name="componentId35" value="${newUserFormComponents[35].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component35" id="component35"  value="${newUserFormComponents[35].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" style="width: 20px;"/>

                                                                                </strong>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                &nbsp;
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </span>
                                                                    <c:if test="${newUserFormComponents[36].status.statusId == 1 && newUserFormComponents[35].status.statusId == 1}">
                                                                        <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                        <span class="txt_bold"  style="float:left;">
                                                                            <strong>

                                                                                <input type="hidden" name="componentId36" value="${newUserFormComponents[36].component.componentId}" /> 
                                                                                <input type="text" class="inputBorder tireComponentHeader"  name="component36" id="component36"  value="${newUserFormComponents[36].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 10)" 
                                                                                           size="10" disabled="disabled"  maxlength="10" style="width:58px;"/>

                                                                            </strong>
                                                                        </span>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div></div>
                                            </div>

                                            <div style="min-height:235px; border-top: 2px solid #000;">
                                                <input type="hidden" name="componentId71" value="${newUserFormComponents[71].component.componentId}" />
                                                <div style="float:left; width:200px;">
                                                    <div style="padding:0px;width:100px;float:left;">
                                                        <div id="imgBottomRight" align="center" class="imgBottomRight" style="float: right;/*margin-top: -27%;*/width:99px;height:200px;">
                                                            <img id="image" src="/ImageLibrary/${newUserFormComponents[71].component.componentName}" alt=" " 
                                                                 style="max-width: 100px;max-height: 170px;"/>
                                                        </div>  
                                                        <a href="javascript:void(0);" id="bottomRightImageClick" onclick="centerImageSelectionPop('bottemRight', 100, 170);
                                                                                    return false;" style="display:inline;margin-top:10px; margin-left: 3px;">Change Image</a><br/>
                                                                                                            <span style="margin-left: 3px">[100 x 170]</span>
                                                    </div>

                                                    <div  class="bordernone interior_inspec padding_reset lessWidth" style="padding:0px;float:right; padding-top:15px;width:100px !important;">
                                                        <div style="height:30px; margin-bottom:10px;">
                                                            <c:if test="${newUserFormComponents[37].status.statusId == 1}">
                                                                <h2 class="titleFont" style="text-align:center;">

                                                                    <input type="hidden" name="componentId37" value="${newUserFormComponents[37].component.componentId}" />
                                                                    <textarea rows="3" cols="12" class="inputBorder tireComponentHeader" name="component37" id="component37" onkeypress="return limitnofotext(event, this.id, 39)"
                                                                              disabled="disabled" style="width:94px;height: 55px;font-style: italic;"  maxlength="25">${newUserFormComponents[37].component.componentName}</textarea>   


                                                                </h2></span>
                                                            </c:if>
                                                        </div>
                                                        <c:if test="${newUserFormComponents[38].status.statusId == 1}">
                                                            <div class="clear" style="height:20px;margin-bottom:10px;">
                                                                <span class="txt_bold txtLeft" style="width:18px;" > <strong>

                                                                        <input type="hidden" name="componentId38" value="${newUserFormComponents[38].component.componentId}" />
                                                                        <input type="text" class="inputBorder tireComponentHeader"  name="component38" id="component38"  value="${newUserFormComponents[38].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                               size="2" disabled="disabled" maxlength="2" style="width: 19px;"/>

                                                                    </strong></span>
                                                                <span width="284" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                            </div>
                                                        </c:if>


                                                        <c:if test="${newUserFormComponents[39].status.statusId == 1}">
                                                            <div class="clear" style="height:20px;margin-bottom:10px;">
                                                                <span class="txt_bold txtLeft"  style="width:18px;"><strong>

                                                                        <input type="hidden" name="componentId39" value="${newUserFormComponents[39].component.componentId}" />
                                                                        <input type="text" class="inputBorder tireComponentHeader"  name="component39" id="component39"  value="${newUserFormComponents[39].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                               size="2" disabled="disabled" maxlength="2"  style=""/>

                                                                </strong></span>
                                                                <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                            </div>
                                                        </c:if>

                                                        <c:if test="${newUserFormComponents[40].status.statusId == 1}">
                                                            <div class="clear" style="height:20px;margin-bottom:10px;">
                                                                <span class="txt_bold txtLeft"  style="width:18px;"><strong>

                                                                        <input type="hidden" name="componentId40" value="${newUserFormComponents[40].component.componentId}" />
                                                                        <input type="text" class="inputBorder tireComponentHeader"  name="component40" id="component40"  value="${newUserFormComponents[40].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                               size="2" disabled="disabled" maxlength="2" />

                                                                </strong></span>
                                                                <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                            </div>
                                                        </c:if>
                                                        <c:if test="${newUserFormComponents[41].status.statusId == 1}">
                                                            <div class="clear" style="height:20px;margin-bottom:10px;">
                                                                <span class="txt_bold txtLeft"  style="width:18px;"><strong>

                                                                        <input type="hidden" name="componentId41" value="${newUserFormComponents[41].component.componentId}" />
                                                                        <input type="text" class="inputBorder tireComponentHeader"  name="component41" id="component41"  value="${newUserFormComponents[41].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                               size="2" disabled="disabled" maxlength="2" />

                                                                </strong></span>
                                                                <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>

                                                            </div>
                                                        </c:if>
                                                    </div>
                                                </div>
                                                <div class="interior_inspec" style="min-height:230px;border-right: 2px solid #000 !important;border-left: 2px solid #000 !important;padding:0px;width:96px; padding:0 3px; float:left;">
                                                    <div cellspacing="0" class="bordernone padding_reset" style="">
                                                        <div style="text-align:center;margin-top:15px;">
                                                            <c:if test="${newUserFormComponents[42].status.statusId == 1}">
                                                                <h2 class="titleFont" style="text-align:center;">

                                                                    <input type="hidden" name="componentId42" value="${newUserFormComponents[42].component.componentId}" /> 
                                                                    <textarea rows="1" cols="10" class="inputBorder tireComponentHeader" style="width:90px;font-style: italic;" name="component42" id="component42"  onkeypress="return limitnofotext(event, this.id, 20)"
                                                                              disabled="disabled"  maxlength="15">${newUserFormComponents[42].component.componentName}</textarea>   
                                                                </h2>
                                                            </c:if>
                                                        </div>
                                                        <input type="hidden" name="componentId72" value="${newUserFormComponents[72].component.componentId}" />
                                                        <div>
                                                            <span style="float:left; width:16px;margin-right:2px; position:relative; top:7px;left:7px;">
                                                                <b class="smallRed" ></b>
                                                            </span>
                                                            <input type="hidden" name="componentId72" value="${newUserFormComponents[72].component.componentId}" />
                                                            <div  style="display:block; float:left;width:40px;margin-left: 27px;">
                                                                <div id="imgBottomMid" align="center" class="imgBottomMid" style="width:40px; height:36px;">
                                                                    <img src="/ImageLibrary/${newUserFormComponents[72].component.componentName}" alt=" " 
                                                                         style="max-width: 40px; max-height: 36px;" id="image"/>
                                                                </div>
                                                                <a href="javascript:void(0);" id="bottomMidImageClick" onclick="centerImageSelectionPop('bottomMid',40,36); return false;" 
                                                                   style="display:inline;margin-top:4px;white-space:nowrap;">Change Image</a>
                                                                         <br/>
                                                                         <div style="font-size:10px;padding-top: 4px;">[40 x 36]</div>
                                                            </div>     
                                                        </div>

                                                        <div class="bordernone interior_inspec">
                                                            <div class="beforeAfter">
                                                                <c:if test="${newUserFormComponents[43].status.statusId == 1}">
                                                                    <span class="beforeTxt"><c:out value="${newUserFormComponents[43].component.componentName}"/></span>
                                                                </c:if>
                                                                &nbsp&nbsp;
                                                                <c:if test="${newUserFormComponents[44].status.statusId == 1}">
                                                                    <span class="afterTxt"><c:out value="${newUserFormComponents[44].component.componentName}"/></span>
                                                                </c:if>
                                                            </div>
                                                            <div style="width:100%;"  class="clear">
                                                                <div  style="float:left;width:15px; position: relative; height: 50px;">
                                                                    <span style="position: absolute;top:0px; left: 2px;"><strong>
                                                                            <c:if test="${newUserFormComponents[45].status.statusId == 1}">
                                                                                <span class="txt_bold">

                                                                                    <input type="hidden" name="componentId45" value="${newUserFormComponents[45].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component45" id="component45"  value="${newUserFormComponents[45].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" />

                                                                                </span>
                                                                            </c:if>
                                                                        </strong></span>
                                                                    <span  style="position: absolute;top:26px; left: 2px;"><strong>
                                                                            <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                                                                <span class="txt_bold">

                                                                                    <input type="hidden" name="componentId46" value="${newUserFormComponents[46].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component46" id="component46"  value="${newUserFormComponents[46].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" />

                                                                                </span>
                                                                            </c:if>
                                                                        </strong></span>
                                                                </div>
                                                                <div style="width:30px; float:left;margin-right:3px;">
                                                                    <span class="white_box">&nbsp;</span><br />
                                                                    <span class="white_box">&nbsp;</span></div>&nbsp;
                                                                    <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                                                    <span class="white_box_rectangle">&nbsp;</span>
                                                                </c:if>
                                                            </div>
                                                            <div style="width:100%;"  class="clear">
                                                                <div  style="float:left;width:15px; position: relative; height: 50px;">
                                                                    <span style="position: absolute;top:3px; left: 2px;"><strong>
                                                                            <c:if test="${newUserFormComponents[47].status.statusId == 1}">
                                                                                <span class="txt_bold">

                                                                                    <input type="hidden" name="componentId47" value="${newUserFormComponents[47].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component47" id="component47"  value="${newUserFormComponents[47].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" />

                                                                                </span>
                                                                            </c:if>
                                                                        </strong></span> 
                                                                    <span style="position: absolute;top:30px; left: 2px;"><strong>
                                                                            <c:if test="${newUserFormComponents[48].status.statusId == 1}">
                                                                                <span class="txt_bold">

                                                                                    <input type="hidden" name="componentId48" value="${newUserFormComponents[48].component.componentId}" /> 
                                                                                    <input type="text" class="inputBorder tireComponentHeader"  name="component48" id="component48"  value="${newUserFormComponents[48].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                                           size="2" disabled="disabled"  maxlength="2" />

                                                                                </span>
                                                                            </c:if> 
                                                                        </strong>
                                                                    </span>
                                                                </div>
                                                                <div style="width:30px; float:left;margin-right:3px;"><span class="white_box">&nbsp;</span><br />
                                                                    <span class="white_box">&nbsp;</span></div>&nbsp;
                                                                    <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                                                    <span class="white_box_rectangle">&nbsp;</span>
                                                                </c:if>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="interior_inspec" style="width:74px;float:left; padding:0 3px;" class="bordernone padding_reset">
                                                    <div style="margin-top:15px;">
                                                        <c:if test="${newUserFormComponents[49].status.statusId == 1}">
                                                            <span><h2 class="titleFont" style="text-align:center;">

                                                                    <input type="hidden" name="componentId49" value="${newUserFormComponents[49].component.componentId}" /> 
                                                                    <textarea rows="3" cols="10" class="inputBorder tireComponentHeader" style="margin-left: 3px;width:80px !important;height: 69px;font-style: italic;" name="component49" id="component49"  onkeypress="return limitnofotext(event, this.id, 40)"
                                                                              disabled="disabled" maxlength="40" >${newUserFormComponents[49].component.componentName}</textarea>   

                                                            </h2></span>
                                                            </c:if>
                                                    </div>
                                                    <div  class="bordernone interior_inspec">
                                                        <c:forEach var="i" begin="50" end="53" step="1" varStatus ="status">
                                                            <div class="clear" style="height:20px;margin-bottom:10px;">
                                                                <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                                    <span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                                                    <span class="txtFont" style="vertical-align:-3px;">

                                                                        <input type="hidden" name="componentId${i}" value="${newUserFormComponents[i].component.componentId}" /> 
                                                                        <input type="text" class="inputBorder componentHeader"  name="component${i}" id="component${i}"  value="${newUserFormComponents[i].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 20)" 
                                                                               size="12" disabled="disabled"  maxlength="10"/>
                                                                    </span>
                                                                </c:if>
                                                            </div>
                                                        </c:forEach> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                
                                        <!--   Brakes  -->
                                        <div class="inspectionTable inspectionTableBg" style="border-bottom:1px solid #000; overflow: hidden;">
                                            <div class="clear row1 row1Title greyBg rightHeadings">
                                                <c:if test="${newUserFormComponents[54].status.statusId == 1}">
                                                    <span class="txtFont">
                                                        <input type="hidden" name="componentId54" value="${newUserFormComponents[54].component.componentId}" /> 
                                                        <input type="text" class="inputBorder componentHeader"  name="component54" id="component54"  value="${newUserFormComponents[54].component.componentName}"  onkeypress="return limitOfCharForMandatory(event, this.id, 40)" 
                                                               size="40" disabled="disabled"  maxlength="30" style="font-family: 'ImpactRegular';font-style: normal; font-size: 11pt !important; width: 120px;"/>
                                                    </span> 
                                                    <span class="handSymbol">
                                                        <img src="../images/ieditover.PNG"  onclick="editMultipleText(54, 64)" id="editButton54"/>
                                                        <img src="../images/ieditOk.PNG"  onclick="removeMultipleEdit(54, 64)" id="okButton54" style="display: none;"  />
                                                    </span>   
                                                </c:if>           
                                            </div>
                                            <div class="clear alignCenter paddingBottom" style="width: 389px; margin-left: 0px;margin-top:2px; padding-bottom: 2px;">
                                                <c:if test="${newUserFormComponents[55].status.statusId == 1}">              
                                                    <span><h2 class="noInspec">
                                                            <input type="hidden" name="componentId55" value="${newUserFormComponents[55].component.componentId}" /> 
                                                            <input type="text" class="inputBorder componentHeader"  name="component55" id="component55"  value="${newUserFormComponents[55].component.componentName}"  onkeypress="return limitOfCharForMandatory(event, this.id, 40)" 
                                                               size="40" disabled="disabled"  maxlength="30" 
                                                               style="font-family: 'MyriadProBold';font-style: normal; font-size: 11pt !important; width: 380px; text-align: center;font-weight: normal;" />
                                                        </h2></span>
                                                </c:if>
                                            </div>
                                            
                                            <div class="clear row1" style="border-bottom:0px; width: 190px; float: left;margin-left: 20px;">
                                                <div style="padding:0px; border:0px;height:130px" >
                                                    <div class="bordernone interior_inspec">
                                                        <div class="clear paddingBottom" style="width:200px; height:30px;">
                                                            <c:if test="${newUserFormComponents[56].status.statusId == 1}">
                                                                <span class="clear" style="display:block;margin-bottom:5px;"><%= green%>
                                                                    <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;">
                                                                        
                                                                        <span class="txtFont">
                                                                            <input type="hidden" name="componentId56" value="${newUserFormComponents[56].component.componentId}" /> 
                                                                            <textarea rows="2" cols="15" class="inputBorder" id="component56" name="component56" onkeypress="return limitnofotext(event, this.id, 39)" 
                                                                                      disabled="disabled" style="height:30px;line-height: 16px; margin-bottom: 10px;width:150px;" maxlength="36">${newUserFormComponents[56].component.componentName}</textarea>
                                                                        </span>
                                                    
                                                                    </span>
                                                                </span>
                                                            </c:if>	
                                                            <c:if test="${newUserFormComponents[57].status.statusId == 1}">
                                                                <span class="clear" style="display:block;margin-bottom:5px;"><%= yellow%>
                                                                    <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"> 
                                                                        
                                                                        <span class="txtFont">
                                                                            <input type="hidden" name="componentId57" value="${newUserFormComponents[57].component.componentId}" /> 
                                                                            <textarea rows="2" cols="15" class="inputBorder" id="component57" name="component57" onkeypress="return limitnofotext(event, this.id, 39)" 
                                                                                      disabled="disabled" style="height:30px;line-height: 16px; margin-bottom: 10px;width:150px;" maxlength="36">${newUserFormComponents[57].component.componentName}</textarea>
                                                                        </span>
                                                                        
                                                                        
                                                                    </span>
                                                                </span>
                                                            </c:if>
                                                            <c:if test="${newUserFormComponents[58].status.statusId == 1}">	
                                                                <span class="clear" style="display:block;margin-bottom:5px;"><%= red%>
                                                                    <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;">
                                                                        
                                                                        <span class="txtFont">
                                                                            <input type="hidden" name="componentId58" value="${newUserFormComponents[58].component.componentId}" /> 
                                                                            <textarea rows="2" cols="15" class="inputBorder" id="component58" name="component58" onkeypress="return limitnofotext(event, this.id, 39)" 
                                                                                      disabled="disabled" style="height:38px !important; font-family: 'MyriadProRegular' !important; margin-top: -2px;line-height: 16px; width:150px !important;" maxlength="36">${newUserFormComponents[58].component.componentName}</textarea>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </c:if>

                                                        </div>
                                                        <div class="clear">

                                                        </div>
                                                    </div></div>
                                            </div>
                                            
                                            <div style="  width: 125px; float: right;">

                                                <div  class="bordernone interior_inspec padding_reset" style="padding:5px;">
                                                    <div style="height: 90px;">
                                                        <c:forEach var="i" begin="59" end="62" step="1" varStatus ="status">
                                                            <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                                <div class="clear">
                                                                        
                                                                    <span class="txt_bold txtLeft" style="width:18px;" > <strong>
                                                                        <input type="hidden" name="componentId${i}" value="${newUserFormComponents[i].component.componentId}" /> 
                                                                        <input type="text" class="inputBorder tireComponentHeader"  name="component${i}" id="component${i}"  value="${newUserFormComponents[i].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 2)" 
                                                                               size="2" disabled="disabled"  maxlength="2" />
                                                                    </strong></span>   
                                                                               
                                                                    <span width="284" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                </div>
                                                            </c:if>
                                                        </c:forEach> 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clear" style="width:300px;margin-top:35px;margin-left: 30px;">
                                                <c:if test="${newUserFormComponents[63].status.statusId == 1}">
                                                    <span class="fontF4" style="float:left;display:block; width:76px;margin-top:12px;">
                                                        <strong>
                                                            <input type="hidden" name="componentId63" value="${newUserFormComponents[63].component.componentId}" /> 
                                                            <input type="text" class="inputBorder tireComponentHeader"  name="component63" id="component63"  value="${newUserFormComponents[63].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 20)" 
                                                                   size="29" disabled="disabled" style="width:115px;font-style: oblique;"  maxlength="12"/>
                                                        </strong>
                                                    </span>
                                                </c:if>
                                                
                                                <input type="hidden" name="componentId73" value="${newUserFormComponents[73].component.componentId}" />
                                                <span style="padding:0px;width:150px;float:left;display:block;margin-right:5px;margin-left:5px; text-align: center;">
                                                    <div id="imgBottomLast" align="center" class="imgBottomLast" style="float: right;margin-top:10px;width: 150px; text-align: center;">
                                                        <img src="/ImageLibrary/${newUserFormComponents[73].component.componentName}" alt=" " 
                                                             style="margin-left:3px; margin-right:3px;max-width:150px; max-height:45px;" id="image"/>
                                                    </div>
                                                    <a href="javascript:void(0);" id="bottomLastImageClick" onclick="centerImageSelectionPop('bottomLast', 150, 45);
                                                                return false;" style="display:inline;margin-top:10px;position:relative; top:-4px;">Change Image</a>[150 x 45]
                                                </span>
                                                
                                                
                                                <c:if test="${newUserFormComponents[64].status.statusId == 1}">
                                                    <span class="fontF4" style="float:left;display:block; width:64px;margin-top:12px;">
                                                        <strong>
                                                            <input type="hidden" name="componentId64" value="${newUserFormComponents[64].component.componentId}" /> 
                                                            <input type="text" class="inputBorder tireComponentHeader"  name="component64" id="component64"  value="${newUserFormComponents[64].component.componentName}"  onkeypress="return limitnofotext(event, this.id, 20)" 
                                                                   size="29" disabled="disabled" style="width:115px;font-style: oblique;"  maxlength="12"/>
                                                        </strong>
                                                    </span>
                                                </c:if>				   
                                            </div>

                                        </div>

                                        <!-- Battery -->
                                        <div class="inspectionTable">
                                            <div class="clear row1 row1Title greyBg rightHeadings">
                                                <c:if test="${newUserFormComponents[65].status.statusId == 1}">
                                                    <span align="center" class="th">
                                                        <input type="hidden" name="componentId65" value="${newUserFormComponents[65].component.componentId}" />
                                                        <input type="text" class="inputBorder componentHeader"  name="component65" id="component65"  value="${newUserFormComponents[65].component.componentName}"  onkeypress="return limitOfCharForMandatory(event, this.id, 40)" 
                                                               size="35" disabled="disabled" maxlength="40" onblur="removeEditMandatory('component65', 40)" style="font-family: 'ImpactRegular';font-style: normal;font-weight: normal;" />
                                                    </span>
                                                    <span class="EditBtnNew handSymbol">
                                                        <img src="../images/ieditover.PNG"  onclick="editText('component65')" id="editButton65"/>
                                                        <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component65', 40)" id="okButton65" style="display: none;"  />
                                                    </span>
                                                </c:if>
                                            </div>
                                            <div class="clear row1" style="height: 120px;">
                                                <div style="width:245px; float:left;padding:5px 0;">
                                                    <c:if test="${newUserFormComponents[66].status.statusId == 1}">
                                                        <span class="textFont4">
                                                            <input type="hidden" name="componentId66" value="${newUserFormComponents[66].component.componentId}" />
                                                            <input type="text" class="inputBorder"  name="component66" id="component66"  value="${newUserFormComponents[66].component.componentName}"  onkeypress="return limitOfCharForMandatory(event, this.id, 35)" 
                                                               size="29" disabled="disabled" maxlength="35" onblur="removeEditMandatory('component66', 35)" style="font-family: 'helvetica';font-style: normal;font-weight: normal; width: 200px !important;"/>
                                                        </span>
                                                        <span class="EditBtnNew handSymbol" style="float: right;">
                                                            <img src="../images/ieditover.PNG"  onclick="editText('component66')" id="editButton66"/>
                                                            <img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component66', 35)" id="okButton66" style="display: none;"  />
                                                        </span>       
                                                    </c:if>
                                                    <div style="padding:10px 15px 0 40px;;">
                                                        <span>
                                                            <b class="greenCircle"></b>
                                                            <b class="yellowCircle"></b>
                                                            <b class="redCircle"></b>
                                                        </span>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="componentId74" value="${newUserFormComponents[74].component.componentId}" />
                                                <div id="img">
                                                    <div id="imgBottomLeft" align="center" class="imgBottomLeft" style="width:120px;height:90px; float:left;">
                                                        <img src="/ImageLibrary/${newUserFormComponents[74].component.componentName}" alt=" " style="max-width: 115px;max-height: 90px;" /> 
                                                    </div>
                                                    <div id="text"  style="top:100px; width: 120px; left: 250px;">
                                                        <a href="javascript:void(0);" id="bottomLeftImageClick" onclick="centerImageSelectionPop('bottemLeft', 115, 90);return false;" 
                                                           style="display:inline;margin-top:10px;">Change Image</a>[115 x 90]
                                                    </div>
                                                </div>
                                             </div>
                                        </div>

                                        <!-- Comments -->            
                                        <div class="inspectionTable" style="margin-bottom: 0px;">
                                            <div class="clear" style="font-family:'MyriadProRegular';font-size: 11pt;height: 1px;margin-left: 8px;padding-top: 5px;">
                                                <c:if test="${newUserFormComponents[67].status.statusId == 1}">
                                                    <span id="component67Span" class="comments" style="float: left;width: 335px;">${newUserFormComponents[67].component.componentName}</span> 
                                                    <span class="th" style="float: left">
                                                        <input type="hidden" name="componentId67" value="${newUserFormComponents[67].component.componentId}" />
                                                        <input type="text" class="inputBorder"  name="component67" id="component67"  value="${newUserFormComponents[67].component.componentName}"  onkeypress="return limitOfCharForMandatory(event,this.id,35)" 
                                                              size="35" disabled="disabled" maxlength="35" style="width:335px;display: none;" onblur="removeEditMandatory('component67', 35)"/>
                                                    </span>
                                                              <span class="EditBtnNew handSymbol" style="top:0;">
                                                        <img src="../images/ieditover.PNG"  onclick="editMultipleTextLast(67,69)" id="editButton67" style="float: right;"/>
                                                        <img src="../images/ieditOk.PNG"  onclick="removeMultipleEditLast(67,69)" id="okButton67" style="display: none;float: right;"  />
                                                    </span>            
                                                </c:if>
                                            </div>
                                            <div class="clear row1" style="height:1px;"></div>
                                            <div class="clear row1" style="height:26px;"></div>
                                            <div class="clear row1" style="height:26px;"></div>
                                        </div>

                                        <div class="bottomtext" style="width: 393px;overflow:hidden;">
                                            <div style="width:400px;margin-top:0px;">
                                               
                                                <div style="float:left;width:279px;overflow:hidden;">
                                                    <span id="cmt" style="float: left;">
                                                        <span  id="component68Span" class="comments">${newUserFormComponents[68].component.componentName}</span> 
                                                        <input type="hidden" name="componentId68" value="${newUserFormComponents[68].component.componentId}" /> 
                                                        <input type="text" class="inputBorder"  name="component68" id="component68"  value="${newUserFormComponents[68].component.componentName}"  
                                                        size="15" disabled="disabled" style="width: 90px;display: none;"    maxlength="15"/>
                                                    </span>
                                                    <span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>    
                                                </div> 
                                               
                                                <div style="float:right;width: 120px;">
                                                    <span id="component69Span" class="comments" style="float: left;">${newUserFormComponents[69].component.componentName}</span> 
                                                    <span style="float: left;">
                                                            <input type="hidden" name="componentId69" value="${newUserFormComponents[69].component.componentId}" /> 
                                                            <input type="text" class="inputBorder" name="component69" id="component69"  value="${newUserFormComponents[69].component.componentName}"   
                                                                    size="15" disabled="disabled" style="width:40px;display: none;" maxlength="10"/>
                                                    </span>
                                                    <span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine3" style="width: 125px"/></span>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>                               
                                                                       
                                                                   

                                                    
                                            
                                        </div>
                                        <div class="clear"></div>
                                        <div style="text-align:center;margin-top:10px;padding-bottom:10px;">
                                            <a href="javascript:void(0);" name="save" value="Save" id="update102"  onclick="updateTemplate105('./updateUserFormByFormId105.do');" />Save</a>
                                            <c:choose>
                                                <c:when test="${userForm.status=='active'}">
                                                    <a  href="./getUserFormByFormId105.do?formId=${userForm.userFormId}"  id="cancelBtn" onclick="launchWindow('#dialog');">Cancel</a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a  href="./createUserComponentForm105.do" id="cancelBtn" onclick="launchWindow('#dialog');">Cancel</a>
                                                </c:otherwise>

                                            </c:choose>

                                        </div>
                                        <div class="clear"></div>
                                        <div style="text-align:right;font-family:'MyriadProRegular'; font-size:6pt;padding:0 20px 10px 0;"></div>
                                        </div>
                                        </div>

                                        </form>
                                        </div>

                                        </div>
                                        <div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
                                        
                                        </body>
                                        </html>
