<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="header.jsp"%>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css"></link>
<script type="text/javascript" src="../js/jquery-latest.js"></script>
<div>
	<select id="editOption" onchange="changeOPtion(this.id)">
	<c:forEach var = "subOption" items="${subOptions }">
		<c:if test="${subOption.subComponentOptionsId == selectId}">
			<option value="${subOption.subComponentOptionsId }" selected="selected">${subOption.options.optionName }</option>
		</c:if>
		<c:if test="${subOption.subComponentOptionsId != selectId}">
			<option value="${subOption.subComponentOptionsId }">${subOption.options.optionName }</option>
		</c:if>
	</c:forEach>
	<c:if test="${fn:length(subOptions) == 0}" >
		<c:if test="${user.userRole == 'admin'}">
			<option value="Select">Select</option>
		</c:if>
	</c:if>
	<c:if test="${fn:length(subOptions) < 3}" >
		<c:if test="${user.userRole == 'admin'}">
			<option value="Create Option">Create Option</option>
		</c:if>
	</c:if>
	</select>
</div>
