<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<title>Edit Template 2</title>
<!-- Style Sheet -->
<link rel="stylesheet" type="text/css" href="../css/style-edit.css"></link>
<link type="text/css" rel="stylesheet" href="../css/jquery.miniColors.css" />
<link type="text/css" rel="stylesheet" href="../css/pagination.css" />
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<script type="text/javascript" src="../js/popup.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/jquery-editTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery-adminEditTemplate.js"></script>
<script type="text/javascript" src="../js/jquery-addOptions.js"></script>
<script type="text/javascript" src="../js/jquery.miniColors.js"></script>
<script type="text/javascript" src="../js/jquery.mini.color.js"></script>
<style>
.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity = 80);
	display: none;
}

.box_content {
	position: fixed;
	top: 10%;
	left: 28%;
	/*height: 30%;*/
	right: 30%;
	/*width: 43%;*/
	width:575px;
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	
	background: none repeat scroll 0 0 #FFFFFF;
	border: 8px solid #ACACAC;
}
#inpDefault{
	width: 20px;
	height: 20px;
	background-color: #045fb4;
}

#imgLogo {
    border: 1px solid black;
    height: 72px;
   
    margin-top: 10px;
    padding: 0;
    text-align: center;
    width: 336px;
    margin-left:auto;
    margin-right:auto;
}
#item .odd, #item .even {
border: 1px solid #ACACAC !important;
margin-right:5px;
margin-bottom:5px;
}
.selectLink {
    left: 0px !important;
    position: relative;
}
</style>
<div class="container">
	<input type="hidden" id=reqHeight name="reqHeight" value="72"/>
	<input type="hidden" id="reqWidth" name="reqWidth" value="336" />
	<input type="hidden" id="current_label_id" value="" /> 
	<input type="hidden" id="current_sublabel_id" value="" />
	<!--Header starts-->
	<div class="header"></div>
	<%@ include file="WEB-INF/header/userNavigation.jsp"%>
	<!-- <div id="centercontainer" class="centercontainer"> -->
	<div class="insidecentercontainer">
		<div class="nav">
			 <%@ include file="WEB-INF/header/headerLink1.jsp"%>
		</div>
		<div style="clear:both"></div>
		<input type="hidden" id="currentSpanOption" class="currentSpanOption" />
		<!-- Inner Container Starts -->
		<div  class="insideinnercontainer" >
		<!-- <div class="innercontainer"> -->
			<!-- Bottom container left Starts -->
			<div id="bottomContainerLeft">
				<!-- Header Starts -->
				<div id="header">
					<p id="headerText" style="display: block; margin-left: 100px;">${template.headerText}</p>
					<input type="text" value="${template.headerText}" id="editText" style="display: none;" maxlength="24" />
					<a href="#" id="editHeading">
						<img  src="../images/ieditover.PNG" width="16px" height="17px">
					</a>
					<a href="#" id="okHeading" style='display:none;'>
						<img id="iok" src="../images/iok.png" width="16px" height="17px">
					</a>
				</div>
				<!-- Header Ends -->
				<!-- Image Starts -->
				<input type="hidden" name="currentDiv" id="currentDiv" value="compDiv_1" /> 
				<input type="hidden" name="prevDiv" id="prevDiv" value="compDiv_1"/>
				<form id="updateForm" name="updateForm" action="./updateTemplate.do" method="post">
				<div id="imgLogo" align="center">
					<c:choose>
						<c:when test="${template.templateImage eq null || template.templateImage eq ''}">
								<p id="msg">(Your logo here)<br/>Dimension:[340 x 75]pixels</p>
						</c:when>
						<c:otherwise>
							<img src="/ImageLibrary/${template.templateImage}" style="max-width:336px !important;max-height:72px !important;margin:0 !important;">
						</c:otherwise>
					</c:choose>
				</div>
				<!-- Image Ends -->
				<!-- Mileage Starts -->
				<div class="text_field">
					<span class="formtxt">Mileage_________________</span>
				</div>
				<!-- Mileage Ends -->
				<!-- VContainer Starts  -->
				<div id="VCcontinner" class="VCcontinner">
					<!-- List left starts -->
						<input type="hidden" name="edited" id="edited" value="false" />
						<input type="hidden" name="headingText" id="headingText" value="${template.headerText}"/>
						<input type= "hidden" name="saveImage" id="saveImage" value="${template.templateImage}"/>
						<input type="hidden" name="templateId" id="templateId" value="1" />
						<input type="hidden" name="headerColor" id="headerColor" value="${template.headingColor}" /> 
						<input type="hidden" name="currHeaderColor" id="currHeaderColor" value="${template.headingColor}" />
						<!-- List Container left Edit Starts -->	
						<div class="listcontatleftEdit">
							<!-- List out first eight elements -->
							<ul class="VClist">
								<c:forEach items="${leftComponents}" var="formComponent" varStatus="status">
								<input type="hidden" name="templatePosition${status.count}" id="templatePosition${status.count}" value="${status.count}" />
									<input type="hidden" name="viewComponent${status.count}" id="viewComponent${status.count}" value="${formComponent.componentId}" />
									<input type="hidden" name="templateComponent${status.count}" id="templateComponent${status.count}" value="${formComponent.componentId}" />
									<input type="hidden" name="viewPosition${status.count}" id="viewPosition${status.count}" value="${status.count}" />
									<input type="hidden" name="componentName${status.count}" id="componentName${status.count}" value="${formComponent.componentName}" />
									<input type="hidden" name="label${status.count}" id="label${status.count}" value="${formComponent.subComponents.label}" />
									<input type="hidden" name="componentId${status.count}" id="componentId${status.count}" value="${formComponent.componentId}" />
									<li>
									<c:if test="${formComponent.templateComponent.status.statusId == 1}">
											<input type="hidden" name="visibility${status.count}" id="visibility${status.count}"  value="" class="displayDiv"/>
											<div id="visibleDiv${status.count}" class="ListDivVisible">
									</c:if> 
									<c:if test="${formComponent.templateComponent.status.statusId == 2}">
											<input type="hidden"  name="visibility${status.count}" id="visibility${status.count}" value="hidden" class="displayDiv" />
											<div id="visibleDiv${status.count}" class="ListDivHidden">
									</c:if>
									<c:if test="${formComponent.templateComponent.status.statusId == 3}">
											<input type="hidden"  name="visibility${status.count}" id="visibility${status.count}" value="deleted" class="displayDiv" />
											<div id="visibleDiv${status.count}" class="ListDivHidden">
									</c:if>
										<div id="compDiv_${status.count}" class="ListDiv">
											<span class="text" id="headingSpan${status.count}">${formComponent.componentName}</span>
											<img class="iImageEdit handSymbol" src="../images/ieditover.PNG" id="editComp_${status.count}" /> 
											<img class="iDeleteEdit handSymbol" src="../images/iDelete.png" id="delComp_${status.count}" />
											<c:if test="${(status.count != 1)&&(status.count != 3)&&(status.count != 6)}">
												<br /><span class="subText" id="labelSpan${status.count}">${formComponent.subComponents.label}</span>
											</c:if>
											<div id="optDivParent${status.count}">
											<c:forEach
												items="${formComponent.subComponents.subComponentsOptions}"
												var="subComponentOption" varStatus="statLeft">
												<c:if test="${subComponentOption.status == 'true'}">
													<c:set var="qt1" value= "\"" scope="page" />   
													<c:set var="qt2" value= "&#34;" scope="page" />    
													<input type="hidden" name="optionValue${status.count}" id="optionValue${status.count}" value="${fn:replace(subComponentOption.options.optionDescription, qt1, qt2)}" />
													<input type="hidden" name="optionString${status.count}" id="optionString${status.count}" value="${fn:replace(subComponentOption.options.optionDescription, qt1, qt2)}" />
													<input type="hidden" name="optionStringUpdate${status.count}" id="optionStringUpdate${status.count}" value="${fn:replace(subComponentOption.options.optionDescription, qt1, qt2)}" />
													<input type="hidden" name="scoptionId${status.count}" id="scoptionId${status.count}" value="${subComponentOption.subComponentOptionsId}" />
													<input type="hidden" name="optionId${status.count}" id="optionId${status.count}" value="${subComponentOption.options.optionId}" />
													<input type= "hidden" id="optType${status.count}" name="optType${status.count}" value="${subComponentOption.optionType}" />
													<div class="checkboxcnt" id="optDiv${status.count}">
														<div class="checkbox">
															<span> 
															<c:forEach var="option" items="${fn:split(subComponentOption.options.optionDescription, '^')}"
																	varStatus="stat">
																	<c:if test="${option != '' && option != null }">
																		<img src="../images/${fn:toLowerCase(subComponentOption.optionType)}.png" class="imageOptionType" id="image_${status.count}${stat.count}">
																		<label id="opt${status.count}${stat.count}" class="optionText">${option}</label>
																	</c:if>
																</c:forEach> </span>
														</div>
													</div>
												</c:if>
											</c:forEach>
											</div>
											<div class="clear"></div>
										</div>
						<!-- List Container left Edit Ends -->
						</li>
						</c:forEach>
						</ul>
						</div>
				<!--List left Ends-->
				.
				<!--List right starts-->
				<div class="listcontatrightEdit">
					<!-- List out rest seven elements -->
					<ul class="VClist">
						<c:forEach items="${rightComponents}" var="formComponentRight"
							varStatus="status">
							<input type="hidden" name="templatePosition${status.count+8}" id="templatePosition${status.count+8}" value="${status.count+8}" />
							<input type="hidden" name="viewComponent${status.count+8}" id="viewComponent${status.count+8}" value="${formComponentRight.componentId}" />
							<input type="hidden" name="viewPosition${status.count+8}" id="viewPosition${status.count+8}" value="${status.count+8}" />
							<input type="hidden" name="templateComponent${status.count+8}" id="templateComponent${status.count+8}" value="${formComponentRight.componentId}" />
							<input type="hidden" name="componentName${status.count+8}" id="componentName${status.count+8}" value="${formComponentRight.componentName}" />
							<input type="hidden" name="label${status.count+8}" id="label${status.count+8}" value="${formComponentRight.subComponents.label}" />
							<input type="hidden" name="componentId${status.count+8}" id="componentId${status.count+8}" value="${formComponentRight.componentId}" />
							<li>
								<c:if test="${formComponentRight.templateComponent.status.statusId == 1}">
									<input type="hidden" name="visibility${status.count+8}" id="visibility${status.count+8}" value=""  class="displayDiv" />
									<div id="visibleDiv${status.count+8}" class="ListDivVisible">
								</c:if> 
								<c:if test="${formComponentRight.templateComponent.status.statusId == 2}">
								<input type="hidden" name="visibility${status.count+8}" id="visibility${status.count+8}" value="hidden" class="displayDiv" />
									<div id="visibleDiv${status.count+8}" class="ListDivHidden">
								</c:if>
								<c:if test="${formComponentRight.templateComponent.status.statusId == 3}">
											<input type="hidden"  name="visibility${status.count+8}" id="visibility${status.count+8}" value="deleted" class="displayDiv" />
											<div id="visibleDiv${status.count+8}" class="ListDivHidden">
									</c:if>
								<div id="compDiv_${status.count+8}" class="ListDiv">
									<span class="text" id="headingSpan${status.count+8}">${formComponentRight.componentName}</span>
									<img class="iImageEdit handSymbol" src="../images/ieditover.PNG" id="editComp_${status.count+8}" /> 
									<img class="iDeleteEdit handSymbol" src="../images/iDelete.png" id="delComp_${status.count+8}" />
									<c:if test="${(status.count != 5)}">
										<br /><span class="subText" id="labelSpan${status.count+8}">${formComponentRight.subComponents.label}</span>
									</c:if>
									<div id="optDivParent${status.count+8}">
									<c:forEach items="${formComponentRight.subComponents.subComponentsOptions}"
										var="subComponentOptionRight">
										<c:if test="${subComponentOptionRight.status == 'true'}">
											<c:set var="qt1" value= "\"" scope="page" />   
											<c:set var="qt2" value= "&#34;" scope="page" />   
											<input type="hidden" name="scoptionId${status.count+8}" id="scoptionId${status.count+8}" value="${subComponentOptionRight.subComponentOptionsId}" />
											<input type="hidden" name="optionId${status.count+8}" id="optionId${status.count+8}" value="${subComponentOptionRight.options.optionId}" />
											<input type="hidden" name="optionValue${status.count+8}" id="optionValue${status.count+8}" value="${fn:replace(subComponentOptionRight.options.optionDescription, qt1, qt2)}" />
											<input type="hidden" name="optionString${status.count+8}" id="optionString${status.count+8}" value="${fn:replace(subComponentOptionRight.options.optionDescription, qt1, qt2)}" />
											<input type="hidden" name="optionStringUpdate${status.count+8}" id="optionStringUpdate${status.count+8}" value="${fn:replace(subComponentOptionRight.options.optionDescription, qt1, qt2)}" />
											<input type= "hidden" id="optType${status.count+8}" name="optType${status.count+8}" value="${subComponentOptionRight.optionType}" />
											<div class="checkboxcnt" id=optDiv${status.count+8}>
												<c:choose>
													<c:when test="${(status.count) == 7}">
														<c:forEach var="optionRight" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}" varStatus="statRight1">
															<div class="checkboxcnt">
																<div class="checkbox15">
																	<c:if test="${optionRight != '' && optionRight != null }">
																	<span>
																		<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png" class="imageOptionType" id="image_${status.count+8}${statRight1.count}">
																		<label id="opt${status.count+8}${statRight1.count}" class="optionText">${optionRight}</label> 
																	</span>
																	</c:if>
																</div>
															</div>
														</c:forEach>
													</c:when>
													<c:otherwise>
														<div class="checkbox">
															<span> 
																<c:forEach var="option" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}" varStatus="statRight2">
																<c:if test="${option != '' && option != null }">
																	<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png" class="imageOptionType" id="image_${status.count+8}${statRight2.count}">
																	<label id="opt${status.count+8}${statRight2.count}" class="optionText">${option}</label>
																</c:if>
																</c:forEach> </span>
														</div>
													</c:otherwise>
												</c:choose>
											</div>
										</c:if>
									</c:forEach>
									</div>
									<div class="clear"></div>
								</div></li>
						</c:forEach>
					</ul>
				</div>
				<!--List right Ends-->
				</form>
				</div>
				<!-- VContainer Ends  -->
				<!-- Bottom Text Starts -->
				<div class="bottomtext">
				<span style="float:left;">Comments:</span>
				<hr style="width: 87%; margin-top:4%;" /><br />
				<hr style="width: 99%;" /><br />
				<hr style="width: 99%;" /><br />    
				<span style="float: left; clear: left; margin-top: -4%;">Inspected by: </span> 
				<hr style="width: 45%;float:left;position:relative; top:-3px;" />
				<span style="float: left; margin-top: -4%; width: 20.5%;">Date:</span>
				<hr style="width: 33.5%;*width:36%;float:left;margin-left:-14%;position:relative; top:-3px;" />
				</div>
								
				<!-- Bottom Text Ends -->
				<!--Container Bottom Starts-->
				<div class="containerbtm" style="padding-left:42px;">
					<a id="updateId" class="btn-txt" onclick="launchWindow('#dialog');">Save </a>
					<a id="cancelButtonId" href="./navigateTemplateView.do"  class="btn-txt" onclick="launchWindow('#dialog');">Cancel </a>
					<a id="createComponent"  class="btn-txt">Create Component </a>
					<a id="deleteComponent" class="btn-txt" >Delete Component</a>
					<%-- <a id="deleteComponent" class="btn-txt" onclick="displayHideBox('2'); return false;">Delete Component</a>
					<a id="updateId" class="btn-txt">Save</a>
					<a id="cancelButtonId" href="./navigateTemplateView.do"  class="btn-txt">Cancel</a>
					<a id="createComponent"  class="btn-txt">Create Component</a> --%>
				</div>
				<!--Container Bottom Ends-->
		</div>
		<!-- Bottom container left Ends -->
		<!-- Bottom container Right Starts -->
		<div id="bottomContainerRight">
			<!-- Upload And Heading color and repository div Starts -->
			<div style="border:1px solid black;height: auto; overflow:auto; ">
			<div style="float: left; margin-left: 10px; margin-top: 0px;">
				<form name="uploadLocalImage" action="./editTemplate2.do" method="post" id="uploadLocalImage" enctype="multipart/form-data">
					<div class="sideheading">
						<label><nobr>Upload your own image :</nobr> </label>
					</div>
					<div>
					   <input type="hidden" id="center" name="center" value="template1">
				      <input type="hidden" id="centerimageHeight" name="centerimageHeight" value="72">
				      <input type="hidden" id="centerimageWidth" name="centerimageWidth" value="336">
						<span><input name="fileImage" type="file" id="localImage" value="${logoImage}" /></span>
						<c:choose>
							<c:when test="${template.templateImage != null && template.templateImage != ''}">
								<span id="delBtn">
									<input type="button" value="Delete" id="deleteButton"/>
								</span>
							</c:when>
							<c:otherwise>
								<span id="delBtn"></span>
							</c:otherwise>
						</c:choose>
						<span id="saveBtn"></span>
					</div>
				</form>
			</div>
			<div style="float: left; margin-left: 10px; margin-top: 10px;">
				<form name="uploadRImage" action="./browseImage.do?type=template1" method="post" id="uploadRImage" enctype="multipart/form-data">
					<div class="sideheading">
						<label><nobr>Choose from existing images :</nobr>
						</label>
					</div>
					<div>
						<input type="text" readonly="readonly" style="height: 18px;margin-right:1px;" id="selectImg">
						<input type="submit" value="Browse..." id="browse" onclick="displayHideBox('1'); return false;" style="margin-left: 0px; width: 75px;">
						<%--<c:choose>
							<c:when test="${template.templateImage != null && template.templateImage != ''}">
								<span id="delBtn1">
									<input type="button" value="Delete" id="deleteButton1" style="display:none;margin-left: 3px;margin-top: -3px;"/>
								</span>
							</c:when>
							<c:otherwise>
								<span id="delBtn1"></span>
							</c:otherwise>
						</c:choose>--%>
						<div id="grayBG" class="grayBox">
						</div>
						<div id="LightBox1" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
							<span style="float: right;"><img src="../images/icon-delete.gif" onclick="displayHideBox('1')"></span>
							<div style="background: #fff; width: 100%;">
								<div onclick="displayHideBox('1'); return false;" style="cursor: pointer;" align="right"></div>
								<p>
								<div id="lbox"></div>
								</p>
							</div>
						</div>
					<div id="LightBox2" class="box_content" style="display: none;" style="background: #fff; width: 100%;">
						<a href="./editTemplate2.do" onclick="displayHideBox('2')"><span style="float: right;"><img src="../images/icon-delete.gif" id="closeShowComp" ></span></a>
						<div style="background: #fff; width: 100%;">
							<div onclick="displayHideBox('1'); return false;" style="cursor: pointer;" align="right"></div>
							<p>
							<div id="compBox"></div>
							</p>
						</div>
					</div>
					</div>
				</form>
				<div class="sideheading" style="clear:left;">
					<label>Heading color:</label>
				</div>
				<div id="iColor">
					&nbsp;&nbsp;&nbsp;<input type="image" name="color3" class="colors" value="#123456" id="inp1" src="../images/images.png"/>
					<!-- &nbsp;&nbsp;&nbsp;<input type="image" class="color" name="clr" id="inp1" src="../images/images.png"> -->
				</div>
				<div class="sideheading">
					<label>Default Heading color:</label> <input type="button"  id="inpDefault"  style="cursor: pointer;" />
				</div>
			</div>
			</div>
			<!-- Upload And Heading color and repository div Ends -->
			<div style="clear: both;"></div>
			<div id="addComponent" style="display: none; float: right;margin-top: 5px;width: 100%; border: none;">
				<div class="rightcontainer" id="addCompRightContainer">
				</div>
			</div>
			<div style="clear: both;"></div>
			<div id="editComponent" style="display: none; float: right;margin-top: 5px;width: 100%;">
			<input type="hidden" name="lastEditOptionDiv" id="lastEditOptionDiv" value="" /> 
				<div class="rightcontainer">
					<div class="sideheading">
						<label>Position on the template:</label><label id="showPosition"></label>
					</div>
					<div class="sideheading">
						<label>Heading</label><br /> <input name="TextboxExample"
							type="text" maxlength="25" id="changedHeading" tabindex="2"
							onkeyup='editHeadingLabel(this.id)'
							style="width: 193px; height: 17px; position: absolute; border-right: none;border-bottom: none;" />
							 <select name="CompDrp" id="CompDrp" tabindex="1000" size="1" style="text-transform: capitalize; font-size: 12pt; font-family: 'MyriadProRegular', Arial; ">
						<%-- <select name="CompDrp" id="CompDrp" tabindex="1000" size="1"
							onchange="DropDownTextToBox(this,'changedHeading');"> --%>
							<option value="0">--Select--</option>
							<c:forEach items="${compDrp}" var="entry" varStatus="status">
								<option value="${entry.key}" id="headingValue">${entry.value}</option>
							</c:forEach>
						</select>
					</div>
					<%--gets replaced when another component is selected from the dropdown --%>
					<div id="compDetailsEdit">
						<!-- Added for drefault values -->
						<div id="subComponentDtls">
							<div class="sideheading">
								<label>Subheading</label> <br /> 
								<input type="text" value="" id="editLabel" onkeyup="editLabel(this.id)" maxlength="24" />
							</div>
							<div class="sideheading" id="sideHeadingOption">
								<label>Option</label><br />
								<div id="editOpdtionDiv">
									<select id="editOption">
										<option value="${options.subComponentOptionsId}"
											selected="selected">${options.options.optionName }</option>
										<option value="${options.subComponentOptionsId }">${options.options.optionName
											}</option>
									</select>
								</div>
							</div>
							<div class="sideheading" id="tickType">
								<select id="editOptionType" name="editOptionType">
									<option id="1" value="Square">Square</option>
									<option id="2" value="Circle">Circle</option>
									<option id="3" value="None">None</option>
								</select>
							</div>
							<div class="sideheading" id="editDesc">
								<div id="optionList"></div>
							</div>
						</div>
						<!-- ends the default div -->
					</div>
					<%-- End of the div that is to be replaced --%>
					<div class="sideheading" style="text-align: center; overflow: auto;">
						<input id="rightOk" class="btn_forright" type="button" value="Ok" />
						<input id="rightCancel" class="btn_forright" type="button" value="Cancel" />
					</div>
				</div>
			</div>
			<!-- EditComponent Ends -->
			<div style="clear: both;"></div>
			<!-- CreateNewContainerRight123 Starts -->
			<div id="createNewContainerRight123">
					<!-- Admin actions will be shown up here -->
					<div id="createNewContainerRightContent123">
						<div>
							<%-- start of div for create component --%>
							<div id="createOptionDetails" >
							
							</div>	
							<%-- end of div for create component --%>
					</div>
				</div>
			</div>
			<!-- CreateNewContainerRight123 Ends -->
			
			<!-- CreateCompOptionDetails Starts -->
			<div id="createCompOptionDetails" style="display: none; float: right;margin-top: 5px;width: 93%; border: 1px solid #000000; padding: 10px;" >
					<div id="createCompOptionDiv" >
							<%-- start of div to create option for an existing Component --%>
							<%-- end of div to create option for an existing Component --%>
					</div>
			</div>
			<!-- CreateCompOptionDetails Ends -->
		</div>
		<!-- Bottom container Right Ends -->
		<div style="clear: both;"></div>
	</div>
	<!-- Center Container Ends -->
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
