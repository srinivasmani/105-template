<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<%@ page import="java.util.*" %>
<style>
div#image { 
    position:relative;
    width:400px; height:300px; /* width accommodates scrollbar on right */
    overflow:auto; /* for non-javascript */
    }
    .imgSet{
     	float: left;
     	width: 30%;";
    }
</style>
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/jquery-editTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>

<link rel="stylesheet" type="text/css" href="../css/pagination.css"></link>
<input type="text" id="pageId" value="${pageId}" style="display:none;" />
<input type = "text" id = "place" class = "place" name = "place" value = "${type}" style="display: none;"/>
<div id="image" style="width: 100%;margin-top:10px;">
<input type="text" id="imageSize" value="${fn:length(items)}" style="display: none;" />
<%! int i=0; %>
	<input type="text" value="${user.userRole}" name="userRole" id="userRole" class="userRole" style="display: none;">
		<display:table name="${items}" id="item" sort="list" pagesize="6" requestURI="./browseImage.do">
		<%i++; %>
			<display:column>
				<div id="boxesdiv<%=i%>" style="width:165px;text-align:center;">
				  <div class="unHighlighted" style="width: 165px" id="imgDiv<%=i%>">
					<img src="/ImageLibrary/${item[0]}" width="150" height="100" id="logo<%=i%>">
				  </div>
					<c:if test="${user.userRole =='admin'}">
						<div class="imgSet">
							<input type="hidden" value="${item[0]}" name="imageName" id="imageName_<%=i%>">
							<a href="#" id="selectLink_<%=i%>" class="selectLink">Select</a>
						</div>
						<div class="imgSet">
							<input type="hidden" value="${item[1]}" name="imageId" id="imageId_<%=i%>">
							<a href="#" id="deleteLink_<%=i%>" class="deleteLink">Delete
							</a>
						</div>
					</c:if>
					<c:if test="${user.userRole == 'user'}">
						<div class="imgSet">
							<input type="hidden" value="${item[0]}" name="imageName" id="imageName_<%=i%>">
							<a href="#" id="selectLink_<%=i%>" class="selectLink"><img src="../images/iSelect.PNG"></a>
						</div>
					</c:if>
				</div>
			</display:column>
			 <%@include file="./displayTagProperties.jsp" %>
		</display:table>
</div>
<script type="text/javascript">

jQuery(document).ready(function(){
	var pageId=1;
	var temp=$("#pageId").val();
	 if(temp==""){
		 jQuery("#pageId").val("/browseImage.do?d-49489-p=1");
	 }
	
	jQuery("#firstPage a").click(function(){
		pageId =jQuery(this).attr('href');
		jQuery("#pageId").val(pageId);
		jQuery(this).attr('href',"#");
			 jQuery.ajax({   
			   url :pageId,
			   type :"POST",
			   dataType : "html",   
			   success : function(data) {
				 jQuery("#image").html(data);
			   },  
			   error : function(xmlhttp, error_msg) {
			   }
  			 });
		});
});
</script>