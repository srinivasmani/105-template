<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<div>
<span class="hellouser">Hello ${user.userName}</span>
	<div id="pageHeading" class="inspectionHeader" align="center">Welcome To Mighty Customizable Vehicle Inspection Forms.</div>
	
	<p class="ifDesc">
	This application will allow you to design your own customized vehicle inspection forms. 
	</p>
	<c:if test="${fn:length(userForms) < 12}">
	<div class="ifLeft">
	<a href="./user/navigateTemplateView.do"><img src="./images/tem1.png" title="Template 101" alt="Template 101" /></a> <br />
	<a href="./user/getUserComponentIfs.do"><img src="./images/tem2.png" title="Template 102" alt="Template 102" /> </a><br />
	<a href="./user/getUserFormView103.do"><img src="./images/tem3.png" title="Template 103" alt="Template 103" /></a> <br />
	<a href="./user/createUserComponentForm104.do"><img src="./images/tem4.png" title="Template 104" alt="Template 104" /></a> <br />
        <a href="./user/createUserComponentForm105.do"><img src="./images/tem5.png" title="Template 105" alt="Template 105" /></a> <br />
	</div>
	</c:if>
	<c:if test="${fn:length(userForms) >= 12}">
	<div class="ifLeft">
	<img src="./images/tem1.png" title="Template 101" alt="Template 101" /> <br />
	<img src="./images/tem2.png" title="Template 102" alt="Template 102" /> <br />
	<img src="./images/tem3.png" title="Template 103" alt="Template 103" /> <br />
	<img src="./images/tem4.png" title="Template 104" alt="Template 104" /> <br />
        <img src="./images/tem5.png" title="Template 105" alt="Template 105" /> <br />
	</div>
	</c:if>
	<div class="ifRight">
	<div id="formList" class="formList" >
	<input type="hidden" value="${form.userFormId}" name="formId" value="formId" />
	<span class="formTitle">My Inspection Forms</span><br />
	<div id="pageContent">
	<div id="userFormList">
	<c:set var="componentDesc" value="${template.headerText}"/>
		<c:if test="${userForms != null}">
			<c:forEach var="form" items="${userForms}" varStatus="status">
					<div style='clear: both;' id="userDefinedForms">
						<div id='formNo'>
							<font color='#A52A2A'><b>${status.count}</b> </font>
						</div>
						<div id='formNames_${status.count}' class='formNames'>
							<font color='#A52A2A'>
								<b>		
								<c:choose>
									<c:when test="${form.templateId eq '1'}">
										<a href="./user/userFormView.do?formid=${form.userFormId}" id="formLink"><span id="formName_${status.count}">${form.formName}</span></a>
									</c:when>
									<c:when test="${form.templateId eq '2'}">
										<a href="./user/getUserViewComponentIfs.do?formid=${form.userFormId}" id="formLink"><span id="formName_${status.count}">${form.formName}</span></a>
									</c:when>
									<c:when test="${form.templateId eq '3'}">
										<a href="./user/getUserComponent103.do?formid=${form.userFormId}" id="formLink"><span id="formName_${status.count}">${form.formName}</span></a>
									</c:when>
									<c:when test="${form.templateId eq '4'}">
										<a href="./user/getUserFormByFormId104.do?formId=${form.userFormId}" id="formLink"><span id="formName_${status.count}">${form.formName}</span></a>
									</c:when>
                                                                        <c:when test="${form.templateId eq '5'}">
										<a href="./user/getUserFormByFormId105.do?formId=${form.userFormId}" id="formLink"><span id="formName_${status.count}">${form.formName}</span></a>
									</c:when>        
								</c:choose>
							</b>
							</font>
						</div>
						<div id='formDeleted'>
							<input type="hidden" value="${form.userFormId}" id="userFormId_${status.count}">
							<input type="hidden" value="${user.userId}" id="userIdIfs_${status.count}">
							<span  id="formDelete_${status.count}" class="deleteLink"> 
								<img src="./images/Button-Close-icon.png"> 
							</span>
						</div>
						<div id='formPdf'>
							<a href="./user/GenerateUserFormPdf.do?userFormId=${form.userFormId}" id="formLink" target="_newtab">
								<img src="./images/PDF_icon.png">
							</a>
						</div>
					</div>
				</c:forEach>
		</c:if>
		</div>
		<div style="padding-bottom: 10px;">
		<c:if test="${userForms == null}">
		<center>
			<p>You have not created any form ..</p>
		</center>
		</c:if>
		<c:if test="${fn:length(userForms) < 12}">
		<div id="createNewBtn" >
		<a href="./user/navigateTemplateView.do" style="text-align:center;" class="btn-txt formlink1" >Create New</a>
		
		</div>
		</c:if>
		<c:if test="${fn:length(userForms) >= 12}">
		<center><p class="deleteForm">Please delete one of the existing forms.</p></center>
		</c:if>
	</div>
	</div>
	</div>
	<div class="instructionList">
	<span class="formTitle">Instructions To Follow</span><br />
	<div id="pageContent" style="border-right:0px;">
	
	<div id="userFormList">
					<div id="userDefinedForms" style="clear: both;">
						<div id="formNo">
							1
						</div>
						<div id="formNames">
										<span id="formLink" >  Pick a template either from your list of saved forms or create a new template</span>
						</div>
					
					
					</div>
				
					<div id="userDefinedForms" style="clear: both;">
						<div id="formNo">
							2
						</div>
						<div id="formNames">
										<span id="formLink">Edit the template</span>
							
						</div>
					
					</div>
					
						<div id="userDefinedForms" style="clear: both;">
						<div id="formNo">
							3
						</div>
						<div id="formNames">
										<span id="formLink">Save the template</span>
							
						</div>
					
					</div>
					
							<div id="userDefinedForms" style="clear: both;">
						<div id="formNo">
							4
						</div>
						<div id="formNames">
										<span id="formLink">Generate PDF</span>
							
						</div>
					
					</div>
		
				
							<div id="userDefinedForms" style="clear: both;">
						<div id="formNo">
							5
						</div>
						<div id="formNames">
										<span id="formLink">You may either print your form or save as PDF</span>
							
						</div>
					
					</div>
		</div>
	
	</div>
	</div>
	</div>

</div>
