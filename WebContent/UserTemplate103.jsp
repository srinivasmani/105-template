<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<link rel="stylesheet" type="text/css" href="../css/template3.style.css" />
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<script type="text/javascript" src="../js/image-resizing.js"></script>

<title>User Template 103</title>
<style type="text/css">
#topCmpt{
	font-family: 'MyriadProBold', Arial;
	font-size: 11pt;
}
.imgLogo{
    height: 100px; 
    width: 380px;
    margin-top: 0px;
    padding: 0;
    text-align:center;
    border:none;
}
.noImgLogo{
	 height: 100px; 
    width: 380px;
    margin-top: 0px;
    padding: 0;
    text-align:center;
    border:1px solid #000;
}
</style>
<script>
jQuery(document).ready(function(){
	var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
		if(is_chrome == true){
			jQuery("#customer").css("width", "760px");
			jQuery(".custSpanText").css("width", "760px");
	}
});
</script>
	<div class="container" style="padding: 0px;">
	 <%@ include file="WEB-INF/header/userNavigation.jsp"%>
		<div id="centercontainer" class="centercontainer">
			<div class="nav">
			<ul>
			<c:if test="${userForm.status ne 'default'}">
				<li><a href="#" class="selected" id="formNameTab">${userForm.formName}</a></li>
			</c:if>
			<c:if test="${userForm.status eq 'default'}">
				<li><a href="./navigateTemplateView.do" onclick="launchWindow('#dialog');">Template 101</a>
				</li>
				<li><a href="./getUserComponentIfs.do" onclick="launchWindow('#dialog');">Template 102</a>
				</li>
				<li><a href="./getUserFormView103.do" class="selected" onclick="launchWindow('#dialog');">Template 103</a>
				</li>
				<li><a href="./createUserComponentForm104.do" onclick="launchWindow('#dialog');">Template 104</a>
				</li>
                                <li><a href="./createUserComponentForm105.do" onclick="launchWindow('#dialog');">Template 105</a>
				</li>
			</c:if>
			</ul>
		</div>
			<div style="clear: both"></div>
			<div class="innercontainer">
				<div id="templateSize">
					<div style="width: 21.5cm;" class="divLogoTab">
						<div class="divLogoRow">
							<c:if test="${userForm.status eq 'default'}">
							<div class="logo1">
								<c:if test="${userForm.formLeftImage ne null && userForm.formLeftImage ne ''}">
									<div class="leftOptImage103" style="border:none;"></div>
								</c:if>
							</div>
							</c:if>
							<c:if test="${userForm.status ne 'default'}">
							<div class="logo1">
								<c:choose>
									<c:when test="${userForm.formLeftImage ne null and userForm.formLeftImage ne ''}">
										<div class="leftOptImage103" style="border:none;">
											<img id="imageSize103" src="/ImageLibrary/${userForm.formLeftImage}" style="margin:0;max-width:100px; max-height:100px;"/>
										</div>	
									</c:when>
									<c:otherwise>
										<div class="leftOptImage103" style="border:none;"></div>
									</c:otherwise>
								</c:choose>
							</div>
							</c:if>
							<div id="imgLogo" align="center"  style="border:none;">
								<c:choose>
									<c:when test="${userForm.formImage eq null or userForm.formImage eq ''}">
										<div class="noImgLogo">
											<p id="msg">(Your logo here)<br/>Dimension:[380 x 100]pixels</p>
										</div>
									</c:when>
									<c:otherwise>
									<div class="imgLogo">
											<img src="/ImageLibrary/${userForm.formImage}" style="margin:0; max-width:380px;max-height:100px;">
										</div>
									</c:otherwise>
								</c:choose>
							</div>
							<c:if test="${userForm.status eq 'default'}">
							<div class="logo2-103">
								<c:if test="${userForm.formRightImage ne null and userForm.formRightImage ne ''}">
									<div class="rghtOptImage103"></div>
								</c:if>
							</div>
							</c:if>
							<c:if test="${userForm.status ne 'default'}">
							<div class="logo2-103">
								<c:choose>
									<c:when test="${userForm.formRightImage ne null and userForm.formRightImage ne ''}">
										<div class="leftOptImage103" style="border:none;">
											<img id="imageSize103" src="/ImageLibrary/${userForm.formRightImage}" style="margin:0;max-width:100px; max-height:100px;"/>
										</div>
									</c:when>
									<c:otherwise>
										<div class="leftOptImage103" style="border:none;"></div>
									</c:otherwise>
								</c:choose>
							</div>
							</c:if>
						</div>
					</div>
			
				
				<%
					String getSVG = (String) request.getAttribute("imgType");
					String green = "";
		    		String yellow = "";
					String red = "";
					String greenSmall = "";
		    		String yellowSmall = "";
					String redSmall = "";
					if ( (getSVG != null) && (getSVG.equalsIgnoreCase("circle"))) {
				%>
				<% 
						getSVG = "<b class='greenCircle'></b><b class='yellowCircle'></b><b class='redCircle'></b>";
			  			green = "<b class='greenCircle'></b>";
		      			yellow = "<b class='yellowCircle'></b>";
			  			red = "<b class='redCircle'></b>";
			  			greenSmall = "<b class='smallGreenCircle'></b>";
		      			yellowSmall = "<b class='smallYellowCircle'></b>";
		      			redSmall = "<b class='smallRedCircle'></b>";
					}else
					{
				%>
				<% 
						getSVG = "<b class='green'></b><b class='yellow'></b><b class='red'></b>";
			  			green = "<b class='green'></b>";
		     	 		yellow = "<b class='yellow'></b>";
			  			red = "<b class='red' ></b>";
			  			greenSmall = "<b class='smallGreen' style='width:20px; height:20px;'></b>";
		     			yellowSmall = "<b class='smallYellow' style='width:20px; height:20px;'></b>";
		     			redSmall = "<b class='smallRed' style='width:20px; height:20px;'></b>";
					}
				%>
				<div id="pageHeading">
					<p id="heading103">
						<label id="lblText" class="text_label" style = "text-transform: uppercase;"><nobr>${userForm.headerText}</nobr></label>
					</p>
					<div class="clear"></div>
				</div>
				<div class="divTable">
					<div class="divRow" style="margin-left:10px;">
						<div id="customer" class="divCell" style="width:763px;">
							<c:forEach items="${detailUserFormComponent}" var="detail" varStatus="status">
								<c:forEach var="cname" items="${fn:split(detail.component.componentName, '~')}" varStatus="stat">
									<div class="custSpanText" style="width: 763px;">
										<span id="cust${stat.count}" style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">${cname}</span>
									</div>
								</c:forEach>
							</c:forEach>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="selTable" style="width:855px;">
					<div class="selRow"  style="100%;">
						<c:forEach items="${topUserFormComponents}" var="top" varStatus="status"> 
						<div class="selCol33" style="margin-right:10px;">
							<div class="selCell">
								<c:if test="${(status.count eq 1)}">
									<span><%=green%></span> 
								</c:if>	
								<c:if test="${(status.count eq 2)}">
									<span><%=yellow%></span> 
								</c:if>	
								<c:if test="${(status.count eq 3)}">
									<span><%=red%></span> 
								</c:if>								 
								<span class="floatLeft">
									<p style="position:relative; left:5px;width:170px;white-space:nowrap;"><span id="topCmpt">${top.component.componentName}</span></p>
								</span>
							</div>
						</div>
						</c:forEach>						
					</div>
				</div>
				<div class="clear"></div>
				<div class="divTable1 paddingLeft">
					<div class="inspectionleftwrap">
						<div class="inspection_bg">
							<div class="inspectionTable">
							<c:forEach items="${topLeftUserFormComponents}" var="topLeft" varStatus="status"> 
								<c:set var="position" value="${topLeft.position.positionId}"></c:set>
								<c:set var="compName" value="${topLeft.component.componentName}"></c:set>
								<c:if test="${position eq 5}">
									<div class="clear row1 row1Title">
										<span align="center" class="th">${compName}</span>
									</div>
								</c:if>
								<c:if test="${position eq 6}">
								<div class="clear row1">
									<span class="smallheading">${compName}</span>
								</div>
								</c:if>
							</c:forEach>
								<div class="clear row1">
									<c:forEach items="${imageUserFormComponents}" var="image" varStatus="status">
										<c:if test="${image.position.positionId eq 70}">
                						<span class="alignCenter" style="display:block; height:120px;">
											<img src="/ImageLibrary/${image.component.componentName}" alt="" style="max-width: 247px;max-height:116px;" />
										</span>
										</c:if>
									</c:forEach>
								</div>
							<c:forEach items="${topLeftUserFormComponents}" var="topLeft" varStatus="status">
							<c:set var="position" value="${topLeft.position.positionId}"></c:set>
							<c:set var="status" value="${topLeft.status.statusId}"></c:set>
							<c:set var="compName" value="${topLeft.component.componentName}"></c:set> 
							<c:if test="${position gt 6 and position le 15}">
								<div class="clear row1">
									<c:choose>
										<c:when test="${status eq 1}">
											<span><%=getSVG%></span> 
											<span class="inspectionTxt">${compName}</span>
										</c:when>
										<c:otherwise>
											<span><%=getSVG%></span> 
											<span class="inspectionTxt"></span>
										</c:otherwise>
									</c:choose>
								</div>
							</c:if>
							</c:forEach>
							</div>
							<div class="inspectionTable">
								<c:forEach items="${bottomLeftUserFormComponents}" var="bottomLeft" varStatus="status">
									<c:set var="position" value="${bottomLeft.position.positionId}"></c:set>
									<c:set var="status" value="${bottomLeft.status.statusId}"></c:set>
									<c:set var="compName" value="${bottomLeft.component.componentName}"></c:set> 
									<c:if test="${position eq 15}">
									<div class="clear row1 row1Title">
										<span>${compName}</span>
									</div>
									</c:if>
									<c:if test="${position gt 15 and position le 26}">
									<div class="clear row1">
										<c:choose>
											<c:when test="${status eq 1}">
												<span><%=getSVG%></span> 
												<span class="inspectionTxt">${compName}</span>
											</c:when>
											<c:otherwise>
												<span><%=getSVG%></span> 
												<span class="inspectionTxt"></span>
											</c:otherwise>
										</c:choose>
									</div>
									</c:if>
								</c:forEach>
								<div class="clear row1">
								<c:forEach items="${bottomLeftUserFormComponents}" var="bottomLeft" varStatus="status">
									<c:set var="position" value="${bottomLeft.position.positionId}"></c:set>
									<c:set var="status" value="${bottomLeft.status.statusId}"></c:set>
									<c:set var="compName" value="${bottomLeft.component.componentName}"></c:set>
									<c:if test="${position gt 26 and position lt 30}">
									<div style="height:30px;">
										<c:choose>
											<c:when test="${status eq 1}">
												<span><%=getSVG%></span>
												<span class="inspectionTxt">${compName}</span>
											</c:when>
											<c:otherwise>
												<span><%=getSVG%></span> 
												<span class="inspectionTxt"></span>
											</c:otherwise>
										</c:choose>
									</div>
									</c:if>
								</c:forEach>
								<div>
									<div style="float: right;margin-top: -23%;width: 91px;">
									<c:forEach items="${imageUserFormComponents}" var="image" varStatus="status">
										<c:if test="${image.position.positionId eq 71}">
											<img src="/ImageLibrary/${image.component.componentName}" style="max-width: 92px;max-height: 60px;" />
										</c:if>
									</c:forEach>
									</div>
								</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="inspectionrightwrap">
						<div class="inspection_bg">
							<div class="inspectionTable">
								<c:forEach items="${topRightUserFormComponents}" var="topRight" varStatus="status">
									<c:set var="position" value="${topRight.position.positionId}"></c:set>
									<c:set var="status" value="${topRight.status.statusId}"></c:set>
									<c:set var="compName" value="${topRight.component.componentName}"></c:set>
									<c:if test="${position eq 30}">
										<div class="clear row1 row1Title">
											<span>${compName}</span>
										</div>
									</c:if>
									<c:if test="${position gt 30 and position le 38}">
										<div class="clear row1">
										<c:choose>
											<c:when test="${status eq 1}">
												<span><%=getSVG%></span> 
												<span class="inspectionTxt">${compName}</span>
											</c:when>
											<c:otherwise>
												<span><%=getSVG%></span> 
												<span class="inspectionTxt"></span>
											</c:otherwise>
										</c:choose>
										</div>
									</c:if>
								</c:forEach>
							</div>
							<div class="inspectionTable" style="border-bottom:1px solid #000;">
							<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status">
								<c:if test="${bottomRight.position.positionId eq 39}">
        						<div class="clear row1 row1Title">
         	 						<span>${bottomRight.component.componentName}</span>
        						</div>
        						</c:if>
        					</c:forEach>
								<div class="clear row1" style="border-bottom:0px;">
          							<div style="padding:0px; border:0px;height:130px" >
          								<div class="bordernone interior_inspec">
          								<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          									<c:if test="${bottomRight.position.positionId eq 40}">
              								<div class="alignCenter clear paddingBottom" style="width:360px;float:none;">
                								<span id="treadDepth">${bottomRight.component.componentName}</span>
              								</div>
              								</c:if>
              							</c:forEach>
              								<div class="clear paddingBottom" style="width:375px; height:30px;">
              								<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          										<c:if test="${bottomRight.position.positionId eq 41}">
          											<span style="width:135px; float:left;">
                									<c:choose>
                										<c:when test="${bottomRight.status.statusId eq 1}">
                											<%= green%>
                											<span class="fontF">${bottomRight.component.componentName}</span>
                										</c:when>
                										<c:otherwise>
                											<b class="white"></b>
                											<span class="fontF"></span>
                										</c:otherwise>
                									</c:choose>
                									</span>
                								</c:if>
                							</c:forEach>
                							<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          										<c:if test="${bottomRight.position.positionId eq 42}">
                									<span style="width:120px; float:left;">
                										<c:choose>
                											<c:when test="${bottomRight.status.statusId eq 1}">
                												<%=yellow%>
                												<span class="fontF">${bottomRight.component.componentName}</span>
                											</c:when>
                											<c:otherwise>
                												<b class="white"></b>
                												<span class="fontF"></span>
                											</c:otherwise>
                										</c:choose>
                									</span>
                								</c:if>
                							</c:forEach>
                							<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          										<c:if test="${bottomRight.position.positionId eq 43}">
                									<span style="width:117px; float:left;">
                										<c:choose>
                											<c:when test="${bottomRight.status.statusId eq 1}">
                												<%=red%>
                												<span class="fontF">${bottomRight.component.componentName}</span>
                											</c:when>
                											<c:otherwise>
                												<b class="white"></b>
                												<span class="fontF"></span>
                											</c:otherwise>
                										</c:choose>
                									</span>
                								</c:if>
                							</c:forEach>
              								</div>
             		 						<div class="clear">
                								<div class="alignCenter" style="width:375px;">
                										<div class="bordernone interior_inspec1 interior_inspecLeft">
                											<c:choose>
				 												<c:when test="${userFormComponents[43].status.statusId eq 1 and userFormComponents[47].status.statusId eq 1}">
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;">${userFormComponents[43].component.componentName}</strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px">${userFormComponents[47].component.componentName}</span>
                      												<span style="display:block;float:right;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
				 												</c:when>
				 												<c:otherwise>
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;"></strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px"></span>
                      												<span style="display:block;float:right;"><b class="white"></b><b class="white"></b><b class="white"></b></span>
				 												</c:otherwise>
				 											</c:choose>
                  										</div>
                										<div class="bordernone interior_inspec1 interior_inspecRight">
                											<c:choose>
				 												<c:when test="${userFormComponents[44].status.statusId eq 1 and userFormComponents[48].status.statusId eq 1}">
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;">${userFormComponents[44].component.componentName}</strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px">${userFormComponents[48].component.componentName}</span>
                      												<span style="display:block;float:right;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
				 												</c:when>
				 												<c:otherwise>
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;"></strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px"></span>
                      												<span style="display:block;float:right;"><b class="white"></b><b class="white"></b><b class="white"></b></span>
				 												</c:otherwise>
				 											</c:choose>
                  										</div>
                										<div class="bordernone interior_inspec1 interior_inspecLeft">
                											<c:choose>
				 												<c:when test="${userFormComponents[45].status.statusId eq 1 and userFormComponents[49].status.statusId eq 1}">
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;">${userFormComponents[45].component.componentName}</strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px">${userFormComponents[49].component.componentName}</span>
                      												<span style="display:block;float:right;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
				 												</c:when>
				 												<c:otherwise>
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;"></strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px"></span>
                      												<span style="display:block;float:right;"><b class="white"></b><b class="white"></b><b class="white"></b></span>
				 												</c:otherwise>
				 											</c:choose>
                  										</div>
                										<div class="bordernone interior_inspec1 interior_inspecRight">
                          									<c:choose>
				 												<c:when test="${userFormComponents[46].status.statusId eq 1 and userFormComponents[50].status.statusId eq 1}">
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;">${userFormComponents[46].component.componentName}</strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px">${userFormComponents[50].component.componentName}</span>
                      												<span style="display:block;float:right;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
				 												</c:when>
				 												<c:otherwise>
				 													<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      													<strong style="display:block;width:20px;"></strong>
                      												</span>
                      												<span class="txt_bold_lower_case" style="position:relative; left:-5px;float:right;display:block;width:67px"></span>
                      												<span style="display:block;float:right;"><b class="white"></b><b class="white"></b><b class="white"></b></span>
				 												</c:otherwise>
				 											</c:choose>
                  										</div>
				 								</div>
             							 	</div>
            							</div>
            						</div>
        						</div>
								<div style="background-color:#d4daee; height:215px;">
			  						<div style="float:left; width:200px;">
			  						<c:forEach items="${imageUserFormComponents}" var="image" varStatus="status">
										<c:if test="${image.position.positionId eq 72}">
                						<span style="padding:0px;width:100px;float:left;">
				 							<img src="/ImageLibrary/${image.component.componentName}" style="max-height: 200px;max-width: 100px;"/>
										</span>
										</c:if>
									</c:forEach>
                						<div class="bordernone interior_inspec padding_reset lessWidth" style="padding:0px;float:right; padding-top:15px;width:100px !important;">
                    					<div style="height:30px;margin-bottom:10px;">
                    					<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          									<c:if test="${bottomRight.position.positionId eq 52}">
                      						<span colspan="2" style="padding:0px;" id="treadDepth">
                      							<h2 style="text-align:center;">${bottomRight.component.componentName}</h2>
                      						</span>
                      						</c:if>
                      					</c:forEach>
                    					</div>
                    					<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          									<c:if test="${bottomRight.position.positionId gt 52 and bottomRight.position.positionId le 56 and bottomRight.component.componentName ne ''}">
                    							<div class="clear" style="display:block; margin-bottom:10px;height:20px;">
                      								<span width="29" class="txt_bold txtLeft" > 
                      									${bottomRight.component.componentName}
                      								</span>
                      								<span width="284" >
                      									<%= greenSmall%><%= yellowSmall%><%= redSmall%>
                      								</span>
                    							</div>
                    						</c:if>
                    					</c:forEach>
                						</div>
									</div>
				   					<div class="interior_inspec" style="height:215px;border-right: 2px solid #0e62af !important;border-left: 2px solid #0e62af !important;padding:0px;width:96px; padding:0 3px; float:left;">
                        				<div cellspacing="0" class="bordernone padding_reset" style="">
                        				<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          									<c:if test="${bottomRight.position.positionId eq 57}">
                            				<div style="text-align:center;margin-top:15px;">
                             				 	<h2 class="titleFont" style="text-align:center;">${bottomRight.component.componentName}</h2>
                            				</div>
                            			</c:if>
                            			</c:forEach>
                            			<div>
                            				<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          										<c:if test="${bottomRight.position.positionId eq 58 and bottomRight.component.componentName ne ''}">
                              						<span style="float:left; width:16px;margin-right:2px;">
                              							<%= redSmall%>
                              						</span>
                              						<span class="fontTxt" style="display:block; float:left;width:75px;">${bottomRight.component.componentName}</span>
                              					</c:if>
                              				</c:forEach>
                            			</div>
                             			<div class="bordernone interior_inspec" style="width:86px !important;">
                                  			<div class="beforeAfter" >
                                  				<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          											<c:if test="${bottomRight.position.positionId eq 59}">
                                    					<span style="float:left; position:relative; left:19px;">${bottomRight.component.componentName}</span>
                                    				</c:if>
                                    			</c:forEach>
                                  			</div>
                                  			<div class="beforeAfter">
                                  				<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          											<c:if test="${bottomRight.position.positionId eq 60}">
                                    					<span style="float:left;position:relative; left:28px;">${bottomRight.component.componentName}</span>
                                    				</c:if>
                                    			</c:forEach>
                                  			</div>
                                  			<div style="width:100%;" class="clear">
                                  				<div style="clear:left;width:58%;float:left;">
                                  			<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
          									<c:if test="${bottomRight.position.positionId gt 60 and bottomRight.position.positionId le 64 and bottomRight.component.componentName ne ''}">
                                  			<div style="width:100%;float:left;" class="clear">
								  				<div style="float:left;width:15px;">
                                 				<span style="position:relative;top:7px;">
                                 					<span class="txt_bold">${bottomRight.component.componentName}</span>
                                 				</span>
								 				</div>
                                 				<div style="width:30px; float:left;margin-right:3px;">
                                 					<span class="white_box"></span>
								  				</div>
                                  			</div>
                                  			</c:if>
                                  			</c:forEach>
                                  			</div>
                                  			<c:if test="${userFormComponents[59].component.componentName ne ''}">
                                  			<div style="width:40%;float:left;">
                                  			<div>
                                 				<div>
                                 					<span class="white_box_rectangle">&nbsp;</span>
								  				</div>
								  				<div>
                                 					<span class="white_box_rectangle" style="position:relative; top:2px;">&nbsp;</span>
								  				</div>
                                  			</div>
                                  			</div>
                                  			</c:if>
                                  			</div>
                                		</div>
                          				</div>
                  					</div>
									<div class="interior_inspec"  style="padding:0px;width:74px;float:left; padding:0 3px;"class="bordernone padding_reset">
										<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
                             		 		<c:if test="${bottomRight.position.positionId eq 65}">
                            					<div style="margin-top:15px;">
                              						<span>
                              							<h2 class="titleFont" style="text-align:center;">${bottomRight.component.componentName}</h2></span>
                            					</div>
                            				</c:if>
                            			</c:forEach>
                             		 	<div  class="bordernone interior_inspec" style="width:80px;">
                             		 	<c:forEach items="${bottomRightUserFormComponents}" var="bottomRight" varStatus="status"> 	
                             		 		<c:if test="${bottomRight.position.positionId gt 65 and bottomRight.position.positionId le 69 and bottomRight.component.componentName ne ''}">
                                  				<div class="clear"  style="height:20px; margin-bottom:5px;">
                                    				<span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                    				<span class="txtFont" style="vertical-align:-3px;">${bottomRight.component.componentName}</span>
                                  				</div>
                                  			</c:if>
                                  		</c:forEach>
                                		</div>
                          			</div>
              					</div>
      						</div>
      						<div class="bottomtext" style="width: 400px;overflow:hidden;">
								<span style="float: left;" id="comp_72" class="comments">${userFormComponents[72].component.componentName}</span> 
								<span style="display:block;overflow:hidden;"><hr class="cmtLine" id="cmtLine1" style="width: 400px;"/></span>
								<br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" />
								<div style="width:400px;margin-top:10px;">
									<div style="float:left;width:279px;overflow:hidden;">
										<span style="float: left;" id="comp_73" class="comments">${userFormComponents[73].component.componentName}</span> 
										<span style="display:block;overflow:hidden;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>
									</div>
									<div style="float:right;width: 120px;">
										<span style="float: left;" id="comp_74" class="comments">${userFormComponents[74].component.componentName}</span> 
										<span style="display:block;overflow:hidden;"><hr class="cmtLine" id="cmtLine3" style="width: 125px;"/></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div id="btnClick">
					<div class="containerbtm" style="padding-left:300px;">
     					<nobr> 
     						<c:if test="${user.userRole == 'user'}">
							<a style="margin-right: 5px;" href="./userFormEdit103.do?userFormId=${userForm.userFormId}&templateId=${templateId}" class="EditBtn btn-txt" onclick="launchWindow('#dialog');">Edit Template
								<span  style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
							</a> 
							</c:if>
							<a href="./GenerateUserFormPdf.do?userFormId=${userForm.userFormId}" target="_newtab" class="PdfBtn btn-txt">Generate PDF
								<span  style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
							</a> 
						</nobr>
					</div>
				</div>
				</div>
			</div>
			
		</div>
	</div>
	<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
