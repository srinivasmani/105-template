<div id="options-table">
	<div class="sideheading">
		<label>Option Name</label> <br /> <input type="text" value=""
			id="optionName" name="optionName" />
	</div>
	<div class="sideheading">
		<label>Option Type</label> <br /> 
		<select id="optionType" name="optionType">
			<option id="1" selected="selected">Squares</option>
			<option id="2">Circles</option>
			<option id="3">None</option>
		</select>
	</div>
	<div  class="sideheading" id="compOptionDesc">
		<div class="checkboxcntDiv" id = "checkboxcnt_1">
			<input type="checkbox"></input>
			<input type="text" value="" id="optionDesc_1" name="optionDesc_1" size="9" class="compOptDesc" />
			<span>
				<input type="button" name="add_1" value="" id="add_1" class="add" />
				<input type="button" name="remove_1" value="" id="remove_1" class="delete"/>
			</span>
		</div>
	</div>
	<div class="sideheading" style="text-align: center;">
		<input class="btn_forright" type="button" value="Ok" id="rightOkComp" /> 
		<input class="btn_forright" type="button" value="Cancel" id="rightCancelComp" />
	</div>
</div> 