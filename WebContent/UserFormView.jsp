<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<head>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css" />
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<title>User Form View</title>
<style>
#msg{
margin-top: 6%;
}
.bottomtext1 {
    color: #000000;
    font-family: 'MyriadProRegular';
    font-size: 11pt;
    line-height: 2;
    margin: 0 36px 0 15%;
    overflow: hidden;
    padding-top: 21px;
    width: 80%;
}
</style>
</head>
<%@ include file="header.jsp"%>
<div class="container">
	<!--Header starts-->
	<div class="header"></div>
	<!--Header Ends-->
	<!--Center Container starts-->
	 <%@ include file="WEB-INF/header/userNavigation.jsp"%>
	<div class="centercontainer">
		<!--Navigation starts-->
		<div class="nav">
			<ul>
			<c:if test="${userForm.status != 'default'}">
				<li><a href="#" id="formNameTab" class="selected">${userForm.formName}</a></li>
			</c:if>
			<c:if test="${userForm.status == 'default'}">
				<li><a href="javascript:void(0);" class="selected" onclick="launchWindow('#dialog');">Template 101</a>
				</li>
				<li><a href="./getUserComponentIfs.do" onclick="launchWindow('#dialog');">Template 102</a>
				</li>
				<li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a>
				</li>
				<li><a href="./createUserComponentForm104.do" onclick="launchWindow('#dialog');">Template 104</a>
				</li>
                                <li><a href="./createUserComponentForm105.do" onclick="launchWindow('#dialog');">Template 105</a>
				</li>
			</c:if>
			</ul>
		</div>
		<div style="clear: both"></div>
		<!--Navigation Ends-->

		<!--Innercontainer starts-->
		<div class="innercontainer" id="innercontainer">
			<div class="VCcentercontent">
				<c:if test="${userForm != null }">
					<b>${userForm.headerText}</b>
						<c:choose>
							<c:when test="${userForm.formImage eq null ||userForm.formImage eq ''}">
								<div id="imgView" align="center">
									<p id="msg">(Your logo here)<br/>Dimension:[340 x 75]pixels</p>
								</div>
							</c:when>
							<c:otherwise>
								<div id="imgViewNoBorder" align="center">
									<img src="/ImageLibrary/${userForm.formImage}" id="imageSize"  style="margin:0; max-width:336px; max-height:75px;">
								</div>
							</c:otherwise>
						</c:choose>
				</c:if>
				<c:if test="${userForm == null }">
					<b>${template.headerText}</b>
					<div id="img" align="center">
						<c:if test="${template.templateImage!=null}">
							<img src="/ImageLibrary/${template.templateImage}" id="imageSize">
						</c:if>
						<c:if test="${template.templateImage==null}">
							<p id="msg">(Your logo here)</p>
						</c:if>
					</div>
				</c:if>
				<span class="formtxt">Mileage_______________</span>
				</div>
				<div class="clear"></div>
				<!--Vc Inner container starts-->
				<div class="VCcontinner">
					<!--List left starts-->
					<div class="listcontatleft">
					<input type="hidden" name="headerColor" id="headerColor" value="${userForm.headingColor}" />
						<!-- List out first eight elements -->
						<input type="hidden" name="templateId" id="templateId"
							value="${templateId}" />
						<ul class="VClist">
							<c:forEach items="${leftUserFormComponents}" var="userFormComponent"
								varStatus="status">
								<c:if test="${userFormComponent.status.statusId == 1}">
								   <li>
									 <div id="visibleDiv${status.count}" class="ListDivVisible"></div>
									 <c:if test="${userFormComponent.status.statusId == 2}">
										<div id="visibleDiv${status.count}" class="ListDivHidden"></div>
									</c:if>
										<div class="VClistdiv">
										<span class="text"><nobr>${userFormComponent.component.componentName}</nobr></span>
										<c:if test="${(status.count != 1)  && (status.count != 3) && (status.count != 6)}">
											<span class="subText"><nobr>${userFormComponent.component.subComponents.label}</nobr></span>
										</c:if>
										<c:forEach items="${userFormComponent.component.subComponents.subComponentsOptions}"
											var="subComponentOption">
											<c:if test="${subComponentOption.status == 'true'}">
												<div class="checkboxcnt">
													<div class="checkbox">
														<span>
															<c:forEach var="option" items="${fn:split(subComponentOption.options.optionDescription, '^')}">
															<c:if test="${option != '' && option != null }">
																<img src="../images/${fn:toLowerCase(subComponentOption.optionType)}.png" >
																	  <label class="option">${option}</label>
															  </c:if>
															</c:forEach> 
														</span>
													</div>
												</div>
											</c:if>
										</c:forEach>
									</div>
									<div class="clear"></div>
								</li>
							  </c:if>
							</c:forEach>
						</ul>
					</div>
				<!--List left Ends-->

				<!--List right starts-->
				<div class="listcontatright">
						<!-- List out rest seven elements -->
						<ul class="VClist">
							<c:forEach items="${rightUserFormsComponents}" var="userFormComponentRight" varStatus="status">
							  <c:if test="${userFormComponentRight.status.statusId == 1}">
								<li>
									<div id="visibleDiv${status.count+8}" class="ListDivVisible"></div>
									<c:if test="${userFormComponentRight.status.statusId == 2}">
										<div id="visibleDiv${status.count+8}" class="ListDivHidden"></div>
									</c:if>
									<div class="VClistdiv  ">
										<span class="text"><nobr>${userFormComponentRight.component.componentName}</nobr></span>
										<c:if test="${(status.count != 5)}">
											<span class="subText"><nobr>${userFormComponentRight.component.subComponents.label}</nobr></span>
										</c:if>
										<c:forEach
											items="${userFormComponentRight.component.subComponents.subComponentsOptions}"
											var="subComponentOptionRight">
											<c:if test="${subComponentOptionRight.status == 'true'}">
												<c:choose>
						                          <c:when test="${(status.count) != 7}">
						                          <c:forEach var="option" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}" >
							                          <div class="checkboxcnt">
							                          <div class="checkbox">
						                           		 <span>
						                           		 <c:if test="${option != '' && option != null }">
															<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png" >
															<label  class="optionText">${option}</label>
															</c:if>
														</span>
						                         	 </div>
						                         	 </div>
				                 				  </c:forEach>
						                          </c:when>
						                          <c:otherwise>
					                           		 <div class="checkboxcnt">
					                           		 <span>
													 	<c:forEach var="option" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}" >
													 	<div class="checkbox15">
													 	<c:if test="${option != '' && option != null }">
													 	<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png" >
														<label class="optionText">${option}</label>
														</c:if>
														</div>
						                 				</c:forEach>
					                 				</span>
					                 				</div>
				                         		</c:otherwise>
				                         		</c:choose>
											</c:if>
										</c:forEach>
									</div>
									<div class="clear"></div>
								</li>
							  </c:if> 
							</c:forEach>
						</ul>
					</div>
			</div>
			<!--List right Ends-->
			<div class="clear"></div>
		
		<!--Vc Inner container Ends-->
		<div class=" bottomtext1">
		<span id="cmt" style="float:left;">Comments:</span> 
		<hr style="width:90%;margin-top: 3%;*margin-top:-4%;"/><br /><hr /><br /><hr /><br />
		<span id="cmt" style="float:left;clear:left;margin-top: -25px;">Inspected by: </span> 
		<hr style="width:60%;position:relative;top:-5px;float:left;" />
		<span style="float: left; margin-top: -25px; margin-left: 60.5%;">Date:</span>
 		<hr style="width:23%;position:relative;top:-5px;float:left;" />
	</div>
		<!--<div class=" editview">
				<span id="cmt" style="float:left;">Comments:</span> <span style="border-bottom:1px solid #000; display:block; float:right;width:615px;margin-top:15px;"></span>
			  <br /><span  style="border-bottom:1px solid #000; display:block; clear:left;width:100%;position:relative;top:10px;"></span>
				<br /><span  style="border-bottom:1px solid #000; display:block;clear:left;width:100%;margin-top:10px;"></span>
				<br /><span id="cmt" style="float:left;clear:left;">Inspected by: </span> <span style="border-bottom:1px solid #000; display:block; float:left;width:38%;margin-top:15px;margin-left:1%;margin-right:1%;"></span><span style="width:20%; flioat:left;">Date:</span>
 			<span style="border-bottom:1px solid #000; display:block; float:right;width:43%;margin-top:15px;"></span>
		</div><br>--><br>
	<!--Container Bottom Starts-->
		<div id="userBtn" style="padding-left:70px;overflow:auto;">
		<c:if test="${user.userRole == 'user'}">
		<a style="margin-right:8px;" href="./userFormEdit.do?userFormId=${userForm.userFormId}&templateId=${templateId}" class="EditBtn btn-txt" id="userFormEdit" onclick="launchWindow('#dialog');">
   			Edit Template
   			<span  style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
 	 	</a>  
	 	</c:if> 
  		<a href="./GenerateUserFormPdf.do?userFormId=${userForm.userFormId}&templateId=${templateId}" target="_newtab"  class="PdfBtn btn-txt">
   			Generate PDF
   			<span  style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
  		</a>
	</div>
	<!--Container Bottom Ends-->

</div>
	<!--Container Bottom Ends-->

</div>
</div>
<!--Innercontainer Ends-->
</div>
<!--Center Container Ends-->
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>

<!--container ends-->
<script type="text/javascript">
	jQuery(document).ready(function() {
		var headerColor = jQuery("#headerColor").val();
		if((headerColor == "") || (headerColor == null)){
			 jQuery("#headerColor").val("#045fb4");
		}
		else{
			 jQuery("#headerColor").val(headerColor);
			 jQuery(".text").css("color", headerColor);
		}
	});
	function launchWindow(id) {
		var divTobeCleared=document.getElementById('successMessage');
		if(divTobeCleared!=null){
			divTobeCleared.innerHTML="";
		}
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		//transition effect  
		$('#mask').fadeIn(0); 
		$('#mask').fadeTo("slow",0.4); 
	 
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
	              
		//Set the popup window to center
		
		$(id).css('left', winW/3);
		//transition effect
		$(id).fadeIn(2000);
	}
</script>
