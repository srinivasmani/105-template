<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<title>User Templates</title>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../css/style-edit.css" />
<body>
	<div class="container">
		<!--Header starts-->
		<div class="header"></div>
		<div id="centercontainer" class="centercontainer">
			<div class="nav">
				<ul>
					<li><a href="#" class="selected">Form 1</a>
					</li>
					<li><a href="#">Form 2</a>
					</li>
					<li><a href="#">Form 3</a>
					</li>
					<li><a href="#">Form 4</a>
					</li>
					<li><a href="#">Form 5</a>
					</li>
				</ul>
			</div>
			<div class="innercontainer">

				<div class="VCcentercontent">
					<b>VEHICLE CONDITION REPORT</b>
					<div class="yourlogo">( Your logo here )</div>
					<br> <br> <span class="formtxt">
						Mileage__________________________________________________________</span>
				</div>
				<div class="clear"></div>
				<!--Vc Inner container starts-->
				<div class="VCcontinner">
					<!--List left starts-->
					<div class="listcontatleft">
						<ul class="VClist">
							<li>

								<div class="VClistdiv  ">
									<span class="heading">LIGHT </span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Pass</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Fail</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>

								<div class="VClistdiv  ">
									<span class="heading">WIPER BLADES</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="VClistdiv  ">
									<span class="heading">OIL LEVEL</span> <span class="subHeading">30,0000
										Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Ok</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>1/2-1qt.
											low</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>1.qt.
											low</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="VClistdiv  ">
									<span class="heading">BRAKE FLUID</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="VClistdiv  ">
									<span class="heading">POWER STEERING FLUID</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="VClistdiv  ">
									<span class="heading">WASHER FLUID</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Ok</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Leaking</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Topped
											Off</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="VClistdiv  ">
									<span class="heading">TRANMISSION FLUID</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>
								<div class="VClistdiv  ">
									<span class="heading">BELT(s) & TENSIONER</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
						</ul>
					</div>
					<!--List left Ends-->
					<!--List right starts-->
					<div class="listcontatright">
						<ul class="VClist">
							<li>
								<div class="VClistdiv  ">
									<span class="heading">ANTIFREEZE / COOLANT</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>

								<div class="VClistdiv  ">
									<span class="heading">AIR FILTER</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>

								<div class="VClistdiv  ">
									<span class="heading">CABIN FILTER</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>

								<div class="VClistdiv  ">
									<span class="heading">FUEL FILTER</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>

								<div class="VClistdiv  ">
									<span class="heading">BATTERY CONDITION</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Pass</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Fail</label>
									</div>

								</div>
								<div class="clear"></div>
							</li>
							<li>

								<div class="VClistdiv  ">
									<span class="heading">DIFFERENTIAL FLUID</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
										</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
							<li>

								<div class="VClistdiv  ">
									<span class="heading">TIRE CONDITION</span> <span
										class="subHeading">30,0000 Miles</span>
								</div>
								<div class="checkboxcnt">
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Good
											(6&#47;32&quot; and above)</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Next
											visit (3&#47;32&quot; and 5&#47;32&quot;)</label>
									</div>
									<div class="checkbox">
										<span> <input type="checkbox" /> </span> <label>Overdue
											(2&#47;32&quot; and below)</label>
									</div>
								</div>
								<div class="clear"></div>
							</li>
						</ul>
					</div>
					<!--List right Ends-->
					<div class="clear"></div>
				</div>
				<br> <br>
				<div class="containerbtm">
					<a href="#"><input class="PdfBtn" value="" type="button" /> </a>
				</div>
				<!--Container Bottom Ends-->
			</div>
		</div>
	</div>
</body>
