<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="header.jsp"%>
<meta http-equiv="content-language" content="en-GB" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<script type="text/javascript" src="../js/popup.js"></script>
<script type="text/javascript" src="../js/image-resizing.js"></script>

<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery.template2.js"></script>
<style>
.logo1 {
	float: left;
	height: 0;
	margin-right: 3px;
	margin-top: -2px;
}
.logo2 {
    float: right;
    height: 0;
    margin-right: 40px;
    margin-top: -113px;
}
.leftOptImage {
	border: 1px solid black;
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}
.rghtOptImage {
    border: 1px solid black;
    font-size: 12px;
    height: 100px !important;
    margin-top: 0px;
    width: 100px !important;
}
.leftOptImageNoBorder {
	border: 1px solid black;
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}
.rghtOptImageNoBorder {
	border: 1px solid black;
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}
.img {
    border: 1px solid black;
    font-size: 28px;
    height: 100px !important;
    width: 380px !important;
}
.imgNoBorder {
    border: 0px solid black;
    font-size: 28px;
    height: 100px !important;
    width: 380px !important;
}
.divTable1 .editme1 {
    position: relative !important;
    top: 12px !important;
    white-space: nowrap !important;
}
#msg {
    font-size: 10pt;
    letter-spacing: 0.5px;
    margin-left: 0;
    margin-top: 12%;
}
</style>
<script>
jQuery(document).ready(function(){
	 var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
		if(is_chrome == true){
			jQuery("#customer").css("width", "760px");
			jQuery("#editComp_6_0").css("width", "760px");
			jQuery("#editComp_6_1").css("width", "760px");
			jQuery(".custSpanText").css("width", "760px");
	}
		
 });
</script>
		<div class="container">
		<%@ include file="WEB-INF/header/userNavigation.jsp"%>
		<div class="header"></div>
		<div class="centercontainer">
		<div class="nav">
			<ul id="headerList">
				<li><a href="./navigateTemplateView.do" class="notSelected" id="tem1" onclick="launchWindow('#dialog');">Template 101</a>
				</li>
				<li><a href="./navigateTemplateIfs.do" class="selected" id="tem2" onclick="launchWindow('#dialog');">Template 102</a>
				</li>
				<li><a href="./navigateTemplate103.do" class="notSelected" id="tem3" onclick="launchWindow('#dialog');">Template 103</a>
				</li>
				<li><a href="./navigateTemplate104.do" class="notSelected" id="tem4" onclick="launchWindow('#dialog');">Template 104</a>
				</li>
                                <li><a href="./navigateTemplate105.do" class="notSelected" id="tem6" onclick="launchWindow('#dialog');">Template 105</a>
				</li>
				<li><a href="./addImageToLibrary.do" class="notSelected" id="tem5" onclick="launchWindow('#dialog');">Add image</a>
				</li>
			</ul>
		</div>
		<div style="clear: both"></div>
		<div class="innercontainer" id="innercontainer">
			<%
				String getSVG = (String) request.getAttribute("imgType");
					if ( (getSVG != null) && (getSVG.equalsIgnoreCase("circle"))) {
			%>
			<% 
						getSVG = "<div class='svgTable' ><img height='41' width='41' alt='' src='../images/green_circle.png'/>"+
									"<img height='41' width='41' alt='' src='../images/Yellow_circle.png'/>"+
									"<img height='41' width='41' alt='' src='../images/red_circle.png'/></div>"; 
					}else{%>
			<% 
						getSVG = "<div class='svgTable' ><img height='41' width='41' alt='' src='../images/green.PNG'/>"+
									"<img height='41' width='41' alt='' src='../images/yellow.PNG'/>"+
									"<img height='41' width='41' alt='' src='../images/red.PNG'/></div>"; 
					}
			%>
			
			<!-- Complete Width and Height -->
			<div id="templateSize">
				<!-- User Logo -->
				<div class="divLogoTab" style="width:21.59cm;;">
					<div class="divLogoRow">
						<div class="logo1">
							<div class="leftOptImage"></div>
						</div>
						<div class="imglogo">
							<div align="center">
								<c:choose>
									<c:when test="${template.templateImage eq null || template.templateImage eq ''}">
										<div class="img" id="img">
											<p id="msg">(Your logo here)<br/>Dimension:[380 x 100]pixels</p>
										</div>
									</c:when>
									<c:otherwise>
										<div class="imgNoBorder" id="img">
											<img src="/ImageLibrary/${template.templateImage}" style="margin:0;max-width:380px !important;max-height:100px !important;"/>
										</div>
									</c:otherwise>
								</c:choose>
							</div>
						</div>						
						<div class="logo2">
							<div class="rghtOptImage"></div>
						</div>
					</div>
				</div>
				
				<!-- Page Heading -->
				<div id="pageHeading">
					<p id="heading">
						<label class="text_label" id="lblText" style = "text-transform: uppercase;">
							<nobr>
								<c:out value="${template.headerText}"/>
							</nobr>
						</label>
					</p>
					<!--<div class="edit"></div>-->																
					<div class="clear"></div>
				</div>

				<!-- Form 1 -->
				<div class="divTable" style="margin-left:23px;">
					<div class="divRow">
						<div class="divCell" id="customer" style="border:0 !important;width: 733px;margin-left: 12px;">
							<c:forEach var="cname" items="${fn:split(template.components[0].componentName, '~')}" varStatus="stat">
								<div class="custSpanText" style="width: 733px;margin:0;">
									<span id="cust${stat.count}" style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">${cname}</span>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				<div class="clear"></div>

				<!-- ATTENTION -->
				<div class="selTable">
					<div class="selRow">
						<div class="selCol">
							<div class="selCell">														
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<img  src="../images/green_circle.png" class="selcimg">
									</c:when>
									<c:otherwise>
										<img height='41' width='41' alt='' src='../images/green.PNG'/>
									</c:otherwise>
								</c:choose>
								<span>
									<p class="editme1" id="attentionP">
										<c:out value="${template.components[1].componentName}"/>
									</p>									
								</span>
								<br /> 
								<input type="image" class="color" name="clr" id="inp1" style="display: none; margin-left: 11px; margin-top: -5px;" 
									src="../images/images.png">
							</div>
						</div>
						
						<div class="selCol">
							<div class="selCell" style="width:305px;">								
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<img   src="../images/Yellow_circle.png" class="selcimg">
									</c:when>
									<c:otherwise>
										<img height='41' width='41' alt='' src='../images/yellow.PNG'/>
									</c:otherwise>
								</c:choose>
								<span id="nobr">
									<p class="editme1" id="fAttentionP">
										<c:out value="${template.components[2].componentName}"/>
									</p>																		
								</span>
								<br /> 
								<input type="image" class="color" name="clr" id="inp2" style="display: none; margin-left: 11px; margin-top: -5px;"
									src="../images/images.png">
							</div>
						</div>
						
						<div class="selCol">
							<div class="selCell">
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<img src="../images/red_circle.png" class="selcimg">
									</c:when>
									<c:otherwise>
										<img height='41' width='41' alt='' src='../images/red.PNG'/>
									</c:otherwise>
								</c:choose>
								<span id="nobr">
									<p class="editme1" id="iAttentionP">
										<c:out value="${template.components[3].componentName}"/>																	
									</p>																		
								</span>
								<br /> 
								<input type="image" class="color" name="clr" id="inp3" style="display: none; margin-left: 11px; margin-top: -5px;"
									src="../images/images.png">
							</div>
						</div>
						
					</div>
				</div>

				<!-- Items And Conditions -->
				<div class="divTable1">
					<div id="div1">
						<div class="divRow ">
						<c:forEach items="${leftheader}" var="left"> 
							<div class="divCell1 rowheading" id="content">
								<p class="editme1" id="itemsP">
									<c:out value="${left.componentName}"/>
								</p>								
							</div>
						</c:forEach>
						</div>
						<c:forEach items="${leftList}" var="leftList"> 
							<div class="divRow">
								<div class="divCell1" id="content">
									<p class="editme1" id="lightsP">
										<c:out value="${leftList.componentName}"/>									
									</p>								
								</div>
								<div class="divCell1">
									<%=getSVG%>
								</div>
							</div>
						</c:forEach>
					</div>
					
					<div id="div2">
						<div class="divRow">
							<c:forEach items="${rightheader}" var="right"> 
								<div class="divCell1 rowheading" id="content">
									<p class="editme1" id="itemsP">
										<c:out value="${right.componentName}"/>
									</p>						
								</div>
							</c:forEach>
						</div>
						<c:forEach items="${rightList}" var="rightList"> 
							<div class="divRow">
								<div class="divCell1" id="content">
									<p class="editme1" id="lightsP">
										<c:out value="${rightList.componentName}"/>									
									</p>								
								</div>
								<div class="divCell1">
									<%=getSVG%>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
		<div class="clear"></div>
			<!-- Form 2 -->
		</div>
		<div class="clear"></div>
		<div class=" bottomtext" style="width:84%;margin-left:8%;">
<div style="width:800px;height:25px;">
	<span id="cmt" style="float:left;">Comments:</span> 
	<hr style="width:90.5%;position:relative; top:18px;"/>
	</div>
	<br /><hr/><br /><hr/><br />
	<div style="width:800px;height:50px;">
	<span id="cmt" style="float:left;clear:left;margin-top: -20px;width:85px;">Inspected by: </span> 
	<hr style="width:60%;float:left;position:relative; top:0px;"/>
	<span style="float: left; margin-top: -20px;">Date:</span>
 	<hr style="width:23%;float:left;position:relative; top:0px;"/>
 	</div>
</div>
		<div class="containerbtm" style="margin: 3% 3% 3% 35%;">
			<a style="margin-right:5px;" href="./editNavTemplateIfs.do" class="EditBtn btn-txt" onclick="launchWindow('#dialog');">Edit Template
				<span  style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
			</a>
       		<a href="./GenerateTemplate2Pdf.do?templateId=${template.templateId}" target="_newtab" class="PdfBtn btn-txt">Generate PDF
       			<span  style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
       		</a>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
