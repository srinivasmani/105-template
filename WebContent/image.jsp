<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div>
	<c:choose>
		<c:when test="${type == 'center'}">
    		<img style="margin:0;max-width: 380px;max-height:100px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogo" id="saveLogo" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'center104'}">
    		<img style="margin:0;max-width: 384px;max-height:72px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogo" id="saveLogo" value="${logoName}">
  		</c:when>
                <c:when test="${type == 'center105'}">
    		<img style="margin:0;max-width: 350px;max-height:250px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogo" id="saveLogo" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'left'}">
    		<img style="margin-top: 0px; margin-left: 0px;max-width:247px;max-height:127px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoLeft" id="saveLogoLeft" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'leftLogo'}">
    		<img style="margin:0;max-width:100px;max-height:100px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoLeft1" id="saveLogoLeft1" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'leftLogo104'}">
    		<img style="margin:0;max-width:150px;max-height:100px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoLeft1" id="saveLogoLeft1" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'rightLogo'}">
    		<img style="margin:0;max-width:100px;max-height:100px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoRight" id="saveLogoRight" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'rightLogo104'}">
    		<img style="margin:0;max-width:150px;max-height:100px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoRight" id="saveLogoRight" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'bottomLeft'}">
    		<img style="margin-top: 0px; margin-left: 0px;max-width:90px;max-height:90px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoBottomLeft" id="saveLogoBottomLeft" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'bottomRight'}">
    		<img style="margin-top: 0px; margin-left: 0px;max-width:100px;max-height:200px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoBottomRight" id="saveLogoBottomRight" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'bottomLast'}">
    		<img style="margin-top: 0px; margin-left: 0px;max-width:150px;max-height:45px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoBottomLast" id="saveLogoBottomLast" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'bottomMid'}">
    		<img style="margin-top: 0px; margin-left: 0px;max-width: 40px;max-height: 36px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoBottomMid" id="saveLogoBottomMid" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'bottomCenter'}">
    		<img style="margin-top: 0px; margin-left: 0px;max-width: 205px;max-height: 196px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogoBottomCenter" id="saveLogoBottomCenter" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'template1'}">
    		<img style="max-width:336px;max-height:72px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogo" id="saveLogo" value="${logoName}">
  		</c:when>
  		<c:when test="${type == 'lbImage'}">
    		<img  src="/ImageLibrary/${logoName}" >
    		<input type="hidden" name="saveLogo" id="saveLogo" value="${logoName}">
  		</c:when>
  		<c:otherwise>
    		<img style="max-width: 384px; max-height:72px;" src="/ImageLibrary/${logoName}">
    		<input type="hidden" name="saveLogo" id="saveLogo" value="${logoName}">
    		<input type="hidden" id="smallImg" name="smallImg" value="${smallImg}" >
  		</c:otherwise>
  	</c:choose>
	<%-- <img src="../logo/${logoName}" style="margin-top:0;margin-left:0;width:10.16cm;height:2.54cm"> --%>
</div>
