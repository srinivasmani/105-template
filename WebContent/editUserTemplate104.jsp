<!DOCTYPE html>
<html>
<head>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<%@include file="UploadImage.jsp" %>
<title>User Edit Template 4</title>
<link rel="stylesheet" type="text/css" href="../css/stylesheet.css"></link>
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/Jquery-Validation.js"></script>
<script type="text/javascript" src="../js/Template104.js"></script>
<script type="text/javascript" src="../js/jquery-userEditTemplate.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<style>
.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity = 80);
	display: none;
}

.box_content {
	position: fixed;
	left: 25%;
	right: 30%;
	width: 575px;
	/*height: 30%;*/
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	border: 8px solid #ACACAC;
	background: none repeat scroll 0 0 #FFFFFF;
	border: 8px solid #ACACAC;
}
#img {
	position:relative;
	float:none;
}
#img #text {
	left: 74px;
    position: absolute;
    top: 7px;
    width: 300px; 
}
 #repositoryPopUp, #leftLogoRepositoryPopUp, #rightLogoRepositoryPopUp{
height:auto !important;
border:8px solid #acacac !important;
width:625px !important;
margin-top:-65px;
}
#item .odd, #item .even {
border: 1px solid #ACACAC !important;
margin-right:5px;
margin-bottom:5px;
}
.selectLink {
    left: 36px !important;
    position: relative;
}
#commentDiv {
    font-family: 'MyriadProRegular',Sans-Serif;
    font-size: 11pt;
    letter-spacing: 0.5px;
}
</style>
</head>
<body>
<div class="container" style="padding:0px;">
<%@ include file="WEB-INF/header/userNavigation.jsp"%>
<div id="centercontainer" class="centercontainer">
	<div class="nav">
		 <ul>
			<c:if test="${userForm.status != 'default'}">
				<li><a href="javascript:void(0)" class="selected" id="formNameTab">${userForm.formName}</a></li>
			</c:if>
			<c:if test="${userForm.status == 'default'}">
				<li><a href="./navigateTemplateView.do" onclick="launchWindow('#dialog');">Template 101</a>
				</li>
				<li><a href="./getUserComponentIfs.do" onclick="launchWindow('#dialog');">Template 102</a>
				</li>
				<li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a>
				</li>
				<li><a href="./createUserComponentForm104.do" class="selected" onclick="launchWindow('#dialog');">Template 104</a>
				</li>
                                <li><a href="./createUserComponentForm105.do" onclick="launchWindow('#dialog');">Template 105</a>
				</li>
			</c:if>
		</ul>
	</div>
<form id="updateAdminTemplate104" name="updateAdminTemplate104" action="./updateUserFormByFormId104.do" method="post">
<input type="hidden" id="imgType" value="${imgType}">
<input type = "hidden" name = "saveImage" id="saveImage104" value = "${userForm.formImage}"> 
<input type= "hidden" name="saveLeftImage" id="saveLeftImage104" value="${userForm.formLeftImage}"/>
<input type= "hidden" name="saveRightImage" id="saveRightImage104" value="${userForm.formRightImage}"/>

<div style="clear: both"></div>
  <div class="innercontainer">
  <div style="text-align:center; padding-top:10px; padding-bottom:10px;">
   <span class="circlesquare">Form Name</span> : <input type="text" name="formName" id="formName" maxlength="20" value="${userForm.formName}"/>
  </div>
	<div id="templateSize">
        <div style="width: 21.5cm;" class="divLogoTab">
			 <div class="divLogoRow">
				<div class="logo1">
					<div class="leftOptImage103" id="logoLeft104">
						 <c:if test="${userForm.formLeftImage !=null && userForm.formLeftImage != ''}">	
							<img id="imageSize103" src="/ImageLibrary/${userForm.formLeftImage}" style="margin:0;max-width:150px; max-height:100px;" />
						</c:if>
					</div><br />
					
					<a href="javascript:void(0);" id="uploadImage1" onclick="centerImageSelectionPop('leftLogo104',150,100); return false;" style="display:inline;margin-top:10px; font-family: 'MyriadProRegular';font-size: 11pt;white-space: nowrap;position:relative; left:-14px; top:-15px;">Click to upload optional logo</a> &nbsp;&nbsp;<span class="imgDimension" style="position:relative; top:-13px;left:10px;">[150 x 100]</span>
					
				</div>
				<div id="imgLogo" align="center" class="imgLogo">
				<c:choose>
				<c:when test="${userForm.formImage!=null && userForm.formImage !=''}">
							<img src="/ImageLibrary/${userForm.formImage}" style="max-width:384px; max-height:72px;margin:0;" />
				</c:when>
				<c:otherwise>
					<p id="msg">(Your logo here)<br/>Dimension:[380 x 75]pixels</p>
				</c:otherwise>
				</c:choose>
				</div>
				<div style="width:300px;margin:0 auto;clear:both;text-align:center;">
			   	 <a href="javascript: void(0);" id="centerImageClick" onclick="centerImageSelectionPop('center104',384,72); return false;" style="height: 0; font-family: 'MyriadProRegular';font-size: 11pt;">Click to upload logo</a> &nbsp;&nbsp;<span class="imgDimension">[380 x 75]</span>
			   	</div>
			   
				<div class="logo2-103" style="margin-top:-97px;">
					<div class="rghtOptImage103" id="logoRight104">
						 <c:if test="${userForm.formRightImage !=null && userForm.formRightImage !=''}">	
							<img id="imageSize103" src="/ImageLibrary/${userForm.formRightImage}" style="max-width:150px; max-height:100px; margin:0;" />
						</c:if>
					</div>
					<a href="javascript:void(0);" id="uploadImage2" onclick="centerImageSelectionPop('rightLogo104',150,100); return false;" style="display:block; width: 97px;white-space: nowrap;margin-top:0px; font-family: 'MyriadProRegular';font-size: 11pt;position: relative;left:-14px;">Click to upload optional logo</a> &nbsp;&nbsp;<span class="imgDimension" style="position:relative; left:20px;">[150 x 100]</span>
				</div>
			 </div>
		</div>
 
    
    
    <input type="hidden" name="formId" value="${userForm.userFormId}"/>
   
    <div id="pageHeading" style="clear:both; margin-top:20px;">
	 <p id="heading">
		<label id="lblText" class="text_label" /><nobr>
				<span id="cmt" style="float:left;clear:left;">
					<input type="text" class="inputBorder"  name="headerText" id="headerText"  value="${userForm.headerText}"  onkeypress="return limitOfCharHeader(event,this.id,25)" 
					  size="100" disabled="disabled"  maxlength="30" style="width: 620px; height: 30px; font-weight: bold; text-align: center; text-transform: uppercase;" onblur="removeEditHeader('headerText', 25)"   />
				</span> 
			   
				<span style="float: right;" class="handSymbol">
						<img src="../images/ieditover.PNG"   onclick="editTextHeader('headerText')" id="editButtonheaderText"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditHeader('headerText', 25)" id="okButtonHeader" style="display: none;"  />
		
	 </p>
	 <br/>
	 <!--<div class="edit"></div>-->																
	 <div class="clear"></div>
	</div>
	
	<div class="divTable">
	 <div class="divRow">
		<div id="customer" class="divCell" style="margin-left:25px;">
		 <div  id="customerP" class="inspec_formp moreWidth"  style="border: 1px solid #000;padding:0 0 !important;width:755px;">
		   <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[0].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[0].component.componentName}"/>
			</c:if>
				<span align="center" class="th">
						<input type="hidden" name="componentId0" value="${newUserFormComponents[0].component.componentId}" />
						<input type="hidden" name="component0" id="component0"  value="" />
					  <c:set var="cname" value=" "/>
					  <c:forEach var="cname" items="${fn:split(newUserFormComponents[0].component.componentName, '~')}" varStatus="stat">
					  <c:if test="${stat.count == 1}">
					  		<div>
					      	 <input type="text" class="inputBorder"  name="component_0${stat.count}" id="component_0${stat.count}"  value="${cname!='' ? fn:trim(cname) :''}"   onkeypress="return limitnofotextCustomer(this.id,event)"    
					      		onblur="comp01FocusOut();" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false"  disabled="disabled"  size="100" style="width:753px;display:block;padding:5px 0; "   />
					   	     </div>
			   	     </c:if>
			   	     <c:if test="${stat.count == 2}">
					  		<div>
					      	 <input type="text" class="inputBorder"  name="component_0${stat.count}" id="component_0${stat.count}"  value="${cname!='' ? fn:trim(cname) :''}"   onkeypress="return limitnofotextCustomer(this.id,event)"    
					   	        onblur="comp02FocusOut();"  onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" disabled="disabled"  size="100" style="width:753px;display:block;padding:5px 0; "   />
					   	     </div>
			   	     </c:if>
						</c:forEach>
					   </span>
			</div>
		
				<span style="float: rightposition:relative; left:20px; top:-80px;" class="handSymbol">
					<img src="../images/ieditover.PNG"  onclick="editTextCustomer('component')" id="editButton0"/>
					<img src="../images/ieditOk.PNG"  onclick="removeEditCustomer('component',250)" id="okButton0" style="display: none;"  />
		  		</span>												
		</div>						
	 </div>
	</div>
	<div id="commentDiv">
		<span id="temp1"></span>
	</div>
								
	<div class="clear"></div>
	<div class="selTable">
	<div style="padding:15px 25px;">
		<%       
		        String getSVG = (String) request.getAttribute("imgType");
		 		String circle = "";
		 		String square = "";
				if (getSVG.equalsIgnoreCase("circle")) 
					circle= "checked";
				else
					square = "checked";
	    %>
	       <span class="circleSuare">
			<input type="radio" name="imgType" value="circle" <%= circle %> />Circle
			</span>
			<span class="circleSuare">
				<input type="radio" name="imgType" value="square"  <%= square %>/>Square
			</span>
	</div>


	 <div class="selRow">
		<div class="selCol selCol3">
		  <div class="selCell">	
		  	<span><b class="green"></b></span>
			
			
			<!--component edit and delete start -->
			
			<span class="floatLeft">			 
			<span id="divTxt_lights"  >
			
			
			 <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[1].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[1].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width4">
					 <input type="hidden" name="componentId1" value="${newUserFormComponents[1].component.componentId}" />
						<input type="text"    class="inputBorder"  name="component1" id="component1"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
						  size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component1', 20)" />
					</span>
					 
			</span>
				<span style="float: right;" class="handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component1')" id="editButton1"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component1',20)"   id="okButton1" style="display: none;"  />
					 
				</span>
			</span>
			
		 <br /> <input type="image" src="../images/images.png" style="display: none; margin-left: 11px; margin-top: -5px;" id="inp1" name="clr" class="color" />
		  </div>
		</div>
		<div class="selCol selCol3">
		  <div class="selCell">								
			<span><b class="yellow"></b></span>
			<span id="nobr" class="floatLeft">
			
			<span class="floatLeft">			 
			<span id="divTxt_lights"  >
			<c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[2].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[2].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width4">
					 <input type="hidden" name="componentId2" value="${newUserFormComponents[2].component.componentId}" />
						<input type="text" class="inputBorder"  name="component2" id="component2"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
						  size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component2', 20)"  />
					</span>
					 
			</span>
				<span style="float: right;" class="handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component2')" id="editButton2"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component2', 20)" id="okButton2" style="display: none;"  />
					 
				</span>
			</span>
			 </span>
			 <br/> <input type="image" src="../images/images.png" style="display: none; margin-left: 11px; margin-top: -5px;" id="inp2" name="clr" class="color" />
		  </div>
		</div>
		<div class="selCol selCol3">
		  <div class="selCell">
			<span><b class="red"></b></span>
			<span id="nobr" class="floatLeft">
			
			<span class="floatLeft">			 
			<span id="divTxt_lights"  >
			<c:if test="${newUserFormComponents[3].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[3].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width4">
					 <input type="hidden" name="componentId3" value="${newUserFormComponents[3].component.componentId}" />
						<input type="text" class="inputBorder"  name="component3" id="component3"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
						  size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component3', 20)" />
					</span>
					 
			</span>
				<span style="float: right;" class="handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component3')" id="editButton3"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component3',20)" id="okButton3" style="display: none;"  />
					 
				</span>
			</span>
			 																		
			</span>
			<br /> <input type="image" src="../images/images.png" style="display: none; margin-left: 11px; margin-top: -5px;" id="inp3" name="clr" class="color" />
		  </div>
		</div>
						
	 </div>
	</div>
	<div class="clear"></div>
  
    <div class="divTable1 paddingLeft">
     <div  class="inspectionleftwrap">
      <div class="inspection_bg">
        <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
        
        <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[4].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[4].component.componentName}"/>
			</c:if>
					<span align="center" class="th">
					<input type="hidden" name="componentId4" value="${newUserFormComponents[4].component.componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component4" id="component4"  value="${componentDesc}"  
						onkeypress="return limitOfCharForMandatory(event,this.id, 40)" maxlength="30" disabled = "disabled" size="40" onblur="removeEditMandatory('component4', 30)" />
					</span>
			 
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component4')" id="editButton4"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component4',40)" id="okButton4" style="display: none;"  />
            </span>
        </div>
        <div class="clear row1" style="text-align:center;padding:3px;">
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[5].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[5].component.componentName}"/>
			</c:if>
					 <span class="smallheading" style="float:left; width:150px;display:block; text-align:center; margin-left:115px;">
					 <input type="hidden" name="componentId5" value="${newUserFormComponents[5].component.componentId}" />
						<input type="text" class="inputBorder"  name="component5" id="component5"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)"  
						  size="40" disabled="disabled" maxlength="40" onblur="removeEditMandatory('component5', 40)" />
					</span>
			 
				<span class="EditBtnNew handSymbol"  style="float:right;">
						<img src="../images/ieditover.PNG"  onclick="editText('component5')" id="editButton5"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component5',40)" id="okButton5" style="display: none;"  />
				</span>
      
        </div>
        <!--<div class="clear row1">
          <span  class="alignCenter"><img src="../images/car_3view.png" alt="" width="244" height="196" /></span>
        </div>
        -->
        <div class="clear row1" id="img">
			<div id="imgLeft" align="center" class="imgLeft" style="height:210px;margin-top:12px;">
                <span class="alignCenter" style="display:block; height:210px;margin-top:12px;">
					<img src="/ImageLibrary/${newUserFormComponents[70].component.componentName}" alt="" style="max-width:244px; max-height:192px;" />
				</span>
			</div>
			<div id="text" id="text" style="width:375px; text-align:center; top:0px;left:0px;">
				<a href="javascript:void(0);" id="leftImageClick" onclick="centerImageSelectionPop('left',244,192); return false;">Change Image</a>[245 x 190]
    		</div>
		</div>
        <div class="clear row1">
          <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[6].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[6].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId6" value="${newUserFormComponents[6].component.componentId}" />
						<input type="text" class="inputBorder"  name="component6" id="component6"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component6', 35)"/>
					</span>	  
					<span class="EditBtnNew handSymbol">
							<img src="../images/ieditover.PNG"  onclick="editText('component6')" id="editButton6"/>
							<img src="../images/ieditOk.PNG"  onclick="removeEdit('component6',35)" id="okButton6" style="display: none;"  />
						 
					</span>
					
					 
			 
			
					 
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
        <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[7].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[7].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId7" value="${newUserFormComponents[7].component.componentId}" />
						<input type="text" class="inputBorder"  name="component7" id="component7"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)"  
						  size="29" disabled="disabled" maxlength="31" onblur="removeEdit('component7', 31)"/>
					</span>	  
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component7')" id="editButton7"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component7',31)" id="okButton7" style="display: none;"  />
					 
					</span>
					
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
        <span>
       <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[8].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[8].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId8" value="${newUserFormComponents[8].component.componentId}" />
						<input type="text" class="inputBorder"  name="component8" id="component8"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,31)" 
						  size="29" disabled="disabled" maxlength="31" onblur="removeEdit('component8', 31)"/>
					</span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component8')" id="editButton8"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component8',31)" id="okButton8" style="display: none;"  />
					 
					</span>
					 
			</span>
			 
        
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[9].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[9].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId9" value="${newUserFormComponents[9].component.componentId}" />
						<input type="text" class="inputBorder"  name="component9" id="component9"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled"  maxlength="35" onblur="removeEdit('component9', 35)"/>
					</span>	  
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component9')" id="editButton9"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component9',35)" id="okButton9" style="display: none;"  />
					 
					</span>
					
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
      </div>
	  <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[10].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[10].component.componentName}"/>
			</c:if>
					 <span align="center" class="th">
					 <input type="hidden" name="componentId10" value="${newUserFormComponents[10].component.componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component10" id="component10"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,30)" 
						  size="29" disabled="disabled"  maxlength="30" onblur="removeEditMandatory('component10', 30)"/>
					</span>
					 
			 
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component10')" id="editButton10"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component10',40)" id="okButton10" style="display: none;"  />
              </span>
        </div>
		    <div class="clear row1">
          <span style="width:245px; float:left;padding:15px 0;">
          
          
        <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[11].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[11].component.componentName}"/>
			</c:if>
					 <span class="textFont" style="display:block; float:left;margin-top:-5px;">
					 <input type="hidden" name="componentId11" value="${newUserFormComponents[11].component.componentId}" />
						<input type="text" class="inputBorder"  name="component11" id="component11"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEditMandatory('component11', 35)" style="width:185px;" />
					</span>	
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component11')" id="editButton11"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component11',35)" id="okButton11" style="display: none;"  />
				 </span>
					 
			</span>
			
				
             <div style="clear:both;padding:15px 5px 0 40px;;">
             <span>
             	<b class="green" ></b><b class="yellow" ></b><b class="red" ></b>
             </span>
			 </div>
		 
          <!--<div>
             <div style="width:120px; float:left;position:relative;left:100px;top:-30px;"><img src="../images/battery.png" width="125" height="111" /> </div>
		   </div>
        -->
         <div id="img">
			<div id="imgBottomLeft" align="center" class="imgBottomLeft" style="float: right;margin-top: -31px;;width: 115px;height:95px;position:relative; top:-23px; left:-20px;">
				<img src="/ImageLibrary/${newUserFormComponents[71].component.componentName}" alt="battery" style="max-width:115px;max-height: 90px;" id="image"/>
			</div>
			<div id="text" style="position:relative; top:40px;left: 137px;width: 210px;">
    			<a href="javascript:void(0);" id="bottomLeftImageClick" onclick="centerImageSelectionPop('bottemLeft',115,90); return false;" style="display:inline;margin-top:10px;">Change Image</a>[115 x 90]
			</div>
		</div>
        </div>
      </div>
      
	  <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[12].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[12].component.componentName}"/>
			</c:if>
					 <span align="center" class="th">
					 <input type="hidden" name="componentId12" value="${newUserFormComponents[12].component.componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component12" id="component12"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)" 
						  size="29" disabled="disabled"  maxlength="30" onblur="removeEditMandatory('component12', 40)"/>
					</span>
					 
			 
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component12')" id="editButton12"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component12',40)" id="okButton12" style="display: none;"  />
				</span>
        </div>
        <div class="clear row1">
          <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[13].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[13].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId13" value="${newUserFormComponents[13].component.componentId}" />
						<input type="text" class="inputBorder"  name="component13" id="component13"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component13', 35)"/>
					</span>	  
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component13')" id="editButton13"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component13',35)" id="okButton13" style="display: none;"  />
					 
					</span>
					
 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[14].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[14].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId14" value="${newUserFormComponents[14].component.componentId}" />
						<input type="text" class="inputBorder"  name="component14" id="component14"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component14', 35)" />
						</span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component14')" id="editButton14"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component14',35)" id="okButton14" style="display: none;"  />
					 
					</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[15].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[15].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId15" value="${newUserFormComponents[15].component.componentId}" />
						<input type="text" class="inputBorder"  name="component15" id="component15"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled"  maxlength="35" onblur="removeEdit('component15', 35)"/>
						</span>
						<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component15')" id="editButton15"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component15',35)" id="okButton15" style="display: none;"  />
					</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[16].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[16].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId16" value="${newUserFormComponents[16].component.componentId}" />
						<input type="text" class="inputBorder"  name="component16" id="component16"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component16', 35)"/>
						  </span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component16')" id="editButton16"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component16',35)" id="okButton16" style="display: none;"  />
					 
					</span>
			</span>
			
				 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[17].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[17].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId17" value="${newUserFormComponents[17].component.componentId}" />
						<input type="text" class="inputBorder"  name="component17" id="component17"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component17', 35)" />
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component17')" id="editButton17"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component17',35)" id="okButton17" style="display: none;"  />
					 
					</span>
					 
			</span>
				
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[18].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[18].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId18" value="${newUserFormComponents[18].component.componentId}" />
						<input type="text" class="inputBorder"  name="component18" id="component18"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled"  maxlength="35" onblur="removeEdit('component18', 35)"/>
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component18')" id="editButton18"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component18',35)" id="okButton18" style="display: none;"  />
					 
					</span>
					 
			</span>
				
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[19].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[19].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId19" value="${newUserFormComponents[19].component.componentId}" />
						<input type="text" class="inputBorder"  name="component19" id="component19"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component19', 35)"/>
						  </span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component19')" id="editButton19"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component19',35)" id="okButton19" style="display: none;"  />
					 
					</span>
					 
			</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
      </div>
	   <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
        
        
        
        
         <c:set var="componentDesc" value=""/>
         <span>
			<c:if test="${newUserFormComponents[20].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[20].component.componentName}"/>
			</c:if>
					 <span align="center" class="th">
					 <input type="hidden" name="componentId20" value="${newUserFormComponents[20].component.componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component20" id="component20"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)" 
						  size="29" disabled="disabled" maxlength="30" onblur="removeEditMandatory('component20', 40)" />
					</span>
					 
			</span>
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component20')" id="editButton20"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component20',40)" id="okButton20" style="display: none;"  />
                </span>
        
         
        </div>
        <div class="clear row1">
        
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[21].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[21].component.componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId21" value="${newUserFormComponents[21].component.componentId}" />
						<!--<input type="text" class="inputBorder"  name="component21" id="component21"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" />
						   -->
						   <textarea rows="1" cols="10"  class="inputBorder"  name="component21" id="component21"    onkeypress="return limitnofotext(event,this.id,55)" 
						    disabled="disabled" style="height: 42px;" onblur="removeEdit('component21', 55)" maxlength="55">${componentDesc}</textarea> 
						  	</span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component21')" id="editButton21"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component21',55)" id="okButton21" style="display: none;"  />
					 
					</span>
					 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[22].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[22].component.componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId22" value="${newUserFormComponents[22].component.componentId}" />
						<input type="text" class="inputBorder"  name="component22" id="component22"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component22', 35)"/>
						  	</span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component22')" id="editButton22"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component22',35)" id="okButton22" style="display: none;"  />
					 
					</span>
		 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[23].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[23].component.componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId23" value="${newUserFormComponents[23].component.componentId}" />
						<input type="text" class="inputBorder"  name="component23" id="component23"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled"  maxlength="35" onblur="removeEdit('component23', 35)"/>
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component23')" id="editButton23"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component23',35)" id="okButton23" style="display: none;"  />
					 
				</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[24].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[24].component.componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId24" value="${newUserFormComponents[24].component.componentId}" />
						<!--<input type="text" class="inputBorder"  name="component24" id="component24"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" />
						   --><textarea rows="1" cols="10"  class="inputBorder"  name="component24" id="component24"   onkeypress="return limitnofotext(event,this.id,41)" 
						   disabled="disabled" style="height: 42px;" onblur="removeEdit('component24', 41)" maxlength="41">${componentDesc}</textarea> 
						  </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component24')" id="editButton24"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component24',35)" id="okButton24" style="display: none;"  />
					 
				</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">

         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[25].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[25].component.componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId25" value="${newUserFormComponents[25].component.componentId}" />
						<input type="text" class="inputBorder"  name="component25" id="component25"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component25', 35)"/>
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component25')" id="editButton25"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component25',35)" id="okButton25" style="display: none;"  />
					 
				</span>
					 
			 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
	  </div>
      
      
      
      
      <div class="clear"></div>
	  
    </div>
  </div>
  <div  class="inspectionrightwrap">
    <div class="inspection_bg">
     
      <div class="inspectionTable" style="border-bottom:1px solid #000;">
        <div class="clear row1 row1Title greyBg">
         <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[26].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[26].component.componentName}"/>
			</c:if>
					 <span  align="center" class="th">
					  <input type="hidden" name="componentId26" value="${newUserFormComponents[26].component.componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component26" id="component26"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,30)" 
						  size="29" disabled="disabled" maxlength="30" />
					</span>
					 
			
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editMultipleText(26,55)" id="editButton26"/>
						<img src="../images/ieditOk.PNG"  onclick="removeMultipleEdit(26,55)" id="okButton26" style="display: none;"  />
						</span>
        </div>
       
        <div class="clear row1" style="border-bottom:0px;">
          <div style="padding:0px; border:0px;height:130px" >
          <div class="bordernone interior_inspec">
              <div class="alignCenter clear paddingBottom" style="width:360px;">
                <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[27].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[27].component.componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <span><h2 class="noInspec">
					    <input type="hidden" name="componentId27" value="${newUserFormComponents[27].component.componentId}" />
						<input type="text" class="inputBorder" style="text-align: center;"  name="component27" id="component27"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="50" disabled="disabled" maxlength="35"/>
						  	<span>
						</span>
					  </h2></span>
					</span>
              
              </div>
              <div class="clear paddingBottom" style="width:385px; height:30px; position:relative; top:10px;">
                <span  style="width:131px; float:left;">
                <span style="display:block; width:25px;float:left;"><b class="green" ></b></span>
                <c:set var="componentDesc" value=""/>
				<c:if test="${newUserFormComponents[28].status.statusId == 1}">
					<c:set var="componentDesc" value="${newUserFormComponents[28].component.componentName}"/>
				</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width150" style="width:96px; float:left;">
					  <span class="fontF fontF4">
					   <input type="hidden" name="componentId28" value="${newUserFormComponents[28].component.componentId}" /> 
						<input type="text" class="inputBorder"  name="component28" id="component28"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="15" disabled="disabled"  maxlength="16"/>
				
					   </span>
					</span>
			</span>
			
				
                
              
              
                <span style="width:127px; float:left;">
                 <span style="display:block; width:25px;float:left;"><b class="yellow" ></b></span>
                
                  <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[29].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[29].component.componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxt4 leftAlignTxt width150" style="width:90px; float:left;">
					  <span class="fontF fontF4">
					   <input type="hidden" name="componentId29" value="${newUserFormComponents[29].component.componentId}" />
						<input type="text" class="inputBorder"  name="component29" id="component29"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="15" disabled="disabled" maxlength="16" />
						  	<span>
						 
				    </span>
					   </span>
					</span>
					 
			</span>
			
			
                
                
                
            
              
                <span style="width:125px; float:left;">
                 <span style="display:block; width:25px;float:left;"><b class="red" ></b></span>
                  <c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[30].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[30].component.componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxt4 leftAlignTxt width150" style="width:92px; float:left;">
					  <span class="fontF fontF4">
					   <input type="hidden" name="componentId30" value="${newUserFormComponents[30].component.componentId}" />
						<input type="text" class="inputBorder"  name="component30" id="component30"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="15" disabled="disabled"  maxlength="14"/>
							<span>
						 
				</span>
					  </span>
					</span>
					 
			</span>
			
				
                
            
              </div>
              <div class="clear" style="padding-top:15px;">
                <div class="alignCenter" style="width:350px; overflow:auto; padding-left:30px;">
				 <div class="bordernone interior_inspec interior_inspecLeft" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                  
                      <span class="txt_bold" style="float:left;margin-right:4px;width:25px;">
                      
                      <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[31].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[31].component.componentName}"/>
						</c:if>
					     <input type="hidden" name="componentId31" value="${newUserFormComponents[31].component.componentId}" />
						 <input type="text" class="inputBorder"  name="component31" id="component31"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  onkeyup="removeNextText(this.id, 32);" size="5" disabled="disabled" maxlength="2"/>
						  	<span style="position:relative; left:-15px;">
					  </span>
			       </span>
			         
                  
                      <span id="imgGYR3132"   ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"></b></span>
                    
                      <span class="txt_bold">
                      <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[32].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[32].component.componentName}"/>
						</c:if>
					      <input type="hidden" name="componentId32" value="${newUserFormComponents[32].component.componentId}" />
						<input type="text" class="inputBorder"  name="component32" id="component32"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id, 8)" 
							onkeyup="removeNextText(this.id, 31);"  size="5" disabled="disabled" maxlength="8" />
						  	<span>
						 
				</span>
			</span>
			
                      
                      
                  
                  </div>
                  <div   class="bordernone interior_inspec interior_inspecRight" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                      <span class="txt_bold"  style="float:left;margin-right:4px;width:25px;">
                       <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[33].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[33].component.componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId33" value="${newUserFormComponents[33].component.componentId}" />
						<input type="text" class="inputBorder"  name="component33" id="component33"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  onkeyup="removeNextText(this.id, 34);" size="5"  maxlength="2" disabled="disabled" />
						  	<span style="position:relative; left:-15px;">
						 </span>
					   </span>
			    
		              <span id="imgGYR3334"  ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
		                  <span class="txt_bold">
		                     <c:set var="componentDesc" value=""/>
								<c:if test="${newUserFormComponents[34].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[34].component.componentName}"/>
								</c:if>
							   <input type="hidden" name="componentId34" value="${newUserFormComponents[34].component.componentId}" />
								<input type="text" class="inputBorder"  name="component34" id="component34"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,8)" 
								  onkeyup="removeNextText(this.id, 33);" size="5" disabled="disabled"  maxlength="8" />
								  	<span>
						</span>
					   </span>
                  </div>
                  <div  class="bordernone interior_inspec interior_inspecLeft" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                      <span class="txt_bold" style="float:left;margin-right:4px;width:25px;">
	                      <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[35].status.statusId == 1}">
								<c:set var="componentDesc" value="${newUserFormComponents[35].component.componentName}"/>
							</c:if>
						   <input type="hidden" name="componentId35" value="${newUserFormComponents[35].component.componentId}" />
							<input type="text" class="inputBorder"  name="component35" id="component35"  value="${componentDesc}" onkeypress="return limitnofotext(event,this.id,3)" 
							onkeyup="removeNextText(this.id, 36);"    maxlength="2" size="5" disabled="disabled" />
						  	<span style="position:relative; left:-15px;"></span>
				  	 </span>
			     
                    <span id="imgGYR3536"  ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    <span class="txt_bold" >
		                    <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[36].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[36].component.componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId36" value="${newUserFormComponents[36].component.componentId}" />
						<input type="text" class="inputBorder"  name="component36" id="component36"  value='${componentDesc}' onkeypress="return limitnofotext(event,this.id,8)" 
							 onkeyup="removeNextText(this.id, 35);"   maxlength="8" size="5" disabled="disabled" />
						  	<span></span>
			   		</span>
                  </div>
                  <div  class="bordernone interior_inspec interior_inspecRight" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                  <span class="txt_bold" style="float:left;margin-right:4px;width:25px;">
                  <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[37].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[37].component.componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId37" value="${newUserFormComponents[37].component.componentId}" />
						<input type="text" class="inputBorder"  name="component37" id="component37"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
							onkeyup="removeNextText(this.id, 38);"  maxlength="2" size="5" disabled="disabled" />
						  	<span style="position:relative; left:-15px;">
						 
				</span>
			   </span>
			     
                     <span id="imgGYR3738"   ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                      <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[38].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[38].component.componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId38" value="${newUserFormComponents[38].component.componentId}" />
						<input type="text" class="inputBorder"  name="component38" id="component38"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,8)" 
						  onkeyup="removeNextText(this.id, 37);"  maxlength="8" size="5" disabled="disabled" />
						  	<span>
				</span>
                    
                  </div>
				 </div>
              </div>
            </div></div>
        </div>
   
          
        <div style="clear:left;background-color:#dddddf; height:238px;" >
			  <div style="float:left; width:200px;">
                <!--<span style="padding:0px;width:100px;float:left;">
				 <img src="../images/rght_tire.png" width="100" height="170" />
				</span>
                -->
                <div style="padding:0px;width:100px;float:left; height:170px;">
					<div id="imgBottomRight" align="center" class="imgBottomRight" style="/*margin-top: -27%;*/width:96px;height:200px;">
						<img  src="/ImageLibrary/${newUserFormComponents[72].component.componentName}" alt="rght_tire" style="max-width:100px;max-height:170px;" id="image"/>
					</div>
					<a href="javascript:void(0);" id="bottomRightImageClick" onclick="centerImageSelectionPop('bottemRight',100,170); return false;" style="display:inline;margin-top:10px;">Change Image</a>[100 x 170]
				</div> 
                <div  class="bordernone interior_inspec padding_reset" style="padding:0px;float:right; padding-top:15px; width:100px;">
                    <div>
                    
                     <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[39].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[39].component.componentName}"/>
						</c:if>
					  <span style="padding:0px;"><h2 style="text-align:center;">
					   <input type="hidden" name="componentId39" value="${newUserFormComponents[39].component.componentId}" />
						<!--<input type="text" class="inputBorder tireComponentHeader"  name="component39" id="component39"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
						  size="15" disabled="disabled" style="width:150px;" />
						  -->
						 <textarea rows="2" cols="10" class="inputBorder tireComponentHeader" name="component39" id="component39" onkeypress="return limitnofotext(event,this.id,39)"
								  disabled="disabled" style="width:100px;"  maxlength="20">${componentDesc}</textarea>   
					</h2><span>
					
						 </span></span>
                    </div>
                   <div class="clear" style="width:100px;margin-bottom:10px;">
                     <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[40].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[40].component.componentName}"/>
						</c:if>
					  <span style="padding:0px;"><h2>
					   <input type="hidden" name="componentId40" value="${newUserFormComponents[40].component.componentId}" />
						<input type="text" class="inputBorder tireComponentHeader"  name="component40" id="component40"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled"  maxlength="2"/>
						  	<span style="position:relative;top:3px;margin-right:10px;">
						 
				</span>
					</h2></span>
                     <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    
                  </div>
                    <div class="clear" style="width:100px;margin-bottom:10px;">
                    
                     <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[41].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[41].component.componentName}"/>
						</c:if>
					  	<span> <h2>
					  	 <input type="hidden" name="componentId41" value="${newUserFormComponents[41].component.componentId}" /> 
						<input type="text" class="inputBorder tireComponentHeader"  name="component41" id="component41"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled"  maxlength="2" />
						  <span style="position:relative;top:3px;margin-right:10px;">
						 
				</span></h2>
					 </span>
			  
                    
                     <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
                   
                    <div class="clear" style="width:100px;margin-bottom:10px;">
                    
                    
                    
                    
                     <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[42].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[42].component.componentName}"/>
						</c:if>
						<span>
					  	<h2>
					  	 <input type="hidden" name="componentId42" value="${newUserFormComponents[42].component.componentId}" />  
						<input type="text" class="inputBorder tireComponentHeader"  name="component42" id="component42"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled" maxlength="2"/>
					 
					 	<span style="position:relative;top:3px;margin-right:10px;">
						 
				</span></h2>
			   </span>
                     <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
              
                    <div class="clear" style="width:100px;">
                      <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[43].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[43].component.componentName}"/>
						</c:if>
						<span>
					  	 <h2>
					  	 <input type="hidden" name="componentId43" value="${newUserFormComponents[43].component.componentId}" /> 
						<input type="text" class="inputBorder tireComponentHeader"  name="component43" id="component43"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled" maxlength="2"/>
					
					 	<span style="position:relative;top:3px;margin-right:10px;">
						 
				</span></h2>
			   </span>
                      <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
					
                    </div>
                </div>
				</div>
				   <div  style="height:238px;border-right: 2px solid #0e62af !important;border-left: 2px solid #0e62af !important;padding:0px;width:96px; padding:0 3px; float:left;">
                        <div cellspacing="0" class="bordernone padding_reset" style="">
                            <div style="margin-top:15px;">
                            
                             <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[44].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[44].component.componentName}"/>
						</c:if>
						<span>
					 <h2 class="titleFont" style="text-align:center;padding-bottom:0px;">
					  <input type="hidden" name="componentId44" value="${newUserFormComponents[44].component.componentId}" /> 
						<!--<input type="text" class="inputBorder tireComponentHeader"  name="component44" id="component44"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
						  size="12" disabled="disabled" />
						  -->
						   <textarea rows="2" cols="10" class="inputBorder tireComponentHeader" name="component44" id="component44"  onkeypress="return limitnofotext(event,this.id,19)"
								  disabled="disabled" maxlength="12">${componentDesc}</textarea>   
						  <span>
						 
				</span>
					</h2> 
			   </span>
                         </div>
                            <div style="padding-left:15px;width:80px;">
                              <span style="float:left; width:22px;margin-right:5px; position:relative; top:10px;display:block;"><b class="smallRed" ></b></span>
                              <!--<span class="fontTxt" style="display:block; float:left;width:65px;text-align:center;">
                              	<img src="../images/symbol.png" alt="" width="40" height="36" />
                              </span>
                              
                              -->
                              <div style="display:block; float:left;width:40px;">
								<div id="imgBottomMid" class="imgBottomMid" style="width:40px; height:36px;">
									<img  src="/ImageLibrary/${newUserFormComponents[73].component.componentName}" alt="symbol" style="max-width:40px;max-height: 36px;" id="image"/>
								</div>
								<a href="javascript:void(0);" id="bottomMidImageClick" onclick="centerImageSelectionPop('bottomMid',40,36); return false;" style="display:inline;margin-top:4px;position:relative;left:-15px;font-style:normal!important;;">Change Image</a><br/><span style="font-size:10px;">[40 x 36]</span>
							</div>
                            </div>
                             <div class="bordernone interior_inspec">
                                  <div class="beforeAfter" style="text-align:leftt;width:85px;">
                              <span  style="display:block; width:38px;float:left;">    
                             <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[45].status.statusId == 1}">
							<c:set var="componentDesc" value="${newUserFormComponents[45].component.componentName}"/>
						</c:if>
					<span>
					 <input type="hidden" name="componentId45" value="${newUserFormComponents[45].component.componentId}" /> 
						<input type="text" class="inputBorder tireComponentHeader"  name="component45" id="component45"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,10)" 
						  size="8" disabled="disabled" maxlength="10"/>
					</span> 
						<span>
								 
						</span>
					   </span>
					   
					 
						
						 
                        <span style="display:block; width:38px;float:left;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[46].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[46].component.componentName}"/>
							</c:if>
							<span>
							 <input type="hidden" name="componentId46" value="${newUserFormComponents[46].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component46" id="component46"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,10)" 
								  size="12" disabled="disabled" style="left:12px !important;" maxlength="10"/>
							</span>
								 
					   </span>
                      </div>
                      <div style="width:90px;clear:both;"  class="clear">
					  <div  style="float:left;width:50px;">
                 
                      <span style="width:88px;clear:both; display:block;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[47].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[47].component.componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId47" value="${newUserFormComponents[47].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component47" id="component47"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2"/>
							</span> 
								 
						<span class="white_box" style="float:left;display:block;">&nbsp;</span>
						
					   </span>
                                   
                               
								 <span  style="position:relative;top:0px;">
								 <!-- <span style="width:88px; margin-top:27px;clear:both; display:block;">    -->
								  <span style="width:88px; clear:both; display:block;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[48].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[48].component.componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId48" value="${newUserFormComponents[48].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component48" id="component48"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2"/>
							</span> 
								 
						 <span class="white_box" style="float:left;display:block;">&nbsp;</span>
					   </span>
							</span>
								 </div>
                                 <span class="white_box_rectangle" style="float:left;display:block;width:30px;">&nbsp;</span>
                                  </div>
								  <div style="width:90px;clear:both;"  class="clear">
								  <div  style="float:left;width:50px;">
                               
                                 <span style="width:88px;clear:both; display:block;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[49].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[49].component.componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId49" value="${newUserFormComponents[49].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component49" id="component49"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2"/>
							</span>
								 
						 <span class="white_box" style="float:left;display:block;">&nbsp;</span>
					   </span>
                    
					 
						<span style="width:88px; margin-top:0px;clear:both; display:block;">    
                           <c:set var="componentDesc" value=""/>
						<c:if test="${newUserFormComponents[50].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[50].component.componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId50" value="${newUserFormComponents[50].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component50" id="component50"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2" />
							</span> 
						 <span class="white_box" style="float:left;display:block;">&nbsp;</span>
					   </span>
					
				 </div>
                                  <span class="white_box_rectangle" style="float:left;display:block;width:30px;">&nbsp;</span>   
                                  </div>
                              
                             
                               
                                </div>
                            
                          </div>
                  </div>
             
             
                
				<div style="padding:0px;width:74px;float:left; padding:0 3px;" class="bordernone padding_reset">
                            <div style="margin-top:15px;">
		                        <span>    
		                             <c:set var="componentDesc" value=""/>
									<c:if test="${newUserFormComponents[51].status.statusId == 1}">
											<c:set var="componentDesc" value="${newUserFormComponents[51].component.componentName}"/>
									</c:if>
									<span><h2 class="titleFont" style="text-align:center;">
									 <input type="hidden" name="componentId51" value="${newUserFormComponents[51].component.componentId}" /> 
										<!--<input type="text" class="inputBorder componentHeader"  name="component51" id="component51"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
										  size="12" disabled="disabled" />
										  	-->
										  	<textarea rows="3" cols="10" class="inputBorder tireComponentHeader" name="component51" id="component51"  onkeypress="return limitnofotext(event,this.id,29)"
										  disabled="disabled" maxlength="29">${componentDesc}</textarea>   
										  	<span></span>
										  </h2>
									</span> 
							   </span>
                            </div>
                            <div  class="bordernone interior_inspec">
                                  <div class="clear" style="margin-bottom:10px;">
                                    <span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                    <span>    
			                             <c:set var="componentDesc" value=""/>
										<c:if test="${newUserFormComponents[52].status.statusId == 1}">
												<c:set var="componentDesc" value="${newUserFormComponents[52].component.componentName}"/>
										</c:if>
										<span class="txtFont" style="text-align:left;vertical-align:-2px;">
										 <input type="hidden" name="componentId52" value="${newUserFormComponents[52].component.componentId}" /> 
											<input type="text" class="inputBorder componentHeader"  name="component52" id="component52"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
											  size="12" disabled="disabled" maxlength="10" />
										</span>
								   </span>
                                  </div>
                                  <div class="clear" style="margin-bottom:10px;">
                                    <span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                     <span>    
			                             <c:set var="componentDesc" value=""/>
										<c:if test="${newUserFormComponents[53].status.statusId == 1}">
												<c:set var="componentDesc" value="${newUserFormComponents[53].component.componentName}"/>
										</c:if>
										<span class="txtFont" style="text-align:left;vertical-align:-2px;">
										 <input type="hidden" name="componentId53" value="${newUserFormComponents[53].component.componentId}" /> 
											<input type="text" class="inputBorder componentHeader"  name="component53" id="component53"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
											  size="12" disabled="disabled"  maxlength="10"/>
										</span>
											 
								   </span>
			                     </div>
                                  <div class="clear" style="margin-bottom:10px;">
                                    <span><span class="white_box_square" style="float:left;margin-right:5px;"></span> </span>
                                     <span>    
			                             <c:set var="componentDesc" value=""/>
										<c:if test="${newUserFormComponents[54].status.statusId == 1}">
												<c:set var="componentDesc" value="${newUserFormComponents[54].component.componentName}"/>
										</c:if>
										<span class="txtFont" style="text-align:left;vertical-align:-2px;">
										 <input type="hidden" name="componentId54" value="${newUserFormComponents[54].component.componentId}" /> 
											<input type="text" class="inputBorder componentHeader"  name="component54" id="component54"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
											  size="12" disabled="disabled" maxlength="10"/>
										</span> 
											 
								   </span>
                                  </div>
                                   <div class="clear">
                                    <span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                     <span>    
			                             <c:set var="componentDesc" value=""/>
										<c:if test="${newUserFormComponents[55].status.statusId == 1}">
												<c:set var="componentDesc" value="${newUserFormComponents[55].component.componentName}"/>
										</c:if>
										<span class="txtFont" style="text-align:left;vertical-align:-2px;">
										 <input type="hidden" name="componentId55" value="${newUserFormComponents[55].component.componentId}" /> 
											<input type="text" class="inputBorder componentHeader"  name="component55" id="component55"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
											  size="12" disabled="disabled" maxlength="10"/>
										</span> 
								   </span>
			                      </div>
                     		</div>
                 	</div>
              </div>
      </div>
	  <div class="inspectionTable inspectionTableBg" style="border-bottom:1px solid #000;clear:both; position:relative;">
        <div class="clear row1 row1Title greyBg">
          <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[56].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[56].component.componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId56" value="${newUserFormComponents[56].component.componentId}" /> 
								<input type="text" class="inputBorder componentHeader"  name="component56" id="component56"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,40)" 
								  size="29" disabled="disabled" maxlength="30" />
							</span> 
					  
						<span class="handSymbol">
								<img src="../images/ieditover.PNG"  onclick="editMultipleText(56,66)" id="editButton56"/>
								<img src="../images/ieditOk.PNG"  onclick="removeMultipleEdit(56,66)" id="okButton56" style="display: none;"  />
						 	</span>		  
        </div>
       
        <div class="clear row1" style="border-bottom:0px;">
          <div style="padding:0px; border:0px;height:155px;width:161px; float:left;" >
          <div class="bordernone interior_inspec">
              <div class="alignCenter clear paddingBottom" style="width:360px;">
 				  <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[57].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[57].component.componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId57" value="${newUserFormComponents[57].component.componentId}" /> 
								<input type="text" class="inputBorder"  name="component57" id="component57"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled" maxlength="30"/>
							</span> 
								 
						
              </div>
              <div class="clear paddingBottom" style="width:180px; height:30px;">
                <span class="clear"><b class="green" ></b>
                 <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[58].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[58].component.componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId58" value="${newUserFormComponents[58].component.componentId}" /> 
								 <!--<input type="text" class="inputBorder"  name="component58" id="component58"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled" /> --> 
								  <textarea rows="2" cols="15" class="inputBorder" id="component58" name="component58" onkeypress="return limitnofotext(event,this.id,39)" 
								  disabled="disabled"  style="height:42px;width:150px;" maxlength="36">${componentDesc}</textarea>
							 </span>
								 
					   </span>
					
						
                <br />
             
                <span class="clear"><b class="yellow" ></b>
                  <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[59].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[59].component.componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId59" value="${newUserFormComponents[59].component.componentId}" /> 
								<!-- <input type="text" class="inputBorder"  name="component59" id="component59"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled" />-->
								    <textarea rows="3" cols="15" class="inputBorder" id="component59" name="component59" onkeypress="return limitnofotext(event,this.id,39)"
								  disabled="disabled" style="height:42px;width:150px;" maxlength="36">${componentDesc}</textarea>
							</span> 
								 
					   </span>
                <br />
                <span class="clear"><b class="red" ></b>
                  <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[60].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[60].component.componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId60" value="${newUserFormComponents[60].component.componentId}" /> 
								<!-- <input type="text" class="inputBorder"  name="component60" id="component60"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled" /> -->
								<textarea rows="3" cols="15" class="inputBorder" id="component60" name="component60" onkeypress="return limitnofotext(event,this.id,39)"
								  disabled="disabled" style="height:60px;width:150px;" maxlength="36">${componentDesc}</textarea>   
							</span> 
					   </span>
              </div>
            
    
              </div>
            </div></div>
                		<div  id="img" style="float:left; width:205px; height:196px; position:absolute; top:75px; margin-left:180px; ">
					<div id="imgBottomCenter" align="center" class="imgBottomCenter" style=" position:relative;">
               		 	<span style=" position:relative;">
							<img  src="/ImageLibrary/${newUserFormComponents[74].component.componentName}" style="position:relative;top:20px;max-height: 196px;max-width: 205px; " alt="" />
						</span>
					</div>
					<div id="text">
						<a href="javascript:void(0);" id="bottomCenterImageClick" onclick="centerImageSelectionPop('bottomCenter',205,196); return false;" style="display:inline;margin-top:10px;">Change Image</a>[205 x 195]
    				</div>
				</div>
                    <div style="background-color:#dddddf; height:210px; ">
                <div  class="bordernone interior_inspec padding_reset" style="padding:15px 30px;">
                    <div class="clear">
                    <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[61].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[61].component.componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="width:30px;margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId61" value="${newUserFormComponents[61].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component61" id="component61"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2"/>
							 
							</strong></span> 
						
                      <span style="float:left;"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
                   
                    <div class="clear">
                    
                    
                    
                     <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[62].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[62].component.componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="width:30px;margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId62" value="${newUserFormComponents[62].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component62" id="component62"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2"/>
								 
							</strong></span> 
					   
					
						
                     
                      <span style="float:left;"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
                  
                    <div class="clear">
                     <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[63].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[63].component.componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="clear:left;width:30px; margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId63" value="${newUserFormComponents[63].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component63" id="component63"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2"/>
								  
							</strong></span> 
					   
						
						
                      <span style="float:left;"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
              
                    <div class="clear">
                     <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[64].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[64].component.componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="width:30px; clear:left; margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId64" value="${newUserFormComponents[64].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component64" id="component64"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled" maxlength="2" />
							</strong>
							 
							</span> 
					   
						
						
                      <span style="float: left"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
					
                    </div>
					
					 <div class="clear" style="width:400px;">
					 
					 
					  <c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[65].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[65].component.componentName}"/>
							</c:if>
							<span class="fontF" style="float:left;display:block; margin-left:-23px;"> <strong>
							 <input type="hidden" name="componentId65" value="${newUserFormComponents[65].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component65" id="component65"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="29" disabled="disabled" style="width:83px;margin-top:15px;" maxlength="10" />
							</strong>
					 	<!--<span>
					  		<img src="../images/brands.png" alt="" width="173" height="45" style="float:left;" />
					  	</span>
					  	-->
					  	</span>
					  	<span style="padding:0px;width:150px;float:left;display:block;margin-right:5px;margin-left:5px;">
							<div id="imgBottomLast" align="center" class="imgBottomLast" style="margin-top:10px;width: 150px;">
								<img src="/ImageLibrary/${newUserFormComponents[75].component.componentName}" alt="brands" style="max-height: 45px;max-width: 150px;" id="image"/>
							</div>
							<a href="javascript:void(0);" id="bottomLastImageClick" onclick="centerImageSelectionPop('bottomLast',150,45); return false;" style="display:inline;margin-top:10px;position:relative;top:-4px;">Change Image</a>[150 x 45]
						</span>
						
						<c:set var="componentDesc" value=""/>
							<c:if test="${newUserFormComponents[66].status.statusId == 1}">
									<c:set var="componentDesc" value="${newUserFormComponents[66].component.componentName}"/>
							</c:if>
							<span class="fontF" style="float:left;display:block"> <strong>
							 <input type="hidden" name="componentId66" value="${newUserFormComponents[66].component.componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component66" id="component66"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="29" disabled="disabled" style="width:115px;margin-top:15px;"  maxlength="12"/>
							</strong>
							</span>
                       <span class="fontF" style="float:left;"> </span>					   
					 </div>
                </div>
			</div>
        </div>
   				<!--<div>
                	<img src = "/ImageLibrary/inspect_Brakes.png"></img>
                </div>
          		-->
      
      
          </div>
		  <div class="inspectionTable" style="clear:both;padding-top:0;">
		<div class="clear row1Title greyBg" style="margin:0px;">
        <c:set var="componentDesc" value=""/>
		<c:if test="${newUserFormComponents[67].status.statusId == 1}">
			<c:set var="componentDesc" value="${newUserFormComponents[67].component.componentName}"/>
		</c:if>
		<span style="display:block;width:345px; float:left;">
			<span  id="component67Span" class="comments commentTitle">${componentDesc}</span> 
				<input type="hidden" name="componentId67" value="${newUserFormComponents[67].component.componentId}" /> 
				<input type="text" class="inputBorder componentHeader"  name="component67" id="component67"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,30)" 
					size="40" disabled="disabled" maxlength="30" style="display: none;margin-left: 40px;"/>
				</span>
				<span style="diaplay:block; width:20px;float:left;" class="handSymbol">	
					<img src="../images/ieditover.PNG"  onclick="editMultipleTextLast(67,69)" id="editButton67" style="float: right;;"/>
			<img src="../images/ieditOk.PNG"  onclick="removeMultipleEditLast(67,69)" id="okButton67" style="display: none;float: right;"  />
			 </span>
			
        </div>
        <div class="clear row1" style="height:27px;">
        </div>
         <div class="clear row1" style="height:27px;">
        </div>
       <div class="clear row1" style="height:27px;">
        </div>
		 <div class="clear row1" style="height:27px;">
        </div>
	 <div class="clear row1" style="height:27px;">
        </div>
        <div class="clear row1" style="height:27px;">
        </div>
		
      </div>
      <div class="bottomtext" style="width: 393px;overflow:hidden;">
		<div style="width:400px;margin-top:10px;">
			<c:set var="componentDesc" value=""/>
			<c:if test="${newUserFormComponents[68].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[68].component.componentName}"/>
			</c:if>
			<div style="float:left;width:279px;overflow:hidden;">
				<span id="cmt" style="float: left;">
				<span  id="component68Span" class="comments">${componentDesc}</span> 
					<input type="hidden" name="componentId68" value="${newUserFormComponents[68].component.componentId}" /> 
					<input type="text" class="inputBorder"  name="component68" id="component68"  value="${componentDesc}"  
					size="15" disabled="disabled" style="width: 90px;display: none;"    maxlength="15"/>
				</span>
				<span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>  
				<span style="display:block; width:20px; float:left;margin-top:5px;">
	  				<c:set var="componentDesc" value=""/>
				</span>
			</div> 
			<c:if test="${newUserFormComponents[69].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[69].component.componentName}"/>
			</c:if>
			<div style="float:right;width: 120px;">
				<span id="component69Span" class="comments" style="float: left;">${componentDesc}</span> 
				<span style="float: left;">
					<input type="hidden" name="componentId69" value="${newUserFormComponents[69].component.componentId}" /> 
					<input type="text" class="inputBorder" name="component69" id="component69"  value="${componentDesc}"   
						size="15" disabled="disabled" style="width:40px;display: none;" maxlength="10"/>
				</span>
				<span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine3" style="width: 125px"/></span>
				<span style="display:block; width:20px; float:left;margin-top:5px;"></span> 
	    	 </div> 
	     </div>
	</div>
					   <!--Component Id for Image -->
					    <input type="hidden" name="componentId70" value="${newUserFormComponents[70].component.componentId}" />
					    <input type="hidden" name="componentId71" value="${newUserFormComponents[71].component.componentId}" />
					    <input type="hidden" name="componentId72" value="${newUserFormComponents[72].component.componentId}" />
					    <input type="hidden" name="componentId73" value="${newUserFormComponents[73].component.componentId}" />
					    <input type="hidden" name="componentId74" value="${newUserFormComponents[74].component.componentId}" />
					    <input type="hidden" name="componentId75" value="${newUserFormComponents[75].component.componentId}" />
 </div>
  </div>
    <div class="clear"></div>
  <div style="text-align:center;margin-top:10px;padding-bottom:10px;"><a href="javascript:void(0);" name="save" value="Save" id="update102"  onclick="updateTemplate104('./updateUserFormByFormId104.do');" />Save</a>
<c:choose>
  <c:when test="${userForm.status=='active'}">
    <a  href="./getUserFormByFormId104.do?formId=${userForm.userFormId}"  id="cancelBtn" onclick="launchWindow('#dialog');">Cancel</a>
  </c:when>
  <c:otherwise>
	<a  href="./createUserComponentForm104.do" id="cancelBtn" onclick="launchWindow('#dialog');">Cancel</a>
 </c:otherwise>

</c:choose>

 </div>
  <div class="clear"></div>
   <div style="text-align:right;font-family:'MyriadProRegular'; font-size:6pt;padding:0 20px 10px 0;"></div>
   </div>
  </div>
  
 </form>
  </div>
  
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>

<script type="text/javascript">
$('document').ready(function () {
    
    if($("input[disabled='disabled']")){
    	$("input[disabled='disabled']").each(function(){$(this).attr('readonly','readonly');});
    	$("input[disabled='disabled']").each(function(){$(this).removeAttr('disabled');}); 
    	
    }
    if($("textarea[disabled='disabled']")){
    	$("textarea[disabled='disabled']").each(function(){$(this).attr('readonly','readonly');});
    	$("textarea[disabled='disabled']").each(function(){$(this).removeAttr('disabled');}); 
    	
    }
   
    $('textarea[maxlength]').live('keypress blur', function() {
        // Store the maxlength and value of the field.
        var maxlength = $(this).attr('maxlength');
        var val = $(this).val();

        // Trim the field if it has content over the maxlength.
        if (val.length > maxlength) {
            $(this).val(val.slice(0, maxlength));
        }
    });
});

</script>
</body>
</html>
