<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<meta http-equiv="content-language" content="en-GB" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link type="text/css" rel="stylesheet" href="../css/jquery.miniColors.css" />
<!-- Java Script -->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jscolor.js"></script>
<script type="text/javascript" src="../js/jquery.ui.js"></script>
<script type="text/javascript" src="../js/jquery.template3.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/easy-editable-text.js"></script>
<script type="text/javascript" src="../js/jquery.editinplace.js"></script>

<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jscolor.js"></script>
<script type="text/javascript" src="../js/jquery-editTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery-adminEditTemplate.js"></script>
<style>
.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity = 80);
	display: none;
}

.box_content {
	position: fixed;
	top: 25%;
	left: 25%;
	right: 30%;
	width: 40%;
	height: 50%;
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	border: 5px solid #ACACAC;
	background: none repeat scroll 0 0 #FFFFFF;
}

#msg {
	font-size: 10pt;
	margin-left: 35%;
	margin-top: 4%;
	letter-spacing: 0.5px;
}
</style>
	<div id="img">
		<a href="javascript: void(0)" onclick="imageCenterSelectionPop('center'); return false;">
			<img src="/ImageLibrary/${logoName}" style="max-height: 2.54cm;max-width:10.16cm;">
		</a>
		<input type= "hidden" name="saveLogo" id="saveLogo" value="${logoName}">
	</div>
