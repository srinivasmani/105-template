/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
        
//        config.toolbar = [
//	                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
//	                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
//	                { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source'] },
//	                '/',
//	                { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },	              	
//	              	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
//	              	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
//	              	{ name: 'insert', items: [ 'Table', 'HorizontalRule', 'SpecialChar' ] },	              	
//	              	
//	              	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ ] }
//	              ];
        config.removePlugins = 'resize';
        config.removeDialogTabs='image:advanced';
        config.extraPlugins = 'scayt';
        config.scayt_autoStartup = true;                  
        config.extraPlugins = 'lineheight';    
        config.line_height="10px;20px;";
        config.toolbar = [
                ['Styles', 'Format', 'Font', 'FontSize'],
                '/',
                ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript','Superscript','RemoveFormat','-','Undo', 'Redo', '-', 'Outdent', 'Indent','-','NumberedList', 'BulletedList','-','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                '/',
                ['Image', 'Table', '-', 'TextColor','HorizontalRule', 'SpecialChar','Source','SpellChecker']
            ];   
};

CKEDITOR.on('dialogDefinition', function( ev ) {
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;

  if(dialogName === 'table') {
    var infoTab = dialogDefinition.getContents('info');
    var border = infoTab.get('txtBorder');
    border['default'] = "0";
    var rows = infoTab.get('txtRows');
    rows['default'] = "1";
  }
});