<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<head>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<meta http-equiv="content-language" content="en-GB" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery.template2.js"></script>
<script type="text/javascript" src="../js/image-resizing.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<style>
.logo1 {
	float: left;
	height: 0;
	margin-right: 3px;
	margin-top: -2px;
}

.logo2 {
	float: right;
	height: 0;
	margin-right: 3px;
	margin-top: -113px;
}

.leftOptImage {
	border: 0px solid black;
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}

.rghtOptImage {
	border: px solid black;
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}

.leftOptImageNoBorder {
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}

.rghtOptImageNoBorder {
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}

.imgNoBorder {
	border: 0px solid black;
	font-size: 28px;
	height: 100px !important;
	width: 380px !important;
}

.divTable1 .editme1 {
	position: relative !important;
	top: 12px !important;
	white-space: nowrap !important;
}

.divTable1 .editme {
	position: relative !important;
	top: 12px !important;
	white-space: nowrap !important;
}

.divTable1 #engineP {
	position: relative !important;
	top: 12px !important;
	white-space: nowrap !important;
}

.img {
	border: 1px solid black;
	font-size: 28px;
	width: 380px !important;
	height: 100px !important;
}

#msg {
	font-size: 10pt;
	letter-spacing: 0.5px;
	margin-left: 0;
	margin-top: 12%;
}
</style>
<script>
jQuery(document).ready(function(){
	 var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
		if(is_chrome == true){
			jQuery("#customer").css("width", "760px");
			jQuery("#editComp_6_0").css("width", "760px");
			jQuery("#editComp_6_1").css("width", "760px");
			jQuery(".custSpanText").css("width", "760px");
	}
 });
</script>
</head>
<%@ include file="header.jsp"%>
<div class="container"><!--Header starts-->
<div class="header"></div>
<%@ include file="WEB-INF/header/userNavigation.jsp"%>
<div id="centercontainer" class="centercontainer">
<div class="nav">
<ul>
	<c:if test="${userForm.status ne 'default'}">
		<li><a href="#" class="selected" id="formNameTab">${userForm.formName}</a></li>
	</c:if>
	<c:if test="${userForm.status eq 'default'}">
		<li><a href="./navigateTemplateView.do" onclick="launchWindow('#dialog');">Template 101</a></li>
		<li><a href="./getUserComponentIfs.do" class="selected" onclick="launchWindow('#dialog');">Template 102</a></li>
		<li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a></li>
		<li><a href="./createUserComponentForm104.do" onclick="launchWindow('#dialog');">Template 104</a></li>
                <li><a href="./createUserComponentForm105.do" onclick="launchWindow('#dialog');">Template 105</a></li>
	</c:if>
</ul>
</div>

<div class="innercontainer">
<%
	String getSVG = (String) request.getAttribute("imgType");
	if (getSVG.equalsIgnoreCase("circle")) {
%> <%
 	getSVG = "<div class='svgTable' ><img height='41' width='41' alt='' src='../images/green_circle.png'/>"
 				+ "<img height='41' width='41' alt='' src='../images/Yellow_circle.png'/>"
 				+ "<img height='41' width='41' alt='' src='../images/red_circle.png'/></div>";
 	} else {
 %> <%
 	getSVG = "<div class='svgTable' ><img height='41' width='41' alt='' src='../images/green.PNG'/>"
 				+ "<img height='41' width='41' alt='' src='../images/yellow.PNG'/>"
 				+ "<img height='41' width='41' alt='' src='../images/red.PNG'/></div>";
 	}
 %> <!-- Complete Width and Height -->
<div id="templateSize"><!-- User Logo -->
	<div class="divLogoTab" style="width: 20.8cm;">
		<div class="divLogoRow">
			<div class="logo1">
				<c:choose>
					<c:when test="${userForm.formLeftImage ne null and userForm.formLeftImage ne ''}">
						<div class="leftOptImage">
							<img src="/ImageLibrary/${userForm.formLeftImage}" id="imageSize" style="margin:0;max-width:100px; max-height:100px;" />
						</div>
					</c:when>
					<c:otherwise>
						<div class="leftOptImageNoBorder"></div>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="imglogo">
				<div align="center">
					<c:choose>
						<c:when test="${userForm.formImage eq null or userForm.formImage eq ''}">
							<div class="img" id="img">
								<p id="msg">(Your logo here)<br/>Dimension:[380 x 100]pixels</p>
							</div>
						</c:when>
						<c:otherwise>
							<div class="imgNoBorder" id="img">
								<img src="/ImageLibrary/${userForm.formImage}" style="max-width:380px; max-height:100px;margin:0;" />
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<div class="logo2">
				<c:choose>
					<c:when test="${userForm.formRightImage ne null and userForm.formRightImage ne ''}">
						<div class="rghtOptImage" style="border: none">
							<img src="/ImageLibrary/${userForm.formRightImage}" id="imageSize" style="margin:0;max-width:100px; max-height:100px;" />
						</div>
					</c:when>
					<c:otherwise>
						<div class="rghtOptImageNoBorder"></div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<!-- Page Heading -->
	<div id="pageHeading">
		<p id="heading">
			<label class="text_label" id="lblText" style="text-transform: uppercase;"> 
				<nobr><c:out value="${userForm.headerText}" /> </nobr> 
			</label>
		</p>
		<div class="clear"></div>
	</div>
	<!-- Form 1 -->
	<div class="divTable" style="margin-left:24px;">
	<div class="divRow">
		<div class="divCell" id="customer" style="border: 0px solid #ACACAC;margin-left: 0px;padding: 0px;width: 763px;">
			<c:forEach var="cname" items="${fn:split(newUserFormComponents[0].component.componentName, '~')}" varStatus="stat">
				<div class="custSpanText" style="width: 763px;">
					<span id="cust${stat.count}" style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">${cname}</span>
				</div>
			</c:forEach>
		</div>
	</div>
	</div>
	<!-- ATTENTION -->
	<div class="selTable">
		<div class="selRow">
			<div class="selCol">
				<div class="selCell">
					<c:choose>
						<c:when test="${userForm.formImgType eq 'circle'}">
							<img src="../images/green_circle.png" class="selcimg">
						</c:when>
						<c:otherwise>
							<img height='41' width='41' alt='' src='../images/green.PNG' />
						</c:otherwise>
					</c:choose> <nobr>
					<p style="position: relative; top: 10px;width:210px;" class="editme1" id="setVal(this.id, 'divTxt_attention','txtVal_attention');">
						<c:out value="${newUserFormComponents[1].component.componentName}" />
					</p>
					</nobr> <br />
					<input type="image" class="color" name="clr" id="inp1" style="display: none; margin-left: 11px; margin-top: -5px;" src="../images/images.png">
				</div>
			</div>
			<div class="selCol">
				<div class="selCell">
					<c:choose>
						<c:when test="${userForm.formImgType eq 'circle'}">
							<img src="../images/Yellow_circle.png" class="selcimg">
						</c:when>
						<c:otherwise>
							<img height='41' width='41' alt='' src='../images/yellow.PNG' />
						</c:otherwise>
					</c:choose> 
					<nobr id="nobr">
						<p style="position: relative; top: 10px;width:260px;" class="editme1" id="setVal(this.id, 'divTxt_fAttention','txtVal_fAttention');">
							<c:out value="${newUserFormComponents[2].component.componentName}" />
						</p>
					</nobr> <br />
					<input type="image" class="color" name="clr" id="inp2" style="display: none; margin-left: 11px; margin-top: -5px;" src="../images/images.png">
				</div>
			</div>
			<div class="selCol">
				<div class="selCell">
					<c:choose>
						<c:when test="${userForm.formImgType eq 'circle'}">
							<img src="../images/red_circle.png" class="selcimg">
						</c:when>
						<c:otherwise>
							<img height='41' width='41' alt='' src='../images/red.PNG' />
						</c:otherwise>
					</c:choose> 
					<nobr id="nobr">
						<p style="position: relative; top: 10px;width:200px;" class="editme1" id="setVal(this.id, 'divTxt_iAttention','txtVal_iAttention');">
							<c:out value="${newUserFormComponents[3].component.componentName}" />
						</p>
					</nobr> <br />
					<input type="image" class="color" name="clr" id="inp3" style="display: none; margin-left: 11px; margin-top: -5px;" src="../images/images.png">
				</div>
			</div>
		</div>
	</div>
	<!-- Items And Conditions -->
	<div class="divTable1">
		<div id="div1">
			<div class="divRow">
				<c:forEach items="${leftheader}" var="left"> 
					<div class="divCell1 rowheading" id="content">
						<p class="editme1" id="itemsP">
							<c:out value="${left.component.componentName}"/>
						</p>								
					</div>
				</c:forEach>
			</div>
			<c:forEach items="${leftList}" var="leftList"> 
				<div class="divRow">
					<div class="divCell1" id="content">
						<p class="editme1" id="lightsP">
							<c:out value="${leftList.component.componentName}"/>									
						</p>								
					</div>
					<div class="divCell1">
						<%=getSVG%>
					</div>
				</div>
			</c:forEach>
		</div>
		<div id="div2">
			<div class="divRow">
				<c:forEach items="${rightheader}" var="right"> 
					<div class="divCell1 rowheading" id="content">
						<p class="editme1" id="itemsP">
							<c:out value="${right.component.componentName}"/>
						</p>								
					</div>
				</c:forEach>
			</div>
			<c:forEach items="${rightList}" var="rightList"> 
				<div class="divRow">
					<div class="divCell1" id="content">
						<p class="editme1" id="lightsP">
							<c:out value="${rightList.component.componentName}"/>									
						</p>								
					</div>
					<div class="divCell1">
						<%=getSVG%>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class=" bottomtext" style="width:84%;margin-left:8%;">
<div style="width:800px;height:25px;">
	<span id="cmt" style="float:left;">Comments:</span> 
	<hr style="width:90.5%;position:relative; top:18px;"/>
	</div>
	<br /><hr/><br /><hr/><br />
	<div style="width:800px;height:50px;">
	<span id="cmt" style="float:left;clear:left;margin-top: -20px;width:85px;">Inspected by: </span> 
	<hr style="width:60%;float:left;position:relative; top:0px;"/>
	<span style="float: left; margin-top: -20px;">Date:</span>
 	<hr style="width:23%;float:left;position:relative; top:0px;"/>
 	</div>
</div>
<div class="containerbtm" style="margin: 3% 3% 10% 35%;"><nobr>
	<c:if test="${user.userRole eq 'user'}">
			<a href="./userEditTemplateIfs.do?userFormId=${userForm.userFormId}" class="EditBtn btn-txt" onclick="launchWindow('#dialog');">Edit Template 
				<span style="position: relative; left: 8px; top: 6px">
					<img src="../images/editimg.png">
				</span> 
			</a>
	</c:if> 
	<a href="./GenerateUserFormPdf.do?userFormId=${userForm.userFormId}" target="_newtab" class="PdfBtn btn-txt"> Generate PDF 
		<span style="position: relative; left: 15px; top: 3px">
			<img src="../images/pdfimg.png">
		</span> 
	</a> </nobr>
</div>
</div><!--Inner Container Ends-->
</div><!--Center Container Ends-->
</div><!--Container Ends-->
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif" /></div></div><div id="mask"></div>
