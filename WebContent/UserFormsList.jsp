<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<div>
	<c:forEach var="form" items="${userForms}" varStatus="status">
		<c:if test="${form.status=='active'}">
		<div style="border-bottom:1px solid #000; overflow:auto;" id="formLine${status.count}">
			<div id="sno">${status.count}</div>
			<div id="name_${status.count}" class="name">${form.formName}</div>
			<div id="master">
				<c:if test="${form.templateId == 1}">
					101
				</c:if>
				<c:if test="${form.templateId == 2}">
					102
				</c:if>
				<c:if test="${form.templateId == 3}">
					103
				</c:if>
				<c:if test="${form.templateId == 4}">
					104
				</c:if>
                                <c:if test="${form.templateId == 5}">
					105
				</c:if>        
			</div>
			<div id="action">
				<c:if test="${form.templateId == 1}">
					<a href="../user/userFormView.do?formid=${form.userFormId}" id="formLink" class="btn-txt formlink1" style="position:relative; top:5px;">View</a>
				</c:if>
				<c:if test="${form.templateId == 2}">
					<a href="../user/getUserViewComponentIfs.do?formid=${form.userFormId}" id="formLink" class="btn-txt formlink1" style="position:relative; top:5px;" >View</a>
				</c:if>
				<c:if test="${form.templateId == 3}">
					<a href="../user/getUserComponent103.do?formid=${form.userFormId}" class="btn-txt formlink1" id="formLink" class="btn-txt" style="position:relative; top:5px;" >View</a>
				</c:if>
				<c:if test="${form.templateId == 4}">
					<a href="../user/getUserFormByFormId104.do?formId=${form.userFormId}" class="btn-txt formlink1" id="formLink" class="btn-txt" style="position:relative; top:5px;">View</a>
				</c:if>
                                <c:if test="${form.templateId == 5}">
					<a href="../user/getUserFormByFormId105.do?formId=${form.userFormId}" class="btn-txt formlink1" id="formLink" class="btn-txt" style="position:relative; top:5px;">View</a>
				</c:if>        
			</div>
			<div id="action">
				<input type="hidden" value="${form.userFormId}" id="userFormId_${status.count}">
				<input type="hidden" value="${form.userIfs.userId}" id="userIdIfs_${status.count}">
				<a href="#" id="deleteForm_${status.count}" class="deleteForm btn-txt">Delete</a>
			</div>
			</div>
		</c:if>
	</c:forEach>
</div>