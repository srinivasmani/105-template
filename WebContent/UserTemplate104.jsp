<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inspection</title>
<link rel="stylesheet" type="text/css" href="../css/stylesheet.css"/>
<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>
<script type="text/javascript" src="../js/image-resizing.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="header.jsp"%>
</head>
<body>
<div class="container" style="padding:0px;">
<%@ include file="WEB-INF/header/userNavigation.jsp"%>
<div id="centercontainer" class="centercontainer">
<div class="nav">
	<ul>
			<c:if test="${userForm.status != 'default'}">
				<li><a href="javascript:void(0);" class="selected" id="formNameTab">${userForm.formName}</a></li>
			</c:if>
			<c:if test="${userForm.status == 'default'}">
				<li><a href="./navigateTemplateView.do" onclick="launchWindow('#dialog');">Template 101</a></li>
				<li><a href="./getUserComponentIfs.do" onclick="launchWindow('#dialog');">Template 102</a></li>
				<li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a></li>
				<li><a href="javascript:void(0);" class="selected" onclick="launchWindow('#dialog');">Template 104</a></li>
                                <li><a href="./createUserComponentForm105.do" onclick="launchWindow('#dialog');">Template 105</a></li>
			</c:if>
			</ul>
</div>

<%String getSVG = (String) request.getAttribute("imgType");
			
			String green = "";
		    String yellow = "";
			String red = "";
			String greenSmall = "";
		    String yellowSmall = "";
			String redSmall = "";
				if ( (getSVG != null) && (getSVG.equalsIgnoreCase("circle"))) {
			%>
			<% getSVG = "<b class='greenCircle'></b><b class='yellowCircle'></b><b class='redCircle'></b>";
			  green = "<b class='greenCircle'></b>";
		      yellow = "<b class='yellowCircle'></b>";
			  red = "<b class='redCircle'></b>";
			  greenSmall = "<b class='smallGreenCircle'></b>";
		      yellowSmall = "<b class='smallYellowCircle'></b>";
		      redSmall = "<b class='smallRedCircle'></b>";
			}else{%>
			<% getSVG = "<b class='green'></b><b class='yellow'></b><b class='red'></b>";
			  green = "<b class='green'></b>";
		      yellow = "<b class='yellow'></b>";
			  red = "<b class='red' ></b>";
			  greenSmall = "<b class='smallGreen' style='width:20px; height:20px;'></b>";
		     yellowSmall = "<b class='smallYellow' style='width:20px; height:20px;'></b>";
		     redSmall = "<b class='smallRed' style='width:20px; height:20px;'></b>";
			}%>
			
			
		<div style="clear: both"></div>
  <div class="innercontainer">
	<div id="templateSize">
        <div style="width: 21.5cm;" class="divLogoTab">
			 <div class="divLogoRow">
				<div class="logo1">
					<c:if test="${userForm.formLeftImage ne null && userForm.formLeftImage ne ''}">
					  <div class="leftOptImage" style="border:none;">	
						<img id="imageSize" src="/ImageLibrary/${userForm.formLeftImage}" style="max-width:150px;max-height:100px; margin:0;" />
					  </div>
				  </c:if>
				</div>
				<div class="yourlogo">
				  <div align="center">
				    <c:choose>
				     <c:when test="${userForm.formImage ne null && userForm.formImage ne ''}">
				     	<div id="img" class="img" style="border:none;">	
					         <img src="/ImageLibrary/${userForm.formImage}"  style="max-width:384px;max-height:72px; margin:0;" />
				         </div>
				     </c:when>
				     <c:otherwise>
				    	 <div class="img">
				     		<p id="msg" style="margin-top: -16%;">(Your logo here)<br/>Dimension:[380 x 75]pixels</p>
				     	</div>
				     </c:otherwise>
				    </c:choose>
				  </div>
				</div>	
			    <div class="logo2">
			    <c:if test="${userForm.formRightImage ne null && userForm.formRightImage ne ''}">
				  <div class="rghtOptImage" style="border:none;">	
					<img id="imageSize" src="/ImageLibrary/${userForm.formRightImage}"  style="max-width:150px;max-height:100px;" />
				  </div>
				  </c:if>
				</div>
			 </div>
		</div>
 
    <div id="pageHeading">
	 <p id="heading">
		<label id="lblText" class="text_label4" style="text-transform: uppercase;"><nobr><c:out value="${userForm.headerText}"/></nobr></label>
	 </p>
	 <!--<div class="edit"></div>-->																
	 <div class="clear"></div>
	</div>
	<div class="divTable">
	 <div class="divRow">
		<div id="customer" class="divCell" style="margin-left:30px;">
		  
		  
		    <div id ="customerPDiv" style="border: 0px solid #000;height: 80px;  width: 760px;">
		  <c:if test="${newUserFormComponents[0].status.statusId == 1}">
		  <c:forEach var="cname" items="${fn:split(newUserFormComponents[0].component.componentName, '~')}" varStatus="stat">
			<p  id="customerP" class="inspec_formp moreWidth" style="height: 20px;width:750px; ">
				<span id="cust${stat.count}" style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">${cname!='' ? cname :''}</span>
			</p>
		  </c:forEach>
		  </c:if>				 
			</div>	
		   													
		</div>						
	 </div>
	</div>
								
	<div class="clear"></div>
	<div class="selTable" style="861px;">
	 <div class="selRow">
		<div class="selCol selCol3" style="margin-right:25px;">
		  <div class="selCell">														
			<%=green%>
			<span class="floatLeft">
			
			<c:if test="${newUserFormComponents[1].status.statusId == 1}">
			  <p id="attentionP" class="editme1">
				<c:out value="${newUserFormComponents[1].component.componentName}"/>
		  	  </p>
		 </c:if>
												
			</span>
			<br />  
		  </div>
		</div>
		<div class="selCol selCol3" style="margin-right:40px;">
		  <div class="selCell">	
		  <%=yellow%>							
			<span id="nobr" class="floatLeft">
			<c:if test="${newUserFormComponents[2].status.statusId == 1}">
			  <p id="fAttentionP" class="editme1">
					<c:out value="${newUserFormComponents[2].component.componentName}"/>
			 </p>
			 </c:if>																		
			 </span>
			 <br>  
		  </div>
		</div>
		<div class="selCol selCol3" style="margin-right:0px;">
		  <div class="selCell">
			<%=red%>
			<span id="nobr" class="floatLeft">
			<c:if test="${newUserFormComponents[3].status.statusId == 1}">
			  <p id="iAttentionP" class="editme1">
				  <c:out value="${newUserFormComponents[3].component.componentName}"/>																
			 </p>
			 </c:if>																		
			</span>
			<br>  
		  </div>
		</div>
						
	 </div>
	</div>
	<div class="clear"></div>
  
    <div class="divTable1 paddingLeft">
     <div  class="inspectionleftwrap">
      <div class="inspection_bg">
        <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
        <c:if test="${newUserFormComponents[4].status.statusId == 1}">
          <span align="center" class="th"><c:out value="${newUserFormComponents[4].component.componentName}"/></span>
          </c:if>
        </div>
        <div class="clear row1" style="text-align:center;padding:3px;">
        <c:if test="${newUserFormComponents[5].status.statusId == 1}">
          <span class="smallheading"><c:out value="${newUserFormComponents[5].component.componentName}"/></span>
        </c:if>
        </div>
        <div class="clear row1">
          <span  class="alignCenter" style="display:block; height:196px;"><img src="/ImageLibrary/${newUserFormComponents[70].component.componentName}" alt="Car" style="max-height: 192px;max-width: 244px;" /></span>
        </div>
        
        
        <c:forEach var="i" begin="6" end="9" step="1" varStatus ="status">
	        <div class="clear row1">
	          <c:if test="${newUserFormComponents[i].status.statusId == 1}">
	           <span class="inspectionTxt4 leftAlignTxt"><c:out value="${newUserFormComponents[i].component.componentName}"/></span>
	          </c:if>
			  <span class="floatRightTxt"><%=getSVG%></span>
	        </div>
	     
	  	</c:forEach> 
      </div>
	  <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
        <c:if test="${newUserFormComponents[10].status.statusId == 1}">
          <span align="center" class="th"><c:out value="${newUserFormComponents[10].component.componentName}"/></span>
          </c:if>
        </div>
		    <div class="clear row1">
          <span style="width:245px; float:left;padding:5px 0;">
           <c:if test="${newUserFormComponents[11].status.statusId == 1}">
		     <span class="textFont4"><c:out value="${newUserFormComponents[11].component.componentName}"/></span>
		   </c:if>
             <div style="padding:10px 15px 0 40px;;">
             <%=getSVG%>
			 </div>
		  </span>
          <div>
		     
             <div style="width:120px;height:90px; float:left;"><img src="/ImageLibrary/${newUserFormComponents[71].component.componentName}" alt="Bettery" style="max-width: 115px;max-height: 90px;" /> </div>
		   </div>
        </div>
      </div>
      
	  <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
        <c:if test="${newUserFormComponents[12].status.statusId == 1}">
          <span align="center" class="th"><c:out value="${newUserFormComponents[12].component.componentName}"/></span>
        </c:if>
        </div>
        
         <c:forEach var="i" begin="13" end="19" step="1" varStatus ="status">
		        <div class="clear row1">
		         <c:if test="${newUserFormComponents[i].status.statusId == 1}">
		          <span class="inspectionTxt4 leftAlignTxt"><c:out value="${newUserFormComponents[i].component.componentName}"/></span>
		          </c:if>
				  <span class="floatRightTxt"><%=getSVG%></span>
		        </div>
	       
	  	</c:forEach> 
        
      </div>
	   <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
        <c:if test="${newUserFormComponents[20].status.statusId == 1}">
          <span align="center" class="th"><c:out value="${newUserFormComponents[20].component.componentName}"/></span>
         </c:if>
        </div>
        
        <c:forEach var="i" begin="21" end="25" step="1" varStatus ="status">
	      <div class="clear row1">
          <c:if test="${newUserFormComponents[i].status.statusId == 1}">
          	<span class="inspectionTxt4 leftAlignTxt"><c:out value="${newUserFormComponents[i].component.componentName}"/> <br /> </span>
           </c:if>
		  <span class="floatRightTxt"><%=getSVG%></span>
        </div>
       
	  	</c:forEach> 
	  </div>
      
      <div class="clear"></div>
	 
    </div>
  </div>
  <div  class="inspectionrightwrap">
    <div class="inspection_bg">
     
      <div class="inspectionTable" style="border-bottom:1px solid #000;">
        <div class="clear row1 row1Title greyBg">
        <c:if test="${newUserFormComponents[26].status.statusId == 1}">
          <span align="center" class="th"><c:out value="${newUserFormComponents[26].component.componentName}"/></span>
        </c:if>
        </div>
       
        <div class="clear row1" style="border-bottom:0px;">
          <div style="padding:0px; border:0px;height:130px" >
          <div class="bordernone interior_inspec">
              <div class="alignCenter clear paddingBottom" style="width:360px;">
              <c:if test="${newUserFormComponents[27].status.statusId == 1}">
                <span><h2 class="noInspec"><c:out value="${newUserFormComponents[27].component.componentName}"/></h2></span>
               </c:if>
              </div>
              <div class="clear paddingBottom" style="width:385px; height:30px;">
                <c:if test="${newUserFormComponents[28].status.statusId == 1}">
                <span style="width:130px; float:left;"><%=green%>
                	<span class="fontF4"><c:out value="${newUserFormComponents[28].component.componentName}"/></span>
                </span>
                </c:if>
                
                <c:if test="${newUserFormComponents[29].status.statusId == 1}">
                <span style="width:125px; float:left;"><%=yellow%>
                	<span class="fontF4"><c:out value="${newUserFormComponents[29].component.componentName}"/></span>
               </span>
                </c:if>
                <c:if test="${newUserFormComponents[30].status.statusId == 1}">
                   <span style="width:124px; float:left;"><%=red%>
                	<span class="fontF4"> <c:out value="${newUserFormComponents[30].component.componentName}"/></span>
               </span>
                </c:if>
               
            
              </div>
             <div class="clear">
                <div class="alignCenter" style="width:375px;">
				 <div class="bordernone interior_inspec interior_inspecLeft">
                      <span class="txt_bold" style="float:left;margin-right:4px; width:18px;">
                      <c:choose>
                        <c:when test="${newUserFormComponents[31].status.statusId == 1 && newUserFormComponents[32].status.statusId == 1}">
                      		<strong><c:out value="${newUserFormComponents[31].component.componentName}"/></strong>
                       </c:when>
                       <c:otherwise>
                         &nbsp;                       
                       </c:otherwise>
                      </c:choose>
                      </span>
                     <c:if test="${newUserFormComponents[32].status.statusId == 1 && newUserFormComponents[31].status.statusId == 1}">
                      <span width="87"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                      <span class="txt_bold"><strong>
                      	<c:out value="${newUserFormComponents[32].component.componentName}"/></strong>
                      </span>
                     </c:if>
                  
                  </div>
                  <div class="bordernone interior_inspec interior_inspecRight">
                      <span class="txt_bold"  style="float:left;margin-right:4px; width:18px;">
                      
                      <c:choose>
                        <c:when test="${newUserFormComponents[33].status.statusId == 1 && newUserFormComponents[34].status.statusId == 1}">
                     	  	<strong><c:out value="${newUserFormComponents[33].component.componentName}"/></strong>
                        </c:when>
                        <c:otherwise>
                       	 &nbsp;                        
                        </c:otherwise>
                      </c:choose>
                       </span>
                      <c:if test="${newUserFormComponents[34].status.statusId == 1 && newUserFormComponents[33].status.statusId == 1}">
                      <span width="87"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                      <span class="txt_bold">
                      		<strong><c:out value="${newUserFormComponents[34].component.componentName}"/></strong>
                      </span>
                       </c:if>
                  
                  </div>
                  <div class="bordernone interior_inspec interior_inspecLeft">
                      <span class="txt_bold" style="float:left;margin-right:4px; width:18px;">
                        <c:choose>
                          <c:when test="${newUserFormComponents[35].status.statusId == 1 && newUserFormComponents[36].status.statusId == 1}">
                      		<strong><c:out value="${newUserFormComponents[35].component.componentName}"/></strong>
                          </c:when>
                           <c:otherwise>
                            &nbsp;
                           </c:otherwise>
                        </c:choose>
                      </span>
                      <c:if test="${newUserFormComponents[36].status.statusId == 1 && newUserFormComponents[35].status.statusId == 1}">
                      <span width="87"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                      	<span class="txt_bold">
                      	<strong><c:out value="${newUserFormComponents[36].component.componentName}"/></strong>
                      </span>
                      </c:if>
                    
                  </div>
                  <div class="bordernone interior_inspec interior_inspecRight">
                     <span class="txt_bold"  style="float:left;margin-right:4px; width:18px;"> 
                       <c:choose>
                        <c:when test="${newUserFormComponents[37].status.statusId == 1 && newUserFormComponents[38].status.statusId == 1}">
                     		  <strong><c:out value="${newUserFormComponents[37].component.componentName}"/></strong>
                       </c:when>
                       <c:otherwise>
                       	&nbsp;                       
                       </c:otherwise>
                       </c:choose>
               		  </span>
                       <c:if test="${newUserFormComponents[38].status.statusId == 1 && newUserFormComponents[37].status.statusId == 1}">
                        <span width="87"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                      	<span class="txt_bold"><strong><c:out value="${newUserFormComponents[38].component.componentName}"/></strong></span>
                      </c:if>
                    
                  </div>
				 </div>
              </div>
            </div></div>
        </div>
   
          
             <div style="background-color:#dddddf; height:218px;">
			  <div style="float:left; width:200px;">
                <span style="padding:0px;width:100px;float:left;">
				 <img src="/ImageLibrary/${newUserFormComponents[72].component.componentName}" style="max-height: 170px;max-width: 100px;" />
				</span>
				  
                <div  class="bordernone interior_inspec padding_reset lessWidth" style="padding:0px;float:right; padding-top:15px;width:100px;">
                    <div style="height:30px; margin-bottom:10px;">
                     <c:if test="${newUserFormComponents[39].status.statusId == 1}">
                      	 <h2 class="titleFont" style="text-align:center;"><c:out value="${newUserFormComponents[39].component.componentName}"/></h2>
                      </c:if>
                    </div>
                     <c:if test="${newUserFormComponents[40].status.statusId == 1}">
                    <div class="clear" style="height:20px; margin-bottom:10px;">
                      	<span class="txt_bold txtLeft" style="width:18px;" > <strong><c:out value="${newUserFormComponents[40].component.componentName}"/></strong></span>
                      <span width="284" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                    </div>
                    </c:if>
                    
                
                    <c:if test="${newUserFormComponents[41].status.statusId == 1}">
                    <div class="clear" style="height:20px; margin-bottom:10px;">
                      	<span class="txt_bold txtLeft" style="width:18px;"><strong><c:out value="${newUserFormComponents[41].component.componentName}"/></strong></span>
                      <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                    </div>
                    </c:if>
            
                     <c:if test="${newUserFormComponents[42].status.statusId == 1}">
                    <div class="clear" style="height:20px; margin-bottom:10px;">
                      	<span class="txt_bold txtLeft" style="width:18px;"><strong><c:out value="${newUserFormComponents[42].component.componentName}"/></strong></span>
                      <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                    </div>
                    </c:if>
             	  <c:if test="${newUserFormComponents[43].status.statusId == 1}">
                    <div class="clear" style="height:20px; margin-bottom:10px;">
                      	<span class="txt_bold txtLeft" style="width:18px;"><strong><c:out value="${newUserFormComponents[43].component.componentName}"/></strong></span>
                      <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
					
                    </div>
                    </c:if>
                </div>
				</div>
				   <div class="interior_inspec" style="height:217px;border-right: 2px solid #0e62af !important;border-left: 2px solid #0e62af !important;padding:0px;width:96px; padding:0 3px; float:left;">
                        <div cellspacing="0" class="bordernone padding_reset" style="">
                            <div style="text-align:center;margin-top:15px;">
                             <c:if test="${newUserFormComponents[44].status.statusId == 1}">
                              	<h2 class="titleFont" style="text-align:center;"><c:out value="${newUserFormComponents[44].component.componentName}"/></h2>
                              </c:if>
                            </div>
                            <div>
                              <span style="float:left; width:16px;margin-right:2px; position:relative; top:7px;left:7px;"><%= redSmall%></span>
                              <span class="fontTxt" style="display:block; float:left;width:65px;text-align:center;">
                              <img src="/ImageLibrary/${newUserFormComponents[73].component.componentName}" alt="" style="max-height: 36px;max-width: 40px;" /></span>
                            </div>
                            
                             <div class="bordernone interior_inspec">
                                  <div class="beforeAfter">
                                    <c:if test="${newUserFormComponents[45].status.statusId == 1}">
                                    	<span class="beforeTxt"><c:out value="${newUserFormComponents[45].component.componentName}"/></span>
                                    </c:if>
                                    &nbsp&nbsp;
                                     <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                    	<span class="afterTxt"><c:out value="${newUserFormComponents[46].component.componentName}"/></span>
                                    </c:if>
                                  </div>
                                  <div style="width:100%;"  class="clear">
								  <div  style="float:left;width:15px;">
                                 <span style="position:relative;top:7px;"><strong>
                                  <c:if test="${newUserFormComponents[47].status.statusId == 1}">
                                 	<span class="txt_bold"><c:out value="${newUserFormComponents[47].component.componentName}"/></span>
                                 </c:if>
                                 </strong></span><br />
								 <span  style="position:relative;top:15px;"><strong>
								 <c:if test="${newUserFormComponents[48].status.statusId == 1}">
								 	<span class="txt_bold"><c:out value="${newUserFormComponents[48].component.componentName}"/></span>
								 </c:if>
								 </strong></span>
								 </div>
                                 <div style="width:30px; float:left;margin-right:3px;">
                                 <span class="white_box">&nbsp;</span><br />
								 <span class="white_box">&nbsp;</span></div>&nbsp;
								 <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                  	<span class="white_box_rectangle">&nbsp;</span>
                                 </c:if>
                                  </div>
								  <div style="width:100%;"  class="clear">
								  <div  style="float:left;width:15px;">
                                 <span style="position:relative;top:7px;"><strong>
                                 <c:if test="${newUserFormComponents[49].status.statusId == 1}">
                                 	<span class="txt_bold"><c:out value="${newUserFormComponents[49].component.componentName}"/></span>
                                 </c:if>
                                 </strong></span><br />
								 <span  style="position:relative;top:15px;"><strong>
								 <c:if test="${newUserFormComponents[50].status.statusId == 1}">
									 <span class="txt_bold"><c:out value="${newUserFormComponents[50].component.componentName}"/></span>
								  </c:if> 
								  </strong></span>
								 </div>
                                 <div style="width:30px; float:left;margin-right:3px;"><span class="white_box">&nbsp;</span><br />
								  <span class="white_box">&nbsp;</span></div>&nbsp;
								 <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                 	<span class="white_box_rectangle">&nbsp;</span>
                                 </c:if>
                                  </div>
                                </div>
                          </div>
                  </div>
				<div class="interior_inspec" style="padding:0px;width:74px;float:left; padding:0 3px;"class="bordernone padding_reset">
                            <div style="margin-top:15px;">
                             <c:if test="${newUserFormComponents[51].status.statusId == 1}">
                              <span><h2 class="titleFont" style="text-align:center;word-wrap:break-word;"><c:out value="${newUserFormComponents[51].component.componentName}"/></h2></span>
                             </c:if>
                            </div>
                            <div  class="bordernone interior_inspec">
                            
                                <c:forEach var="i" begin="52" end="55" step="1" varStatus ="status">
	                              <div class="clear" style="height:20px; margin-bottom:8px;">
	               						<c:if test="${newUserFormComponents[i].status.statusId == 1}">
	                 				    <span class="white_box_square" style="float:left;margin-right:5px;"></span>
	                 				   <span class="txtFont" style="vertical-align:-3px;"><c:out value="${newUserFormComponents[i].component.componentName}"/></span>
	                 				</c:if>
	                 			</div>
								</c:forEach>
                                </div>
                          </div>
              </div>
      </div>
	  <div class="inspectionTable inspectionTableBg" style="border-bottom:1px solid #000;position:relative;">
        <div class="clear row1 row1Title greyBg">
         <c:if test="${newUserFormComponents[56].status.statusId == 1}">
          <span align="center" class="th"><c:out value="${newUserFormComponents[56].component.componentName}"/></span>
         </c:if>
        </div>
       
        <div class="clear row1" style="border-bottom:0px;">
          <div style="padding:0px; border:0px;height:130px" >
          <div class="bordernone interior_inspec">
              <div class="alignCenter clear paddingBottom" style="width:360px;">
 				<c:if test="${newUserFormComponents[57].status.statusId == 1}">              
                 	<span><h2 class="noInspec"><c:out value="${newUserFormComponents[57].component.componentName}"/></h2></span>
                </c:if>
              </div>
              <div class="clear paddingBottom" style="width:200px; height:30px;">
               <c:if test="${newUserFormComponents[58].status.statusId == 1}">
                <span class="clear" style="display:block;margin-bottom:5px;"><%= green%>
               
                	<span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"><c:out value="${newUserFormComponents[58].component.componentName}"/></span>
                
                </span>
                </c:if>	
             <c:if test="${newUserFormComponents[59].status.statusId == 1}">
                <span class="clear" style="display:block;margin-bottom:5px;"><%= yellow%>
                 
                	<span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"> <c:out value="${newUserFormComponents[59].component.componentName}"/></span>
                
                </span>
                 </c:if>
                <c:if test="${newUserFormComponents[60].status.statusId == 1}">	
                <span class="clear" style="display:block;margin-bottom:5px;"><%= red%>
                	<span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"><c:out value="${newUserFormComponents[60].component.componentName}"/></span>
                </span>
                 </c:if>
            
              </div>
              <div class="clear">
    
              </div>
            </div></div>
        </div>
          <div id="imgBottomCenter" align="center" class="imgBottomCenter" style="float:left; width:205px; height:196px; position:absolute; top:75px; margin-left:180px; z-index:33333;">
               		 	<span class="alignCenter">
							<img src="/ImageLibrary/${newUserFormComponents[74].component.componentName}" alt="" style="max-height: 196px;max-width: 205px;" />
						</span>
					</div>
              <div style="background-color:#dddddf; height:205px;">
			
                <div  class="bordernone interior_inspec padding_reset" style="padding:15px 30px;">
                  <div style="height: 90px;">
                 <c:forEach var="i" begin="61" end="64" step="1" varStatus ="status">
                <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                 <div class="clear">
                      <span width="29" class="txt_bold txtLeft" style="width:18px;" > <strong><c:out value="${newUserFormComponents[i].component.componentName}"/></strong></span>
                      <span width="284" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                    </div>
                 </c:if>
				</c:forEach> 
				</div>	
					 <div class="clear" style="width:300px;margin-top:35px;">
					  <c:if test="${newUserFormComponents[65].status.statusId == 1}">
					  	 <span class="fontF4" style="float:left;display:block; width:76px;margin-top:12px;"><c:out value="${newUserFormComponents[65].component.componentName}"/></span>
					  </c:if>
					  <img src="/ImageLibrary/${newUserFormComponents[75].component.componentName}" alt="" style="float:left;margin-left:3px; margin-right:3px;max-width: 150px;max-height: 45px;" />
                       <span class="fontF4" style="float:left;"> </span>
                       	 <c:if test="${newUserFormComponents[66].status.statusId == 1}">
					  	 <span class="fontF4" style="float:left;display:block; width:66px;margin-top:12px;"><c:out value="${newUserFormComponents[66].component.componentName}"/></span>
					  </c:if>				   
					 </div>
                </div>
				
			</div>
          </div>
		  
		  <div class="inspectionTable">
        <div class="clear row1 row1Title greyBg">
         <c:if test="${newUserFormComponents[67].status.statusId == 1}">
          	<span align="center" class="th"><c:out value="${newUserFormComponents[67].component.componentName}"/></span>
          </c:if>
        </div>
        <div class="clear row1" style="height:33px;">
          
        </div>
         <div class="clear row1" style="height:33px;">
          
        </div>
       <div class="clear row1" style="height:33px;">
          
        </div>
		 <div class="clear row1" style="height:33px;">
          
        </div>
	 <div class="clear row1" style="height:33px;">
          
        </div>
		
		
      </div>
    
	<div class="bottomtext" style="width: 393px;overflow:hidden;">
		<div style="width:400px;margin-top:10px;">
			<c:if test="${newUserFormComponents[68].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[68].component.componentName}"/>
			</c:if>
			<div style="float:left;width:279px;overflow:hidden;">
				<span id="cmt" style="float: left;">${newUserFormComponents[68].component.componentName}</span>
				<span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>  
			</div> 
			<c:if test="${newUserFormComponents[69].status.statusId == 1}">
				<c:set var="componentDesc" value="${newUserFormComponents[69].component.componentName}"/>
			</c:if>
			<div style="float:right;width: 120px;">
				<span id="component69Span" class="comments" style="float: left;">${newUserFormComponents[69].component.componentName}</span> 
				<span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine3" style="width: 125px"/></span>
	    	 </div> 
	     </div>
	</div>  
    </div>
  </div>
  </div>
  <div class="clear"></div>
  
   <div style="text-align:right;padding-right:20px;font-family:'MyriadProRegular'; font-size:6pt;position:relative; top:45px;"></div>

			<div class="containerbtm btnContainer">
				<nobr>
				<c:if test="${user.userRole eq 'user'}">
					<a style="margin-right:5px;" href="./userEditTemplate104ByFormId.do?formId=${userForm.userFormId}" class="EditBtn btn-txt" onclick="launchWindow('#dialog');">
					Edit Template
		            <span style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
					</a>
       			</c:if>
       			<a href="./GenerateUserFormPdf.do?userFormId=${userForm.userFormId}" target="_newtab"  class="PdfBtn btn-txt">
       			Generate PDF
		       <span style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
       			</a>
				</nobr>
			</div>
		
 
  </div>
     </div>
  </div>
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
</body>

