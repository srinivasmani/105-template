<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<%@ include file="header.jsp"%>
<title>Welcome</title>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css">
<!-- Java Script -->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<body>
	<div class="container">
		<!--Header starts-->
		<div class="header"></div>
			<div id="centercontainer" class="centercontainer">
			<div class="innercontainer" style="min-height:300px;">
				 <c:if test="${user.userRole =='admin'}">
				 <div class="welcometxt">Hello admin</div>
						<div class="admincontainer">
							
								<a class="adminbtn" href="../admin/navigateTemplateView.do"
									style="font-size: 11pt;"><span>Master Templates</span></a>
							
							
								<a class="adminbtn" href="./user/navigateUserDefinedTemplate.do"
									id="navigateFormView" style="font-size: 11pt;"><span>User Forms</span></a>
									
								<%-- <a class="adminbtn" href="./admin/GenerateTemplate2Pdf.do"
									id="navigateFormView" style="font-size: 11pt;"><span>Template2</span></a>	--%>
									
							
						</div>
						
					</c:if> 
					<c:if test="${user.userRole =='user'}">
				<div class="box">
					<%@ include file="SessionUserHomePage.jsp"%>
				</div>
			</c:if> 
				</div>
			</div>

		
		 
	</div>
</body>
