<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Inspection</title>
        <link rel="stylesheet" type="text/css" href="../css/stylesheet.css"/>
        <link rel="stylesheet" type="text/css" href="../css/popUp.css">
            <script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
            <script type="text/javascript" src="../js/popup.js"></script>
            <script type="text/javascript" src="../js/image-resizing.js"></script>
            <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
            <%@ page import="java.io.*,java.util.*" language="java"%>
            <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
            <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
            <%@ include file="header.jsp"%>
            <style>
                #msg {
                    font-size: 10pt;
                    letter-spacing: 0.5px;
                    margin-left: 33%;
                    margin-top: -16%;
                }
                .imgLogo {
                    border: 1px solid black;
                    height: 200px;
                    margin-top: 0;
                    padding: 0;
                    text-align: center;
                    width: 393px;
                }

                .customer-details {
                    font-family: 'MyriadProRegular';
                    font-size: 11pt;
                    list-style: none outside none;
                    margin-top: 5px;
                }

                .customer-details li {
                    border-bottom: 1px solid #333;
                    padding:15px 0 3px;
                    display: block;
                }

                #customer {
                    margin-left: 0;
                }
                
                #templateSize.template-105 {
                    //margin-left: 48px;
                }
                
                #templateSize.template-105 .divLogoTab{
                    border-spacing: 0;
                    float: left;
                    margin-left: 10px;
                    width: 354px;
                }
                #templateSize.template-105 .divLogoTab .imgLogo{
                    float: left;
                    height: 250px !important;
                    width: 350px !important;
                    margin-left: 24px !important;
                    margin-top: 25px !important;
                }
                #templateSize.template-105 .divLogoTab .imgLogo #msg {
                    margin-left: 25% !important;
                    margin-top: 30% !important;
                }
                #templateSize.template-105 #customer.divCell  {
                    margin-right: 7px;
                }
                #templateSize.template-105 #customer.divCell .customer-details li{
                    padding: 2px 0 3px;
                    height: 19px;
                    margin-bottom: 13px;
                }
                #templateSize.template-105 .inspectionleftwrap {
                    width: 392px;
                }
                #templateSize.template-105 .divTable { margin-top: 0px;}
                #templateSize.template-105 .inspectionTxt4 {
                    font-size: 11.5pt;
                }
                #templateSize.commentsClass {
                    font-family: 'MyriadProRegular';
                    font-size: 13pt;
                    height: 1px;
                    margin-left: 8px;
                    padding-top: 5px;
                }
                .width160 {
                    width: 160px;
                }
                .brake-dots {
                    display: block; 
                    overflow: hidden; 
                    margin: 2px 0px;
                }
                .brake-dots b { 
                    margin-left: 4px;
                }
                .margin-top2 {margin-top:4px;}
                .smallGreenCircle, .smallRedCircle, .smallYellowCircle {
                    height:23px;
                    width: 23px;
                }
                #logoText{
                    height: 230px;
                    width: 350px;
                }
                .smallGreenCircle, .smallRedCircle, .smallYellowCircle {
                width: 22px;
                height: 22px;
                }
                 #logoText h1 {
                float: none;
                font-size: 22px;
            }
            #logoText h2 {
                float: none;
                font-size: 16.5px;
            }
            #logoText h3 {
                float: none;
                font-size: 12.8px;
            }
            #logoText h4 {
                float: none;
                font-size: 11px;
            }
            #logoText h5 {
                float: none;
                font-size: 9px;
            }
            #logoText h6 {
                float: none;
                font-size: 7.3px;
            }
            #logoText p {
                float: none;
                word-wrap: break-word;
            }
            .inspection_bg .greyBg span.th {
                width: 100%;
            }
            //h2.titleFont{
            //    font-style: normal;
            //}
            //.txtFont{
            //    font-style: normal;
            //}
            </style>
    </head>
    <body>
        <div class="container" style="padding:0px;">
            <%@ include file="WEB-INF/header/userNavigation.jsp"%>
            <div id="centercontainer" class="centercontainer">
                <div class="nav">
                    <ul id="headerList">
                        <li><a href="./navigateTemplateView.do" class="notSelected" id="tem1" onclick="launchWindow('#dialog');">Template 101</a>
                        </li>
                        <li><a href="./navigateTemplateIfs.do" class="notSelected" id="tem2" onclick="launchWindow('#dialog');">Template 102</a>
                        </li>
                        <li><a href="./navigateTemplate103.do" class="notSelected" id="tem3" onclick="launchWindow('#dialog');">Template 103</a>
                        </li>
                        <li><a href="./navigateTemplate104.do" class="notSelected" id="tem4" onclick="launchWindow('#dialog');">Template 104</a>
                        </li>
                        <li><a href="./navigateTemplate105.do" class="selected" id="tem6" onclick="launchWindow('#dialog');">Template 105</a>
                        </li>
                        <li><a href="./addImageToLibrary.do" class="notSelected" id="tem5" onclick="launchWindow('#dialog');">Add image</a>
                        </li>
                    </ul>
                </div>
                <%String getSVG = (String) request.getAttribute("imgType");

                    String green = "";
                    String yellow = "";
                    String red = "";
                    String greenSmall = "";
                    String yellowSmall = "";
                    String redSmall = "";
                    if ((getSVG != null) && (getSVG.equalsIgnoreCase("circle"))) {
                %>
                <% getSVG = "<b class='greenCircle'></b><b class='yellowCircle'></b><b class='redCircle'></b>";
                    green = "<b class='greenCircle'></b>";
                    yellow = "<b class='yellowCircle'></b>";
                    red = "<b class='redCircle'></b>";

                    greenSmall = "<b class='smallGreenCircle'></b>";
                    yellowSmall = "<b class='smallYellowCircle'></b>";
                    redSmall = "<b class='smallRedCircle'></b>";
                        } else {%>
                <% getSVG = "<b class='green'></b><b class='yellow'></b><b class='red'></b>";
                        green = "<b class='green'></b>";
                        yellow = "<b class='yellow'></b>";
                        red = "<b class='red' ></b>";
                        greenSmall = "<b class='smallGreen' style='width:20px; height:20px;'></b>";
                        yellowSmall = "<b class='smallYellow' style='width:20px; height:20px;'></b>";
                        redSmall = "<b class='smallRed' style='width:20px; height:20px;'></b>";
                            }%>


                <div style="clear: both"></div>
                <div class="innercontainer">
                    <div id="templateSize" class="template-105">
                        
                        <!--Header-->
                        <div id="pageHeading">
                            <p id="heading">
                                <label id="lblText" class="text_label4" style="text-transform: uppercase;"><nobr><c:out value="${template.headerText}"/></nobr></label>
                            </p>
                            <!--<div class="edit"></div>-->																
                            <div class="clear"></div>
                        </div>
                        
                        <!--Logo-->                            
                        <div class="divLogoTab">
                            <div class="divLogoRow">
                                <div class="yourlogo">
                                    <div align="center">
                                        <c:choose>
                                            <c:when test="${template.templateImage ne null && template.templateImage ne ''}">
                                                 <div id="img" align="center" class="imgLogo" style="border: 0px solid black;">
                                                    <img src="/ImageLibrary/${template.templateImage}" style="margin:0; max-width:350px; max-height:250px;">
                                                </div>
                                            </c:when>
                                            <c:when test="${template.logoText  ne  null && template.logoText ne '' }">
                                                <div id="logoText"><c:out value="${template.logoText}" escapeXml="false"/></div>
                                            </c:when>
                                            <c:otherwise>
                                                <div id="img" align="center" class="imgLogo">
                                                    <p id="msg">(Your logo here)<br/>Dimension:[350 x 250]pixels</p>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>	
                            </div>
                        </div>
                        
                        <!--Name-->
                        <div class="divTable" style="width:390px; float: right;clear:none;">
                            <div class="divRow">
                                <div id="customer" class="divCell">
                                    <div id="customerPDiv" style="border: 0px solid #000;">
                                        <c:if test="${template.components[0].templateComponent.status.statusId == 1}">
                                            <p  id="customerP" class="inspec_formp moreWidth" style="width:390px; display: none;">
                                                <span style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">
                                                    <ul class="customer-details">
                                                        <c:forEach var="cnameSplit" items="${fn:split(template.components[0].componentName, '_')}" >
                                                            <li> ${cnameSplit}</li> 
                                                        </c:forEach>
                                                    </ul>
                                                </span>
                                            </p>
                                        </c:if>				 
                                    </div>											
                                </div>						
                            </div>
                        </div>

                        <div class="clear"></div>
                        
                        <!--Checked-Future-Immediate-->
                        <div class="selTable">
                            <div class="selRow">
                                <div class="selCol selCol3" style="margin-right:25px;">
                                    <div class="selCell">														
                                        <%=green%>
                                        <span class="floatLeft">

                                            <c:if test="${template.components[1].templateComponent.status.statusId == 1}">
                                                <p id="attentionP" class="editme1">
                                                    <c:out value="${template.components[1].componentName}"/>
                                                </p>
                                            </c:if>

                                        </span>

                                    </div>
                                </div>

                                <div class="selCol selCol3" style="margin-right:40px;">
                                    <div class="selCell">								
                                        <%=yellow%>
                                        <span id="nobr" class="floatLeft">
                                            <c:if test="${template.components[2].templateComponent.status.statusId == 1}">
                                                <p id="fAttentionP" class="editme1">
                                                    <c:out value="${template.components[2].componentName}"/>
                                                </p>
                                            </c:if>																		
                                        </span>

                                    </div>
                                </div>
                                <div class="selCol selCol3" style="margin-right:0px;">
                                    <div class="selCell">
                                        <%=red%>
                                        <span id="nobr" class="floatLeft">
                                            <c:if test="${template.components[3].templateComponent.status.statusId == 1}">
                                                <p id="iAttentionP" class="editme1">
                                                    <c:out value="${template.components[3].componentName}"/>																
                                                </p>
                                            </c:if>																		
                                        </span>

                                    </div>
                                </div>

                            </div>
                        </div>
                                        
                        <div class="clear"></div>
                        
                        <div class="divTable paddingLeft" style="padding-left: 0;">
                            <div  class="inspectionleftwrap">
                                <div class="inspection_bg">
                                    
                                    <!--Car-->
                                    <div class="inspectionTable">

                                        <c:if test="${template.components[4].templateComponent.status.statusId == 1}">
                                            <div class="clear row1 row1Title greyBg">
                                                <span align="center" class="th"><c:out value="${template.components[4].componentName}"/></span>
                                            </div>
                                        </c:if>

                                        <c:if test="${template.components[5].templateComponent.status.statusId == 1}">
                                            <div class="clear row1" style="text-align:center;padding:3px;">
                                                <span class="smallheading"><c:out value="${template.components[5].componentName}"/></span>
                                            </div>
                                        </c:if>
                                        <div class="clear row1">
                                            <div  class="alignCenter" style="display:block; height:196px;">
                                                <img src="/ImageLibrary/${template.components[70].componentName}" alt="${template.components[70].componentName}" style="max-height: 192px;max-width:244px;" />
                                            </div>
                                        </div>

                                        <c:forEach var="i" begin="6" end="9" step="1" varStatus ="status">

                                            <div class="clear row1">
                                                <c:if test="${template.components[i].templateComponent.status.statusId == 1}">
                                                    <span class="inspectionTxt4 leftAlignTxt"><c:out value="${template.components[i].componentName}"/></span>
                                                </c:if>
                                                <span class="floatRightTxt"><%=getSVG%></span>
                                            </div>

                                        </c:forEach> 

                                    </div>

                                    <!--Underhood-->        
                                    <div class="inspectionTable">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${template.components[10].templateComponent.status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${template.components[10].componentName}"/></span>
                                            </c:if>
                                        </div>


                                        <c:forEach var="i" begin="11" end="17" step="1" varStatus ="status">

                                            <div class="clear row1">
                                                <c:if test="${template.components[i].templateComponent.status.statusId == 1}">
                                                    <span class="inspectionTxt4 leftAlignTxt"><c:out value="${template.components[i].componentName}"/></span>
                                                </c:if>
                                                <span class="floatRightTxt"><%=getSVG%></span>
                                            </div>

                                        </c:forEach> 

                                    </div>
                                    
                                    <!--Under Vehicle-->
                                    <div class="inspectionTable">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${template.components[18].templateComponent.status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${template.components[18].componentName}"/></span>
                                            </c:if>
                                        </div>
                                         
                                        <div class="clear row1">
                                            <c:if test="${template.components[19].templateComponent.status.statusId == 1}">
                                                <span class="inspectionTxt4 leftAlignTxt" style="min-height: 45px; padding: 0px !important; width: 290px;"><c:out value="${template.components[19].componentName}"/> </span>
                                            </c:if>
                                            <span class="floatRightTxt"><%=getSVG%></span>
                                        </div>

                                        <c:forEach var="i" begin="20" end="23" step="1" varStatus ="status">
                                            <div class="clear row1">
                                                <c:if test="${template.components[i].templateComponent.status.statusId == 1}">
                                                    <span class="inspectionTxt4 leftAlignTxt" style="padding: 0px !important; width: 290px;"><c:out value="${template.components[i].componentName}"/> </span>
                                                </c:if>
                                                <span class="floatRightTxt"><%=getSVG%></span>
                                            </div>

                                        </c:forEach> 
                                    </div>

                                    <div class="clear"></div>

                                </div>
                            </div>
                            <div  class="inspectionrightwrap" style="margin-left: 32px;">
                                <div class="inspection_bg">
                                    
                                    <!--Tires-->
                                    <div class="inspectionTable" style="border-bottom:1px solid #000;">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${template.components[24].templateComponent.status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${template.components[24].componentName}"/></span>
                                            </c:if>
                                        </div>

                                        <div class="clear row1" style="border-bottom:0px;">
                                            <div style="padding:0px; border:0px;height:130px" >
                                                <div class="bordernone interior_inspec">
                                                    <div class="alignCenter clear paddingBottom" style="width:360px; padding-bottom: 0;">
                                                        <c:if test="${template.components[25].templateComponent.status.statusId == 1}">
                                                            <span><h2 class="noInspec"><c:out value="${template.components[25].componentName}"/></h2></span>
                                                        </c:if>
                                                    </div>
                                                    <div class="clear paddingBottom" style="width:385px; height:30px;">
                                                        <c:if test="${template.components[26].templateComponent.status.statusId == 1}">
                                                            <span style="width:138px; float:left;"><%=green%>
                                                                <span class="fontF4"><c:out value="${template.components[26].componentName}"/></span>
                                                            </span>
                                                        </c:if>
                                                        <c:if test="${template.components[27].templateComponent.status.statusId == 1}">
                                                            <span style="width:123px; float:left;"><%=yellow%>
                                                                <span class="fontF4"><c:out value="${template.components[27].componentName}"/></span>
                                                            </span>
                                                        </c:if>
                                                        <c:if test="${template.components[28].templateComponent.status.statusId == 1}">
                                                            <span style="width:124px; float:left;"><%=red%>
                                                                <span class="fontF4"> <c:out value="${template.components[28].componentName}"/></span>
                                                            </span>
                                                        </c:if>

                                                    </div>
                                                    <div class="clear">
                                                        <div class="alignCenter" style="width:375px;">
                                                            <div class="bordernone interior_inspec interior_inspecLeft width160">
                                                                <span class="txt_bold" style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${template.components[29].templateComponent.status.statusId == 1 && template.components[30].templateComponent.status.statusId == 1}">
                                                                            <strong><c:out value="${template.components[29].componentName}"/></strong>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${template.components[30].templateComponent.status.statusId == 1 && template.components[29].templateComponent.status.statusId == 1}">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;"><strong>
                                                                            <c:out value="${template.components[30].componentName}"/>
                                                                        </strong>
                                                                    </span>
                                                                </c:if>

                                                            </div>
                                                            <div class="bordernone interior_inspec interior_inspecRight width160">
                                                                <span class="txt_bold"  style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${template.components[31].templateComponent.status.statusId == 1 && template.components[32].templateComponent.status.statusId == 1}">
                                                                            <strong><c:out value="${template.components[31].componentName}"/></strong>   
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${template.components[32].templateComponent.status.statusId == 1 && template.components[31].templateComponent.status.statusId == 1}">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;">
                                                                        <strong><c:out value="${template.components[32].componentName}"/></strong>
                                                                    </span>
                                                                </c:if>

                                                            </div>
                                                            <div class="bordernone interior_inspec interior_inspecLeft width160">

                                                                <span class="txt_bold" style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${template.components[33].templateComponent.status.statusId == 1 && template.components[34].templateComponent.status.statusId == 1}">
                                                                            <strong><c:out value="${template.components[33].componentName}"/></strong>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${template.components[34].templateComponent.status.statusId == 1 && template.components[33].templateComponent.status.statusId == 1}">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;">
                                                                        <strong><c:out value="${template.components[34].componentName}"/></strong>
                                                                    </span>
                                                                </c:if>

                                                            </div>
                                                            <div class="bordernone interior_inspec interior_inspecRight width160">
                                                                <span class="txt_bold"  style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${template.components[35].templateComponent.status.statusId == 1 && template.components[36].templateComponent.status.statusId == 1}">
                                                                            <strong><c:out value="${template.components[35].componentName}"/></strong>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${template.components[36].templateComponent.status.statusId == 1 && template.components[35].templateComponent.status.statusId == 1}">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;"><strong><c:out value="${template.components[36].componentName}"/></strong></span>
                                                                        </c:if>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></div>
                                        </div>

                                        <div style="height:200px; border-top: 2px solid #000;">
                                            <div style="float:left; width:200px;">
                                              
                                                <div style="padding:0px;width:100px;float:left;">
                                                    <img src="/ImageLibrary/${template.components[71].componentName}" alt="${template.components[71].componentName}" style="max-width: 100px;max-height: 170px;"/>
                                                </div>

                                                <div  class="bordernone interior_inspec padding_reset lessWidth" style="padding:0px;float:right; padding-top:15px;width:100px !important;">
                                                    <div style="height:30px; margin-bottom:10px;">
                                                        <c:if test="${template.components[37].templateComponent.status.statusId == 1}">
                                                            <h2 class="titleFont" style="text-align:center;"><c:out value="${template.components[37].componentName}"/></h2></span>
                                                        </c:if>
                                                    </div>
                                                    <c:if test="${template.components[38].templateComponent.status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft" style="width:18px;" > <strong><c:out value="${template.components[38].componentName}"/></strong></span>
                                                            <span width="284" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                        </div>
                                                    </c:if>


                                                    <c:if test="${template.components[39].templateComponent.status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft"  style="width:18px;"><strong><c:out value="${template.components[39].componentName}"/></strong></span>
                                                            <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                        </div>
                                                    </c:if>

                                                    <c:if test="${template.components[40].templateComponent.status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft"  style="width:18px;"><strong><c:out value="${template.components[40].componentName}"/></strong></span>
                                                            <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${template.components[41].templateComponent.status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft"  style="width:18px;"><strong><c:out value="${template.components[41].componentName}"/></strong></span>
                                                            <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>

                                                        </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <div class="interior_inspec" style="height:200px;border-right: 2px solid #000 !important;border-left: 2px solid #000 !important;padding:0px;width:96px; padding:0 3px; float:left;">
                                                <div cellspacing="0" class="bordernone padding_reset" style="">
                                                    <div style="text-align:center;margin-top:15px;">
                                                        <c:if test="${template.components[42].templateComponent.status.statusId == 1}">
                                                            <h2 class="titleFont" style="text-align:center;padding-bottom:4px;"><c:out value="${template.components[42].componentName}"/></h2>
                                                        </c:if>
                                                    </div>
                                                    <div>
                                                        <span style="float:left; width:16px;position:relative; top:7px;margin-left:15px;"><%= redSmall%></span>
                                                        <span class="fontTxt" style="display:block; float:right;width:65px;text-align:center;">
                                                        <img src="/ImageLibrary/${template.components[72].componentName}" alt="${template.components[72].componentName}" style="max-width: 40px;max-height: 36px;"/></span>
                                                    </div>

                                                    <div class="bordernone interior_inspec">
                                                        <div class="beforeAfter" style="margin-top: 2px;">
                                                            <c:if test="${template.components[43].templateComponent.status.statusId == 1}">
                                                                <span class="beforeTxt"><c:out value="${template.components[43].componentName}"/></span>
                                                            </c:if>
                                                            &nbsp&nbsp;
                                                            <c:if test="${template.components[44].templateComponent.status.statusId == 1}">
                                                                <span class="afterTxt"><c:out value="${template.components[44].componentName}"/></span>
                                                            </c:if>
                                                        </div>
                                                        <div style="width:100%; margin-top: 3px;"  class="clear">
                                                            <div  style="float:left;width:15px;">
                                                                <span style="position:relative;top:7px;"><strong>
                                                                        <c:if test="${template.components[45].templateComponent.status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${template.components[45].componentName}"/></span>
                                                                        </c:if>
                                                                    </strong></span><br />
                                                                <span  style="position:relative;top:15px;"><strong>
                                                                        <c:if test="${template.components[46].templateComponent.status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${template.components[46].componentName}"/></span>
                                                                        </c:if>
                                                                    </strong></span>
                                                            </div>
                                                            <div style="width:30px; float:left;margin-right:4px;">
                                                                <span class="white_box">&nbsp;</span><br />
                                                                <span class="white_box">&nbsp;</span></div>&nbsp;
                                                                <c:if test="${template.components[46].templateComponent.status.statusId == 1}">
                                                                <span class="white_box_rectangle">&nbsp;</span>
                                                            </c:if>
                                                        </div>
                                                        <div style="width:100%;"  class="clear">
                                                            <div  style="float:left;width:15px;">
                                                                <span style="position:relative;top:7px;"><strong>
                                                                        <c:if test="${template.components[47].templateComponent.status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${template.components[47].componentName}"/></span>
                                                                        </c:if>
                                                                    </strong></span><br />
                                                                <span  style="position:relative;top:15px;"><strong>
                                                                        <c:if test="${template.components[48].templateComponent.status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${template.components[48].componentName}"/></span>
                                                                        </c:if> 
                                                                    </strong></span>
                                                            </div>
                                                            <div style="width:30px; float:left;margin-right:4px;"><span class="white_box">&nbsp;</span><br />
                                                                <span class="white_box">&nbsp;</span></div>&nbsp;
                                                                <c:if test="${template.components[46].templateComponent.status.statusId == 1}">
                                                                <span class="white_box_rectangle">&nbsp;</span>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="interior_inspec" style="padding:0px;width:74px;float:left; padding:0 3px;"class="bordernone padding_reset">
                                                <div style="margin-top:15px;">
                                                    <c:if test="${template.components[49].templateComponent.status.statusId == 1}">
                                                        <span><h2 class="titleFont" style="text-align:center;"><c:out value="${template.components[49].componentName}"/></h2></span>
                                                        </c:if>
                                                </div>
                                                <div  class="bordernone interior_inspec">
                                                    <c:forEach var="i" begin="50" end="53" step="1" varStatus ="status">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <c:if test="${template.components[i].templateComponent.status.statusId == 1}">
                                                                <span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                                                <span class="txtFont" style="vertical-align:-3px;"><c:out value="${template.components[i].componentName}"/></span>
                                                            </c:if>
                                                        </div>
                                                    </c:forEach> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     
                                    <!--      Brakes     -->
                                    <div class="inspectionTable inspectionTableBg" style="border-bottom:1px solid #000; overflow: hidden;">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${template.components[54].templateComponent.status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${template.components[54].componentName}"/></span>
                                            </c:if>
                                        </div>
                                        <div class="clear alignCenter paddingBottom" style="width:200px; margin-left: 80px;margin-top:10px;">
                                            <c:if test="${template.components[55].templateComponent.status.statusId == 1}">              
                                                <span class="txt_bold"><h2 class="noInspec" style="width: 200px;"><c:out value="${template.components[55].componentName}"/></h2></span>
                                            </c:if>
                                        </div>
                                        <div class="clear row1" style="border-bottom:0px; width: 190px; float: left;margin-left: 20px;">
                                            <div style="padding:0px; border:0px;height:130px" >
                                                <div class="bordernone interior_inspec">
                                                    <div class="clear paddingBottom" style="width:200px; height:30px;">
                                                        <c:if test="${template.components[56].templateComponent.status.statusId == 1}">
                                                            <span class="clear" style="display:block;margin-bottom:5px;"><%= green%>
                                                                <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"><c:out value="${template.components[56].componentName}"/></span>
                                                            </span>
                                                        </c:if>	
                                                        <c:if test="${template.components[57].templateComponent.status.statusId == 1}">
                                                            <span class="clear" style="display:block;margin-bottom:5px;"><%= yellow%>

                                                                <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"> <c:out value="${template.components[57].componentName}"/></span>

                                                            </span>
                                                        </c:if>
                                                        <c:if test="${template.components[58].templateComponent.status.statusId == 1}">	
                                                            <span class="clear" style="display:block;margin-bottom:5px;"><%= red%>
                                                                <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"><c:out value="${template.components[58].componentName}"/></span>
                                                            </span>
                                                        </c:if>

                                                    </div>
                                                    <div class="clear">

                                                    </div>
                                                </div></div>
                                        </div>
                                        
                                        <div style="  width: 125px; float: right;">
                                            <div  class="bordernone interior_inspec padding_reset" style="padding:5px;">
                                                    <c:forEach var="i" begin="59" end="62" step="1" varStatus ="status">
                                                        <c:if test="${template.components[i].templateComponent.status.statusId == 1}">
                                                            <div class="clear">
                                                                <span class="txt_bold txtLeft margin-top2"> <strong><c:out value="${template.components[i].componentName}"/></strong></span>
                                                                <span class="brake-dots" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                            </div>
                                                        </c:if>
                                                    </c:forEach> 
                                            </div>
                                        </div>

                                        <div class="clear" style="width:300px;margin:auto;">
                                            <c:if test="${template.components[63].templateComponent.status.statusId == 1}">
                                                <span class="fontF4" style="float:left;display:block; width:76px;margin-top:12px;"><c:out value="${template.components[63].componentName}"/></span>
                                            </c:if>
                                            <img src="/ImageLibrary/${template.components[73].componentName}" alt="${template.components[73].componentName}" style="float:left;margin-left:3px; margin-right:3px;max-width:150px; max-height:45px; " />
                                            <span class="fontF4" style="float:left;"> </span>
                                            <c:if test="${template.components[64].templateComponent.status.statusId == 1}">
                                                <span class="fontF4" style="float:left;display:block; width:66px;margin-top:12px;"><c:out value="${template.components[64].componentName}"/></span>
                                            </c:if>				   
                                        </div>


                                    </div>

                                    <!-- Battery -->
                                    <div class="inspectionTable">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${template.components[65].templateComponent.status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${template.components[65].componentName}"/></span>
                                            </c:if>
                                        </div>

                                        <div class="clear row1">
                                            <div style="width:245px; float:left;padding:5px 0;">
                                                <c:if test="${template.components[66].templateComponent.status.statusId == 1}">
                                                    <span class="textFont4"><c:out value="${template.components[66].componentName}"/></span>
                                                </c:if>
                                                <div style="padding:10px 15px 0 40px;;">
                                                    <%=getSVG%>
                                                </div>
                                            </div>
                                            <div>
                                                <div style="width:120px;height:90px; float:left;">
                                                    <img src="/ImageLibrary/${template.components[74].componentName}" alt="${template.components[74].componentName}" style="max-width: 115px;max-height: 90px;" /> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comments -->            
                                    <div class="inspectionTable" style="margin-bottom: 0px;">
                                        <div class="clear" style="font-family:'MyriadProRegular';font-size: 11pt;height: 1px;margin-left: 8px;padding-top: 5px;">
                                            <c:if test="${template.components[67].templateComponent.status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${template.components[67].componentName}"/></span>
                                            </c:if>
                                        </div>
                                        <div class="clear row1" style="height:26px;">

                                        </div>
                                        <div class="clear row1" style="height:26px;">

                                        </div>
                                        <div class="clear row1" style="height:26px;">

                                        </div>
                                    </div>

                                    <div class="bottomtext" style="width: 393px;overflow:hidden;">
                                        <div style="width:400px;">
                                            <c:if test="${template.components[68].templateComponent.status.statusId == 1}">
                                                <c:set var="componentDesc" value="${template.components[68].componentName}"/>
                                            </c:if>
                                            <div style="float:left;width:279px;overflow:hidden;">
                                                <span id="cmt" style="float: left;">${template.components[68].componentName}</span>
                                                <span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>  
                                            </div> 
                                            <c:if test="${template.components[69].templateComponent.status.statusId == 1}">
                                                <c:set var="componentDesc" value="${template.components[69].componentName}"/>
                                            </c:if>
                                            <div style="float:right;width: 120px;">
                                                <span id="component69Span" class="comments" style="float: left;">${template.components[69].componentName}</span> 
                                                <span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine3" style="width: 125px"/></span>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="form4"></div>

                        <div class="containerbtm btnContainer">
                            <nobr>
                                <a style="margin-right:5px;" href="./editTemplate105.do" class="EditBtn btn-txt" onclick="launchWindow('#dialog');">
                                    Edit Template 
                                    <span style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
                                </a>
                                <a href="./GenerateTemplate5Pdf.do?templateId=${template.templateId}" target="_newtab" class="PdfBtn btn-txt">Generate PDF
                                    <span  style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
                                </a> 
                            </nobr>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
    </body>
</html>
