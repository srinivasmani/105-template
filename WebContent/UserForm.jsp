<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="header.jsp"%>
<!-- Style Sheet -->
<link rel="stylesheet" type="text/css" href="../css/style-edit.css" />
<style>
#msg {
	font-size: 10pt;
	margin-left: 35%;
	margin-top: 4%;
	letter-spacing: 0.5px;
}
</style>
<title>User Form</title>
<!--container starts-->
<div class="container">
	<div class="header"></div>
	<div class="centercontainer">
		<div class="nav">
			<ul>
				<c:forEach items="${userForms}" var="form">
					<c:if test="${form.status=='active'}">
						<c:if test="${form.userFormId == userForm.userFormId}">
							<li><a href="#" class="selected"><nobr>${form.formName}</nobr>
							</a></li>
						</c:if>
						<c:if test="${form.userFormId != userForm.userFormId}">
							<li><a href="#"><nobr>${form.formName}</nobr>
							</a></li>
						</c:if>
					</c:if>
				</c:forEach>
			</ul>
		</div>
		<div style="clear: both"></div>
		<!--Innercontainer starts-->
		<div class="innercontainer" id="innercontainer">
			<div class="VCcentercontent">
				<b>${userForm.headerText}</b>
				<div id="img" align="center">
					<c:if test="${userForm.formImage!=null}">
						<img src="/ImageLibrary/${userForm.formImage}" width="362px" height="82px">
					</c:if>
					<c:if test="${userForm.formImage==null}">
						<p id="msg">(Your logo here)</p>
					</c:if>
				</div>
				<span class="formtxt"> Mileage_______________</span>
				</div>
				<div class="clear"></div>
				<!--Vc Inner container starts-->
				<div class="VCcontinner">
					<!--List left starts-->
					<div class="listcontatleft">
						<input type="hidden" name="headerColor" id="headerColor"
							value="${userForm.headingColor}" />
						<!-- List out first eight elements -->
						<input type="hidden" name="templateId" id="templateId"
							value="${templateId}" />
						<ul class="VClist">
							<c:forEach items="${leftUserFormComponents}"
								var="userFormComponent" varStatus="status">
								<li><c:if test="${userFormComponent.status.statusId == 1}">
										<div id="visibleDiv${status.count}" class="ListDivVisible">
									</c:if> <c:if test="${userFormComponent.status.statusId == 2}">
										<div id="visibleDiv${status.count}" class="ListDivHidden">
									</c:if>
									<div class="VClistdiv">
										<span class="text" style="display: -moz-inline-stack;text-align: left;width: 100%;"><nobr>${userFormComponent.component.componentName}</nobr></span>
										<c:if
											test="${(status.count != 1)  && (status.count != 3) && (status.count != 6)}">
											<span class="subText"><nobr>${userFormComponent.component.subComponents.label}</nobr>
											</span>
										</c:if>
										<c:forEach
											items="${userFormComponent.component.subComponents.subComponentsOptions}"
											var="subComponentOption">
											<c:if test="${subComponentOption.status == 'true'}">
												<div class="checkboxcnt">
													<div class="checkbox">
														<span> <c:forEach var="option"
																items="${fn:split(subComponentOption.options.optionDescription, '^')}">
																<img src="../images/checkbox.png">
																<label class="option">${option}</label>
															</c:forEach> </span>
													</div>
												</div>
											</c:if>
										</c:forEach>
									</div>
									<div class="clear"></div></li>
							</c:forEach>
						</ul>
					</div>
					<!--List left Ends-->

					<!--List right starts-->
					<div class="listcontatright">
						<!-- List out rest seven elements -->
						<ul class="VClist">
							<c:forEach items="${rightUserFormsComponents}"
								var="userFormComponentRight" varStatus="status">
								<li><c:if
										test="${userFormComponentRight.status.statusId == 1}">
										<div id="visibleDiv${status.count+8}" class="ListDivVisible">
									</c:if> <c:if test="${userFormComponentRight.status.statusId == 2}">
										<div id="visibleDiv${status.count+8}" class="ListDivHidden">
									</c:if>
									<div class="VClistdiv  ">
										<span class="text" style="display: -moz-inline-stack;text-align: left;width: 100%;"><nobr>${userFormComponentRight.component.componentName}</nobr></span>
										<c:if test="${(status.count != 5)}">
											<span class="subText"><nobr>${userFormComponentRight.component.subComponents.label}</nobr>
											</span>
										</c:if>
										<c:forEach
											items="${userFormComponentRight.component.subComponents.subComponentsOptions}"
											var="subComponentOptionRight">
											<c:if test="${subComponentOptionRight.status == 'true'}">
												<c:choose>
													<c:when test="${(status.count) == 7}">
														<c:forEach var="option"
															items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}">
															<div class="checkboxcnt">
																<div class="checkbox">
																	<span> <img src="../images/checkbox.png" class="rectangle" />
																		<label class="optionText">${option}</label> </span>
																</div>
															</div>
															<br>
														</c:forEach>
													</c:when>
													<c:otherwise>
														<div class="checkbox">
															<span> <c:forEach var="option"
																	items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}">
																	<img src="../images/checkbox.png" class="rectangle" />
																	<label class="optionText">${option}</label>
																</c:forEach> </span>
														</div>
													</c:otherwise>
												</c:choose>
											</c:if>
										</c:forEach>
									</div>
									<div class="clear"></div></li>
							</c:forEach>
						</ul>
					</div>
				</div>
				<!--List right Ends-->
				<div class="clear"></div>

				<!--Vc Inner container Ends-->
				<div class="editview">
					<span id="cmt">Comments:</span>___________________________________________________________________________________
					<span id="cmtline">______________________________________________________________________</span>
					<span id="cmtline">______________________________________________________________________</span>
					<span id="cmt"> <nobr>
							Inspected By:<span id="cmtlne">_________________________________</span>
						</nobr>&nbsp;Date: <span id="cmtlne">__________________________________________</span> </span>
				</div>
				<br />
				<br />
			
		</div>
	</div>
</div>