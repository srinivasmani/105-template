<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<title>User Form Edit</title>
<!-- Style Sheet -->
<link rel="stylesheet" type="text/css" href="../css/style-edit.css"></link>
<link type="text/css" rel="stylesheet" href="../css/jquery.miniColors.css" />
<!-- Java Script -->
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery-editTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery-userEditTemplate.js"></script>	
<script type="text/javascript" src="../js/jquery.miniColors.js"></script>
<script type="text/javascript" src="../js/jquery.mini.color.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>
<link type="text/css" rel="stylesheet" href="../css/pagination.css" />
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<style>
.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity = 80);
	display: none;
}

.box_content {
   /* height: 30%;*/
	position: fixed;
	top: 10%;
	left: 28%;
	right: 30%;
	width: 575px;
	border: 8px solid #ACACAC;
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	border: 5px solid #ACACAC;
	background: none repeat scroll 0 0 #FFFFFF;
}
#msg{
	font-size: 10pt;
    margin-top: 6%;
    letter-spacing:0.5px;
}
#inpDefault{
	width: 20px;
	height: 20px;
	background-color: #045fb4;
}
#imgLogo {
    border: 1px solid black;
    height:72px;
    margin-left: auto;
    margin-right:auto;
    margin-top: 0px;
    padding: 0;
    text-align: center;
    width: 336px;
}
#item .odd, #item .even {
border: 1px solid #ACACAC !important;
margin-right:5px;
margin-bottom:5px;
}
.selectLink {
    left: 36px !important;
    position: relative;
}
</style>
<body>
<!--container starts-->
<div class="container">
<input type="hidden" id=reqHeight name="reqHeight" value="72" />
<input type="hidden" id="reqWidth" name="reqWidth" value="336" />
<input type="hidden" id="current_label_id" value="" /> 
<input type="hidden" id="current_sublabel_id" value="" />
  	<!--Header starts-->
    <div class="header"></div>
    <!--Header Ends-->
  	<!--Center Container starts-->
  	<div class="container">
  	 <%@ include file="WEB-INF/header/userNavigation.jsp"%>
    <div class="insidecentercontainer">
      <!--Navigation starts-->
        <div class="nav">
        <ul>
			<c:if test="${userForm.status != 'default'}">
				<li><a href="javascript:void(0);" id="formNameTab" class="selected" >${userForm.formName}</a></li>
			</c:if>
			<c:if test="${userForm.status == 'default'}">
				<li><a href="./navigateTemplateView.do" class="selected" onclick="launchWindow('#dialog');">Template 101</a>
				</li>
				<li><a href="./getUserComponentIfs.do" onclick="launchWindow('#dialog');">Template 102</a>
				</li>
				<li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a>
				</li>
				<li><a href="./createUserComponentForm104.do" onclick="launchWindow('#dialog');">Template 104</a>
				</li>
                                <li><a href="./createUserComponentForm105.do" onclick="launchWindow('#dialog');">Template 105</a>
				</li>
			</c:if>
			</ul>
        </div>  
        <div style="clear:both"></div>
         <!--Navigation Ends-->
         <!--Innercontainer starts-->
         <div  class="insideinnercontainer" >
       	  	<div style="width:576px;margin:16px 5px; float:left;">
          		<div style="width:579px; border:1px #000 solid; float:left;">
          		<input type="hidden" name="currentDiv" id="currentDiv" value="compDiv_1" />
        	 	<input type="hidden" name="prevDiv" id="prevDiv" value="compDiv_1" />
        	 	<input type="hidden" name="edited" id="edited" value="false" />
         	 	<form id="updateForm" name="updateForm" action="./updateForm.do" method="post" >
            	<!-- Template will be shown up here -->
						<!-- Header Starts -->
						<c:if test="${userForm != null }">
							<div id="header">
								<p id="headerText" style="display: block; margin-left: 100px;">${userForm.headerText}</p>
								<input type="text" value="${userForm.headerText}" id="editText"
									style="display: none;" maxlength="24">
							</div>
							<a href="#" id="editHeading" style="position:relative; top:-30px;">
								<img src="../images/ieditover.PNG" width="16px" height="17px">
							</a> 
							<a href="#" id="okHeading" style='display:none;position:relative; top:-30px;'>
								<img id="iok" src="../images/iok.png" width="16px" height="17px">
							</a>
							<!-- Header Ends -->
							<div id="imgLogo" align="center">
							<c:if test="${userForm.formImage !=null && userForm.formImage != ''}">
								<img src="/ImageLibrary/${userForm.formImage}" id="imageSize" style=" margin:0;max-width: 336px;  max-height: 72px;" >
							</c:if>
							<c:if test="${userForm.formImage == null || userForm.formImage == ''}">
								<p id="msg">(Your logo here)<br/>Dimension:[340 x 75]pixels</p>
							</c:if>
							</div>
						</c:if>
						<c:if test="${userForm == null }">
							<div id="header">
							<p id="headerText" style="display: block; margin-left: 133px;">${template.headerText}</p>
							<a href="#" id="editHeading" style="display: none;"><img
								src="../images/iediting.PNG"> </a> <input type="text"
								value="${userForm.headerText}" id="editText"
								style="display: none;margin-left: 55px;">
							</div>
							<!-- Header Ends -->
							<div id="imgLogo" align="center">
								${template.templateImage}
								<c:choose>
									<c:when test="${template.templateImage eq null || template.templateImage eq ''}">
										<p id="msg">(Your logo here)<br/>Dimension:[340 x 75]pixels</p>
									</c:when>
									<c:otherwise>
										<img src="/ImageLibrary/${template.templateImage}" style="margin-left: -1px; width: 336px; margin-top: -1px; height: 72px;">
									</c:otherwise>
								</c:choose>
							</div>
						</c:if>
						<div class="text_field">Mileage_________________</div>
           
            		<!--Vc Inner container starts-->
            		<div id="VCcontinner" class="VCcontinner">
           			
           			<!--List left starts-->
           			
           			<input type="hidden" name="userFormId" id="userFormId" value="${userForm.userFormId}" />
           			<input type="hidden" name="templateId" id="templateId" value="${templateId}" />
           			<input type="hidden" name="headingText" id="headingText" value="${userForm.headerText}" /> 
					<input type="hidden" name="saveImage" id="saveImage" value="${userForm.formImage}" /> 
           			<input type="hidden" name="headerColor" id="headerColor" value="${userForm.headingColor}" />
           			<input type="hidden" name="currHeaderColor" id="currHeaderColor" value="${userForm.headingColor}" />
	            		<div class="listcontatleftEdit">
	            		<!-- List out first eight elements -->
	               		<ul class="VClist">
	               		<c:forEach items="${leftUserFormComponents}" var="userFormComponent" varStatus="status" >
	               		<input type="hidden" name="viewComponent${status.count}" id="viewComponent${status.count}" value="${userFormComponent.userFormsComponentId}" />
	               		<input type="hidden" name="templateComponent${status.count}" id="templateComponent${status.count}" value="${userFormComponent.userFormsComponentId}" />
	               		<input type="hidden" name="viewPosition${status.count}" id="viewPosition${status.count}" value="${status.count}" />
						<input type="hidden" name="templatePosition${status.count}" id="templatePosition${status.count}" value="${userFormComponent.position.positionId}" />
						<input type="hidden" name="componentName${status.count}"  id="componentName${status.count}" value="${userFormComponent.component.componentName}" />
						<input type="hidden" name="label${status.count}"  id="label${status.count}" value="${userFormComponent.component.subComponents.label}" />
                        <li>
                        <c:if test="${userFormComponent.status.statusId == 1}">
                        <input type="hidden" name="visibility${status.count}" id="visibility${status.count}" value="" class="displayDiv" />
	                    <div id="visibleDiv${status.count}" class="ListDivVisible">
	                    </c:if>
	                    <c:if test="${userFormComponent.status.statusId == 2}">
	                     <input type="hidden" name="visibility${status.count}" id="visibility${status.count}" value="hidden" class="displayDiv" />
	                    <div id="visibleDiv${status.count}" class="ListDivHidden">
	                    </c:if>
	                    <c:if test="${userFormComponent.status.statusId == 3}">
                        <input type="hidden" name="visibility${status.count}" id="visibility${status.count}" value="deleted" class="displayDiv" />
	                    <div id="visibleDiv${status.count}" class="ListDivHidden">
	                    </c:if>
                        <div id="compDiv_${status.count}" class="ListDiv">
                        	<span class="text" id="headingSpan${status.count}">${userFormComponent.component.componentName}</span>
                        	 <img class="iImageEdit handSymbol" src="../images/ieditover.PNG" id="editComp_${status.count}"> 
						<img class="iDeleteEdit handSymbol" src="../images/iDelete.png" id="delComp_${status.count}">
	                          <c:if test="${(status.count != 1)  && (status.count != 3) && (status.count != 6)}">
								  <span class="subText" id = "labelSpan${status.count}">${userFormComponent.component.subComponents.label}</span>
							   </c:if>
						   <div id="optDivParent${status.count}">
                           <c:forEach items="${userFormComponent.component.subComponents.subComponentsOptions}" var="subComponentOption" varStatus="statLeft">
                           <c:if test="${subComponentOption.status == 'true'}">
                           <c:set var="qt1" value= "\"" scope="page" />   
						   <c:set var="qt2" value= "&#34;" scope="page" />  
                           <input type="hidden" name="scoptionId${status.count}"  id="scoptionId${status.count}" value="${subComponentOption.subComponentOptionsId}" />
                           <input type="hidden" name="optionId${status.count}"  id="optionId${status.count}" value="${subComponentOption.options.optionId}" />
                           <input type="hidden" name="optionValue${status.count}"  id="optionValue${status.count}" value="${fn:replace(subComponentOption.options.optionDescription, qt1, qt2)}" />
                           <input type="hidden" name="optionString${status.count}" id="optionString${status.count}" value="${fn:replace(subComponentOption.options.optionDescription, qt1, qt2)}" />
                           <input type= "hidden" id="optType${status.count}" name="optType${status.count}" value="${subComponentOption.optionType}" />
                           <div class="checkboxcnt" id="optDiv${status.count}">
                              <div class="checkbox">
                                <span>
								<c:forEach var="option" items="${fn:split(subComponentOption.options.optionDescription, '^')}" varStatus="stat">
								<c:if test="${option != '' && option != null }">
								<img src="../images/${fn:toLowerCase(subComponentOption.optionType)}.png" class="imageOptionType" id="image_${status.count}${stat.count}">
								<label id="opt${status.count}${stat.count}" class="optionText">${option}</label>
								</c:if>
			                 	</c:forEach>
			                 	</span>
                           </div>
                           </div>
                           </c:if>
                           </c:forEach>
                           </div>
                           <div class="clear"></div>
                          </div>
                         </li>
		                 </c:forEach>   
		            </ul>
	            </div>
                <!--List left Ends-->
                
                <!--List right starts-->
                <div class="listcontatrightEdit">
                 <!-- List out rest seven elements -->
	                <ul class="VClist">
	                <c:forEach items="${rightUserFormsComponents}" var="userFormComponentRight" varStatus="status" >
	                	<input type="hidden" name="viewComponent${status.count+8}" id="viewComponent${status.count+8}" value="${userFormComponentRight.userFormsComponentId}" />
                        <input type="hidden" name="viewPosition${status.count+8}" id="viewPosition${status.count+8}" value="${status.count+8}" />
                        <input type="hidden" name="templateComponent${status.count+8}" id="templateComponent${status.count+8}" value="${userFormComponentRight.userFormsComponentId}" />
						<input type="hidden" name="templatePosition${status.count+8}" id="templatePosition${status.count+8}" value="${userFormComponentRight.position.positionId}" />
						<input type="hidden" name="componentName${status.count+8}"  id="componentName${status.count+8}" value="${userFormComponentRight.component.componentName}" />
						<input type="hidden" name="label${status.count+8}"  id="label${status.count+8}" value="${userFormComponentRight.component.subComponents.label}" />
	                    <li>
	                    <c:if test="${userFormComponentRight.status.statusId == 1}">
	                    <input type="hidden" name="visibility${status.count+8}" id="visibility${status.count+8}" value="" class="displayDiv" />
	                    <div id="visibleDiv${status.count+8}" class="ListDivVisible">
	                    </c:if>
	                    <c:if test="${userFormComponentRight.status.statusId == 2}">
	                    <input type="hidden" name="visibility${status.count+8}" id="visibility${status.count+8}" value="hidden" class="displayDiv" />
	                    <div  id="visibleDiv${status.count+8}" class="ListDivHidden">
	                    </c:if>
	                     <c:if test="${userFormComponentRight.status.statusId == 3}">
	                    <input type="hidden" name="visibility${status.count+8}" id="visibility${status.count+8}" value="deleted" class="displayDiv" />
	                    <div id="visibleDiv${status.count+8}" class="ListDivHidden">
	                    </c:if>
	                    <div id="compDiv_${status.count+8}" class="ListDiv">
	                        <span class="text" id="headingSpan${status.count+8}">${userFormComponentRight.component.componentName}</span>
	                         <img class="iImageEdit handSymbol" src="../images/ieditover.PNG" id="editComp_${status.count+8}"> 
						<img class="iDeleteEdit handSymbol" src="../images/iDelete.png" id="delComp_${status.count+8}">
	                        <c:if test="${(status.count != 5)}">
								   <span class="subText" id = "labelSpan${status.count+8}">${userFormComponentRight.component.subComponents.label}</span>
						    </c:if>
					    <div id="optDivParent${status.count+8}">
                         <c:forEach items="${userFormComponentRight.component.subComponents.subComponentsOptions}" var="subComponentOptionRight" >
                         <c:if test="${subComponentOptionRight.status == 'true'}">
                         	<c:set var="qt1" value= "\"" scope="page" />   
						   	<c:set var="qt2" value= "&#34;" scope="page" />
                       		 <input type="hidden" name="scoptionId${status.count+8}"  id="scoptionId${status.count+8}" value="${subComponentOptionRight.subComponentOptionsId}" />
	                         <input type="hidden" name="optionId${status.count+8}"  id="optionId${status.count+8}" value="${subComponentOptionRight.options.optionId}" />
	                         <input type="hidden" name="optionValue${status.count+8}"  id="optionValue${status.count+8}" value="${fn:replace(subComponentOptionRight.options.optionDescription, qt1, qt2)}" />
	                         <input type="hidden" name="optionString${status.count+8}" id="optionString${status.count+8}" value="${fn:replace(subComponentOptionRight.options.optionDescription, qt1, qt2)}" />
	                         <input type= "hidden" id="optType${status.count+8}" name="optType${status.count+8}" value="${subComponentOptionRight.optionType}" />
                         	 <div class="checkboxcnt" id=optDiv${status.count+8}>
		                        <c:choose>
		                          <c:when test="${(status.count) != 7}">
		                           <div class="checkboxcnt" >
		                          <c:forEach var="option" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}" varStatus="statRight1" >
		                          <div class="checkbox">
	                           		 <span>
	                           		 <c:if test="${option != '' && option != null }">
										<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png" class="imageOptionType" id="image_${status.count+8}${statRight1.count}">
										<label id="opt${status.count+8}${statRight1.count}" class="optionText">${option}</label>
									</c:if>
									</span>
	                         	 </div>
                 				  </c:forEach>
                 				   </div>
		                          </c:when>
		                          <c:otherwise>
		                           <div class="checkboxcnt">
		                           <span>
									 	<c:forEach var="option" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}" varStatus="statRight2">
									 	<div class="checkbox15">
									 	<c:if test="${option != '' && option != null }">
										<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png" class="imageOptionType" id="image_${status.count+8}${statRight2.count}"> 
										<label id="opt${status.count+8}${statRight2.count}" class="optionText">${option}</label>
										</c:if>
										</div>
		                 				</c:forEach>
		                 				</span>
	                         		</div>
                         		</c:otherwise>
                         		</c:choose>
                       		</div>
                       </c:if>
                       </c:forEach>
                       </div>
                         <div class="clear"></div>
                         </div>
                         </div>
	                   </li>
                   </c:forEach>
                   </ul>
                 </div>
                 <!--List right Ends-->
                 <div class="clear"></div>
                
            </div>
            <div class="clear"></div>
              <!--Vc Inner container Ends-->
              	<div class="bottomtext">
				<span style="float:left;">Comments:</span>
				<hr style="width: 87%; margin-top:4%;" /><br />
				<hr style="width: 99%;" /><br />
				<hr style="width: 99%;" /><br />    
				<span style="float: left; clear: left; margin-top: -4%;">Inspected by: </span> 
				<hr style="width: 45%;float:left;position:relative; top:-3px;" />
				<span style="float: left; margin-top: -4%; width: 20.5%;">Date:</span>
				<hr style="width: 33.5%;float:left;margin-left:-14%;position:relative; top:-3px;" />
				</div>
               <div class="clear"></div>
                </div>
            <!--Container Bottom Starts-->
            <div class="containerbtm" style="padding-left:200px;">
        		<a id="updateId" class="btn-txt" onclick="launchWindow('#dialog');">Save</a>
     			<a id="cancelButtonId" href="./userFormView.do?formid=${userForm.userFormId}" class="btn-txt" onclick="launchWindow('#dialog');">Cancel</a>
            </div>
            <!--Container Bottom Ends-->
            <div class="clear"></div>
             </div>
                  <!--Innercontainer left starts-->
					<div style="float: left; width: 333px; margin: 16px 5px;">
					<div class="rightcontainer">
						<div class="sideheading">
							<label>Form name : </label>
							<div>
								<input type="text" id = "formName" name = "formName" value="${userForm.formName}" maxlength="20" />
							</div>
						</div>
					</form> 
					<form name="uploadLocalImage" action="./userFormEdit.do" method="post" id="uploadLocalImage" enctype="multipart/form-data">
						<div class="sideheading"><label>Upload your own image:</label>
						<div>
						
						   <input type="hidden" id="center" name="center" value="template1">
				           <input type="hidden" id="centerimageHeight" name="centerimageHeight" value="72">
				           <input type="hidden" id="centerimageWidth" name="centerimageWidth" value="336">
							<input name="fileImage" type="file" id="localImage" value="${logoImage}" />
							<c:choose>
							<c:when test="${userForm.formImage != null && userForm.formImage != ''}">
								<span id="delBtn">
									<input type="button" value="Delete" id="deleteButton"/>
								</span>
							</c:when>
							<c:otherwise>
								<span id="delBtn"></span>
							</c:otherwise>
							</c:choose>	
						</div>
						</div>
					</form>
					<form name="uploadRImage" action="./browseImage.do?type=template1" method="post"
					id="uploadRImage" enctype="multipart/form-data">
					<div class="sideheading">
						<label><nobr>Choose from existing images :</nobr>
						</label>
					</div>
					<div style="width:340px;">
						<input type="text" readonly="readonly" style="height: 22px;width: 143px;float:left;">
						<input type="submit" value="Browse..." id="browse" onclick="displayHideBox('1'); return false;"
							 style="left: -80px;margin-top: 0px;position: relative;float:left;" >
						<c:choose>
							<c:when test="${userForm.formImage != null && userForm.formImage != ''}">
								<span id="delBtn1">
									<input type="button" value="Delete" id="deleteButton1" style="display:none;margin-left: 3px;margin-top: -3px;width:74px;height:27px;"/>
								</span>
							</c:when>
							<c:otherwise>
								<span id="delBtn1"></span>
							</c:otherwise>
						</c:choose>
						<div id="grayBG" class="grayBox">
						</div>
						<div id="LightBox1" class="box_content" style="display: none;"
							style="background: #fff; width: 100%;">
							<span style="float: right;"><img src="../images/icon-delete.gif" onclick="displayHideBox('1')"></span>
							<div style="background: #fff; width: 100%;">
								<div onclick="displayHideBox('1'); return false;"
									style="cursor: pointer;" align="right">
									
								</div>
								<p>
								<div id="lbox"></div>
								</p>
							</div>
						</div>
					</div>
				</form>
					<!-- <div class="sideheading">
							<label>Choose From Existing Images </label> <input type="file" />
					</div> -->
							<div class="sideheading" style="clear:left;">
					     	 	<label>Heading color</label>
			    		 	</div>
				    	  <div id="iColor">
						       		<input type="image" name="color3" class="colors" value="#123456" id="inp1" src="../images/images.png"/>
						       		<!-- <input type="image" class="color" name="clr" id="inp1" src="../images/images.png"> -->
						      </div>
				    	 <div class="sideheading">
							 <label>Default Heading color:</label> <input type="button"  id="inpDefault" style="cursor: pointer;" />
						 </div>
                     	</div>               
                     </div>
    				<div id="editComponent" style="float: left; width: 333px; display: none; margin: 0px 5px;">
                     	<div class="rightcontainer">
                     	<div class="sideheading">
                     	<input type="hidden" name="lastEditOptionDiv" id="lastEditOptionDiv" value="" /> 
                     	<label>Position on the template :</label><label id="showPosition"></label>
                     	</div>
                     	<div class="sideheading">
                     	 <label>Heading</label><br />
                         <input name="TextboxExample" type="text" maxlength="25" id="changedHeading" tabindex="2"
							onchange="DropDownIndexClear('DropDownExTextboxExample');"  onkeyup="editHeadingLabel(this.id)"
							style="width: 215px; height: 17px; position: absolute; border-right: none; border-bottom:none;" />
							<select name="CompDrp" id="CompDrp" tabindex="1000" size="1"  style="text-transform: capitalize; font-size: 12pt; font-family: 'MyriadProRegular', Arial; ">
								<option value="0">--Select--</option>
								<c:forEach items="${compDrp}" var="entry" varStatus="status">
									<option value="${entry.key}" id="headingValue">${entry.value}</option>
								</c:forEach>
							</select>
						</div>
						
						<div id="compDetails">
						<!-- Added for drefault values -->
						<div>
						<div class="sideheading">
							<label>Subheading</label>
							<br />
						  	<input type="text" value="" id="editLabel" onkeyup="editLabel(this.id)" maxlength="24" />
						</div>
						<div class="sideheading">
							<label>Option</label>
							<br />
							<div id="editOpdtionDiv">
							  	<select id="editOption" onchange = "changeOPtion(this.id)">
							  			<option value="${options.subComponentOptionsId}" selected="selected">${options.options.optionName }</option>
							  			<option value="${options.subComponentOptionsId }">${options.options.optionName }</option>
							  	</select>
						  	</div>
						</div>
	
						<div class="sideheading" id="tickType">
								<select id="editOptionType" name="editOptionType">
									<option id="1" value="Square">Square</option>
									<option id="2" value="Circle">Circle</option>
									<option id="3" value="None">None</option>
								</select>
						</div>
	
	
						<div class="sideheading" id="editDesc">
							 <div id="optionList">
								 
							 </div>
						</div>
</div>
						
						<!-- ends the default div -->
						
						</div>
                        
                        <div class="sideheading" style="text-align:center;">
                          <input id="rightOk" class="btn_forright"  type="button"  value="Ok"/>
                          <input id="rightCancel" class="btn_forright" type="button"  value="Cancel"/>
                        </div>
                        </div>               
                     </div>           
                  <!--Innercontainer left Ends-->
              </div>
             <!--Innercontainer  Ends-->
  </div>
    	  <!--Center Container Ends-->
    </div>
<!--container ends-->
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
