<!DOCTYPE html>
<html>
<head>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<%@ include file="UploadImage.jsp"%>


<title>Admin Edit Template 4</title>


<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/Jquery-Validation.js"></script>
<script type="text/javascript" src="../js/Template104.js"></script>
<script type="text/javascript" src="../js/jquery-adminEditTemplate.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>

<link rel="stylesheet" type="text/css" href="../css/stylesheet.css">
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<link rel="stylesheet" type="text/css" href="../css/pagination.css">
<style>
.box_content {
	position: fixed;
	left: 25%;
	right: 30%;
	width: 575px;
	/*height: 30%;*/
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	background: none repeat scroll 0 0 #FFFFFF;
	border: 8px solid #ACACAC;
}

#img {
	position: relative;
	float: none;
}

#img #text {
	left: 74px;
	position: absolute;
	top: 7px;
	width: 300px;
}

#repositoryPopUp {
	height: auto !important;
	border: 8px solid #acacac !important;
	width: 625px !important;
	margin-top: -65px;
}

#item .odd,#item .even {
	border: 1px solid #ACACAC !important;
	margin-right: 5px;
	margin-bottom: 5px;
}

.selectLink {
	left: 0px !important;
	position: relative;
}

#commentDiv {
	font-family: 'MyriadProRegular', Sans-Serif;
	font-size: 11pt;
	letter-spacing: 0.5px;
	width: 900px;
}
</style>

</head>
 <body>
<div class="container" style="padding:0px;">
<%@ include file="WEB-INF/header/userNavigation.jsp"%>
<div id="centercontainer" class="centercontainer">

		<div class="nav">
			<ul>
				<li><a href="./navigateTemplateView.do">Template 101</a>
				</li>
				<li><a href="./navigateTemplateIfs.do" >Template 102</a>				
				</li>
				<li><a href="./navigateTemplate103.do">Template 103</a>
				</li>
				<li><a href="./navigateTemplate104.do" class="selected">Template 104</a>
				</li>
                                <li><a href="./navigateTemplate105.do" >Template 105</a>
				</li>
				<li><a href="./addImageToLibrary.do" >Add Image</a>
				</li>
			</ul>
		</div>
<form id="updateAdminTemplate104" name="updateAdminTemplate104" action="./updateAdminTemplate104.do" method="post">
<input type="hidden" id="imgType" value="${imgType}">
<input type = "hidden" name = "saveImage" id="saveImage104" value = "${template.templateImage}"> 

<div style="clear: both"></div>
  <div class="innercontainer">
	<div id="templateSize">
        <div style="width: 21.5cm;" class="divLogoTab">
			 <div class="divLogoRow">
				<div class="logo1">
					<div class="leftOptImage103" id="logoLeft">
					</div>
				</div>
				<div id="imgLogo" align="center" class="imgLogo">
				<c:choose>
				 <c:when test="${template.templateImage!=null && template.templateImage !=''}">
						<img src="/ImageLibrary/${template.templateImage}" style="max-width:384px; max-height:72px;">
				 </c:when>
				 <c:otherwise>
				 	<p id="msg">(Your logo here)<br/>Dimension:[380 x 75]pixels</p>
				 </c:otherwise>
				</c:choose>
				</div>
				<div style="width:300px;margin:0 auto;clear:both;text-align:center;">
			  	 <a href="javascript:void(0);" id="centerImageClick" onclick="centerImageSelectionPop('center104',384,72); return false;" style="height: 0;font-family: 'MyriadProRegular';font-size: 11pt;">Click to upload logo</a>&nbsp;&nbsp;<span class="imgDimension">[380 x 75]</span>
			   </div>
				<div class="logo2-103">
					<div class="rghtOptImage103" id="logoRight">
					</div>
				</div>
			 </div>
		</div>
  
     <div id="pageHeading">
	 <p id="heading" class="text_label4">
		<label id="lblText" class="text_label4" />
		
		<c:if test="${template.headerText}">
									<c:set var="componentDesc" value="${template.headerText}"/>
							</c:if>
							<span id="cmt" style="float:left;clear:left;">
								<input type="text" class="inputBorder"  name="headerText" id="headerText"  value="${template.headerText}"  onkeypress="return limitOfCharHeader(event,this.id,25)" 
								  size="100" disabled="disabled" maxlength="30" style="width: 620px; height: 30px; font-weight: bold; text-align: center;text-transform: uppercase;" onblur="removeEditHeader('headerText', 25)"  />
							</span> 
					   </label>
						<span style="float: right;cursor: pointer;">
								<img src="../images/ieditover.PNG"  onclick="editTextHeader('headerText')" id="editButtonheaderText"/>
								<img src="../images/ieditOk.PNG"  onclick="removeEditHeader('headerText', 25)" id="okButtonHeader" style="display: none;"  />
		                </span>
		
	 </p>
	 <br/>
	 <!--<div class="edit"></div>-->																
	 <div class="clear"></div>
	</div>
	<div class="divTable">
	 <div class="divRow">
		<div id="customer" class="divCell"  style="margin-left:25px;">
				  <p  id="customerP" class="inspec_formp moreWidth"  style="border: 1px solid #000;padding:0 0 !important;width:755px;">
		   <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[0].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[0].componentName}"/>
			</c:if>
			    
					
				 <input type="hidden" name="componentId0" value="${template.components[0].componentId}" />
						<input type="hidden" name="component0" id="component0"  value="" />
					  	<c:set var="cname" value=" "/>
						<c:forEach var="cname" items="${fn:split(template.components[0].componentName, '~')}" varStatus="stat">
						  	<c:if test="${stat.count == 1}">
						      <input type="text" class="inputBorder"  name="component_0${stat.count}" id="component_0${stat.count}"  value="${cname!='' ? cname :''}"  disabled="disabled"  style=" width:753px;display:block;padding:5px 0; " onkeypress="return limitnofotextCustomer(this.id,event);"   onblur="comp01FocusOut();" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" />
						      </c:if>
						      <c:if test="${stat.count == 2}">
						      <input type="text" class="inputBorder"  name="component_0${stat.count}" id="component_0${stat.count}"  value="${cname!='' ? cname :''}"  disabled="disabled"  style=" width:753px;display:block;padding:5px 0; " onkeypress="return limitnofotextCustomer(this.id,event);"   onblur="comp02FocusOut();" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false"/>
						     </c:if>
						</c:forEach>
					  
					  
			</p>
				<span style="float: right;cursor: pointer;">
						<img src="../images/ieditover.PNG"  onclick="editTextCustomer('component')" id="editButton0"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditCustomer('component',250)" id="okButton0" style="display: none;"  />
		  		</span>	
		
										
		</div>						
	 </div>
	</div>
		<div id="commentDiv">
		<span id="temp1"></span>
	</div>						
	<div class="clear"></div>
	<div class="selTable">
	<div style="padding:15px 25px;">
	
		<%       
		        String getSVG = (String) request.getAttribute("imgType");
		 		String circle = "";
		 		String square = "";
				if (getSVG.equalsIgnoreCase("circle")) 
					circle= "checked";
				else
					square = "checked";
	    %>
			<input type="radio" name="imgType" value="circle" <%= circle %> /> <span class="circleSuare">Circle</span>
			</span>
			<span>
				<input type="radio" name="imgType" value="square"  <%= square %> /> <span class="circleSuare">Square</span>
			</span>
	</div>


	 <div class="selRow">
		<div class="selCol selCol3">
		  <div class="selCell">	
		  	<span><b class="green"></b></span>
			
			
			<!--component edit and delete start -->
			
			<span class="floatLeft">			 
			<span id="divTxt_lights"  >
			
			
			 <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[1].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[1].componentName}"/>
			</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width4">
					 <input type="hidden" name="componentId1" value="${template.components[1].componentId}" />
						<input type="text"    class="inputBorder"  name="component1" id="component1"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
						  size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component1',20)"    />
					</span>
					 
			</span>
				<span style="float: right;" class="handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component1')" id="editButton1"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component1',20)"   id="okButton1" style="display: none;"  />
					 
				</span>
			</span>
			
		 <br /> <input type="image" src="../images/images.png" style="display: none; margin-left: 11px; margin-top: -5px;" id="inp1" name="clr" class="color" />
		  </div>
		</div>
		<div class="selCol selCol3">
		  <div class="selCell">								
			<span><b class="yellow"></b></span>
			<span id="nobr" class="floatLeft">
			
			<span class="floatLeft">			 
			<span id="divTxt_lights"  >
			<c:set var="componentDesc" value=""/>
			<c:if test="${template.components[2].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[2].componentName}"/>
			</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width4">
					 <input type="hidden" name="componentId2" value="${template.components[2].componentId}" />
						<input type="text"   class="inputBorder" name="component2" id="component2"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
						  size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component2', 20)"/>
					</span>
					 
			</span>
				<span style="float: right;" class="handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component2')" id="editButton2"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component2', 20)" id="okButton2" style="display: none;"  />
					 
				</span>
			</span>
			 </span>
			 <br/> <input type="image" src="../images/images.png" style="display: none; margin-left: 11px; margin-top: -5px;" id="inp2" name="clr" class="color" />
		  </div>
		</div>
		<div class="selCol selCol3">
		  <div class="selCell">
			<span><b class="red"></b></span>
			<span id="nobr" class="floatLeft">
			
			<span class="floatLeft">			 
			<span id="divTxt_lights"  >
			<c:if test="${template.components[3].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[3].componentName}"/>
			</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width4">
					 <input type="hidden" name="componentId3" value="${template.components[3].componentId}" />
						<input type="text" class="inputBorder"  name="component3" id="component3"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,20)" 
						  size="25" disabled="disabled" maxlength="20" onblur="removeEditMandatory('component3',20)"/>
					</span>
					 
			</span>
				<span style="float: right;" class="handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component3')" id="editButton3"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component3',20)" id="okButton3" style="display: none;"  />
					 
				</span>
			</span>
			 																		
			</span>
			<br /> <input type="image" src="../images/images.png" style="display: none; margin-left: 11px; margin-top: -5px;" id="inp3" name="clr" class="color" />
		  </div>
		</div>
						
	 </div>
	</div>
	<div class="clear"></div>
  
    <div class="divTable1 paddingLeft">
     <div  class="inspectionleftwrap">
      <div class="inspection_bg">
        <div class="inspectionTable">
        <div class="clear row1Title greyBg">
        
        <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[4].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[4].componentName}"/>
			</c:if>
					<span class="th">
					
					<input type="hidden" name="componentId4" value="${template.components[4].componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component4" id="component4"  value="${componentDesc}"  
						onkeypress="return limitOfCharForMandatory(event,this.id, 40)" maxlength="30" disabled="disabled" size="40" onblur="removeEditMandatory('component4', 40)"/>
					</span>
			 
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component4')" id="editButton4"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component4',40)" id="okButton4" style="display: none;"  />
            </span>
        </div>
        <div class="clear row1" style="text-align:center;padding:3px;">
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[5].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[5].componentName}"/>
			</c:if>
					 <span class="smallheading" style="float:left; width:150px;display:block; text-align:center; margin-left:115px;">
					 <input type="hidden" name="componentId5" value="${template.components[5].componentId}" />
						<input type="text" class="inputBorder"  name="component5" id="component5"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)"  
						  size="40" disabled="disabled" maxlength="40" onblur="removeEditMandatory('component5', 40)"/>
					</span>
			 
				<span class="EditBtnNew handSymbol" style="float:right;">
						<img src="../images/ieditover.PNG"  onclick="editText('component5')" id="editButton5"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component5',40)" id="okButton5" style="display: none;"  />
				</span>
      
        </div>
        <!--<div class="clear row1">
          <span  class="alignCenter"><img src="../images/car_3view.png" alt="" width="244" height="196" /></span>
        </div>
        -->
        <div class="clear row1" id="img">
			<div id="imgLeft" align="center" class="imgLeft" style="height:210px;margin-top:12px;">
                <span class="alignCenter" style="display:block; height:210px;margin-top:12px;">
					<img  src="/ImageLibrary/${template.components[70].componentName}" alt="" style="max-width:244px;max-height:192px;" />
				</span>
			</div>
			<div id="text" style="width:375px; text-align:center; top:0px;left:0px;">
				<a href="javascript:void(0);" id="leftImageClick" onclick="centerImageSelectionPop('left',244,192); return false;">Change Image</a>[245 x 190]
    		</div>
		</div>
        <div class="clear row1">
          <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[6].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[6].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId6" value="${template.components[6].componentId}" />
						<input type="text" class="inputBorder"  name="component6" id="component6"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component6', 35)"/>
					</span>	  
					<span class="EditBtnNew handSymbol">
							<img src="../images/ieditover.PNG"  onclick="editText('component6')" id="editButton6"/>
							<img src="../images/ieditOk.PNG"  onclick="removeEdit('component6',35)" id="okButton6" style="display: none;"  />
						 
					</span>
					
					 
			 
			
					 
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
        <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[7].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[7].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId7" value="${template.components[7].componentId}" />
						<input type="text" class="inputBorder"  name="component7" id="component7"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)"  
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component7', 35)"/>
					</span>	  
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component7')" id="editButton7"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component7',35)" id="okButton7" style="display: none;"  />
					 
					</span>
					
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
        <span>
       <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[8].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[8].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId8" value="${template.components[8].componentId}" />
						<input type="text" class="inputBorder"  name="component8" id="component8"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component8', 35)"/>
					</span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component8')" id="editButton8"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component8',35)" id="okButton8" style="display: none;"  />
					 
					</span>
					 
			</span>
			 
        
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[9].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[9].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId9" value="${template.components[9].componentId}" />
						<input type="text" class="inputBorder"  name="component9" id="component9"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component9', 35)" />
					</span>	  
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component9')" id="editButton9"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component9',35)" id="okButton9" style="display: none;"  />
					 
					</span>
					
		   <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
      </div>
	  <div class="inspectionTable">
        <div class="clear row1Title greyBg">
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[10].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[10].componentName}"/>
			</c:if>
					 <span align="center" class="th">
					 <input type="hidden" name="componentId10" value="${template.components[10].componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component10" id="component10"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)" 
						  size="40" disabled="disabled" maxlength="30" onblur="removeEditMandatory('component10', 40)"/>
					</span>
					 
			 
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component10')" id="editButton10"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component10',40)" id="okButton10" style="display: none;"  />
              </span>
        </div>
		    <div class="clear row1">
          <span style="width:300px; float:left;padding:15px 0;">
          
          
        <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[11].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[11].componentName}"/>
			</c:if>
					 <span class="textFont" style="display:block; float:left;margin-top:-5px;">
					 <input type="hidden" name="componentId11" value="${template.components[11].componentId}" />
						<input type="text" class="inputBorder"  name="component11" id="component11"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEditMandatory('component11', 35)"/>
					</span>	
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component11')" id="editButton11"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component11',35)" id="okButton11" style="display: none;"  />
				 </span>
					 
			</span>
			
				
             <div style="clear:both;padding:15px 15px 0 40px;;">
             <span>
             	<b class="green" ></b><b class="yellow" ></b><b class="red" ></b>
             </span>
			 </div>
		 
          <!--<div>
             <div style="width:120px; float:left;position:relative;left:100px;top:-30px;"><img src="../images/battery.png" width="125" height="111" /> </div>
		   </div>
        -->
         <div id="img">
			<div id="imgBottomLeft" align="center" class="imgBottomLeft" style="float: right;margin-top: -31px;;width: 115px;height:95px;position:relative;top:-23px; left:-20px;">
				<img src="/ImageLibrary/${template.components[71].componentName}" alt="battery" style="max-width: 115px;max-height: 90px;" id="image"/>
			</div>
			<div id="text" style="position:relative; top:40px; left: 137px;width: 250px;">
    			<a href="javascript:void(0);" id="bottomLeftImageClick" onclick="centerImageSelectionPop('bottemLeft',115,90); return false;" style="display:inline;margin-top:10px;">Change Image</a>[115 x 90]
			</div>
		</div>
        </div>
      </div>
      
      
      
      
      
      
	  <div class="inspectionTable">
        <div class="clear row1Title greyBg">
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[12].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[12].componentName}"/>
			</c:if>
					 <span align="center" class="th">
					 <input type="hidden" name="componentId12" value="${template.components[12].componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component12" id="component12"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)" 
						  size="40" disabled="disabled" maxlength="30" onblur="removeEditMandatory('component12', 40)"/>
					</span>
					 
			 
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component12')" id="editButton12"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component12',40)" id="okButton12" style="display: none;"  />
				</span>
        </div>
        <div class="clear row1">
          <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[13].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[13].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId13" value="${template.components[13].componentId}" />
						<input type="text" class="inputBorder"  name="component13" id="component13"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component13', 35)"/>
					</span>	  
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component13')" id="editButton13"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component13',35)" id="okButton13" style="display: none;"  />
					 
					</span>
					
 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[14].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[14].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId14" value="${template.components[14].componentId}" />
						<input type="text" class="inputBorder"  name="component14" id="component14"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component14', 35)"/>
						</span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component14')" id="editButton14"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component14',35)" id="okButton14" style="display: none;"  />
					 
					</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[15].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[15].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId15" value="${template.components[15].componentId}" />
						<input type="text" class="inputBorder"  name="component15" id="component15"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component15', 35)"/>
						</span>
						<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component15')" id="editButton15"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component15',35)" id="okButton15" style="display: none;"  />
					 
				
					</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[16].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[16].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId16" value="${template.components[16].componentId}" />
						<input type="text" class="inputBorder"  name="component16" id="component16"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component16', 35)"/>
						  </span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component16')" id="editButton16"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component16',35)" id="okButton16" style="display: none;"  />
					 
					</span>
			</span>
			
				 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[17].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[17].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId17" value="${template.components[17].componentId}" />
						<input type="text" class="inputBorder"  name="component17" id="component17"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35"  onblur="removeEdit('component17', 35)"/>
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component17')" id="editButton17"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component17',35)" id="okButton17" style="display: none;"  />
					 
					</span>
					 
			</span>
				
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[18].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[18].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId18" value="${template.components[18].componentId}" />
						<input type="text" class="inputBorder"  name="component18" id="component18"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component18', 35)" />
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component18')" id="editButton18"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component18',35)" id="okButton18" style="display: none;"  />
					</span>
					 
			</span>
				
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		<span>
		 <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[19].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[19].componentName}"/>
			</c:if>
					 <span class="inspectionTxtNew">
					 <input type="hidden" name="componentId19" value="${template.components[19].componentId}" />
						<input type="text" class="inputBorder"  name="component19" id="component19"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component19', 35)"/>
						  </span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component19')" id="editButton19"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component19',35)" id="okButton19" style="display: none;"  />
					 
					</span>
					 
			</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
      </div>
	   <div class="inspectionTable">
        <div class="clear row1Title greyBg">
        
        
        
        
         <c:set var="componentDesc" value=""/>
         <span>
			<c:if test="${template.components[20].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[20].componentName}"/>
			</c:if>
					 <span align="center" class="th">
					 <input type="hidden" name="componentId20" value="${template.components[20].componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component20" id="component20"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)" 
						  size="40" disabled="disabled" maxlength="30" onblur="removeEditMandatory('component20', 40)"/>
					</span>
					 
			</span>
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component20')" id="editButton20"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEditMandatory('component20',40)" id="okButton20" style="display: none;"  />
                </span>
        
         
        </div>
        <div class="clear row1">
        
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[21].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[21].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId21" value="${template.components[21].componentId}" />
						<!--<input type="text" class="inputBorder"  name="component21" id="component21"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" />
						  	-->
						  	 <textarea rows="1" cols="10"  class="inputBorder"  name="component21" id="component21"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,55)" 
						  size="29" disabled="disabled" style="height: 42px;" onblur="removeEdit('component21', 55)" maxlength="55">${componentDesc}</textarea> 
						  	
						  	</span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component21')" id="editButton21"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component21',55)" id="okButton21" style="display: none;"  />
					 
					</span>
					 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[22].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[22].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId22" value="${template.components[22].componentId}" />
						<input type="text" class="inputBorder"  name="component22" id="component22"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component22', 35)" />
						  	</span>
						  	<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component22')" id="editButton22"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component22',35)" id="okButton22" style="display: none;"  />
					</span>
		 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
        <div class="clear row1">
        
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[23].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[23].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId23" value="${template.components[23].componentId}" />
						<input type="text" class="inputBorder"  name="component23" id="component23"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" maxlength="35" onblur="removeEdit('component23', 35)"/>
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component23')" id="editButton23"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component23',35)" id="okButton23" style="display: none;"  />
				</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">
		
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[24].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[24].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId24" value="${template.components[24].componentId}" />
						<!--<input type="text" class="inputBorder"  name="component24" id="component24"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled" />
						   -->
						   <textarea rows="1" cols="10"  class="inputBorder"  name="component24" id="component24"  onkeypress="return limitnofotext(event,this.id,35)" 
						    disabled="disabled" style="height: 42px;" onblur="removeEdit('component24', 41)" maxlength="41">${componentDesc}</textarea> 
						  </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component24')" id="editButton24"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component24',35)" id="okButton24" style="display: none;"  />
				</span>
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
		<div class="clear row1">

         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[25].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[25].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <input type="hidden" name="componentId25" value="${template.components[25].componentId}" />
						<input type="text" class="inputBorder"  name="component25" id="component25"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="29" disabled="disabled"  maxlength="35" onblur="removeEdit('component25', 35)"  />
						 </span>
						  <span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editText('component25')" id="editButton25"/>
						<img src="../images/ieditOk.PNG"  onclick="removeEdit('component25',35)" id="okButton25" style="display: none;"  />
				</span>
					 
			 
		  <span class="floatRightTxt"><b class="greenCircle" >&nbsp;</b><b class="yellowCircle" >&nbsp;</b><b class="redCircle" >&nbsp;</b></span>
        </div>
	  </div>
      
      
      
      
      <div class="clear"></div>
	  
    </div>
  </div>
  <div  class="inspectionrightwrap">
    <div class="inspection_bg">
     
      <div class="inspectionTable" id="tiresBlock" style="border-bottom:1px solid #000;">
        <div class="clear row1Title greyBg">
         <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[26].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[26].componentName}"/>
			</c:if>
					 <span  align="center" class="th">
					  <input type="hidden" name="componentId26" value="${template.components[26].componentId}" />
						<input type="text" class="inputBorder componentHeader"  name="component26" id="component26"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,40)" 
						  size="40" disabled="disabled"  maxlength="30"/>
					</span>
					 
			
				<span class="EditBtnNew handSymbol">
						<img src="../images/ieditover.PNG"  onclick="editMultipleText(26,55)" id="editButton26"/>
						<img src="../images/ieditOk.PNG"  onclick="removeMultipleEdit(26,55)" id="okButton26" style="display: none;"  />
						</span>
        </div>
       
        <div class="clear row1" style="border-bottom:0px;">
          <div style="padding:0px; border:0px;height:130px" >
          <div class="bordernone interior_inspec">
              <div class="alignCenter clear paddingBottom" style="width:360px;">
                <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[27].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[27].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxtNew">
					  <span><h2 class="noInspec">
					    <input type="hidden" name="componentId27" value="${template.components[27].componentId}" />
						<input type="text" class="inputBorder" style="text-align: center;"  name="component27" id="component27"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="50" disabled="disabled" maxlength="35"/>
						  	<span>
						</span>
					  </h2></span>
					</span>
              
              </div>
              <div class="clear paddingBottom" style="width:385px; height:30px; position:relative; top:10px;">
                <span style="width:131px; float:left;">
                <span style="display:block; width:25px;float:left;"><b class="green" ></b></span>
                <c:set var="componentDesc" value=""/>
				<c:if test="${template.components[28].templateComponent.status.statusId == 1}">
					<c:set var="componentDesc" value="${template.components[28].componentName}"/>
				</c:if>
					 <span class="inspectionTxt4 leftAlignTxt width150" style="width:96px; float:left;">
					  <span class="fontF fontF4 ">
					   <input type="hidden" name="componentId28" value="${template.components[28].componentId}" /> 
						<input type="text" class="inputBorder"  name="component28" id="component28"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="15" disabled="disabled"  maxlength="16"/>
						  	<span>
						 
				  </span>
					   </span>
					</span>
			</span>
			
				
                
              
              
                <span style="width:127px; float:left;">
                <span style="display:block; width:25px;float:left;"><b class="yellow" ></b></span>
                
                  <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[29].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[29].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxt4 leftAlignTxt width150" style="width:90px; float:left;">
					  <span class="fontF fontF4">
					   <input type="hidden" name="componentId29" value="${template.components[29].componentId}" />
						<input type="text" class="inputBorder"  name="component29" id="component29"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="15" disabled="disabled" maxlength="16"/>
						  	<span>
						 
				    </span>
					   </span>
					</span>
					 
			</span>
			
			
                
                
                
            
              
                <span style="width:125px; float:left;">
                <span style="display:block; width:25px;float:left;"><b class="red" ></b></span>
                  <c:set var="componentDesc" value=""/>
			<c:if test="${template.components[30].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[30].componentName}"/>
			</c:if>
			     	 
					 <span class="inspectionTxt4 leftAlignTxt width150" style="width:92px; float:left;">
					  <span class="fontF fontF4">
					   <input type="hidden" name="componentId30" value="${template.components[30].componentId}" />
						<input type="text" class="inputBorder"  name="component30" id="component30"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,35)" 
						  size="15" disabled="disabled" maxlength="14"/>
							<span>
						 
				</span>
					  </span>
					</span>
					 
			</span>
			
				
                
            
              </div>
              <div class="clear" style="padding-top:15px;">
                <div class="alignCenter" style="width:350px; overflow:auto; padding-left:30px;">
				 <div class="bordernone interior_inspec interior_inspecLeft" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                  
                      <span class="txt_bold" style="float:left;margin-right:4px;width:25px;">
                      
                      <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[31].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[31].componentName}"/>
						</c:if>
					     <input type="hidden" name="componentId31" value="${template.components[31].componentId}" />
						 <input type="text" class="inputBorder"  name="component31" id="component31"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  onkeyup="removeNextText(this.id, 32);" size="5" disabled="disabled" maxlength="2" />
						  	<span style="position:relative; left:-15px;">
					  </span>
			       </span>
			         
                  
                      <span id="imgGYR3132"   ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"></b></span>
                    
                      <span class="txt_bold">
                      <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[32].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[32].componentName}"/>
						</c:if>
					      <input type="hidden" name="componentId32" value="${template.components[32].componentId}" />
						<input type="text" class="inputBorder"  name="component32" id="component32"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id, 8)" 
							onkeyup="removeNextText(this.id, 31);"   maxlength="8" size="5" disabled="disabled" />
						  	<span>
						 
				</span>
			</span>
			
                      
                      
                  
                  </div>
                  <div   class="bordernone interior_inspec interior_inspecRight" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                 
                      <span class="txt_bold"  style="float:left;margin-right:4px;width:25px;">
                       <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[33].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[33].componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId33" value="${template.components[33].componentId}" />
						<input type="text" class="inputBorder"  name="component33" id="component33"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  onkeyup="removeNextText(this.id, 34);" size="5" disabled="disabled" maxlength="2" />
						  	<span style="position:relative; left:-15px;">
						 
				 </span>
			   </span>
			    
              <span id="imgGYR3334"  ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                  <span class="txt_bold">
                     <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[34].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[34].componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId34" value="${template.components[34].componentId}" />
						<input type="text" class="inputBorder"  name="component34" id="component34"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,8)" 
						  onkeyup="removeNextText(this.id, 33);" size="5" disabled="disabled"  maxlength="8"/>
						  	<span>
				</span>
			   </span>
                  </div>
                  <div  class="bordernone interior_inspec interior_inspecLeft" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                      <span class="txt_bold" style="float:left;margin-right:4px;width:25px;">
                      <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[35].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[35].componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId35" value="${template.components[35].componentId}" />
						<input type="text" class="inputBorder"  name="component35" id="component35"  value="${componentDesc}" onkeypress="return limitnofotext(event,this.id,3)" 
							onkeyup="removeNextText(this.id, 36);"   size="5" disabled="disabled" maxlength="2"/>
						  	<span style="position:relative; left:-15px;">
						 
				</span>
			   </span>
			     
			                      <span id="imgGYR3536"  ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
			                      <span class="txt_bold" >
			                      <c:set var="componentDesc" value=""/>
									<c:if test="${template.components[36].templateComponent.status.statusId == 1}">
										<c:set var="componentDesc" value="${template.components[36].componentName}"/>
									</c:if>
								   <input type="hidden" name="componentId36" value="${template.components[36].componentId}" />
									<input type="text" class="inputBorder"  name="component36" id="component36"  value='${componentDesc}' onkeypress="return limitnofotext(event,this.id,8)" 
										 onkeyup="removeNextText(this.id, 35);"  size="5" disabled="disabled" maxlength="8" />
									  	<span>
							 </span>
						   </span>
                     
                    
                  </div>
                  <div  class="bordernone interior_inspec interior_inspecRight" style="margin-left:0px; margin-right:0;margin-bottom:10px;width:170px;">
                  <span class="txt_bold" style="float:left;margin-right:4px;width:25px;">
                  <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[37].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[37].componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId37" value="${template.components[37].componentId}" />
						<input type="text" class="inputBorder"  name="component37" id="component37"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
							onkeyup="removeNextText(this.id, 38);" size="5" disabled="disabled" maxlength="2" />
						  	<span style="position:relative; left:-15px;">
						 
				</span>
			   </span>
			     
                     <span id="imgGYR3738"   ><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                      <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[38].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[38].componentName}"/>
						</c:if>
					   <input type="hidden" name="componentId38" value="${template.components[38].componentId}" />
						<input type="text" class="inputBorder"  name="component38" id="component38"  value='${componentDesc}'  onkeypress="return limitnofotext(event,this.id,8)" 
						  onkeyup="removeNextText(this.id, 37);" size="5" disabled="disabled"  maxlength="8"/>
						  	<span>
				</span>
                    
                  </div>
				 </div>
              </div>
            </div></div>
        </div>
   
          
        <div style="clear:left;background-color:#dddddf; height:239px;" >
			  <div style="float:left; width:200px;">
                <!--<span style="padding:0px;width:100px;float:left;">
				 <img src="../images/rght_tire.png" width="100" height="170" />
				</span>
                -->
                <div style="padding:0px;width:100px;float:left; height:170px;">
					<div id="imgBottomRight" align="center" class="imgBottomRight" style="float: right;/*margin-top: -27%;*/width:99px;height:200px;">
						<img  src="/ImageLibrary/${template.components[72].componentName}" alt="rght_tire" style="max-width: 100px;max-height: 200px;" id="image"/>
					</div>
					<a href="javascript:void(0);" id="bottomRightImageClick" onclick="centerImageSelectionPop('bottemRight',100,170); return false;" style="display:inline;margin-top:10px;">Change Image</a><br/>[100 x 170]
				</div> 
                <div  class="bordernone interior_inspec padding_reset" style="padding:0px;float:right; padding-top:15px; width:99px;">
                    <div>
                    
                     <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[39].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[39].componentName}"/>
						</c:if>
					  <span style="padding:0px;"><h2 style="text-align:center;">
					   <input type="hidden" name="componentId39" value="${template.components[39].componentId}" />
						<!--<input type="text" class="inputBorder tireComponentHeader"  name="component39" id="component39"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
						  size="15" disabled="disabled" style="width:150px;" />-->
						 
						 <textarea rows="3" cols="12" class="inputBorder tireComponentHeader" name="component39" id="component39" onkeypress="return limitnofotext(event,this.id,39)"
								  disabled="disabled" style="width:100px;"  maxlength="20">${componentDesc}</textarea>   
					</h2><span>
					
						 </span></span>
                    </div>
                     <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[40].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[40].componentName}"/>
						</c:if>
					  <span style="padding:0px;clear:both;display:block;"><h2>
					   <input type="hidden" name="componentId40" value="${template.components[40].componentId}" />
						<input type="text" class="inputBorder tireComponentHeader"  name="component40" id="component40"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled" maxlength="2" />
						  	<span style="position:relative;top:3px;margin-right:10px;">
						 
				</span>
					</h2></span>
                     <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                     
                                    <div class="clear" style="width:110px;margin-bottom:10px;">
                    
                     <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[41].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[41].componentName}"/>
						</c:if>
					  	<span> <h2>
					  	 <input type="hidden" name="componentId41" value="${template.components[41].componentId}" /> 
						<input type="text" class="inputBorder tireComponentHeader"  name="component41" id="component41"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled" maxlength="2" />
						  <span style="position:relative;top:3px;margin-right:10px;">
						 
				</span></h2>
					 </span>
			  
                    
                     <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
                    
                                 <div class="clear" style="width:110px;margin-bottom:10px;">
                    
                    
                    
                    
                     <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[42].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[42].componentName}"/>
						</c:if>
						<span>
					  	<h2>
					  	 <input type="hidden" name="componentId42" value="${template.components[42].componentId}" />  
						<input type="text" class="inputBorder tireComponentHeader"  name="component42" id="component42"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled"  maxlength="2"/>
					 
					 	<span style="position:relative;top:3px;margin-right:10px;">
						 
				</span></h2>
			   </span>
                     <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
                    
                            <div class="clear" style="width:110px;">
                      <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[43].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[43].componentName}"/>
						</c:if>
						<span>
					  	 <h2>
					  	 <input type="hidden" name="componentId43" value="${template.components[43].componentId}" /> 
						<input type="text" class="inputBorder tireComponentHeader"  name="component43" id="component43"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
						  size="2" disabled="disabled"  maxlength="2"/>
					
					 	<span style="position:relative;top:3px;margin-right:10px;">
						 
				</span></h2>
			   </span>
                      <span><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
					
                    </div>
                    </div>
                  
     
                   
       
              
            
                </div>
				
				   <div style="height:239px;border-right: 2px solid #0e62af !important;border-left: 2px solid #0e62af !important;padding:0px;width:96px; padding:0 3px; float:left;">
                        <div cellspacing="0" class="bordernone padding_reset" style="">
                            <div style="margin-top:15px;">
                            
                             <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[44].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[44].componentName}"/>
						</c:if>
						<span>
					 <h2 class="titleFont" style="text-align:centert;padding-bottom:0px;">
					  <input type="hidden" name="componentId44" value="${template.components[44].componentId}" /> 
						<!--<input type="text" class="inputBorder tireComponentHeader"  name="component44" id="component44"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
						  size="12" disabled="disabled" />
						  -->
						   <textarea rows="2" cols="10" class="inputBorder tireComponentHeader" name="component44" id="component44"  onkeypress="return limitnofotext(event,this.id,20)"
								  disabled="disabled"  maxlength="12">${componentDesc}</textarea>   
						  <span>
						 
				</span>
					</h2> 
			   </span>
                         </div>
                            <div style="padding-left:15px;width:80px;">
                              <span style="float:left; width:22px;margin-right:5px; position:relative; top:10px;display:block;"><b class="smallRed" ></b></span>
                              <!--<span class="fontTxt" style="display:block; float:left;width:65px;text-align:center;">
                              	<img src="../images/symbol.png" alt="" width="40" height="36" />
                              </span>
                              
                              -->
                              <div  style="display:block; float:left;width:40px;">
								<div id="imgBottomMid" align="center" class="imgBottomMid" style="width:40px; height:36px;">
									<img src="/ImageLibrary/${template.components[73].componentName}" alt="symbol" style="max-width: 40px; max-height: 36px;" id="image"/>
								</div>
								<a href="javascript:void(0);" id="bottomMidImageClick" onclick="centerImageSelectionPop('bottomMid',40,36); return false;" style="display:inline;margin-top:4px;white-space:nowrap;">Change Image</a><br/><span style="font-size:10px;">[40 x 36]</span>
							</div>
                            </div>
                             <div class="bordernone interior_inspec">
                                  <div class="beforeAfter" style="text-align:right;width:85px;">
                              <span style="display:block; width:38px;float:left;">    
                             <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[45].templateComponent.status.statusId == 1}">
							<c:set var="componentDesc" value="${template.components[45].componentName}"/>
						</c:if>
						
					<span>
					 <input type="hidden" name="componentId45" value="${template.components[45].componentId}" /> 
						<input type="text" class="inputBorder tireComponentHeader"  name="component45" id="component45"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,10)" 
						  size="8" disabled="disabled"  maxlength="10"/>
					</span> 
					  </span>
					 
					   
					 
						
						 
                        <span style="display:block; width:38px;float:left;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[46].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[46].componentName}"/>
							</c:if>
							<span>
							 <input type="hidden" name="componentId46" value="${template.components[46].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component46" id="component46"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,10)" 
								  size="12" disabled="disabled"  maxlength="10" />
							</span>
								 
					   </span>
                      </div>
                      <div style="width:90px;clear:both;"  class="clear">
					  <div  style="float:left;width:50px;">
                 
                      <span style="width:88px; clear:both; display:block;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[47].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[47].componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId47" value="${template.components[47].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component47" id="component47"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2" />
							</span> 
								 
						<span class="white_box" style="float:left;display:block;">&nbsp;</span>
						
					   </span>
                 
							
								 <!-- <span style="width:88px; margin-top:21px;clear:both; display:block;">    -->
								 <span style="width:88px; clear:both; display:block;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[48].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[48].componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId48" value="${template.components[48].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component48" id="component48"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2"/>
							</span> 
								 
						 <span class="white_box" style="float:left;display:block;">&nbsp;</span>
					   </span>
								
								 </div>
                                 <span class="white_box_rectangle" style="float:left;display:block;width:30px;">&nbsp;</span>
                                  </div>
								  <div  style="width:90px;clear:both;"  class="clear">
								  <div  style="float:left;width:50px;">
                               
                                 <span style="width:88px; margin-top:0px;clear:both; display:block;">    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[49].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[49].componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId49" value="${template.components[49].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component49" id="component49"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2"/>
							</span>
								 
						 <span class="white_box" style="float:left;display:block;">&nbsp;</span>
					   </span>
               
					 <span  style="">
						<span style="width:88px; margin-top:0px;clear:both; display:block;">    
                           <c:set var="componentDesc" value=""/>
						<c:if test="${template.components[50].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[50].componentName}"/>
							</c:if>
							<span style="float:left; display:block;width:16px;">
							 <input type="hidden" name="componentId50" value="${template.components[50].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component50" id="component50"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2"/>
							</span> 
						 <span class="white_box" style="float:left;display:block;">&nbsp;</span>
					   </span>
					</span>
				 </div>
                               
                                 <span class="white_box_rectangle" style="float:left;display:block;width:30px;">&nbsp;</span>   
                                  </div>
                              
                              
                               
                                </div>
                            
                          </div>
                  </div>
             
             
                
				<div style="padding:0px;width:74px;float:left; padding:0 3px;" class="bordernone padding_reset">
                            <div style="margin-top:15px;">
                             
                        <span>    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[51].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[51].componentName}"/>
							</c:if>
							<span><h2 class="titleFont" style="text-align:left;">
							 <input type="hidden" name="componentId51" value="${template.components[51].componentId}" /> 
								<!--<input type="text" class="inputBorder componentHeader"  name="component51" id="component51"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="12" disabled="disabled" />
								  	-->
								  	<textarea rows="2" cols="10" class="inputBorder tireComponentHeader" name="component51" id="component51"  onkeypress="return limitnofotext(event,this.id,29)"
								  disabled="disabled" maxlength="29" >${componentDesc}</textarea>   
								   
								  	<span>
								  	
								 
						</span>
								  </h2>
							</span> 
					   </span>
                             
                            </div>
                            <div  class="bordernone interior_inspec">
                                  <div class="clear" style="margin-bottom:10px;">
                                    <span class="white_box_square" style="float:left;margin-right:0px;"></span>
                                    
                                    
                                    <span>    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[52].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[52].componentName}"/>
							</c:if>
							<span class="txtFont" style="text-align:canter;margin-left:3px;vertical-align:-2px;">
							 <input type="hidden" name="componentId52" value="${template.components[52].componentId}" /> 
								<input type="text" class="inputBorder componentHeader"  name="component52" id="component52"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="12" disabled="disabled"  maxlength="10"/>
							</span>
								 
					   </span>
                                  </div>
                                  <div class="clear" style="margin-bottom:10px;">
                                    <span class="white_box_square" style="float:left;margin-right:0px;"></span>
                                     <span>    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[53].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[53].componentName}"/>
							</c:if>
							<span class="txtFont" style="text-align:center;margin-left:3px;vertical-align:-2px;">
							 <input type="hidden" name="componentId53" value="${template.components[53].componentId}" /> 
								<input type="text" class="inputBorder componentHeader"  name="component53" id="component53"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="12" disabled="disabled"   maxlength="10"/>
							</span>
								 
					   </span>
                     </div>
                                  <div class="clear" style="margin-bottom:10px;">
                                    <span><span class="white_box_square" style="float:left;margin-right:0px;"></span> </span>
                                     <span>    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[54].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[54].componentName}"/>
							</c:if>
							<span class="txtFont" style="text-align:center;margin-left:3px;vertical-align:-2px;">
							 <input type="hidden" name="componentId54" value="${template.components[54].componentId}" /> 
								<input type="text" class="inputBorder componentHeader"  name="component54" id="component54"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="12" disabled="disabled"  maxlength="10" />
							</span> 
								 
					   </span>
                                  </div>
                                   <div class="clear">
                                    <span class="white_box_square" style="float:left;margin-right:0px;"></span>
                                     <span>    
                             <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[55].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[55].componentName}"/>
							</c:if>
							<span class="txtFont" style="text-align:center;margin-left:3px;vertical-align:-2px;">
							 <input type="hidden" name="componentId55" value="${template.components[55].componentId}" /> 
								<input type="text" class="inputBorder componentHeader"  name="component55" id="component55"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="12" disabled="disabled"  maxlength="10"/>
							</span> 
					   </span>
                      </div>
                     </div>
                 </div>
              </div>
      </div>
	  <div class="inspectionTable inspectionTableBg" style="border-bottom:1px solid #000;clear:both; position:relative;">
        <div class="clear row1Title greyBg">
          <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[56].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[56].componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId56" value="${template.components[56].componentId}" /> 
								<input type="text" class="inputBorder componentHeader"  name="component56" id="component56"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,40)" 
								  size="40" disabled="disabled"  maxlength="30"/>
							</span> 
					  
						<span class="handSymbol">
								<img src="../images/ieditover.PNG"  onclick="editMultipleText(56,66)" id="editButton56"/>
								<img src="../images/ieditOk.PNG"  onclick="removeMultipleEdit(56,66)" id="okButton56" style="display: none;"  />
						 	</span>		  
        </div>
       
        <div class="clear row1" style="border-bottom:0px;">
          <div style="padding:0px; border:0px;height:155px;width:161px; float:left;" >
          <div class="bordernone interior_inspec">
              <div class="alignCenter clear paddingBottom" style="width:360px;">
 				  <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[57].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[57].componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId57" value="${template.components[57].componentId}" /> 
								<input type="text" class="inputBorder"  name="component57" id="component57"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled"  maxlength="30"/>
							</span> 
								 
						
              </div>
              <div class="clear paddingBottom" style="width:180px; height:30px;">
                <span class="clear"><b class="green" ></b>
                 <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[58].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[58].componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId58" value="${template.components[58].componentId}" /> 
								 <!--<input type="text" class="inputBorder"  name="component58" id="component58"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled" /> --> 
								  <textarea rows="2" cols="15" class="inputBorder" id="component58" name="component58" onkeypress="return limitnofotext(event,this.id,39)" 
								  disabled="disabled" style="height:42px;width:150px;" maxlength="36">${componentDesc}</textarea>
							 </span>
								 
					   </span>
					
						
                <br />
             
                <span class="clear"><b class="yellow" ></b>
                  <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[59].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[59].componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId59" value="${template.components[59].componentId}" /> 
								<!-- <input type="text" class="inputBorder"  name="component59" id="component59"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled" />-->
								    <textarea rows="3" cols="15" class="inputBorder" id="component59" name="component59" onkeypress="return limitnofotext(event,this.id,39)"
								  disabled="disabled" style="height:42px;width:150px;"  maxlength="36">${componentDesc}</textarea>
							</span> 
								 
					   </span>
                <br />
                <span class="clear"><b class="red" ></b>
                  <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[60].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[60].componentName}"/>
							</c:if>
							<span class="txtFont">
							 <input type="hidden" name="componentId60" value="${template.components[60].componentId}" /> 
								<!-- <input type="text" class="inputBorder"  name="component60" id="component60"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,35)" 
								  size="29" disabled="disabled" /> -->
								<textarea rows="3" cols="15" class="inputBorder" id="component60" name="component60" onkeypress="return limitnofotext(event,this.id,39)"
								  disabled="disabled" style="height:60px;width:150px;"  maxlength="36">${componentDesc}</textarea>   
							</span> 
					   </span>
              </div>
            
            </div></div>
            
        </div>
        	<div  id="img" style="float:left; width:205px; height:196px; position:absolute; top:75px; margin-left:180px;">
					<div id="imgBottomCenter" class="imgBottomCenter" style=" position:relative;">
               		 	<span style=" position:relative;">
							<img  src="/ImageLibrary/${template.components[74].componentName}" style="position:relative;top:20px;max-height: 196px;max-width: 205px;" alt=""/>
						</span>
					</div>
					<div id="text" style="">
						<a href="javascript:void(0);" id="bottomCenterImageClick" onclick="centerImageSelectionPop('bottomCenter',205,196); return false;" style="display:inline;margin-top:10px;">Change Image</a>[205 x 195]
    				</div>
				</div>
   				<!--<div>
                	<img src = "/ImageLibrary/inspect_Brakes.png"></img>
                </div>
          		-->
          	
              <div style="background-color:#dddddf; height:220px; ">
                <div  class="bordernone interior_inspec padding_reset" style="padding:15px 30px;">
                    <div class="clear">
                    <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[61].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[61].componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="width:30px;margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId61" value="${template.components[61].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component61" id="component61"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2" />
							 
							</strong></span> 
						
                      <span style="float:left;"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
                   
                    <div class="clear">
                    
                    
                    
                     <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[62].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[62].componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="clear:left;width:30px; margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId62" value="${template.components[62].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component62" id="component62"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2"/>
								 
							</strong></span> 
					   
					
						
                     
                      <span style="float:left;"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
                  
                    <div class="clear">
                     <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[63].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[63].componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="width:30px; clear:left; margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId63" value="${template.components[63].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component63" id="component63"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2"/>
								  
							</strong></span> 
					   
						
						
                      <span style="float:left;"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
                    </div>
              
                    <div class="clear">
                     <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[64].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[64].componentName}"/>
							</c:if>
							<span class="txt_bold txtLeft" style="width:30px; clear:left; margin-bottom:10px;" > <strong>
							 <input type="hidden" name="componentId64" value="${template.components[64].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component64" id="component64"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,3)" 
								  size="2" disabled="disabled"  maxlength="2" />
							</strong>
							 
							</span> 
					   
						
						
                      <span style="float: left"><b class="smallGreen"   ></b><b class="smallYellow"  ></b><b class="smallRed"  ></b></span>
					
                    </div>
					
					 <div class="clear" style="width:400px;">
					 
					 
					  <c:set var="componentDesc" value=""/>
							<c:if test="${template.components[65].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[65].componentName}"/>
							</c:if>
							<span class="fontF" style="float:left;display:block; margin-left:-23px;"> <strong>
							 <input type="hidden" name="componentId65" value="${template.components[65].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component65" id="component65"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="29" disabled="disabled" style="width:83px;margin-top:15px;"  maxlength="10" />
							</strong>
					 	<!--<span>
					  		<img src="../images/brands.png" alt="" width="173" height="45" style="float:left;" />
					  	</span>
					  	-->
					  	</span>
					  	<span style="padding:0px;width:150px;float:left;display:block;margin-right:5px;margin-left:5px;">
							<div id="imgBottomLast" align="center" class="imgBottomLast" style="float: right;margin-top:10px;width: 150px;">
								<img src="/ImageLibrary/${template.components[75].componentName}" alt="brands" style="max-width: 150px; max-height: 45px;" id="image"/>
							</div>
							<a href="javascript:void(0);" id="bottomLastImageClick" onclick="centerImageSelectionPop('bottomLast',150,45); return false;" style="display:inline;margin-top:10px;position:relative; top:-4px;">Change Image</a>[150 x 45]
						</span>
						
						<c:set var="componentDesc" value=""/>
							<c:if test="${template.components[66].templateComponent.status.statusId == 1}">
									<c:set var="componentDesc" value="${template.components[66].componentName}"/>
							</c:if>
							<span class="fontF" style="float:left;display:block"> <strong>
							 <input type="hidden" name="componentId66" value="${template.components[66].componentId}" /> 
								<input type="text" class="inputBorder tireComponentHeader"  name="component66" id="component66"  value="${componentDesc}"  onkeypress="return limitnofotext(event,this.id,20)" 
								  size="29" disabled="disabled" style="width:115px;margin-top:15px;"  maxlength="12"/>
							</strong>
							</span>
                       <span class="fontF" style="float:left;"> </span>					   
					 </div>
                </div>
			</div>
          </div>
		  
		  <div class="inspectionTable" style="clear:both;">
        <div class="clear row1Title greyBg">
        <c:set var="componentDesc" value=""/>
		<c:if test="${template.components[67].templateComponent.status.statusId == 1}">
			<c:set var="componentDesc" value="${template.components[67].componentName}"/>
		</c:if>
		<span style="display:block;width:345px; float:left;">
			<span  id="component67Span" class="comments commentTitle">${componentDesc}</span> 
				<input type="hidden" name="componentId67" value="${template.components[67].componentId}" /> 
				<input type="text" class="inputBorder componentHeader"  name="component67" id="component67"  value="${componentDesc}"  onkeypress="return limitOfCharForMandatory(event,this.id,30)" 
					size="40" disabled="disabled" maxlength="30" style="display: none;margin-left: 40px;"/>
			</span> 
			<span style="diaplay:block; width:20px;float:left;" class="handSymbol">
			<img src="../images/ieditover.PNG"  onclick="editMultipleTextLast(67,69)" id="editButton67" style="float: right;"/>
			<img src="../images/ieditOk.PNG"  onclick="removeMultipleEditLast(67,69)" id="okButton67" style="display: none;float: right;"  />
			</span>
        </div>
        <div class="clear row1" style="height:32px;">
          
        </div>
         <div class="clear row1" style="height:32px;">
          
        </div>
       <div class="clear row1" style="height:32px;">
          
        </div>
		 <div class="clear row1" style="height:32px;">
          
        </div>
	 <div class="clear row1" style="height:32px;">
          
        </div>
		 
      </div>
      <div class="bottomtext" style="width: 393px;overflow:hidden;">
		<div style="width:400px;margin-top:10px;">
			<c:set var="componentDesc" value=""/>
			<c:if test="${template.components[68].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[68].componentName}"/>
			</c:if>
			<div style="float:left;width:279px;overflow:hidden;">
				<span id="cmt" style="float: left;">
				<span  id="component68Span" class="comments">${componentDesc}</span> 
					<input type="hidden" name="componentId68" value="${template.components[68].componentId}" /> 
					<input type="text" class="inputBorder"  name="component68" id="component68"  value="${componentDesc}"  
					size="15" disabled="disabled" style="width: 90px;display: none;"    maxlength="15"/>
				</span>
				<span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>  
				<span style="display:block; width:20px; float:left;margin-top:5px;">
	  				<c:set var="componentDesc" value=""/>
				</span>
			</div> 
			<c:if test="${template.components[69].templateComponent.status.statusId == 1}">
				<c:set var="componentDesc" value="${template.components[69].componentName}"/>
			</c:if>
			<div style="float:right;width: 120px;">
				<span id="component69Span" class="comments" style="float: left;">${componentDesc}</span> 
				<span style="float: left;">
					<input type="hidden" name="componentId69" value="${template.components[69].componentId}" /> 
					<input type="text" class="inputBorder" name="component69" id="component69"  value="${componentDesc}"   
						size="15" disabled="disabled" style="width:40px;display: none;" maxlength="10"/>
				</span>
				<span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine3" style="width: 125px"/></span>
				<span style="display:block; width:20px; float:left;margin-top:5px;"></span> 
	    	 </div> 
	     </div>
	</div>
					   <!--Component Id for Image -->
					    <input type="hidden" name="componentId70" value="${template.components[70].componentId}" />
					    <input type="hidden" name="componentId71" value="${template.components[71].componentId}" />
					    <input type="hidden" name="componentId72" value="${template.components[72].componentId}" />
					    <input type="hidden" name="componentId73" value="${template.components[73].componentId}" />
					    <input type="hidden" name="componentId74" value="${template.components[74].componentId}" />
					    <input type="hidden" name="componentId75" value="${template.components[75].componentId}" />
					
 
 </div>
  </div>
  </div>
  <div class="clear"></div>
  <div style="text-align:center;margin-top:10px;padding-bottom:10px;">
  <a  href="javascript:void(0);" name="save" value="Save" id="update102"  onclick="updateTemplate104('./updateAdminTemplate104.do'); " >Save</a>
  <a  href="./navigateTemplate104.do" id="cancelBtn" onclick="launchWindow('#dialog');">Cancel</a>
 </div>

 
  <div class="clear"></div>
   <div style="text-align:right;font-family:'MyriadProRegular'; font-size:6pt;padding:0 20px 10px 0;"></div>
   </div>
  </div>
 
  
 </form>
  </div>
  
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
<script type="text/javascript">
$('document').ready(function () {
    
    if($("input[disabled='disabled']")){
    	$("input[disabled='disabled']").each(function(){$(this).attr('readonly','readonly');});
    	$("input[disabled='disabled']").each(function(){$(this).removeAttr('disabled');}); 
    	
    }
    if($("textarea[disabled='disabled']")){
    	$("textarea[disabled='disabled']").each(function(){$(this).attr('readonly','readonly');});
    	$("textarea[disabled='disabled']").each(function(){$(this).removeAttr('disabled');}); 
    	
    }
    $('textarea[maxlength]').live('keypress blur', function() {
        // Store the maxlength and value of the field.
        var maxlength = $(this).attr('maxlength');
        var val = $(this).val();

        // Trim the field if it has content over the maxlength.
        if (val.length > maxlength) {
            $(this).val(val.slice(0, maxlength));
        }
    });
   
});

</script>
</body>
</html>
