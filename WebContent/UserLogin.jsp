<!DOCTYPE html>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript" src="../js/jquery-latest.js"></script>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css">
		<title>Login</title>
<script>
	jQuery(document).ready(function() {
		if((jQuery("#sessionUser").val()).toLowerCase() == "sessionuser"){
			jQuery('#sessionIndexForm').submit();
			}
		jQuery("#submitId").click(function() {
			jQuery('#userSignUpForm').submit();	
		});
	});
</script>
	

<body>
	<!--container starts-->
	<input type="hidden" id="sessionUser" name ="sessionUser" value="${sessionUser}" />
	<c:if test="${sessionUser == 'sessionUser'}">
	<form id="sessionIndexForm" name="sessionIndexForm" action="../Login.do" method="POST">
		<input type="hidden" name="username" id="username" value="${user.userName}" />
		<input type="hidden" name="role" id="role" value="${user.userRole}" />
	</form> 
	</c:if>
	<c:if test="${sessionUser != 'sessionUser'}">
	<div class="container">
		<!--Header starts-->
		<div class="header"></div>
		<!--Header Ends-->
		<!--Center Container starts-->
		<div class="centercontainer">
			<!--Innercontainer starts-->
			<div class="innercontainer">
				<div class="admincontainer">
					<form name="userSignUpForm" id="userSignUpForm" method="post"
						action="../Login.do" accept="text/html">
						<fieldset style="width: 300px;">
							<br />
							<div class="space">
								<div>
									<label>User Name :*</label> <input type="text" name="username"
										id="username" />
								</div>
								<br />
							</div>
							<div class="space">
								<div>
									<label>User Role&nbsp;&nbsp; :*</label> <input type="text"
										name="role" id="role" />
								</div>
							</div>
							<br />
							<div class="signupbutton">
								<a id="submitId"><img src="../images/submit.png"></img> </a>
							</div>
						</fieldset>
					</form>
					<p>&nbsp;</p>
					<p>
						<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
					</p>
				</div>
			</div>
			<!--Innercontainer Ends-->
		</div>
		<!--Center Container Ends-->
	</div>
	</c:if>
	<!--container ends-->
</body>
