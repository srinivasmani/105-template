<display:setProperty name="paging.banner.full">
	<div class="pagination">
		<div class="paginationRight" style="float: left;padding: 0px;">
			<div class="paginationRightDiv1 paginationLinks" id="firstPage"><a id="pageBnr3" href="{3}"></a></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="float:left;"><a id="firstPage1" class="pageLinkFirst" href="{1}">First &gt;</a></div>
			<div class="paginationRightDiv2 paginationLinks" id="firstPage"><span class="links">{0}</span></div>
			<div class="paginationRightDiv3 paginationLinks" id="firstPage" style="float:left;"  class="pagLinks"><a class="pageLink" id="pageBnr2" href="{2}"></a></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="float:right;"><a class="pageLinkLast" id="lastPage" href="{4}">&lt; Last </a></div>
		</div>
	</div>
</display:setProperty>
<display:setProperty name="paging.banner.first">
	<div class="pagination">
		<div class="paginationRight" style="float: left;padding: 0px;">
			<div class="paginationRightDiv1" id="firstPage"><a id="pgBnrFirst" href="{3}"></a></div>
			<div class="paginationRightDiv2 paginationLinks" id="firstPage" style="padding-left:25px;padding-right:15px"><span class="links">{0}</span></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage"></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" ></div>
			<div class="paginationRightDiv1" id="firstPage" ><a id="lastPg" href="{4}" class="pageLinkLast">&lt; Last </a></div>
		</div>
	</div>
</display:setProperty>
<display:setProperty name="paging.banner.last">
	<div class="pagination">
		<div class="paginationRight" style="float: left;padding: 0px;">
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="padding-left: 5px;width:35px;"><span class="pageLink"></span></div>
			<div class="paginationRightDiv3 paginationLinks" id="firstPage"></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="float:left;"><a class="pageLinkFirst" id="firstPg" href="{1}">First &gt;</a></div>
			<div class="paginationRightDiv2 paginationLinks" id="firstPage" style="float:left;"><span class="links">{0}</span></div>
			
		</div>
	</div>
	<div style="clear: both;"></div>
</display:setProperty>

<display:setProperty name="paging.banner.placement" value="bottom"/>
<display:setProperty name="paging.banner.some_items_found" ><span class="paginationLeft">Results {2}-{3} of {0}</span></display:setProperty>
<display:setProperty name="paging.banner.page.separator" > &nbsp;&nbsp;&nbsp;&nbsp;</display:setProperty>
<display:setProperty name="paging.banner.one_item_found" > </display:setProperty>	
<display:setProperty name="paging.banner.all_items_found" > </display:setProperty>
<display:setProperty name="paging.banner.onepage"> </display:setProperty>
<display:setProperty name="basic.empty.showtable">false</display:setProperty>
<display:setProperty name="paging.banner.page.selected"><strong>{0}</strong></display:setProperty>
<display:setProperty name="paging.banner.group_size">5</display:setProperty>
