<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="header.jsp"%>
<script type="text/javascript" src="../js/jquery-latest.js"></script>
<input type="hidden" name="selectedType" id="selectedType" value="${optionType}" />
<div id="optionList">
	<c:forEach var="desc" items="${fn:split(optionDesc, '^')}" varStatus="status">
		<div class="checkbox" id="checkboxcnt_${status.count}">
		<c:if test="${fn:length(fn:split(optionDesc, '^')) != status.count}">
			<span class="checkbox" style="display:block;">
				<img src="../images/${fn:toLowerCase(optionType)}.png" class="imageOptionType" id="image${status.count}">&nbsp;&nbsp;
				<input type="text" size="9" class="optiontext" id="optiontext_${status.count}" value="${desc}" style="width: 100px;" onkeyup="editOtions(this.id)"/>
				<input type="button" name="remove_${status.count}" value="" id="remove_${status.count}" class="deleteEditOption"/>
				<input type="button" name="add_${status.count}" value="" id="add_${status.count}" class="addEditOption" style="display: none;" />
			</span>
		</c:if>
		<c:if test="${fn:length(fn:split(optionDesc, '^')) == status.count}">		
			<span class="checkbox" style="display:block;">
				<img src="../images/${fn:toLowerCase(optionType)}.png" class="imageOptionType" id="image${status.count}">&nbsp;&nbsp;
				<input type="text" size="9" class="optiontext" id="optiontext_${status.count}" value="${desc}" style="width: 100px;" onkeyup="editOtions(this.id)"/>
				<input type="button" name="remove_${status.count}" value="" id="remove_${status.count}" class="deleteEditOption"/>
				<input type="button" name="add_${status.count}" value="" id="add_${status.count}" class="addEditOption" />
			</span>
		</c:if>
		</div>
	</c:forEach>	
</div>
