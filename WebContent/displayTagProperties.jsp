<display:setProperty name="paging.banner.full">
	<div class="pagination" style="margin-left:145px;">
		<div class="paginationRight" style="width:260px;">
			<div class="paginationRightDiv1 paginationLinks" id="firstPage"><a id="pageBnr3" href="{3}"></a></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="width:40px;float:left;"><a class="pageLinkFirst" id="firstPage1" href="{1}">First &gt;</a></div>
			<div class="paginationRightDiv2" id="firstPage"  style="width:175px;"><span class="links">{0}</span></div>
			<div class="paginationRightDiv3 paginationLinks" id="firstPage"><a id="pageBnr2" href="{2}"></a></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="width:85px;float:right;margin-top:-15px;"><a class="pageLinkLast" id="lastPage" href="{4}">&lt; Last</a></div>
		</div>
	</div>
</display:setProperty>
<display:setProperty name="paging.banner.first">
	<div class="pagination"  style="margin-left:145px;">
		<div class="paginationRight" style="width:235px;">
			<div class="paginationRightDiv1" id="firstPage"><a id="pgBnrFirst" href="{3}"></a></div>
			<div class="paginationRightDiv2 paginationLinks" id="firstPage" style="padding-left:25px;padding-right:15px;width:175px;"><span class="links">{0}</span></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage"></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="padding-right: 5px;width:35px;"></div>
			<div class="paginationRightDiv1" id="firstPage" style="padding-left: 5px;width:85px;float:right;margin-top:-15px;"><a class="pageLinkLast" id="lastPg" href="{4}">&lt; Last</a></div>
		</div>
	</div>
</display:setProperty>
<display:setProperty name="paging.banner.last">
	<div class="pagination"  style="margin-left:145px;">
		<div class="paginationRight" style="width:235px;">
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="padding-left: 5px;width:35px;"></div>
			<div class="paginationRightDiv3 paginationLinks" id="firstPage"></div>
			<div class="paginationRightDiv1 paginationLinks" id="firstPage" style="width:40px;float:left;"><a class="pageLinkFirst" id="firstPg" href="{1}">First &gt;</a></div>
			<div class="paginationRightDiv2 paginationLinks" id="firstPage" style="float:left;width:175px;"><span class="links">{0}</span></div>
		</div>
	</div>
	<div style="clear: both;"></div>
</display:setProperty>

<display:setProperty name="paging.banner.placement" value="bottom"/>
<display:setProperty name="paging.banner.some_items_found" ><span class="paginationLeft paginationLeftPopup">Results {2}-{3} of {0}</span></display:setProperty>
<display:setProperty name="paging.banner.page.separator" > &nbsp;&nbsp;&nbsp;&nbsp;</display:setProperty>
<display:setProperty name="paging.banner.one_item_found" > </display:setProperty>	
<display:setProperty name="paging.banner.all_items_found" > </display:setProperty>
<display:setProperty name="paging.banner.onepage"> </display:setProperty>
<display:setProperty name="basic.empty.showtable">false</display:setProperty>
<display:setProperty name="basic.msg.empty_list"><span style="margin-left:150px;font-size: 20px;">No Image Found!</span></display:setProperty>
<display:setProperty name="paging.banner.page.selected"><strong>{0}</strong></display:setProperty>
<display:setProperty name="paging.banner.group_size">5</display:setProperty>
