<!DOCTYPE html>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="header.jsp"%>
<%@include file="UploadImage.jsp" %>
<meta http-equiv="content-language" content="en-GB" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link type="text/css" rel="stylesheet" href="../css/jquery.miniColors.css" />
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<!-- Java Script -->
<script type="text/javascript" src="../js/popup.js"></script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.ui.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery.template2.js"></script>
<script type="text/javascript" src="../js/jquery-userEditTemplate.js"></script>
<style>
.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity = 80);
	display: none;
}

.box_content {
	position: fixed;
	top: 25%;
	left: 25%;
	right: 30%;
	
	/*height: 30%;*/
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	border: 5px solid #ACACAC;
	background: none repeat scroll 0 0 #FFFFFF;
	border:8px solid #acacac;
	width:575px;
}

#msg {
	font-size: 10pt;
	/*margin-left: 35%;*/
	margin-top: 9%;
	letter-spacing: 0.5px;
}

#imgLogo {
    border: 1px solid black;
    height: 100px; 
    width: 380px;
    margin-left: 200px; 
    margin-top: 0px;
    padding: 0;
    text-align:center;
    position:relative;
    top:10px;
}
#imgLogoNoBorder{
	height: 100px; 
    width: 380px;
    margin-left: 200px; 
    margin-top: 0px;
    padding: 0;
    text-align:center;
}
  
.rghtOptImage {
	border: 1px solid black;
	font-size: 12px;
	margin-top: -1px;
	height: 100px !important;
	width: 100px !important;
	  position:relative;
    top:10px;
}

.rghtOptImageNoBorder {
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}

.leftOptImage {
	border: 1px solid black;
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
	position:relative;
	top:10px;
}

.leftOptImageNoBorder {
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}

.logo1Parent {
	float: left;
	height: 0;
	margin-right: 3px;
	margin-top: -2px;
}

.logo2 {
    float: right;
    height: 0;
    margin-right: 3px;
    margin-top: -113px;
}

#imageSize {
    /*height: 100px;
    width: 100px;*/
}

.yellowCircle{
	height:41px;
	width:41px;
	background:  url("../images/Yellow_circle.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
	margin-right:3px;
}

.redCircle{
	height:41px;
	width:41px;
	background: url("../images/red_circle.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
	margin-right:3px;
}
.greenCircle {
	height: 41px;
	width: 41px;
	background: url("../images/green_circle.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
}

.greenSquare{
	height:41px;
	width:41px;
	background: url("../images/green_large.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
}

.yellowSquare{
	height:41px;
	width:41px;
	background: url("../images/yellow_large.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
}

.redSquare{
	height:41px;
	width:41px;
	background: url("../images/red_large.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
}
	
.hiddenImage{
	height:41px;
	width:30px;
	display: block;
	border: none;
	float: left;
	background-color: #FFFFFF;
}

#editText102{
	text-transform: uppercase;
}

#headerText102{
	text-transform: uppercase;
}
#repositoryPopUp, #leftLogoRepositoryPopUp, #rightLogoRepositoryPopUp{
height:auto !important;
border:8px solid #acacac !important;
width:625px !important;
margin-top:-65px;
}
#item .odd, #item .even {
border: 1px solid #ACACAC !important;
margin-right:5px;
margin-bottom:5px;
}
.selectLink {
    left: 36px !important;
    position: relative;
}
</style>
<script>
jQuery(document).ready(function(){
 var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
 var text = "";
 var custText = jQuery(".custSpanText").find(".customerSpanText");
	jQuery(custText).each(function(index){
		text = text+jQuery(this).text();
	});
	if(trimSpanText(text).length == 0){
		jQuery("#customer").css("border","1px solid white");
	}
	else{
		jQuery("#customer").css("border","1px solid #ACACAC");
	}
	if(is_chrome == true){
		jQuery("#customer").css("width", "760px");
		jQuery("#editComp_6_0").css("width", "760px");
		jQuery("#editComp_6_1").css("width", "760px");
		jQuery(".custSpanText").css("width", "846px");
	}
 });
function checkStringLength(id, event)
{
	var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
	var maxWidth = 720;
	if(is_chrome == true){
		maxWidth = 645;
	}
	
	jQuery("#temp1").empty();
	jQuery("#temp1").text(jQuery("#"+id).val());
	var matches = jQuery("#"+id).val();
	var pattern=/_/g;
    var v=matches.match(pattern);
    var width = 0;
    jQuery("#temp1").hide();
    if(v!=null)
    {  
    	
    	width=jQuery("#temp1").width()-((matches.match(pattern).length/2)*.5);
    }
    else
    {
    	 width=jQuery("#temp1").width();
    } 
	if(width>=maxWidth && (event.keyCode != 8 && event.keyCode != 46))
	{
		jQuery("#"+id).attr('maxlength',jQuery("#"+id).val().length);
		alert("Cannot add more characters.");
		if(id != "editComp_6_1"){
			  jQuery("#editComp_6_1").focus();
    	}
		return true;
	}
	else if(event.keyCode == 13 ){
    	if(id != "editComp_6_1"){
    		 jQuery("#editComp_6_1").focus();
    	}
    }
}
function updateForm(){
	if(checkMandatory() == true){
		removeFirstComponentEdit('editComp_6',186);
		 jQuery("#updateUserForm").submit();
			return true;
		}
	else{
			return false;
		}
}

</script>

<div class="container">
	<!--Header starts-->
	<%@ include file="WEB-INF/header/userNavigation.jsp"%>
	<div class="centercontainer">
	<div class="header"></div>
	<div id="centercontainer" class="centercontainer">
		<div class="nav">
			<ul>
			<c:if test="${userForm.status ne 'default'}">
				<li><a href="#" class="selected" id="formNameTab">${userForm.formName}</a></li>
			</c:if>
			<c:if test="${userForm.status eq 'default'}">
				<li><a href="./navigateTemplateView.do" onclick="launchWindow('#dialog');">Template 101</a>
				</li>
				<li><a href="./getUserComponentIfs.do" class="selected" onclick="launchWindow('#dialog');">Template 102</a>				
				</li>
				<li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a>
				</li>
				<li><a href="./createUserComponentForm104.do" onclick="launchWindow('#dialog');">Template 104</a>
                                </li>
                                <li><a href="./createUserComponentForm105.do" onclick="launchWindow('#dialog');">Template 105</a>
				</li>
			</c:if>
			</ul>
		</div>
		<div style="clear: both"></div>
		<div class="innercontainer">
			<% 
				String getSVG = (String) request.getAttribute("imgType");
				if (getSVG.equalsIgnoreCase("circle")) {
			%>
			<% 	
					getSVG = "<div class='svgTable' ><span><input type='button' class='hiddenImage' /><input type='button' class='greenCircle' />"+
							"<input type='button' class='yellowCircle' />"+
								"<input type='button' class='redCircle' /></span></div>"; 
				}else
				{ 
			%>
			<% 
					getSVG = "<div class='svgTable' ><span><input type='button' class='hiddenImage' /><input type='button'  class='greenSquare' />"+
							"<input type='button'  class='yellowSquare' />"+
								"<input type='button' class='redSquare' /></span></div>"; 
				} 
			%>
								
			<!-- Complete Width and Height -->
			<form name="updateUserForm" action="./updateUserFormIfs.do" method="post" id="updateUserForm">
			<input type= "hidden" name="saveImage" id="saveImage" value="${userForm.formImage}"/>
			<input type= "hidden" name="saveLeftImage" id="saveLeftImage" value="${userForm.formLeftImage}"/>
			<input type= "hidden" name="saveRightImage" id="saveRightImage" value="${userForm.formRightImage}"/>
			<div style="margin-left:36%;padding:8px;">
				 <span class="circlesquare">Form Name :</span> <input type="text" name="formName" id="formName" value="${userForm.formName}" maxlength="25" /> 
			</div>
			<input type="hidden" name="userFormId" value="${userForm.userFormId}"/>
			<div id="templateSize">
				<div class="divLogoTab" style="width: 20.8cm;">					
					<div class="divLogoRow">
						<div class="logo1Parent">
							<div class="logo1">
								<c:choose>
									<c:when test="${userForm.formLeftImage eq null or userForm.formLeftImage eq ''}">
										<div class="leftOptImage" id="logoLeft"></div>
									</c:when>
									<c:otherwise>
										<div class="leftOptImage" id="logoLeft">
											<img id="imageSize" src="/ImageLibrary/${userForm.formLeftImage}" style="max-width:100px;max-height:100px;"/>
										</div>
									</c:otherwise>
								</c:choose>
							</div>
							<a href="javascript:void(0);" id="uploadImage1" onclick="centerImageSelectionPop('leftLogo',100,100); return false;" style="display: block !important;margin-top: 115px;margin-left: -40px; font-size: 14px;font-family: 'MyriadProRegular';">Click to upload optional logo</a> <span class="imgDimension" style="position:relative; left:10px;">[100 x 100]</span>
						</div>
						<c:choose>
							<c:when test="${userForm.formImage eq null or userForm.formImage eq ''}">
								<div id="imgLogo" align="center" class="imgLogo">
									<p id="msg">(Your logo here)<br/>Dimension:[380 x 100]pixels</p>
								</div>
							</c:when>
							<c:otherwise>
								<div id="imgLogo" align="center" class="imgLogo">
									<img src="/ImageLibrary/${userForm.formImage}" style="max-width:380px;max-height:100px;margin:0;">
								</div>
							</c:otherwise>
						</c:choose>
						<a href="#" id="centerImageClick" onclick="centerImageSelectionPop('center',380,100); return false;" style="margin-left: 0px;height: 0; font-family: 'MyriadProRegular'; font-size: 11pt;position:relative; left:38%;top:10px;">Click to upload logo</a> &nbsp;&nbsp;<span class="imgDimension" style="position:relative; left:38%;top:10px;">[380 x 100]</span>
						<div class="logo2">
							<c:choose>
								<c:when test="${userForm.formRightImage eq null or userForm.formRightImage eq ''}">
									<div class="rghtOptImage" id="logoRight"></div>	
								</c:when>
								<c:otherwise>
									<div class="rghtOptImage" id="logoRight">
										<img id="imageSize" src="/ImageLibrary/${userForm.formRightImage}" style="margin:0; max-width:100px; max-height:100px;"/>
									</div>
								</c:otherwise>
							</c:choose>
							<a href="javascript:void(0);" id="uploadImage2" onclick="centerImageSelectionPop('rightLogo',100,100); return false;" style="display:inline;margin-top:0px; font-family: 'MyriadProRegular';font-size: 14px;white-space:nowrap;position:relative;left:-32px;top:10px;">Click to upload optional logo</a><br/> <span class="imgDimension" style="position:relative; top:8px;left:20px;">[100 x 100]</span>
						</div>
					</div>
				</div>
			</div>
			<div id="pageHeading">
				<p id="headerText102" style="display: block; margin-left: 20px;font-size:20pt;">${userForm.headerText}</p>
				<input type="hidden" name="headingText102" id="headingText102" value="${userForm.headerText}"/>
				<input type="text" value="${userForm.headerText}" id="editText102" style="display: none; font-size: 20pt;font-weight: bold;margin-left: 87px;width: 80%;" maxlength="44" />
				<a href="javascript:void(0);" id="editHeading102" style='float:right;margin-right: 51px;margin-top: -30px;'>
					<img src="../images/ieditover.PNG" width="16px" height="17px">
				</a>
				<a href="javascript:void(0);" id="okHeading102" style='display:none;' style='display:none;float:right;margin-right: 51px;margin-top: -30px;'>
					<img id="iok" src="../images/iok.png" width="16px" height="17px">
				</a>
				<div class="clear"></div>
			</div>
			<!-- Form 1 -->
			<div class="divTable" style="width:850px;">
				<div class="divRow">
					<div class="divCell" id="customer" style="border: 1px solid #ACACAC;margin-left: 40px;width: 763px;">
						<div class="divCell" id="inner_Customer">
						<c:forEach var="cname" items="${fn:split(newUserFormComponents[0].component.componentName, '~')}" varStatus="stat">
							<input  id="editComp_6_${stat.count - 1}" name="editComp_6_${stat.count - 1}" type="text" 
									style="text-align:left;display:none;width:763px;height:30px;font-family: MyriadProRegular;font-size: 11pt;border:1px solid black;"  onkeypress="return checkStringLength(this.id, event);" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" value="${cname}" onblur=" mouseOut${stat.count - 1}()"/>
						</c:forEach>
						</div>
						<span id="divTxt_attention" style="display:none;">
							<input type="text" id="editComp_6" name="editComp_6" style="text-align:left;display:none;width:763px;height:30px;" value="${newUserFormComponents[0].component.componentName}">
							<input type="hidden" id="componentId6" name="componentId6" value="${newUserFormComponents[0].component.componentId}">
						</span>
						<%--<div style="font-family: 'MyriadProRegular',Sans-Serif;font-size:11pt;letter-spacing:.5px">
							<span id="temp1"></span>
						</div> --%>
						<c:forEach var="cname" items="${fn:split(newUserFormComponents[0].component.componentName, '~')}" varStatus="stat">
							<div class ="custSpanText" style="width: 800px;">
								<span id="cust${stat.count}" style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9" class="customerSpanText">${cname}</span>
							</div>
						</c:forEach>
					</div>
					<a href="javascript:void(0);" id="editButton6" onclick="editTextFirstCompnent('component6')" style="float:left; position:relative; left:5px;">
						<img src="../images/ieditover.PNG" width="16px" height="17px">
					</a> 
					<a href="javascript:void(0);" id="okButton6" onclick="removeFirstComponentEdit('editComp_6',186)" style='display:none;float:left; position:relative; left:5px;'>
						<img id="iok" src="../images/iok.png" width="16px" height="17px">
					</a>
				</div>
			</div>
			<div id="commentDiv" style="font-family: 'MyriadProRegular', Sans-Serif; font-size: 11pt; letter-spacing: .5px; width: 960px;">
				<span id="temp1"></span>
			</div>
			<%
				String getSVGImage = (String) request.getAttribute("imgType");
				if (getSVGImage.equalsIgnoreCase("circle")) {
			%>
				<div style="padding:15px 65px;">
					<span class="circlesquare" style="font-family: 'MyriadProBold';">
						<input  type="radio" name="imgType" value="circle" checked="checked" /> Circle
					</span>
					<span class="circlesquare" style="font-family: 'MyriadProBold';">
						<input type="radio" name="imgType" value="square"/> Square
					</span>
				</div>
			<%
				}
				else{
			%>
				<div style="padding:15px 55px;">
					<span class="circlesquare">
						<input  type="radio" name="imgType" value="circle"  /> Circle
					</span>
					<span class="circlesquare">
						<input type="radio" name="imgType" value="square" checked="checked"/> Square
					</span>
				</div>
					
			<% }%>
			<!-- ATTENTION -->
			<div class="selTable selTableEdit">
				<div class="selRow">
					<div class="selCol">
						<div class="selCell">							
							<c:choose>
								<c:when test="${userForm.formImgType eq 'circle'}">
									<input type='button' class='greenCircle' />
								</c:when>
								<c:otherwise>
									<input type='button'  class='greenSquare' />
								</c:otherwise>
							</c:choose>
							<nobr>
								<p id="comp_7">${newUserFormComponents[1].component.componentName}</p>
								<input type="text"  id="editComp_7" name="editComp_7" value = "${newUserFormComponents[1].component.componentName}" style='display:none;float: none;width: 200px;' class="focusOutSave"  maxlength = "23"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId7" value="${newUserFormComponents[1].component.componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton7" onclick="editText('component7')">
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton7" onclick="removeEdit('editComp_7',23)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</nobr>
						</div>
					</div>
					<div class="selCol">
						<div class="selCell">
							<c:choose>
								<c:when test="${userForm.formImgType eq 'circle'}">
									<input type='button' class='yellowCircle' />
								</c:when>
								<c:otherwise>
									<input type='button'  class='yellowSquare' />
								</c:otherwise>
							</c:choose>
							<nobr id="nobr">
								<p id="comp_8">${newUserFormComponents[2].component.componentName}</p>
								<input type="text"  id="editComp_8" name="editComp_8" value = "${newUserFormComponents[2].component.componentName}" style='display:none;float: none;width: 200px;' class="focusOutSave" maxlength = "30"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId8" value="${newUserFormComponents[2].component.componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton8" onclick="editText('component8')">
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton8" onclick="removeEdit('editComp_8',25)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</nobr>
						</div>
					</div>
					<div class="selCol">
						<div class="selCell">
							<c:choose>
								<c:when test="${userForm.formImgType eq 'circle'}">
									<input type='button' class='redCircle' />
								</c:when>
								<c:otherwise>
									<input type='button' class='redSquare' />
								</c:otherwise>
							</c:choose>
							<nobr id="nobr">
								<p id="comp_9">${newUserFormComponents[3].component.componentName}</p>
								<input type="text"  id="editComp_9" name="editComp_9" value = "${newUserFormComponents[3].component.componentName}" style='display:none;float: none;width: 200px;' class="focusOutSave" maxlength = "23"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId9" value="${newUserFormComponents[3].component.componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton9" onclick="editText('component9')">
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton9" onclick="removeEdit('editComp_9',25)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</nobr>								
						</div>
					</div>
				</div>
			</div>
			<!-- Items And Conditions -->
				<div class="divTable1">
					<div id="div1" style="margin-left:40px;">
						<div class="divRow">
							<div class="divCell1 rowheading" id="content">
								<p id="comp_10">${newUserFormComponents[4].component.componentName}</p>
								<input type="text"  id="editComp_10" name="editComp_10" value = "${newUserFormComponents[4].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="10"  class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId10" value="${newUserFormComponents[4].component.componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton10" onclick="editText('component10')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton10" onclick="removeEdit('editComp_10',25)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>	
							</div>
							
							<div class="divCell1 rowheading" id="content">
								<p id="comp_11">${newUserFormComponents[5].component.componentName}</p>
								<input type="text"  id="editComp_11" name="editComp_11" value = "${newUserFormComponents[5].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="10" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId11" value="${newUserFormComponents[5].component.componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton11" onclick="editText('component11')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton11" onclick="removeEdit('editComp_11',25)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_12">${newUserFormComponents[6].component.componentName}</p>
								<input type="text"  id="editComp_12" name="editComp_12" value = "${newUserFormComponents[6].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId12" value="${newUserFormComponents[6].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[6].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component12" style='float: right;' onclick="deleteText('component12')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton12" onclick="editText('component12')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton12" onclick="removeEdit('editComp_12',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_13">${newUserFormComponents[7].component.componentName}</p>
								<input type="text"  id="editComp_13" name="editComp_13" value = "${newUserFormComponents[7].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId13" value="${newUserFormComponents[7].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[7].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component13" style='float: right;' onclick="deleteText('component13')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton13" onclick="editText('component13')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton13" onclick="removeEdit('editComp_13',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_14">${newUserFormComponents[8].component.componentName}</p>
								<input type="text"  id="editComp_14" name="editComp_14" value = "${newUserFormComponents[8].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId14" value="${newUserFormComponents[8].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[8].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component14" style='float: right;' onclick="deleteText('component14')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton14" onclick="editText('component14')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton14" onclick="removeEdit('editComp_14',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_15">${newUserFormComponents[9].component.componentName}</p>
								<input type="text"  id="editComp_15" name="editComp_15" value = "${newUserFormComponents[9].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId15" value="${newUserFormComponents[9].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[9].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component15" style='float: right;' onclick="deleteText('component15')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton15" onclick="editText('component15')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton15" onclick="removeEdit('editComp_15',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_16">${newUserFormComponents[10].component.componentName}</p>
								<input type="text"  id="editComp_16" name="editComp_16" value = "${newUserFormComponents[10].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId16" value="${newUserFormComponents[10].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[10].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component16" style='float: right;' onclick="deleteText('component16')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton16" onclick="editText('component16')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton16" onclick="removeEdit('editComp_16',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_17">${newUserFormComponents[11].component.componentName}</p>
								<input type="text"  id="editComp_17" name="editComp_17" value = "${newUserFormComponents[11].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId17" value="${newUserFormComponents[11].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[11].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component17" style='float: right;' onclick="deleteText('component17')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton17" onclick="editText('component17')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton17" onclick="removeEdit('editComp_17',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_18">${newUserFormComponents[12].component.componentName}</p>
								<input type="text"  id="editComp_18" name="editComp_18" value = "${newUserFormComponents[12].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId18" value="${newUserFormComponents[12].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[12].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component18" style='float: right;' onclick="deleteText('component18')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton18" onclick="editText('component18')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton18" onclick="removeEdit('editComp_18',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>						
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_19">${newUserFormComponents[13].component.componentName}</p>
								<input type="text"  id="editComp_19" name="editComp_19" value = "${newUserFormComponents[13].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId19" value="${newUserFormComponents[13].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[13].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component19" style='float: right;' onclick="deleteText('component19')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton19" onclick="editText('component19')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton19" onclick="removeEdit('editComp_19',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_20">${newUserFormComponents[14].component.componentName}</p>
								<input type="text"  id="editComp_20" name="editComp_20" value = "${newUserFormComponents[14].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId20" value="${newUserFormComponents[14].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[14].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component20" style='float: right;' onclick="deleteText('component20')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton20" onclick="editText('component20')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton20" onclick="removeEdit('editComp_20',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_21">${newUserFormComponents[15].component.componentName}</p>
								<input type="text"  id="editComp_21" name="editComp_21" value = "${newUserFormComponents[15].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId21" value="${newUserFormComponents[15].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[15].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component21" style='float: right;' onclick="deleteText('component21')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton21" onclick="editText('component21')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton21" onclick="removeEdit('editComp_21',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_22">${newUserFormComponents[16].component.componentName}</p>
								<input type="text"  id="editComp_22" name="editComp_22" value = "${newUserFormComponents[16].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId22" value="${newUserFormComponents[16].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[16].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component22" style='float: right;' onclick="deleteText('component22')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton22" onclick="editText('component22')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton22" onclick="removeEdit('editComp_22',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_23">${newUserFormComponents[17].component.componentName}</p>
								<input type="text"  id="editComp_23" name="editComp_23" value = "${newUserFormComponents[17].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId23" value="${newUserFormComponents[17].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[17].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component23" style='float: right;' onclick="deleteText('component23')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton23" onclick="editText('component23')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton23" onclick="removeEdit('editComp_23',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_24">${newUserFormComponents[18].component.componentName}</p>
								<input type="text"  id="editComp_24" name="editComp_24" value = "${newUserFormComponents[18].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId24" value="${newUserFormComponents[18].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[18].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component24" style='float: right;' onclick="deleteText('component24')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton24" onclick="editText('component24')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton24" onclick="removeEdit('editComp_24',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_25">${newUserFormComponents[19].component.componentName}</p>
								<input type="text"  id="editComp_25" name="editComp_25" value = "${newUserFormComponents[19].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId25" value="${newUserFormComponents[19].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[19].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component25" style='float: right;' onclick="deleteText('component25')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton25" onclick="editText('component25')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton25" onclick="removeEdit('editComp_25',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
					</div>
					
					<div id="div2" style="margin-right:35px;">
						<div class="divRow">
							<div class="divCell1 rowheading" id="content">
								<p id="comp_26">${newUserFormComponents[20].component.componentName}</p>
								<input type="text"  id="editComp_26" name="editComp_26" value = "${newUserFormComponents[20].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="10" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId26" value="${newUserFormComponents[20].component.componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton26" onclick="editText('component26')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton26" onclick="removeEdit('editComp_26',10)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>						
							</div>
							
							<div class="divCell1 rowheading" id="content">
								<p id="comp_27">${newUserFormComponents[21].component.componentName}</p>
								<input type="text"  id="editComp_27" name="editComp_27" value = "${newUserFormComponents[21].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="10" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId27" value="${newUserFormComponents[21].component.componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton27" onclick="editText('component27')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton27" onclick="removeEdit('editComp_27',10)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>	
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_28">${newUserFormComponents[22].component.componentName}</p>
								<input type="text"  id="editComp_28" name="editComp_28" value = "${newUserFormComponents[22].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId28" value="${newUserFormComponents[22].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[22].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component28" style='float: right;' onclick="deleteText('component28')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton28" onclick="editText('component28')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton28" onclick="removeEdit('editComp_28',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_29">${newUserFormComponents[23].component.componentName}</p>
								<input type="text"  id="editComp_29" name="editComp_29" value = "${newUserFormComponents[23].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId29" value="${newUserFormComponents[23].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[23].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component29" style='float: right;' onclick="deleteText('component29')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton29" onclick="editText('component29')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton29" onclick="removeEdit('editComp_29',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_30">${newUserFormComponents[24].component.componentName}</p>
								<input type="text"  id="editComp_30" name="editComp_30" value = "${newUserFormComponents[24].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId30" value="${newUserFormComponents[24].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[24].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component30" style='float: right;' onclick="deleteText('component30')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton30" onclick="editText('component30')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton30" onclick="removeEdit('editComp_30',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_31">${newUserFormComponents[25].component.componentName}</p>
								<input type="text"  id="editComp_31" name="editComp_31" value = "${newUserFormComponents[25].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId31" value="${newUserFormComponents[25].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[25].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component31" style='float: right;' onclick="deleteText('component31')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton31" onclick="editText('component31')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton31" onclick="removeEdit('editComp_31',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_32">${newUserFormComponents[26].component.componentName}</p>
								<input type="text"  id="editComp_32" name="editComp_32" value = "${newUserFormComponents[26].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId32" value="${newUserFormComponents[26].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[26].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component32" style='float: right;' onclick="deleteText('component32')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton32" onclick="editText('component32')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton32" onclick="removeEdit('editComp_32',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_33">${newUserFormComponents[27].component.componentName}</p>
								<input type="text"  id="editComp_33" name="editComp_33" value = "${newUserFormComponents[27].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId33" value="${newUserFormComponents[27].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[27].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component33" style='float: right;' onclick="deleteText('component33')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton33" onclick="editText('component33')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton33" onclick="removeEdit('editComp_33',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_34">${newUserFormComponents[28].component.componentName}</p>
								<input type="text"  id="editComp_34" name="editComp_34" value = "${newUserFormComponents[28].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId34" value="${newUserFormComponents[28].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[28].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component34" style='float: right;' onclick="deleteText('component34')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton34" onclick="editText('component34')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton34" onclick="removeEdit('editComp_34',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_35">${newUserFormComponents[29].component.componentName}</p>
								<input type="text"  id="editComp_35" name="editComp_35" value = "${newUserFormComponents[29].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25"  class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId35" value="${newUserFormComponents[29].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[29].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component35" style='float: right;' onclick="deleteText('component35')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton35" onclick="editText('component35')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton35" onclick="removeEdit('editComp_35',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_36">${newUserFormComponents[30].component.componentName}</p>
								<input type="text"  id="editComp_36" name="editComp_36" value = "${newUserFormComponents[30].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25"  class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId36" value="${newUserFormComponents[30].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[30].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component36" style='float: right;' onclick="deleteText('component36')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton36" onclick="editText('component36')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton36" onclick="removeEdit('editComp_36',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_37">${newUserFormComponents[31].component.componentName}</p>
								<input type="text"  id="editComp_37" name="editComp_37" value = "${newUserFormComponents[31].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId37" value="${newUserFormComponents[31].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[31].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component37" style='float: right;' onclick="deleteText('component37')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton37" onclick="editText('component37')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton37" onclick="removeEdit('editComp_37',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_38">${newUserFormComponents[32].component.componentName}</p>
								<input type="text"  id="editComp_38" name="editComp_38" value = "${newUserFormComponents[32].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId38" value="${newUserFormComponents[32].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[32].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component38" style='float: right;' onclick="deleteText('component38')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton38" onclick="editText('component38')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton38" onclick="removeEdit('editComp_38',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_39">${newUserFormComponents[33].component.componentName}</p>
								<input type="text"  id="editComp_39" name="editComp_39" value = "${newUserFormComponents[33].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId39" value="${newUserFormComponents[33].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[33].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component39" style='float: right;' onclick="deleteText('component39')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton39" onclick="editText('component39')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton39" onclick="removeEdit('editComp_39',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_40">${newUserFormComponents[34].component.componentName}</p>
								<input type="text"  id="editComp_40" name="editComp_40" value = "${newUserFormComponents[34].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId40" value="${newUserFormComponents[34].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[34].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component40" style='float: right;' onclick="deleteText('component40')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton40" onclick="editText('component40')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton40" onclick="removeEdit('editComp_40',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_41">${newUserFormComponents[35].component.componentName}</p>
								<input type="text"  id="editComp_41" name="editComp_41" value = "${newUserFormComponents[35].component.componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId41" value="${newUserFormComponents[35].component.componentId}">
								</span>
								<c:if test="${newUserFormComponents[35].component.componentName != ' '}">
								<a href="javascript:void(0);" id="component41" style='float: right;' onclick="deleteText('component41')">
									<img id="iok" src="../images/iDelete.png" width="17px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton41" onclick="editText('component41')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="17px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton41" onclick="removeEdit('editComp_41',27)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="17px" height="17px">
								</a>
							</div>
							<div class="divCell1"><%=getSVG%></div>
						</div>
					</div>
				</div>
				<!-- Form 2 -->
				<div class=" bottomtext" style="width:88%;">
				   <div style="width:838px; height:25px;">
					<span id="cmt" style="float:left;">Comments:</span> 
					<hr style="width:90%;position:relative; top:18px;"/>
				   </div>	
					<br /><hr/><br /><hr/><br />
				   <div style="width:838px; height:25px;">	
					<span id="cmt" style="float:left;clear:left;margin-top: -25px;">Inspected by: </span> 
					<hr style="width:60%;float:left;position:relative; top:-5px;"/>
					<span style="float: left; margin-top: -25px; margin-left: 60.5%;">Date:</span>
 					<hr style="width:25%;float:left;position:relative; top:-5px;"/>
 				   </div>	
				</div>
				<div style="text-align:center;margin-top:10px;padding-bottom:10px;overflow:auto; padding-left:380px;">
					<input type="button" name="save" class="savebtn"  value="Save" onclick="updateForm();launchWindow('#dialog');">
						<!-- <a id="cancelButtonId" href="./getUserComponentIfs.do"  class="btn-txt"><input type="button" class="cancelbtn" value="Cancel"></a> -->
				</div>
			</form>
			<form  id="cancelAdminForm" method="get" action="./getUserViewComponentIfs.do" >
			<input type="hidden" name="formid" id="formid" value = "${userForm.userFormId}" />
				<input type="submit" value = "Cancel" class="cancelbtn1" onclick="launchWindow('#dialog')">
			</form>
		</div>
		</div>									
	</div>
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
