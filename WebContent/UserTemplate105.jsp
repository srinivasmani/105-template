<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Inspection</title>
        <link rel="stylesheet" type="text/css" href="../css/stylesheet.css"/>
        <script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
        <script type="text/javascript" src="../js/popup.js"></script>
        <script type="text/javascript" src="../js/image-resizing.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/popUp.css">
            <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
            <%@ page import="java.io.*,java.util.*" language="java"%>
            <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
            <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
            <%@ include file="header.jsp"%>
            <style>
                #msg {
                    font-size: 10pt;
                    letter-spacing: 0.5px;
                    margin-left: 33%;
                    margin-top: -16%;
                }
                .imgLogo {
                    border: 1px solid black;
                    height: 200px;
                    margin-top: 0;
                    padding: 0;
                    text-align: center;
                    width: 393px;
                }

                .customer-details {
                    font-family: 'MyriadProRegular';
                    font-size: 11pt;
                    list-style: none outside none;
                    margin-top: 5px;
                }

                .customer-details li {
                    border-bottom: 1px solid #333;
                    padding:15px 0 3px;
                    display: block;
                }

                #customer {
                    margin-left: 0;
                }
                
                #templateSize.template-105 {
                    //margin-left: 48px;
                }
                
                #templateSize.template-105 .divLogoTab{
                    border-spacing: 0;
                    float: left;
                    margin-left: 10px;
                    width: 354px;
                }
                #templateSize.template-105 .divLogoTab .imgLogo{
                    float: left;
                    height: 250px !important;
                    width: 350px !important;
                    margin-left: 24px !important;
                    margin-top: 25px !important;
                }
                #templateSize.template-105 .divLogoTab .imgLogo #msg {
                    margin-left: 25% !important;
                    margin-top: 30% !important;
                }
                #templateSize.template-105 #customer.divCell  {
                    margin-right: 7px;
                }
                #templateSize.template-105 #customer.divCell .customer-details li{
                    padding: 2px 0 3px;
                    height: 19px;
                    margin-bottom: 13px;
                }
                #templateSize.template-105 .inspectionleftwrap {
                    width: 392px;
                }
                #templateSize.template-105 .divTable { margin-top: 0px;}
                #templateSize.template-105 .inspectionTxt4 {
                    font-size: 11.5pt;
                }
                #templateSize.commentsClass {
                    font-family: 'MyriadProRegular';
                    font-size: 13pt;
                    height: 1px;
                    margin-left: 8px;
                    padding-top: 5px;
                }
                .width160 {
                    width: 160px;
                }
                .brake-dots {
                    display: block; 
                    overflow: hidden; 
                    margin: 2px 0px;
                }
                .brake-dots b { 
                    margin-left: 4px;
                }
                .margin-top2 {margin-top:4px;}
                .smallGreenCircle, .smallRedCircle, .smallYellowCircle {
                    height:23px;
                    width: 23px;
                }
                 #logoText{
                    height: 230px;
                    width: 350px;
                }
                #iframelogotext{
                border: 1px solid black;
                height: 280px;
                width: 395px;
                padding-top: 5px;
                }
                .smallGreenCircle, .smallRedCircle, .smallYellowCircle {
                    width: 22px;
                    height: 22px;
                }
                
                 #logoText h1 {
                float: none;
                font-size: 22px;
            }
            #logoText h2 {
                float: none;
                font-size: 16.5px;
            }
            #logoText h3 {
                float: none;
                font-size: 12.8px;
            }
            #logoText h4 {
                float: none;
                font-size: 11px;
            }
            #logoText h5 {
                float: none;
                font-size: 9px;
            }
            #logoText h6 {
                float: none;
                font-size: 7.3px;
            }
            .inspection_bg .greyBg span.th {
                width: 100%;
            }
            #logoText p {
                float: none;
                word-wrap: break-word;
            }
            //h2.titleFont{
            //    font-style: normal;
            //}
            //.txtFont{
            //    font-style: normal;
            //}
            </style>
            <script type="text/javascript">
                
                $('document').ready(function() {
                   $('#iframelogotext').contents().find('html').html(jQuery("div#logoText").html());
                   $('#iframelogotext').contents().find('div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,label,fieldset,input,p,blockquote').css({"margin-top":"1px","margin-bottom":"0px"});
                });
            </script>
    </head>
    <body>
        <div class="container" style="padding:0px;">
            <%@ include file="WEB-INF/header/userNavigation.jsp"%>
            <div id="centercontainer" class="centercontainer">
                <div class="nav">
                    <ul>
                        <c:if test="${userForm.status != 'default'}">
                            <li><a href="javascript:void(0);" class="selected" id="formNameTab">${userForm.formName}</a></li>
                            </c:if>
                            <c:if test="${userForm.status == 'default'}">
                            <li><a href="./navigateTemplateView.do" onclick="launchWindow('#dialog');">Template 101</a></li>
                            <li><a href="./getUserComponentIfs.do" onclick="launchWindow('#dialog');">Template 102</a></li>
                            <li><a href="./getUserFormView103.do" onclick="launchWindow('#dialog');">Template 103</a></li>
                            <li><a href="./createUserComponentForm104.do" onclick="launchWindow('#dialog');">Template 104</a></li>
                            <li><a href="./createUserComponentForm105.do" class="selected" onclick="launchWindow('#dialog');">Template 105</a></li>
                            </c:if>
                    </ul>
                </div>

                <%String getSVG = (String) request.getAttribute("imgType");

                    String green = "";
                    String yellow = "";
                    String red = "";
                    String greenSmall = "";
                    String yellowSmall = "";
                    String redSmall = "";
                    if ((getSVG != null) && (getSVG.equalsIgnoreCase("circle"))) {
                %>
                <% getSVG = "<b class='greenCircle'></b><b class='yellowCircle'></b><b class='redCircle'></b>";
                    green = "<b class='greenCircle'></b>";
                    yellow = "<b class='yellowCircle'></b>";
                    red = "<b class='redCircle'></b>";
                    greenSmall = "<b class='smallGreenCircle'></b>";
                    yellowSmall = "<b class='smallYellowCircle'></b>";
                    redSmall = "<b class='smallRedCircle'></b>";
                        } else {%>
                <% getSVG = "<b class='green'></b><b class='yellow'></b><b class='red'></b>";
                        green = "<b class='green'></b>";
                        yellow = "<b class='yellow'></b>";
                        red = "<b class='red' ></b>";
                        greenSmall = "<b class='smallGreen' style='width:20px; height:20px;'></b>";
                        yellowSmall = "<b class='smallYellow' style='width:20px; height:20px;'></b>";
                        redSmall = "<b class='smallRed' style='width:20px; height:20px;'></b>";
                            }%>


                <div style="clear: both"></div>

                <div class="innercontainer">
                    <div id="templateSize" class="template-105">

                        <!--Header-->
                        <div id="pageHeading">
                            <p id="heading">
                                <label id="lblText" class="text_label4" style="text-transform: uppercase;"><nobr><c:out value="${userForm.headerText}"/></nobr></label>
                            </p>
                            <div class="clear"></div>
                        </div>

                        <!--Logo-->       
                        <div class="divLogoTab">
                            <div class="divLogoRow">
                                <div class="yourlogo">
                                    <div align="center">
                                        <c:choose>
                                            
                                            <c:when test="${userForm.formImage ne null && userForm.formImage ne ''}">
                                                <div id="img" align="center" class="imgLogo" style="border: 0px solid black;">
                                                    <img src="/ImageLibrary/${userForm.formImage}" style="margin:0; max-width:350px; max-height:250px;">
                                                </div>
                                            </c:when>
                                            <c:when test="${userForm.logoText  ne  null && userForm.logoText ne ''}">
                                                <div id="logoText" style="display:none;"><c:out value="${userForm.logoText}" escapeXml="false"/></div>
                                                <iframe id="iframelogotext">                                                
                                                    
                                                </iframe>
                                            </c:when>
                                            <c:otherwise>
                                                <div id="img" align="center" class="imgLogo">
                                                    <p id="msg">(Your logo here)<br/>Dimension:[350 x 250]pixels</p>
                                                </div>
                                            </c:otherwise>
                                            
                                        </c:choose>
                                    </div>
                                </div>	
                            </div>
                        </div>

                        <!--Name-->
                        <div class="divTable" style="width:390px; float: right;clear:none;">
                            <div class="divRow">
                                <div id="customer" class="divCell">
                                    <div id="customerPDiv" style="border: 0px solid #000;">
                                        <c:if test="${newUserFormComponents[0].status.statusId == 1}">
                                            <p  id="customerP" class="inspec_formp moreWidth" style="width:390px; display: none;">
                                                <span style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">
                                                    <ul class="customer-details">
                                                        <c:forEach var="cnameSplit" items="${fn:split(newUserFormComponents[0].component.componentName, '_')}" >
                                                            <li> ${cnameSplit}</li> 
                                                            </c:forEach>
                                                    </ul>
                                                </span>
                                            </p>
                                        </c:if>				 
                                    </div>											
                                </div>						
                            </div>
                        </div>

                        <div class="clear"></div>

                        <!--Checked-Future-Immediate-->
                        <div class="selTable">
                            <div class="selRow">
                                <div class="selCol selCol3" style="margin-right:25px;">
                                    <div class="selCell">														
                                        <%=green%>
                                        <span class="floatLeft">

                                            <c:if test="${newUserFormComponents[1].status.statusId == 1}">
                                                <p id="attentionP" class="editme1">
                                                    <c:out value="${newUserFormComponents[1].component.componentName}"/>
                                                </p>
                                            </c:if>

                                        </span>

                                    </div>
                                </div>

                                <div class="selCol selCol3" style="margin-right:40px;">
                                    <div class="selCell">								
                                        <%=yellow%>
                                        <span id="nobr" class="floatLeft">
                                            <c:if test="${newUserFormComponents[2].status.statusId == 1}">
                                                <p id="fAttentionP" class="editme1">
                                                    <c:out value="${newUserFormComponents[2].component.componentName}"/>
                                                </p>
                                            </c:if>																		
                                        </span>

                                    </div>
                                </div>
                                <div class="selCol selCol3" style="margin-right:0px;">
                                    <div class="selCell">
                                        <%=red%>
                                        <span id="nobr" class="floatLeft">
                                            <c:if test="${newUserFormComponents[3].status.statusId == 1}">
                                                <p id="iAttentionP" class="editme1">
                                                    <c:out value="${newUserFormComponents[3].component.componentName}"/>																
                                                </p>
                                            </c:if>																		
                                        </span>

                                    </div>
                                </div>

                            </div>
                        </div>                

                        <div class="clear"></div>

                        <div class="divTable paddingLeft" style="padding-left: 0;">
                            <div  class="inspectionleftwrap">
                                <div class="inspection_bg">

                                    <!--Car-->
                                    <div class="inspectionTable">

                                        <c:if test="${newUserFormComponents[4].status.statusId == 1}">
                                            <div class="clear row1 row1Title greyBg">
                                                <span align="center" class="th"><c:out value="${newUserFormComponents[4].component.componentName}"/></span>
                                            </div>
                                        </c:if>

                                        <c:if test="${newUserFormComponents[5].status.statusId == 1}">
                                            <div class="clear row1" style="text-align:center;padding:3px;">
                                                <span class="smallheading"><c:out value="${newUserFormComponents[5].component.componentName}"/></span>
                                            </div>
                                        </c:if>
                                        <div class="clear row1">
                                            <div  class="alignCenter" style="display:block; height:196px;">
                                                <img src="/ImageLibrary/${newUserFormComponents[70].component.componentName}" alt="${newUserFormComponents[70].component.componentName}" style="max-height: 192px;max-width:244px;" />
                                            </div>
                                        </div>

                                        <c:forEach var="i" begin="6" end="9" step="1" varStatus ="status">

                                            <div class="clear row1">
                                                <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                    <span class="inspectionTxt4 leftAlignTxt"><c:out value="${newUserFormComponents[i].component.componentName}"/></span>
                                                </c:if>
                                                <span class="floatRightTxt"><%=getSVG%></span>
                                            </div>

                                        </c:forEach> 

                                    </div>        
<!--Underhood-->              
                                    <div class="inspectionTable">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${newUserFormComponents[10].status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${newUserFormComponents[10].component.componentName}"/></span>
                                            </c:if>
                                        </div>
                                        <c:forEach var="i" begin="11" end="17" step="1" varStatus ="status">
                                            <div class="clear row1">
                                                <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                    <span class="inspectionTxt4 leftAlignTxt"><c:out value="${newUserFormComponents[i].component.componentName}"/></span>
                                                </c:if>
                                                <span class="floatRightTxt"><%=getSVG%></span>
                                            </div>
                                        </c:forEach> 
                                    </div>

                                    <!--Under Vehicle-->
                                    <div class="inspectionTable">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${newUserFormComponents[18].status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${newUserFormComponents[18].component.componentName}"/></span>
                                            </c:if>
                                        </div>
                                        <div class="clear row1">
                                            <c:if test="${newUserFormComponents[19].status.statusId == 1}">
                                                <span class="inspectionTxt4 leftAlignTxt" style="min-height: 45px; padding: 0px !important; width: 290px;"><c:out value="${newUserFormComponents[19].component.componentName}"/> </span>
                                            </c:if>
                                            <span class="floatRightTxt"><%=getSVG%></span>
                                        </div>
                                        <c:forEach var="i" begin="20" end="23" step="1" varStatus ="status">
                                            <div class="clear row1">
                                                <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                    <span class="inspectionTxt4 leftAlignTxt" style="padding: 0px !important; width: 290px;"><c:out value="${newUserFormComponents[i].component.componentName}"/> </span>
                                                </c:if>
                                                <span class="floatRightTxt"><%=getSVG%></span>
                                            </div>

                                        </c:forEach> 
                                    </div>

                                    <div class="clear"></div>

                                </div>
                            </div>



                            <!--                            right side                -->
                            <div  class="inspectionrightwrap" style="margin-left: 32px;">
                                <div class="inspection_bg">
                                    <!--  Tires-->
                                    <div class="inspectionTable" style="border-bottom:1px solid #000;">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${newUserFormComponents[24].status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${newUserFormComponents[24].component.componentName}"/></span>
                                            </c:if>
                                        </div>

                                        <div class="clear row1" style="border-bottom:0px;">
                                            <div style="padding:0px; border:0px;height:130px" >
                                                <div class="bordernone interior_inspec">
                                                    <div class="alignCenter clear paddingBottom" style="width:360px; padding-bottom: 0;">
                                                        <c:if test="${newUserFormComponents[25].status.statusId == 1}">
                                                            <span><h2 class="noInspec"><c:out value="${newUserFormComponents[25].component.componentName}"/></h2></span>
                                                        </c:if>
                                                    </div>
                                                    <div class="clear paddingBottom" style="width:385px; height:30px;">
                                                        <c:if test="${newUserFormComponents[26].status.statusId == 1}">
                                                            <span style="width:138px; float:left;"><%=green%>
                                                                <span class="fontF4"><c:out value="${newUserFormComponents[26].component.componentName}"/></span>
                                                            </span>
                                                        </c:if>
                                                        <c:if test="${newUserFormComponents[27].status.statusId == 1}">
                                                            <span style="width:123px; float:left;"><%=yellow%>
                                                                <span class="fontF4"><c:out value="${newUserFormComponents[27].component.componentName}"/></span>
                                                            </span>
                                                        </c:if>
                                                        <c:if test="${newUserFormComponents[28].status.statusId == 1}">
                                                            <span style="width:124px; float:left;"><%=red%>
                                                                <span class="fontF4"> <c:out value="${newUserFormComponents[28].component.componentName}"/></span>
                                                            </span>
                                                        </c:if>
                                                    </div>

                                                    <div class="clear">
                                                        <div class="alignCenter" style="width:375px;">

                                                            <div class="bordernone interior_inspec interior_inspecLeft width160">
                                                                <span class="txt_bold" style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${newUserFormComponents[29].status.statusId == 1 && newUserFormComponents[30].status.statusId == 1}">
                                                                            <strong><c:out value="${newUserFormComponents[29].component.componentName}"/></strong>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${newUserFormComponents[30].status.statusId == 1 && newUserFormComponents[29].status.statusId == 1}">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;"><strong>
                                                                            <c:out value="${newUserFormComponents[30].component.componentName}"/>
                                                                        </strong>
                                                                    </span>
                                                                </c:if>

                                                            </div>

                                                            <div class="bordernone interior_inspec interior_inspecRight width160">
                                                                <span class="txt_bold"  style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${newUserFormComponents[31].status.statusId == 1  && newUserFormComponents[32].status.statusId == 1 }">
                                                                            <strong><c:out value="${newUserFormComponents[31].component.componentName}"/></strong>   
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${newUserFormComponents[32].status.statusId == 1  && newUserFormComponents[31].status.statusId == 1 }">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;">
                                                                        <strong><c:out value="${newUserFormComponents[32].component.componentName}"/></strong>
                                                                    </span>
                                                                </c:if>
                                                            </div>

                                                            <div class="bordernone interior_inspec interior_inspecLeft width160">

                                                                <span class="txt_bold" style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${newUserFormComponents[33].status.statusId == 1 && newUserFormComponents[34].status.statusId == 1}">
                                                                            <strong><c:out value="${newUserFormComponents[33].component.componentName}"/></strong>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${newUserFormComponents[34].status.statusId == 1 && newUserFormComponents[33].status.statusId == 1}">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;">
                                                                        <strong><c:out value="${newUserFormComponents[34].component.componentName}"/></strong>
                                                                    </span>
                                                                </c:if>

                                                            </div>

                                                            <div class="bordernone interior_inspec interior_inspecRight width160">
                                                                <span class="txt_bold"  style="float:left;margin-right:4px;width:18px;">
                                                                    <c:choose>
                                                                        <c:when test="${newUserFormComponents[35].status.statusId == 1 && newUserFormComponents[36].status.statusId == 1}">
                                                                            <strong><c:out value="${newUserFormComponents[35].component.componentName}"/></strong>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            &nbsp;
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </span>
                                                                <c:if test="${newUserFormComponents[36].status.statusId == 1 && newUserFormComponents[35].status.statusId == 1}">
                                                                    <span style="width: 73px; float:left;"><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                                    <span class="txt_bold" style="float:left;"><strong><c:out value="${newUserFormComponents[36].component.componentName}"/></strong></span>
                                                                        </c:if>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div></div>
                                        </div>


                                        <div style="height:200px; border-top: 2px solid #000;">
                                            <div style="float:left; width:200px;">

                                                <div style="padding:0px;width:100px;float:left;">
                                                    <img src="/ImageLibrary/${newUserFormComponents[71].component.componentName}" alt="${newUserFormComponents[71].component.componentName}" style="max-width: 100px;max-height: 170px;"/>
                                                </div>

                                                <div  class="bordernone interior_inspec padding_reset lessWidth" style="padding:0px;float:right; padding-top:15px;width:100px !important;">
                                                    <div style="height:30px; margin-bottom:10px;">
                                                        <c:if test="${newUserFormComponents[37].status.statusId == 1}">
                                                            <h2 class="titleFont" style="text-align:center;"><c:out value="${newUserFormComponents[37].component.componentName}"/></h2></span>
                                                        </c:if>
                                                    </div>
                                                    <c:if test="${newUserFormComponents[38].status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft" style="width:18px;" > <strong><c:out value="${newUserFormComponents[38].component.componentName}"/></strong></span>
                                                            <span width="284" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                        </div>
                                                    </c:if>


                                                    <c:if test="${newUserFormComponents[39].status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft"  style="width:18px;"><strong><c:out value="${newUserFormComponents[39].component.componentName}"/></strong></span>
                                                            <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                        </div>
                                                    </c:if>

                                                    <c:if test="${newUserFormComponents[40].status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft"  style="width:18px;"><strong><c:out value="${newUserFormComponents[40].component.componentName}"/></strong></span>
                                                            <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${newUserFormComponents[41].status.statusId == 1}">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <span class="txt_bold txtLeft"  style="width:18px;"><strong><c:out value="${newUserFormComponents[41].component.componentName}"/></strong></span>
                                                            <span><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>

                                                        </div>
                                                    </c:if>
                                                </div>
                                            </div>

                                            <div class="interior_inspec" style="height:200px;border-right: 2px solid #000 !important;border-left: 2px solid #000 !important;padding:0px;width:96px; padding:0 3px; float:left;">
                                                <div cellspacing="0" class="bordernone padding_reset" style="">
                                                    <div style="text-align:center;margin-top:15px;">
                                                        <c:if test="${newUserFormComponents[42].status.statusId == 1}">
                                                            <h2 class="titleFont" style="text-align:center;padding-bottom:4px;"><c:out value="${newUserFormComponents[42].component.componentName}"/></h2>
                                                        </c:if>
                                                    </div>
                                                    <div>
                                                        <span style="float:left; width:16px;position:relative; top:7px;margin-left:15px;"><%= redSmall%></span>
                                                        <span class="fontTxt" style="display:block; float:right;width:65px;text-align:center;">
                                                            <img src="/ImageLibrary/${newUserFormComponents[72].component.componentName}" alt="${newUserFormComponents[72].component.componentName}" style="max-width: 40px;max-height: 36px;"/></span>
                                                    </div>

                                                    <div class="bordernone interior_inspec">
                                                        <div class="beforeAfter" style="margin-top: 2px;">
                                                            <c:if test="${newUserFormComponents[43].status.statusId == 1}">
                                                                <span class="beforeTxt"><c:out value="${newUserFormComponents[43].component.componentName}"/></span>
                                                            </c:if>
                                                            &nbsp&nbsp;
                                                            <c:if test="${newUserFormComponents[44].status.statusId == 1}">
                                                                <span class="afterTxt"><c:out value="${newUserFormComponents[44].component.componentName}"/></span>
                                                            </c:if>
                                                        </div>
                                                        <div style="width:100%; margin-top: 3px;"  class="clear">
                                                            <div  style="float:left;width:15px;">
                                                                <span style="position:relative;top:7px;"><strong>
                                                                        <c:if test="${newUserFormComponents[45].status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${newUserFormComponents[45].component.componentName}"/></span>
                                                                        </c:if>
                                                                    </strong></span><br />
                                                                <span  style="position:relative;top:15px;"><strong>
                                                                        <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${newUserFormComponents[46].component.componentName}"/></span>
                                                                        </c:if>
                                                                    </strong></span>
                                                            </div>
                                                            <div style="width:30px; float:left;margin-right:4px;">
                                                                <span class="white_box">&nbsp;</span><br />
                                                                <span class="white_box">&nbsp;</span></div>&nbsp;
                                                                <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                                                <span class="white_box_rectangle">&nbsp;</span>
                                                            </c:if>
                                                        </div>
                                                        <div style="width:100%;"  class="clear">
                                                            <div  style="float:left;width:15px;">
                                                                <span style="position:relative;top:7px;"><strong>
                                                                        <c:if test="${newUserFormComponents[47].status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${newUserFormComponents[47].component.componentName}"/></span>
                                                                        </c:if>
                                                                    </strong></span><br />
                                                                <span  style="position:relative;top:15px;"><strong>
                                                                        <c:if test="${newUserFormComponents[48].status.statusId == 1}">
                                                                            <span class="txt_bold"><c:out value="${newUserFormComponents[48].component.componentName}"/></span>
                                                                        </c:if> 
                                                                    </strong></span>
                                                            </div>
                                                            <div style="width:30px; float:left;margin-right:4px;"><span class="white_box">&nbsp;</span><br />
                                                                <span class="white_box">&nbsp;</span></div>&nbsp;
                                                                <c:if test="${newUserFormComponents[46].status.statusId == 1}">
                                                                <span class="white_box_rectangle">&nbsp;</span>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>        

                                            <div class="interior_inspec" style="padding:0px;width:74px;float:left; padding:0 3px;"class="bordernone padding_reset">
                                                <div style="margin-top:15px;">
                                                    <c:if test="${newUserFormComponents[49].status.statusId == 1}">
                                                        <span><h2 class="titleFont" style="text-align:center;"><c:out value="${newUserFormComponents[49].component.componentName}"/></h2></span>
                                                        </c:if>
                                                </div>
                                                <div  class="bordernone interior_inspec">
                                                    <c:forEach var="i" begin="50" end="53" step="1" varStatus ="status">
                                                        <div class="clear" style="height:20px;margin-bottom:10px;">
                                                            <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                                <span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                                                <span class="txtFont" style="vertical-align:-3px;"><c:out value="${newUserFormComponents[i].component.componentName}"/></span>
                                                            </c:if>
                                                        </div>
                                                    </c:forEach> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                              Tire ends                      -->


                                    <!--                                     Brakes               -->
                                    <div class="inspectionTable inspectionTableBg" style="border-bottom:1px solid #000; overflow: hidden;">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${newUserFormComponents[54].status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${newUserFormComponents[54].component.componentName}"/></span>
                                            </c:if>
                                        </div>
                                        <div class="clear alignCenter paddingBottom" style="width:200px; margin-left: 80px;margin-top:10px;">
                                            <c:if test="${newUserFormComponents[55].status.statusId == 1}">              
                                                <span class="txt_bold"><h2 class="noInspec" style="width: 200px;"><c:out value="${newUserFormComponents[55].component.componentName}"/></h2></span>
                                                </c:if>
                                        </div>
                                        <div class="clear row1" style="border-bottom:0px; width: 190px; float: left;margin-left: 20px;">
                                            <div style="padding:0px; border:0px;height:130px" >
                                                <div class="bordernone interior_inspec">
                                                    <div class="clear paddingBottom" style="width:200px; height:30px;">
                                                        <c:if test="${newUserFormComponents[56].status.statusId == 1}">
                                                            <span class="clear" style="display:block;margin-bottom:5px;"><%= green%>
                                                                <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"><c:out value="${newUserFormComponents[56].component.componentName}"/></span>
                                                            </span>
                                                        </c:if>	
                                                        <c:if test="${newUserFormComponents[57].status.statusId == 1}">
                                                            <span class="clear" style="display:block;margin-bottom:5px;"><%= yellow%>

                                                                <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"> <c:out value="${newUserFormComponents[57].component.componentName}"/></span>

                                                            </span>
                                                        </c:if>
                                                        <c:if test="${newUserFormComponents[58].status.statusId == 1}">	
                                                            <span class="clear" style="display:block;margin-bottom:5px;"><%= red%>
                                                                <span class="fontF4 fontF49" style="display:block; width:160px; float:left;word-wrap: break-word;"><c:out value="${newUserFormComponents[58].component.componentName}"/></span>
                                                            </span>
                                                        </c:if>

                                                    </div>
                                                    <div class="clear">

                                                    </div>
                                                </div></div>
                                        </div>

                                        <div style="  width: 125px; float: right;">
                                            <div  class="bordernone interior_inspec padding_reset" style="padding:5px;">
                                                <c:forEach var="i" begin="59" end="62" step="1" varStatus ="status">
                                                    <c:if test="${newUserFormComponents[i].status.statusId == 1}">
                                                        <div class="clear">
                                                            <span class="txt_bold txtLeft margin-top2"> <strong><c:out value="${newUserFormComponents[i].component.componentName}"/></strong></span>
                                                            <span class="brake-dots" ><%= greenSmall%><%= yellowSmall%><%= redSmall%></span>
                                                        </div>
                                                    </c:if>
                                                </c:forEach> 
                                            </div>
                                        </div>

                                        <div class="clear" style="width:300px;margin:auto;">
                                            <c:if test="${newUserFormComponents[63].status.statusId == 1}">
                                                <span class="fontF4" style="float:left;display:block; width:76px;margin-top:12px;"><c:out value="${newUserFormComponents[63].component.componentName}"/></span>
                                            </c:if>
                                            <img src="/ImageLibrary/${newUserFormComponents[73].component.componentName}" alt="${newUserFormComponents[73].component.componentName}" style="float:left;margin-left:3px; margin-right:3px;max-width:150px; max-height:45px; " />
                                            <span class="fontF4" style="float:left;"> </span>
                                            <c:if test="${newUserFormComponents[64].status.statusId == 1}">
                                                <span class="fontF4" style="float:left;display:block; width:66px;margin-top:12px;"><c:out value="${newUserFormComponents[64].component.componentName}"/></span>
                                            </c:if>				   
                                        </div>


                                    </div>



                                    <!--Brake ends-->





                                    <!--Battery-->                   <!-- Battery -->
                                    <div class="inspectionTable">
                                        <div class="clear row1 row1Title greyBg">
                                            <c:if test="${newUserFormComponents[65].status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${newUserFormComponents[65].component.componentName}"/></span>
                                            </c:if>
                                        </div>

                                        <div class="clear row1">
                                            <div style="width:245px; float:left;padding:5px 0;">
                                                <c:if test="${newUserFormComponents[66].status.statusId == 1}">
                                                    <span class="textFont4"><c:out value="${newUserFormComponents[66].component.componentName}"/></span>
                                                </c:if>
                                                <div style="padding:10px 15px 0 40px;;">
                                                    <%=getSVG%>
                                                </div>
                                            </div>
                                            <div>
                                                <div style="width:120px;height:90px; float:left;">
                                                    <img src="/ImageLibrary/${newUserFormComponents[74].component.componentName}" alt="${newUserFormComponents[74].component.componentName}" style="max-width: 115px;max-height: 90px;" /> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>            
                                    <!--Battery ends-->




                                    <!--                                Comments-->
                                    <div class="inspectionTable" style="margin-bottom: 0px;">
                                        <div class="clear" style="font-family:'MyriadProRegular';font-size: 11pt;height: 1px;margin-left: 8px;padding-top: 5px;">
                                            <c:if test="${newUserFormComponents[67].status.statusId == 1}">
                                                <span align="center" class="th"><c:out value="${newUserFormComponents[67].component.componentName}"/></span>
                                            </c:if>
                                        </div>
                                        <div class="clear row1" style="height:26px;"></div>
                                        <div class="clear row1" style="height:26px;"></div>
                                        <div class="clear row1" style="height:26px;"></div>
                                    </div>

                                    <div class="bottomtext" style="width: 393px;overflow:hidden;">
                                        <div style="width:400px;">
                                            <c:if test="${newUserFormComponents[68].status.statusId == 1}">
                                                <c:set var="componentDesc" value="${newUserFormComponents[68].component.componentName}"/>
                                            </c:if>
                                            <div style="float:left;width:279px;overflow:hidden;">
                                                <span id="cmt" style="float: left;">${newUserFormComponents[68].component.componentName}</span>
                                                <span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>  
                                            </div> 
                                            <c:if test="${newUserFormComponents[69].status.statusId == 1}">
                                                <c:set var="componentDesc" value="${newUserFormComponents[69].component.componentName}"/>
                                            </c:if>
                                            <div style="float:right;width: 120px;">
                                                <span id="component69Span" class="comments" style="float: left;">${newUserFormComponents[69].component.componentName}</span> 
                                                <span style="display:block;overflow:hidden;margin-top: 19px;"><hr class="cmtLine" id="cmtLine3" style="width: 125px"/></span>
                                            </div> 
                                        </div>
                                    </div>            

                                </div>
                                <!--                                                Comments ends-->
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div style="text-align:right;padding-right:20px;font-family:'MyriadProRegular'; font-size:6pt;position:relative; top:45px;"></div>

                        <div class="containerbtm btnContainer">
                            <nobr>
                                <c:if test="${user.userRole eq 'user'}">
                                <a style="margin-right:5px;" href="./userEditTemplate105ByFormId.do?formId=${userForm.userFormId}" class="EditBtn btn-txt" 
                                   onclick="launchWindow('#dialog');">
                                    Edit Template
                                    <span style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
                                </a>
                                </c:if>
                                <a href="./GenerateUserFormPdf.do?userFormId=${userForm.userFormId}" target="_newtab"  class="PdfBtn btn-txt">
                                    Generate PDF
                                    <span style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
                                </a>
                            </nobr>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
    </body>

