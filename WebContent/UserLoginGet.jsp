<!DOCTYPE html>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="js/jquery-latest.js"></script>
<link rel="stylesheet" type="text/css" href="css/style-edit.css">
<link rel="stylesheet" type="text/css" href="css/popUp.css">
		<title>Login</title>
<script>
	jQuery(document).ready(function() {
		jQuery("#submitId").click(function() {
			jQuery('#userSignUpForm').submit();
		});
	});
	function launchWindow(id) {
		var divTobeCleared=document.getElementById('successMessage');
		if(divTobeCleared!=null){
			divTobeCleared.innerHTML="";
		}
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		//transition effect  
		$('#mask').fadeIn(0); 
		$('#mask').fadeTo("slow",0.4); 
	 
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
	              
		//Set the popup window to center
		$(id).css('left', winW/3);
		//transition effect
		$(id).fadeIn(2000);
	}
</script>
</head>
	

<body>
	<!--container starts-->
	<div class="container">
		<!--Header starts-->
		<div class="header"></div>
		<!--Header Ends-->
		<!--Center Container starts-->
		<div class="centercontainer">
			<!--Innercontainer starts-->
			<div class="innercontainer">
				<div class="admincontainer">
					<form name="userSignUpForm" id="userSignUpForm" method="post"
						action="Login.do" accept="text/html">
						<fieldset style="width: 300px;">
							<br />
							<div class="space">
								<div>
									<label>User Name :*</label> <input type="text" name="username"
										id="username" />
								</div>
								<br />
							</div>
							<div class="space">
								<div>
									<label>User Role&nbsp;&nbsp; :*</label> <input type="text"
										name="role" id="role" />
								</div>
							</div>
							<br />
							<div class="signupbutton">
								<a id="submitId" onclick="launchWindow('#dialog');"><img src="images/submit.png"></img> </a>
							</div>
						</fieldset>
					</form>
					<p>&nbsp;</p>
					<p>
						<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
					</p>
				</div>
			</div>
			<!--Innercontainer Ends-->
		</div>
		<!--Center Container Ends-->
	</div>
	<div id="boxes"><div id="dialog" class="window"><img src="images/ajaxloader.gif"/></div></div><div id="mask"></div>
	<!--container ends-->
</body>
