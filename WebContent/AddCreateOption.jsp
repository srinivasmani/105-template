<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<div>
	  	<select id="editOption" onchange = "changeOPtion(this.id)">
		  	<c:forEach var="subOption" items="${component.subComponents.subComponentsOptions}">
				<c:if test="${subOption.status == 'true'}">
					<option value="${subOption.subComponentOptionsId}" selected="selected">${subOption.options.optionName }</option>
				</c:if>
				<c:if test="${subOption.status != 'true'}">
					<option value="${subOption.subComponentOptionsId }">${subOption.options.optionName }</option>
				</c:if>
			</c:forEach>
			<c:if test="${user.userRole == 'admin'}">
				<c:if test="${fn:length(component.subComponents.subComponentsOptions) < 3}">
					<option value="Create Option">Create Option</option>
				</c:if>
			</c:if>
 		</select>
</div>
<div style="margin-top: 10px;">
	<select id="editOptionType" name="editOptionType">
		<option id="1" value="Square">Square</option>
		<option id="2" value="Circle">Circle</option>
		<option id="3" value="None">None</option>
	</select>
</div>
<div style="margin-top: 10px;">
	<div class="sideheading" id="editDesc">
	
	</div>
</div>
<script>
jQuery(document).ready(function(){
	getCreatedOption();
});
function getCreatedOption(){
	var selectOpt = jQuery("#editOption").attr("value");
	jQuery("#edited").val("true");
	jQuery.ajax({
		url :"./getOptions.do",
		type :"POST",
		async: false,
		dataType : "html",
		data :"optId="+selectOpt,
		success : function(data) {
			jQuery("#editDesc").empty();
			jQuery("#editDesc").html(data);
		},
		complete: function(xData, status){
			jQuery("#editOptionType").val((jQuery("#selectedType").val()));
			jQuery("#sideHeadingOption").append('<div class="sideheading" id="tickType"><select id="editOptionType" name="editOptionType"><option id="1" value="Square">Square</option><option id="2" value="Circle">Circle</option><option id="3" value="None">None</option></select>');
			jQuery("#createCompOptionDetails").css("display", "none");
			var curDiv = jQuery("#currentDiv").attr("value");
			var compNum = curDiv.split("_");
			setOptionDrpListRight(compNum);
		},
		error : function(xmlhttp, error_msg) {
		}
		});
}
</script>