	<!DOCTYPE html>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ page isELIgnored="false"%>
	<%@ include file="header.jsp"%>
	<link rel="stylesheet" type="text/css" href="../css/style-edit.css" />
	<title>Template View</title>
	<script type="text/javascript" src="../js/jquery-1.7.min.js"></script>
	<script type="text/javascript" src="../js/popup.js"></script>
	<script type="text/javascript" src="../js/image-resizing.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/popUp.css">
	<!--container starts-->
	<script type="text/javascript">
		jQuery(document).ready(function() {
			var headerColor = jQuery("#headerColor").val();
			if ((headerColor == "") || (headerColor == null)) {
				jQuery("#headerColor").val("#045fb4");
			} else {
				jQuery(".text").css("color", jQuery("#headerColor").val());
			}
		});
	</script>
	<div class="container">
	<div class="header"></div>
	<%@ include file="WEB-INF/header/userNavigation.jsp"%>
	<div class="centercontainer">
		<div class="nav">
			<ul id="headerList">
		<li><a href="./navigateTemplateView.do" class="selected" id="tem1" onclick="launchWindow('#dialog');">Template 101</a>
		</li>
		<li><a href="./navigateTemplateIfs.do" class="notSelected" id="tem2" onclick="launchWindow('#dialog');">Template 102</a>
		</li>
		<li><a href="./navigateTemplate103.do" class="notSelected" id="tem3" onclick="launchWindow('#dialog');">Template 103</a>
		</li>
		<li><a href="./navigateTemplate104.do" class="notSelected" id="tem4" onclick="launchWindow('#dialog');">Template 104</a>
		</li>
                <li><a href="./navigateTemplate105.do" class="notSelected" id="tem6" onclick="launchWindow('#dialog');">Template 105</a>
		</li>
		<li><a href="./addImageToLibrary.do" class="notSelected" id="tem5" onclick="launchWindow('#dialog');">Add image </a>
		</li>
	</ul>
 
		</div>
	<div style="clear: both"></div>
	<div class="innercontainer" id="innercontainer">
	<div class="VCcentercontent"><b>${template.headerText}</b>
	<c:choose>
		<c:when test="${template.templateImage eq null || template.templateImage eq ''}">
			<div id="imgView" align="center" class="yourlogo">
				<span id="msg" class="textlogo">(Your logo here)<br/>Dimension:[340 x 75]pixels</span>
			</div>
		</c:when>
		<c:otherwise>
			<div id="imgViewNoBorder" align="center"> 
				<img src="/ImageLibrary/${template.templateImage}" style="max-width:336px; max-height:72px;">
			</div>
		</c:otherwise>
	</c:choose>
    <span class="formtxt"> Mileage ____________________________</span>
    </div>
	<div class="clear"></div>
	<!--Vc Inner container starts-->
	<div class="VCcontinner"><!--List left starts-->
	<div class="listcontatleft"><!-- List out first eight elements -->
	<input type="hidden" name="templateId" id="templateId"
		value="${template.templateId}" /> <input type="hidden"
		name="headerColor" id="headerColor" value="${template.headingColor}" />
	<input type="hidden" name="currHeaderColor" id="currHeaderColor"
		value="${template.headingColor}" />
	<ul class="VClist">
		<c:forEach items="${leftComponents}" var="formComponent" varStatus="status">
			
			<c:if test="${formComponent.templateComponent.status.statusId == 1}">
			<li> 
				<div id="visibleDiv${status.count}" class="ListDivVisible"></div>
			  <c:if test="${formComponent.templateComponent.status.statusId == 2}">
				<div id="visibleDiv${status.count}" class="ListDivHidden"></div>
			</c:if>
			<div class="VClistdiv"><span class="text"><b>${formComponent.componentName}</b></span>
			<c:if
				test="${(status.count != 1)  && (status.count != 3) && (status.count != 6)}">
				<span class="subText" id="labelSpan${status.count}">${formComponent.subComponents.label}</span>
			</c:if></div>
			<c:forEach items="${formComponent.subComponents.subComponentsOptions}"
				var="subComponentOption">
				<c:if test="${subComponentOption.status == 'true'}">
					<div class="checkboxcnt">
						<c:forEach var="option" items="${fn:split(subComponentOption.options.optionDescription, '^')}">
							<div class="checkbox">
								<c:if test="${option != '' && option != null }">
								<span> 
									<img src="../images/${fn:toLowerCase(subComponentOption.optionType)}.png">
									<label class="option">${option}</label> 
								</span>
								</c:if>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</c:forEach>
	
			<div class="clear"></div>
			</li>
	   </c:if>
			
			 
		</c:forEach>
	</ul>
	</div>
	<!--List left Ends--> <!--List right starts-->
	<div class="listcontatright"><!-- List out rest seven elements -->
	<ul class="VClist">
		<c:forEach items="${rightComponents}" var="formComponentRight"
			varStatus="status">
			<c:if test="${formComponentRight.templateComponent.status.statusId == 1}">
				<li>
				<div id="visibleDiv${status.count+8}" class="ListDivVisible">
			 <c:if
				test="${formComponentRight.templateComponent.status.statusId == 2}">
				<div id="visibleDiv${status.count+8}" class="ListDivHidden">
			</c:if>
			<div class="VClistdiv  "><span class="text"><b>${formComponentRight.componentName}</b></span>
			<c:if test="${(status.count != 5)}">
				<span class="subText" id="labelSpan${status.count+8}">${formComponentRight.subComponents.label}</span>
			</c:if></div>
			<c:forEach
				items="${formComponentRight.subComponents.subComponentsOptions}"
				var="subComponentOptionRight">
				<c:if test="${subComponentOptionRight.status == 'true'}">
					<c:choose>
						<c:when test="${status.count != 7}">
							<div class="checkboxcnt">
								<span> 
									<c:forEach var="optionRight" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}">
										<div class="checkbox">
											<c:if test="${optionRight != '' && optionRight != null }">
											<span> 
												<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png">
												<label class="option">${optionRight}</label> 
											</span>
											</c:if>
										</div>
									</c:forEach> 
								</span>
							</div>
						</c:when>
						<c:otherwise>
							<div class="checkboxcnt"><c:forEach var="optionRight" items="${fn:split(subComponentOptionRight.options.optionDescription, '^')}">
								<div class="checkbox15">
									<c:if test="${optionRight != '' && optionRight != null }">
										<span> 
											<img src="../images/${fn:toLowerCase(subComponentOptionRight.optionType)}.png">
											<label class="option">${optionRight}</label> 
										</span>
									</c:if>
								</div>
							</c:forEach></div>
							<br />
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	
			<div class="clear"></div>
	</div>
	</li>
	</c:if>
	</c:forEach>
	</ul>
	<div class="clear"></div>
	</div>
	
	<!--List right Ends-->
	<div class="clear"></div>
	</div>
	<!--Vc Inner container Ends-->
	
	<div class="clear"></div>
	<div class=" bottomtext1">
		<span id="cmt" style="float:left;">Comments:</span> 
		<hr style="width:90%;margin-top: 3%;*margin-top:-4%;"/><br /><hr /><br /><hr /><br />
		<span id="cmt" style="float:left;clear:left;margin-top: -25px;">Inspected by: </span> 
		<hr style="width:60%;position:relative;top:-5px;float:left;" />
		<span style="float: left; margin-top: -25px; margin-left: 60.5%;">Date:</span>
 		<hr style="width:23%;position:relative;top:-5px;float:left;" />
	</div>
	<br>
	<br>
	
	<div id="btnClick"><c:if test="${user.userRole =='admin'}">
		<div class="containerbtm" style="padding-left:300px;"><nobr> 
		<a style="margin-right: 5px;" href="./editTemplate2.do" class="EditBtn btn-txt" onclick="launchWindow('#dialog');">Edit Template
		<span  style="position:relative;left:8px;top:6px"><img src="../images/editimg.png"></span>
		</a> 
		
		<a href="./GenerateTemplate1Pdf.do?templateId=${template.templateId}" target="_newtab" class="PdfBtn btn-txt">Generate PDF
		<span  style="position:relative;left:15px;top:3px"><img src="../images/pdfimg.png"></span>
		</a> </nobr></div>
	</c:if> <c:if test="${user.userRole=='user'}">
		<div class="containerbtm"><nobr> <a
			style="margin-right: 5px;" href="./editTemplate2.do"><img
			src="../images/Edit_btn.png" class="EditBtn"></a> <a href="#"><input
			class="PdfBtn" type="button" /> </a> </nobr></div>
	</c:if></div>
	</div>
	</div>

<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>