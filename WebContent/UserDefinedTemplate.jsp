<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ include file="header.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Defined Template</title>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css">
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-adminEditTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.miniColors.js"></script>
<script type="text/javascript" src="../js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<style>
/*Pagination for custom display table*/
.paginationLeft {
	width: 213px;
	display: block;
	text-align: right;
	font-size: 12px;
	color: #585858;
	font-weight: bold;
	line-height: 30px;
	background-color: transparent;
	margin-bottom: 10px;
	margin-left: 4px;
	border-bottom: 1px dotted #565656;
}

.paginationLeftPopup {
	width: 95%;
	font-family: helvetica;
}

.pagination {
	float: left;
	text-align: left;
	background-color: #transparent;
}

.paginationRight {
	width: 100%;
	float: right;
	text-align: left;
	padding: 8px 10px 0px 0px;
	font-family: "Helvetica Neue", Arial, Verdana;
	font-size: 12px;
	margin-bottom: 6px;
}

.paginationRightDiv1 {
	float: right; *
	padding-top: 2px;
}

.paginationRightDiv2 {
	float: left;
	padding: 0px 0px;
	color: #000000;
	font-size: 12px;
	width:160px;
}
.paginationRightDiv2 span{
 margin:0;
 display:block;
 width:160px;
 margin:0px;
}
.paginationRightDiv2 a {
	text-decoration: none;
	color: #000000;
}
page
.paginationRightDiv3 {
	float: right; *
	padding-top: 2px;
}

.displayTable {
	padding: 12px 5px;
	color: #000;
	width: 223px;
	padding: 0px;
	margin: 0px;
}

.displayTable  tr td {
	color: #000;
	list-style: none;
	display: block;
	background: #e6e6e6;
}

.odd {
	padding: 0px;
	color: #000;
	font-size: 14px;
	text-decoration: none;
	display: block;
	background: #e6e6e6;
	border-bottom: 1px solid #565656;
	width:220px;
    word-wrap: break-word;
}

.even {
	padding: 0px;
	color: #000;
	font-size: 14px;
	text-decoration: none;
	display: block;
	background: #e6e6e6;
	border-bottom: 1px solid #565656;
	width:220px;
    word-wrap: break-word;
}

.links a,.links strong {
	background-color:#fff;
    border: 1px solid #565656;
    
    display: block;
    float: left;
    height: 11px;
    left: 25px;
    line-height: 12px;
    margin-right: 3px;
    padding: 2px 2px 2px;
    position: relative;
    text-align: center;
   /* vertical-align: top;*/
    width: 10px;
}
.links strong{
background-color:#680C0F;
color:#fff;
}

.pageLinkFirst{
	color: #565656;
	font-size: 12px;
	font-family: helvetica;
	font-weight: bold;
	vertical-align: top;
	height: 15px;
	line-height: 12px;
}
#lastPg, .pageLinkLast {
    background-color:#EEEEEE;
    border: 1px solid #565656;
    
    color: #565656;
    font-family: helvetica;
    font-size: 12px;
    font-weight: bold;
    height: 15px;
    line-height: 12px;
    padding: 2px 4px;
   position: relative;
    top: -16px;
   /*  vertical-align: top;*/
}

.pageLinkFirst {
	background-color: #EEEEEE;
    border: 1px solid #565656;
     color: #565656;
    font-weight: bold;
    left: 3px;
    padding: 2px 4px;
    position: relative;
    top:2px;
}
.odd td a, .even td a{
 color:#000 !important;
  display: block;
    margin: 0;
    padding: 12px 5px;
}
/*diaplay table custom css end*/
</style>
<!--container starts-->
<div class="container">
<%@ include file="WEB-INF/header/userNavigation.jsp"%>
	<!--Header starts-->
	<div class="header"></div>
	<!--Header Ends-->
	<!--Center Container starts-->
	<div class="centercontainer">
		<!--Innercontainer starts-->
		<div class="innercontainer">
			<div class="userleftcount">
			 <form action="./navigateSearchUser.do" method="get" class="searchForm">
			  <input type="text" id="searchString" name="searchString" value="${searchString}" class="searchTB" />
		       <input type="submit" value="Search User" class="btn-txt" id="SearchComp" />
		     
		     </form>
			<input type="hidden" id="len" value="${pageId1}" />
			<input type="hidden" id="impUserId" value="${userId}" />

				<div class="headingbg">User Defined Templates</div>
				<div class="clear"></div>
				<div class="verticltab_container" style="font-size: 10pt;font-family: 'MyriadProRegular';color: black;">
					<display:table name="${users}" id="userName" sort="list" pagesize="12" requestURI="" class="displayTable" defaultsort="1" >
			<display:setProperty name="basic.msg.empty_list"  value="No such user found." />
						<display:column property="userName" href="./navigateUserCreatedTemplate.do" paramId="id" paramProperty="userId" title="" sortName="userName" headerClass="userId" />
						 <%@include file="./displayTagPropertiesUser.jsp" %>
					</display:table>
				</div>
			</div>
			<div class="right_container_forms" id = "userFormsList">
				<div id="user1">
					<div style='clear: both; padding:10px 0;'>
					<div style="border-bottom:1px solid #000; overflow:auto;width:705px;">
					<div id='sno'><font color='#A52A2A'><b>SL No.</b> </font></div>
					<div id='name'><font color='#A52A2A'><b>Name</b> </font></div>
					<div id='master'><font color='#A52A2A'><b>Master Template</b> </font></div>
					<div id='actionHead'><font color='#A52A2A'><b>Action</b> </font></div>
					</div>
					</div>
					<div id="innerData" style="width:705px;">
					<c:forEach var="form" items="${userForms}" varStatus="status">
						<c:if test="${form.status=='active'}">
						<div style="border-bottom:1px solid #000; overflow:auto;">
							<div id="sno">${status.count}</div>
							<div id="name_${status.count}" class="name">${form.formName}</div>
							<div id="master">
								<c:if test="${form.templateId == 1}">
									101
								</c:if>
								<c:if test="${form.templateId == 2}">
									102
								</c:if>
								<c:if test="${form.templateId == 3}">
									103
								</c:if>
								<c:if test="${form.templateId == 4}">
									104
								</c:if>
                                                                <c:if test="${form.templateId == 5}">
									105
								</c:if>        
							</div>
							<div id="action">
								<c:if test="${form.templateId == 1}">
									<a href="../user/userFormView.do?formid=${form.userFormId}" id="formLink" class="btn-txt formlink1" style="position:relative; top:5px;">View</a>
								</c:if>
								<c:if test="${form.templateId == 2}">
									<a href="../user/getUserViewComponentIfs.do?formid=${form.userFormId}" id="formLink" class="btn-txt formlink1" style="position:relative; top:5px;" >View</a>
								</c:if>
								<c:if test="${form.templateId == 3}">
									<a href="../user/getUserComponent103.do?formid=${form.userFormId}" class="btn-txt formlink1" id="formLink" class="btn-txt" style="position:relative; top:5px;" >View</a>
								</c:if>
								<c:if test="${form.templateId == 4}">
									<a href="../user/getUserFormByFormId104.do?formId=${form.userFormId}" class="btn-txt formlink1" id="formLink" class="btn-txt" style="position:relative; top:5px;">View</a>
								</c:if>
                                                                <c:if test="${form.templateId == 5}">
									<a href="../user/getUserFormByFormId105.do?formId=${form.userFormId}" class="btn-txt formlink1" id="formLink" class="btn-txt" style="position:relative; top:5px;">View</a>
								</c:if>
							</div>
							<div id="action">
								<input type="hidden" value="${form.userFormId}" id="userFormId_${status.count}">
								<input type="hidden" value="${form.userIfs.userId}" id="userIdIfs_${status.count}">
								<a href="#" id="deleteForm_${status.count}" class="deleteForm btn-txt">Delete</a>
							</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
<script type="text/javascript">

jQuery(document).ready(function(){
	 jQuery(".displayTable").find('.odd:first').find("td").css('background-color','#aaa');
	jQuery(".odd").click(function() {
	  var attr = $(this).find("td").find("a").attr("href");
	  var userPageId=attr.split("=");
	  var uri=attr+"&d-4037936-p="+jQuery("#len").val();
	  jQuery('tr').each(function() {
		  jQuery(this).css("background-color","#E6E6E6");
		  jQuery(this).find("td").css("background-color","#E6E6E6");
		});
	  jQuery(this).css("background-color","#aaa");
	  jQuery(this).find("td").css("background-color","#aaa");
	 
	   var uniqueClass ="kabi"+userPageId[1];
	  $(this).find("td").find("a").addClass(uniqueClass);
	 
	  jQuery(this).find("td").find("a").attr("href","#");
	   jQuery.ajax({   
	     url :uri,
	     type :"GET",
	     dataType : "html",   
	     success : function(data) {
	    jQuery("#innerData").html(data);
	   
	     },  
	     complete: function(){
	    jQuery("."+uniqueClass).attr("href",attr);
	    },
	     error : function(xmlhttp, error_msg) {
	     }
	   });
	 });
 
 jQuery(".even").click(function() {  
   var attr = $(this).find("td").find("a").attr("href");
   var userPageId=attr.split("=");
   var uri=attr+"&d-4037936-p="+jQuery("#len").val();
   jQuery('tr').each(function() {
		  jQuery(this).css("background-color","#E6E6E6");
		  jQuery(this).find("td").css("background-color","#E6E6E6");
		});
	 
	  jQuery(this).css("background-color","#aaa");
	  jQuery(this).find("td").css("background-color","#aaa");
	  
	 
   var uniqueClass ="kabi"+userPageId[1];
  $(this).find("td").find("a").addClass(uniqueClass);
   jQuery(this).find("td").find("a").attr("href","#");
    jQuery.ajax({ 
      url :uri,
      type :"GET",
      dataType : "html",   
      success : function(data) {      
      jQuery("#innerData").html(data);
      },  
      complete: function(){
    	  jQuery("."+uniqueClass).attr("href",attr);
    },
      error : function(xmlhttp, error_msg) {
      }
    });
  });
});
</script>

