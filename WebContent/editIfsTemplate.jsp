<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*" language="java"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="header.jsp"%>
<%@include file="UploadImage.jsp" %>
<meta http-equiv="content-language" content="en-GB" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/pagination.css">
<link type="text/css" rel="stylesheet" href="../css/jquery.miniColors.css" />
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<!-- Java Script -->
<script type="text/javascript" src="../js/popup.js"></script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.ui.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery.template2.js"></script>
<script type="text/javascript" src="../js/jquery-adminEditTemplate.js"></script>

<style>
.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity =   80);
	display: none;
}

.box_content {
	position: fixed;
	top: 25%;
	left: 25%;
	right: 30%;
	width: 575px;
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	border: 8px solid #ACACAC;
	background: none repeat scroll 0 0 #FFFFFF;
}

#msg {
	font-size: 10pt;
	margin-left: 0%;
	margin-top: 10%;
	letter-spacing: 0.5px;
}

#imgLogo {
	border: 1px solid black;
	height: 100px;
	width: 380px;
	margin-left: 200px;
	margin-top: 0px;
	padding: 0;
	text-align: center;
}

.delete_button {
	display: none;
}

.logo1 {
	float: left;
	height: 0;
	margin-right: 3px;
	margin-top: -2px;
}

.logo2 {
	float: right;
	height: 0;
	margin-right: 4px;
	margin-top: -113px;
}

.leftOptImage {
	border: 1px solid black;
	font-size: 12px;
	height: 100px !important;
	width: 100px !important;
}

.rghtOptImage {
    border: 1px solid black;
    font-size: 12px;
    height: 100px !important;
    margin-top: -5px;
    width: 100px !important;
}

.greenCircle {
	height: 41px;
	width: 41px;
	background: url("../images/green_circle.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
}

.yellowCircle {
	height: 41px;
	width: 41px;
	background: url("../images/Yellow_circle.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
}

.redCircle {
	height: 41px;
	width: 41px;
	background: url("../images/red_circle.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
}

.greenSquare {
	height: 41px;
	width: 41px;
	background: url("../images/green_large.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
	margin-right: 3px;
}

.yellowSquare {
	height: 41px;
	width: 41px;
	background: url("../images/yellow_large.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
	margin-right: 3px;
}

.redSquare {
	height: 41px;
	width: 41px;
	background: url("../images/red_large.png");
	background-repeat: no-repeat;
	border: none;
	float: left;
	margin-right: 3px;
}

.hiddenImage {
	height: 41px;
	width: 30px;
	display: block;
	border: none;
	float: left;
	background-color: #FFFFFF;
}
#headerText102{
	text-transform: uppercase;
}
.divCell a{
	float:right;
	margin-left:800px;
}
.divCell p{
	padding:5px 0px; 
}
#repositoryPopUp{
height:auto !important;
border:8px solid #acacac !important;
width:625px !important;
margin-top:-65px;
}
#item .odd, #item .even {
border: 1px solid #ACACAC !important;
margin-right:5px;
margin-bottom:5px;
}
.selectLink {
    left: 0px !important;
    position: relative;
}
</style>
<script>
//updateForm

 jQuery(document).ready(function(){
 var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
 var text = "";
 var custText = jQuery(".custSpanText").find(".customerSpanText");
	jQuery(custText).each(function(index){
		text = text+jQuery(this).text();
	});
	if(trimSpanText(text).length == 0){
		jQuery("#customer").css("border","1px solid white");
	}
	else{
		jQuery("#customer").css("border","1px solid #ACACAC");
	}
	if(is_chrome == true){
		jQuery("#customer").css("width", "760px");
		jQuery("#editComp_6_0").css("width", "760px");
		jQuery("#editComp_6_1").css("width", "760px");
		jQuery(".custSpanText").css("width", "846px");
	}
 });
 function checkStringLength(id, event)
 {
 	var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
 	var maxWidth = 750;
 	if(is_chrome == true){
 		maxWidth = 675;
 	}
 	//if(jQuery.browser.msie){
		//var maxWidth = 760;
		//}
 	jQuery("#temp1").empty();
 	jQuery("#temp1").text(jQuery("#"+id).val());
 	var matches = jQuery("#"+id).val();
 	var pattern=/_/g;
     var v=matches.match(pattern);
     var width = 0;
     jQuery("#temp1").hide();
     if(v!=null)
     {  
     	
     	width=jQuery("#temp1").width()-((matches.match(pattern).length/2)*.5);
     }
     else
     {
     	 width=jQuery("#temp1").width();
     } 
 	if(width>=maxWidth && (event.keyCode != 8 && event.keyCode != 46))
 	{
 		jQuery("#"+id).attr('maxlength',jQuery("#"+id).val().length);
 		alert("Cannot add more characters.");
 		if(id != "editComp_6_1"){
 			  jQuery("#editComp_6_1").focus();
     	}
 		return true;
 	}
 	else if(event.keyCode == 13 ){
     	if(id != "editComp_6_1"){
     		 jQuery("#editComp_6_1").focus();
     	}
     }
 }
function updateForm(){
	if(checkMandatory() == true){
		removeFirstComponentEdit('editComp_6',186);
		jQuery("#updateAdminForm").submit();
		return true;
	}
	else{
		return false;
	}
}
</script>
<div class="container">
	<div class="header"></div>
	<%@ include file="WEB-INF/header/userNavigation.jsp"%>
	<div class="centercontainer">
		<div class="nav">
			<ul>
				<%@ include file="WEB-INF/header/headerLink2.jsp"%>
			</ul>
		</div>
		<div style="clear: both"></div>
		<div class="innercontainer" id="innercontainer">
		
			<%
						String getSVG = (String) request.getAttribute("imgType");
						if (getSVG.equalsIgnoreCase("circle")) {
					%>
			<%
				getSVG = "<div class='svgTable' ><span><input type='button' class='hiddenImage' /><input type='button' class='greenCircle' />"
							+ "<input type='button' class='yellowCircle' />"
							+ "<input type='button' class='redCircle' /></span></div>";
				} else {
			%>
			<%
				getSVG = "<div class='svgTable' ><span><input type='button' class='hiddenImage' /><input type='button'  class='greenSquare' />"
							+ "<input type='button'  class='yellowSquare' />"
							+ "<input type='button' class='redSquare' /></span></div>";
				}
			%>
			<!-- Complete Width and Height -->
			<form id="updateAdminForm" name="uploadImage" action="./getFreeIns.do" method="post">
			<input type= "hidden" name="saveImage" id="saveImage" value="${template.templateImage}"/>
			<div id="templateSize">
				<!-- User Logo -->
				<div class="divLogoTab" style="width: 20.8cm;">
					<div class="divLogoRow">
							<div class="logo1">
								<div class="leftOptImage">
									<a href="javascript: void(0)"></a>
								</div>
							</div>
							<div id="imgLogo" align="center" class="imgLogo">
								<c:choose>
									<c:when test="${template.templateImage eq null or template.templateImage eq ''}">
										<p id="msg">(Your logo here)<br/>Dimension:[380 x 100]pixels</p>
									</c:when>
									<c:otherwise>
										<img style="max-width:380px !important;max-height:100px !important;" src="/ImageLibrary/${template.templateImage}">
									</c:otherwise>
								</c:choose>
							</div>
							<a href="javascript:void(0);" id="centerImageClick" onclick="centerImageSelectionPop('center',380,100); return false;" style="margin-left: 38%;height: 0;color:blue;font-family:'MyriadProRegular';font-size:11pt;">Click to upload logo</a> &nbsp;&nbsp;<span class="imgDimension">[380 x 100]</span>
							<div class="logo2">
								<div class="rghtOptImage">
									<a href="javascript: void(0);"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Page Heading -->
				<div id="pageHeading">
					<p id="headerText102" style="display: block; margin-left: 20px;font-size:20pt;">${template.headerText}</p>
					<input type="hidden" name="headingText102" id="headingText102" value="${template.headerText}"/>
					<input type="text" value="${template.headerText}" id="editText102" maxlength="44" />
					<a href="javascript:void(0);" id="editHeading102" style='float:right;margin-right: 51px;margin-top: -30px;'>
						<img src="../images/ieditover.PNG" width="16px" height="17px">
					</a>
					<a href="javascript:void(0);" id="okHeading102" style='display:none;' style='display:none;float:right;margin-right: 51px;margin-top: -30px;width:20px; height:20px;'>
						<img id="iok" src="../images/iok.png" width="16px" height="17px">
					</a>
					<div class="clear"></div>
				</div>
				<!-- Form 1 -->
				
				<div class="divTable" style="width:850px;">
					<div class="divRow">
						<div class="divCell" id="customer" style="border:1px solid #ACACAC;width: 733px;margin-left: 48px;">
							<div class="divCell" id="inner_Customer" >
							<c:forEach var="cname" items="${fn:split(template.components[0].componentName, '~')}" varStatus="stat">
								<input  id="editComp_6_${stat.count - 1}" name="editComp_6_${stat.count - 1}" type="text" style="text-align:left;display:none;width:733px;height:30px;font-family: MyriadProRegular;font-size: 11pt;border:1px solid black;"  onkeypress="return checkStringLength(this.id, event);" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" value="${cname}" onblur=" mouseOut${stat.count - 1}()"/>
							</c:forEach>
							</div> 
							<span id="divTxt_attention" style="display:none;">
								<input type="text" id="editComp_6" name="editComp_6" style="text-align:left;display:none;width:750px;height:30px;" value="${template.components[0].componentName}">
								<input type="hidden" id="componentId6" name="componentId6" value="${template.components[0].componentId}">
							</span>
							<c:forEach var="cname" items="${fn:split(template.components[0].componentName, '~')}" varStatus="stat">
								<div class ="custSpanText" style="width: 800px;">
									<span id="cust${stat.count}" style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">${cname}</span>
								</div>
							</c:forEach>
						</div>
						<a href="javascript:void(0);" id="editButton6" onclick="editTextFirstCompnent('component6')" style="float:left; position:relative; left:5px;">
							<img src="../images/ieditover.PNG" width="16px" height="17px">
						</a> 
						<a href="javascript:void(0);" id="okButton6" onclick="removeFirstComponentEdit('editComp_6',186)" style='display:none;float:left; position:relative; left:5px;'>
							<img id="iok" src="../images/iok.png" width="16px" height="17px">
						</a>
					</div>
				</div>
				<div id="commentDiv" style="font-family: 'MyriadProRegular', Sans-Serif; font-size: 11pt; letter-spacing: .8px; width: 960px;">
					<span id="temp1"></span>
				</div>
<%
	String getSVGImage = (String) request.getAttribute("imgType");
	if (getSVGImage.equalsIgnoreCase("circle")) {
%>
				<div style="padding:15px 65px;">
					<span class="circlesquare">
						<input type="radio" name="imgType" value="circle" checked="checked"/> <span class="circlesquare">Circle</span>
					</span>
					<span class="circlesquare">
						<input type="radio" name="imgType" value="square" /> <span class="circlesquare">Square</span>
					</span>
				</div>
				<%
					} else {
				%>
					<div style="padding:15px 62px;">
					<span class="circlesquare">
						<input type="radio" name="imgType" value="circle" /> Circle
					</span>
					<span class="circlesquare">
						<input type="radio" name="imgType" value="square" checked="checked" /> Square
					</span>
				</div>
					<%
						}
					%>
				<!-- ATTENTION -->
				<div class="selTable selTableEdit">
					<div class="selRow">
						<div class="selCol">
							<div class="selCell" id="selCell7">							
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<input type='button' class='greenCircle' />
									</c:when>
									<c:otherwise>
										<input type='button'  class='greenSquare' />
									</c:otherwise>
								</c:choose>								
								<nobr>
									<p id="comp_7">${template.components[1].componentName}</p>
									<input type="text" id="editComp_7" name="editComp_7" value = "${template.components[1].componentName}" style="display:none;float: none;width: 200px;" class="focusOutSave" maxlength="23"/>
									<span id="divTxt_attention" style="display:none;">
										<input type="hidden" name="componentId7" value="${template.components[1].componentId}">
									</span>
									<a href="javascript:void(0);" id="editButton7" onclick="editText('component7')">
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a> 
									<a href="javascript:void(0);" id="okButton7" onclick="removeEdit('editComp_7',23)" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
								</nobr>
							</div>
						</div>
						
						<div class="selCol">
							<div class="selCell" id="selCell8">
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<input type='button' class='yellowCircle' />
									</c:when>
									<c:otherwise>
										<input type='button'  class='yellowSquare' />
									</c:otherwise>
								</c:choose>	
								<nobr id="nobr">
									<p id="comp_8">${template.components[2].componentName}</p>
									<input type="text"  id="editComp_8" name="editComp_8" value = "${template.components[2].componentName}" style="display:none;float: none;width: 245px;" class="focusOutSave" maxlength="30"/>
									<span id="divTxt_attention" style="display:none;">
										<input type="hidden" name="componentId8" value="${template.components[2].componentId}">
									</span>
									<a href="javascript:void(0);" id="editButton8" onclick="editText('component8')">
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a> 
									<a href="javascript:void(0);" id="okButton8" onclick="removeEdit('editComp_8',30)" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
								</nobr>
							</div>
						</div>
						
						<div class="selCol">
							<div class="selCell" id="selCell9">
								<c:choose>
									<c:when test="${template.imageType eq 'circle'}">
										<input type='button' class='redCircle' />
									</c:when>
									<c:otherwise>
										<input type='button' class='redSquare' />
									</c:otherwise>
								</c:choose>
								<nobr id="nobr">
									<p id="comp_9">${template.components[3].componentName}</p>
									<input type="text"  id="editComp_9" name="editComp_9" value = "${template.components[3].componentName}" style="display:none;float: none;width: 186px;" class="focusOutSave" maxlength="23"/>
									<span id="divTxt_attention" style="display:none;">
										<input type="hidden" name="componentId9" value="${template.components[3].componentId}">
									</span>
									<a href="javascript:void(0);" id="editButton9" onclick="editText('component9')">
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a> 
									<a href="javascript:void(0);" id="okButton9" onclick="removeEdit('editComp_9',23)" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
								</nobr>								
							</div>
						</div>
						
					</div>
				</div>

				<!-- Items And Conditions -->
				<div class="divTable1">
					<div id="div1" style="margin-left:40px;">
						<div class="divRow">
							<div class="divCell1 rowheading" id="content">
									<p id="comp_10">${template.components[4].componentName}</p>
									<input type="text"  id="editComp_10" name="editComp_10" value = "${template.components[4].componentName}" style='display:none;float: none;width: 170px;' maxlength="10" class="focusOutSave" />
									<span id="divTxt_attention" style="display:none;">
										<input type="hidden" name="componentId10" value="${template.components[4].componentId}">
									</span>
									<a href="javascript:void(0);" id="editButton10" onclick="editText('component10')" style='float: right;'>
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a> 
									<a href="javascript:void(0);" id="okButton10" onclick="removeEdit('editComp_10',10)" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
							</div>
							<div class="divCell1 rowheading" id="content">
									<p id="comp_11">${template.components[5].componentName}</p>
									<input type="text"  id="editComp_11" name="editComp_11" value = "${template.components[5].componentName}" style='display:none;float: none;width: 170px;' maxlength="10" class="focusOutSave" />
									<span id="divTxt_attention" style="display:none;">
										<input type="hidden" name="componentId11" value="${template.components[5].componentId}">
									</span>
									<a href="javascript:void(0);" id="editButton11" onclick="editText('component11')" style='float: right;'>
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a> 
									<a href="javascript:void(0);" id="okButton11" onclick="removeEdit('editComp_11',10)" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_12">${template.components[6].componentName}</p>
								<input type="text"  id="editComp_12" name="editComp_12" value = "${template.components[6].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId12" value="${template.components[6].componentId}">
								</span>
								<c:if test="${template.components[6].componentName != ' '}">
								<a href="javascript:void(0);" id="component12" style='float: right;' onclick="deleteText('component12')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton12" onclick="editText('component12')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton12" onclick="removeEdit('editComp_12',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_13">${template.components[7].componentName}</p>
								<input type="text"  id="editComp_13" name="editComp_13" value = "${template.components[7].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId13" value="${template.components[7].componentId}">
								</span>
								<c:if test="${template.components[7].componentName != ' '}">
								<a href="javascript:void(0);" id="component13" style='float: right;' onclick="deleteText('component13')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton13" onclick="editText('component13')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton13" onclick="removeEdit('editComp_13',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_14">${template.components[8].componentName}</p>
								<input type="text"  id="editComp_14" name="editComp_14" value = "${template.components[8].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId14" value="${template.components[8].componentId}">
								</span>
								<c:if test="${template.components[8].componentName != ' '}">
								<a href="javascript:void(0);" id="component14" style='float: right;' onclick="deleteText('component14')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton14" onclick="editText('component14')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton14" onclick="removeEdit('editComp_14',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_15">${template.components[9].componentName}</p>
								<input type="text"  id="editComp_15" name="editComp_15" value = "${template.components[9].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId15" value="${template.components[9].componentId}">
								</span>
								<c:if test="${template.components[9].componentName != ' '}">
								<a href="javascript:void(0);" id="component15" style='float: right;' onclick="deleteText('component15')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton15" onclick="editText('component15')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton15" onclick="removeEdit('editComp_15',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_16">${template.components[10].componentName}</p>
								<input type="text"  id="editComp_16" name="editComp_16" value = "${template.components[10].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId16" value="${template.components[10].componentId}">
								</span>
								<c:if test="${template.components[10].componentName != ' '}">
								<a href="javascript:void(0);" id="component16" style='float: right;' onclick="deleteText('component16')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton16" onclick="editText('component16')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton16" onclick="removeEdit('editComp_16',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_17">${template.components[11].componentName}</p>
								<input type="text"  id="editComp_17" name="editComp_17" value = "${template.components[11].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId17" value="${template.components[11].componentId}">
								</span>
								<c:if test="${template.components[11].componentName != ' '}">
								<a href="javascript:void(0);" id="component17" style='float: right;' onclick="deleteText('component17')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton17" onclick="editText('component17')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton17" onclick="removeEdit('editComp_17',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_18">${template.components[12].componentName}</p>
								<input type="text"  id="editComp_18" name="editComp_18" value = "${template.components[12].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId18" value="${template.components[12].componentId}">
								</span>
								<c:if test="${template.components[12].componentName != ' '}">
								<a href="javascript:void(0);" id="component18" style='float: right;' onclick="deleteText('component18')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton18" onclick="editText('component18')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton18" onclick="removeEdit('editComp_18',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_19">${template.components[13].componentName}</p>
								<input type="text"  id="editComp_19" name="editComp_19" value = "${template.components[13].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId19" value="${template.components[13].componentId}">
								</span>
								<c:if test="${template.components[13].componentName != ' '}">
								<a href="javascript:void(0);" id="component19" style='float: right;' onclick="deleteText('component19')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton19" onclick="editText('component19')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton19" onclick="removeEdit('editComp_19',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_20">${template.components[14].componentName}</p>
								<input type="text"  id="editComp_20" name="editComp_20" value = "${template.components[14].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId20" value="${template.components[14].componentId}">
								</span>
								<c:if test="${template.components[14].componentName != ' '}">
								<a href="javascript:void(0);" id="component20" style='float: right;' onclick="deleteText('component20')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton20" onclick="editText('component20')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton20" onclick="removeEdit('editComp_20',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_21">${template.components[15].componentName}</p>
								<input type="text"  id="editComp_21" name="editComp_21" value = "${template.components[15].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId21" value="${template.components[15].componentId}">
								</span>
								<c:if test="${template.components[15].componentName != ' '}">
								<a href="javascript:void(0);" id="component21" style='float: right;' onclick="deleteText('component21')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton21" onclick="editText('component21')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton21" onclick="removeEdit('editComp_21',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_22">${template.components[16].componentName}</p>
								<input type="text"  id="editComp_22" name="editComp_22" value = "${template.components[16].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId22" value="${template.components[16].componentId}">
								</span>
								<c:if test="${template.components[16].componentName != ' '}">
								<a href="javascript:void(0);" id="component22" style='float: right;' onclick="deleteText('component22')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton22" onclick="editText('component22')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton22" onclick="removeEdit('editComp_22',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_23">${template.components[17].componentName}</p>
								<input type="text"  id="editComp_23" name="editComp_23" value = "${template.components[17].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId23" value="${template.components[17].componentId}">
								</span>
								<c:if test="${template.components[17].componentName != ' '}">
								<a href="javascript:void(0);" id="component23" style='float: right;' onclick="deleteText('component23')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton23" onclick="editText('component23')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton23" onclick="removeEdit('editComp_23',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_24">${template.components[18].componentName}</p>
								<input type="text"  id="editComp_24" name="editComp_24" value = "${template.components[18].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId24" value="${template.components[18].componentId}">
								</span>
								<c:if test="${template.components[18].componentName != ' '}">
								<a href="javascript:void(0);" id="component24" style='float: right;' onclick="deleteText('component24')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton24" onclick="editText('component24')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton24" onclick="removeEdit('editComp_24',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_25">${template.components[19].componentName}</p>
								<input type="text"  id="editComp_25" name="editComp_25" value = "${template.components[19].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"/>
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId25" value="${template.components[19].componentId}">
								</span>
								<c:if test="${template.components[19].componentName != ' '}">
								<a href="javascript:void(0);" id="component25" style='float: right;' onclick="deleteText('component25')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton25" onclick="editText('component25')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton25" onclick="removeEdit('editComp_25',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
					</div>
					
					
					<div id="div2" style="margin-right:35px;">
						<div class="divRow">
							<div class="divCell1 rowheading " id="content">
								<p id="comp_26">${template.components[20].componentName}</p>
								<input type="text"  id="editComp_26" name="editComp_26" value = "${template.components[20].componentName}" style='display:none;float: none;width: 170px;' maxlength="10" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId26" value="${template.components[20].componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton26" onclick="editText('component26')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton26" onclick="removeEdit('editComp_26',10)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>						
							</div>
							<div class="divCell1 rowheading" id="content">
								<p id="comp_27">${template.components[21].componentName}</p>
								<input type="text"  id="editComp_27" name="editComp_27" value = "${template.components[21].componentName}" style='display:none;float: none;width: 170px;' maxlength="10" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId27" value="${template.components[21].componentId}">
								</span>
								<a href="javascript:void(0);" id="editButton27" onclick="editText('component27')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton27" onclick="removeEdit('editComp_27',10)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>	
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_28">${template.components[22].componentName}</p>
								<input type="text"  id="editComp_28" name="editComp_28" value = "${template.components[22].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId28" value="${template.components[22].componentId}">
								</span>
								<c:if test="${template.components[22].componentName != ' '}">
								<a href="javascript:void(0);" id="component28" style='float: right;' onclick="deleteText('component28')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton28" onclick="editText('component28')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton28" onclick="removeEdit('editComp_28',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_29">${template.components[23].componentName}</p>
								<input type="text"  id="editComp_29" name="editComp_29" value = "${template.components[23].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId29" value="${template.components[23].componentId}">
								</span>
								<c:if test="${template.components[23].componentName != ' '}">
								<a href="javascript:void(0);" id="component29" style='float: right;' onclick="deleteText('component29')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton29" onclick="editText('component29')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton29" onclick="removeEdit('editComp_29',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_30">${template.components[24].componentName}</p>
								<input type="text"  id="editComp_30" name="editComp_30" value = "${template.components[24].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId30" value="${template.components[24].componentId}">
								</span>
								<c:if test="${template.components[24].componentName != ' '}">
								<a href="javascript:void(0);" id="component30" style='float: right;' onclick="deleteText('component30')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton30" onclick="editText('component30')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton30" onclick="removeEdit('editComp_30',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_31">${template.components[25].componentName}</p>
								<input type="text"  id="editComp_31" name="editComp_31" value = "${template.components[25].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId31" value="${template.components[25].componentId}">
								</span>
								<c:if test="${template.components[25].componentName != ' '}">
								<a href="javascript:void(0);" id="component31" style='float: right;' onclick="deleteText('component31')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton31" onclick="editText('component31')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton31" onclick="removeEdit('editComp_31',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_32">${template.components[26].componentName}</p>
								<input type="text"  id="editComp_32" name="editComp_32" value = "${template.components[26].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId32" value="${template.components[26].componentId}">
								</span>
								<c:if test="${template.components[26].componentName != ' '}">
								<a href="javascript:void(0);" id="component32" style='float: right;' onclick="deleteText('component32')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton32" onclick="editText('component32')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton32" onclick="removeEdit('editComp_32',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_33">${template.components[27].componentName}</p>
								<input type="text"  id="editComp_33" name="editComp_33" value = "${template.components[27].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId33" value="${template.components[27].componentId}">
								</span>
								<c:if test="${template.components[27].componentName != ' '}">
								<a href="javascript:void(0);" id="component33" style='float: right;' onclick="deleteText('component33')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton33" onclick="editText('component33')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton33" onclick="removeEdit('editComp_33',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_34">${template.components[28].componentName}</p>
								<input type="text"  id="editComp_34" name="editComp_34" value = "${template.components[28].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId34" value="${template.components[28].componentId}">
								</span>
								<c:if test="${template.components[28].componentName != ' '}">
								<a href="javascript:void(0);" id="component34" style='float: right;' onclick="deleteText('component34')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton34" onclick="editText('component34')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton34" onclick="removeEdit('editComp_34',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_35">${template.components[29].componentName}</p>
								<input type="text"  id="editComp_35" name="editComp_35" value = "${template.components[29].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId35" value="${template.components[29].componentId}">
								</span>
								<c:if test="${template.components[29].componentName != ' '}">
								<a href="javascript:void(0);" id="component35" style='float: right;' onclick="deleteText('component35')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton35" onclick="editText('component35')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton35" onclick="removeEdit('editComp_35',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_36">${template.components[30].componentName}</p>
								<input type="text"  id="editComp_36" name="editComp_36" value = "${template.components[30].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId36" value="${template.components[30].componentId}">
								</span>
								<c:if test="${template.components[30].componentName != ' '}">
								<a href="javascript:void(0);" id="component36" style='float: right;' onclick="deleteText('component36')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton36" onclick="editText('component36')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton36" onclick="removeEdit('editComp_36',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_37">${template.components[31].componentName}</p>
								<input type="text"  id="editComp_37" name="editComp_37" value = "${template.components[31].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId37" value="${template.components[31].componentId}">
								</span>
								<c:if test="${template.components[31].componentName != ' '}">
								<a href="javascript:void(0);" id="component37" style='float: right;' onclick="deleteText('component37')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton37" onclick="editText('component37')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton37" onclick="removeEdit('editComp_37',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_38">${template.components[32].componentName}</p>
								<input type="text"  id="editComp_38" name="editComp_38" value = "${template.components[32].componentName}" style='display:none;float: none;width: 170px;' maxlength="26" class="focusOutSave"  />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId38" value="${template.components[32].componentId}">
								</span>
								<c:if test="${template.components[32].componentName != ' '}">
								<a href="javascript:void(0);" id="component38" style='float: right;' onclick="deleteText('component38')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton38" onclick="editText('component38')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton38" onclick="removeEdit('editComp_38',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_39">${template.components[33].componentName}</p>
								<input type="text"  id="editComp_39" name="editComp_39" value = "${template.components[33].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId39" value="${template.components[33].componentId}">
								</span>
								<c:if test="${template.components[33].componentName != ' '}">
								<a href="javascript:void(0);" id="component39" style='float: right;' onclick="deleteText('component39')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton39" onclick="editText('component39')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton39" onclick="removeEdit('editComp_39',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_40">${template.components[34].componentName}</p>
								<input type="text"  id="editComp_40" name="editComp_40" value = "${template.components[34].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId40" value="${template.components[34].componentId}">
								</span>
								<c:if test="${template.components[34].componentName != ' '}">
								<a href="javascript:void(0);" id="component40" style='float: right;' onclick="deleteText('component40')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton40" onclick="editText('component40')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton40" onclick="removeEdit('editComp_40',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
						
						<div class="divRow">
							<div class="divCell1" id="content">
								<p id="comp_41">${template.components[35].componentName}</p>
								<input type="text"  id="editComp_41" name="editComp_41" value = "${template.components[35].componentName}" style='display:none;float: none;width: 170px;' maxlength="25" class="focusOutSave" />
								<span id="divTxt_attention" style="display:none;">
									<input type="hidden" name="componentId41" value="${template.components[35].componentId}">
								</span>
								<c:if test="${template.components[35].componentName != ' '}">
								<a href="javascript:void(0);" id="component41" style='float: right;' onclick="deleteText('component41')">
									<img id="iok" src="../images/iDelete.png" width="16px" height="17px">
								</a>
								</c:if>
								<a href="javascript:void(0);" id="editButton41" onclick="editText('component41')" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okButton41" onclick="removeEdit('component41',26)" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</div>
							<div class="divCell1">
								<%=getSVG%>
							</div>
						</div>
					</div>
				</div>
				
			<div class=" bottomtext" style="width:88%;">
				   <div style="width:838px; height:25px;">
					<span id="cmt" style="float:left;">Comments:</span> 
					<hr style="width:90%;position:relative; top:18px;"/>
				   </div>	
					<br /><hr/><br /><hr/><br />
				   <div style="width:838px; height:25px;">	
					<span id="cmt" style="float:left;clear:left;margin-top: -25px;">Inspected by: </span> 
					<hr style="width:60%;float:left;position:relative; top:-5px;"/>
					<span style="float: left; margin-top: -25px; margin-left: 60.5%;">Date:</span>
 					<hr style="width:25%;float:left;position:relative; top:-5px;"/>
 				   </div>	
				</div>
				<div style="text-align:center;margin-top:10px;padding-bottom:10px;overflow:auto;">
					<input type="button" name="save" value="Save" id="update102" onclick="updateForm();launchWindow('#dialog');" > 
				</div>
				</form>
				<form  id="cancelAdminForm" style="margin-left: 440px;margin-top: -36px;padding-bottom: 20px;text-align: center;" action="./navigateTemplateIfs.do" method="get">
					<input type="submit" value="Cancel" id="cancelBtn" onclick="launchWindow('#dialog');">
				</form>
	</div>
</div>
</div>
 <div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
