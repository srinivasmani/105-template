<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="header.jsp"%>
<title>User Document</title>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css" />
<style type="text/css">
div#boxes {
	width: 100%;
}

div#boxes div {
	width: 49.9%;
	float: left;
}
span{
 width:200px;
}
</style>
<script type="text/javascript" src="../js/jscolor.js"></script>
<script type="text/javascript" src="../js/jquery-latest.js"></script>
<script type="text/javascript" src="../js/jquery-editTemplate.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
			jQuery(".VClistdiv").mouseover(function(){
				var divId = jQuery(this).attr("id");
				var labelId = jQuery("#" + divId).children().attr("id");
				var imageId = jQuery("#" + labelId).next().attr("id");
				var imageDId = jQuery("#" + labelId).next().next().attr("id");
				jQuery("#" + imageId).show();
				jQuery("#" + imageDId).show();
				jQuery(this).css("background-color", "#EEEEEE");	
				jQuery(this).css("border-radius", "10px");
				jQuery(this).css("padding", "5px");
				jQuery("#" + imageDId).click(function(){
					jQuery("#" + divId).hide();
				});
			});
		});
</script>
<body>




	<!--container starts-->
	<div class="container">
		<!--Header starts-->
		<div class="header"></div>
		<!--Header Ends-->
		

		<!--Center Container starts-->
		<div class="insidecentercontainer">
			<!--Navigation starts-->
			<div class="nav">
				<%@ include file="WEB-INF/header/headerLink.jsp"%>
			</div>

			<div style="clear: both"></div>
			<!--Navigation Ends-->

			<!--Innercontainer starts-->
			<form name="adminEdit" id="adminEdit" method="post" action="./saveForm.do">
			<div class="insideinnercontainer">
				<div style="width: 532px; margin: 16px; float: left;">
					<div style="width: 532px; border: 1px #000 solid; float: left;">
						<div class="VCcentercontent" style="width: 360px !important">
							<b style="font-size: 28px">Vehicle Condition Report</b>
							<div class="yourlogo">( Your logo here )</div>
							<span class="formtxt"> <br />
								Mileage_____________________________________________</span>
						</div>
						<div id="boxes">
							<c:forEach items="${template.components}" var="component" varStatus="status" >
								<div id="boxone">
									<div class="VCcontinner">
										<div style="margin-left: 30px !important;">
											<ul class="VClist">
											<input type="hidden" name="viewPosition${status.count}" id="viewPosition${status.count}" value="${status.count}" />
											<input type="hidden" name="templatePosition${status.count}" id="templatePosition${status.count}" value="${component.position.positionId}" />
											<input type="hidden" name="componentName${status.count}"  id="componentName${status.count}" value="${component.componentName}" />
											<input type="hidden" name="label${status.count}"  id="label${status.count}" value="${component.subComponents.label}" />
												<li>
													<div class="VClistdiv">
														<span class="text">${component.componentName}</span> 
														<span>${component.subComponents.label}</span>
														<img class="iImage" src="../images/ieditover.PNG" id="editimage${status.count}"> 
														<img class="iDelete" src="../images/iDelete.png" id="delimage${status.count}">
														<c:forEach items="${component.subComponents.subComponentsOptions}" var="subComponentOption" >
															<c:if test="${subComponentOption.status == 'true'}">
															<c:if test="${status.count != 15}">
															<div class="checkboxcnt">
																<input type="hidden" name="option${status.count}"  id="option${status.count}" value="${subComponentOption.subComponentOptionsId}" />
																<div class="checkbox"><span>
																<c:forEach var="option" items="${fn:split(subComponentOption.options.optionDescription, '^')}">
																<input type="checkbox" /> <label>${option}</label>
											                 	</c:forEach>
											                 	</span>
											                 	</div>
											                 	</div>
										                 	</c:if>
											                <c:if test="${status.count == 15}">
											                 <div class="checkboxcnt">
																<input type="hidden" name="option${status.count}"  id="option${status.count}" value="${subComponentOption.subComponentOptionsId}" />
																<c:forEach var="option" items="${fn:split(subComponentOption.options.optionDescription, '^')}">
																<div class="checkbox"><span>
																<input type="checkbox" /> <label>${option}</label>
																</span>
											                 	</div>
											                 	</c:forEach>
										                 	</div>
											                </c:if>
											                 	
															</c:if>
														</c:forEach>
													</div>
													
												</li>
											</ul>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
						<!--Vc Inner container Ends-->
						<div class="bottomtext">
							<br />

							Comment_______________________________________________________<br />
							______________________________________________________________<br />

							______________________________________________________________<br />
							_____________________________________Date_____________________<br />
							<br />
						</div>
						<div class="clear"></div>
					</div>
					<!--Container Bottom Starts-->
					<div class="containerbtm">
						<input class="btngrey" value="Create component" type="button" />
					</div>
					<!--Container Bottom Ends-->
					<div class="clear"></div>
				</div>
				<!--Innercontainer left starts-->
				<div style="float: left; margin: 16px 16px 0px 0px; width: 346px;">
					<div class="rightcontainer">
						<div class="sideheading">
							<label>Form name</label>
							<div>
								<input />
							</div>
						</div>
						<div class="sideheading">
							<label>Browse Your Own</label> <input type="file" />
						</div>
						<div class="sideheading">
							<label>Choose From Existing Images </label> <input type="file" />
						</div>
						<div class="sideheading">
							<label><span> <img class="iImage"
									src="../images/images.png" style="width: 20px; height: 20px">&nbsp;&nbsp;&nbsp;Heading
									Colors</span> </label>
						</div>
					</div>
					<div style="float: left; margin: 16px 0px 16px 0px; width: 346px;">
						<div class="rightcontainer" style="display:none;">
							<div class="sideheading">
								<label>Show On Template</label> <br /> <input type="text"
									value="" /> <label>Heading</label> <br /> <select>
									<option value="LIGHTS" title="LIGHTS">LIGHTS</option>
									<option value="WIPER BLADES" title="WIPER BLADES">WIPER
										BLADES</option>
									<option value="OIL LABEL" title="OIL LABEL">OIL LABEL</option>
									<option value="POWER STERING FLUID" title="POWER STERING FLUID">POWER
										STERING FLUID</option>
									<option value="WASHER FLUID" title="WASHER FLUID">WASHER
										FLUID</option>
									<option value="TRANSMISSION FLUID" title="TRANSMISSION FLUID">TRANSMISSION
										FLUID</option>
									<option value="BELT(s) TENSIONER" title="BELT(s) TENSIONER">BELT(s)
										TENSIONER</option>
									<option value="ANTIFREEZE/COOLANT" title="ANTIFREEZE/COOLANT">ANTIFREEZE/COOLANT</option>
									<option value="AIR FILTER" title="AIR FILTER">AIR
										FILTER</option>
									<option value="CABIN FILTER" title="CABIN FILTER">CABIN
										FILTER</option>
									<option value="FUEL FILTER" title="FUEL FILTER">FUEL
										FILTER</option>
									<option value="BATTERY CONDITION" title="BATTERY CONDITION">BATTERY
										CONDITION</option>
									<option value="DIFFERENTIAL FLUID" title="DIFFERENTIAL FLUID">DIFFERENTIAL
										FLUID</option>
									<option value="TIRE CONDITION" title="TIRE CONDITION">TIRE
										CONDITION</option>
								</select>

							</div>
							<div class="sideheading">
								<label>SubHeading</label> <br /> <select
									name="DropDownExTextboxExample">
									<option value="60,000 Miles" title="60,000 Miles">60,000
										Miles</option>
									<option value="6,000 Miles" title="6,000 Miles">6,000
										Miles</option>
									<option value="30,000 Miles" title="30,000 Miles">30,000
										Miles</option>
									<option value="48,000 Miles" title="48,000 Miles">48,000
										Miles</option>
									<option value="12,000 Miles" title="12,000 Miles">12,000
										Miles</option>
									<option value="15,000 Miles" title="15,000 Miles">15,000
										Miles</option>
								</select>

							</div>
							<div class="sideheading">
								<label>Options</label> <br /> <select id="drpOption"
									style="width: 120px;">
									<option value="first">First</option>
									<option value="second">Second</option>

								</select>
							</div>


						</div>
						<!--Innercontainer  Ends-->
					</div>
					<!--Center Container Ends-->
				</div>
				</div>
				<div><input type="submit" name="submit" value="SAVE" /></div>
				</form>
				<!--container ends-->
</body>
