<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<link rel="stylesheet" type="text/css" href="../css/style-edit.css">
<script type="text/javascript" src="../js/jquery-latest.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<body>
	<b>Welcome ${user.userName}</b>
	<div id="pageHeading" align="center">My Inspection Forms</div>
	<div id="formList">
	<input type="hidden" value="${form.userFormId}" name="formId" value="formId" />
	<div id="pageContent">
		<c:if test="${userForms != null}">
			<c:forEach var="form" items="${userForms}" varStatus="status">
					<div style='clear: both;'>
						<div id='formNo'>
							<font color='#A52A2A'><b>${status.count}</b> </font>
						</div>
						<div id='formNames'>
							<font color='#A52A2A'>
							<b>							
							<c:choose>
								<c:when test="${form.templateId eq '1'}">
									<a href="../user/userFormView.do?formid=${form.userFormId}" id="formLink">${form.formName}</a>
								</c:when>
								<c:when test="${form.templateId eq '2'}">
									<a href="../user/getUserViewComponentIfs.do?formid=${form.userFormId}" id="formLink">${form.formName}</a>
								</c:when>
							</c:choose>
							</b>
							</font>
						</div>
						<div id='formDeleted'>
							<a href="../userFormDelete.do?formId=${form.userFormId}&userId=${user.userId}"
								id="formLink"> <img src="../images/Button-Close-icon.png" id="formDelete"> </a>
						</div>
						<div id='formPdf'>
							<a href="../user/GenerateUserFormPdf.do?userFormId=${form.userFormId}" id="formLink" target="_newtab"><img src="../images/PDF_icon.png"></a>
						</div>
					</div>
				</c:forEach>
		</c:if>
		<c:if test="${userForms == null}">
		<center>
			<p>You have not created any form ..</p>
		</center>
		</c:if>
		<c:if test="${fn:length(userForms) < 12}">
		<div id="createNewBtn" >
		<a href="../user/navigateTemplateView.do"><img src="../images/iCreate.PNG"></img></a>
		</div>
		</c:if>
		<c:if test="${fn:length(userForms) >= 12}">
		<center><p>Please delete one of the existing forms.</p></center>
		</c:if>
	</div>
	
	</div>
</body>


