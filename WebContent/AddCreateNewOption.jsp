<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<div id="editOpdtionDiv">
	  	<select id="editOption" onchange = "changeOPtion(this.id)">
		  	<c:forEach var="options" items="${component.subComponents.subComponentsOptions}">
				<c:if test="${options.status == 'true'}">
					<option value="${options.subComponentOptionsId}" selected="selected">${options.options.optionName }</option>
				</c:if>
				<c:if test="${options.status != 'true'}">
					<option value="${options.subComponentOptionsId }">${options.options.optionName }</option>
				</c:if>
			</c:forEach>
			<c:if test="${fn:length(selectedComp.subComponents.subComponentsOptions) < 3}">
				<option value="Create Option">Create Option</option>
			</c:if>
		</select>
</div>