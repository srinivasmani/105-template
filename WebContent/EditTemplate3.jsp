<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="header.jsp"%>
<%@ page language="java"%>
<%@include file="UploadImage.jsp" %>
<title>Edit Template 3</title>
<!-- Style Sheet -->
<link rel="stylesheet" type="text/css" href="../css/template3.style.css"></link>
<link rel="stylesheet" type="text/css" href="../css/pagination.css"></link>
<link rel="stylesheet" type="text/css" href="../css/popUp.css">
<script type="text/javascript" src="../js/popup.js"></script>
<script type="text/javascript" src="../js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/jquery-adminEditTemplate.js"></script>
<script type="text/javascript" src="../js/jquery.template3.js"></script>
<script type="text/javascript">

jQuery(document).ready(function(){
	var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
	var text = "";
	var custText = jQuery(".custSpanText").find(".customerSpanText");
	jQuery(custText).each(function(index){
		text = text+jQuery(this).text();
	});
	text = jQuery.trim(text);
	if(text.length == 0){
		jQuery("#customer").css("border","1px solid white");
		jQuery("#editDetailHeading_377").css("margin-top","2px");
		
	}
	else{
		jQuery("#customer").css("border","1px solid black");
	}
	if(is_chrome == true){
		jQuery("#customer").css("width", "783px");
		jQuery(".custSpanText").css("width", "846px");
	}
	
	jQuery("#editButton72").click(function(){
		jQuery("#cmtLine1").css("margin-left","75px");
		jQuery("#cmtLine2").css("margin-left","96px");
		jQuery("#cmtLine3").css("margin-left","37px");
	});
	jQuery("#okButton72").click(function(){
		jQuery(".cmtLine").css("margin-left","0px");		
	});

	jQuery(".focusOutSaveDetail").keypress(function(){		
		var data = jQuery(".focusOutSaveDetail").text();
		jQuery(".detailCmpt").val(data);
		jQuery(".focusOutSaveDetail").attr('contentEditable',true);
		var len = jQuery(".focusOutSaveDetail").text().length;
		if(len == 250){
			alert("Your Limitation exceed..");
			jQuery(".focusOutSaveDetail").attr('contentEditable',false);
		}
	});

	
	
});

function disableEnterKey(e)
{
     var key;     
     if(window.event)
          key = window.event.keyCode; //IE
     else
          key = e.which; //firefox     

     return (key != 13);
}
function updateForm(){
	if(jQuery("#editText103").val()=="" || 
			jQuery(".focusOutSaveTop").val()=="" ||
			jQuery(".focusOutSaveLeftHeading").val()=="" ||
			jQuery(".focusOutSaveRightHeading").val()=="" ||
			jQuery(".focusOutSaveBLeftHeading").val()=="" ||
			jQuery("#editComp_72").val()=="" ||
			jQuery("editComp_73").val()=="" ||
			jQuery("editComp_74").val()==""){
		alert("This field can not be blank.");
		return false;
	}
	else{
		jQuery("#updateForm3").submit();
		return true;
	}
}

</script>
<style>
.comments {
	text-align: justify;
	letter-spacing: .5px;
}

.grayBox {
	position: fixed;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity =   80);
	display: none;
}

.box_content {
	position: fixed;
	left: 25%;
	right: 30%;
	width: 575px;
	/*height: 30%;*/
	padding: 16px;
	z-index: 1002;
	overflow: auto;
	background: none repeat scroll 0 0 #FFFFFF;
	border: 8px solid #ACACAC;
}

#img {
	position: relative;
	float: none;
}

#img #text {
	left: 74px;
	position: absolute;
	top: 7px;
	width: 300px;
}

#comp_43,#comp_44,#comp_45,#comp_46,#editComp_43,#editComp_44,#editComp_45,#editComp_46
	{ /*text-transform: uppercase;*/
	
}

#topCmpt {
	font-family: 'MyriadProBold', Arial;
	font-size: 11pt;
}

#repositoryPopUp {
	height: auto !important;
	border: 8px solid #acacac !important;
	width: 625px !important;
	margin-top: -65px;
}

#item .odd,#item .even {
	border: 1px solid #ACACAC !important;
	margin-right: 5px;
	margin-bottom: 5px;
}

.selectLink {
	left: 0px !important;
	position: relative;
}
</style>
	<div class="container" style="padding: 0px;">
	<%@ include file="WEB-INF/header/userNavigation.jsp"%>
		<div id="centercontainer" class="centercontainer">
			<div class="nav">
				<%@ include file="WEB-INF/header/headerLink3.jsp"%>
			</div>
			<div style="clear: both"></div>
			<div class="innercontainer">
			<form action="./updateTemplate3.do" id="updateForm3" name="updateForm3" method="POST">
				<div id="templateSize">
					<div style="width: 21.5cm;" class="divLogoTab">
						<div class="divLogoRow">
							<div class="logo1">
								<div class="leftOptImage103">
								</div>
							</div>
							<div id="imgLogo" align="center" class="imgLogo">
								<c:choose>
									<c:when test="${template.templateImage eq null || template.templateImage eq ''}">
										<p id="msg">(Your logo here)<br/>Dimension:[380 x 100]pixels</p>
									</c:when>
									<c:otherwise>
										<img src="/ImageLibrary/${template.templateImage}"  style="max-width:380px; max-height:100px;">
									</c:otherwise>
								</c:choose>
							</div>
							<a href="javascript:void(0);" id="centerImageClick" onclick="centerImageSelectionPop('center',380,100); return false;" style="margin-left: 38%;height: 0; font-family: 'MyriadProRegular';font-size:11pt;">Click to upload logo</a>&nbsp;&nbsp;<span class="imgDimension">[380 x 100]</span>
							<div class="logo2-103">
								<div class="rghtOptImage103">
								</div>
							</div>
						</div>
					</div>
				<input type="hidden" id="imgType" name = "imgType" value="${imgType}">
				<input type = "hidden" name = "saveImage" id="saveImage" value = "${template.templateImage}"> 
				<div id="pageHeading" style="width:825px;">
					<!-- Header Starts -->
					<div id="heading-edit">
						<label id="headerText">${template.headerText}</label> 
						<input type="text" value="${template.headerText}" id="editText103" name = "headingText" style="display: none;" maxlength="30" />
					</div>
					<div id="headingImage">
						<a href="javascript:void(0);" id="editHeading103">
							<img src="../images/ieditover.PNG" width="16px" height="17px">
						</a> 
						<a href="javascript:void(0);" id="okHeading103" style='display:none;'>
							<img id="iok" src="../images/iok.png" width="16px" height="17px">
						</a>
					</div>
					<!-- Header Ends -->
					<div class="clear"></div>
				</div>
				<div class="divTable">
					<div class="divRow">
						<c:forEach items="${detailComponent}" var="detail" varStatus="status"> 
							<c:set var="compName" value="${detail.componentName}"></c:set>
							<c:set var="compId" value="${detail.componentId}"></c:set>
							<!--<div id="detailDiv">-->
							<div style="position:relative;">
							<div class="divCell" id="customer" style="border:1px solid black;font-size: 11pt;width: 765px !important;text-transform: capitalize;font-family: 'MyriadProRegular', Arial, Helvetica, sans-serif;">
								<c:forEach var="cname" items="${fn:split(detail.componentName, '~')}" varStatus="stat">
									<div class ="custSpanText" style="width: 763px;">
										<span id="cust${stat.count}" style="font-family: MyriadProRegular;font-size: 11pt;line-height: 1.9;" class="customerSpanText">${cname}</span>
									</div>
								</c:forEach>
								<c:forEach var="cname" items="${fn:split(detail.componentName, '~')}" varStatus="stat">
									<input type="text" class="detailTbox" value="${cname}" id="editComp_${compId}_${stat.count}" name="editComp_${compId}_${stat.count}" 
										onkeypress="return checkStringLength('editComp_${compId}_${stat.count}', event);" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" style="width:763px;" onblur="mouseOut103_${stat.count}()" />
								</c:forEach>
							</div>
							<input type="hidden" value="${compName}" id="Component_${compId}" name = "Component_${compId}" class="detailCmpt">
							
							<a href="javascript:void(0);" id="editDetailHeading_${compId}" class="editDetailHeading" style=" position:absolute; top:58px;margin-left:773px;">
								<img src="../images/ieditover.PNG" width="16px" height="17px" >
							</a>
							<a href="javascript:void(0);" id="okDetailHeading_${compId}" style='display:none; position:relative; left:-10px;' class="okDetailHeading">
								<img id="iok" src="../images/iok.png" width="16px" height="17px">
							</a>
						</div>
						</c:forEach>
						
					</div>
				</div>
				<div id="commentDiv">
					<span id="temp1"></span>
				</div>
				<div style="padding:15px 25px;margin-top:20px;">
					<span class="circleSuare">
					<%String getSVG = (String) request.getAttribute("imgType");
		 				String circle = "";
		 				String square = "";
						if (getSVG.equalsIgnoreCase("circle")) 
							circle= "checked";
						else
							square = "checked";
	   		 		%>
					<input type="radio" name="imgType" value="circle" <%= circle %> /> Circle
					</span>
					<span class="circleSuare">
						<input type="radio" name="imgType" value="square"  <%= square %>/> Square
					</span>
				</div>
				<div class="clear"></div>
				<div class="selTable33">
					<div class="selRow">
						<div class="selCol33">
							<c:set var="compId" value="${template.components[1].componentId}"></c:set>
							<c:set var="compName" value="${template.components[1].componentName}"></c:set>
							<div class="selCell" style="margin-right:1px;">
							<span><b class="green"></b></span>
							<span class="floatLeft">
								<p id="topText_${compId}" style="font-weight:normal !important;"><span id="topCmpt">${compName}</span></p>
								<input type="text" value="${compName}" id="Component_${compId}" class="focusOutSaveTop" name="Component_${compId}" style="width: 150px;" maxlength="15">
								<a href="javascript:void(0);" id="editTopHeading_${compId}" class="editTopHeading" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okTopHeading_${compId}" class="okTopHeading" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</span>
							</div>
						</div>
						<div class="selCol33">
							<c:set var="compId" value="${template.components[2].componentId}"></c:set>
							<c:set var="compName" value="${template.components[2].componentName}"></c:set>
							<div class="selCell" style="margin-right:1px;">
							<span><b class="yellow"></b></span>
							<span class="floatLeft">
								<p id="topText_${compId}" style="font-weight:normal !important;"><span id="topCmpt">${compName}</span></p>
								<input type="text" value="${compName}" id="Component_${compId}" class="focusOutSaveTop" name="Component_${compId}" style="width: 202px;" maxlength="22">
								<a href="javascript:void(0);" id="editTopHeading_${compId}" class="editTopHeading" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okTopHeading_${compId}" class="okTopHeading" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</span>
							</div>
						</div>
						<div class="selCol33">
							<c:set var="compId" value="${template.components[3].componentId}"></c:set>
							<c:set var="compName" value="${template.components[3].componentName}"></c:set>
							<div class="selCell" style="margin-right:1px;">
							<span><b class="red"></b></span>
							<span class="floatLeft">
								<p id="topText_${compId}" style="font-weight:normal !important;"><span id="topCmpt">${compName}</span></p>
								<input type="text" value="${compName}" id="Component_${compId}" class="focusOutSaveTop" name="Component_${compId}" style="width: 258px;" maxlength="30">
								<a href="javascript:void(0);" id="editTopHeading_${compId}" class="editTopHeading" style='float: right;'>
									<img src="../images/ieditover.PNG" width="16px" height="17px">
								</a> 
								<a href="javascript:void(0);" id="okTopHeading_${compId}" class="okTopHeading" style='display:none;float: right;'>
									<img id="iok" src="../images/iok.png" width="16px" height="17px">
								</a>
							</span>
							</div>
						</div>
					</div>
				</div>	
				<div class="clear"></div>
				<!-- All Hidden Field  -->
				<div class="divTable1 paddingLeft">
					<div class="inspectionleftwrap">
						<div class="inspection_bg">
							<div class="inspectionTable">
							<c:forEach items="${topLeftComponents}" var="topLeft" varStatus="status">
								<c:set var="compId" value="${topLeft.componentId}"></c:set> 
								<c:set var="compName" value="${topLeft.componentName}"></c:set>
								<c:set var="position" value="${topLeft.templateComponent.position.positionId}"></c:set>	
								<c:if test="${position eq 5}">
									<div class="clear row1 row1Title">
										<span align="center" class="th" id="topLeft_${compId}" style="text-transform: uppercase;">${compName}</span>
										<input type="text" value="${compName}" id="Component_${compId}"  class="focusOutSaveLeftHeading" name="Component_${compId}" style="display: none;width: 146px;text-transform: uppercase;" maxlength="25">
										<a href="javascript:void(0);" id="editTopLeftHeading_${compId}" class="editTopLeftHeading">
											<img src="../images/ieditover.PNG" width="16px" height="17px" style='float: right;'>
										</a> 
										<a href="javascript:void(0);" id="okTopLeftHeading_${compId}" class="okTopLeftHeading" style='display:none;float: right;'>
											<img id="iok" src="../images/iok.png" width="16px" height="17px">
										</a>
									</div>
								</c:if>
								<c:if test="${position eq 6}">
								<div class="clear row1">
									<span class="smallheading" id="topLeft_${compId}">${compName}</span>
									<input type="text" value="${compName}" id="Component_${compId}" name="Component_${compId}" class="focusOutSaveLeftHeading" style="display: none;width: 345px;height:31px;" maxlength="65">
									<a href="javascript:void(0);" id="editTopLeftHeading_${compId}" class="editTopLeftHeading">
										<img src="../images/ieditover.PNG" width="16px" height="17px" style='float: right;'>
									</a> 
									<a href="javascript:void(0);" id="okTopLeftHeading_${compId}" class="okTopLeftHeading" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
								</div>
								</c:if>
							</c:forEach>
								<div class="clear row1" id="img">
									<c:forEach items="${imageComponents}" var="image" varStatus="status">
										<c:if test="${image.templateComponent.position.positionId eq 70}">
										<div id="imgLeft" align="center" class="imgLeft" style="height:130px;">
											<img src="/ImageLibrary/${image.componentName}" alt="car" style="max-width:247px; max-height:116px;" id="image"/>
										</div>
										<input type="hidden" value="${image.componentName}" id="Component_${image.componentId}" name="Component_${image.componentId}">
										</c:if>
									</c:forEach>
									<div id="text" style="position:absolute; top:118px; width:375px;text-align:center;left:0px;">
										<a href="javascript:void(0);" id="leftImageClick" onclick="centerImageSelectionPop('left',247,116); return false;">Change Image</a>[250 x 120]
    								</div>
    							
								</div>
							<c:forEach items="${topLeftComponents}" var="topLeft" varStatus="status"> 
							<c:set var="compId" value="${topLeft.componentId}"></c:set> 
							<c:set var="compName" value="${topLeft.componentName}"></c:set>
							<c:set var="status" value="${topLeft.templateComponent.status.statusId}"></c:set>
							<c:set var="position" value="${topLeft.templateComponent.position.positionId}"></c:set>
							<c:if test="${position gt 6 and position le 15}">
								<div class="clear row1">
									<span><b class="green"></b><b class="yellow"></b><b class="red"></b></span> 
									<c:choose>
										<c:when test="${status eq 1}">
											<span class="inspectionTxt" id="topLeft_${compId}">${compName}</span>	
										</c:when>
										<c:otherwise>
											<span class="inspectionTxt" id="topLeft_${compId}"></span>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${position eq 14}">
											<input type="text" value="${compName}" class="focusOutSaveLeft" id="Component_${compId}" name="Component_${compId}" style="display: none;width: 230px;" maxlength="25">
										</c:when>
										<c:otherwise>
											<input type="text" value="${compName}" class="focusOutSaveLeft" id="Component_${compId}" name="Component_${compId}" style="display: none;width: 230px;" maxlength="30">
										</c:otherwise>
									</c:choose>
									<!--<c:if test="${status eq 1}">
										<a href="javascript:void(0);" id="deleteTopLeft_${compId}" class="deleteTopLeft">
											<img src="../images/iDelete.png" width="16px" height="17px">
										</a>
									</c:if>-->
									<a href="javascript:void(0);" id="editTopLeft_${compId}" class="editTopLeft">
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a>
									<a href="javascript:void(0);" id="okTopLeft_${compId}" class="okTopLeft" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
								</div>
							</c:if>
							</c:forEach>
							</div>
							<div class="inspectionTable">
								<c:forEach items="${bottomLeftComponents}" var="bottomLeft" varStatus="status"> 
									<c:set var="compId" value="${bottomLeft.componentId}"></c:set> 
									<c:set var="compName" value="${bottomLeft.componentName}"></c:set>
									<c:set var="status" value="${bottomLeft.templateComponent.status.statusId}"></c:set>
									<c:set var="position" value="${bottomLeft.templateComponent.position.positionId}"></c:set>
									<c:if test="${position eq 15}">
									<div class="clear row1 row1Title">
										<span align="center" class="th" id="bottomLeft_${compId}" style="text-transform: uppercase;">${compName}</span>
										<input type="text" value="${compName}" class="focusOutSaveBLeftHeading" id="Component_${compId}" name="Component_${compId}" style="display: none;width: 146px;text-transform: uppercase;" maxlength="25">
										<a href="javascript:void(0);" id="editBottomLeftHeading_${compId}" class="editBottomLeftHeading" style='float: right;'>
											<img src="../images/ieditover.PNG" width="16px" height="17px">
										</a> 
										<a href="javascript:void(0);" id="okBottomLeftHeading_${compId}" class="okBottomLeftHeading" style='display:none;float: right;'>
											<img id="iok" src="../images/iok.png" width="16px" height="17px">
										</a>
									</div>
									</c:if>
									<c:if test="${position gt 15 and position le 26}">
									<div class="clear row1">
										<span><b class="green"></b><b class="yellow"></b><b class="red"></b></span> 
										<span class="inspectionTxt" id="bottomLeft_${compId}">${compName}</span>
										<c:choose>
										<c:when test="${position eq 26}">
											<input type="text" value="${compName}" class="focusOutSaveBLeft" id="Component_${compId}" name="Component_${compId}" style="display: none;width: 230px;" maxlength="25">
										</c:when>
										<c:otherwise>
											<input type="text" value="${compName}" class="focusOutSaveBLeft" id="Component_${compId}" name="Component_${compId}" style="display: none;width: 230px;" maxlength="30">
										</c:otherwise>
										</c:choose>
										<!--<c:if test="${status eq 1}">
											<a href="javascript:void(0);" id="deleteBottomLeft_${compId}" class="deleteBottomLeft">
												<img src="../images/iDelete.png" width="16px" height="17px">
											</a>
										</c:if>-->
										<a href="javascript:void(0);" id="editBottomLeft_${compId}" class="editBottomLeft">
											<img src="../images/ieditover.PNG" width="16px" height="17px">
										</a> 
										<a href="javascript:void(0);" id="okBottomLeft_${compId}" class="okBottomLeft" style='display:none;float: right;'>
											<img id="iok" src="../images/iok.png" width="16px" height="17px">
										</a>
									</div>
									</c:if>
								</c:forEach>
								<div class="clear row1">
								<c:forEach items="${bottomLeftComponents}" var="bottomLeft" varStatus="status">
									<c:set var="compId" value="${bottomLeft.componentId}"></c:set> 
									<c:set var="compName" value="${bottomLeft.componentName}"></c:set>
									<c:set var="status" value="${bottomLeft.templateComponent.status.statusId}"></c:set>
									<c:set var="position" value="${bottomLeft.templateComponent.position.positionId}"></c:set>
									<c:if test="${position gt 26 and position le 30}">
									<div style="height:30px;">
										<span><b class="green"></b><b class="yellow"></b><b class="red"></b></span> 
										<span class="inspectionTxt" id="bottomLeft_${compId}">${compName}</span>
										<input type="text" value="${compName}" class="focusOutSaveBLeft" id="Component_${compId}" name="Component_${compId}" style="display: none;width: 230px;" maxlength="30">
										<!--<c:if test="${status eq 1}">
										<a href="javascript:void(0);" id="deleteBottomLeft_${compId}" class="deleteBottomLeft" style="float:right;">
											<img src="../images/iDelete.png" width="16px" height="17px">
										</a>
										</c:if>-->
										<a href="javascript:void(0);" id="editBottomLeft_${compId}" class="editBottomLeft" style="float:right;">
											<img src="../images/ieditover.PNG" width="16px" height="17px">
										</a> 
										<a href="javascript:void(0);" id="okBottomLeft_${compId}" class="okBottomLeft" style='display:none;float:right;'>
											<img id="iok" src="../images/iok.png" width="16px" height="17px">
										</a>
									</div>
									</c:if>
								</c:forEach>
								<div id="img">
									<c:forEach items="${imageComponents}" var="image" varStatus="status">
										<c:if test="${image.templateComponent.position.positionId eq 71}">
											<span id="imgBottomLeft" align="center" class="imgBottomLeft" style="float: right;margin-top: -24%;margin-right: 31px;">
												<img src="/ImageLibrary/${image.componentName}" alt="battery" style="max-width:90px; max-height:90px;" id="image"/>
											</span>
											<input type="hidden" value="${image.componentName}" id="Component_${image.componentId}" name="Component_${image.componentId}">
										</c:if>
									</c:forEach>
										<span id="imgBottomLeft" align="center" class="imgBottomLeft" style="margin-top: -25%;position:relative; left:250px;">
    										<a href="javascript:void(0);" id="bottomLeftImageClick" onclick="centerImageSelectionPop('bottemLeft103',90,90); return false;" style="">Change Image</a>[90 x 90]
										</span>
								</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="inspectionrightwrap">
						<div class="inspection_bg">
							<div class="inspectionTable">
								<c:forEach items="${topRightComponents}" var="topRight" varStatus="status">
									<c:set var="compId" value="${topRight.componentId}"></c:set> 
									<c:set var="compName" value="${topRight.componentName}"></c:set>
									<c:set var="status" value="${topRight.templateComponent.status.statusId}"></c:set>
									<c:set var="position" value="${topRight.templateComponent.position.positionId}"></c:set>
									<c:if test="${position eq 30}">
										<div class="clear row1 row1Title">
											<span align="center" class="th" id="topRight_${compId}" style="text-transform: uppercase;">${compName}</span>
											<input type="text" value="${compName}" class="focusOutSaveRightHeading" id="Component_${compId}" name="Component_${compId}" style="display: none;text-transform: uppercase;" maxlength="25">
											<a href="javascript:void(0);" id="editTopRightHeading_${compId}" class="editTopRightHeading"  style='float: right;'>
												<img src="../images/ieditover.PNG" width="16px" height="17px">
											</a> 
											<a href="javascript:void(0);" id="okTopRightHeading_${compId}" class="okTopRightHeading" style='display:none;float: right;'>
												<img id="iok" src="../images/iok.png" width="16px" height="17px">
											</a>
										</div>
									</c:if>
									<c:if test="${position gt 30 and position le 38}">
										<div class="clear row1">
											<span><b class="green"></b><b class="yellow"></b><b class="red"></b></span> 
											<span class="inspectionTxt" id="topRight_${compId}">${compName}</span>
											<c:choose>
											<c:when test="${position eq 38}">
												<input type="text" value="${compName}" class="focusOutSaveRight" id="Component_${compId}" name="Component_${compId}" style="display: none;width:230px;" maxlength="25">
											</c:when>
											<c:otherwise>
												<input type="text" value="${compName}" class="focusOutSaveRight" id="Component_${compId}" name="Component_${compId}" style="display: none;width:230px;" maxlength="30">
											</c:otherwise>
											</c:choose>
											
											<!--<c:if test="${status eq 1}">
											<a href="javascript:void(0);" id="deleteTopRight_${compId}" class="deleteTopRight">
												<img src="../images/iDelete.png" width="16px" height="17px">
											</a>
											</c:if>-->
											<a href="javascript:void(0);" id="editTopRight_${compId}" class="editTopRight">
												<img src="../images/ieditover.PNG" width="16px" height="17px">
											</a> 
											<a href="javascript:void(0);" id="okTopRight_${compId}" class="okTopRight" style='display:none;float: right;'>
												<img id="iok" src="../images/iok.png" width="16px" height="17px">
											</a>
										</div>
									</c:if>
								</c:forEach>
							</div>
      						<div class="inspectionTable" style="border-bottom:1px solid #000;">
        						<div class="clear row1 row1Title">
          							<span align="center" class="th" id="comp_38" style="text-transform: uppercase;">${template.components[38].componentName}</span>
          							<input type = "text" id = "editComp_38" name="editComp_38"   value = "${template.components[38].componentName}" style="display:none;text-transform: uppercase;" maxlength="25"/>
          							<a href="javascript:void(0);" id="editButton38" class="editTopRightHeading" onclick="editMultipleText('component38',31)" style='float: right;'>
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a> 
									<a href="javascript:void(0);" id="okButton38" class="okTopRightHeading" onclick="removeMultipleEdit('component38',25,31)" style='display:none;float: right;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
								</div>
        					<div class="clear row1" style="border-bottom:0px;">
          						<div style="padding:0px; border:0px;height:130px" id="topBox">
          							<div class="bordernone interior_inspec">
              							<div class="alignCenter clear paddingBottom" style="width:375px;">
                							<span id="comp_39"><h2 style="margin-left: 140px;">${template.components[39].componentName}</h2></span>
                							<input type="text" class="fontF" id="editComp_39" name="editComp_39" value = "${template.components[39].componentName}" style='display:none;float: none;width: 100px;' maxlength="25"/>
              							</div>
              							<div class="clear paddingBottom" style="width:375px; height:30px;">
                							<span style="width:135px; float:left;">
                								<b class="green"></b>
                								<span class="fontF" id="comp_40">${template.components[40].componentName}</span>
                								<input type="text" id="editComp_40" name="editComp_40" value='${template.components[40].componentName}' class="fontF" style="width:88px;display:none;" maxlength="17"/>
                							</span>
                							<span style="width:123px; float:left;">
                								<b class="yellow"></b>
                								<span class="fontF" id="comp_41">${template.components[41].componentName}</span>
                								<input type="text" id="editComp_41" name="editComp_41" value='${template.components[41].componentName}' class="fontF" style="width:78px;display:none;" maxlength="15"/>
                							</span>
                							<span style="width:109px; float:left;">
                								<b class="red"></b>
                								<span class="fontF" id="comp_42">${template.components[42].componentName}</span>
                								<input type="text" id="editComp_42" name="editComp_42" value='${template.components[42].componentName}' class="fontF" style="width:64px;display:none;" maxlength="14"/>
                							</span>
              							</div>
              							<div class="clear">
               	 							<div class="alignCenter" style="width:375px;">
				 							<div class="bordernone interior_inspec1 interior_inspecLeft1" id="box1" style="text-align:right;">
                      							<span class="txt_bold_lower_case" style="float:left;margin-right:4px; display:block; width:20px;text-align:right;">
                      								<strong id="comp_43" style="display:block;width:20px;">${template.components[43].componentName}</strong>
                      								<input type = "text" id="editComp_43" name="editComp_43" value = "${template.components[43].componentName}" class="txt_bold_lower_case" style="width:18px;display:none;" maxlength="2"/>
                      							</span>
                      							<span class="txt_bold_lower_case" id="comp_47" style="position:relative; left:-5px;float:right;display:block;width:67px">${template.components[47].componentName}</span>
                      							<input type = "text"  id="editComp_47" name="editComp_47" value = '${template.components[47].componentName}' class="txt_bold_lower_case" style="width:65px;display:none;position:relative; left:0px;float:right;" maxlength="8"/>
                      							<span style="display:block;float:right;" id="optionB47"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                  							</div>
                  							
                  							<div class="bordernone interior_inspec1 interior_inspecRight" id="box2" style="text-align:right;">
                      							<span class="txt_bold_lower_case"  style="float:left;margin-right:4px;display:block; width:20px;text-align:right;"> 
                      								<strong id="comp_44" style="display:block;width:20px;">${template.components[44].componentName}</strong>
                      								<input type="text" id="editComp_44" name="editComp_44" value = "${template.components[44].componentName}" class="txt_bold_lower_case" class="txt_bold_lower_case" style="width:18px;display:none;" maxlength="2"/>
                      							</span>
                      							<span class="txt_bold_lower_case" id="comp_48" style="position:relative; left:-5px;float:right;display:block;width:67px">${template.components[48].componentName}</span>
                      							<input type = "text" id="editComp_48" name="editComp_48" value = '${template.components[48].componentName}' class="txt_bold_lower_case" style="width:65px;display:none;position:relative; left:0px;float:right;" maxlength="8"/>
                      							<span style="display:block;float:right;" id="optionB48"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                  							</div>
                  							
                  							<div class="bordernone interior_inspec1 interior_inspecLeft1" id="box3" style="text-align:right;">
                      							<span class="txt_bold_lower_case" style="float:left;margin-right:4px;display:block; width:20px;text-align:right;">
                      								<strong id="comp_45" style="display:block;width:20px;">${template.components[45].componentName}</strong>
                      								<input type="text" id="editComp_45" name="editComp_45" value="${template.components[45].componentName}" class="txt_bold_lower_case" style="width:18px;display:none;" maxlength="2"/>
                      							</span>
                      							
                      							<span class="txt_bold_lower_case" id="comp_49"  style="position:relative; left:-5px;float:right;display:block;width:67px">${template.components[49].componentName}</span>
                      							<input type = "text" id="editComp_49" name="editComp_49" value = '${template.components[49].componentName}' class="txt_bold_lower_case" style="width:65px;display:none;position:relative; left:0px;float:right;" maxlength="8"/>
                      							<span style="display:block;float:right;" id="optionB49"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                 		 					</div>
                 		 					
                  							<div class="bordernone interior_inspec1 interior_inspecRight" id="box4" style="text-align:right;">
                          						<span class="txt_bold_lower_case"  style="float:left;margin-right:4px;display:block; width:20px;text-align:right;"> 
                          							<strong id="comp_46" style="display:block;width:20px;">${template.components[46].componentName}</strong>
                          							<input type="text" id="editComp_46" name="editComp_46" value = '${template.components[46].componentName}' class="txt_bold_lower_case" style="width:18px;display:none;" maxlength="2"/>
                          						</span>
                      							
                      							<span class="txt_bold_lower_case" id="comp_50" style="position:relative; left:-5px;float:right;display:block;width:67px">${template.components[50].componentName}</span>
                      							<input type = "text"  id="editComp_50" name="editComp_50" value = '${template.components[50].componentName}' class="txt_bold_lower_case" style="width:65px;display:none;position:relative; left:0px;float:right;" maxlength="8"/>
                      							<span style="display:block;float:right;" id="optionB50"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                  							</div>
				 						</div>
              						</div>
            					</div>
            				</div>
        				</div>
              			<div style="background-color:#d4daee; height:229px;">
			  				<div style="float:left; width:200px;">
                				<span style="padding:0px;width:100px;float:left;">
									<c:forEach items="${imageComponents}" var="image" varStatus="status">
										<c:if test="${image.templateComponent.position.positionId eq 72}">
											<div id="imgBottomRight" align="center" class="imgBottomRight" style="float: right;margin-top: -6%;width: 105px;height:200px;">
												<img src="/ImageLibrary/${image.componentName}" alt="rght_tire" style="max-width:100px; max-height:200px;" id="image"/>
											</div>
											<input type="hidden" value="${image.componentName}" id="Component_${image.componentId}" name="Component_${image.componentId}">
										</c:if>
									</c:forEach>
									<a href="javascript:void(0);" id="bottomRightImageClick" onclick="centerImageSelectionPop('bottemRight',100,200); return false;" style="display:inline;margin-top:10px;">Change Image</a><br/>[100 x 200]
								</span>
               	 				<div class="bordernone interior_inspec padding_reset lessWidth" style="padding:0px;float:right; padding-top:15px;width:100px !important;">
                    				<div style="height:30px;margin-bottom:10px;width: 99px;">
                      					<span  id="comp_51" colspan="2" style="padding:0px;word-wrap:break-word;"><h2 style="text-align:center;width:99px;">${template.components[51].componentName}</h2></span>
                      					<input type = "text" id="editComp_51" name="editComp_51" value = "${template.components[51].componentName}" style="width:90px;display:none;" maxlength="20"/>
                    				</div>
                    				<div class="clear" id="c1" style="margin-top: 20px;text-align:right;width:100px; margin-bottom:10px;height:20px;">
                    				    <span style="width:75px; display:block;float:right;"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                      					<span class="txt_bold txtLeft"> 
                      						<span id="comp_52">${template.components[52].componentName}</span>
                      						<input type="text" id="editComp_52"  name="editComp_52" value = "${template.components[52].componentName}"  class="txt_bold_lower_case" style="width:18px;display:none;" maxlength="2"/>
                      					</span>
                      					
                    				</div>
                    			
                    				<div class="clear" id="c2" style="text-align:right;width:100px; margin-bottom:10px;height:20px;">
                    				    <span style="width:75px; display:block;float:right;"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                      					<span class="txt_bold txtLeft">
                      						<span id="comp_53">${template.components[53].componentName}</span>
                      						<input type="text" id="editComp_53" name="editComp_53" value = "${template.components[53].componentName}"  class="txt_bold_lower_case" style="width:18px;display:none;" maxlength="2"/>
                      					</span>
                      					
                    				</div>
                    			
                    				<div class="clear" id="c3" style="text-align:right;width:100px; margin-bottom:10px;height:20px;">
                      					<span style="width:75px; display:block;float:right;"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                      					<span class="txt_bold txtLeft">
                      						<span id="comp_54">${template.components[54].componentName}</span>
                      						<input type="text" id="editComp_54" class="txt_bold_lower_case" name="editComp_54" value="${template.components[54].componentName}" style="width:18px;display:none;" maxlength="2"/>
                      					</span>
                      					
                    				</div>
                    				<div class="clear" id="c4" style="text-align:right;width:100px; margin-bottom:10px;height:20px;">
                      					<span style="width:75px; display:block;float:right;"><b class="smallGreen"></b><b class="smallYellow"></b><b class="smallRed"></b></span>
                      					<span class="txt_bold txtLeft">
                      						<span id="comp_55">${template.components[55].componentName}</span>
                      						<input type="text" id="editComp_55" class="txt_bold_lower_case" name="editComp_55" value="${template.components[55].componentName}" style="width:18px;display:none;" maxlength="2"/>
                      					</span>
                      					
                    				</div>
                				</div>
							</div>
				  	 		<div class="interior_inspec" style="border-right: 2px solid #0e62af !important;border-left: 2px solid #0e62af !important;padding:0px;width:96px; padding:0 3px; float:left;height:230px;">
                        		<div class="bordernone padding_reset" style="">
                            		<div style="text-align:center;margin-top:15px;">
                              			<h2 class="titleFont" id="comp_56" style="text-align:center;">
                              				${template.components[56].componentName}
                              			</h2>
                              			<input type="text" id="editComp_56" name="editComp_56" value="${template.components[56].componentName}" style="width:80px;display:none;" maxlength="18"/>
                            		</div>
                            		<div id="redBlock">
                              			<span style="float:left; width:16px;margin-right:2px;">
                              				<b class="smallRed"></b>
                              			</span>
                              			<span class="fontTxt" style="display:block; float:left;width:75px;" id="comp_57">
                              				${template.components[57].componentName}
                              			</span>
                              			<span class="fontTxt" style="display:block; float:left;width:75px;">
                              				<input type="text" id="editComp_57" name="editComp_57" value = "${template.components[57].componentName}" style="width:80px;display:none;" maxlength="20"/>
                              			</span>
                            		</div>
                             		<div class="bordernone interior_inspec">
                                  		<div class="beforeAfter">
                                    		<span id="comp_58" style="position:relative; left:7px;">
                                    			${template.components[58].componentName}
                                    		</span>
                                    		<input type="text" id="editComp_58" name="editComp_58" value = "${template.components[58].componentName}" style="width:60px;display:none;" maxlength="6"/>
                                    		<span id="comp_59" style="position:relative; left:13px;">
                                    			${template.components[59].componentName}
                                    		</span>
                                    		<input type="text" id="editComp_59" name="editComp_59" value = "${template.components[59].componentName}" style="width:75px;display:none;" maxlength="8"/>
                                  		</div>
                                  		<div style="width:100%;"  class="clear">
								  			<div  style="float:left;width:20px;">
                                 				<span style="width:20px;display:block; float:left; height:27px;">
                                 					<span class="txt_bold" id="comp_60">${template.components[60].componentName}</span>
                                 					<input type="text" id="editComp_60" name="editComp_60" value = "${template.components[60].componentName}" class="txt_bold_lower_case1" style="width:18px;display:none;" maxlength="2"/>
                                 				</span>
								 				<span style="width:20px;display:block; float:left; height:27px;">
								 					<span class="txt_bold" id="comp_61">${template.components[61].componentName}</span>
								 					<input type="text" id="editComp_61" name="editComp_61" value = "${template.components[61].componentName}" class="txt_bold_lower_case1" style="width:18px;display:none;" maxlength="2"/>
								 				</span>
								 			</div>
                                 			<div style="width:30px; float:left;margin-right:3px;">
                                 				<span class="white_box">&nbsp;</span><br />
								  				<span class="white_box">&nbsp;</span>
								  			</div>
                                 			<span class="white_box_rectangle">&nbsp;</span>
                                  		</div>
								  		<div style="width:100%;"  class="clear">
								  			<div  style="float:left;width:20px;">
                                 				<span style="width:20px;display:block; float:left; height:27px;">
                                 					<strong>
                                 						<span class="txt_bold" id="comp_62">${template.components[62].componentName}</span>
                                 						<input type="text" id="editComp_62" name="editComp_62" value = "${template.components[62].componentName}" class="txt_bold_lower_case1" style="width:18px;display:none;" maxlength="2"/>
                                 					</strong>
                                 				</span>
								 				<span style="width:20px;display:block; float:left; height:27px;">
								 				
								 						<span class="txt_bold" id="comp_63">${template.components[63].componentName}</span>
								 						<input type="text" id="editComp_63" name="editComp_63" value = "${template.components[63].componentName}" class="txt_bold_lower_case1" style="width:18px;display:none;" maxlength="2"/>
								 				
								 				</span>
								 			</div>
                                 			<div style="width:30px; float:left;margin-right:3px;">
                                 				<span class="white_box">&nbsp;</span><br />
								  				<span class="white_box">&nbsp;</span>
								  			</div>&nbsp;
                                 			<span class="white_box_rectangle">&nbsp;</span>
                                  		</div>
                                	</div>
                          		</div>
                  			</div>
								<div class="interior_inspec" style="padding:0px;width:74px;float:left; padding:0 3px;"class="bordernone padding_reset">
                            		<div style="margin-top:15px;">
                              			<span><h2 class="titleFont" style="text-align:center;" id="comp_64">${template.components[64].componentName}</h2></span>
                              			<input type="text" id="editComp_64" name="editComp_64" value = "${template.components[64].componentName}" style="width:88px;display:none;" maxlength="35"/>
                            		</div>
                              		<div  class="bordernone interior_inspec"  style="width:80px;">
                                  		<div class="clear" id="b1" style="height:20px; margin-bottom:5px;">
                                    		<span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                    		<span class="txtFont" id="comp_65" style="vertical-align:-3px;">${template.components[65].componentName}</span>
                                    		<span class="txtFont" style="vertical-align:-3px;"><input type="text" id="editComp_65" name="editComp_65" value = "${template.components[65].componentName}" style="width:45px;display:none;" maxlength="9"/></span>
                                  		</div>
                                  		<div class="clear" id="b2" style="height:20px; margin-bottom:5px;">
                                   		 	<span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                    		<span class="txtFont" id="comp_66" style="vertical-align:-3px;">${template.components[66].componentName}</span>
                                    		<span class="txtFont" style="vertical-align:-3px;"><input type="text" id="editComp_66" name="editComp_66" value = "${template.components[66].componentName}" style="width:45px;display:none;" maxlength="9"/></span>
                                  		</div>
                                  		<div class="clear" id="b3" style="height:20px; margin-bottom:5px;">
                                    		<span><span class="white_box_square" style="float:left;margin-right:5px;"></span> </span>
                                    		<span style="vertical-align:-3px;" class="txtFont" id="comp_67">${template.components[67].componentName}</span>
                                    		<span style="vertical-align:-3px;" class="txtFont"><input type="text" id="editComp_67" name="editComp_67" value = "${template.components[67].componentName}" style="width:45px;display:none;" maxlength="9"/></span>
                                  		</div>
                                   		<div class="clear" id="b4" style="height:20px; margin-bottom:5px;">
                                    		<span class="white_box_square" style="float:left;margin-right:5px;"></span>
                                    		<span style="vertical-align:-3px;" class="txtFont" id="comp_68">${template.components[68].componentName}</span>
                                    		<span style="vertical-align:-3px;" class="txtFont"><input type="text" id="editComp_68" name="editComp_68" value = "${template.components[68].componentName}" style="width:45px;display:none;" maxlength="9"/></span>
                                  		</div>
                                	</div>
                          		</div>
              				</div>
      					</div>
      						<div style="height:1px;">
									<a href="javascript:void(0);" id="editButton72" class="editTopRightHeading" onclick="editMultipleText('component72',3)" style='float: right;'>
										<img src="../images/ieditover.PNG" width="16px" height="17px">
									</a> 
									<a href="javascript:void(0);" id="okButton72" class="okTopRightHeading" onclick="removeMultipleEdit('component72',25,3)" style='display:none;float: right;margin-right: -15px;'>
										<img id="iok" src="../images/iok.png" width="16px" height="17px">
									</a>
								</div>
      						<div class="bottomtext" style="width: 400px;overflow:hidden;">
								<span style="float: left;" id="comp_72" class="comments">${template.components[72].componentName}</span> 
								<span style="float: left;display:inline;">
									<input type="text"  class="comments" id="editComp_72" name="editComp_72" value = "${template.components[72].componentName}" style="width:80px;display:none;float: left;font-family: 'MyriadProRegular';font-size: 11pt;border: 1px solid black;" maxlength="30"/>
								</span>
								<span style="display:block;overflow:hidden;"><hr class="cmtLine" id="cmtLine1" style="width: 400px;"/></span>
								<br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" /><br /><hr style="width: 400px;" />
								<div style="width:400px;margin-top:10px;">
									<div style="float:left;width:279px;overflow:hidden;">
										<span style="float: left;" id="comp_73" class="comments">${template.components[73].componentName}</span> 
										<span style="float: left;">
											<input type="text"  class="comments" id="editComp_73" name="editComp_73" value = "${template.components[73].componentName}" style="width:90px;display:none;float: left;font-family: 'MyriadProRegular';font-size: 11pt;border: 1px solid black;" maxlength="15"/>
										</span>
										<span style="display:block;overflow:hidden;"><hr class="cmtLine" id="cmtLine2" style="width: 276px;"/></span>
									</div>
									<div style="float:right;width: 120px;">
										<span style="float: left;" id="comp_74" class="comments">${template.components[74].componentName}</span> 
										<span style="float: left;">
											<input type="text"  class="comments" id="editComp_74" name="editComp_74" value = "${template.components[74].componentName}" style="width:35px;display:none;float: left;font-family: 'MyriadProRegular';font-size: 11pt;border: 1px solid black;" maxlength="10"/>
										</span>
										<span style="display:block;overflow:hidden;"><hr class="cmtLine" id="cmtLine3" style="width: 125px"/></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="containerbtm3">
					<input type="button" value = "Save" class="cancelbtn" id="saveTemplate3" onclick="updateForm();launchWindow('#dialog');" style="position:relative;left:10px"> 
				</div>
				</div>
				</form>
				<!--<a href="./navigateTemplate103.do" style="text-decoration: none;margin-top: -46px;">
					<input type="button" value = "Cancel" class="cancelbtn" onclick="launchWindow('#dialog');" style="position:relative;left:-25px;"> 
				</a>-->
				<form  style="margin-top: -46px;" action="./navigateTemplate103.do" method="get">
					<input type="submit" value="Cancel" id="cancelBtn" style="position:relative;left:-25px;" />
				</form>
		</div>
	</div>
	<div id="boxes"><div id="dialog" class="window"><img src="../images/ajaxloader.gif"/></div></div><div id="mask"></div>
