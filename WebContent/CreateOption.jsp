<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<div id="createCompOption" >
	<form id="createCompOptionForm" action="./adminAddCreateOption.do" method="post" >
		<div id="options-table">
		<input type="hidden" name="createCompOptId" id="createCompOptId" value="" />
		<input type="hidden" name="optionDescription" id="optionDescription" value="" />
		<input type="hidden" name="lastoptionDiv" id="lastoptionDiv" value="" />
			<div class="sideheading">
				<label>Option Type</label> <br /> 
				<select id="optionType" name="optionType">
					<option id="1" selected="selected">Square</option>
					<option id="2">Circle</option>
					<option id="3">None</option>
				</select>
			</div>
			<div class="sideheading" id="optionComp">
				<label>Option Name</label> <br /> <input type="text" value="${optionSelect}"
					id="optionName" name="optionName" maxlength="20" />
			</div>
			<div  class="sideheading" id="compOptionDesc">
				<div class="checkboxcntDiv" id = "checkboxcnt_1">
					<img src="../images/square.png" class="imageOptionType" id="image_1">
					<input type="text" value="" id="optionDesc_1" name="optionDesc_1" size="9" class="compOptDesc" style="width: 127px;"/>
					<span><input type="button" name="add_1" value="" id="add_1" class="add" />
					<input type="button" name="remove_1" value="" id="remove_1" class="delete"/></span>
				</div>
			</div>
			<div class="sideheading" style="text-align: center;">
				<input class="btn_forright" type="button" value="Ok" id="createOptOk" /> 
				<input class="btn_forright" type="button" value="Cancel" id="createOptCancel" />
			</div>
		</div>
	</form>
</div>	