<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="header.jsp"%>
<script type="text/javascript" src="../js/jquery-latest.js"></script>

<div>
	<div class="sideheading">
		<label>Subheading</label>
		<br />
	  	<input type="text" value="${selectedComp.subComponents.label}" id="editLabel" onkeydown="editLabel(this.id)" maxlength="24" />
	</div>
	<div class="sideheading">
		<label>Option</label>
		<br />
		<div id="editOpdtionDiv">
		  	<select id="editOption" onchange = "changeOPtion(this.id)">
		  	<c:forEach var="options" items="${selectedComp.subComponents.subComponentsOptions}">
		  		<c:if test="${options.status == 'true'}">
		  			<option value="${options.subComponentOptionsId}" selected="selected">${options.options.optionName }</option>
		  		</c:if>
		  		<c:if test="${options.status != 'true'}">
		  			<option value="${options.subComponentOptionsId }">${options.options.optionName }</option>
		  		</c:if>
		  	</c:forEach>
		  	<c:if test="${fn:length(selectedComp.subComponents.subComponentsOptions) == 0}">
		  	<c:if test="${user.userRole == 'admin'}">
		  		<option value="Select">Select</option>
	  		</c:if>
		  	</c:if>
		  	<c:if test="${fn:length(selectedComp.subComponents.subComponentsOptions) < 3}">
		  	<c:if test="${user.userRole == 'admin'}">
		  		<option value="Create Option">Create Option</option>
	  		</c:if>
		  	</c:if>
		  	</select>
	  	</div>
	</div>
	<input type="hidden" name="selectOptType" id="selectOptType" value="${selectOptType}" style="text-transform: capitalize;" />
	<div class="sideheading" id="tickType">
		<select id="editOptionType" name = "editOptionType">
			<option id="1" value="Square" selected="selected">Square</option>
			<option id="2" value="Circle" >Circle</option>
			<option id="3" value="None" >None</option>
		</select>
	</div>
	<div class="sideheading" id="editDesc">
		 <div id="optionList">
	<c:if test="${fn:length(selectedComp.subComponents.subComponentsOptions) > 0}">
			 <c:forEach var="desc" items="${fn:split(selectDesc, '^')}" varStatus="status">
				 <div class="checkbox" id="checkboxcnt_${status.count}">
				 <c:if test="${fn:length(fn:split(selectDesc, '^')) != status.count}">
				 	<c:set var="qt1" value= "\"" scope="page" />   
					<c:set var="qt2" value= "&#34;" scope="page" />
				 	<span class="checkbox" style="display:block;">
				 	<img src="../images/${fn:toLowerCase(selectOptType)}.png" class="imageOptionType" id="editImage${status.count}">
				 	<input type="text" size="9" class="optiontext" id="optiontext_${status.count}" value="${fn:replace(desc, qt1, qt2)}" style="width: 100px;" onkeyup="editOtions(this.id)"/>
				 	<input type="button" name="remove_${status.count}" value="" id="remove_${status.count}" class="deleteEditOption"/>
				 	<input type="button" name="add_${status.count}" value="" id="add_${status.count}" class="addEditOption" style="display: none;" />
				 	</span>
			 	</c:if>
			 	<c:if test="${fn:length(fn:split(selectDesc, '^')) == status.count}">
			 		<c:set var="qt1" value= "\"" scope="page" />   
					<c:set var="qt2" value= "&#34;" scope="page" />
				 	<span class="checkbox" style="display:block;">
				 	<img src="../images/${fn:toLowerCase(selectOptType)}.png" class="imageOptionType" id="editImage${status.count}">
				 	<input type="text" size="9" class="optiontext" id="optiontext_${status.count}" value="${fn:replace(desc, qt1, qt2)}" style="width: 100px;" onkeyup="editOtions(this.id)"/>
				 	<input type="button" name="remove_${status.count}" value="" id="remove_${status.count}" class="deleteEditOption"/>
				 	<input type="button" name="add_${status.count}" value="" id="add_${status.count}" class="addEditOption" /></span>
			 	</c:if>
				 </div>
			 </c:forEach>	
	</c:if>
	 </div>
	</div>
</div>

