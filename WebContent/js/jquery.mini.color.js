jQuery(document).ready( function() {
	/*---------------------------- Heading Color ---------------------------*/
	jQuery(".colors").miniColors({
		   change: function(hex, rgb) {
		    jQuery(".text").css('color',hex);
		    jQuery("#currHeaderColor").val(hex);
		   }
	});
});