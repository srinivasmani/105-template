/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;

//loading popup with jQuery magic!


function launchWindow(id) {
	var divTobeCleared=document.getElementById('successMessage');
	var edited = jQuery("#edited").val();
	if(divTobeCleared!=null){
		divTobeCleared.innerHTML="";
	}
	if(edited != "true"){
		var maskHeight = jQuery(document).height();
		var maskWidth = jQuery(window).width();
		jQuery('#mask').css({'width':maskWidth,'height':maskHeight});
		//transition effect  
		jQuery('#mask').fadeIn(0); 
		jQuery('#mask').fadeTo("slow",0.4); 
	 
		//Get the window height and width
		var winH = jQuery(window).height();
		var winW = jQuery(window).width();
	              
		//Set the popup window to center
		jQuery(id).css('left', winW/2);
		//transition effect
		jQuery(id).fadeIn(2000);
	}
}