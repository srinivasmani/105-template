
jQuery(document).ready(function() {
	
	/* Start of the events for create component*/
		/*when user clicks on create component*/
		jQuery("#createComponent").click(function(){
			if(jQuery("#edited").val() == "true"){
				//alert("Please save or cancel the changes on component.");
				alert("Please click Ok or Cancel.");
			}else{
			jQuery("#editComponent").css("display", "none");
			jQuery.ajax({
				url :"./adminCreateComponent.do",
				type :"GET",
				dataType : "html",
				success : function(data) {
					jQuery("#addCompRightContainer").empty();
					jQuery("#addCompRightContainer").html(data);
					if((jQuery("#optionList").length != 0)){
					jQuery("#optionList").empty();
					}
				},
				complete: function(xData, status){
					jQuery("#addCompDetails").css("display","block");
					jQuery("#createCompOptionDetails").css("display", "none");
					jQuery("#addComponent").css("display", "block");
				},
				error : function(xmlhttp, error_msg) {
				}
				});
			}
			});
		
	    /* when user select an option to create an option.*/
	    jQuery("#createOption").live("change", function(){
	    	if(jQuery("#createOption").val() != "Select"){
	    		jQuery("#createOptionDetails").css("display", "none");
	    	jQuery.ajax({
				url :"./adminAddComponentOptions.do?opt="+jQuery("#createOption").val(),
				type :"GET",
				dataType : "html",
				success : function(data) {
					jQuery("#editComponent").css("display", "none");
					jQuery("#createOptionDetails").empty();
					jQuery("#createOptionDetails").html(data);
					jQuery("#createOptionDetails").css("display", "block");
				},
				complete: function(xData, status){
				},
				error : function(xmlhttp, error_msg) {
				}
				});
	    	}
	    	else{
	    		jQuery("#options-table").empty();
	    	}
	        });
	    /* when user changes the option type. */
	    jQuery("#optionType").live("change", function(){
	    	var imageOptionType = jQuery("#optionType").val();
	    	var chckboxCntDiv = $("#compOptionDesc").find(".checkboxcntDiv");
	    	var image = "";
	    	if(imageOptionType.toLowerCase() == "square"){
	    		image = "../images/square.png";
	    	}
	    	else if(imageOptionType.toLowerCase() == "circle"){
	    		image = "../images/circle.png";
	    	}
	    	else{
	    		image = "../images/none.png";
	    	}
	    	jQuery(chckboxCntDiv).each(function(index){
	    		var imageOptionType = jQuery(this).find(".imageOptionType");
	    		jQuery(imageOptionType).each(function(index){
	    			var imageId = jQuery(this).attr("id");
		    		jQuery("#"+imageId).attr("src", image);	    		
		    		});
	    	});
	    });
		/* when user clicks on add button to add one more option. */
		jQuery(".add").live("click",function(){
			var imageOptionType = jQuery("#optionType").val();
			var addId = $(this).attr("id");
			var cuurOption = addId.split("_");
			var cuurOptionnum = parseInt(cuurOption[1]);
			var newOptionNum = cuurOptionnum+1;
			var optDesc = jQuery("#optionDescription").val();
			var lastOptDiv = "add_"+newOptionNum;
			var curOptDesc = "optionDesc_"+cuurOption[1];
			jQuery("#"+addId).removeClass("del ");
			jQuery("#"+addId).addClass("add");
			jQuery("#"+addId).val("");
			jQuery("#"+addId).css("display", "block");
			jQuery("#"+addId).css("width", "16px");
			jQuery("#"+addId).css("float", "left");
			jQuery("#"+addId).css("margin-right", "3px");
			jQuery(".compOptDesc").css("float", "left");
			jQuery(".compOptDesc").css("margin-right", "0px");
			jQuery("#optionDesc_1").css("margin-right", "5px");
			jQuery(".compOptDesc").css("margin-left", "-7px");
			jQuery(".delete").css("margin-left", "-3px");
		
			jQuery(".checkboxcntDiv > img").css("width", "14px");
			jQuery(".checkboxcntDiv > img").css("float", "left");
			jQuery(".checkboxcntDiv > img").css("margin-right", "10px");
			jQuery(".checkboxcntDiv").css("overflow", "auto");
			var image = "";
	    	if(imageOptionType.toLowerCase() == "square"){
	    		image = "../images/square.png";
	    	}
	    	else if(imageOptionType.toLowerCase() == "circle"){
	    		image = "../images/circle.png";
	    	}
	    	else{
	    		image = "../images/none.png";
	    	}
			if((jQuery("#"+curOptDesc).val()).length == 0){
				alert("Please enter options.");
				}
			else{
			getOptionDesc();
			var totalLen = (jQuery("#optionDescription").val()).length; 
			if(totalLen >24){
				alert("Cannot add more.");
				}
			else{
			for(var i=1; i<=cuurOptionnum; i++){
				var addbutt = "add_"+i;
				if(jQuery("#"+addbutt).length != 0 ){
					jQuery("#"+addbutt).removeClass("add");
					jQuery("#"+addbutt).css("display", "none");
					}
				}
			jQuery("#compOptionDesc").append('<div class="checkboxcntDiv" id = "checkboxcnt_'+newOptionNum+'"><img src="'+image+'" class="imageOptionType" id="image_'+newOptionNum+'">&nbsp;<input type="text" value="" id="optionDesc_'+newOptionNum+'" name="optionDesc_'+newOptionNum+'" size="9" class="compOptDesc" style="width:127px;" />&nbsp;<span style="display:inline;"><input type="button" name="add_'+newOptionNum+'" value="" id="add_'+newOptionNum+'" class="add" /><input type="button" name="remove_'+newOptionNum+'" value="" id="remove_'+newOptionNum+'" class="delete"/></span></div>');
			jQuery("#lastoptionDiv").val(lastOptDiv);
			}
			}
	  });
		jQuery(".delete").live("click",function(){
			var delId = $(this).attr("id");
			var cuurOption = delId.split("_");
			var cuurOptionnum = parseInt(cuurOption[1]);
			var removeDiv = "checkboxcnt_"+cuurOptionnum;
			var lastoptDiv = jQuery("#lastoptionDiv").val();
			var lastDiVNum = lastoptDiv.split("_");
			if(cuurOptionnum == parseInt(lastDiVNum[1])){
				var adddiv = getLastDiv(lastDiVNum[1]);
				jQuery("#"+adddiv).removeClass("del ");
				jQuery("#"+adddiv).val("");
				jQuery("#"+adddiv).addClass("add");
				jQuery("#"+adddiv).css("display", "block");
				jQuery("#"+adddiv).css("width", "16px");
				jQuery("#"+adddiv).css("float", "left");
				
				jQuery("#lastoptionDiv").val(adddiv);
				if(jQuery("#"+adddiv).css("display", "block")){
					jQuery("#"+adddiv).css("margin-left", "3px");
					jQuery("#add_1").css("margin-right", "7px");
				     }
				}
			jQuery("#"+removeDiv).remove();
			getOptionDesc();
			});
		
		jQuery(".compOptDesc").live("keyup", function(e){
			getOptionDesc();
			var maxLength = 32;
			var optionDescription = jQuery("#optionDescription").val();
			var currId = $(this).attr("id");
			var optionValue = (jQuery("#optionDescription").val()).split("^");
	        var totalLength =  ((jQuery("#optionDescription").val()).length)+(optionValue.length * 2);
				if(totalLength > maxLength){
					if((e.keyCode != 8) && (e.keyCode != 13) && (e.keyCode != 46)){
						alert("Maximum size is "+(maxLength));
					}
					}
			});
		
	/* Start of Final save or cacel for create component */
		jQuery("#rightOkComp").live("click",function(){
			if((jQuery("#heading").val()).length > 0){
			jQuery("#options-table").empty();
			jQuery("#options-table").css("display", "none");
			jQuery("#addComp").ajaxForm({ target:'#CompDrp'}).submit();
			jQuery("#addComponent").hide();
			}
			else{
				alert("Please enter heading.");
				}
			});
		jQuery("#rightCancelComp").live("click",function(){
			jQuery().empty("#createOptionDetails").empty();
			jQuery("#options-table").empty();
			jQuery("#options-table").css("display", "none");
			jQuery("#addComponent").hide();
		});
		/* End of Final save or cancel for create component */
		/* Start of Create options for the new component */
		jQuery("#rightOkCompOption").live("click", function(){
			var createOpt = jQuery("#createOption").val();
			var optionType = jQuery("#optionType").val();
			var optDescId = "optionDescription"+createOpt;
			var optName = "optionName"+createOpt;
			var optTye = "optiontype"+createOpt;
			jQuery("#"+optName).val(jQuery("#optionName").val());
			jQuery("#"+optTye).val(jQuery("#optionType").val());
			jQuery("#"+optDescId).val(jQuery("#optionDescription").val());	
			jQuery("#createOptionDetails").empty();
		});
		
		jQuery("#rightCancelCompOption").live("click", function(){
			var createOpt = jQuery("#createOption").val();
			var optionType = jQuery("#optionType").val();
			var optDescId = "optionDescription"+createOpt;
			var optName = "optionName"+createOpt;
			var optTye = "optionType"+createOpt;
			jQuery("options-table").empty();
			jQuery("#"+optName).val("");
			jQuery("#"+optDescId).val("");	
			jQuery("#"+optTye).val("");
			jQuery("#optionDescription").val("");
			jQuery("#createOptionDetails").empty();
		});
		
		jQuery("#createOptOk").live("click", function(){
			if(jQuery("#optionName").val() == ""){
				alert("please enter option name.");
			}
			else if(jQuery("#optionDescription").length != 0){
				var totalLen = (jQuery("#optionDescription").val()).length;
				if(totalLen <= 0){
					alert("Please enter options.");
				}
				else{
					jQuery("#optionList").empty();
					jQuery("#createCompOptId").val(jQuery("#CompDrp").val());
					jQuery("#editOpdtionDiv").empty();
					jQuery("#createCompOptionForm").ajaxForm({target: '#editOpdtionDiv'}).submit();
					jQuery("#createCompOptionDetails").css("display", "none");
					jQuery("#editOptionType").empty();
				}
			}
		  });
		jQuery("#createOptCancel").live("click", function(){
			if(jQuery("#editOptionType").length == 0){
				jQuery("#editOptionType").append();
			}
			jQuery("#createCompOptionDetails").css("display", "none");
			jQuery("#createCompOptionDiv").empty();
			jQuery("#editOption").val(jQuery("#editOption option:first").val());
			jQuery("#editOption").css("display", "block");
		  });
	/* End of Create options for the new component */
	/* Start of the events for create component*/
	/* Start of of list of methods used for create component */
	function getOptionDesc(){
		var chckboxCntDiv = $("#compOptionDesc").find(".checkboxcntDiv");
		var optionString =  "";
		jQuery(chckboxCntDiv).each(function(index){
			var containerDiv = $(this).attr("id");
			var optionDescDiv = $("#"+containerDiv).find(".compOptDesc");
			jQuery(optionDescDiv).each(function(indexContent){
				var optDescId = $(this).attr("id");
				var optText = $("#"+optDescId).val();
				if(optionString ==""){
		    	       optionString = optText;
		    	       }
		    	    else{
		    		   optionString = optionString+"^"+optText;
	    	       }
				});
			});
		 jQuery("#optionDescription").val(optionString); 
		}
	function getLastDiv(lastDiv){
			var lastDivnum = parseInt(lastDiv);
			var flag = false; 
			var currDiv = "";
			for(var i=(lastDiv-1); i>0; i--){
				currDiv = "add_"+i;
					if(jQuery("#"+currDiv).length !=0){
							flag = true;
							lastDivnum = i;
							break;
						}
				}
			if(flag == true){
					currDiv = "add_"+lastDivnum;
				}
			else{
				    currDiv = "add_"+lastDiv; 	
				}
			return currDiv;
		}
	
	/*End of list of methods used for create component */
	
});