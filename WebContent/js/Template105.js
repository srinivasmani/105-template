 var errors = 0;
	jQuery(document).ready(function(){
		customerEditBorder();
		var isValidation = true;
		changeImage(jQuery("#imgType").val());
		jQuery("input[name='imgType']").change(function(){
			var imgType = jQuery(this).val();
			changeImage(imgType);		
		});
		
		jQuery("#formName").keyup(function(){
			jQuery("#formNameTab").text(jQuery("#formName").val());
		});
                
                jQuery("#updateAdminTemplate105").validate({
	    	onkeyup: false,
	    	onclick: false,
	    	onblur: false,
	    	onfocusout:false,
	    	invalidHandler: function(form, validator) {
	          errors = validator.numberOfInvalids();
                  alert(errors);
	        if (errors) {
	        	alert("This field can not be blank.");
	        	errors=0;
	        }
	        else{
	        	launchWindow('#dialog');
	        }},
		    rules: {
	    	headerText: {
	    	  requiredHeader: true
	            },
		      component1: {
	    		required: true
		         },
		      component2: {
		    	required:true
			   },
			  component3: {
				required:true
			   },
			 component4: {
			 	required:true
			  },
			  component5: {
		    	required: true
			  },
			  component10: {
			   	required:true
			 },
			 component18: {
				required:true
			  },
			 component24: {
				required: true
			  },
	 	      component54: {
			   		required: true
		      },
			  component65: {
				required:true
			   },
			 component66: {
				 	required:true
			  },
			  component67: {
				required:true
			  },
			  component68: {
				required:true
			  },
			  component69: {
					required:true
				  }
	       }  
                });
	
	jQuery.validator.addMethod("required", function(comp , element) {
		 var val = jQuery.trim(jQuery("#"+element.id).val());
		if(val==''){
	          var imageId = element.id.substring(9);
			  jQuery("#editButton"+imageId).show();
			  jQuery("#okButton"+imageId).hide();
			return false;
		}
		else{
			return true;

			}
 	},"");
	jQuery.validator.addMethod("requiredHeader", function(comp , element) {
		 var val = jQuery.trim(jQuery("#"+element.id).val());
		if(val==''){
			jQuery("#editButtonheaderText").show();
			jQuery("#okButtonHeader").hide();
			return false;
		}
		else{
			return true;

			}
	},"");
});
	
function updateTemplate105(action) {
	if (errors <= 0) {
		if (jQuery("#component_01").val() === "") {
			jQuery("#component_01").val(" ");
		}
		if (jQuery("#component_02").val() === "") {
			jQuery("#component_02").val(" ");
		}
                if (jQuery("#component_03").val() === "") {
			jQuery("#component_03").val(" ");
		}
                if (jQuery("#component_04").val() === "") {
			jQuery("#component_04").val(" ");
		}
                if (jQuery("#component_05").val() === "") {
			jQuery("#component_05").val(" ");
		}
                if (jQuery("#component_06").val() === "") {
			jQuery("#component_06").val(" ");
		}
                if (jQuery("#component_07").val() === "") {
			jQuery("#component_07").val(" ");
		}
		var component0 = jQuery("#component_01").val() +"_"+
                                jQuery("#component_02").val() +"_"+
                                jQuery("#component_03").val() +"_"+
                                jQuery("#component_04").val() +"_"+
                                jQuery("#component_05").val() +"_"+
                                jQuery("#component_06").val() +"_"+
                                jQuery("#component_07").val();
                        
		jQuery("#component0").val(component0);
		jQuery("input").attr("readonly", false);
		jQuery("textarea").attr("readonly", false);
                
		document.updateAdminTemplate105.action = action;

		jQuery("#updateAdminTemplate105").submit();
		jQuery("input[type=text]").attr("readonly", true);
		jQuery("textarea").attr("readonly", true);
	}
	return true;
}

function limitOfCharHeader(event, id, charlength) {
	var key;
	var val = (jQuery("#" + id).val());
	maxChar = val.length;
	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = event.which; // firefox
	if (key == 13) {
		// removeEditMandatory(id , maxChar);
		removeEditHeader(id, maxChar);
		return false;
	}
	return true;
}

function limitnofotextCustomer(id, event) {
	var is_chrome = /chrome/.test(navigator.userAgent.toLowerCase());
	var maxWidth = 770;
	if (is_chrome == true) {
		maxWidth = 685;
	}
	jQuery("#temp1").empty();
	jQuery("#temp1").text(jQuery("#" + id).val());
	var matches = jQuery("#" + id).val();
	var pattern = /_/g;
	var v = matches.match(pattern);
	var width = 0;
	jQuery("#temp1").hide();
	if (v != null) {
		width = jQuery("#temp1").width()
				- ((matches.match(pattern).length / 2) * .5);
	} else {
		width = jQuery("#temp1").width();
	}
	if (width >= maxWidth && (event.keyCode != 8 && event.keyCode != 46)) {
		jQuery("#" + id).attr('maxlength', jQuery("#" + id).val().length);
		alert("Cannot add more characters.");
		if ((id != "component_02")) {
			jQuery("#component_02").focus();
		} else {
			comp02FocusOut();
		}
	} else if (event.keyCode == 13) {
		if ((id != "component_02")) {
			jQuery("#component_02").focus();
		} else {
			comp02FocusOut();
		}
	}
	return true;
}

function comp01FocusOut() {
	jQuery("#component_02").focus();
}
		
function comp02FocusOut() {
	jQuery("#editButton0").show();
	jQuery("#okButton0").hide();
	jQuery("#component_01").removeClass("whiteBG");
	jQuery("#component_02").removeClass("whiteBG");
        jQuery("#component_03").removeClass("whiteBG");
	jQuery("#component_04").removeClass("whiteBG");
        jQuery("#component_05").removeClass("whiteBG");
	jQuery("#component_06").removeClass("whiteBG");
        jQuery("#component_07").removeClass("whiteBG");
	        
	jQuery("#component_01").addClass('componentHeader');
	jQuery("#component_02").addClass('componentHeader');
        jQuery("#component_03").addClass('componentHeader');
	jQuery("#component_04").addClass('componentHeader');
        jQuery("#component_05").addClass('componentHeader');
	jQuery("#component_06").addClass('componentHeader');
        jQuery("#component_07").addClass('componentHeader');
	
        
	jQuery("#component_01").css('border', 'none');
	jQuery("#component_02").css('border', 'none');
        jQuery("#component_03").css('border', 'none');
	jQuery("#component_04").css('border', 'none');
        jQuery("#component_05").css('border', 'none');
	jQuery("#component_06").css('border', 'none');
        jQuery("#component_07").css('border', 'none');
	
                
	jQuery("#component_01").attr("readonly", true);
	jQuery("#component_02").attr("readonly", true);
        jQuery("#component_03").attr("readonly", true);
	jQuery("#component_04").attr("readonly", true);
        jQuery("#component_05").attr("readonly", true);
	jQuery("#component_06").attr("readonly", true);
        jQuery("#component_07").attr("readonly", true);
	
        
	jQuery("#component_01").focusout();
	jQuery("#component_02").focusout();
        jQuery("#component_03").focusout();
	jQuery("#component_04").focusout();
        jQuery("#component_05").focusout();
	jQuery("#component_06").focusout();
        jQuery("#component_07").focusout();
	        
	jQuery("#component1").focus();
	customerEditBorder();
}

function limitOfCharForMandatory(event, id, charlength) {
	var key;
	var val = (jQuery("#" + id).val());
	maxChar = val.length;
	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = event.which; // firefox
	if (key == 13) {
		removeEditMandatory(id, maxChar);
		// removeEditHeader(id , maxChar);
		return false;
	}
	return true;
}

function limitnofotext(event, id, charlength) {
	var key;
	var val = (jQuery("#" + id).val());
	maxChar = val.length;
	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = event.which; // firefox
	if (key == 13) {
		removeEdit(id, maxChar);
		// removeEditHeader(id , maxChar);
		return false;
	}
	return true;
}
	
function editText(id) {
	var imageId = id.substring(9);
	jQuery("#editButton" + imageId).hide();
	jQuery("#okButton" + imageId).show();
	jQuery("#" + id).removeClass("componentHeader");
	jQuery("#" + id).addClass('whiteBG');
	jQuery("#" + id).css('border', 'solid 1px black');
	jQuery("#" + id).attr("readonly", false);
	jQuery("#" + id).focus();
}
	
function editTextCustomer(id) {
	jQuery("#editButton0").hide();
	jQuery("#okButton0").show();
        
        jQuery("#component_01").removeClass("componentHeader");
	jQuery("#component_02").removeClass("componentHeader");
        jQuery("#component_03").removeClass("componentHeader");
	jQuery("#component_04").removeClass("componentHeader");
        jQuery("#component_05").removeClass("componentHeader");
	jQuery("#component_06").removeClass("componentHeader");
        jQuery("#component_07").removeClass("componentHeader");
        
	jQuery("#component_01").addClass('whiteBG');
	jQuery("#component_02").addClass('whiteBG');
        jQuery("#component_03").addClass('whiteBG');
        jQuery("#component_04").addClass('whiteBG');
        jQuery("#component_05").addClass('whiteBG');
        jQuery("#component_06").addClass('whiteBG');
        jQuery("#component_07").addClass('whiteBG');
        
	jQuery("#component_01").css('border', 'solid 1px black');
	jQuery("#component_02").css('border', 'solid 1px black');
        jQuery("#component_03").css('border', 'solid 1px black');
        jQuery("#component_04").css('border', 'solid 1px black');
        jQuery("#component_05").css('border', 'solid 1px black');
        jQuery("#component_06").css('border', 'solid 1px black');
        jQuery("#component_07").css('border', 'solid 1px black');
        
	jQuery("#component_01").attr("readonly", false);
	jQuery("#component_02").attr("readonly", false);
        jQuery("#component_03").attr("readonly", false);
        jQuery("#component_04").attr("readonly", false);
        jQuery("#component_05").attr("readonly", false);
        jQuery("#component_06").attr("readonly", false);
        jQuery("#component_07").attr("readonly", false);
        
	jQuery("#component_01").focus();
	jQuery("#" + id).focus();
}
	
function removeEditMandatory(id, maxChar) {
	var val = jQuery.trim(jQuery("#" + id).val());
	if (val <= 0) {
		alert("This field can not be blank.");
		jQuery("#" + id).focus();
	} else {
		jQuery("#" + id).val(val);
		var imageId = id.substring(9);
		jQuery("#editButton" + imageId).show();
		jQuery("#okButton" + imageId).hide();
		jQuery("#" + id).removeClass("whiteBG");
		jQuery("#" + id).addClass('componentHeader');
		jQuery("#" + id).css('border', 'none');
		jQuery("#" + id).attr("readonly", true);
	}
}
	
function onblurDo(id, maxChar) {
	var val = jQuery.trim(jQuery("#" + id).val());
	if (val <= 0) {
		alert("This field can not be blank.");
		jQuery("#" + id).focus();

	} else {
		jQuery("#" + id).val(val);
		var imageId = id.substring(9);
		jQuery("#editButton" + imageId).show();
		jQuery("#okButton" + imageId).hide();
		jQuery("#" + id).removeClass("whiteBG");
		jQuery("#" + id).addClass('componentHeader');
		jQuery("#" + id).css('border', 'none');
		jQuery("#" + id).attr("readonly", true);
	}
}
	
function removeEdit(id, maxChar) {
	var val = jQuery.trim(jQuery("#" + id).val());
	if (val.length > 0) {
		var deleteImageId = id.substring(9);
		if (jQuery("#deleteButton" + deleteImageId)) {
			jQuery("#deleteButton" + deleteImageId).show();
		}
	}
	jQuery("#" + id).val(val);
	var imageId = id.substring(9);
	jQuery("#editButton" + imageId).show();
	jQuery("#okButton" + imageId).hide();
	jQuery("#" + id).removeClass("whiteBG");
	jQuery("#" + id).addClass('componentHeader');
	jQuery("#" + id).css('border', 'none');
	jQuery("#" + id).attr("readonly", true);
	jQuery("#" + id).focusout();

}
	
function removeEditCustomer(id, maxChar) {
	jQuery("#editButton0").show();
	jQuery("#okButton0").hide();
	jQuery("#component_01").removeClass("whiteBG");
	jQuery("#component_02").removeClass("whiteBG");
        jQuery("#component_03").removeClass("whiteBG");
        jQuery("#component_04").removeClass("whiteBG");
        jQuery("#component_05").removeClass("whiteBG");
        jQuery("#component_06").removeClass("whiteBG");
        jQuery("#component_07").removeClass("whiteBG");
        
	jQuery("#component_01").addClass('componentHeader');
	jQuery("#component_02").addClass('componentHeader');
        jQuery("#component_03").addClass('componentHeader');
        jQuery("#component_04").addClass('componentHeader');
        jQuery("#component_05").addClass('componentHeader');
        jQuery("#component_06").addClass('componentHeader');
        jQuery("#component_07").addClass('componentHeader');
        
	jQuery("#component_01").css('border', 'none');
	jQuery("#component_02").css('border', 'none');
        jQuery("#component_03").css('border', 'none');
        jQuery("#component_04").css('border', 'none');
        jQuery("#component_05").css('border', 'none');
        jQuery("#component_06").css('border', 'none');
        jQuery("#component_07").css('border', 'none');
        
	jQuery("#component_01").attr("readonly", true);
	jQuery("#component_02").attr("readonly", true);
        jQuery("#component_03").attr("readonly", true);
        jQuery("#component_04").attr("readonly", true);
        jQuery("#component_05").attr("readonly", true);
        jQuery("#component_06").attr("readonly", true);
        jQuery("#component_07").attr("readonly", true);
        
	jQuery("#component_01").focusout();
	jQuery("#component_02").focusout();
        jQuery("#component_03").focusout();
        jQuery("#component_04").focusout();
        jQuery("#component_05").focusout();
        jQuery("#component_06").focusout();
        jQuery("#component_07").focusout();
        
	customerEditBorder();
}
	
function removeMultipleEdit(from, to) {
	var val = '';
	if (jQuery.trim(jQuery("#component24").val()) == ''
			|| jQuery.trim(jQuery("#component54").val()) == ''
			|| jQuery.trim(jQuery("#component63").val()) == ''
			|| jQuery.trim(jQuery("#component64").val()) == ''
			|| jQuery.trim(jQuery("#component65").val()) == ''
			|| jQuery.trim(jQuery("#component66").val()) == ''
                        || jQuery.trim(jQuery("#component67").val()) == ''
                        || jQuery.trim(jQuery("#component68").val()) == ''
			|| jQuery.trim(jQuery("#component69").val()) == '') {
		alert("This field can not be blank.");
	}
	else {
		jQuery("#editButton" + from).show();
		jQuery("#okButton" + from).hide();
		while (to >= from) {
			val = jQuery.trim(jQuery("#component" + from).val());
			jQuery("#component" + from).removeClass("whiteBG");
			jQuery("#component" + from).addClass('componentHeader');
			jQuery("#component" + from).removeClass("tireComponentHeaderEdit");
			jQuery("#component" + from).addClass('tireComponentHeader');
			jQuery("#component" + from).val(val);
			jQuery("#component" + from).css('border', 'none');
			jQuery("#component" + from).attr("readonly", true);
			from = from + 1;
		}
	}
}
	 
	
function editMultipleText(from, to) {
	jQuery("#editButton" + from).hide();
	jQuery("#okButton" + from).show();
	while (to >= from) {
		jQuery("#component" + from).removeClass("componentHeader");
		jQuery("#component" + from).addClass('whiteBG');
		jQuery("#component" + from).removeClass("tireComponentHeader");
		jQuery("#component" + from).addClass('tireComponentHeaderEdit');
		jQuery("#component" + from).css('border', 'solid 1px black');
		jQuery("#component" + from).attr("readonly", false);
		from = from + 1;
	}
}
	
function deleteCompContent(id, inputId) {
	var r = confirm("Are you sure want to delete?");
	if (r == true) {
		jQuery("#" + inputId).val("");
		jQuery("#" + id).hide();
	}
}
	
function editTextHeader(id) {
	var imageId = id.substring(9);
	jQuery("#editButtonheaderText").hide();
	jQuery("#okButtonHeader").show();

	jQuery("#" + id).css('border', 'solid 1px black');
	jQuery("#" + id).attr("readonly", false);
	jQuery("#" + id).focus();
}
	
function removeEditHeader(id, maxChar) {

	var val = jQuery.trim(jQuery("#" + id).val());
	jQuery("#" + id).val(val);
	var imageId = id.substring(9);
	if (val <= 0) {
		alert("This field can not be blank.");
		jQuery("#" + id).focus();
	} else {
		jQuery("#editButtonheaderText").show();
		jQuery("#okButtonHeader").hide();
		jQuery("#" + id).css('border', 'none');
		jQuery("#" + id).attr("readonly", true);
	}
}

function showEditor(id,contextPath) {
        jQuery("div#logoText").hide();
        jQuery("div#msgText").hide();
        jQuery("div#imgLogo").hide();
        jQuery("#editButtonLogoText").hide();
	jQuery("#okButtonLogo").show();
        //tinymce.get('logoEditorTiny').show();
        //jQuery('#iframelogotext').hide();
        showCKEditor(id,contextPath);
        return false;
}

function showCKEditor(id,contextPath){
            var imageLibraryCP = "/ImageLibrary";
            CKEDITOR.replace('logoEditorTiny', {
                on: {
                        instanceReady: function (ev)
                        {
                            this.dataProcessor.writer.setRules('p',
                                    {
                                        indent: false,
                                        breakBeforeOpen: true,
                                        breakAfterOpen: false,
                                        breakBeforeClose: false,
                                        breakAfterClose: true
                                    });
                        }
                },
                contentsCss: "../editor/ckeditor/ckeditor/customckcontents.css",
                 filebrowserImageBrowseUrl: imageLibraryCP+'/editor/ckeditor/ofm/ofm.jsp?fileConnector='+imageLibraryCP+'/ck/ofm/fileManager?treeConnector='+imageLibraryCP+'/ck/ofm/filetree&type=Image',
                  //  customConfig: '/ckeditor.filebrowser/editor/ckeditor/myckconfig.js',
                    filebrowserLinkBrowseUrl: imageLibraryCP+'/editor/ckeditor/ofm/ofm.jsp?fileConnector='+imageLibraryCP+'/ck/ofm/fileManager?treeConnector='+imageLibraryCP+'/ck/ofm/filetree&type=File',
                    //filebrowserImageUploadUrl: imageLibraryCP+'/ck/sfm/uploader?Type=Image',
			filebrowserImageUploadUrl: imageLibraryCP+'/mightyUpload/uploadFile',
                uiColor: '#CCEAEE',
                width:'390px',
                height:'280px'
                });
                
}
	
function hideEditor(id) {
        var data = CKEDITOR.instances.logoEditorTiny.getData();//tinymce.get('logoEditorTiny').getContent();
                
        $("div#logoText" ).empty();
        if(jQuery.trim(data) != ''){
            $("div#logoText").append(data);
            $("#logoTextId").val(data);
            jQuery("div#msgText").hide();
            jQuery("div#logoText").show();
            
            //jQuery('#iframelogotext').contents().find('html').html(jQuery("div#logoText").html());
            $('#logoText').contents().find('div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,label,fieldset,input,p,blockquote').css({"margin-top":"1px","margin-bottom":"0px"});
            //$('#iframelogotext').show();
            generateCanvas();
            
        }else if($("#imgLogo").find("img").length > 0){
            //$("#logoTextId").val("");
            jQuery("div#imgLogo").show();
        }else{
            jQuery("div#msgText").show();
        }
        jQuery("#editButtonLogoText").show();
        jQuery("#okButtonLogo").hide();
        jQuery("#" + id).css('border', 'none');
        jQuery("#" + id).attr("readonly", true);
        //tinymce.get('logoEditorTiny').hide();
        CKEDITOR.instances.logoEditorTiny.updateElement();
        CKEDITOR.instances.logoEditorTiny.destroy();
        jQuery("#logoEditorTiny").hide();
        return false;
}

function generateCanvas(){

    //renderSubSuperScripts();
    renderLists();                                        
    html2canvas($("div#logoText"),{
		onrendered: function(canvas){
                        var ctx=canvas.getContext("2d");
                        ctx.webkitImageSmoothingEnabled = false;
                        ctx.mozImageSmoothingEnabled = false;
                        ctx.imageSmoothingEnabled = false;
			$("#html2canvasImg").val(canvas.toDataURL());
		}
	});
}


function editMultipleTextLast(from, to) {
	jQuery("#editButton" + from).hide();
	jQuery("#okButton" + from).show();
	while (to >= from) {
		jQuery("#component" + from + "Span").hide();
		jQuery("#component" + from).removeClass("componentHeader");
		jQuery("#component" + from).addClass('whiteBG');
		jQuery("#component" + from).removeClass("tireComponentHeader");
		jQuery("#component" + from).addClass('tireComponentHeaderEdit');
		jQuery("#component" + from).css('border', 'solid 1px black');
		jQuery("#component" + from).css('display', 'block');
		jQuery("#component" + from).attr("readonly", false);
		from = from + 1;
	}

}
function removeMultipleEditLast(from, to) {

	var val = '';
	if (jQuery.trim(jQuery("#component67").val()) == ''
			|| jQuery.trim(jQuery("#component68").val()) == ''
			|| jQuery.trim(jQuery("#component69").val()) == '') {
		alert("This field can not be blank.");
	}

	else {
		jQuery("#editButton" + from).show();
		jQuery("#okButton" + from).hide();
		while (to >= from) {
			val = jQuery.trim(jQuery("#component" + from).val());
			jQuery("#component" + from).removeClass("whiteBG");
			jQuery("#component" + from).addClass('componentHeader');
			jQuery("#component" + from).removeClass("tireComponentHeaderEdit");
			jQuery("#component" + from).addClass('tireComponentHeader');
			jQuery("#component" + from).val(val);
			jQuery("#component" + from + "Span").html(val);
			jQuery("#component" + from + "Span").show();
			jQuery("#component" + from).css('display', 'none');
			jQuery("#component" + from).css('border', 'none');
			jQuery("#component" + from).attr("readonly", true);
			from = from + 1;
		}
	}
}

function customerEditBorder(){
	var custTextEdit = jQuery("#component_01").val()+""+jQuery("#component_02").val()+""
                            +jQuery("#component_03").val()+""+jQuery("#component_04").val()+""
                            +jQuery("#component_05").val()+""+jQuery("#component_06").val()+""
                           +jQuery("#component_07").val();
	var trimCustEdit = jQuery.trim(custTextEdit);
	if((trimCustEdit.length) ==0){
		jQuery("#customerP").css("border", "none");
	}
	else{
		jQuery("#customerP").css("border", "1px solid black");
	}
}