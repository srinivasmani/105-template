jQuery(document).ready(function() {
	// for image resizing.. by Madhu
	
	/*$("img.resizeImage").each(function(){
		 
		var width = $(this).width();    // Current image width
	    var height = $(this).height();  // Current image height
		var pic_real_width, pic_real_height;	
		//var pic = $(this);
		$(this).removeAttr("width");
		$(this).removeAttr("height");
		$(this).css("width","");
		$(this).css("height","");
		$(this).css("max-width",width);
		$(this).css("max-height",height);
	});*/
		
	
	var deleteAlert = "Are you sure you want to delete?";
	var headingAlert = "This field can not be blank.";
		jQuery("#selectImg").val("");
		jQuery("#localImage").val("");
		var isValidation = true;
		changeImage($("#imgType").val());
		$("input[name='imgType']").change(function(){
			var imgType = $(this).val();
			jQuery("#imgType").val(imgType);
			changeImage(imgType);		
		});
		
	 	setDelEditImage();
		var headerColor = jQuery("#headerColor").val();
		if((headerColor == "") || (headerColor == null)){
			 jQuery("#headerColor").val("#045fb4");
		}
		else{
			 jQuery("#headerColor").val(headerColor);
			 jQuery(".text").css("color", headerColor);
		}
		jQuery("#inpDefault").click(function(){
			jQuery("#currHeaderColor").val("#045fb4");
			jQuery(".text").css("color", "#045fb4");
		});
		jQuery("#formName").keyup(function(){
			jQuery("#formNameTab").text(jQuery("#formName").val());
		});
		jQuery("#updateId").click(function(){
			if(jQuery("#editText").val()==""){
				alert("Please enter heading.");
			}
			else if(jQuery("#edited").val() == "true"){
				alert("Please click Ok or Cancel.");
			}
			else {
				var preDiv = jQuery("#prevDiv").attr("value");
				var componentDiv = jQuery(this).attr("id");
				jQuery("#headerColor").val(jQuery("#currHeaderColor").val());
				jQuery("#updateForm").submit();
				inActiveComponent(preDiv);
				activeComponent(componentDiv);
			}
		});
		/*---------------------------- Edit header text ---------------------------*/
		jQuery("#headerText").click(function(){
			jQuery("#editHeading").css("display", "block");
		});
		
		jQuery("#editHeading").click(function(){
			jQuery("#editText").css("display","block");
			jQuery("#editText").focus();
			jQuery("#headerText").hide();
			jQuery("#editHeading").hide();
			jQuery("#okHeading").show();
		});
		jQuery("#editText").keyup(function(e){
			if(e.keyCode == 13){
				if(jQuery("#editText").val()==""){
					alert("Please enter heading.");
				}else{
					saveEditText();
				}
			}
		});
		jQuery("#editText").focusout(function(){
			if(jQuery("#editText").val()==""){
				alert("Please enter heading.");
			}
			else {
				saveEditText();
			}
		});
		jQuery("#okHeading").click(function(){
			if(jQuery("#editText").val()==""){
				alert("Please enter heading.");
			}
			else {
				saveEditText();
			}
		});
		/*-----------------------------------------------------------------------*/	
		
	$('.selectLink').live('click',function(){
		var type = jQuery("#place").val();
		jQuery("#localImage").val('');
		if(jQuery("#localImage").val()==""||jQuery("#localImage").val()==null){
			jQuery("#deleteButton").hide();
			jQuery("#saveButton").hide();
			
		}
		if(jQuery("#saveImage").val()==""||jQuery("#saveImage").val()== null){
			jQuery("#delBtn1").html('<input type="button" value="Delete" id="deleteButton1" style="margin-left: 3px;margin-top: -25px;"/>');
		}
		else  if(jQuery("#saveImage").val()!=""||jQuery("#saveImage").val()!= null){
			jQuery("#deleteButton1").show();
		}
		var selectId = jQuery(this).attr("id");
		var userRole = jQuery(".userRole").val();
		var selectNum = selectId.split("_");
		var imageid = "imageName_"+selectNum[1];
		var imgName = jQuery("#"+imageid).val();
		var isSmall;
		var imageHeight= $("#reqHeight").val();
		var imageWidth= $("#reqWidth").val();
		jQuery("#selectImg").val(imgName);
		jQuery.ajax({
			url : "./showUserRepositoryImage.do?imgName="+imgName+"&type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth,
			type : "POST",
			dataType : "html",
			data : "vardd=1",
			success : function(data) {
				if(type == 'center'){
					jQuery("#imgLogo").empty();
					jQuery("#imgLogo").html(data);
				}
				else if(type == 'center104'){
					jQuery("#imgLogo").empty();
					jQuery("#imgLogo").html(data);
				}
                                else if(type == 'center105'){
					jQuery("#imgLogo").empty();
					jQuery("#imgLogo").html(data);
				}
				else if(type == 'left'){
					jQuery("#imgLeft").empty();
					jQuery("#imgLeft").html(data);
				}
				else if(type == 'leftLogo'){
					jQuery("#logoLeft").empty();
					jQuery("#logoLeft").html(data);
				}
				else if(type == 'leftLogo104'){
					jQuery("#logoLeft104").empty();
					jQuery("#logoLeft104").html(data);
				}
				else if(type == 'rightLogo'){
					jQuery("#logoRight").empty();
					jQuery("#logoRight").html(data);
				}
				else if(type == 'rightLogo104'){
					jQuery("#logoRight104").empty();
					jQuery("#logoRight104").html(data);
				}
				else if(type == 'bottomLeft'){
					jQuery("#imgBottomLeft").empty();
					jQuery("#imgBottomLeft").html(data);
				}
				else if(type == 'bottomRight'){
					jQuery("#imgBottomRight").empty();
					jQuery("#imgBottomRight").html(data);
				}
				else if(type == 'bottomLast'){
					jQuery("#imgBottomLast").empty();
					jQuery("#imgBottomLast").html(data);
				}
				else if(type == 'bottomMid'){
					jQuery("#imgBottomMid").empty();
					jQuery("#imgBottomMid").html(data);
				}
				else if(type == 'bottomCenter'){
					jQuery("#imgBottomCenter").empty();
					jQuery("#imgBottomCenter").html(data);
				}
				else if(type == 'template1'){
					jQuery("#imgLogo").empty();
					jQuery("#imgLogo").html(data);
				}
				var name = "#imgDiv"+selectNum[1];
				jQuery(".unHighlighted").css("background-color","");
				jQuery(name).css("background-color","grey");
				isSmall=jQuery("#smallImg").val();
			},
			complete: function(){
				if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
					var stretch;
					var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
					var response = confirm(msg);
					if(response){
						stretch="yes";
						
					}else{
						stretch="no";
					}
					jQuery.ajax({
						url : "./showUserRepositoryImage.do?imgName="+imgName+"&type="+type+"&stretch="+stretch+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth,
						type : "POST",
						dataType : "html",
						data : "vardd=1",
						success : function(data) {
							if(type == 'center'){
								jQuery("#imgLogo").empty();
								jQuery("#imgLogo").html(data);
							}
							else if(type == 'center104'){
								jQuery("#imgLogo").empty();
								jQuery("#imgLogo").html(data);
							}
                                                        else if(type == 'center105'){
								jQuery("#imgLogo").empty();
								jQuery("#imgLogo").html(data);
							}
							else if(type == 'left'){
								jQuery("#imgLeft").empty();
								jQuery("#imgLeft").html(data);
							}
							else if(type == 'leftLogo'){
								jQuery("#logoLeft").empty();
								jQuery("#logoLeft").html(data);
							}
							else if(type == 'leftLogo104'){
								jQuery("#logoLeft104").empty();
								jQuery("#logoLeft104").html(data);
							}
							else if(type == 'rightLogo'){
								jQuery("#logoRight").empty();
								jQuery("#logoRight").html(data);
							}
							else if(type == 'rightLogo104'){
								jQuery("#logoRight104").empty();
								jQuery("#logoRight104").html(data);
							}
							else if(type == 'bottomLeft'){
								jQuery("#imgBottomLeft").empty();
								jQuery("#imgBottomLeft").html(data);
							}
							else if(type == 'bottomRight'){
								jQuery("#imgBottomRight").empty();
								jQuery("#imgBottomRight").html(data);
							}
							else if(type == 'bottomLast'){
								jQuery("#imgBottomLast").empty();
								jQuery("#imgBottomLast").html(data);
							}
							else if(type == 'bottomMid'){
								jQuery("#imgBottomMid").empty();
								jQuery("#imgBottomMid").html(data);
							}
							else if(type == 'bottomCenter'){
								jQuery("#imgBottomCenter").empty();
								jQuery("#imgBottomCenter").html(data);
							}
							else if(type == 'template1'){
								jQuery("#imgLogo").empty();
								jQuery("#imgLogo").html(data);
							}
						},
						complete: function(){
							jQuery("#smallImg").val("");
						  }
					});
					
				}
		  }
		});
	});
	
	jQuery(".ListDiv .iImageEdit").click(function() {
		   var edited = jQuery("#edited").val();
		   if(edited != "true"){
			   var preDiv = jQuery("#prevDiv").attr("value");
			   var componentDiv = jQuery(this).parent().attr("id");
			   inActiveComponent(preDiv);
			   activeComponent(componentDiv);
			   jQuery("#prevDiv").attr("value",componentDiv);
			   jQuery("#currentDiv").attr("value",componentDiv); 
			   if(isUniqueComponent(componentDiv, "hidden") != false){
				   var compNum = componentDiv.split("_");
				   var cmpName = "componentName"+compNum[1];
				   var visibilityDiv = "visibility"+compNum[1];
				   if(jQuery("#"+visibilityDiv).val() == "deleted"){
					   jQuery("#editComponent").css("display", "block");
					   jQuery("#CompDrp").val("0"); 
					   jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
					   jQuery("#compDetails").css("display", "none");
				   }
				   else{
					   serComponentVisible(compNum);
					   var delButt = "delComp_"+compNum[1];
					   var labelId = jQuery("#" + componentDiv).children().attr("id");
					   var imageId = jQuery("#" + labelId).next().attr("id");
					   var cmpId = jQuery("#" + imageId).next().attr("id");
					   var subLabelId = jQuery("#" + cmpId).next().attr("id");
					   jQuery("#"+delButt).css("visibility", "visible");
			           jQuery("#current_label_id").attr("value",cmpName);
					   jQuery("#current_sublabel_id").attr("value",subLabelId);
					   var componentNum = componentDiv.split("_");
					   inActiveComponent(preDiv);
					   activeComponent(componentDiv);
					   setComponents(componentDiv);
					   showOnTemplate(componentNum);
					   setHeadingDrp(componentNum);
					   setLabelDisplay(componentNum);
					   setEditOptionType(componentNum);
					   setOptionDescription(componentNum);
					   getOptionDrp(componentNum);
					   jQuery("#compDetails").css("display", "block");
					   if(jQuery("#"+visibilityDiv).val() == "hidden"){
						   jQuery("#edited").val("true");
						   }else{
							   jQuery("#edited").val("false"); 
						   }
				   }
			   
			   }else{
				   jQuery("#editComponent").css("display", "block");
				   jQuery("#CompDrp").val("0"); 
				   jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
				   jQuery("#compDetails").css("display", "none");
			   }
		   }
		   else{
			   alert("Please click Ok or Cancel.");
		   }
		  });
	/*if ($.browser.msie) {
		  $("#CompDrp").click(function() {
			  setTimeout( function() {
				  this.focus();
				}, 500 );
		  });
		}*/

	jQuery("#CompDrp").change(function(){
		var currDiv = jQuery("#currentDiv").val();
		var currNum = currDiv.split("_");
		if(jQuery("#CompDrp option:selected").text() != "Select"){
		var compNum ="";
		var replaceComp = true;
		var selectedOpt = jQuery(this).attr("value");
		var currName = (jQuery("#CompDrp option:selected").text());
		var changedName = "";
		for(var i=1; i<=15; i++){
			var searchViewComp = "viewComponent"+i;
			if((parseInt(selectedOpt) == parseInt(jQuery("#"+searchViewComp).val())) && (i != parseInt(currNum[1]))){
				var compDiv = "compDiv_"+i;
				if(((jQuery("#"+compDiv).css("visibility")) == "visible") || ((jQuery("#"+compDiv).css("visibility")) == "inherit")){
					var compName = "componentName"+i;
					changedName = (jQuery("#"+compName).val());
					replaceComp = false;
				}
			}
		}
		if(replaceComp != true){
			var curDiv  = jQuery("#currentDiv").val();
			compNum = curDiv.split("_");
			var currViewComp = "viewComponent"+compNum[1];
			jQuery("#CompDrp").val(jQuery("#"+currViewComp).val());
			jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
			if(currName != changedName){
				alert("'"+currName+"' changed as '"+changedName+"' and it is already in use. Choose another one.");
			}else{
				alert("The component is already in use. Choose another one.");
			}
		}else{
		jQuery.ajax({
			url :"./getEditComponent.do",
			type :"POST",
			async : false,
			dataType : "html",
			data :"ufsId="+selectedOpt,
			success : function(data) {
				jQuery("#compDetails").empty();
				jQuery("#compDetails").html(data);
		   		},
	   	   complete: function(){
	 	   	    var curDiv  = jQuery("#currentDiv").val();
				compNum = curDiv.split("_");
				var headingSpan = "headingSpan"+compNum[1];
				jQuery("#"+headingSpan).text(jQuery("#CompDrp :selected").text());
				jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
				setSelectedLabel(compNum);
				setOptionDrpListRight(compNum);
				serComponentVisible(compNum);
				jQuery("#compDetails").css("display", "block");
				jQuery("#edited").val("true");
	 	   	   },
		   error : function(xmlhttp, error_msg) {
	   		  }
		  		});
		}
		}
      	});

		jQuery(".ListDiv .iDeleteEdit").click(function(){
			if(jQuery("#edited").val() == "true"){
				alert("Please click Ok or Cancel.");
			 }else{
	        answer = confirm("Are you sure to delete?");
	        if(answer){
	        var curDiv = jQuery(this).parent().attr("id");
			compNum = curDiv.split("_");
			var curVisibilityDiv = "visibility"+compNum[1];
			var curr_compDiv = "compDiv_"+compNum[1];
			var labelSpanDiv = "labelSpan"+compNum[1];
			var editCompDiv = "editComp_"+compNum[1];
			var delCompDiv = "delComp_"+compNum[1];
			
			jQuery("#"+curr_compDiv).css("visibility", "hidden");
			jQuery("#"+labelSpanDiv).css("visibility", "hidden");
			jQuery("#"+curVisibilityDiv).val("hidden");
			jQuery("#"+editCompDiv).css("visibility", "visible");
			jQuery("#"+delCompDiv).css("visibility", "hidden");
			jQuery("#editComponent").css("display", "none");
			jQuery("#edited").val("false");
	        }
			}
			});
		
		jQuery("#editComponent #rightOk").click(function(){
			if(jQuery("#edited").val() == "true"){
			var curDiv  = jQuery("#currentDiv").val();
			compNum = curDiv.split("_");
			var curVisibilityDiv = "visibility"+compNum[1];
			jQuery("#"+curVisibilityDiv).val("visible");
			setEditdHeading(compNum);
			setEditedLabel(compNum);
			setOptionType(compNum);
			setEditedDesription(compNum);
			setViewComponentId(compNum);
			var delImgButt = "delComp_"+compNum[1];
			jQuery("#"+delImgButt).css("visibility", "visible");
			jQuery("#edited").val("false");
			jQuery("#editComponent").css("display", "none");
			}else{
				jQuery("#edited").val("false");
				jQuery("#editComponent").css("display", "none");
				 jQuery("#compDetails").css("display", "none");
			}
			});
		
		jQuery("#editComponent #rightCancel").click(function(){
			var curDiv  = jQuery("#currentDiv").val();
			compNum = curDiv.split("_");
			var curVisibilityDiv = "visibility"+compNum[1];
			var visibility = jQuery("#"+curVisibilityDiv).val();
			setPrevHeading(compNum);
			setPrevLabel(compNum);
			setPrevDescription(compNum);
			jQuery("#edited").val("false");
			jQuery("#editComponent").css("display", "none");
			if((visibility != "visible") && (visibility != "") ){
				var delImgButt = "delComp_"+compNum[1];
				jQuery("#"+delImgButt).css("visibility", "hidden");
				serComponentHidden(compNum);
			}
			});
		
		jQuery("#deleteButton").live('click',function() { 			
			var r = confirm("Are you sure you want to delete?");
			if (r==true)
			{
				jQuery("#localImage").val('');
			    jQuery("#imgLogo").empty();
			    jQuery("#saveImage").val('');
			    jQuery("#deleteButton").hide();
			    jQuery("#imgLogo").append('<p id="msg">(Your logo here)</p>');
			} 
		});
		
		jQuery("#deleteButton104").live('click',function() { 
			var r = confirm(deleteAlert);
			if (r==true)
			{
				jQuery("#localImage104").val('');
				jQuery("#selectImg").val('');
				jQuery("#imgLogo").empty();
				jQuery("#saveImage104").val('');
				jQuery("#saveButton104").hide();
				jQuery("#deleteButton104").hide();
				jQuery("#imgLogo").append('<p id="msg">(Your logo here)</p>');
			}else{}	
		});
                
                jQuery("#deleteButton105").live('click',function() { 
			var r = confirm(deleteAlert);
			if (r==true)
			{
				jQuery("#localImage105").val('');
				jQuery("#selectImg").val('');
				jQuery("#imgLogo").empty();
				jQuery("#saveImage105").val('');
				jQuery("#saveButton105").hide();
				jQuery("#deleteButton105").hide();
				jQuery("#imgLogo").append('<p id="msg">(Your logo here)</p>');
			}else{}	
		});
		
		jQuery("#deleteButton1").live('click',function() { 			
			var r = confirm("Are you sure you want to delete?");
			if (r==true)
			{
				jQuery("#localImage").val('');
			    jQuery("#imgLogo").empty();
			    jQuery("#saveImage").val('');
			    jQuery("#deleteButton1").hide();
			    jQuery("#imgLogo").append('<p id="msg" style="margin-top: -10%;">(Your logo here)</p>');
			} 
			else{}
		}); 
			  
		jQuery("#deleteLeftButton").live('click',function() { 			
			var r = confirm("Are you sure you want to delete left logo image?");
			if (r==true)
			{
				jQuery("#localLeftLogoImage").val('');
			    jQuery("#logoLeft").empty();
			    jQuery("#saveLeftImage").val('');
			    jQuery("#deleteLeftButton").hide();
			} 
			else{}
		}); 
		jQuery("#deleteLeftButton104").live('click',function() { 			
			var r = confirm("Are you sure you want to delete left logo image?");
			if (r==true)
			{
				jQuery("#localLeftLogoImage104").val('');
			    jQuery("#logoLeft104").empty();
			    jQuery("#saveLeftImage104").val('');
			    jQuery("#deleteLeftButton104").hide();
			} 
			else{}
		});
			  
		jQuery("#deleteRightButton").live('click',function() {			
			var r = confirm("Are you sure you want to delete right logo image?");
			if (r==true)
			{
				jQuery("#localRightLogoImage").val('');
			    jQuery("#logoRight").empty();
			    jQuery("#saveRightImage").val('');
			    jQuery("#deleteRightButton").hide();
			} 
			else{}
		});
		jQuery("#deleteRightButton104").live('click',function() { 			
			var r = confirm("Are you sure you want to delete right logo image?");
			if (r==true)
			{
				jQuery("#localRightLogoImage104").val('');
			    jQuery("#logoRight104").empty();
			    jQuery("#saveRightImage104").val('');
			    jQuery("#deleteRightButton104").hide();
			} 
			else{}
		});
		/*if ($.browser.msie) {
			  $("#editOptionType").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}*/
		jQuery("#editOptionType").live("change", function(){
			jQuery("#edited").val("true");
	    	var imageOptionType = jQuery("#editOptionType").val();
	    	var imageOptionDiv = $("#optionList").find(".imageOptionType");
	    	var curDiv  = jQuery("#currentDiv").val();
			var compNum = curDiv.split("_");
			var optDiv = "optDiv"+compNum[1];
			var optDivImage = $("#"+optDiv).find(".imageOptionType");
	    	var image = "";
	    	if(imageOptionType.toLowerCase() == "square"){
	    		image = "../images/square.png";
	    	}
	    	else if(imageOptionType.toLowerCase() == "circle"){
	    		image = "../images/circle.png";
	    	}
	    	else{
	    		image = "../images/none.png";
	    	}
    		jQuery(imageOptionDiv).each(function(index){
    			var imageId = jQuery(this).attr("id");
	    		jQuery("#"+imageId).attr("src", image);	    		
	    		});
    		jQuery(optDivImage).each(function(index){
    			var imageId = jQuery(this).attr("id");
	    		jQuery("#"+imageId).attr("src", image);	    		
	    		});
    		
	});
		jQuery(".addEditOption").live("click", function(){
			jQuery("#edited").val("true");
			var addId = jQuery(this).attr("id");
			var addNum = addId.split("_");
			var componentDiv = jQuery("#currentDiv").val();
			var componentNum = componentDiv.split("_");
	        var curOptDiv = "optDiv"+componentNum[1];
	        var index = parseInt(addNum[1]);
	        var imageType = jQuery("#editOptionType").val();
	        var currValDiv = "optiontext_"+addNum[1];
	        var currVal = jQuery("#"+currValDiv).val();
	        var optionValueDiv = "optionValue"+componentNum[1];
	        var maxLength = 32;
	        var addFlag = true;
	        if((imageType.toLowerCase()) == "square" ){
	        	image = "../images/square.png";
	        }
	        else if((imageType.toLowerCase()) == "circle"){
	        	image = "../images/circle.png";
	        }
	        else{
	        	image = "../images/none.png";
	        }
	        
	        getOptionDesc(optionValueDiv);
	        var optionValue = (jQuery("#"+optionValueDiv).val()).split("^");
	        var optionValueLen = ((jQuery("#"+optionValueDiv).val()).length)+(optionValue.length * 2);
	        if(parseInt(componentNum[1]) == 15){
	        	if(optionValue.length  >= 3){
	        		addFlag = false;
	        		alert("Cannot add more than three options.");
	        	}
	        	maxLength = 96;
	        }
	        if(currVal.length == 0){
	        	var lastOptDiv = "add_"+(index);
	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
	        	alert("Please enter options.");
	        }
	        else if((optionValueLen < maxLength)  && (addFlag == true)){
	        	for(var i=1; i<=parseInt(addNum[1]); i++){
					var addbutt = "add_"+i;
					if(jQuery("#"+addbutt).length != 0 ){
						jQuery("#"+addbutt).removeClass("add");
						jQuery("#"+addbutt).css("display", "none");
						}
					}
	        	jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_'+(index+1)+'"><span class="checkbox" style="display:block;"><img src="'+image+'" class="imageOptionType" id="editImage_'+(index+1)+'">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="" style="width: 100px;" /><input type="button" name="remove_'+(index+1)+'" value="" id="remove_'+(index+1)+'" class="deleteEditOption"/><input type="button" name="add_'+(index+1)+'" value="" id="add_'+(index+1)+'" class="addEditOption" /></span></div>');
	        	if(componentNum[1] != 15){
		  	    	jQuery("#"+curOptDiv).append('<div class="checkbox" ><span><img src="'+image+'" class="imageOptionType" id="image_'+componentNum[1]+""+(index+1)+'"><label id="opt'+componentNum[1]+""+(index+1)+'" class="optionText"></label></span></div>');
		  	    }
		  	    else{
		  	    	jQuery("#"+curOptDiv).append('<div class="checkbox15" ><span><img src="'+image+'" class="imageOptionType" id="image_'+componentNum[1]+""+(index+1)+'"><label id="opt'+componentNum[1]+""+(index+1)+'" class="optionText"></label></span></div>');
			  	    }
	        	var lastOptDiv = "add_"+(index+1);
	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
	        }
	        else if(optionValueLen >= maxLength){
	        	var lastOptDiv = "add_"+(index);
	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
	        	alert("Cannot add more.");
	        }
		});
		
		jQuery(".deleteEditOption").live("click", function(){
			jQuery("#edited").val("true");
			var delId = jQuery(this).attr("id");
			var cuurOption = delId.split("_");
			var cuurOptionnum = parseInt(cuurOption[1]);
			var componentDiv = jQuery("#currentDiv").attr("value");
			var componentNum = componentDiv.split("_");
			var imageDiv = "image_"+componentNum[1]+""+cuurOption[1];
			var optDiv = "opt"+componentNum[1]+""+cuurOption[1];
			var removeDiv = "checkboxcnt_"+cuurOptionnum;
			var lastoptDiv = jQuery("#lastEditOptionDiv").val();
			var lastDiVNum = lastoptDiv.split("_"); 
			if(cuurOptionnum == parseInt(lastDiVNum[1])){
				var adddiv = getLastDiv(lastDiVNum[1]);
				jQuery("#"+adddiv).val("");
				jQuery("#"+adddiv).css("display", "block");
				jQuery("#"+adddiv).css("width", "16px");
				jQuery("#"+adddiv).css("float", "right");
				jQuery("#"+adddiv).css("margin-right", "160px");
				jQuery("#"+adddiv).css("margin-top", "4px");
				jQuery("#lastEditOptionDiv").val(adddiv);
				}
			jQuery("#"+imageDiv).remove();
			jQuery("#"+optDiv).remove();
			jQuery("#"+removeDiv).remove();
			var optionList = jQuery("#optionList").find(".optiontext");
			if(optionList.length < 1){
				jQuery("#editDesc").append('<div id="addOptionDiv"><div style="float:left;"><div id="createNewOptionDiv" class="sideheading"><label>Add Option</label>&nbsp;&nbsp<img src="../images/Button-Add-icon.png" id="createNewOption" /></div></div></div>');
			}
		});
		
		//Edit Detail
		jQuery(".editDetailHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			jQuery("#editComp_"+selectNum[1]+"_"+1).live("cut copy paste",function(e) {
			      e.preventDefault();
			});
			jQuery("#editComp_"+selectNum[1]+"_"+2).live("cut copy paste",function(e) {
			      e.preventDefault();
			});
			jQuery("#editComp_"+selectNum[1]+"_"+1).css("display","block");
			jQuery("#editComp_"+selectNum[1]+"_"+2).css("display","block");
			jQuery("#cust1").hide(); 
			jQuery("#cust2").hide(); 
			jQuery(".detailTextLbl").hide();
			jQuery("#Component_"+selectNum[1]).hide();
			jQuery("#okDetailHeading_"+selectNum[1]).show();
			jQuery("#editDetailHeading_"+selectNum[1]).hide();
			jQuery("#editComp_"+selectNum[1]+"_"+1).focus();
		});
		
		jQuery(".okDetailHeading").click(function(){
			okDetailHeading103(this);
		});
		
		/*jQuery(".detailTbox").focusout(function(){
			okDetailHeading103(this);
		});
		
		jQuery(".detailTbox").keydown(function(e){	
			if(e.keyCode == 13){
				okDetailHeading103(this);
			}
		});*/
		
		jQuery(".focusOutSaveDetail").keydown(function(e){
			if(e.keyCode == 13){
				var tb_val = $(".detailCmpt").val();
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				var edit = "editDetailHeading_"+selectNum[1];
				var ok = "okDetailHeading_"+selectNum[1];
				var data = jQuery("#Component_"+selectNum[1]).val();
				jQuery("#detailText_"+selectNum[1]).html(tb_val);
				jQuery("#detailText_"+selectNum[1]).show();
				jQuery("#Component_"+selectNum[1]).hide();			
				jQuery("#"+ok).hide();
				jQuery("#"+edit).show();
			}
		});
		
		jQuery(".focusOutSaveDetail").focusout(function(){
			var tb_val = $(".detailCmpt").val();
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			var edit = "editDetailHeading_"+selectNum[1];
			var ok = "okDetailHeading_"+selectNum[1];
			var data = jQuery("#Component_"+selectNum[1]).val();
			jQuery("#detailText_"+selectNum[1]).html(tb_val);
			jQuery("#detailText_"+selectNum[1]).show();
			jQuery("#Component_"+selectNum[1]).hide();			
			jQuery("#"+ok).hide();
			jQuery("#"+edit).show();
		});
		
		//Edit Top
		jQuery(".editTopHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("TopHeading","topText",selectNum);
		});
		
		jQuery(".focusOutSaveTop").keydown(function(e){
			if(e.keydown == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					alert(headingAlert);
					$(".focusOutSaveTop").focus();
				}
				else {
					okTopCmpt("TopHeading","topText",selectNum);			
				}
			}
		});
		jQuery(".focusOutSaveTop").focusout(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
				$(".focusOutSaveTop").focus();
			}
			else {
				okTopCmpt("TopHeading","topText",selectNum);			
			}
		});
		
		jQuery(".okTopHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
				jQuery(".focusOutSaveTop").focus();
			}
			else {
				okTopCmpt("TopHeading","topText",selectNum);			
			}
		});
		 
		//Edit Top Left Heading
			
		jQuery(".editTopLeftHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("TopLeftHeading","topLeft",selectNum);
		});
		
		jQuery(".okTopLeftHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
			}
			else {
				okTopCmpt("TopLeftHeading","topLeft",selectNum);
			}
			jQuery(".focusOutSaveLeftHeading").focus();
		});
		
		jQuery(".focusOutSaveLeftHeading").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					alert(headingAlert);
				}
				else {
					okTopCmpt("TopLeftHeading","topLeft",selectNum);
				}
				jQuery(".focusOutSaveLeftHeading").focus();
			}
		});
		
		jQuery(".focusOutSaveLeftHeading").focusout(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
			}
			else {
				okTopCmpt("TopLeftHeading","topLeft",selectNum);
			}
			jQuery(".focusOutSaveLeftHeading").focus();
		});
		//Edit Top Left Component
		jQuery(".deleteTopLeft").click(function(){
			var r = confirm(deleteAlert);
			if (r==true)
			{
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				deleteComp("TopLeft","topLeft",selectNum);
			}
			else{}
		});
		
		jQuery(".editTopLeft").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			
			editCmpt("TopLeft","topLeft",selectNum);
		});
		
		jQuery(".okTopLeft").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			okCmpt("TopLeft","topLeft",selectNum);
		});
		
		jQuery(".focusOutSaveLeft").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				okCmpt("TopLeft","topLeft",selectNum);
			}
		});
		
		jQuery(".focusOutSaveLeft").focusout(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			okCmpt("TopLeft","topLeft",selectNum);
		});
		
		//Edit Top Right Heading
		jQuery(".editTopRightHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("TopRightHeading","topRight",selectNum);
		});
		
		jQuery(".okTopRightHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
			}
			else {
				okTopCmpt("TopRightHeading","topRight",selectNum);
			}
			jQuery(".focusOutSaveRightHeading").focus();
		});
		
		jQuery(".focusOutSaveRightHeading").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					alert(headingAlert);
				}
				else {
					okTopCmpt("TopRightHeading","topRight",selectNum);
				}
				jQuery(".focusOutSaveRightHeading").focus();
			}
		});
		
		jQuery(".focusOutSaveRightHeading").focusout(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
			}
			else {
				okTopCmpt("TopRightHeading","topRight",selectNum);
			}
			jQuery(".focusOutSaveRightHeading").focus();
		});
		
		$(document).click(function(){
			jQuery("#editText103").focus();
			jQuery(".focusOutSaveTop").focus();
			jQuery(".focusOutSaveLeftHeading").focus();
			jQuery(".focusOutSaveRightHeading").focus();
			jQuery(".focusOutSaveBLeftHeading").focus();
       });
		
		//Edit Top Right Component
		jQuery(".deleteTopRight").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			var r = confirm(deleteAlert);
			if (r==true)
			{
				deleteComp("TopRight","topRight",selectNum);
			}
			else{}
		});
		
		jQuery(".editTopRight").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCmpt("TopRight","topRight",selectNum);
		});
		
		jQuery(".focusOutSaveRight").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				okCmpt("TopRight","topRight",selectNum);
			}
		});
		
		jQuery(".focusOutSaveRight").focusout(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			okCmpt("TopRight","topRight",selectNum);
		});
		
		jQuery(".okTopRight").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			okCmpt("TopRight","topRight",selectNum);
		});
		
		//Edit Bottom Left Heading
		jQuery(".editBottomLeftHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("BottomLeftHeading","bottomLeft",selectNum);
		});
		
		jQuery(".okBottomLeftHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
			}
			else {
				okTopCmpt("BottomLeftHeading","bottomLeft",selectNum);	
			}
			jQuery(".focusOutSaveBLeftHeading").focus();
		});
		
		jQuery(".focusOutSaveBLeftHeading").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					alert(headingAlert);
				}
				else {
					okTopCmpt("BottomLeftHeading","bottomLeft",selectNum);	
				}
				jQuery(".focusOutSaveBLeftHeading").focus();
			}
		});	
		
		jQuery(".focusOutSaveBLeftHeading").focusout(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			if(jQuery("#Component_"+selectNum[1]).val()==""){
				alert(headingAlert);
			}
			else {
				okTopCmpt("BottomLeftHeading","bottomLeft",selectNum);	
			}
			jQuery(".focusOutSaveBLeftHeading").focus();
		});
		
		//Edit Bottom Left Component
		jQuery(".deleteBottomLeft").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			var r = confirm(deleteAlert);
			if (r==true)
			{
				deleteComp("BottomLeft","bottomLeft",selectNum);
			}
			else{}
		});
		
		jQuery(".editBottomLeft").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCmpt("BottomLeft","bottomLeft",selectNum);
		});
		
		jQuery(".focusOutSaveBLeft").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				okCmpt("BottomLeft","bottomLeft",selectNum);
			}
		});
		
		jQuery(".focusOutSaveBLeft").focusout(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			okCmpt("BottomLeft","bottomLeft",selectNum);
		});
		
		jQuery(".okBottomLeft").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			okCmpt("BottomLeft","bottomLeft",selectNum);	
		});
		
		//Edit heading
		jQuery("#editHeading103").click(function(){
			jQuery("#editText103").show();
			jQuery("#headerText").hide();
			jQuery("#editHeading103").hide();
			jQuery("#okHeading103").show();
		});
		
		jQuery("#editText103").keydown(function(e){
			if(e.keyCode == 13){
				if(jQuery("#editText103").val()==""){
					//alert(headingAlert);
					//jQuery("#editText103").focusout();
				}
				else {
					var data = jQuery("#editText103").val();
					jQuery("#headerText").html(data);
					jQuery("#headingText").val(data);
					jQuery("#headerText").show();
					jQuery("#editText103").hide();
					jQuery("#okHeading103").hide();
					jQuery("#editHeading103").show();
				}
			}
		});
		
		jQuery("#editText103").focusout(function(){
			if(jQuery("#editText103").val()==""){
				alert(headingAlert);
				jQuery("#editText103").focusin();
			}
			else {
				var data = jQuery("#editText103").val();
				jQuery("#headerText").html(data);
				jQuery("#headingText").val(data);
				jQuery("#headerText").show();
				jQuery("#editText103").hide();
				jQuery("#okHeading103").hide();
				jQuery("#editHeading103").show();
			}
		});
		
		jQuery("#okHeading103").click(function(){
			if(jQuery("#editText103").val()==""){
				alert(headingAlert);
				jQuery("#editText103").focusin();
			}
			else {
				var data = jQuery("#editText103").val();
				jQuery("#headerText").html(data);
				jQuery("#headingText").val(data);
				jQuery("#headerText").show();
				jQuery("#editText103").hide();
				jQuery("#okHeading103").hide();
				jQuery("#editHeading103").show();
			}
		});
	
		//Add Local Images In all Places
		$(document).keypress(function(e) {
		    if(e.keyCode == 13  && $("#localImage").val()=="") {
		        return false;
		    }
		}); 
		if ($.browser.msie) {
			  $("#localImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		var isSmall;
		jQuery('#localImage').live('change', function(){ 
			jQuery("#selectImg").val("");
			jQuery("#saveImage").val("");
			if(jQuery('#localImage').val() != ""){
				
				if(jQuery("#selectImg").val()==""||jQuery("#selectImg").val()==null){
					jQuery("#deleteButton1").hide();
				}
				jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				if(jQuery("#localImage").val()!=""||jQuery("#localImage").val()!=null){
					if(jQuery("#saveImage").val()==""||jQuery("#saveImage").val()== null){
						jQuery("#delBtn").html('<input type="button" value="Delete" id="deleteButton"/>');
					}
					else {
						jQuery("#deleteButton").show();
					}
				}
				else {
					jQuery("#saveBtn").hide();
					
				}
				
				if(document.getElementById("localImage").value != ""){
					var type=$("#center").val();
					var isSmall;
					var imageHeight= $("#centerimageHeight").val();
					var imageWidth= $("#centerimageWidth").val();
					$("#uploadLocalImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
						jQuery("#uploadLocalImage").ajaxForm({
							target: '#imgLogo',
							success: function(){
								isSmall=jQuery("#smallImg").val();
								},
							complete: function(){
											if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
												jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
												var stretch;
												var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
												var response = confirm(msg);
												if(response){
													stretch="yes";
													
												}else{
													stretch="no";
												}
												$("#uploadLocalImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
												jQuery("#uploadLocalImage").ajaxForm({
													target: '#imgLogo',
													success: function(){
														},
													complete: function(){
																jQuery("#smallImg").val("");
															  }
										           }).submit();
											}
									  }
				           }).submit();
				}
				
			}
		});
		if ($.browser.msie) {
			  $("#localImage104").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
                        
                        if ($.browser.msie) {
			  $("#localImage105").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
                        
		jQuery('#localImage104').live('change', function(){ 
			jQuery("#selectImg").val("");
			jQuery("#saveImage").val("");
			if(jQuery('#localImage104').val() != ""){
				
				if(jQuery("#selectImg").val()==""||jQuery("#selectImg").val()==null){
					jQuery("#deleteButton1").hide();
				}
				jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				if(jQuery("#localImage104").val()!=""||jQuery("#localImage104").val()!=null){
					if(jQuery("#saveImage104").val()==""||jQuery("#saveImage104").val()== null){
						jQuery("#delBtn104").html('<input type="button" value="Delete" id="deleteButton104"/>');
					}
					else {
						jQuery("#deleteButton104").show();
					}
				}
				else {
					jQuery("#saveBtn104").hide();
					
				}
				
				if(document.getElementById("localImage104").value != ""){
					var type=$("#center104").val();
					var imageHeight= $("#centerimageHeight104").val();
					var imageWidth= $("#centerimageWidth104").val();
					$("#uploadLocalImage104").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
						jQuery("#uploadLocalImage104").ajaxForm({
							target: '#imgLogo',
							success: function(){
							isSmall=jQuery("#smallImg").val();
							},
							complete: function(){
										if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
											jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
											var stretch;
											var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
											var response = confirm(msg);
											if(response){
												stretch="yes";
												
											}else{
												stretch="no";
											}
											$("#uploadLocalImage104").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
											jQuery("#uploadLocalImage104").ajaxForm({
												target: '#imgLogo',
												success: function(){
													},
												complete: function(){
															jQuery("#smallImg").val("");
														  }
									           }).submit();
										}
								  }
				           }).submit();
				}
				
			}
		});
                
                
                jQuery('#localImage105').live('change', function(){ 
			jQuery("#selectImg").val("");
			jQuery("#saveImage").val("");
			if(jQuery('#localImage105').val() != ""){
				
				if(jQuery("#selectImg").val()==""||jQuery("#selectImg").val()==null){
					jQuery("#deleteButton1").hide();
				}
				jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				if(jQuery("#localImage105").val()!=""||jQuery("#localImage105").val()!=null){
					if(jQuery("#saveImage105").val()==""||jQuery("#saveImage105").val()== null){
						jQuery("#delBtn105").html('<input type="button" value="Delete" id="deleteButton105"/>');
					}
					else {
						jQuery("#deleteButton105").show();
					}
				}
				else {
					jQuery("#saveBtn105").hide();
					
				}
				
				if(document.getElementById("localImage105").value != ""){
					var type=$("#center105").val();
					var imageHeight= $("#centerimageHeight105").val();
					var imageWidth= $("#centerimageWidth105").val();
					$("#uploadLocalImage105").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
						jQuery("#uploadLocalImage105").ajaxForm({
							target: '#imgLogo',
							success: function(){
							isSmall=jQuery("#smallImg").val();
							},
							complete: function(){
										if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
											jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
											var stretch;
											var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
											var response = confirm(msg);
											if(response){
												stretch="yes";
												
											}else{
												stretch="no";
											}
											$("#uploadLocalImage105").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
											jQuery("#uploadLocalImage105").ajaxForm({
												target: '#imgLogo',
												success: function(){
													},
												complete: function(){
															jQuery("#smallImg").val("");
														  }
									           }).submit();
										}
								  }
				           }).submit();
				}
				
			}
		});
                
                
		if ($.browser.msie) {
			  $("#localLeftLogoImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#localLeftLogoImage').live('change', function(){
			if(jQuery("#localLeftLogoImage").val()!=""){
			jQuery("#logoLeft").html('<img src="../images/loader.gif" alt="Uploading...." style="width: 96px; height: 20px;"/>');
			}
			if(jQuery("#localLeftLogoImage").val()!="" || jQuery("#localLeftLogoImage").val()!=null){
				if(jQuery("#saveLeftImage").val()=="" || jQuery("#saveLeftImage").val()== null){					
					jQuery("#delBtnLeft").html('<input type="button" value="Delete" id="deleteLeftButton"/>');
				}
				else {
					jQuery("#deleteLeftButton").show();
				}
			}
			if(jQuery("#localLeftLogoImage").val()!=""){
				var type=$("#leftLogo").val();
				var imageHeight= $("#leftLogoimageHeight").val();
				var imageWidth= $("#leftLogoimageWidth").val();
				 $("#uploadLeftLogoLocalImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				jQuery("#uploadLeftLogoLocalImage").ajaxForm({
					target: '#logoLeft',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#logoLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadLeftLogoLocalImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadLeftLogoLocalImage").ajaxForm({
										target: '#logoLeft',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#localLeftLogoImage104").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#localLeftLogoImage104').live('change', function(){
			if(jQuery("#localLeftLogoImage104").val()!=""){
			jQuery("#logoLeft104").html('<img src="../images/loader.gif" alt="Uploading...." style="width: 96px; height: 20px;"/>');
			}
			if(jQuery("#localLeftLogoImage104").val()!="" || jQuery("#localLeftLogoImage104").val()!=null){
				if(jQuery("#saveLeftImage104").val()=="" || jQuery("#saveLeftImage104").val()== null){					
					jQuery("#delBtnLeft104").html('<input type="button" value="Delete" id="deleteLeftButton104"/>');
				}
				else {
					jQuery("#deleteLeftButton104").show();
				}
			}
			if(jQuery("#localLeftLogoImage104").val()!=""){
				var type=$("#leftLogo104").val();
				var imageHeight= $("#leftLogoimageHeight104").val();
				var imageWidth= $("#leftLogoimageWidth104").val();
				 $("#uploadLeftLogoLocalImage104").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				jQuery("#uploadLeftLogoLocalImage104").ajaxForm({
					target: '#logoLeft104',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#logoLeft104").html('<img src="../images/loader.gif" alt="Uploading...." style="width: 96px; height: 20px;"/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadLeftLogoLocalImage104").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadLeftLogoLocalImage104").ajaxForm({
										target: '#logoLeft104',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#localRightLogoImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#localRightLogoImage').live('change', function(){ 
			if(jQuery("#localRightLogoImage").val()!=""){
			jQuery("#logoRight").html('<img src="../images/loader.gif" alt="Uploading...." style="width: 96px; height: 20px;"/>');
			}
			if(jQuery("#localRightLogoImage").val()!="" || jQuery("#localRightLogoImage").val()!=null){
				if(jQuery("#saveRightImage").val()=="" || jQuery("#saveRightImage").val()== null){
					jQuery("#delBtnRight").html('<input type="button" value="Delete" id="deleteRightButton"/>');
				}
				else {
					jQuery("#deleteRightButton").show();
				}	
			}
			if(jQuery("#localRightLogoImage").val()!=""){
				var type=$("#rightLogo").val();
				var imageHeight= $("#rightLogoimageHeight").val();
				var imageWidth= $("#rightLogoimageWidth").val();
				 $("#uploadRightLogoLocalImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				jQuery("#uploadRightLogoLocalImage").ajaxForm({
					target: '#logoRight',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#logoRight").html('<img src="../images/loader.gif" alt="Uploading...." style="width: 96px; height: 20px;"/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadRightLogoLocalImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadRightLogoLocalImage").ajaxForm({
										target: '#logoRight',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#localRightLogoImage104").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#localRightLogoImage104').live('change', function(){ 
			if(jQuery("#localRightLogoImage104").val()!=""){
			jQuery("#logoRight104").html('<img src="../images/loader.gif" alt="Uploading...." style="width: 96px; height: 20px;"/>');
			}
			if(jQuery("#localRightLogoImage104").val()!="" || jQuery("#localRightLogoImage104").val()!=null){
				if(jQuery("#saveRightImage104").val()=="" || jQuery("#saveRightImage104").val()== null){
					jQuery("#delBtnRight104").html('<input type="button" value="Delete" id="deleteRightButton104"/>');
				}
				else {
					jQuery("#deleteRightButton104").show();
				}	
			}
			if(jQuery("#localRightLogoImage104").val()!=""){
				var type=$("#rightLogo104").val();
				var imageHeight= $("#rightLogoimageHeight104").val();
				var imageWidth= $("#rightLogoimageWidth104").val();
				 $("#uploadRightLogoLocalImage104").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				jQuery("#uploadRightLogoLocalImage104").ajaxForm({
					target: '#logoRight104',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#logoRight104").html('<img src="../images/loader.gif" alt="Uploading...." style="width: 96px; height: 20px;"/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadRightLogoLocalImage104").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadRightLogoLocalImage104").ajaxForm({
										target: '#logoRight104',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		
		if ($.browser.msie) {
			  $("#leftImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#leftImage').live('change', function(){ 
			if(jQuery('#leftImage').val() != ""){
				jQuery("#imgLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=$("#left").val();
				var imageHeight= $("#leftimageHeight").val();
				var imageWidth= $("#leftimageWidth").val();
				 $("#uploadLeftImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				 
				jQuery("#uploadLeftImage").ajaxForm({
					target: '#imgLeft',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#imgLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadLeftImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadLeftImage").ajaxForm({
										target: '#imgLeft',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomLeftImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomLeftImage').live('change', function(){ 
			if(jQuery('#bottomLeftImage').val() != ""){
				jQuery("#imgBottomLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=$("#bottomLeft").val();
				var imageHeight= $("#bottomLeftimageHeight").val();
				var imageWidth= $("#bottomLeftimageWidth").val();
				
				 $("#uploadBottomLeftImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				 
				jQuery("#uploadBottomLeftImage").ajaxForm({
					target: '#imgBottomLeft',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#imgBottomLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadBottomLeftImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadBottomLeftImage").ajaxForm({
										target: '#imgBottomLeft',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomRightImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomRightImage').live('change', function(){ 
			if(jQuery('#bottomRightImage').val() != ""){
				jQuery("#imgBottomRight").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=$("#bottomRight").val();
				var imageHeight= $("#bottomRightimageHeight").val();
				var imageWidth= $("#bottomRightimageWidth").val();
				 $("#uploadBottomRightImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				 
				jQuery("#uploadBottomRightImage").ajaxForm({
					target: '#imgBottomRight',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#imgBottomRight").html('<img src="../images/loader.gif" alt="Uploading...."/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadBottomRightImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadBottomRightImage").ajaxForm({
										target: '#imgBottomRight',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomLastImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomLastImage').live('change', function(){ 
			if(jQuery('#bottomLastImage').val() != ""){
				jQuery("#imgBottomLast").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=$("#bottomLast").val();
				var imageHeight= $("#bottomLastimageHeight").val();
				var imageWidth= $("#bottomLastimageWidth").val();
				 $("#uploadBottomLastImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				
				jQuery("#uploadBottomLastImage").ajaxForm({
					target: '#imgBottomLast',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#imgBottomLast").html('<img src="../images/loader.gif" alt="Uploading...."/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadBottomLastImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadBottomLastImage").ajaxForm({
										target: '#imgBottomLast',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomMidImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomMidImage').live('change', function(){ 
			if(jQuery('#bottomMidImage').val() != ""){
				jQuery("#imgBottomMid").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=$("#bottomMid").val();
				var imageHeight= $("#bottomMidimageHeight").val();
				var imageWidth= $("#bottomMidimageWidth").val();
				 $("#uploadBottomMidImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				
				jQuery("#uploadBottomMidImage").ajaxForm({
					target: '#imgBottomMid',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#imgBottomMid").html('<img src="../images/loader.gif" alt="Uploading...."/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadBottomMidImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadBottomMidImage").ajaxForm({
										target: '#imgBottomMid',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomCenterImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomCenterImage').live('change', function(){ 
			if(jQuery('#bottomCenterImage').val() != ""){
				jQuery("#imgBottomCenter").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=$("#bottomCenter").val();
				var imageHeight= $("#bottomCenterimageHeight").val();
				var imageWidth= $("#bottomCenterimageWidth").val();
				 $("#uploadBottomCenterImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				
				jQuery("#uploadBottomCenterImage").ajaxForm({
					target: '#imgBottomCenter',
					success: function(){
					isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
								if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
									jQuery("#imgBottomCenter").html('<img src="../images/loader.gif" alt="Uploading...."/>');
									var stretch;
									var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
									var response = confirm(msg);
									if(response){
										stretch="yes";
										
									}else{
										stretch="no";
									}
									$("#uploadBottomCenterImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch="+stretch);
									jQuery("#uploadBottomCenterImage").ajaxForm({
										target: '#imgBottomCenter',
										success: function(){
											
											},
										complete: function(){
													jQuery("#smallImg").val("");
												  }
							           }).submit();
								}
						  }
		           }).submit();
			}
		});
		
		//Add Images From Repository
		jQuery('#browse').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRImage").ajaxForm({
				target: '#lbox'
	           }).submit();
		});
		jQuery('#browse104').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRImage104").ajaxForm({
				target: '#lbox104'
	           }).submit();
		});
                
                jQuery('#browse105').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRImage105").ajaxForm({
				target: '#lbox105'
	           }).submit();
		});
		
		jQuery('#browseLeftLogo').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRLeftLogoImage").ajaxForm({
				target: '#leftLogobox'
	           }).submit();
		});
		jQuery('#browseLeftLogo104').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRLeftLogoImage104").ajaxForm({
				target: '#leftLogobox104'
	           }).submit();
		});
		
		jQuery('#browseRightLogo').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRRightLogoImage").ajaxForm({
				target: '#rightLogobox'
	           }).submit();
		});
		
		jQuery('#browseRightLogo104').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRRightLogoImage104").ajaxForm({
				target: '#rightLogobox104'
	           }).submit();
		});
		
		jQuery('#browseLeft').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRLeftImage").ajaxForm({
				target: '#leftBox'
	           }).submit();
		});
		
		jQuery('#browseBottomLeft').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomLeftImage").ajaxForm({
				target: '#bottomLeftBox'
	           }).submit();
		});
		
		jQuery('#browseBottomRight').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomRightImage").ajaxForm({
				target: '#bottomRightBox'
	           }).submit();
		});
		
		jQuery('#browseBottomLast').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomLastImage").ajaxForm({
				target: '#bottomLastBox'
	           }).submit();
		});
		
		jQuery('#browseBottomMid').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomMidImage").ajaxForm({
				target: '#bottomMidBox'
	           }).submit();
		});
		
		jQuery('#browseBottomCenter').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomCenterImage").ajaxForm({
				target: '#bottomCenterBox'
	           }).submit();
		});
		
		jQuery('#browseRepository').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRepositoryImage").ajaxForm({
				target: '#repositoryBox'
	           }).submit();
		});
		
		jQuery("#createNewOption").live('click',function(){
			var curDiv = jQuery("#currentDiv").attr("value");
			var compNum = curDiv.split("_");
			var imageType = (jQuery("#editOptionType option:selected").text()).toLowerCase();
			jQuery("#optionList").empty();
			jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_1"><span class="checkbox" style="display:block;"><img src="../images/'+imageType+'.png" class="imageOptionType" id="image1">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_1" value="" style="width: 100px;" onkeyup="editOtions(this.id)"/><input type="button" name="remove_1" value="" id="remove_1" class="deleteEditOption"/><input type="button" name="add_1" value="" id="add_1" class="addEditOption"/></span></div>');
			setOptionDrpListRight(compNum);
			jQuery("#addOptionDiv").remove();
		});
		
	});

	function displayHideBox(boxNumber)
	{
		if(document.getElementById("LightBox"+boxNumber).style.display=="none") {
			document.getElementById("LightBox"+boxNumber).style.display="block";
			document.getElementById("grayBG").style.display="block";
		} else {
			document.getElementById("LightBox"+boxNumber).style.display="none";
			document.getElementById("grayBG").style.display="none";
		}
	}
	
	function editHeadingLabel(id){
		jQuery("#edited").val("true");
		var curDiv = jQuery("#currentDiv").attr("value");
		var compNum = curDiv.split("_");
		var reflectHeading = "headingSpan"+compNum[1]; 
		jQuery("#"+reflectHeading).text(jQuery("#"+id).val());		
	}

	function editLabel(id){
			jQuery("#edited").val("true");
			var curDiv = jQuery("#currentDiv").attr("value");
			var compNum = curDiv.split("_");
			var reflectLabel = "labelSpan"+compNum[1]; 
			jQuery("#"+reflectLabel).text(jQuery("#"+id).val());
		}
	function changeOPtion(id){
		jQuery("#edited").val("true");
		var selectOpt = jQuery("#"+id).attr("value");
		jQuery.ajax({
			url :"./getOptions.do",
			type :"POST",
			dataType : "html",
			data :"optId="+selectOpt,
			success : function(data) {
				jQuery("#optionList").empty();
				jQuery("#editDesc").empty();
				jQuery("#editDesc").html(data);
			},
			complete: function(xData, status){
				jQuery("#editOptionType").val((jQuery("#selectedType").val()));
				var curDiv = jQuery("#currentDiv").attr("value");
				var compNum = curDiv.split("_");
				setOptionDrpListRight(compNum);
			},
			error : function(xmlhttp, error_msg) {
			}
			});
		}

	//function editOtions(id){
	jQuery(".optiontext").live("keyup",function(e){
		var id = jQuery(this).attr("id");
		jQuery("#edited").val("true");
		
		var maxLength = 40;
		var optionList = jQuery("#optionList").find(".optiontext");
		var optionString = "";
		var curDiv = jQuery("#currentDiv").attr("value");
		var compNum = curDiv.split("_");
		if(parseInt(compNum[1]) == 15){
			optionString = jQuery("#"+id).val();
		}
		else{
			jQuery(optionList).each(function(index) {
	    	    var curOptTextId = jQuery(this).attr("id");
	    	    var curOptText = jQuery("#"+curOptTextId).val();
	    	    optionString = optionString+"^"+curOptText;
	        });
		}

		// if it exceeds than the max range.
		var optionStringLen = optionString.length + ((optionString.split("^")).length * 2);
		if(optionStringLen > parseInt(maxLength)){
        	if(parseInt(compNum[1]) == 15){
            	var curOptTextLen = (jQuery("#"+id).val()).length;
        		jQuery("#"+id).attr("maxlength", parseInt(curOptTextLen));
    		}
        	else{
        	jQuery(optionList).each(function(index) {
        	    var curOptTextId = jQuery(this).attr("id");
        	    var curOptText = jQuery("#"+curOptTextId).val();
        	    var curOptTextLen = curOptText.length;
        	    jQuery("#"+curOptTextId).attr("maxlength", parseInt(curOptTextLen));
            	});
        	}
        	if((e.keyCode != 8) && (e.keyCode != 13) && (e.keyCode != 46)){
        		var currText = jQuery("#"+id).val();
        		var len = currText.length;
        		var removeChar = optionStringLen - parseInt(maxLength);
        		var optNum = id.split("_");
        		var reflectOpt = "opt"+compNum[1]+""+optNum[1];
        		jQuery("#"+id).val(currText.substring(0, len-removeChar));
        		jQuery("#"+reflectOpt).text(jQuery("#"+id).val());
        		//alert("The maximum length for options is "+((parseInt(maxLength))-((optionString.split("^")).length * 2))+".");
        		alert("You have reached the maximum character limit.");
        	}
            }
        else{
        jQuery(optionList).each(function(index) {
    	    var curOptTextId = jQuery(this).attr("id");
    	    jQuery("#"+curOptTextId).removeAttr("maxlength");
        });
		var curOptId = id;
		var optNum = curOptId.split("_");
		var reflectOpt = "opt"+compNum[1]+""+optNum[1];
		jQuery("#"+reflectOpt).text(jQuery("#"+curOptId).val());
        }
		
	});
		//}
	
    function inActiveComponent(preDiv){
    	var prevImg1 = jQuery("#"+preDiv).children().attr("id");
    	var prevImg2 = jQuery("#"+prevImg1).next().attr("id");
    	jQuery("#"+preDiv).css("background-color", "#FFFFFF");
        }
    function activeComponent(componentDiv){
    	var curImg1 = jQuery("#"+componentDiv).children().attr("id");
    	var curImg2 = jQuery("#"+curImg1).next().attr("id");
    	jQuery("#"+componentDiv).css("background-color", "#EEEEEE");
        }
    function setComponents(componentDiv){
    	jQuery("#currentDiv").val(componentDiv);
    	jQuery("#prevDiv").val(componentDiv );
        }
	function showOnTemplate(componentNum){
		var userFidDiv = "templateComponent"+componentNum[1];
		jQuery("#showPosition").text(componentNum[1]);
		}
    function setHeadingDrp(componentNum){
    	var userFidDiv = "viewComponent"+componentNum[1];
    	var userFid = jQuery("#"+userFidDiv).attr("value");
    	jQuery("#CompDrp").val(userFid);
    	jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
        }
    function setLabelDisplay(componentNum){
    	var reflectLabel = "labelSpan"+componentNum[1];
    	if(jQuery("#"+reflectLabel).length != 0){
    		var reflectLabelVal = jQuery("#"+reflectLabel).text();
        	jQuery("#editLabel").val(reflectLabelVal);
    		jQuery("#editLabel").removeAttr('readonly','readonly');
    		jQuery("#editLabel").css("background-color","#FFFFFF");
    	}
    	else{
    		jQuery("#editLabel").val("");
    		jQuery("#editLabel").attr('readonly','readonly');
    		jQuery("#editLabel").css("background-color","#8A929A");
    	}
        }
	 
    function setOptionDescription(componentNum){
    	var optionValueDiv = "optionValue"+componentNum[1];
        var optionValue = jQuery("#"+optionValueDiv).val();
        var optionString = ""; 
        var optDiv = "optDiv"+componentNum[1];
        var allOption = jQuery("#"+optDiv).find('.optionText');
        var optTypeDiv = "optType"+componentNum[1];
        if(jQuery("#"+optTypeDiv).length != 0){
        var imageOptionType = jQuery("#"+optTypeDiv).val();
        var image = "";
    	if(imageOptionType.toLowerCase() == "square"){
    		image = "../images/square.png";
    	}
    	else if(imageOptionType.toLowerCase() == "circle"){
    		image = "../images/circle.png";
    	}
    	else{
    		image = "../images/none.png";
    	}
    	
        jQuery("#optionList").empty();
        if(allOption.length < 1){
        	var curDiv = jQuery("#currentDiv").attr("value");
			var compNum = curDiv.split("_");
        	var imageType = (jQuery("#editOptionType option:selected").text()).toLowerCase();
        	jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_1"><span class="checkbox" style="display:block;"><img src="../images/'+imageType+'.png" class="imageOptionType" id="image1">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_1" value="" style="width: 100px;" onkeyup="editOtions(this.id)"/><input type="button" name="remove_1" value="" id="remove_1" class="deleteEditOption"/><input type="button" name="add_1" value="" id="add_1" class="addEditOption"/></span></div>');
			setOptionDrpListRight(compNum);
        }
        jQuery(allOption).each(function(index) {
    	    var curOptText = jQuery(this).text();
    	    curOptText = curOptText.replace(/"/g, "&#34");
    	    if(optionString ==""){
    	       optionString = curOptText;
    	       }
    	    else{
    		   optionString = optionString+"^"+curOptText;
    	       }
    	    if(index != (allOption.length-1)){
	    		jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_'+(index+1)+'"><span class="checkbox" style="display:block;"><img src="'+image+'" class="imageOptionType" id="editImage_'+(index+1)+'">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="'+curOptText+'" style="width: 100px;" /><input type="button" name="remove_'+(index+1)+'" value="" id="remove_'+(index+1)+'" class="deleteEditOption"/><input type="button" name="add_'+(index+1)+'" value="" id="add_'+(index+1)+'" class="addEditOption" style="display: none;" /></span></div>');
	    	}
	    	else{
	    		jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_'+(index+1)+'"><span class="checkbox" style="display:block;"><img src="'+image+'" class="imageOptionType" id="editImage_'+(index+1)+'">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="'+curOptText+'" style="width: 100px;" /><input type="button" name="remove_'+(index+1)+'" value="" id="remove_'+(index+1)+'" class="deleteEditOption"/><input type="button" name="add_'+(index+1)+'" value="" id="add_'+(index+1)+'" class="addEditOption" /></span></div>');
	    		var lastOptDiv = "add_"+(index+1);
	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
	    	}
        });
        }
       }
    
	function getOptionDrp(componentNum){
		var viewCompId = "viewComponent"+componentNum[1];
		var subOptId = "scoptionId"+componentNum[1];
		var compId = jQuery("#"+viewCompId).val();
		var selectId = jQuery("#"+subOptId).val();
		jQuery.ajax({
		async: false,
		url :"./getOptionsDropdown.do",
		type :"POST",
		dataType : "html",
		data :"cid="+compId+"&slectOPtId="+selectId,
		success : function(data) {
			jQuery("#editOpdtionDiv").empty();
			jQuery("#editOpdtionDiv").html(data);
		},
		complete:function(xData, status){
			setOptionDescription(componentNum);
			jQuery("#editComponent").css("display", "block");
		},
		error : function(xmlhttp, error_msg) {
		}
		});
		}
	
	function setSelectedLabel(componentNum){
		var curLabel = jQuery("#editLabel").val();
		var reflectLabel = "labelSpan"+componentNum[1];
		if((componentNum[1] == 1) || (componentNum[1] == 3) || (componentNum[1] == 6) || (componentNum[1] == 13)){
			jQuery("#editLabel").attr("readonly","readonly");
			jQuery("#editLabel").css("background-color", "#8A929A");
		}
		if((curLabel != "") || (curLabel != " ")){
    		jQuery("#"+reflectLabel).text(jQuery("#editLabel").val());
		}else{
			jQuery("#"+reflectLabel).append('<div style="height:10px;"></div>');
		}
		}

	function setOptionDrpListRight(compNum){
		var optList = jQuery("#optionList").find(".optiontext");
		var curOptDiv = "optDiv"+compNum[1];
		var curOptParentDiv = "optDivParent"+compNum[1];
		var imageOptionType = jQuery("#editOptionType").val();
		var lastAdd = "add_"+(jQuery(optList).length);
		jQuery("#lastEditOptionDiv").val(lastAdd);
		var totalText = "";
		var reqlength = "";
        var image = "";
        var subCurrText = "";
    	if(imageOptionType.toLowerCase() == "square"){
    		image = "../images/square.png";
    	}
    	else if(imageOptionType.toLowerCase() == "circle"){
    		image = "../images/circle.png";
    	}
    	else{
    		image = "../images/none.png";
    	}
    	if(jQuery("#"+curOptDiv).length != 0){
    		jQuery("#"+curOptDiv).empty();
        	}
    	else if(jQuery("#"+curOptDiv).length == 0){
			var optType = jQuery("#editOptionType option:selected").text(); 
			var optionId = jQuery("#editOption").val();
			jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionValue'+compNum[1]+'" id="optionValue'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionString'+compNum[1]+'" id="optionString'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionStringUpdate'+compNum[1]+'" id="optionStringUpdate'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="scoptionId'+compNum[1]+'" id="scoptionId'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionId'+compNum[1]+'" id="optionId'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type= "hidden" id="optType'+compNum[1]+'" name="optType'+compNum[1]+'" value="'+optionId+'" />');
    		jQuery("#"+curOptParentDiv).append('<div class="checkboxcnt" id=optDiv'+compNum[1]+'></div>');
    	}
		jQuery(optList).each(function(index) {
	  	    var curOptId = jQuery(this).attr("id");
	  	    var curOptText = jQuery("#"+curOptId).val();
	  	    if(totalText.length < 50){
	  	    	reqlength = (23 - (parseInt(totalText.length)));
	  	    	totalText = totalText+"^"+curOptText;
	  	    	if(totalText.length > 23){
	  	    		if(reqlength >  (parseInt(curOptText.length))){
	  	    			reqlength = parseInt(curOptText.length);
	  	    		}
		  	    	subCurrText =  (curOptText.substring(0,reqlength));
	  	    	}
	  	    	else{
	  	    		subCurrText = curOptText;
	  	    	}
		  	    if(compNum[1] != 15){
		  	    	jQuery("#"+curOptDiv).append('<div class="checkbox"><span><img src="'+image+'" class="imageOptionType" id="image_'+compNum[1]+""+(index+1)+'"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+subCurrText+'</label></span></div>');
		  	    }
		  	    else{
		  	    	jQuery("#"+curOptDiv).append('<div class="checkbox15"><span><img src="'+image+'" class="imageOptionType" id="image_'+compNum[1]+""+(index+1)+'"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+curOptText+'</label></span></div>');
		  	    } 
	  	    }
			});
		}
	
	function setEditdHeading(compNum){
		var headingSpan = "headingSpan"+compNum[1];
		var compName = "componentName"+compNum[1];
		jQuery("#"+compName).val(jQuery("#"+headingSpan).text());
		}
	function setEditedLabel(compNum){
		var labelDiv = "label"+compNum[1];
		var labeSpanDiv ="labelSpan"+compNum[1];
		jQuery("#"+labelDiv).val(jQuery("#editLabel").val());
		}
	function setEditedDesription(compNum){
		var optionDescDiv = "optionValue"+compNum[1];
		var optionStringDiv = "optionString"+compNum[1];
		var optionListDiv = "optDiv"+compNum[1];
		var optionList = jQuery("#"+optionListDiv).find('.optionText');
		var optionString="";
        jQuery(optionList).each(function(index) {
    	    var curOptText = jQuery(this).text();
    	    if(optionString ==""){
    	       optionString = curOptText;
    	       }
    	    else{
    		   optionString = optionString+"^"+curOptText;
    	       }
        });
        jQuery("#"+optionDescDiv).val(optionString); 
        jQuery("#"+optionStringDiv).val(optionString); 
		}
	function setViewComponentId(compNum){
		var viewCompId = "viewComponent"+compNum[1];
		var subOptId = "scoptionId"+compNum[1];
		jQuery("#"+viewCompId).val(jQuery("#CompDrp").val());
		jQuery("#"+subOptId).val(jQuery("#editOption").val());
		}
	function setPrevHeading(compNum){
		var compnentNamediv = "componentName"+compNum[1];
		var headingSpan = "headingSpan"+compNum[1];
		jQuery("#"+headingSpan).text(jQuery("#"+compnentNamediv).val());
		}
	function setPrevLabel(compNum){
		var labelDiv = "label"+compNum[1];
		var labelSpanDiv = "labelSpan"+compNum[1];
		jQuery("#"+labelSpanDiv).text(jQuery("#"+labelDiv).val());
		jQuery("#editLabel").val(jQuery("#"+labelDiv).val());
		}
	function setPrevDescription(compNum){
		var curOptTypeDiv = "optType"+compNum[1];
		var curOptType = "";
		if(jQuery("#"+curOptTypeDiv).length == 0){
			curOptType = "square";
		}else{
			curOptType = jQuery("#"+curOptTypeDiv).val();
		}
    	var image = "";
    	if(curOptType.toLowerCase() == "square"){
    		image = "../images/square.png";
    	}
    	else if(curOptType.toLowerCase() == "circle"){
    		image = "../images/circle.png";
    	}
    	else{
    		image = "../images/none.png";
    	}
    	var optionValueDiv = "optionString"+compNum[1];
    	if(jQuery("#"+optionValueDiv).length != 0){
		var prevDesc = jQuery("#"+optionValueDiv).val();
		var prevDescList = prevDesc.split("^");
		var curOptDiv = "optDiv"+compNum[1];
		jQuery("#"+curOptDiv).empty();
		jQuery("#optionList").empty();
		for(var index=0; index<prevDescList.length; index++){
			if((prevDescList[index]).length != 0){
			if(compNum[1] != 15){
				jQuery("#"+curOptDiv).append('<div class="checkbox" ><span><img src="'+image+'"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+prevDescList[index]+'</label></span></div>');
	  	    }
	  	    else{
	  	    	jQuery("#"+curOptDiv).append('<div class="checkbox15"><span><img src="'+image+'" class="rectangle"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+prevDescList[index]+'</label></span></div>');
		  	    } 
			jQuery("#optionList").append('<div class="checkbox"><span class="checkbox" style="display:block;"><img src="'+image+'" class="rectangle">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="'+prevDescList[index]+'" style="width: 100px; /></span></div>');
			}
		}
    	}
	}
	function setEditOptionType(componentNum){
		var optType = "optType"+componentNum[1];
		if(jQuery("#"+optType).length != 0){
			jQuery("#editOptionType").val(jQuery("#"+optType).val());
		}
	}
	function setOptionType(compNum){
		var optType = "optType"+compNum[1];
		jQuery("#"+optType).val(jQuery("#editOptionType").val());
	}
	function getOptionDesc(optionValueDiv){
		var chckboxCntDiv = $("#optionList").find(".optiontext");
		var optionString =  "";
		jQuery(chckboxCntDiv).each(function(index){
				var optDescId = $(this).attr("id");
				var optText = $("#"+optDescId).val();
				if(optionString ==""){
		    	       optionString = optText;
		    	       }
		    	    else{
		    		   optionString = optionString+"^"+optText;
	    	       }
			});
		jQuery("#"+optionValueDiv).val(optionString); 
		}
	function getLastDiv(lastDiv){
		var lastDivnum = parseInt(lastDiv);
		var flag = false; 
		var currDiv = "";
		for(var i=(lastDiv-1); i>0; i--){
			currDiv = "add_"+i;
				if(jQuery("#"+currDiv).length !=0){
						flag = true;
						lastDivnum = i;
						break;
					}
			}
		if(flag == true){
				currDiv = "add_"+lastDivnum;
			}
		else{
			    currDiv = "add_"+lastDiv; 	
			}
		return currDiv;
	}
	function setDelEditImage(){
		var displayDivArr = jQuery("#VCcontinner").find(".displayDiv");
		jQuery(displayDivArr).each(function(index){
			var id = jQuery(this).attr("id");
			var visibility = jQuery("#"+id).val();
			if(visibility == "hidden"){
				var delButt = "delComp_"+(index+1);
				jQuery("#"+delButt).css("visibility", "hidden");
			}
		});
		}
	function serComponentVisible(compNum){
		   var curr_compDiv = "compDiv_"+compNum[1];
		   var labelSpanDiv = "labelSpan"+compNum[1];
		   jQuery("#"+curr_compDiv).css("visibility", "visible");
		   jQuery("#"+labelSpanDiv).css("visibility", "visible");
	}
	function serComponentHidden(compNum){
		   var curr_compDiv = "compDiv_"+compNum[1];
		   var labelSpanDiv = "labelSpan"+compNum[1];
		   jQuery("#"+curr_compDiv).css("visibility", "hidden");
		   jQuery("#"+labelSpanDiv).css("visibility", "hidden");
	}
	function isUniqueComponent(componentDiv , visibiltyValue){
		var compNum = componentDiv.split("_");
		var viewComp = "viewComponent"+compNum[1];
		var viewCompId = jQuery("#"+viewComp).val();
		var visibility = "visibility"+compNum[1];
		var flag = true;
		var componentName = "componentName"+compNum[1];
		if((jQuery("#"+visibility).val() == visibiltyValue) ){
			for(var i=1; i <= 15; i++){
				if(i != parseInt(compNum[1])){
				var viewCompCurr = "viewComponent"+i;
				var viewCompIdCurr = jQuery("#"+viewCompCurr).val();
				if(viewCompIdCurr == viewCompId ){
					flag = false;
				}
				}
		}
		}
		if((flag == false) && (visibiltyValue == "hidden")){
			alert(jQuery("#"+componentName).val() + " is already in use. Please select another form the heading list.");
		}
		return flag;
	}
	function repositoryImageSelectionPop(divId) {
		if(divId == "center"){
	        jQuery("#repositoryPopUp").show();
	        jQuery(".box_content1").css("top","25%");
	        jQuery("#grayBG").show();
	    } 
		else if(divId == "center104"){
	        jQuery("#repositoryPopUp104").show();
	        jQuery(".box_content1").css("top","25%");
	        jQuery("#grayBG").show();
	    }
            else if(divId == "center105"){
	        jQuery("#repositoryPopUp105").show();
	        jQuery(".box_content1").css("top","25%");
	        jQuery("#grayBG").show();
	    }
		else if(divId == "leftLogo"){
			 jQuery("#leftLogoRepositoryPopUp").show();
			 jQuery(".box_content1").css("top","25%");
			 jQuery("#grayBG").show();
		}
		else if(divId == "leftLogo104"){
			 jQuery("#leftLogoRepositoryPopUp104").show();
			 jQuery(".box_content1").css("top","25%");
			 jQuery("#grayBG").show();
		}
		else if(divId == "rightLogo"){
			 jQuery("#rightLogoRepositoryPopUp").show();
			 jQuery(".box_content1").css("top","25%");
			 jQuery("#grayBG").show();
		}
		else if(divId == "rightLogo104"){
			 jQuery("#rightLogoRepositoryPopUp104").show();
			 jQuery(".box_content1").css("top","25%");
			 jQuery("#grayBG").show();
		}
		else if(divId == "left"){
			jQuery("#leftRepositoryPopUp").show();
			jQuery(".box_content1").css("top","25%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomLeft"){
			jQuery("#bottomLeftRepositoryPopUp").show();
			jQuery(".box_content1").css("top","45%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomRight"){
			jQuery("#bottomRightRepositoryPopUp").show();
			jQuery(".box_content1").css("top","35%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomLast"){
			jQuery("#bottomLastRepositoryPopUp").show();
			jQuery(".box_content1").css("top","50%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomMid"){
			jQuery("#bottomMidRepositoryPopUp").show();
			jQuery(".box_content1").css("top","33%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomCenter"){
			jQuery("#bottomCenterRepositoryPopUp").show();
			jQuery(".box_content1").css("top","45%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "close"){
			clearFileUploader();
			jQuery("#selectImg").val("");
			jQuery("#bottomCenterRepositoryPopUp").hide();
			jQuery("#bottomMidRepositoryPopUp").hide();
			jQuery("#bottomLastRepositoryPopUp").hide();
	        jQuery("#bottomRightRepositoryPopUp").hide();
			jQuery("#bottomLeftRepositoryPopUp").hide();
			jQuery("#rightLogoRepositoryPopUp").hide();
			jQuery("#leftLogoRepositoryPopUp").hide();
			jQuery("#leftLogoRepositoryPopUp104").hide();
			jQuery("#rightLogoRepositoryPopUp104").hide();
			jQuery("#leftRepositoryPopUp").hide();
			jQuery("#repositoryPopUp").hide();
			jQuery("#repositoryPopUp104").hide();
                        jQuery("#repositoryPopUp105").hide();
			jQuery("#grayBG").hide();
	        centerImageSelectionPop('close');
	    }
            else if(divId == "close105"){
                        if(jQuery("#imgLogo").find("img").length > 0){
                            jQuery("#msgText").css("display", "none"); 
                            jQuery("#logoText").css("display", "none");
                            jQuery("#imgLogo").css("display", "block");
                        //}else if(jQuery("#logoText").trim != ""){
                        }else if((jQuery("#logoText").length != 0) && (jQuery("#logoText").val().length != 0)){
                            jQuery("#imgLogo").css("display", "none");
                            jQuery("#msgText").css("display", "none"); 
                            jQuery("#logoText").css("display", "block");
                        }else{
                            jQuery("#logoText").css("display", "none");
                            jQuery("#imgLogo").css("display", "none");
                            jQuery("#msgText").css("display", "block"); 
                        }
			clearFileUploader();
			jQuery("#selectImg").val("");
			jQuery("#bottomCenterRepositoryPopUp").hide();
			jQuery("#bottomLastRepositoryPopUp").hide();
                        jQuery("#bottomRightRepositoryPopUp").hide();
			jQuery("#bottomLeftRepositoryPopUp").hide();
			jQuery("#leftRepositoryPopUp").hide();
			jQuery("#repositoryPopUp104").hide();
                        jQuery("#repositoryPopUp105").hide();
			jQuery("#repositoryPopUp").hide();
			jQuery("#grayBG").hide();
                        centerImageSelectionPop('close105');
	    }
	}
		
	function centerImageSelectionPop(divId, imageWidth, imageHeight) {
		if(divId == "center"){
			jQuery("#centerImagePop").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			$("#centerimageHeight").val(imageHeight);
			$("#centerimageWidth").val(imageWidth);
		}
		else if(divId == "center104"){
			jQuery("#centerImagePop104").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			jQuery("#centerimageHeight104").val(imageHeight);
			jQuery("#centerimageWidth104").val(imageWidth);
		}
                else if(divId == "center105"){
			jQuery("#centerImagePop105").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			jQuery("#centerimageHeight105").val(imageHeight);
			jQuery("#centerimageWidth105").val(imageWidth);
		}
		else if(divId == "leftLogo"){
			 
			jQuery("#leftLogoImagePop").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			$("#leftLogoimageHeight").val(imageHeight);
			$("#leftLogoimageWidth").val(imageWidth);
		}
		else if(divId == "leftLogo104"){
			 
			jQuery("#leftLogoImagePop104").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			$("#leftLogoimageHeight104").val(imageHeight);
			$("#leftLogoimageWidth104").val(imageWidth);
		}
		else if(divId == "rightLogo"){
			jQuery("#rightLogoImagePop").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			$("#rightLogoimageHeight").val(imageHeight);
			$("#rightLogoimageWidth").val(imageWidth);
		}
		else if(divId == "rightLogo104"){
			jQuery("#rightLogoImagePop104").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			$("#rightLogoimageHeight104").val(imageHeight);
			$("#rightLogoimageWidth104").val(imageWidth);
		}
		else if(divId == "left"){
			jQuery("#leftImagePop").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			$("#leftimageHeight").val(imageHeight);
			$("#leftimageWidth").val(imageWidth);
		}
		else if(divId == "bottemLeft"){
			jQuery("#bottomLeftImagePop").show();
			jQuery(".box_content").css("top","45%");
			jQuery("#grayBG").show();
			$("#bottomLeftimageHeight").val(imageHeight);
			$("#bottomLeftimageWidth").val(imageWidth);
		}
		else if(divId == "bottemLeft103"){
			jQuery("#bottomLeftImagePop").show();
			jQuery(".box_content").css("top","65%");
			jQuery("#grayBG").show();
			$("#bottomLeftimageHeight").val(imageHeight);
			$("#bottomLeftimageWidth").val(imageWidth);
		}
		else if(divId == "bottemRight"){
			jQuery("#bottomRightImagePop").show();
			jQuery(".box_content").css("top","35%");
			jQuery("#grayBG").show();
			$("#bottomRightimageHeight").val(imageHeight);
			$("#bottomRightimageWidth").val(imageWidth);
		}
		else if(divId == "bottomLast"){
			jQuery("#bottomLastImagePop").show();
			jQuery(".box_content").css("top","50%");
			jQuery("#grayBG").show();
			$("#bottomLastimageHeight").val(imageHeight);
			$("#bottomLastimageWidth").val(imageWidth);
		}
		else if(divId == "bottomMid"){
			jQuery("#bottomMidImagePop").show();
			jQuery(".box_content").css("top","33%");
			jQuery("#grayBG").show();
			$("#bottomMidimageHeight").val(imageHeight);
			$("#bottomMidimageWidth").val(imageWidth);
		}
		else if(divId == "bottomCenter"){
			jQuery("#bottomCenterImagePop").show();
			jQuery(".box_content").css("top","45%");
			jQuery("#grayBG").show();
			$("#bottomCenterimageHeight").val(imageHeight);
			$("#bottomCenterimageWidth").val(imageWidth);
		}
		else if(divId == "close"){
			clearFileUploader();
			jQuery("#selectImg").val("");
			jQuery("#bottomCenterImagePop").hide();
			jQuery("#bottomLastImagePop").hide();
			jQuery("#bottomMidImagePop").hide();
			jQuery("#bottomRightImagePop").hide();
			jQuery("#bottomLeftImagePop").hide();
			jQuery("#rightLogoImagePop").hide();
			jQuery("#leftLogoImagePop").hide();
			jQuery("#leftLogoImagePop104").hide();
			jQuery("#rightLogoImagePop104").hide();
			jQuery("#centerImagePop").hide();
			jQuery("#centerImagePop104").hide();
                        jQuery("#centerImagePop105").hide();
			jQuery("#leftImagePop").hide();
			jQuery("#grayBG").hide();
		}
                else if(divId == "close105"){
                        if(jQuery("#imgLogo").find("img").length > 0){
                            jQuery("#imgLogo").css("display", "block");
                            jQuery("#logoText").css("display", "none");
                            jQuery("#msgText").css("display", "none");
                        }else if((jQuery("#logoText").length != 0) && (jQuery("#logoText").val().length != 0)){
                            jQuery("#imgLogo").css("display", "none");
                            jQuery("#logoText").css("display", "block");
                            jQuery("#msgText").css("display", "none");
                        }else{
                            jQuery("#imgLogo").css("display", "none");
                            jQuery("#logoText").css("display", "none");
                            jQuery("#msgText").css("display", "block");
                        }
			clearFileUploader();
			jQuery("#selectImg").val("");
			jQuery("#bottomCenterImagePop").hide();
			jQuery("#bottomLastImagePop").hide();
			jQuery("#bottomMidImagePop").hide();
			jQuery("#bottomRightImagePop").hide();
			jQuery("#bottomLeftImagePop").hide();
			jQuery("#centerImagePop").hide();
			jQuery("#centerImagePop104").hide();
                        jQuery("#centerImagePop105").hide();
			jQuery("#leftImagePop").hide();
			jQuery("#grayBG").hide();
		}
		$("#reqHeight").val(imageHeight);
		$("#reqWidth").val(imageWidth);
	}
	
	function changeImage(imgType) {
		if(imgType == 'circle'){
			$("span").find("b[class='green']").addClass("greenCircle");
			$("span").find("b[class='green greenCircle']").removeClass("green");
			$("span").find("b[class='red']").addClass("redCircle");
			$("span").find("b[class='red redCircle']").removeClass("red");
			$("span").find("b[class='yellow']").addClass("yellowCircle");
			$("span").find("b[class='yellow yellowCircle']").removeClass("yellow");

			 $("span").find("b[class='smallGreen']").addClass("smallGreenCircle");
			$("span").find("b[class='smallGreen smallGreenCircle']").removeClass("smallGreen");
			$("span").find("b[class='smallRed']").addClass("smallRedCircle");
			$("span").find("b[class='smallRed smallRedCircle']").removeClass("smallRed");
			$("span").find("b[class='smallYellow']").addClass("smallYellowCircle");
			$("span").find("b[class='smallYellow smallYellowCircle']").removeClass("smallYellow"); 
			
		} else {
			$("span").find("b[class='greenCircle']").addClass("green");
			$("span").find("b[class='greenCircle green']").removeClass("greenCircle");
			$("span").find("b[class='redCircle']").addClass("red");
			$("span").find("b[class='redCircle red']").removeClass("redCircle");
			$("span").find("b[class='yellowCircle']").addClass("yellow");
			$("span").find("b[class='yellowCircle yellow']").removeClass("yellowCircle");

			 $("span").find("b[class='smallGreenCircle']").addClass("smallGreen");
			$("span").find("b[class='smallGreenCircle smallGreen']").removeClass("smallGreenCircle");
			$("span").find("b[class='smallRedCircle']").addClass("smallRed");
			$("span").find("b[class='smallRedCircle smallRed']").removeClass("smallRedCircle");
			$("span").find("b[class='smallYellowCircle']").addClass("smallYellow");
			$("span").find("b[class='smallYellowCircle smallYellow']").removeClass("smallYellowCircle"); 
			}
	}
	
	//Close All Div()
	function closeAllDiv(){
		jQuery("#lbox").empty();
		jQuery("#lbox104").empty();
                jQuery("#lbox105").empty();
		jQuery("#leftBox").empty();
		jQuery("#leftLogobox").empty();
		jQuery("#leftLogobox104").empty();
		jQuery("#rightLogobox").empty();
		jQuery("#bottomLeftBox").empty();
		jQuery("#bottomRightBox").empty();
		jQuery("#bottomLastBox").empty();
		jQuery("#bottomMidBox").empty();
		jQuery("#bottomCenterBox").empty();
		jQuery("#repositoryBox").empty();
		jQuery("#rightLogobox104").empty();
		
	}
	//Clear File uploader
	function clearFileUploader(){
		jQuery("#localImage").val("");
		jQuery("#leftImage").val("");
		jQuery("#bottomLeftImage").val("");
		jQuery("#bottomRightImage").val("");
		jQuery("#bottomLastImage").val("");
		jQuery("#bottomMidImage").val("");
		jQuery("#bottomCenterImage").val("");
		jQuery("#leftLogoImagePop").val("");
		jQuery("#leftLogoImagePop104").val("");
		jQuery("#rightLogoImagePop").val("");
		jQuery("#rightLogoImagePop104").val("");
		
		
	}
	function saveEditText(){
		var data = jQuery("#editText").val();
		jQuery("#headerText").html(data);
		jQuery("#headingText").val(data);
		jQuery("#headerText").show();
		jQuery("#editText").hide();
		jQuery("#okHeading").hide();
		jQuery("#editHeading").show();
	}

	function editCmpt(cm,pos,selectNum){
		var edit = "edit"+cm+"_"+selectNum[1];
		var ok = "ok"+cm+"_"+selectNum[1];
		var delete1 = "delete"+cm+"_"+selectNum[1];
		jQuery("#"+pos+"_"+selectNum[1]).hide();
		jQuery("#Component_"+selectNum[1]).show();
		jQuery("#Component_"+selectNum[1]).focus();
		jQuery("#"+ok).show();
		jQuery("#"+delete1).hide();
		jQuery("#"+edit).hide();
	}

	function editCompHeading(cm,pos,selectNum){
		var edit = "edit"+cm+"_"+selectNum[1];
		var ok = "ok"+cm+"_"+selectNum[1];
		var delete1 = "delete"+cm+"_"+selectNum[1];
		jQuery("#"+pos+"_"+selectNum[1]).hide();
		jQuery("#Component_"+selectNum[1]).show();
		jQuery("#Component_"+selectNum[1]).focus();
		jQuery("#"+ok).show();
		jQuery("#"+edit).hide();
	}

	function okCmpt(cm,pos,selectNum){
		var edit = "edit"+cm+"_"+selectNum[1];
		var ok = "ok"+cm+"_"+selectNum[1];
		var delete1 = "delete"+cm+"_"+selectNum[1];
		var delClass = "delete"+cm;
		var data = jQuery("#Component_"+selectNum[1]).val();
		jQuery("#"+pos+"_"+selectNum[1]).html(data);
		jQuery("#"+pos+"_"+selectNum[1]).show();
		if(pos == "topText"){
			jQuery("#"+pos+"_"+selectNum[1]).css("font-weight","bold");
			jQuery("#"+pos+"_"+selectNum[1]).css("font-family","'MyriadProBold', Arial");
			jQuery("#"+pos+"_"+selectNum[1]).css("font-size","11pt");
		}
		jQuery("#Component_"+selectNum[1]).hide();			
		jQuery("#"+ok).hide();
		jQuery("#"+edit).show();
		if(data=="")
		{
			jQuery("#"+delete1).hide();
		}
		else
		{
			
		}
	}
	
	function okTopCmpt(cm,pos,selectNum){
		var edit = "edit"+cm+"_"+selectNum[1];
		var ok = "ok"+cm+"_"+selectNum[1];
		var delete1 = "delete"+cm+"_"+selectNum[1];
		var delClass = "delete"+cm;
		var data = jQuery("#Component_"+selectNum[1]).val();
		jQuery("#"+pos+"_"+selectNum[1]).html(data);
		jQuery("#"+pos+"_"+selectNum[1]).show();
		if(pos == "topText"){
			jQuery("#"+pos+"_"+selectNum[1]).css("font-weight","bold");
			jQuery("#"+pos+"_"+selectNum[1]).css("font-family","'MyriadProBold', Arial");
			jQuery("#"+pos+"_"+selectNum[1]).css("font-size","11pt");
		}
		jQuery("#Component_"+selectNum[1]).hide();			
		jQuery("#"+ok).hide();
		jQuery("#"+edit).show();
		if(data=="")
		{
			jQuery("#"+delete1).hide();
		}
	}

	function deleteComp(cm,pos,selectNum){
		var delete1 = "delete"+cm+"_"+selectNum[1];
		jQuery("#"+pos+"_"+selectNum[1]).hide();			
		jQuery("#Component_"+selectNum[1]).val("");
		jQuery("#"+delete1).hide();
	}
	
	//checkStringLength
	function checkStringLength(id, event)
	{
		var currNum = id.split("_");
		var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
		var maxWidth = 770;
		if(is_chrome == true){
			maxWidth = 720;
		}
		jQuery("#temp1").empty();
		jQuery("#temp1").text(jQuery("#"+id).val());
		var matches = jQuery("#"+id).val();
		var pattern=/_/g;
	    var v=matches.match(pattern);
	    var width = 0;
	    jQuery("#temp1").hide();
	    if(v!=null)
	    {
	    	width=jQuery("#temp1").width()-((matches.match(pattern).length/2)*.5);
	    }
	    else
	    {
	    	 width=jQuery("#temp1").width();
	    } 
		if(width>=maxWidth && (event.keyCode != 8 && event.keyCode != 46))
		{
			$("#"+id).attr('maxlength',$("#"+id).val().length);
			alert("Cannot add more characters.");
			if(id != "editComp_"+currNum[1]+"_2"){
				  jQuery("#editComp_"+currNum[1]+"_2").focus();
	    	}
			return true;
		}
		else if(event.keyCode == 13 ){
	    	if(id != "editComp_"+compNum[1]+"_2"){
	    		 jQuery("#editComp_"+compNum[1]+"_2").focus();
	    	}
	    }
	}
	
	function okDetailHeading103(thisId){
		var selectId = jQuery(thisId).attr("id");
		var selectNum = selectId.split("_");
		jQuery("#editComp_"+selectNum[1]+"_"+1).css("display","none");
		jQuery("#editComp_"+selectNum[1]+"_"+2).css("display","none");
		jQuery("#okDetailHeading_"+selectNum[1]).hide();
		jQuery("#editDetailHeading_"+selectNum[1]).show();
		if(jQuery("#editComp_"+selectNum[1]+"_"+1).val().length==0)
		{
			jQuery("#editComp_"+selectNum[1]+"_"+1).val(" ");
		
		}
		if(jQuery("#editComp_"+selectNum[1]+"_"+2).val().length==0)
		{
			jQuery("#editComp_"+selectNum[1]+"_"+2).val(" ");
		}
		jQuery("#cust1").html(jQuery("#editComp_"+selectNum[1]+"_"+1).val());
		jQuery("#cust2").html(jQuery("#editComp_"+selectNum[1]+"_"+2).val());
		jQuery("#detailText_"+selectNum[1]).val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
		jQuery(".detailCmpt").val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
		jQuery("#cust1").show();
		jQuery("#cust2").show();
		if((jQuery.trim((jQuery("#editComp_"+selectNum[1]+"_"+1).val())).length == 0) && (jQuery.trim((jQuery("#editComp_"+selectNum[1]+"_"+1).val())).length == 0)){
			jQuery("#customer").css("border","1px solid white");
		}
		else{
			jQuery("#customer").css("border","1px solid black");
		}
	}
	
		
	function mouseOut103_1(textBoxId){
			jQuery("#"+textBoxId+"_2").focus();
		
	}
	function mouseOut103_2(textBoxId){
		    var selectNum = textBoxId.split("_");
			jQuery("#"+textBoxId+"_1").css("display","none");
			jQuery("#"+textBoxId+"_2").css("display","none");
			jQuery("#okDetailHeading_"+selectNum[1]).hide();
			jQuery("#editDetailHeading_"+selectNum[1]).show();
			var len = jQuery("#"+textBoxId+"_1").val();
			if(jQuery("#"+textBoxId+"_1").val().length==0)
			{
				jQuery("#"+textBoxId+"_1").val(" ");
			
			}
			if(jQuery("#"+textBoxId+"_2").val().length==0)
			{
				jQuery("#"+textBoxId+"_2").val(" ");
			}
			jQuery("#cust1").html(jQuery("#"+textBoxId+"_1").val());
			jQuery("#cust2").html(jQuery("#"+textBoxId+"_2").val());
			jQuery("#detailText_"+selectNum[1]).val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
			jQuery(".detailCmpt").val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
			jQuery("#cust1").show();
			jQuery("#cust2").show();
			if((jQuery.trim((jQuery("#"+textBoxId+"_1").val())).length == 0) && (jQuery.trim((jQuery("#"+textBoxId+"_1").val())).length == 0)){
				jQuery("#customer").css("border","1px solid white");
			}
			else{
				jQuery("#customer").css("border","1px solid black");
			}
	}