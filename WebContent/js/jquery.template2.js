jQuery(document).ready(function(){
		jQuery("#cancelBtn").click(function(event){		
		event.stopPropagation();
	});
	
	//Edit Heading
	jQuery("#editHeading102").click(function(){
		jQuery("#editText102").css("display","block");
		jQuery("#editText102").focus();
		jQuery("#headerText102").hide();
		jQuery("#editHeading102").hide();
		jQuery("#okHeading102").css("display","block");
		jQuery("#okHeading102").css("width","20px");
		jQuery("#okHeading102").css("height","20px");
		jQuery("#okHeading102").css("float","right");
		jQuery("#okHeading102").css("marginRight","51px");
	});
	
	jQuery("#okHeading102").click(function(){
		checkMandatory();
		var editText = jQuery.trim(jQuery("#editText102").val());
		if(editText.length > 0 || editText != ""){
			var data = jQuery("#editText").val();
			jQuery("#headerText102").html(data);
			jQuery("#headingText102").val(data);
			jQuery("#headerText102").show();
			jQuery("#editText102").hide();
			jQuery("#okHeading102").hide();
			jQuery("#editHeading102").show();
		}
	});
	
	//change radio to circle or circle to radio
	$('input[name=imgType]:radio').click(function(){
		var imageType = $(this).val();
		var image = "../images/green.PNG";
		if(imageType == "circle"){
			var greenSquares  = jQuery(".container").find(".greenSquare");
			jQuery(greenSquares).each(function(index){
				jQuery(this).removeClass("greenSquare");
				jQuery(this).addClass("greenCircle");
			});
			var yellowSquares = jQuery(".container").find(".yellowSquare");
			jQuery(yellowSquares).each(function(index){
				jQuery(this).removeClass("yellowSquare");
				jQuery(this).addClass("yellowCircle");
			});
			var redSquares = jQuery(".container").find(".redSquare");
			jQuery(redSquares).each(function(index){
				jQuery(this).removeClass("redSquare");
				jQuery(this).addClass("redCircle");
			});
		}
		else{
			var greenCircles  = jQuery(".container").find(".greenCircle");
			jQuery(greenCircles).each(function(index){
				jQuery(this).removeClass("greenCircle");
				jQuery(this).addClass("greenSquare");
			});
			var yellowCircles = jQuery(".container").find(".yellowCircle");
			jQuery(yellowCircles).each(function(index){
				jQuery(this).removeClass("yellowCircle");
				jQuery(this).addClass("yellowSquare");
			});
			var redCircles = jQuery(".container").find(".redCircle");
			jQuery(redCircles).each(function(index){
				jQuery(this).removeClass("redCircle");
				jQuery(this).addClass("redSquare");
			});
		}
	});

	//focusOutSave focusout
	jQuery(".focusOutSave").focusout(function(){
		var maxChar = jQuery(this).attr("maxlength");
		var id = jQuery(this).attr("id");
		var idNum = id.split("_");
		if(parseInt(idNum[1]) == 7 || parseInt(idNum[1]) == 8 || parseInt(idNum[1]) == 9|| 
				parseInt(idNum[1]) == 10 ||parseInt(idNum[1]) == 11 ||
					parseInt(idNum[1]) == 26 ||parseInt(idNum[1]) == 27){
			if(checkMandatory() == true){
				removeEdit(jQuery(this).attr("id") , maxChar);
			}
		}
		else{
			removeEdit(jQuery(this).attr("id") , maxChar);
		}
	});

	//focusOutSave keydown
	jQuery(".focusOutSave").keydown(function(e){
		if(e.keyCode == 13){
			var maxChar = jQuery(this).attr("maxlength");
			var id = jQuery(this).attr("id");
			var topText = jQuery("#"+id).val();
			var idNum = id.split("_");
			if(parseInt(idNum[1]) == 7 || parseInt(idNum[1]) == 8 || parseInt(idNum[1]) == 9|| 
					parseInt(idNum[1]) == 10 ||parseInt(idNum[1]) == 11 ||
						parseInt(idNum[1]) == 26 ||parseInt(idNum[1]) == 27){
				if(topText.length <= 0 || topText == ""){
				}
				else{
					if(checkMandatory() == true){
						removeEdit(jQuery(this).attr("id") , maxChar);
					}
				}
			}
			else{
				removeEdit(jQuery(this).attr("id") , maxChar);
			}
			return false;
		}
	});

	//editText102 focusout
	jQuery("#editText102").focusout(function(){
		checkMandatory();
		var editText = jQuery.trim(jQuery("#editText102").val());
		if(editText.length > 0 || editText != ""){
			if(((jQuery(this).val()).length <= 44 )){
				var data = jQuery("#editText102").val();
				jQuery("#headerText102").html(data);
				jQuery("#headingText102").val(data);
				jQuery("#headerText102").show();
				jQuery("#editText102").hide();
				jQuery("#okHeading102").hide();
				jQuery("#editHeading102").show();
			}
		}
	});

	//editText102 keydown
	jQuery("#editText102").keydown(function(e){
		if(((jQuery(this).val()).length <= 44) && (e.keyCode == 13)){
			var editText = jQuery.trim(jQuery("#editText102").val());
			if(editText.length > 0 || editText != ""){
				var data = jQuery("#editText102").val();
				jQuery("#headerText102").html(data);
				jQuery("#headingText102").val(data);
				jQuery("#headerText102").show();
				jQuery("#editText102").hide();
				jQuery("#okHeading102").hide();
				jQuery("#editHeading102").show();
			}
		}
	});
});

//Check Mandatory
function checkMandatory(){
	var editText = jQuery("#editText102").val();
	var topLeftText = jQuery("#editComp_7").val();
	var topMiddleText = jQuery("#editComp_8").val();
	var topRightText = jQuery("#editComp_9").val();
	var leftItem = jQuery("#editComp_10").val();
	var leftCond = jQuery("#editComp_11").val();
	var rightItem = jQuery("#editComp_26").val();
	var rightCond = jQuery("#editComp_27").val();
	var message = "This field cannot be blank.";
	if(editText.length <= 0 || editText == ""){
		alert(message);
		jQuery("#editText102").css("display","block");
		jQuery("#editText102").focus();
		jQuery("#headerText102").hide();
		jQuery("#editHeading102").hide();
		jQuery("#okHeading102").css("display","block");
		jQuery("#okHeading102").css("width","20px");
		jQuery("#okHeading102").css("height","20px");
		jQuery("#okHeading102").css("float","right");
		jQuery("#okHeading102").css("marginRight","51px");
		return false;
	}
	else if(topLeftText.length <= 0 || topLeftText == ""){
		alert(message);
		jQuery("#editButton7").hide();
		jQuery("#okButton7").show();
		jQuery("#comp_7").hide();
		jQuery("#editComp_7").css("display","inline");
		jQuery("#editComp_7").focus();
		jQuery("#editComp_7").css("fontFamily","MyriadProRegular");
		jQuery("#editComp_7").css("fontSize","11pt");
		return false;
	}
	else if(topMiddleText.length <= 0 || topMiddleText == ""){
		alert(message);
		jQuery("#editButton8").hide();
		jQuery("#okButton8").show();
		jQuery("#comp_8").hide();
		jQuery("#editComp_8").css("display","inline");
		jQuery("#editComp_8").focus();
		jQuery("#editComp_8").css("fontFamily","MyriadProRegular");
		jQuery("#editComp_8").css("fontSize","11pt");
		return false;
	}
	else if(topRightText.length <= 0 || topRightText == ""){
		alert(message);
		jQuery("#editButton9").hide();
		jQuery("#okButton9").show();
		jQuery("#comp_9").hide();
		jQuery("#editComp_9").css("display","inline");
		jQuery("#editComp_9").focus();
		jQuery("#editComp_9").css("fontFamily","MyriadProRegular");
		jQuery("#editComp_9").css("fontSize","11pt");
		return false;
	}
	else if(leftItem.length <= 0 || leftItem == ""){
		alert(message);
		jQuery("#editButton10").hide();
		jQuery("#okButton10").show();
		jQuery("#comp_10").hide();
		jQuery("#editComp_10").css("display","inline");
		jQuery("#editComp_10").focus();
		jQuery("#editComp_10").css("fontFamily","MyriadProRegular");
		jQuery("#editComp_10").css("fontSize","11pt");
		return false;
	}
	else if(leftCond.length <= 0 || leftCond == ""){
		alert(message);
		jQuery("#editButton11").hide();
		jQuery("#okButton11").show();
		jQuery("#comp_11").hide();
		jQuery("#editComp_11").css("display","inline");
		jQuery("#editComp_11").focus();
		jQuery("#editComp_11").css("fontFamily","MyriadProRegular");
		jQuery("#editComp_11").css("fontSize","11pt");
		return false;
	}
	else if(rightItem.length <= 0 || rightItem == ""){
		alert(message);
		jQuery("#editButton26").hide();
		jQuery("#okButton26").show();
		jQuery("#comp_26").hide();
		jQuery("#editComp_26").css("display","inline");
		jQuery("#editComp_26").focus();
		jQuery("#editComp_26").css("fontFamily","MyriadProRegular");
		jQuery("#editComp_26").css("fontSize","11pt");
		return false;
	}
	else if(rightCond.length <= 0 || rightCond == ""){
		alert(message);
		jQuery("#editButton27").hide();
		jQuery("#okButton27").show();
		jQuery("#comp_27").hide();
		jQuery("#editComp_27").css("display","inline");
		jQuery("#editComp_27").focus();
		jQuery("#editComp_27").css("fontFamily","MyriadProRegular");
		jQuery("#editComp_27").css("fontSize","11pt");
		return false;
	}
	else{
		return true;
	}
}

//editText
function editText(id){
	if(checkMandatory() == true){
		var imageId = id.substring(9);
		jQuery("#editButton"+imageId).hide();
		jQuery("#okButton"+imageId).show();
		jQuery("#comp_"+imageId).hide();
		jQuery("#editComp_"+imageId).css("display","inline");
		jQuery("#editComp_"+imageId).focus();
		jQuery("#editComp_"+imageId).css("fontFamily","MyriadProRegular");
		jQuery("#editComp_"+imageId).css("fontSize","11pt");
	}
}

//removeEdit
function removeEdit(id , maxChar){
	var imageId = id.split("_");
	jQuery("#comp_"+imageId[1]).show();
	jQuery("#editComp_"+imageId[1]).css("display","none");
	jQuery("#editButton"+imageId[1]).show();
	jQuery("#okButton"+imageId[1]).hide();
	jQuery("#comp_"+imageId[1]).html($("#editComp_"+imageId[1]).val());
	checkMandatory();
}

//first detail component edit
function editTextFirstCompnent(id){
	var imageId = id.substring(9);
	jQuery("#editButton"+imageId).hide();
	jQuery("#okButton"+imageId).show();
	jQuery("#cust1").hide(); 
	jQuery("#cust2").hide(); 
	//jQuery("#editComp_6_0").val(("#cust1").val());
	jQuery("#editComp_6_0").live("cut copy paste",function(e) {
	      e.preventDefault();
	});
	jQuery("#editComp_6_1").live("cut copy paste",function(e) {
	      e.preventDefault();
	});
	jQuery("#editComp_6_0").val(jQuery("#cust1").text());
	jQuery("#editComp_6_1").val(jQuery("#cust2").text());	
	jQuery("#editComp_6_0").css("display","inline");
	jQuery("#editComp_6_0").focus();
	jQuery("#editComp_6_0"+imageId).css("fontFamily","MyriadProRegular");
	jQuery("#editComp_6_0"+imageId).css("fontSize","11pt"); 
	jQuery("#editComp_6_1").css("display","inline");
	//jQuery("#editComp_6_1"+imageId).focus();
	jQuery("#editComp_6_1"+imageId).css("fontFamily","MyriadProRegular");
	jQuery("#editComp_6_1"+imageId).css("fontSize","11pt"); 
	
}

//first detail component ok
function removeFirstComponentEdit(id , maxChar){
	var imageId = id.split("_");	
	jQuery("#editComp_6_0").css("display","none");
	jQuery("#editComp_6_1").css("display","none");
	jQuery("#editButton"+imageId[1]).show();
	jQuery("#okButton"+imageId[1]).hide();
	if(jQuery("#editComp_6_0").val().length==0)
	{
		jQuery("#editComp_6_0").val(" ");
	
	}
	if(jQuery("#editComp_6_1").val().length==0)
	{
		jQuery("#editComp_6_1").val(" ");
	}
	jQuery("#cust1").html($("#editComp_6_0").val());
	jQuery("#cust2").html($("#editComp_6_1").val());
	jQuery("#editComp_6").val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
	jQuery("#cust1").show();
	jQuery("#cust2").show();
	if((trimSpanText(jQuery("#editComp_6_1").val()).length == 0) && (trimSpanText(jQuery("#editComp_6_0").val()).length == 0)){
		jQuery("#customer").css("border","1px solid white");
	}
	else{
		jQuery("#customer").css("border","1px solid #ACACAC");
	}
}

function trimSpanText(s){
	return s.replace(/^\s+|\s+$/g, "");
}


//deleteText
function deleteText(id){
	var r = confirm("Are you sure you want to delete?");
	if (r==true){
		var imageId = id.substring(9);
		jQuery("#comp_"+imageId).hide();
		jQuery("#component"+imageId).hide();
		jQuery("#editComp_"+imageId).val("");
	}else{}
}
// ----------------
	function mouseOut0(){
			jQuery("#editComp_6_1").focus();
		
	}
	function mouseOut1(){
				removeFirstComponentEdit('editComp_6',186);
	}
