 var errors = 0;
	jQuery(document).ready(function(){
		customerEditBorder();
		var isValidation = true;
		changeImage(jQuery("#imgType").val());
		jQuery("input[name='imgType']").change(function(){
			var imgType = jQuery(this).val();
			changeImage(imgType);		
		});
		
		jQuery("#formName").keyup(function(){
			jQuery("#formNameTab").text(jQuery("#formName").val());
		});
	    jQuery("#updateAdminTemplate104").validate({
	    	onkeyup: false,
	    	onclick: false,
	    	onblur: false,
	    	onfocusout:false,
	    	invalidHandler: function(form, validator) {
	          errors = validator.numberOfInvalids();
	        if (errors) {
	        	alert("This field can not be blank.");
	        	errors=0;
	        }
	        else
	        	launchWindow('#dialog');
	      },
		    rules: {
	    	headerText: {
	    	  requiredHeader: true
		         },
		      component1: {
	    		required: true
		         },
		      component2: {
		    	required:true
			   },
			  component3: {
				required:true
			   },
			 component4: {
			 	required:true
			  },
			  component5: {
		    	required: true
			  },
			  component10: {
			   	required:true
			 },
			 component11: {
				required:true
			  },
			 component12: {
			 	required:true
			 },
			 component20: {
				required:true
			 },
			 component26: {
				required: true
			  },
	 	      component56: {
			   		required: true
		      },
			  component65: {
				required:true
			   },
			 component66: {
				 	required:true
			  },
			  component67: {
				required:true
			  },
			  component68: {
				required:true
			  },
			  component69: {
					required:true
				  }
	       }  
	});
	
	jQuery.validator.addMethod("required", function(comp , element) {
		 var val = jQuery.trim(jQuery("#"+element.id).val());
		if(val==''){
	          var imageId = element.id.substring(9);
			  jQuery("#editButton"+imageId).show();
			  jQuery("#okButton"+imageId).hide();
			return false;
		}
		else{
			return true;

			}
 	},"");
	jQuery.validator.addMethod("requiredHeader", function(comp , element) {
		 var val = jQuery.trim(jQuery("#"+element.id).val());
		if(val==''){
			jQuery("#editButtonheaderText").show();
			jQuery("#okButtonHeader").hide();
			return false;
		}
		else{
			return true;

			}
	},"");
});
	
function updateTemplate104(action) {
	// alert("update:"+errors);
	if (errors <= 0) {
		if (jQuery("#component_01").val() == "") {
			jQuery("#component_01").val(" ");
		}
		if (jQuery("#component_02").val() == "") {
			jQuery("#component_02").val(" ");
		}
		var component0 = jQuery("#component_01").val() + "~"
				+ jQuery("#component_02").val();
		jQuery("#component0").val(component0);
		jQuery("input").attr("readonly", false);
		jQuery("textarea").attr("readonly", false);
		document.updateAdminTemplate104.action = action;

		jQuery("#updateAdminTemplate104").submit();
		jQuery("input[type=text]").attr("readonly", true);
		jQuery("textarea").attr("readonly", true);
	}
	return true;
}

function limitOfCharHeader(event, id, charlength) {
	var key;
	var val = (jQuery("#" + id).val());
	maxChar = val.length;
	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = event.which; // firefox
	if (key == 13) {
		// removeEditMandatory(id , maxChar);
		removeEditHeader(id, maxChar);
		return false;
	}
	return true;
}

function limitnofotextCustomer(id, event) {
	var is_chrome = /chrome/.test(navigator.userAgent.toLowerCase());
	var maxWidth = 770;
	if (is_chrome == true) {
		maxWidth = 685;
	}
	jQuery("#temp1").empty();
	jQuery("#temp1").text(jQuery("#" + id).val());
	var matches = jQuery("#" + id).val();
	var pattern = /_/g;
	var v = matches.match(pattern);
	var width = 0;
	jQuery("#temp1").hide();
	if (v != null) {
		width = jQuery("#temp1").width()
				- ((matches.match(pattern).length / 2) * .5);
	} else {
		width = jQuery("#temp1").width();
	}
	if (width >= maxWidth && (event.keyCode != 8 && event.keyCode != 46)) {
		jQuery("#" + id).attr('maxlength', jQuery("#" + id).val().length);
		alert("Cannot add more characters.");
		if ((id != "component_02")) {
			jQuery("#component_02").focus();
		} else {
			comp02FocusOut();
		}
	} else if (event.keyCode == 13) {
		if ((id != "component_02")) {
			jQuery("#component_02").focus();
		} else {
			comp02FocusOut();
		}
	}
	return true;
}

function comp01FocusOut() {
	jQuery("#component_02").focus();
}
		
function comp02FocusOut() {
	jQuery("#editButton0").show();
	jQuery("#okButton0").hide();
	jQuery("#component_01").removeClass("whiteBG");
	jQuery("#component_02").removeClass("whiteBG");
	jQuery("#component_01").addClass('componentHeader');
	jQuery("#component_02").addClass('componentHeader');
	jQuery("#component_01").css('border', 'none');
	jQuery("#component_02").css('border', 'none');
	jQuery("#component_01").attr("readonly", true);
	jQuery("#component_02").attr("readonly", true);
	jQuery("#component_01").focusout();
	jQuery("#component_02").focusout();
	jQuery("#component1").focus();
	customerEditBorder();
}

function limitOfCharForMandatory(event, id, charlength) {
	var key;
	var val = (jQuery("#" + id).val());
	maxChar = val.length;
	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = event.which; // firefox
	if (key == 13) {
		removeEditMandatory(id, maxChar);
		// removeEditHeader(id , maxChar);
		return false;
	}
	return true;
}

function limitnofotext(event, id, charlength) {
	var key;
	var val = (jQuery("#" + id).val());
	maxChar = val.length;
	if (window.event)
		key = window.event.keyCode; // IE
	else
		key = event.which; // firefox
	if (key == 13) {
		removeEdit(id, maxChar);
		// removeEditHeader(id , maxChar);
		return false;
	}
	return true;
}
	
function editText(id) {
	var imageId = id.substring(9);
	jQuery("#editButton" + imageId).hide();
	jQuery("#okButton" + imageId).show();
	jQuery("#" + id).removeClass("componentHeader");
	jQuery("#" + id).addClass('whiteBG');
	jQuery("#" + id).css('border', 'solid 1px black');
	jQuery("#" + id).attr("readonly", false);
	jQuery("#" + id).focus();
}
	
function editTextCustomer(id) {
	jQuery("#editButton0").hide();
	jQuery("#okButton0").show();
	jQuery("#component_01").removeClass("componentHeader");
	jQuery("#component_02").removeClass("componentHeader");
	jQuery("#component_01").addClass('whiteBG');
	jQuery("#component_02").addClass('whiteBG');
	jQuery("#component_01").css('border', 'solid 1px black');
	jQuery("#component_02").css('border', 'solid 1px black');
	jQuery("#component_01").attr("readonly", false);
	jQuery("#component_02").attr("readonly", false);
	jQuery("#component_01").focus();
	jQuery("#" + id).focus();
}
	
function removeEditMandatory(id, maxChar) {
	var val = jQuery.trim(jQuery("#" + id).val());
	if (val <= 0) {
		alert("This field can not be blank.");
		jQuery("#" + id).focus();
	} else {
		jQuery("#" + id).val(val);
		var imageId = id.substring(9);
		jQuery("#editButton" + imageId).show();
		jQuery("#okButton" + imageId).hide();
		jQuery("#" + id).removeClass("whiteBG");
		jQuery("#" + id).addClass('componentHeader');
		jQuery("#" + id).css('border', 'none');
		jQuery("#" + id).attr("readonly", true);
	}
}
	
function onblurDo(id, maxChar) {
	var val = jQuery.trim(jQuery("#" + id).val());
	if (val <= 0) {
		alert("This field can not be blank.");
		jQuery("#" + id).focus();

	} else {
		jQuery("#" + id).val(val);
		var imageId = id.substring(9);
		jQuery("#editButton" + imageId).show();
		jQuery("#okButton" + imageId).hide();
		jQuery("#" + id).removeClass("whiteBG");
		jQuery("#" + id).addClass('componentHeader');
		jQuery("#" + id).css('border', 'none');
		jQuery("#" + id).attr("readonly", true);
	}
}
	
function removeEdit(id, maxChar) {
	var val = jQuery.trim(jQuery("#" + id).val());
	if (val.length > 0) {
		var deleteImageId = id.substring(9);
		if (jQuery("#deleteButton" + deleteImageId)) {
			jQuery("#deleteButton" + deleteImageId).show();
		}
	}
	jQuery("#" + id).val(val);
	var imageId = id.substring(9);
	jQuery("#editButton" + imageId).show();
	jQuery("#okButton" + imageId).hide();
	jQuery("#" + id).removeClass("whiteBG");
	jQuery("#" + id).addClass('componentHeader');
	jQuery("#" + id).css('border', 'none');
	jQuery("#" + id).attr("readonly", true);
	jQuery("#" + id).focusout();

}
	
function removeEditCustomer(id, maxChar) {
	jQuery("#editButton0").show();
	jQuery("#okButton0").hide();
	jQuery("#component_01").removeClass("whiteBG");
	jQuery("#component_02").removeClass("whiteBG");
	jQuery("#component_01").addClass('componentHeader');
	jQuery("#component_02").addClass('componentHeader');
	jQuery("#component_01").css('border', 'none');
	jQuery("#component_02").css('border', 'none');
	jQuery("#component_01").attr("readonly", true);
	jQuery("#component_02").attr("readonly", true);
	jQuery("#component_01").focusout();
	jQuery("#component_02").focusout();
	customerEditBorder();
}
	
function removeMultipleEdit(from, to) {
	var val = '';
	if (jQuery.trim(jQuery("#component26").val()) == ''
			|| jQuery.trim(jQuery("#component56").val()) == ''
			|| jQuery.trim(jQuery("#component65").val()) == ''
			|| jQuery.trim(jQuery("#component66").val()) == ''
			|| jQuery.trim(jQuery("#component67").val()) == ''
			|| jQuery.trim(jQuery("#component68").val()) == ''
			|| jQuery.trim(jQuery("#component69").val()) == '') {
		alert("This field can not be blank.");
	}
	else {
		jQuery("#editButton" + from).show();
		jQuery("#okButton" + from).hide();
		while (to >= from) {
			val = jQuery.trim(jQuery("#component" + from).val());
			jQuery("#component" + from).removeClass("whiteBG");
			jQuery("#component" + from).addClass('componentHeader');
			jQuery("#component" + from).removeClass("tireComponentHeaderEdit");
			jQuery("#component" + from).addClass('tireComponentHeader');
			jQuery("#component" + from).val(val);
			jQuery("#component" + from).css('border', 'none');
			jQuery("#component" + from).attr("readonly", true);
			from = from + 1;
		}
	}
}
	 
	
function editMultipleText(from, to) {
	jQuery("#editButton" + from).hide();
	jQuery("#okButton" + from).show();
	while (to >= from) {
		jQuery("#component" + from).removeClass("componentHeader");
		jQuery("#component" + from).addClass('whiteBG');
		jQuery("#component" + from).removeClass("tireComponentHeader");
		jQuery("#component" + from).addClass('tireComponentHeaderEdit');
		jQuery("#component" + from).css('border', 'solid 1px black');
		jQuery("#component" + from).attr("readonly", false);
		from = from + 1;
	}
}
	
function deleteCompContent(id, inputId) {
	var r = confirm("Are you sure want to delete?");
	if (r == true) {
		jQuery("#" + inputId).val("");
		jQuery("#" + id).hide();
	}
}
	
function editTextHeader(id) {
	var imageId = id.substring(9);
	jQuery("#editButtonheaderText").hide();
	jQuery("#okButtonHeader").show();

	jQuery("#" + id).css('border', 'solid 1px black');
	jQuery("#" + id).attr("readonly", false);
	jQuery("#" + id).focus();
}
	
function removeEditHeader(id, maxChar) {

	var val = jQuery.trim(jQuery("#" + id).val());
	jQuery("#" + id).val(val);
	var imageId = id.substring(9);
	if (val <= 0) {
		alert("This field can not be blank.");
		jQuery("#" + id).focus();
	} else {
		jQuery("#editButtonheaderText").show();
		jQuery("#okButtonHeader").hide();
		jQuery("#" + id).css('border', 'none');
		jQuery("#" + id).attr("readonly", true);
	}
}
	
function editMultipleTextLast(from, to) {
	jQuery("#editButton" + from).hide();
	jQuery("#okButton" + from).show();
	while (to >= from) {
		jQuery("#component" + from + "Span").hide();
		jQuery("#component" + from).removeClass("componentHeader");
		jQuery("#component" + from).addClass('whiteBG');
		jQuery("#component" + from).removeClass("tireComponentHeader");
		jQuery("#component" + from).addClass('tireComponentHeaderEdit');
		jQuery("#component" + from).css('border', 'solid 1px black');
		jQuery("#component" + from).css('display', 'block');
		jQuery("#component" + from).attr("readonly", false);
		from = from + 1;
	}

}
function removeMultipleEditLast(from, to) {

	var val = '';
	if (jQuery.trim(jQuery("#component67").val()) == ''
			|| jQuery.trim(jQuery("#component68").val()) == ''
			|| jQuery.trim(jQuery("#component69").val()) == '') {
		alert("This field can not be blank.");
	}

	else {
		jQuery("#editButton" + from).show();
		jQuery("#okButton" + from).hide();
		while (to >= from) {
			val = jQuery.trim(jQuery("#component" + from).val());
			jQuery("#component" + from).removeClass("whiteBG");
			jQuery("#component" + from).addClass('componentHeader');
			jQuery("#component" + from).removeClass("tireComponentHeaderEdit");
			jQuery("#component" + from).addClass('tireComponentHeader');
			jQuery("#component" + from).val(val);
			jQuery("#component" + from + "Span").html(val);
			jQuery("#component" + from + "Span").show();
			jQuery("#component" + from).css('display', 'none');
			jQuery("#component" + from).css('border', 'none');
			jQuery("#component" + from).attr("readonly", true);
			from = from + 1;
		}
	}
}

function customerEditBorder(){
	var custTextEdit = jQuery("#component_01").val()+""+jQuery("#component_02").val();
	var trimCustEdit = jQuery.trim(custTextEdit);
	if((trimCustEdit.length) ==0){
		jQuery("#customerP").css("border", "none");
	}
	else{
		jQuery("#customerP").css("border", "1px solid black");
	}
}