$(document).ready(function() {
		/* ------ Change Color ------ */
		$("#inp1").blur(function() {
			var color = "#" + $("#inp1").attr("value");
			$(".ispanHead").css("color", color);
		});
		$("#inp2").blur(function() {
			var color = "#" + $("#inp2").attr("value");
			$(".myspanHead").css("color", color);
		});
		$("#inp3").blur(function() {
			var color = "#" + $("#inp3").attr("value");
			$(".check_text").css("color", color);
		});
		
		$(".divData").mouseover(function(){
			var divId = $(this).attr("id");
			var labelId = $("#" + divId).children().attr("id");
			var imageId = $("#" + labelId).next().attr("id");
			var imageDId = $("#" + labelId).next().next().attr("id");
			$("#" + imageId).show();
			$("#" + imageDId).show();
			$(this).css("background-color", "#EEEEEE");	
			$(this).css("border-radius", "10px");
			$(this).css("padding", "5px");
			$("#" + imageDId).click(function(){
				$("#" + divId).hide();
			});
		});
		$(".divData").mouseout(function(){
			var divId = $(this).attr("id");
			var labelId = $("#" + divId).children().attr("id");
			var imageId = $("#" + labelId).next().attr("id");
			var imageDId = $("#" + labelId).next().next().attr("id");
			$("#" + imageId).hide();
			$("#" + imageDId).hide();
			$(this).css("background-color", "#ffffff");
		});
		
		$(".divData").mouseenter(function(){
			$("#"+$(this).attr("id")).click(function(){
				var labelId = $("#" + $(this).attr("id")).children().attr("id");
				var subLabelId = $("#" + labelId).next().next().next().attr("id");
				$("#current_label_id").attr("value",labelId);
				$("#current_sublabel_id").attr("value",subLabelId);
				$("#changedHeading").keyup(function(event) {
					$('#'+$("#current_label_id").val()).html($('#changedHeading').val());
				});
				$("#changedSubHeading").keyup(function(event) {
					$('#'+$("#current_sublabel_id").val()).html($('#changedSubHeading').val());
				});
			});
		});
		
		/*$('.add').live('click',function() {
			$(this).val('Delete');
			$(this).attr('class', 'del');
			var choice = $("#choiceOption").val().toLowerCase();
			var appendTxt;
				if (choice == "checkbox"){
					appendTxt = '<div><img src="../images/checkbox.png" name="check_box_one[]" />&nbsp;<input type="text" name="input_box_two[]" id="input_box_two_chk"/>&nbsp;<img src="../images/Button-Close-icon.png" class="add" value="Add More" width="14" height="14"/></div>';
				}
				else if(choice == "radio"){
					appendTxt = '<div><input type="radio" name="check_box_one[]" />&nbsp;<input type="text" name="input_box_two[]" id="input_box_two_rdo"/>&nbsp;<img src="../images/Button-Close-icon.png" class="add" value="Add More" width="14" height="14"/></div>';
				}
			$("#new:last").after(appendTxt);
		});*/
		
		var count = 0;
		$(".divData").click(function() {
			var id = $(this).attr("id");
			var labelId = $("#" + id).children().attr("id");
			var pId = $("#" + labelId).next().next().next().next().next().attr("id");
			//alert($("#" + pId).text());
			$("#position").val($("#" + pId).text());
			editComponent(id);
		});
		
		function editComponent(id) {
			count++;
			// empty the fields..
			$("#changedHeading").val("");
			$("#changedSubHeading").val("");
			$("#divOption").empty();
			$("#drpOption").val("First");
			var flag = true;
			var headingSpan = $("#" + id).children().attr("id");
			if ($("#" + headingSpan).text().length != 0) {
				$("#changedHeading").val($("#" + headingSpan).text());
			}
			var subheadingSpan = $("#" + headingSpan).next().next().next().attr("id");
			if ($("#" + subheadingSpan).text().length != 0) {
				$("#changedSubHeading").val($("#" + subheadingSpan).text());
				$("#divSubHeading").css("display", "block");
			} 
			else if ($("#" + subheadingSpan).text().length == 0) {
				$("#divSubHeading").css("display", "none");
			}
			var divOption = $("#" + headingSpan).next().next().next().attr("id");
			var currentSpanOption = divOption + "Span";
			$("#currentSpanOption").attr("value",currentSpanOption);
			for ( var i = 1; flag == true; i++) {
				var spanOption = divOption + "Span" + i;
				var spanChangeOPtion = "spanChangeOPtion" + i;
				if ($("#" + spanOption).length != 0) {
					$("#divOption").append(
					'<div><img src="../images/checkbox.png"> <span id="'+spanChangeOPtion+'">'
									+ $("#" + spanOption)
									.text()
									+ '</span><div>');
				} else {
					flag = false;
				}
			}
			$("#bottomContainerRight").css("display", "block");
			$("#createNewContainerRight").css("display", "none");
		}
		$("#drpOption").change(function() {
			var currentSpan = $("#currentSpanOption").val();
			var selectedOption = $("#drpOption").val();
			if (selectedOption.toLowerCase() == "first") {
				$("#divOption").css("display", "block");
				$("#divOption").empty();
				$("#divOption").append(
						'<div><img src="../images/checkbox.png"><span id="'+currentSpan+'1" >&nbsp;Pass&nbsp;</span><img src="../images/checkbox.png"> <span id="'+currentSpan+'2" >&nbsp;Fail&nbsp;</span><div>');
						$("#createNewContainerRight").css("display", "none");
			} 
			else if (selectedOption.toLowerCase() == "second") {
				$("#divOption").css("display", "block");
				$("#divOption").empty();
				$("#divOption").append(
							'<div><img src="../images/checkbox.png"><span id="'+currentSpan+'1" >&nbsp;Ok&nbsp;</span><img src="../images/checkbox.png"><span id="'+currentSpan+'2" >&nbsp;Cancel&nbsp;</span><div>');
				$("#createNewContainerRight").css("display", "none");
			} 
			else if (selectedOption.toLowerCase() == "third") {
				$("#createNewContainerRight").css("display", "block");
				$("#divOption").css("display", "none");
				$("#choiceOption").change(function(){
					var choice = $("#choiceOption").val();
					if (choice.toLowerCase() == "checkbox") {
						$("#choiceSelectOption").empty();
						$("#choiceSelectOption").append(
								'<div id="options-table"><div id="new"><br /><img src="../images/checkbox.png" name="check_box_one[]" id="check_box_one[]"/>&nbsp;<input type="text" name="input_box_two[]" id="input_box_two_add_chk"/>&nbsp;<img src="../images/Button-Add-icon.png" class="add" width="14" height="14" /></div></div>');
					} 
					else if (choice.toLowerCase() == "radio") {
						$("#choiceSelectOption").empty();
						$("#choiceSelectOption").append(
						'<div id="options-table"><div id="new"><br /><input type="radio" name="check_box_one[]" />&nbsp;<input type="text" name="input_box_two[]" id="input_box_two_add_rdo"/>&nbsp;<img src="../images/Button-Add-icon.png" class="add" width="14" height="14" /></div></div>');
					}
				});
			}
			});
			$("#addOPtion").click(function() {
			});
			
			
		});