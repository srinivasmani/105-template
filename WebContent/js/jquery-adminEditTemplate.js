jQuery(document).ready(function() {
		
		var deleteAlert = "Are you sure you want to delete?";
		var headingAlert = "This field can not be blank.";
		
		jQuery("#selectImg").val("");
		jQuery("#localImage").val("");
		
		var isValidation = true;
		
		changeImage(jQuery("#imgType").val());
		jQuery("input[name='imgType']").change(function(){
			var imgType = jQuery(this).val();
			jQuery("#imgType").val(imgType);
			changeImage(imgType);		
		});
	
	    setDelEditImage();
		var headerColor = jQuery("#headerColor").val();
		if((headerColor == "") || (headerColor == null)){
			jQuery("#headerColor").val("#045fb4");
		}
		else{
			jQuery("#headerColor").val(headerColor);
			jQuery(".text").css("color", headerColor);
		}
		
		jQuery("#inpDefault").click(function(){
			jQuery("#currHeaderColor").val("#045fb4");
			jQuery(".text").css("color", "#045fb4");
		});
		
		jQuery("#updateId").click(function(){
			if(jQuery("#editText").val()==""){
				alert("Please enter heading.");
			}
			else if(jQuery("#edited").val() == "true"){
				alert("Please click Ok or Cancel.");
			}
			else {
				var preDiv = jQuery("#prevDiv").attr("value");
				var componentDiv = jQuery(this).attr("id");
				jQuery("#headerColor").val(jQuery("#currHeaderColor").val());
				jQuery("#updateForm").submit();
				inActiveComponent(preDiv);
				activeComponent(componentDiv);
			}
		});
		
		 jQuery("#image").mouseover(function(){
			 jQuery("#link").show();
		});
		 
		jQuery("#image").mouseout(function(){
			  jQuery("#link").hide();
		});
		
		jQuery("#cancelBtn").click(function(event){			
			event.stopPropagation();
		});
		
		/*---------------------------- Edit header text ---------------------------*/
		jQuery("#headerText").click(function(){
			jQuery("#editHeading").show();
		});
		
		jQuery(".imgLogo").mouseover(function(){
			jQuery("#uploadCenterImage").css("display","block");
		});
		jQuery(".imgLogo").mouseout(function(){
			jQuery("#uploadCenterImage").css("display","block");
		});
		jQuery(".logo1").mouseover(function(){
			jQuery("#uploadImage1").css("display","block");
		});
		jQuery(".logo1").mouseout(function(){
			jQuery("#uploadImage1").css("display","block");
		});
		jQuery(".logo2").mouseover(function(){
			jQuery("#uploadImage2").css("display","block");
		});
		jQuery(".logo2").mouseout(function(){
			jQuery("#uploadImage2").css("display","block");
		});
		
		//Edit Detail
		jQuery(".editDetailHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			jQuery("#editComp_"+selectNum[1]+"_"+1).live("cut copy paste",function(e) {
			      e.preventDefault();
			});
			jQuery("#editComp_"+selectNum[1]+"_"+2).live("cut copy paste",function(e) {
			      e.preventDefault();
			});
			jQuery("#editComp_"+selectNum[1]+"_"+1).css("display","block");
			jQuery("#editComp_"+selectNum[1]+"_"+2).css("display","block");
			jQuery("#cust1").hide(); 
			jQuery("#cust2").hide(); 
			jQuery(".detailTextLbl").hide();
			jQuery("#Component_"+selectNum[1]).hide();
			jQuery("#okDetailHeading_"+selectNum[1]).show();
			jQuery("#editDetailHeading_"+selectNum[1]).hide();
			jQuery("#editComp_"+selectNum[1]+"_"+1).focus();
		});
		
		jQuery(".okDetailHeading").click(function(){
			okDetailHeading103(this);
		});
		
		/*jQuery("#customer").focusout(function(){
			
		});*/
		
		/*jQuery(".detailTbox").keydown(function(e){
			if(e.keyCode == 13){
				okDetailHeading103(this);
			}
		});*/
		
		jQuery(".focusOutSaveDetail").focusout(function(){
			var tb_val = jQuery(".detailCmpt").val();
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			var edit = "editDetailHeading_"+selectNum[1];
			var ok = "okDetailHeading_"+selectNum[1];
			var data = jQuery("#Component_"+selectNum[1]).val();
			jQuery("#detailText_"+selectNum[1]).html(tb_val);
			jQuery("#detailText_"+selectNum[1]).show();
			jQuery("#Component_"+selectNum[1]).hide();			
			jQuery("#"+ok).hide();
			jQuery("#"+edit).show();
		});
		
		//Edit Top
		jQuery(".editTopHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("TopHeading","topText",selectNum);
		});
		
		jQuery(".focusOutSaveTop").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					jQuery(".focusOutSaveTop").focus();
				}
				else {
					okTopCmpt("TopHeading","topText",selectNum);				
				}
			}
		});
		
		jQuery(".focusOutSaveTop").focusout(function(){
			okTop103(this);
		});
		
		jQuery(".okTopHeading").click(function(){
			okTop103(this);
		});
		
		//Edit Top Left Heading
		jQuery(".editTopLeftHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("TopLeftHeading","topLeft",selectNum);
		});
		
		jQuery(".okTopLeftHeading").click(function(){
			topLeft103(this);
		});
		
		jQuery(".focusOutSaveLeftHeading").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					jQuery(".focusOutSaveLeftHeading").focus();
				}
				else {
					okTopCmpt("TopLeftHeading","topLeft",selectNum);
				}
				jQuery(".focusOutSaveLeftHeading").focus();
			}
		});
		
		jQuery(".focusOutSaveLeftHeading").focusout(function(){
			topLeft103(this);
		});
		
		jQuery(document).click(function(){
			jQuery("#editText103").focus();
			jQuery(".focusOutSaveTop").focus();
			jQuery(".focusOutSaveLeftHeading").focus();
			jQuery(".focusOutSaveRightHeading").focus();
			jQuery(".focusOutSaveBLeftHeading").focus();
       });
		
		//Edit Top Left components
		jQuery(".deleteTopLeft").click(function(){
			var r = confirm(deleteAlert);
			if (r==true)
			{
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				deleteComp("TopLeft","topLeft",selectNum);
			}
			else{}
		});
		
		jQuery(".editTopLeft").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCmpt("TopLeft","topLeft",selectNum);
		});
		
		jQuery(".okTopLeft").click(function(){
			topLeftS103(this);
		});
		
		jQuery(".focusOutSaveLeft").keydown(function(e){
			if(e.keyCode == 13){
				topLeftS103(this);
			}
		});
		
		jQuery(".focusOutSaveLeft").focusout(function(){
			topLeftS103(this);
		});
		
		//Edit Top Right Heading
		jQuery(".editTopRightHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("TopRightHeading","topRight",selectNum);
		});
		
		jQuery(".focusOutSaveRightHeading").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					jQuery(".focusOutSaveRightHeading").focus();
				}
				else {
					okTopCmpt("TopRightHeading","topRight",selectNum);
				}
				jQuery(".focusOutSaveRightHeading").focus();
			}
		});
		
		jQuery(".focusOutSaveRightHeading").focusout(function(){
			topRightHeading103(this);
		});
		
		jQuery(".okTopRightHeading").click(function(){
			topRightHeading103(this);
		});
		
		//Edit Top Right Component
		jQuery(".deleteTopRight").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			var r = confirm(deleteAlert);
			if (r==true)
			{
				deleteComp("TopRight","topRight",selectNum);
			}
			else{}
		});
		
		jQuery(".editTopRight").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCmpt("TopRight","topRight",selectNum);
		});
		
		jQuery(".focusOutSaveRight").keydown(function(e){
			if(e.keyCode == 13){
				topRight103(this);
			}
		});
		
		jQuery(".focusOutSaveRight").focusout(function(){
			topRight103(this);
		});
		
		jQuery(".okTopRight").click(function(){
			topRight103(this);
		});
		
		//Edit Bottom Left Heading
		jQuery(".editBottomLeftHeading").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			editCompHeading("BottomLeftHeading","bottomLeft",selectNum);
		});
		
		jQuery(".focusOutSaveBLeftHeading").keydown(function(e){
			if(e.keyCode == 13){
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				if(jQuery("#Component_"+selectNum[1]).val()==""){
					jQuery(".focusOutSaveBLeftHeading").focus();
				}
				else {
					okTopCmpt("BottomLeftHeading","bottomLeft",selectNum);	
				}
				jQuery(".focusOutSaveBLeftHeading").focus();
			}
		});
		
		jQuery(".focusOutSaveBLeftHeading").focusout(function(){
			bottomLeftHeading103(this);
		});
		
		jQuery(".okBottomLeftHeading").click(function(){
			bottomLeftHeading103(this);
		});
		
		//Edit Bottom Left Component
		jQuery(".deleteBottomLeft").click(function(){
			
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");
			var r = confirm(deleteAlert);
			if (r==true)
			{
				deleteComp("BottomLeft","bottomLeft",selectNum);
			}
			else{}
		});
		
		jQuery(".editBottomLeft").click(function(){
			var selectId = jQuery(this).attr("id");
			var selectNum = selectId.split("_");			
			editCmpt("BottomLeft","bottomLeft",selectNum);
		});
		
		jQuery(".focusOutSaveBLeft").keydown(function(e){
			if(e.keyCode == 13){
				bottomLeft103(this);
			}
		});
		
		jQuery(".focusOutSaveBLeft").focusout(function(){
			bottomLeft103(this);
		});
		
		jQuery(".okBottomLeft").click(function(){
			bottomLeft103(this);	
		});
		
		//Edit Heading
		jQuery("#editHeading").click(function(){
			jQuery("#editText").css("display","block");
			jQuery("#editText").focus();
			jQuery("#headerText").hide();
			jQuery("#editHeading").hide();
			jQuery("#okHeading").show();
		});
		jQuery("#editText").keyup(function(e){
			if(e.keyCode == 13){
				if(jQuery("#editText").val()==""){
					alert(headingAlert);
				}else{
					saveEditText();
				}
			}
		});
		
		jQuery("#editText").keydown(function(e){
			if(e.keyCode == 13){
				if(jQuery("#editText").val()==""){
					alert(headingAlert);
				}
				else {
					saveEditText();
				}
			}
		});
		
		jQuery("#editText103").keydown(function(e){
		 if(e.keyCode == 13){
				var data = jQuery("#editText103").val();
				jQuery("#headerText").html(data);
				jQuery("#headingText").val(data);
				jQuery("#headerText").show();
				jQuery("#editText103").hide();
				jQuery("#okHeading103").hide();
				jQuery("#editHeading103").show();
				
			}
		});
		
		jQuery("#editText").focusout(function(){
			if(jQuery("#editText").val()==""){
				alert(headingAlert);
			}
			else {
				saveEditText();
			}
		});
		
		jQuery("#okHeading").click(function(){
			if(jQuery("#editText").val()==""){
				alert(headingAlert);
			}
			else {
				saveEditText();
			}
		});
		
		//Edit Heading 103
		jQuery("#editHeading103").click(function(){
			jQuery("#editText103").show();
			jQuery("#headerText").hide();
			jQuery("#editHeading103").hide();
			jQuery("#okHeading103").show();
			jQuery("#editText103").focus();
		});
		
		jQuery("#editText103").focusout(function(e){
			if(jQuery("#editText103").val()==""){
				alert(headingAlert);
				jQuery("#editText103").focusin();
			}
			else {
				var data = jQuery("#editText103").val();
				jQuery("#headerText").html(data);
				jQuery("#headingText").val(data);
				jQuery("#headerText").show();
				jQuery("#editText103").hide();
				jQuery("#okHeading103").hide();
				jQuery("#editHeading103").show();
			}
		});
		
		jQuery("#okHeading103").click(function(){
			if(jQuery("#editText103").val()==""){
				alert(headingAlert);
				jQuery("#editText103").focusin();
			}
			else {
				var data = jQuery("#editText103").val();
				jQuery("#headerText").html(data);
				jQuery("#headingText").val(data);
				jQuery("#headerText").show();
				jQuery("#editText103").hide();
				jQuery("#okHeading103").hide();
				jQuery("#editHeading103").show();
			}
		});
						
		jQuery(".ListDiv .iImageEdit").click(function() {
		   var edited = jQuery("#edited").val();
		   if(edited != "true"){
		   var preDiv = jQuery("#prevDiv").attr("value");
		   var componentDiv = jQuery(this).parent().attr("id");
		   
		   inActiveComponent(preDiv);
		   activeComponent(componentDiv);
		   jQuery("#prevDiv").attr("value",componentDiv);
		   jQuery("#currentDiv").attr("value",componentDiv);  
		   
		   if(isUniqueComponent(componentDiv, "hidden") != false){   
		   var componentNum = componentDiv.split("_");
		   serComponentVisible(componentNum);
		   var visibilityDiv = "visibility"+componentNum[1];
		   if(jQuery("#"+visibilityDiv).val() == "deleted"){
			   jQuery("#editComponent").css("display", "block");
			   jQuery("#CompDrp").val("0"); 
			   jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
			   jQuery("#compDetailsEdit").css("display", "none"); 
		   }
		   else{
				   var cmpName = "componentName"+componentNum[1];
				   var delButt = "delComp_"+componentNum[1];
				   var labelId = jQuery("#" + componentDiv).children().attr("id");
				   var imageId = jQuery("#" + labelId).next().attr("id");
				   var cmpId = jQuery("#" + imageId).next().attr("id");
				   var subLabelId = jQuery("#" + cmpId).next().attr("id");
				   jQuery("#"+delButt).css("visibility", "visible");
				   //jQuery("#changedHeading").val(jQuery("#" + cmpName).val());
				   jQuery("#current_label_id").attr("value",cmpName);
				   jQuery("#current_sublabel_id").attr("value",subLabelId);
				   jQuery("#options-table").empty();
				   jQuery("#options-table").css("display", "none");
				   inActiveComponent(preDiv);
				   activeComponent(componentDiv);
				   setComponents(componentDiv);
				   showOnTemplate(componentNum);
				   setHeadingDrp(componentNum);
				   setLabelDisplay(componentNum);
				   setEditOptionType(componentNum);
				   setOptionDescription(componentNum);
				   getOptionDrp(componentNum);
				   jQuery("#compDetailsEdit").css("display", "block");
				   jQuery("#addCompDetails").css("display","none");
				   jQuery("#addComponent").css("display", "none");
				   if(jQuery("#"+visibilityDiv).val() == "hidden"){
				   jQuery("#edited").val("true");
				   }else{
					   jQuery("#edited").val("false"); 
				   }
		   
		   }
		   }
		   else{
			   jQuery("#editComponent").css("display", "block");
			   //jQuery("#CompDrp option:selected").text("--Select--"); 
			   jQuery("#CompDrp").val("0"); 
			   jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
			   jQuery("#compDetailsEdit").css("display", "none");
		   }
		   }
		   else{
			   //alert("Please save or cancel the changes on component.");
			   alert("Please click Ok or Cancel.");
			   }
		  });
		/*if ($.browser.msie) {
			  $("#CompDrp").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}*/
	jQuery("#CompDrp").change(function(){
		var currDiv = jQuery("#currentDiv").val();
		var currNum = currDiv.split("_");
		jQuery("#createCompOptionDetails").css("display", "none");
		if(jQuery("#CompDrp option:selected").text() != "--Select--"){
		var compNum ="";
		var replaceComp = true;
		var selectedOpt = jQuery(this).attr("value");
		var currName = (jQuery("#CompDrp option:selected").text());
		var changedName = "";
		for(var i=1; i<=15; i++){
			var searchViewComp = "viewComponent"+i;
			if((parseInt(selectedOpt) == parseInt(jQuery("#"+searchViewComp).val())) && (i != parseInt(currNum[1])) ){
				var compDiv = "compDiv_"+i;
				if(((jQuery("#"+compDiv).css("visibility")) == "visible") || ((jQuery("#"+compDiv).css("visibility")) == "inherit")){
					var compName = "componentName"+i;
					changedName = (jQuery("#"+compName).val());
					replaceComp = false;
				}
			}
		}
		if(replaceComp != true){
			var curDiv  = jQuery("#currentDiv").val();
			compNum = curDiv.split("_");
			var cmpName = "componentName"+compNum[1];
			var currViewComp = "viewComponent"+compNum[1];
			jQuery("#CompDrp").val(jQuery("#"+currViewComp).val());
			jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
			if(currName != changedName){
				alert("'"+currName+"' changed as '"+changedName+"' and it is already in use. Choose another one.");
			}else{
				alert("The component is already in use. Choose another one.");
			}
		}else{
		jQuery.ajax({
			url :"./getAdminEditComponent.do",
			type :"POST",
			async : false,
			dataType : "html",
			data :"ufsId="+selectedOpt,
			success : function(data) {
				jQuery("#compDetailsEdit").empty();
				jQuery("#compDetailsEdit").html(data);
		   		},
	   	   complete: function(){
		   		jQuery("#editOptionType").val(jQuery("#selectOptType").val());
		   		jQuery("#compDetailsEdit").css("display", "block");
	 	   	    var curDiv  = jQuery("#currentDiv").val();
				compNum = curDiv.split("_");
				var headingSpan = "headingSpan"+compNum[1];
				jQuery("#"+headingSpan).text(jQuery("#CompDrp :selected").text());
				jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
				setSelectedLabel(compNum);
				setOptionDrpListRight(compNum);
				serComponentVisible(compNum);
				jQuery("#edited").val("true");
	 	   	   },
		   error : function(xmlhttp, error_msg) {
	   		  }
		  		});
		}
		}
      	});

		jQuery(".ListDiv .iDeleteEdit").click(function(){
			if(jQuery("#edited").val() == "true"){
				 //alert("Please save or cancel the changes on component.");
				alert("Please click Ok or Cancel.");
			 }else{
	        answer = confirm("Are you sure to delete?");
	        if(answer){
	        var curDiv = jQuery(this).parent().attr("id");
			compNum = curDiv.split("_");
			var curVisibilityDiv = "visibility"+compNum[1];
			var curr_compDiv = "compDiv_"+compNum[1];
			var labelSpanDiv = "labelSpan"+compNum[1];
			var editCompDiv = "editComp_"+compNum[1];
			var delCompDiv = "delComp_"+compNum[1];
			jQuery("#"+curr_compDiv).css("visibility", "hidden");
			jQuery("#"+labelSpanDiv).css("visibility", "hidden");
			jQuery("#"+curVisibilityDiv).val("hidden");
			jQuery("#"+editCompDiv).css("visibility", "visible");
			jQuery("#"+delCompDiv).css("visibility", "hidden");
			jQuery("#editComponent").css("display", "none");
			jQuery("#edited").val("false");
	        }
			}
			});
		
		jQuery("#editComponent #rightOk").click(function(){
			//alert(jQuery("#edited").val());
			if(jQuery("#edited").val() == "true"){
			jQuery("#createCompOptionDetails").css("display", "none");
			if((jQuery("#changedHeading").val()).length <= 0){
				alert("Please enter heading.");
			}else{
				var selectedOpt = jQuery("#editOption").val(); 
				if((isNaN(parseInt(selectedOpt))) == true){
					jQuery("#editOption").val(jQuery("#editOption option:first").val());
				}
				var curDiv  = jQuery("#currentDiv").val();
				compNum = curDiv.split("_");
				var curVisibilityDiv = "visibility"+compNum[1];
				jQuery("#"+curVisibilityDiv).val("visible");
				setEditdHeading(compNum);
				setEditedLabel(compNum);
				setEditedDesription(compNum);
				setViewComponentId(compNum);
				setOptionType(compNum);
				var delImgButt = "delComp_"+compNum[1];
				jQuery("#"+delImgButt).css("visibility", "visible");
				jQuery("#edited").val("false");
				jQuery("#editComponent").css("display", "none");
			}
			}else{
				jQuery("#edited").val("false");
				jQuery("#editComponent").css("display", "none");
				jQuery("#createCompOptionDetails").css("display", "none");
			}
			});
		
		jQuery("#editComponent #rightCancel").click(function(){
			var curDiv  = jQuery("#currentDiv").val();
			compNum = curDiv.split("_");
			var curVisibilityDiv = "visibility"+compNum[1];
			var visibility = jQuery("#"+curVisibilityDiv).val();
			setPrevHeading(compNum);
			setPrevLabel(compNum);
			setPrevDescription(compNum);
			jQuery("#edited").val("false");
			jQuery("#editComponent").css("display", "none");
			jQuery("#createCompOptionDetails").css("display", "none");
			if((visibility != "visible") && (visibility != "") ){
				var delImgButt = "delComp_"+compNum[1];
				jQuery("#"+delImgButt).css("visibility", "hidden");
				serComponentHidden(compNum);
			}
		});
		
		/*---------------------------- Browse Your Own Image ---------------------------*/
			
		// for adding images to the image library.
		jQuery('#lbImage').live('change', function(){ 
		jQuery("#saveBtn").hide();
		jQuery("#selectImg").val("");
		jQuery("#saveImage").val("");
			if(jQuery('#lbImage').val() != ""){
				if(jQuery("#selectImg").val()==""||jQuery("#selectImg").val()==null){
					jQuery("#deleteButton").hide();
				}
				jQuery("#loaderImg").css("display", "block");
				jQuery("#loaderImg").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				if(jQuery("#lbImage").val()!=""||jQuery("#lbImage").val()!=null){
					//jQuery("#saveBtn").html('<input type="button" value="Save" id="saveButton"/>');
					if(jQuery("#saveImage").val()==""||jQuery("#saveImage").val()== null){
						jQuery("#delBtn").html('<input type="button" value="Delete" id="deleteButton"/>');
					}
					else {
						jQuery("#deleteButton").show();
					}
				}
				else {
					jQuery("#saveBtn").hide();
				}
					var type=jQuery("#center").val();
					var imageHeight= jQuery("#centerimageHeight").val();
					var imageWidth= jQuery("#centerimageWidth").val();
					jQuery("#uploadLocalImage").attr("action","./editTemplate2.do?lbimage=yes&type=lbImage");
					 if(document.getElementById("lbImage").value != ""){
						 jQuery("#uploadLocalImage").ajaxForm({
							target: '#imgLogo',
							success: function(){
							 jQuery("#loaderImg").html('<img src="../images/loader.gif" alt="Uploading...."/>');
							 
							},
							complete: function(){
								jQuery("#loaderImg").css("display", "none");
								setTimeout( function() {
									jQuery("#saveBtn").html('<input type="button" style="float: right" value="Save" id="saveButton"/>');
								}, 500 );
								jQuery("#saveBtn").show();
							 	return false;
							}
					    }).submit();
					 }
				}
			});
		if (jQuery.browser.msie) {
			jQuery("#localImage").click(function() {
				setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
			jQuery('#localImage').live('change', function(){ 
				jQuery("#saveBtn").hide();
			jQuery("#selectImg").val("");
			jQuery("#saveImage").val("");
				if(jQuery('#localImage').val() != ""){
					if(jQuery("#selectImg").val()==""||jQuery("#selectImg").val()==null){
						jQuery("#deleteButton").hide();
					}
					jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
					if(jQuery("#localImage").val()!=""||jQuery("#localImage").val()!=null){
						//jQuery("#saveBtn").html('<input type="button" value="Save" id="saveButton"/>');
						if(jQuery("#saveImage").val()==""||jQuery("#saveImage").val()== null){
							jQuery("#delBtn").html('<input type="button" value="Delete" id="deleteButton"/>');
						}
						else {
							jQuery("#deleteButton").show();
						}
					}
					else {
						jQuery("#saveBtn").hide();
					}
						var isSmall;
						var type=jQuery("#center").val();
						var imageHeight= jQuery("#centerimageHeight").val();
						var imageWidth= jQuery("#centerimageWidth").val();
						jQuery("#uploadLocalImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
						 if(document.getElementById("localImage").value != ""){
							 jQuery("#uploadLocalImage").ajaxForm({
								target: '#imgLogo',
								success: function(){
									isSmall=jQuery("#smallImg").val();
								},
								complete: function(){
									if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
										jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');							
										var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
										var response = confirm(msg);
										if(response) {
											jQuery("#uploadLocalImage").attr("action","./editTemplate2.do?type="+type
										    	+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
										    jQuery("#uploadLocalImage").ajaxForm({
												target: '#imgLogo',
												success: function(){
							    	   			},
							    	   			complete: function(){
							    	   				jQuery("#smallImg").val("");
							    	   			}
										    }).submit();
										} else {
										    jQuery("#uploadLocalImage").attr("action","./editTemplate2.do?type="+type
										    	+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
										    jQuery("#uploadLocalImage").ajaxForm({
												target: '#imgLogo',
												success: function(){
						    	   				},
						    	   				complete: function(){
						    	   					jQuery("#smallImg").val("");
						    	   				}
										    }).submit();
										  }
									}
									/*setTimeout( function() {
										jQuery("#saveBtn").html('<input type="button" value="Save" id="saveButton"/>');
									}, 500 );
									jQuery("#saveBtn").show();*/
								 	return false;
								}
						    }).submit();
						 }
					}
				});
			if ($.browser.msie) {
				  $("#localImage104").click(function() {
					  setTimeout( function() {
						  this.focus();
						}, 500 );
				  });
				}
		jQuery('#localImage104').live('change', function(){ 
			jQuery("#saveBtn104").hide();
			jQuery("#selectImg").val("");
			jQuery("#saveImage").val("");
				if(jQuery('#localImage104').val() != ""){
					if(jQuery("#selectImg").val()==""||jQuery("#selectImg").val()==null){
						jQuery("#deleteButton104").hide();
					}
					jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
					if(jQuery("#localImage104").val()!=""||jQuery("#localImage104").val()!=null){
						//jQuery("#saveBtn104").html('<input type="button" value="Save" id="saveButton104"/>');
						if(jQuery("#saveImage104").val()==""||jQuery("#saveImage104").val()== null){
							jQuery("#delBtn104").html('<input type="button" value="Delete" id="deleteButton104"/>');
							
						}
						else {
							jQuery("#deleteButton104").show();
							
						}
					}
					else {
						jQuery("#saveBtn104").hide();
						
					}
						var isSmall;
						var type=jQuery("#center104").val();
						var imageHeight= jQuery("#centerimageHeight104").val();
						var imageWidth= jQuery("#centerimageWidth104").val();
						jQuery("#uploadLocalImage104").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
						 if(document.getElementById("localImage104").value != ""){
							 jQuery("#uploadLocalImage104").ajaxForm({
									target: '#imgLogo',
									success: function(){
									isSmall=jQuery("#smallImg").val();
							 },
								complete: function(){
									if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
										jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
										var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
										var response = confirm(msg);
										if(response) {
											jQuery("#uploadLocalImage104").attr("action","./editTemplate2.do?type="+type
										    	+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
										    jQuery("#uploadLocalImage104").ajaxForm({
												target: '#imgLogo',
												success: function(){
							    	   			},
							    	   			complete: function(){
							    	   				jQuery("#smallImg").val("");
							    	   			}
										    }).submit();
										} else {
										    jQuery("#uploadLocalImage104").attr("action","./editTemplate2.do?type="+type
										    	+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
										    jQuery("#uploadLocalImage104").ajaxForm({
												target: '#imgLogo',
												success: function(){
						    	   				},
						    	   				complete: function(){
						    	   					jQuery("#smallImg").val("");
						    	   				}
										    }).submit();
										  }
									}
									/*setTimeout( function() {
										jQuery("#saveBtn104").html('<input type="button" value="Save" id="saveButton104"/>');
									}, 500 );
									jQuery("#saveBtn104").show();*/
								 	return false;
								}
						         }).submit();
						 }
				}
			});
                        
                        if ($.browser.msie) {
				  $("#localImage105").click(function() {
					  setTimeout( function() {
						  this.focus();
						}, 500 );
				  });
				}
                                
                        jQuery('#localImage105').live('change', function(){ 
			jQuery("#saveBtn105").hide();
			jQuery("#selectImg").val("");
			jQuery("#saveImage").val("");
				if(jQuery('#localImage105').val() != ""){
					if(jQuery("#selectImg").val()==""||jQuery("#selectImg").val()==null){
						jQuery("#deleteButton105").hide();
					}
					jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
					if(jQuery("#localImage105").val()!=""||jQuery("#localImage105").val()!=null){
						if(jQuery("#saveImage105").val()==""||jQuery("#saveImage105").val()== null){
							jQuery("#delBtn105").html('<input type="button" value="Delete" id="deleteButton105"/>');
							
						}
						else {
							jQuery("#deleteButton105").show();
							
						}
					}
					else {
						jQuery("#saveBtn105").hide();
						
					}
						var isSmall;
						var type=jQuery("#center105").val();
						var imageHeight= jQuery("#centerimageHeight105").val();
						var imageWidth= jQuery("#centerimageWidth105").val();
						jQuery("#uploadLocalImage105").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
						 if(document.getElementById("localImage105").value != ""){
							 jQuery("#uploadLocalImage105").ajaxForm({
									target: '#imgLogo',
									success: function(){
									isSmall=jQuery("#smallImg").val();
							 },
								complete: function(){
									if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
										jQuery("#imgLogo").html('<img src="../images/loader.gif" alt="Uploading...."/>');
										var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
										var response = confirm(msg);
										if(response) {
											jQuery("#uploadLocalImage105").attr("action","./editTemplate2.do?type="+type
										    	+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
										    jQuery("#uploadLocalImage105").ajaxForm({
												target: '#imgLogo',
												success: function(){
							    	   			},
							    	   			complete: function(){
							    	   				jQuery("#smallImg").val("");
							    	   			}
										    }).submit();
										} else {
										    jQuery("#uploadLocalImage105").attr("action","./editTemplate2.do?type="+type
										    	+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
										    jQuery("#uploadLocalImage105").ajaxForm({
												target: '#imgLogo',
												success: function(){
						    	   				},
						    	   				complete: function(){
						    	   					jQuery("#smallImg").val("");
						    	   				}
										    }).submit();
										  }
									}
									return false;
								}
						         }).submit();
						 }
				}
			});
                        
                        
		
		$(document).keypress(function(e) {
			//alert(e.keyCode);
		    if(e.keyCode == 13 && $("#localImage").val()=="") {
		      //  alert('You pressed enter!..'+($("#localImage").val()==""));
		        return false;
		    }
		}); 
		if ($.browser.msie) {
			  $("#leftImage").click(function() {
				  setTimeout( function() {
					  //this.blur();
					  this.focus();
					}, 500 );
			    
			  });
			}
		
		jQuery('#leftImage').live('change', function(){ 
			if(jQuery('#leftImage').val() != ""){
				jQuery("#imgLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var isSmall;
				var type=jQuery("#left").val();
				var imageHeight= jQuery("#leftimageHeight").val();
				var imageWidth= jQuery("#leftimageWidth").val();
				jQuery("#uploadLeftImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				
				jQuery("#uploadLeftImage").ajaxForm({
					target: '#imgLeft',
					success: function(){
					isSmall=jQuery("#smallImg").val();
				},
				complete: function(){
					if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
						jQuery("#imgLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
						var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
						var response = confirm(msg);
					       if(response) {
					    	   jQuery("#uploadLeftImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
					    	   jQuery("#uploadLeftImage").ajaxForm({
									target: '#imgLeft',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();

					       } else {
					    	   jQuery("#uploadLeftImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
					    	   jQuery("#uploadLeftImage").ajaxForm({
									target: '#imgLeft',
									success: function(){
					    		   		//alert("not stretched...");
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();
					       }
					}
				  }	
		           }).submit();
					    	   
			}
		});
		if ($.browser.msie) {
			  $("#bottomLeftImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomLeftImage').live('change', function(){ 
			if(jQuery('#bottomLeftImage').val() != ""){
				jQuery("#imgBottomLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=jQuery("#bottomLeft").val();
				var imageHeight= jQuery("#bottomLeftimageHeight").val();
				var imageWidth= jQuery("#bottomLeftimageWidth").val();
				var isSmall;
				jQuery("#uploadBottomLeftImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				 
				jQuery("#uploadBottomLeftImage").ajaxForm({
					target: '#imgBottomLeft',
					success: function(){
					isSmall=jQuery("#smallImg").val();
				},
				complete: function(){
					if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
						jQuery("#imgBottomLeft").html('<img src="../images/loader.gif" alt="Uploading...."/>');
						var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
						var response = confirm(msg);
					       if(response) {
					    	   jQuery("#uploadBottomLeftImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
					    	   jQuery("#uploadBottomLeftImage").ajaxForm({
									target: '#imgBottomLeft',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();

					       } else {
					    	   jQuery("#uploadBottomLeftImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
					    	   jQuery("#uploadBottomLeftImage").ajaxForm({
									target: '#imgBottomLeft',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();
					       }
					}
				  }	
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomRightImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomRightImage').live('change', function(){ 
			if(jQuery('#bottomRightImage').val() != ""){
				jQuery("#imgBottomRight").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var type=jQuery("#bottomRight").val();
				var imageHeight= jQuery("#bottomRightimageHeight").val();
				var imageWidth= jQuery("#bottomRightimageWidth").val();
				 jQuery("#uploadBottomRightImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				 var isSmall;
				jQuery("#uploadBottomRightImage").ajaxForm({
					target: '#imgBottomRight',
						success: function(){
						isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
						if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
							jQuery("#imgBottomRight").html('<img src="../images/loader.gif" alt="Uploading...."/>');
							var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
							var response = confirm(msg);
						       if(response) {
						    	   jQuery("#uploadBottomRightImage").attr("action","./editTemplate2.do?type="+type
						    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
						    	   jQuery("#uploadBottomRightImage").ajaxForm({
										target: '#imgBottomRight',
										success: function(){
			    	   					},
						    	        complete: function(){
			    	   						jQuery("#smallImg").val("");
			    	   					}
						    	   }).submit();

						       } else {
						    	   jQuery("#uploadBottomRightImage").attr("action","./editTemplate2.do?type="+type
						    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
						    	   jQuery("#uploadBottomRightImage").ajaxForm({
										target: '#imgBottomRight',
										success: function(){
			    	   					},
						    	        complete: function(){
			    	   						jQuery("#smallImg").val("");
			    	   					}
						    	   }).submit();
						       }
						}
					  }	
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomLastImage").click(function() {
				  setTimeout( function() {
					  this.focus();
				  }, 500 );
			  });
			}
		jQuery('#bottomLastImage').live('change', function(){ 
			if(jQuery('#bottomLastImage').val() != ""){
				jQuery("#imgBottomLast").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var isSmall;
				var type=jQuery("#bottomLast").val();
				var imageHeight= jQuery("#bottomLastimageHeight").val();
				var imageWidth= jQuery("#bottomLastimageWidth").val();
				 jQuery("#uploadBottomLastImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				jQuery("#uploadBottomLastImage").ajaxForm({
					target: '#imgBottomLast',
					success: function(){
					isSmall=jQuery("#smallImg").val();
				},
				complete: function(){
					if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
						jQuery("#imgBottomLast").html('<img src="../images/loader.gif" alt="Uploading...."/>');
						var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
						var response = confirm(msg);
					       if(response) {
					    	   jQuery("#uploadBottomLastImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
					    	   jQuery("#uploadBottomLastImage").ajaxForm({
									target: '#imgBottomLast',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();

					       } else {
					    	   jQuery("#uploadBottomLastImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
					    	   jQuery("#uploadBottomLastImage").ajaxForm({
									target: '#imgBottomLast',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();
					       }
					}
				  }	
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomMidImage").click(function() {
				  setTimeout( function() {
					  this.focus();
				  }, 500 );
			  });
			}
		jQuery('#bottomMidImage').live('change', function(){ 
			if(jQuery('#bottomMidImage').val() != ""){
				jQuery("#imgBottomMid").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var isSmall;
				var type=jQuery("#bottomMid").val();
				var imageHeight= jQuery("#bottomMidimageHeight").val();
				var imageWidth= jQuery("#bottomMidimageWidth").val();
				 jQuery("#uploadBottomMidImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				
				jQuery("#uploadBottomMidImage").ajaxForm({
					target: '#imgBottomMid',
					success: function(){
					isSmall=jQuery("#smallImg").val();
				},
				complete: function(){
					if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
						jQuery("#imgBottomMid").html('<img src="../images/loader.gif" alt="Uploading...."/>');
						var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
						var response = confirm(msg);
					       if(response) {
					    	   jQuery("#uploadBottomMidImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
					    	   jQuery("#uploadBottomMidImage").ajaxForm({
									target: '#imgBottomMid',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();

					       } else {
					    	   jQuery("#uploadBottomMidImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
					    	   jQuery("#uploadBottomMidImage").ajaxForm({
									target: '#imgBottomMid',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();
					       }
					}
				  }	
		           }).submit();
			}
		});
		if ($.browser.msie) {
			  $("#bottomCenterImage").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}
		jQuery('#bottomCenterImage').live('change', function(){ 
			if(jQuery('#bottomCenterImage').val() != ""){
				jQuery("#imgBottomCenter").html('<img src="../images/loader.gif" alt="Uploading...."/>');
				var isSmall;
				var type=jQuery("#bottomCenter").val();
				var imageHeight= jQuery("#bottomCenterimageHeight").val();
				var imageWidth= jQuery("#bottomCenterimageWidth").val();
				 jQuery("#uploadBottomCenterImage").attr("action","./editTemplate2.do?type="+type+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth);
				jQuery("#uploadBottomCenterImage").ajaxForm({
					target: '#imgBottomCenter',
					success: function(){
					isSmall=jQuery("#smallImg").val();
				},
				complete: function(){
					if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
						jQuery("#imgBottomCenter").html('<img src="../images/loader.gif" alt="Uploading...."/>');
						var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
						var response = confirm(msg);
					       if(response) {
					    	   jQuery("#uploadBottomCenterImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=yes");
					    	   jQuery("#uploadBottomCenterImage").ajaxForm({
									target: '#imgBottomCenter',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();

					       } else {
					    	   jQuery("#uploadBottomCenterImage").attr("action","./editTemplate2.do?type="+type
					    			   +"&imageHeight="+imageHeight+"&imageWidth="+imageWidth+"&stretch=no");
					    	   jQuery("#uploadBottomCenterImage").ajaxForm({
									target: '#imgBottomCenter',
									success: function(){
		    	   					},
					    	        complete: function(){
		    	   						jQuery("#smallImg").val("");
		    	   					}
					    	   }).submit();
					       }
					}
				  }	
		           }).submit();
			}
		});
		
		/*---------------------------- Click on Save Button Image ---------------------------*/
		jQuery("#saveButton").live('click',function() { 
			var saveLogo = jQuery("#saveLogo").val();
			if(jQuery("#localImage").val()==''){
				alert("Please select any image.");
			}
			else{
				jQuery.ajax({
					url : "./saveLogoImage.do?saveLogo=" + saveLogo,
					type : "POST",
					dataType : "html",
					data : "vardd=1",
					async: false,
					success : function(data) {
					jQuery("#saveBtn").html(data);
						//alert("Your image successfully saved.");
					},
					complete: function(){
						var msg = jQuery("#msgImageSave").val();
						alert(msg);
					}
				});
			}
		});	
		
		jQuery("#saveButton104").live('click',function() { 
			var saveLogo = jQuery("#saveLogo").val();
			if(jQuery("#localImage104").val()==''){
				alert("Please select any image.");
			}
			else{
				jQuery.ajax({
					url : "./saveLogoImage.do?saveLogo=" + saveLogo,
					type : "POST",
					dataType : "html",
					data : "vardd=1",
					async: false,
					success : function(data) {
					jQuery("#saveBtn").html(data);
						//alert("Your image successfully saved.");
					},
					complete: function(){
						var msg = jQuery("#msgImageSave").val();
						alert(msg);
						jQuery("#saveBtn104").hide();
					}
				});
			}
		});	
                
                jQuery("#saveButton105").live('click',function() { 
			var saveLogo = jQuery("#saveLogo").val();
			if(jQuery("#localImage105").val()==''){
				alert("Please select any image.");
			}
			else{
				jQuery.ajax({
					url : "./saveLogoImage.do?saveLogo=" + saveLogo,
					type : "POST",
					dataType : "html",
					data : "vardd=1",
					async: false,
					success : function(data) {
					jQuery("#saveBtn").html(data);
						//alert("Your image successfully saved.");
					},
					complete: function(){
						var msg = jQuery("#msgImageSave").val();
						alert(msg);
						jQuery("#saveBtn105").hide();
					}
				});
			}
		});	
                
                
		
		jQuery("#deleteButton").live('click',function() { 
			var r = confirm(deleteAlert);
			if (r==true)
			{
				jQuery("#localImage").val('');
				jQuery("#selectImg").val('');
				jQuery("#imgLogo").empty();
				jQuery("#saveImage").val('');
				jQuery("#saveButton").hide();
				jQuery("#deleteButton").hide();
				jQuery("#imgLogo").append('<p id="msg">(Your logo here)</p>');
			}else{}	
		});	
		
		jQuery("#deleteButton104").live('click',function() { 
			var r = confirm(deleteAlert);
			if (r==true)
			{
				jQuery("#localImage104").val('');
				jQuery("#selectImg").val('');
				jQuery("#imgLogo").empty();
				jQuery("#saveImage104").val('');
				jQuery("#saveButton104").hide();
				jQuery("#deleteButton104").hide();
				jQuery("#imgLogo").append('<p id="msg">(Your logo here)</p>');
			}else{}	
		});	
                
                jQuery("#deleteButton105").live('click',function() { 
			var r = confirm(deleteAlert);
			if (r==true)
			{
				jQuery("#localImage105").val('');
				jQuery("#selectImg").val('');
				jQuery("#imgLogo").empty();
				jQuery("#saveImage105").val('');
				jQuery("#saveButton105").hide();
				jQuery("#deleteButton105").hide();
				jQuery("#imgLogo").append('<p id="msg">(Your logo here)</p>');
			}else{}	
		});	
		
		jQuery("#deleteButton1").live('click',function() {
			var r = confirm(deleteAlert);
			if (r==true)
			{
				jQuery("#localImage").val('');
				jQuery("#selectImg").val('');
				jQuery("#imgLogo").empty();
				jQuery("#saveImage").val('');
				jQuery("#saveButton").hide();
				jQuery("#deleteButton1").hide();
				jQuery("#imgLogo").append('<p id="msg" style="margin-top: 10%;">(Your logo here)</p>');
			}else{}	
		});	
		
		jQuery(".deleteForm").live('click',function(){
			var deleteId = jQuery(this).attr("id");
			var numId = deleteId.split("_");
			var numDeleteId = numId[1]; 
			var formeNameDiv = "name_"+numDeleteId;
			var formName = jQuery("#"+formeNameDiv).html();
			var r = confirm("Are you sure you want to delete '"+formName+"'?");
			if (r==true)
			{
			var userFormId = "userFormId_"+numDeleteId;
			var userIdIfs = "userIdIfs_"+numDeleteId;
			var formId = jQuery("#"+userFormId).val();
			var userId = jQuery("#"+userIdIfs).val();
			jQuery.ajax({
				url : "./navigateShowUserDelete.do?formid="+formId+"&userid="+userId,
				type : "POST",
				dataType : "html",
				data : "vardd=1",
				success : function(data) {
					jQuery("#formLine"+numDeleteId).remove();
					jQuery("#innerData").empty();
					jQuery("#innerData").html(data);
				},
				complete: function(){
					jQuery("#formLine"+numDeleteId).remove();
				}
			});
			}else{}
		});
			
		/*---------------------------- Browse From Repository ---------------------------*/
		jQuery('#browse').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRImage").ajaxForm({
				target: '#lbox'
	           }).submit();
		});
		
		jQuery('#browse104').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRImage104").ajaxForm({
				target: '#lbox104'
	           }).submit();
		});
                
                jQuery('#browse105').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRImage105").ajaxForm({
				target: '#lbox105'
	           }).submit();
		});
		
		jQuery('#browseLeft').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRLeftImage").ajaxForm({
				target: '#leftBox'
	           }).submit();
		});
		
		jQuery('#browseBottomLeft').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomLeftImage").ajaxForm({
				target: '#bottomLeftBox'
	           }).submit();
		});
		
		jQuery('#browseBottomRight').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomRightImage").ajaxForm({
				target: '#bottomRightBox'
	           }).submit();
		});
		
		jQuery('#browseBottomLast').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomLastImage").ajaxForm({
				target: '#bottomLastBox'
	           }).submit();
		});
		
		jQuery('#browseBottomMid').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomMidImage").ajaxForm({
				target: '#bottomMidBox'
	           }).submit();
		});
		
		jQuery('#browseBottomCenter').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRBottomCenterImage").ajaxForm({
				target: '#bottomCenterBox'
	           }).submit();
		});
		
		jQuery('#browseRepository').live('click',function(){
			closeAllDiv();
			jQuery("#uploadRepositoryImage").ajaxForm({
				target: '#repositoryBox'
	           }).submit();
		});
		
		/*---------------------------- Delete Image From Repository ---------------------------*/
		jQuery('.deleteLink').live('click',function(){
			var r = confirm(deleteAlert);
			if (r==true)
			{
				var deleteId = jQuery(this).attr("id");
				var deleteNum = deleteId.split("_");
				var imageid = "imageId_"+deleteNum[1]; 
				var imgId = jQuery("#"+imageid).val();
				var boxesdiv = "boxesdiv"+deleteNum[1];
				var pageId=jQuery("#pageId").val();
				var patt1=/imageDelete/gi;
				if(pageId.match(patt1)!=null){
					pageId=pageId.substr(0,pageId.indexOf("imageDelete")-1);
					}
				var patt2=/type/gi;
				if(pageId.match(patt2)!=null){
					
					var typeIndex=pageId.indexOf("type");
					var temp4=pageId.substring(typeIndex,pageId.indexOf("&",typeIndex));
					pageId=pageId.replace(temp4+"&","");
					}
					var pageNo=pageId.split("=");
					
				var imgSize=jQuery("#imageSize").val();
				var rem = imgSize%6;
				
				var lastPage = Math.ceil(imgSize/6);				
				jQuery.ajax({
					url : "./lightBoxController.do?d-49489-p="+pageNo[1],
					type : "POST",	
					dataType : "html",
					data : "imageDelete="+imgId,					
					success : function(data) {					
						if(rem == 1 && pageNo[1] == lastPage){
							var lastPageIdList=pageId.lastIndexOf("=");
							var str= pageId.substr(0,lastPageIdList+1);
							pageId = str+ parseInt(parseInt(pageNo[1]-1));
							jQuery.ajax({
								url : pageId,
								type : "POST",
								dataType : "html",
								success : function(data) {
									jQuery("#"+boxesdiv).remove();
									jQuery("#image").html(data);
									jQuery("#pageId").val(pageId);
								},
								complete: function(){
									jQuery("#"+boxesdiv).remove();
									jQuery("#imageSize").val(imgSize-1);
									jQuery("#pageId").val(pageId);
								}							
							});						
						}
						else
							{
							jQuery("#image").html(data);
							jQuery("#pageId").val(pageId);
							jQuery("#imageSize").val(imgSize-1);
							
							}						
					},
					complete: function(){
						
					}
				});
			}else{}
		});
		
		/*---------------------------- Select Image From Repository ---------------------------*/
		jQuery('.selectLink').live('click',function(){
			var iType = jQuery("#place").val();
			jQuery("#localImage").val('');
			if(jQuery("#localImage").val()==""||jQuery("#localImage").val()==null){
				jQuery("#deleteButton").hide();
				jQuery("#saveButton").hide();
			}
			if(jQuery("#saveImage").val()==""||jQuery("#saveImage").val()== null){
				jQuery("#delBtn1").html('<input type="button" value="Delete" id="deleteButton1" style="margin-left: 3px;margin-top: -3px;"/>');
			}
			else if(jQuery("#saveImage").val()!=""||jQuery("#saveImage").val()!= null){
				jQuery("#deleteButton1").show();
			}
			var selectId = jQuery(this).attr("id");
			var userRole = jQuery("#userRole").val();
			var selectNum = selectId.split("_");
			var imageid = "imageName_"+selectNum[1]; 
			var imgName = jQuery("#"+imageid).val();
			var imageHeight= $("#reqHeight").val();
			var imageWidth= $("#reqWidth").val();
			jQuery("#selectImg").val(imgName);
				jQuery.ajax({
					url : "./showRepositoryImage.do?imgName="+imgName+"&type="+iType+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth,
					type : "POST",
					dataType : "html",
					data : "vardd=1",
					success : function(data) {
						if(iType == 'center'){
							jQuery("#imgLogo").empty();
							jQuery("#imgLogo").html(data);
						}
						else if(iType == 'center104'){
							jQuery("#imgLogo").empty();
							jQuery("#imgLogo").html(data);
						}
                                                else if(iType == 'center105'){
							jQuery("#imgLogo").empty();
							jQuery("#imgLogo").html(data);
						}
						else if(iType == 'left'){
							jQuery("#imgLeft").empty();
							jQuery("#imgLeft").html(data);
						}
						else if(iType == 'bottomLeft'){
							jQuery("#imgBottomLeft").empty();
							jQuery("#imgBottomLeft").html(data);
						}
						else if(iType == 'bottomRight'){
							jQuery("#imgBottomRight").empty();
							jQuery("#imgBottomRight").html(data);
						}
						else if(iType == 'bottomLast'){
							jQuery("#imgBottomLast").empty();
							jQuery("#imgBottomLast").html(data);
						}
						else if(iType == 'bottomMid'){
							jQuery("#imgBottomMid").empty();
							jQuery("#imgBottomMid").html(data);
						}
						else if(iType == 'bottomCenter'){
							jQuery("#imgBottomCenter").empty();
							jQuery("#imgBottomCenter").html(data);
						}
						else if(iType == 'template1'){
							jQuery("#imgLogo").empty();
							jQuery("#imgLogo").html(data);
						}
						var name = "#imgDiv"+selectNum[1];
						jQuery(".unHighlighted").css("background-color","");
						jQuery(name).css("background-color","grey");
						isSmall=jQuery("#smallImg").val();
					},
					complete: function(){
						if(isSmall != null && (isSmall == 'yes' ||isSmall == 'no')){
							var stretch;							
							var msg = "Do you want to stretch the image?\n\n** Click OK to strech it.\n** Click Cancel to retain the same size.";
							var response = confirm(msg);
							if(response){
								stretch="yes";
							}else{
								stretch="no";
							}
							jQuery.ajax({
								url : "./showRepositoryImage.do?imgName="+imgName+"&type="+iType+"&stretch="+stretch+"&imageHeight="+imageHeight+"&imageWidth="+imageWidth,
								type : "POST",
								dataType : "html",
								data : "vardd=1",
								success : function(data) {
									if(iType == 'center'){
										jQuery("#imgLogo").empty();
										jQuery("#imgLogo").html(data);
									}
									else if(iType == 'center104'){
										jQuery("#imgLogo").empty();
										jQuery("#imgLogo").html(data);
									}
                                                                        else if(iType == 'center105'){
										jQuery("#imgLogo").empty();
										jQuery("#imgLogo").html(data);
									}
									else if(iType == 'left'){
										jQuery("#imgLeft").empty();
										jQuery("#imgLeft").html(data);
									}
									else if(iType == 'bottomLeft'){
										jQuery("#imgBottomLeft").empty();
										jQuery("#imgBottomLeft").html(data);
									}
									else if(iType == 'bottomRight'){
										jQuery("#imgBottomRight").empty();
										jQuery("#imgBottomRight").html(data);
									}
									else if(iType == 'bottomLast'){
										jQuery("#imgBottomLast").empty();
										jQuery("#imgBottomLast").html(data);
									}
									else if(iType == 'bottomMid'){
										jQuery("#imgBottomMid").empty();
										jQuery("#imgBottomMid").html(data);
									}
									else if(iType == 'bottomCenter'){
										jQuery("#imgBottomCenter").empty();
										jQuery("#imgBottomCenter").html(data);
									}
									else if(iType == 'template1'){
										jQuery("#imgLogo").empty();
										jQuery("#imgLogo").html(data);
									}
								},
								complete: function(){
									jQuery("#smallImg").val("");
								  }
							});
							
						}
					}
				});
		});
		/*if ($.browser.msie) {
			  $("#editOptionType").click(function() {
				  setTimeout( function() {
					  this.focus();
					}, 500 );
			  });
			}*/
		jQuery("#editOptionType").live("change", function(){
				jQuery("#edited").val("true");
		    	var imageOptionType = jQuery("#editOptionType").val();
		    	var imageOptionDiv = jQuery("#optionList").find(".imageOptionType");
		    	var curDiv  = jQuery("#currentDiv").val();
				var compNum = curDiv.split("_");
				var optDiv = "optDiv"+compNum[1];
				var optDivImage = jQuery("#"+optDiv).find(".imageOptionType");
		    	var image = "";
		    	if(imageOptionType.toLowerCase() == "square"){
		    		image = "../images/square.png";
		    	}
		    	else if(imageOptionType.toLowerCase() == "circle"){
		    		image = "../images/circle.png";
		    	}
		    	else{
		    		image = "../images/none.png";
		    	}
	    		jQuery(imageOptionDiv).each(function(index){
	    			var imageId = jQuery(this).attr("id");
		    		jQuery("#"+imageId).attr("src", image);	    		
		    		});
	    		jQuery(optDivImage).each(function(index){
	    			var imageId = jQuery(this).attr("id");
		    		jQuery("#"+imageId).attr("src", image);	    		
		    		});
	    		
		});
		
		jQuery(".notSelected").live("click", function(){
			var notSelectedId = jQuery(this).attr("id");
			var selectedDiv = jQuery("#centercontainer").find(".selected");
			jQuery(selectedDiv).each(function(index) {
	    	    var selectedId = jQuery(this).attr("id");
	    	    jQuery("#"+selectedId).removeClass("selected");
	    	    jQuery("#"+selectedId).addClass("notSelected");
	        });
			jQuery("#"+notSelectedId).removeClass("notSelected");
			jQuery("#"+notSelectedId).addClass("selected");
		});
		
		jQuery(".addEditOption").live("click", function(){
			jQuery("#edited").val("true");
			var addId = jQuery(this).attr("id");
			var addNum = addId.split("_");
			var componentDiv = jQuery("#currentDiv").val();
			var componentNum = componentDiv.split("_");
	        var curOptDiv = "optDiv"+componentNum[1];
	        var index = parseInt(addNum[1]);
	        var imageType = jQuery("#editOptionType").val();
	        var currValDiv = "optiontext_"+addNum[1];
	        var currVal = jQuery("#"+currValDiv).val();
	        var optionValueDiv = "optionValue"+componentNum[1];
	        var maxLength = 32;
	        var addFlag = true;
	        if((imageType.toLowerCase()) == "square" ){
	        	image = "../images/square.png";
	        }
	        else if((imageType.toLowerCase()) == "circle"){
	        	image = "../images/circle.png";
	        }
	        else{
	        	image = "../images/none.png";
	        }
	        
	        getOptionDesc(optionValueDiv);
	        var optionValue = (jQuery("#"+optionValueDiv).val()).split("^");
	        var optionValueLen = ((jQuery("#"+optionValueDiv).val()).length)+(optionValue.length * 2);
	       // alert("optionValueLen : "+optionValueLen+" maxLength : "+maxLength);
	        if(parseInt(componentNum[1]) == 15){
	        	if(optionValue.length  >= 3){
	        		addFlag = false;
	        		alert("Cannot add more than three options.");
	        	}
	        	maxLength = 96;
	        }
	        if(currVal.length == 0){
	        	var lastOptDiv = "add_"+(index);
	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
	        	alert("Please enter options.");
	        }
	        else if((optionValueLen < maxLength) && (addFlag == true)){
	        	for(var i=1; i<=parseInt(addNum[1]); i++){
					var addbutt = "add_"+i;
					if(jQuery("#"+addbutt).length != 0 ){
						jQuery("#"+addbutt).css("display", "none");
						}
					}
	        	jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_'+(index+1)+'"><span class="checkbox" style="display:block;"><img src="'+image+'" class="imageOptionType" id="editImage_'+(index+1)+'">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="" style="width: 100px;" /><input type="button" name="remove_'+(index+1)+'" value="" id="remove_'+(index+1)+'" class="deleteEditOption"/><input type="button" name="add_'+(index+1)+'" value="" id="add_'+(index+1)+'" class="addEditOption" /></span></div>');
	        	if(componentNum[1] != 15){
		  	    	jQuery("#"+curOptDiv).append('<div class="checkbox" ><span><img src="'+image+'" class="imageOptionType" id="image_'+componentNum[1]+""+(index+1)+'"><label id="opt'+componentNum[1]+""+(index+1)+'" class="optionText"></label></span></div>');
		  	    }
		  	    else{
		  	    	jQuery("#"+curOptDiv).append('<div class="checkbox15" ><span><img src="'+image+'" class="imageOptionType" id="image_'+componentNum[1]+""+(index+1)+'"><label id="opt'+componentNum[1]+""+(index+1)+'" class="optionText"></label></span></div>');
			  	    }
	        	var lastOptDiv = "add_"+(index+1);
	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
	        }
	        else if(optionValueLen >= maxLength){
	        	var lastOptDiv = "add_"+(index);
	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
	        	alert("Cannot add more.");
	        }
		});
		
		jQuery(".deleteEditOption").live("click", function(){
			jQuery("#edited").val("true");
			var delId = jQuery(this).attr("id");
			var cuurOption = delId.split("_");
			var cuurOptionnum = parseInt(cuurOption[1]);
			var componentDiv = jQuery("#currentDiv").val();
			var componentNum = componentDiv.split("_");
			var imageDiv = "image_"+componentNum[1]+""+cuurOption[1];
			var optDiv = "opt"+componentNum[1]+""+cuurOption[1];
			var removeDiv = "checkboxcnt_"+cuurOptionnum;
			var lastoptDiv = jQuery("#lastEditOptionDiv").val();
			var lastDiVNum = lastoptDiv.split("_");
			var adddiv1 = "add_"+(cuurOptionnum-1);
			if(cuurOptionnum == parseInt(lastDiVNum[1])){
				var adddiv = getLastDiv(lastDiVNum[1]);
				jQuery("#"+adddiv).css("display", "block");
				jQuery("#"+adddiv).css("width", "16px");
				jQuery("#"+adddiv).css("float", "right");
				jQuery("#"+adddiv).css("margin-right", "148px");
				jQuery("#"+adddiv).css("margin-top", "5px");
				jQuery("#lastEditOptionDiv").val(adddiv);
				}
			jQuery("#"+imageDiv).remove();
			jQuery("#"+optDiv).remove();
			jQuery("#"+removeDiv).remove();
			var optionList = jQuery("#optionList").find(".optiontext");
			if(optionList.length < 1){
				//jQuery("#editDesc").append('<div><div style="float:right;"><input id="createNewOption" class="btn_forright" type="button" value="Add"></div></div>');
				jQuery("#editDesc").append('<div id="addOptionDiv"><div style="float:left;"><div id="createNewOptionDiv" class="sideheading"><label>Add Option</label>&nbsp;&nbsp<img src="../images/Button-Add-icon.png" id="createNewOption" /></div></div></div>');
			}
		});
		
		jQuery("#createNewOption").live('click',function(){
			var curDiv = jQuery("#currentDiv").attr("value");
			var compNum = curDiv.split("_");
			var imageType = (jQuery("#editOptionType option:selected").text()).toLowerCase();
			jQuery("#optionList").empty();
			jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_1"><span class="checkbox" style="display:block;"><img src="../images/'+imageType+'.png" class="imageOptionType" id="image1">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_1" value="" style="width: 100px;" onkeyup="editOtions(this.id)"/><input type="button" name="remove_1" value="" id="remove_1" class="deleteEditOption"/><input type="button" name="add_1" value="" id="add_1" class="addEditOption"/></span></div>');
			setOptionDrpListRight(compNum);
			jQuery("#addOptionDiv").remove();
		});
		
		jQuery('#deleteComponent').live('click',function(){
			if(document.getElementById("LightBox2").style.display=="none") {
		        document.getElementById("LightBox2").style.display="block";
		        document.getElementById("grayBG").style.display="block";
		    } else {
		        document.getElementById("LightBox2").style.display="none";
		        document.getElementById("grayBG").style.display="none";
		    }
			jQuery.ajax({
				url :"./getComponents.do",
				type :"POST",
				async : false,
				dataType : "html",
				//data :"",
				success : function(data) {
					jQuery("#compBox").empty();
					jQuery("#compBox").html(data);
				},
				complete: function(xData, status){
				},
				error : function(xmlhttp, error_msg) {
				}
				});
			
		});
		
		jQuery("#closeShowComp").click(function(){
			document.getElementById("LightBox2").style.display="none";
	        document.getElementById("grayBG").style.display="none";
		});
});

	function editHeadingLabel(id){
		jQuery("#edited").val("true");
		var curDiv = jQuery("#currentDiv").attr("value");
		var compNum = curDiv.split("_");
		var reflectHeading = "headingSpan"+compNum[1]; 
		jQuery("#"+reflectHeading).text(jQuery("#"+id).val());		
	}

	function editLabel(id){
			jQuery("#edited").val("true");
			var curDiv = jQuery("#currentDiv").attr("value");
			var compNum = curDiv.split("_");
			var reflectLabel = "labelSpan"+compNum[1]; 
			jQuery("#"+reflectLabel).text(jQuery("#"+id).val());
		}
	function changeOPtion(id){
		var selectOpt = jQuery("#"+id).attr("value");
		if(selectOpt.toLowerCase() != "create option"){
		jQuery("#edited").val("true");
		jQuery.ajax({
			url :"./getOptions.do",
			type :"POST",
			async : false,
			dataType : "html",
			data :"optId="+selectOpt,
			success : function(data) {
				jQuery("#optionList").empty();
				jQuery("#editDesc").empty();
				jQuery("#editDesc").html(data);
			},
			complete: function(xData, status){
				if(jQuery("#editOptionType").length == 0){
					jQuery("#tickType").append('<select id="editOptionType" name="editOptionType" style="display: block;"><option id="1" value="Square">Square</option><option id="2" value="Circle">Circle</option><option id="3" value="None">None</option></select>');
				}
				jQuery("#editOptionType").val((jQuery("#selectedType").val()));
				//jQuery("#editOptionType option:selected").text((jQuery("#selectedType").val()));
				jQuery("#createCompOptionDiv").empty();
				jQuery("#createCompOptionDetails").css("display", "none");
				var curDiv = jQuery("#currentDiv").attr("value");
				var compNum = curDiv.split("_");
				setOptionDrpListRight(compNum);
			},
			error : function(xmlhttp, error_msg) {
			}
			});
		}else{
			jQuery.ajax({
				url :"./adminCreateOption.do",
				type :"GET",
				dataType : "html",
				success : function(data) {
					jQuery("#editOptionType").remove();
					jQuery("#createCompOptionDiv").empty();
					jQuery("#createCompOptionDiv").html(data);
					jQuery("#optionList").empty();
				},
				complete: function(xData, status){
					jQuery("#createCompOptionDetails").css("display", "block");
				},
				error : function(xmlhttp, error_msg) {
				}	
			});
		}
		}
	
	jQuery(".optiontext").live("keyup",function(e){
		var id = jQuery(this).attr("id");
		jQuery("#edited").val("true");
		var maxLength = 40;
		var optionList = jQuery("#optionList").find(".optiontext");
		var optionString = "";
		var curDiv = jQuery("#currentDiv").attr("value");
		var compNum = curDiv.split("_");
		if(parseInt(compNum[1]) == 15){
			optionString = jQuery("#"+id).val();
		}
		else{
			jQuery(optionList).each(function(index) {
	    	    var curOptTextId = jQuery(this).attr("id");
	    	    var curOptText = jQuery("#"+curOptTextId).val();
	    	    optionString = optionString+"^"+curOptText;
	        });
		}

		// if it exceeds than the max range.
		var optionStringLen = optionString.length + ((optionString.split("^")).length * 2);
		if(optionStringLen > parseInt(maxLength)){
        	if(parseInt(compNum[1]) == 15){
            	var curOptTextLen = (jQuery("#"+id).val()).length;
        		jQuery("#"+id).attr("maxlength", parseInt(curOptTextLen));
    		}
        	else{
        	jQuery(optionList).each(function(index) {
        	    var curOptTextId = jQuery(this).attr("id");
        	    var curOptText = jQuery("#"+curOptTextId).val();
        	    var curOptTextLen = curOptText.length;
        	    jQuery("#"+curOptTextId).attr("maxlength", parseInt(curOptTextLen));
            	});
        	}
        	if((e.keyCode != 8) && (e.keyCode != 13) && (e.keyCode != 46)){
        		var currText = jQuery("#"+id).val();
        		var len = currText.length;
        		var removeChar = optionStringLen - parseInt(maxLength);
        		var optNum = id.split("_");
        		var reflectOpt = "opt"+compNum[1]+""+optNum[1];
        		jQuery("#"+id).val(currText.substring(0, len-removeChar));
        		jQuery("#"+reflectOpt).text(jQuery("#"+id).val());
        		//alert("The maximum length for options is "+((parseInt(maxLength))-((optionString.split("^")).length * 2))+".");
        		alert("You have reached the maximum character limit.");
        	}
            }
        else{
        jQuery(optionList).each(function(index) {
    	    var curOptTextId = jQuery(this).attr("id");
    	    jQuery("#"+curOptTextId).removeAttr("maxlength");
        });
		var curOptId = id;
		var optNum = curOptId.split("_");
		var reflectOpt = "opt"+compNum[1]+""+optNum[1];
		jQuery("#"+reflectOpt).text(jQuery("#"+curOptId).val());
        }
	});
	
    function inActiveComponent(preDiv){
    	var prevImg1 = jQuery("#"+preDiv).children().attr("id");
    	var prevImg2 = jQuery("#"+prevImg1).next().attr("id");
    	jQuery("#"+preDiv).css("background-color", "#FFFFFF");
        }
    function activeComponent(componentDiv){
    	var curImg1 = jQuery("#"+componentDiv).children().attr("id");
    	var curImg2 = jQuery("#"+curImg1).next().attr("id");
    	jQuery("<br/>");
    	jQuery("#"+componentDiv).css("background-color", "#EEEEEE");
        }
    function setComponents(componentDiv){
    	jQuery("#currentDiv").val(componentDiv);
    	jQuery("#prevDiv").val(componentDiv );
        }
	function showOnTemplate(componentNum){
		var userFidDiv = "templateComponent"+componentNum[1];
		jQuery("#showPosition").text(componentNum[1]);
		}
    function setHeadingDrp(componentNum){
    	var userFidDiv = "viewComponent"+componentNum[1];
    	var userFid = jQuery("#"+userFidDiv).attr("value");
    	var templateId = jQuery("#templateId").val();
    	jQuery("#CompDrp").val(userFid);
    	jQuery("#changedHeading").val(jQuery("#CompDrp option:selected").text());
        }
    function setLabelDisplay(componentNum){
    	var reflectLabel = "labelSpan"+componentNum[1];
    	if(jQuery("#"+reflectLabel).length != 0){
    		var reflectLabelVal = jQuery("#"+reflectLabel).text();
        	jQuery("#editLabel").val(reflectLabelVal);
    		jQuery("#editLabel").removeAttr('readonly','readonly');
    		jQuery("#editLabel").css("background-color","#FFFFFF");
    	}
    	else{
    		jQuery("#editLabel").val("");
    		jQuery("#editLabel").attr('readonly','readonly');
    		jQuery("#editLabel").css("background-color","#ACACAC");
    	}
        }
	 
    function setOptionDescription(componentNum){
    	var optionValueDiv = "optionValue"+componentNum[1];
        var optionValue = jQuery("#"+optionValueDiv).val();
        var optionString = ""; 
        var optDiv = "optDiv"+componentNum[1];
        var allOption = jQuery("#"+optDiv).find('.optionText');
        var optTypeDiv = "optType"+componentNum[1];
        if(jQuery("#"+optTypeDiv).length != 0){
        var imageOptionType = jQuery("#"+optTypeDiv).val();
        var image = "";
    	if(imageOptionType.toLowerCase() == "square"){
    		image = "../images/square.png";
    	}
    	else if(imageOptionType.toLowerCase() == "circle"){
    		image = "../images/circle.png";
    	}
    	else{
    		image = "../images/none.png";
    	}
    	
        jQuery("#optionList").empty();
        if(allOption.length < 1){
        	var curDiv = jQuery("#currentDiv").attr("value");
			var compNum = curDiv.split("_");
        	var imageType = (jQuery("#editOptionType option:selected").text()).toLowerCase();
        	jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_1"><span class="checkbox" style="display:block;"><img src="../images/'+imageType+'.png" class="imageOptionType" id="image1">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_1" value="" style="width: 100px;" onkeyup="editOtions(this.id)"/><input type="button" name="remove_1" value="" id="remove_1" class="deleteEditOption"/><input type="button" name="add_1" value="" id="add_1" class="addEditOption"/></span></div>');
			setOptionDrpListRight(compNum);
        }
        jQuery(allOption).each(function(index) {
    	    var curOptText = jQuery(this).text();
    	    curOptText = curOptText.replace(/"/g, "&#34");
    	    if(optionString ==""){
    	       optionString = curOptText;
    	       }
    	    else{
    	    	optionString = optionString.replace(/"/g, "&#34");
    	       }
    	    	if(index != (allOption.length-1)){
    	    		jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_'+(index+1)+'"><span class="checkbox" style="display:block;"><img src="'+image+'" class="imageOptionType" id="editImage_'+(index+1)+'">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="'+curOptText+'" style="width: 100px;" /><input type="button" name="remove_'+(index+1)+'" value="" id="remove_'+(index+1)+'" class="deleteEditOption"/><input type="button" name="add_'+(index+1)+'" value="" id="add_'+(index+1)+'" class="addEditOption" style="display: none;" /></span></div>');
    	    	}
    	    	else{
    	    		jQuery("#optionList").append('<div class="checkbox" id="checkboxcnt_'+(index+1)+'"><span class="checkbox" style="display:block;"><img src="'+image+'" class="imageOptionType" id="editImage_'+(index+1)+'">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="'+curOptText+'" style="width: 100px;" /><input type="button" name="remove_'+(index+1)+'" value="" id="remove_'+(index+1)+'" class="deleteEditOption"/><input type="button" name="add_'+(index+1)+'" value="" id="add_'+(index+1)+'" class="addEditOption" /></span></div>');
    	    		var lastOptDiv = "add_"+(index+1);
    	        	jQuery("#lastEditOptionDiv").val(lastOptDiv);
    	    	}
        });
        }
       }
    
	function getOptionDrp(componentNum){
		var viewCompId = "viewComponent"+componentNum[1];
		var subOptId = "scoptionId"+componentNum[1];
		var compId = jQuery("#"+viewCompId).val();
		var selectId = jQuery("#"+subOptId).val();
		jQuery.ajax({
		async: false,
		url :"./getOptionsDropdown.do",
		type :"POST",
		dataType : "html",
		data :"cid="+compId+"&slectOPtId="+selectId,
		success : function(data) {
			jQuery("#editOpdtionDiv").empty();
			jQuery("#editOpdtionDiv").html(data);
		},
		complete:function(xData, status){
			jQuery("#createCompOptionDiv").empty();
			setOptionDescription(componentNum);
			jQuery("#editComponent").css("display", "block");
		},
		error : function(xmlhttp, error_msg) {
		}
		});
		}
	
	function setSelectedLabel(componentNum){
		var curLabel = jQuery("#editLabel").val();
		var reflectLabel = "labelSpan"+componentNum[1];
		if((componentNum[1] == 1) || (componentNum[1] == 3) || (componentNum[1] == 6) || (componentNum[1] == 13)){
			jQuery("#editLabel").attr("readonly","readonly");
			jQuery("#editLabel").css("background-color", "#8A929A");
		}
		if((curLabel != "") || (curLabel != " ")){
    		jQuery("#"+reflectLabel).text(jQuery("#editLabel").val());
		}else{
			jQuery("#"+reflectLabel).append('<div style="height:10px;"></div>');
		}
		}

	function setOptionDrpListRight(compNum){
		var optList = jQuery("#optionList").find(".optiontext");
		var curOptDiv = "optDiv"+compNum[1];
		var curOptParentDiv = "optDivParent"+compNum[1];
		var optVal = "optionValue"+compNum[1];
        var imageOptionType = jQuery("#editOptionType option:selected").text();
        var lastAdd = "add_"+(jQuery(optList).length);
		jQuery("#lastEditOptionDiv").val(lastAdd);
        var image = "";
        var totalText = "";
        var subCurrText = "";
    	if(imageOptionType.toLowerCase() == "square"){
    		image = "../images/square.png";
    	}
    	else if(imageOptionType.toLowerCase() == "circle"){
    		image = "../images/circle.png";
    	}
    	else{
    		image = "../images/none.png";
    	}
    	if(jQuery("#"+curOptDiv).length != 0){
    		jQuery("#"+curOptDiv).empty();
        	}
    	else if((jQuery("#"+curOptDiv).length == 0) || (jQuery("#"+optVal).val().length == 0)){
			var optType = jQuery("#editOptionType option:selected").text(); 
			var optionId = jQuery("#editOption").val();
			jQuery("#"+curOptParentDiv).empty();
			jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionValue'+compNum[1]+'" id="optionValue'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionString'+compNum[1]+'" id="optionString'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionStringUpdate'+compNum[1]+'" id="optionStringUpdate'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="scoptionId'+compNum[1]+'" id="scoptionId'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type="hidden" name="optionId'+compNum[1]+'" id="optionId'+compNum[1]+'" value="'+totalText+'" />');
    		jQuery("#"+curOptParentDiv).append('<input type= "hidden" id="optType'+compNum[1]+'" name="optType'+compNum[1]+'" value="'+optionId+'" />');
    		jQuery("#"+curOptParentDiv).append('<div class="checkboxcnt" id=optDiv'+compNum[1]+'></div>');
    	}
    	
		jQuery(optList).each(function(index) {
	  	    var curOptId = jQuery(this).attr("id");
	  	    var curOptText = jQuery("#"+curOptId).val();
	  	    if(totalText.length < 50){
	  	    	reqlength = (23 - (parseInt(totalText.length)));
	  	    	totalText = totalText+"^"+curOptText;
	  	    	if(totalText.length > 23){
	  	    		if(reqlength >  (parseInt(curOptText.length))){
	  	    			reqlength = parseInt(curOptText.length);
	  	    		}
		  	    	subCurrText =  (curOptText.substring(0,reqlength));
	  	    	}
	  	    	else{
	  	    		subCurrText = curOptText;
	  	    	}
	  	    if(compNum[1] != 15){
	  	    	jQuery("#"+curOptDiv).append('<div class="checkbox"><span><img src="'+image+'" class="imageOptionType" id="image_'+compNum[1]+""+(index+1)+'"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+subCurrText+'</label></span></div>');
	  	    }
	  	    else{
	  	    	jQuery("#"+curOptDiv).append('<div class="checkbox15"><span><img src="'+image+'" class="imageOptionType" id="image_'+compNum[1]+""+(index+1)+'"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+curOptText+'</label></span></div>');
		  	    }    
	  	    }
			});
		
		}
	function displayHideBox(boxNumber)
	{
	    if(document.getElementById("LightBox"+boxNumber).style.display=="none") {
	        document.getElementById("LightBox"+boxNumber).style.display="block";
	        document.getElementById("grayBG").style.display="block";
	    } else {
	        document.getElementById("LightBox"+boxNumber).style.display="none";
	        document.getElementById("grayBG").style.display="none";
	    }
	}
	
	function repositoryImageSelectionPop(divId) {
		if(divId == "center"){
	        jQuery("#repositoryPopUp").show();
	        jQuery(".box_content1").css("top","25%");
	        jQuery("#grayBG").show();
	    } 
		else if(divId == "center104"){
	        jQuery("#repositoryPopUp104").show();
	        jQuery(".box_content1").css("top","25%");
	        jQuery("#grayBG").show();
	    } 
            else if(divId == "center105"){
	        jQuery("#repositoryPopUp105").show();
	        jQuery(".box_content1").css("top","25%");
	        jQuery("#grayBG").show();
	    } 
		else if(divId == "left"){
			jQuery("#leftRepositoryPopUp").show();
			jQuery(".box_content1").css("top","25%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomLeft"){
			jQuery("#bottomLeftRepositoryPopUp").show();
			jQuery(".box_content1").css("top","45%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomRight"){
			jQuery("#bottomRightRepositoryPopUp").show();
			jQuery(".box_content1").css("top","35%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomLast"){
			jQuery("#bottomLastRepositoryPopUp").show();
			jQuery(".box_content1").css("top","50%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomMid"){
			jQuery("#bottomMidRepositoryPopUp").show();
			jQuery(".box_content1").css("top","33%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "bottomCenter"){
			jQuery("#bottomCenterRepositoryPopUp").show();
			jQuery(".box_content1").css("top","45%");
			jQuery("#grayBG").show();
	    } 
		else if(divId == "close"){
			clearFileUploader();
			jQuery("#selectImg").val("");
			jQuery("#bottomCenterRepositoryPopUp").hide();
			jQuery("#bottomLastRepositoryPopUp").hide();
	        jQuery("#bottomRightRepositoryPopUp").hide();
			jQuery("#bottomLeftRepositoryPopUp").hide();
			jQuery("#leftRepositoryPopUp").hide();
			jQuery("#repositoryPopUp104").hide();
                        jQuery("#repositoryPopUp105").hide();
			jQuery("#repositoryPopUp").hide();
			jQuery("#grayBG").hide();
	        centerImageSelectionPop('close');
	    }
            else if(divId == "close105"){
                        if(jQuery("#imgLogo").find("img").length > 0){
                            jQuery("#msgText").css("display", "none"); 
                            jQuery("#logoText").css("display", "none");
                            jQuery("#imgLogo").css("display", "block");
                        //}else if(jQuery("#logoText").trim != ""){
                        }else if((jQuery("#logoText").length != 0) && (jQuery("#logoText").val().length != 0)){
                            jQuery("#imgLogo").css("display", "none");
                            jQuery("#msgText").css("display", "none"); 
                            jQuery("#logoText").css("display", "block");
                        }else{
                            jQuery("#logoText").css("display", "none");
                            jQuery("#imgLogo").css("display", "none");
                            jQuery("#msgText").css("display", "block"); 
                        }
			clearFileUploader();
			jQuery("#selectImg").val("");
			jQuery("#bottomCenterRepositoryPopUp").hide();
			jQuery("#bottomLastRepositoryPopUp").hide();
                        jQuery("#bottomRightRepositoryPopUp").hide();
			jQuery("#bottomLeftRepositoryPopUp").hide();
			jQuery("#leftRepositoryPopUp").hide();
			jQuery("#repositoryPopUp104").hide();
                        jQuery("#repositoryPopUp105").hide();
			jQuery("#repositoryPopUp").hide();
			jQuery("#grayBG").hide();
                        centerImageSelectionPop('close105');
	    }
	}
	
	function centerImageSelectionPop(divId,imageWidth,imageHeight ) {
            	if(divId == "center"){
			jQuery("#centerImagePop").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			jQuery("#centerimageHeight").val(imageHeight);
			jQuery("#centerimageWidth").val(imageWidth);
		}
		else if(divId == "center104"){
			jQuery("#centerImagePop104").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			jQuery("#centerimageHeight104").val(imageHeight);
			jQuery("#centerimageWidth104").val(imageWidth);
		}
                else if(divId == "center105"){
                    	jQuery("#centerImagePop105").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			jQuery("#centerimageHeight105").val(imageHeight);
			jQuery("#centerimageWidth105").val(imageWidth);
		}
		else if(divId == "left"){
			jQuery("#leftImagePop").show();
			jQuery(".box_content").css("top","25%");
			jQuery("#grayBG").show();
			jQuery("#leftimageHeight").val(imageHeight);
			jQuery("#leftimageWidth").val(imageWidth);
		}
		else if(divId == "bottemLeft"){
			jQuery("#bottomLeftImagePop").show();
			jQuery(".box_content").css("top","45%");
			jQuery("#grayBG").show();
			jQuery("#bottomLeftimageHeight").val(imageHeight);
			jQuery("#bottomLeftimageWidth").val(imageWidth);
		}
		else if(divId == "bottemLeft103"){
			jQuery("#bottomLeftImagePop").show();
			jQuery(".box_content").css("top","65%");
			jQuery("#grayBG").show();
			$("#bottomLeftimageHeight").val(imageHeight);
			$("#bottomLeftimageWidth").val(imageWidth);
		}
		else if(divId == "bottemRight"){
			jQuery("#bottomRightImagePop").show();
			jQuery(".box_content").css("top","35%");
			jQuery("#grayBG").show();
			jQuery("#bottomRightimageHeight").val(imageHeight);
			jQuery("#bottomRightimageWidth").val(imageWidth);
		}
		else if(divId == "bottomLast"){
			jQuery("#bottomLastImagePop").show();
			jQuery(".box_content").css("top","50%");
			jQuery("#grayBG").show();
			jQuery("#bottomLastimageHeight").val(imageHeight);
			jQuery("#bottomLastimageWidth").val(imageWidth);
		}
		else if(divId == "bottomMid"){
			jQuery("#bottomMidImagePop").show();
			jQuery(".box_content").css("top","33%");
			jQuery("#grayBG").show();
			jQuery("#bottomMidimageHeight").val(imageHeight);
			jQuery("#bottomMidimageWidth").val(imageWidth);
		}
		else if(divId == "bottomCenter"){
			jQuery("#bottomCenterImagePop").show();
			jQuery(".box_content").css("top","45%");
			jQuery("#grayBG").show();
			jQuery("#bottomCenterimageHeight").val(imageHeight);
			jQuery("#bottomCenterimageWidth").val(imageWidth);
		}
		else if(divId == "close"){
			clearFileUploader();
			jQuery("#selectImg").val("");
			jQuery("#bottomCenterImagePop").hide();
			jQuery("#bottomMidImagePop").hide();
			jQuery("#bottomLastImagePop").hide();
			jQuery("#bottomRightImagePop").hide();
			jQuery("#bottomLeftImagePop").hide();
			jQuery("#centerImagePop").hide();
			jQuery("#leftImagePop").hide();
			jQuery("#centerImagePop104").hide();
                        jQuery("#centerImagePop105").hide();
			jQuery("#grayBG").hide();
		}
                else if (divId == "close105") {
                    if(jQuery("#imgLogo").find("img").length > 0){
                        jQuery("#msgText").css("display", "none"); 
                        jQuery("#logoText").css("display", "none");
                        jQuery("#imgLogo").css("display", "block");
                    //}else if(jQuery("#logoText").trim != ""){
                    }else if((jQuery("#logoText").length != 0) && (jQuery("#logoText").val().length != 0)){
                        jQuery("#imgLogo").css("display", "none");
                        jQuery("#msgText").css("display", "none"); 
                        jQuery("#logoText").css("display", "block");
                    }else{
                        jQuery("#logoText").css("display", "none");
                        jQuery("#imgLogo").css("display", "none");
                        jQuery("#msgText").css("display", "block"); 
                    }
                    clearFileUploader();
                    jQuery("#selectImg").val("");
                    jQuery("#bottomCenterImagePop").hide();
                    jQuery("#bottomMidImagePop").hide();
                    jQuery("#bottomLastImagePop").hide();
                    jQuery("#bottomRightImagePop").hide();
                    jQuery("#bottomLeftImagePop").hide();
                    jQuery("#centerImagePop").hide();
                    jQuery("#leftImagePop").hide();
                    jQuery("#centerImagePop104").hide();
                    jQuery("#centerImagePop105").hide();
                    jQuery("#grayBG").hide();
                }
                $("#reqHeight").val(imageHeight);
		$("#reqWidth").val(imageWidth);
	}
	
	function changeImage(imgType) {
		if(imgType == 'circle'){
			jQuery("span").find("b[class='green']").addClass("greenCircle");
			jQuery("span").find("b[class='green greenCircle']").removeClass("green");
			jQuery("span").find("b[class='red']").addClass("redCircle");
			jQuery("span").find("b[class='red redCircle']").removeClass("red");
			jQuery("span").find("b[class='yellow']").addClass("yellowCircle");
			jQuery("span").find("b[class='yellow yellowCircle']").removeClass("yellow");

			 jQuery("span").find("b[class='smallGreen']").addClass("smallGreenCircle");
			jQuery("span").find("b[class='smallGreen smallGreenCircle']").removeClass("smallGreen");
			jQuery("span").find("b[class='smallRed']").addClass("smallRedCircle");
			jQuery("span").find("b[class='smallRed smallRedCircle']").removeClass("smallRed");
			jQuery("span").find("b[class='smallYellow']").addClass("smallYellowCircle");
			jQuery("span").find("b[class='smallYellow smallYellowCircle']").removeClass("smallYellow"); 
			
		} else {
			jQuery("span").find("b[class='greenCircle']").addClass("green");
			jQuery("span").find("b[class='greenCircle green']").removeClass("greenCircle");
			jQuery("span").find("b[class='redCircle']").addClass("red");
			jQuery("span").find("b[class='redCircle red']").removeClass("redCircle");
			jQuery("span").find("b[class='yellowCircle']").addClass("yellow");
			jQuery("span").find("b[class='yellowCircle yellow']").removeClass("yellowCircle");

			 jQuery("span").find("b[class='smallGreenCircle']").addClass("smallGreen");
			jQuery("span").find("b[class='smallGreenCircle smallGreen']").removeClass("smallGreenCircle");
			jQuery("span").find("b[class='smallRedCircle']").addClass("smallRed");
			jQuery("span").find("b[class='smallRedCircle smallRed']").removeClass("smallRedCircle");
			jQuery("span").find("b[class='smallYellowCircle']").addClass("smallYellow");
			jQuery("span").find("b[class='smallYellowCircle smallYellow']").removeClass("smallYellowCircle"); 
			}
	}
	
	function setEditdHeading(compNum){
		var headingSpan = "headingSpan"+compNum[1];
		var compName = "componentName"+compNum[1];
		jQuery("#"+compName).val(jQuery("#"+headingSpan).text());
		}
	function setEditedLabel(compNum){
		var labelDiv = "label"+compNum[1];
		var labeSpanDiv ="labelSpan"+compNum[1];
		jQuery("#"+labelDiv).val(jQuery("#editLabel").val());
		}
	function setEditedDesription(compNum){
		var optionDescDiv = "optionValue"+compNum[1];
		var optionStringDiv = "optionString"+compNum[1];
		var optionListDiv = "optDiv"+compNum[1];
		var optionList = jQuery("#"+optionListDiv).find('.optionText');
		var optionString="";
        jQuery(optionList).each(function(index) {
    	    var curOptText = jQuery(this).text();
    	    if(optionString ==""){
    	       optionString = curOptText;
    	       }
    	    else{
    		   optionString = optionString+"^"+curOptText;
    	       }
        });
        jQuery("#"+optionDescDiv).val(optionString); 
        jQuery("#"+optionStringDiv).val(optionString); 
		}
	function setViewComponentId(compNum){
		var viewCompId = "viewComponent"+compNum[1];
		var subOptId = "scoptionId"+compNum[1];
		jQuery("#"+viewCompId).val(jQuery("#CompDrp").val());
		jQuery("#"+subOptId).val(jQuery("#editOption").val());
		}
	function setPrevHeading(compNum){
		var compnentNamediv = "componentName"+compNum[1];
		var headingSpan = "headingSpan"+compNum[1];
		jQuery("#"+headingSpan).text(jQuery("#"+compnentNamediv).val());
		//jQuery("#CompDrp :selected").text(jQuery("#"+compnentNamediv).val());
		}
	function setPrevLabel(compNum){
		var labelDiv = "label"+compNum[1];
		var labelSpanDiv = "labelSpan"+compNum[1];
		jQuery("#"+labelSpanDiv).text(jQuery("#"+labelDiv).val());
		jQuery("#editLabel").val(jQuery("#"+labelDiv).val());
		}
	function setPrevDescription(compNum){
		var curOptTypeDiv = "optType"+compNum[1];
		var curOptType = "";
		if(jQuery("#"+curOptTypeDiv).length == 0){
			curOptType = "square";
		}else{
			curOptType = jQuery("#"+curOptTypeDiv).val();
		}
	    	var image = "";
	    	if(curOptType.toLowerCase() == "square"){
	    		image = "../images/square.png";
	    	}
	    	else if(curOptType.toLowerCase() == "circle"){
	    		image = "../images/circle.png";
	    	}
	    	else{
	    		image = "../images/none.png";
	    	}
	    	var optionValueDiv = "optionString"+compNum[1];
	    	if(jQuery("#"+optionValueDiv).length != 0){
			var prevDesc = jQuery("#"+optionValueDiv).val();
			var prevDescList = prevDesc.split("^");
			var curOptDiv = "optDiv"+compNum[1];
			jQuery("#"+curOptDiv).empty();
			jQuery("#optionList").empty();
			for(var index=0; index<prevDescList.length; index++){
				if((prevDescList[index]).length != 0){
				if(compNum[1] != 15){
					jQuery("#"+curOptDiv).append('<div class="checkbox" ><span><img src="'+image+'"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+prevDescList[index]+'</label></span></div>');
		  	    }
		  	    else{
		  	    	jQuery("#"+curOptDiv).append('<div class="checkbox15"><span><img src="'+image+'" class="rectangle"><label id="opt'+compNum[1]+""+(index+1)+'" class="optionText">'+prevDescList[index]+'</label></span></div>');
			  	    } 
				jQuery("#optionList").append('<div class="checkbox"><span class="checkbox" style="display:block;"><img src="'+image+'" class="rectangle">&nbsp;&nbsp;<input type="text" size="9" class="optiontext" id="optiontext_'+(index+1)+'" value="'+prevDescList[index]+'" style="width: 100px; /></span></div>');
			}
			}
	    	}
	}
	function setEditOptionType(componentNum){
		var optType = "optType"+componentNum[1];
		if(jQuery("#"+optType).length != 0){
			jQuery("#editOptionType").val(jQuery("#"+optType).val());
			jQuery("#editOptionType").css("display", "block");
		}
	}
	function setOptionType(compNum){
		var optType = "optType"+compNum[1];
		jQuery("#"+optType).val(jQuery("#editOptionType").val());
	}
	function getOptionDesc(optionValueDiv){
		var chckboxCntDiv = jQuery("#optionList").find(".optiontext");
		var optionString =  "";
		jQuery(chckboxCntDiv).each(function(index){
				var optDescId = jQuery(this).attr("id");
				var optText = jQuery("#"+optDescId).val();
				if(optionString ==""){
		    	       optionString = optText;
		    	       }
		    	    else{
		    		   optionString = optionString+"^"+optText;
	    	       }
			});
		jQuery("#"+optionValueDiv).val(optionString); 
		}
	function getLastDiv(lastDiv){
		var lastDivnum = parseInt(lastDiv);
		var flag = false; 
		var currDiv = "";
		for(var i=(lastDiv-1); i>0; i--){
			currDiv = "add_"+i;
				if(jQuery("#"+currDiv).length !=0){
						flag = true;
						lastDivnum = i;
						break;
					}
			}
		if(flag == true){
				currDiv = "add_"+lastDivnum;
			}
		else{
			    currDiv = "add_"+lastDiv; 	
			}
		return currDiv;
	}
	function setDelEditImage(){
		var displayDivArr = jQuery("#VCcontinner").find(".displayDiv");
		jQuery(displayDivArr).each(function(index){
			var id = jQuery(this).attr("id");
			var visibility = jQuery("#"+id).val();
			if(visibility == "hidden"){
				var delButt = "delComp_"+(index+1);
				jQuery("#"+delButt).css("visibility", "hidden");
			}
		});
		}
	
	function isUniqueComponent(componentDiv , visibiltyValue){
		var compNum = componentDiv.split("_");
		var viewComp = "viewComponent"+compNum[1];
		var viewCompId = jQuery("#"+viewComp).val();
		var visibility = "visibility"+compNum[1];
		var flag = true;
		var componentName = "componentName"+compNum[1];
		if((jQuery("#"+visibility).val() == visibiltyValue) ){
			for(var i=1; i <= 15; i++){
				if(i != parseInt(compNum[1])){
				var viewCompCurr = "viewComponent"+i;
				var viewCompIdCurr = jQuery("#"+viewCompCurr).val();
				if(viewCompIdCurr == viewCompId ){
					flag = false;
				}
				}
		}
		}
		if((flag == false) && (visibiltyValue == "hidden") || (visibiltyValue == "inherit")){
			alert(jQuery("#"+componentName).val() + " is already in use. Please select another form the heading list.");
		}
		return flag;
	}
	
	/*My Changes starts from here*/
	if ($.browser.msie) {
		  $("#leftOptOwnImage").click(function() {
			  setTimeout( function() {
				  this.focus();
				}, 500 );
		  });
		}
	jQuery('#leftOptOwnImage').live('change', function(){ 
		jQuery("#leftOptImage").html('');
		jQuery("#leftOptImage").html('<img src="../images/loader.gif" alt="Uploading...."/>');		
		jQuery("#saveRghtImgBtn").html('');
		jQuery("#uploadImage1").ajaxForm({
			target: '#leftOptImage'
           }).submit();
	});
	if ($.browser.msie) {
		  $("#rghtOptOwnImage").click(function() {
			  setTimeout( function() {
				  this.focus();
				}, 500 );
		  });
		}
	jQuery('#rghtOptOwnImage').live('change', function(){ 
		jQuery("#rghtOptImage").html('');
		jQuery("#rghtOptImage").html('<img src="../images/loader.gif" alt="Uploading...."/>');		
		jQuery("#saveLftImgBtn").html('');
		jQuery("#uploadImage2").ajaxForm({
			target: '#rghtOptImage'
           }).submit();
	});

	jQuery("#saveOptionalLftImage").live('click',function() { 			
			var saveOptionalLftLogo = jQuery("#saveOptionalLftLogo").val();
			jQuery.ajax({        
				url : "./saveLogoImage.do?saveLogo=" + saveOptionalLftLogo,
				type : "POST",
				dataType : "html",
				data : "vardd=1",
				success : function(data) {
				}
			});
		});	
	
	jQuery("#saveOptionalRghtImage").live('click',function() { 			
		var saveOptionalLftLogo = jQuery("#saveOptionalLftLogo").val();
		jQuery.ajax({        
			url : "./saveLogoImage.do?saveLogo=" + saveOptionalLftLogo,
			type : "POST",
			dataType : "html",
			data : "vardd=1",
			success : function(data) {
			}
		});
	});	
	if ($.browser.msie) {
		  $("#centerOwnImage").click(function() {
			  setTimeout( function() {
				  this.focus();
				}, 500 );
		  });
		}
	jQuery('#centerOwnImage').live('change', function(){ 
		jQuery("#img").html('');
		jQuery("#img").html('<img src="../images/loader.gif" alt="Uploading...."/>');				
		jQuery("#uploadCenterImage").ajaxForm({
			target: '#img'
           }).submit();
	});

	jQuery('#browseOptImg').live('click',function(){			
			jQuery("#uploadOptImage").ajaxForm({
				target: '#existingImg'
	           }).submit();
		});
	
	jQuery('#browseCenterOptImg').live('click',function(){			
		jQuery("#browseCenterRImage").ajaxForm({
			target: '#existingImg'
           }).submit();
	});


	function imageSelectionPop(boxNumber,divId)
	{		
	    if(document.getElementById("imgLightBox"+boxNumber).style.display=="none") 
	    {
	        document.getElementById("imgLightBox"+boxNumber).style.display="block";
	        document.getElementById("grayBG").style.display="block";
	        if(divId == "rght"){
	    		document.getElementById("rghtOptForm").style.display="block";
	    		document.getElementById("leftOptForm").style.display="none";
	    	}
	        else{
	    		document.getElementById("leftOptForm").style.display="block";
	    		document.getElementById("rghtOptForm").style.display="none";
	    	}
	    } 
	    else {	    	
	        document.getElementById("imgLightBox"+boxNumber).style.display="none";
	        document.getElementById("grayBG").style.display="none";
	        jQuery("#existingImg").html('');
	        
	    }		
	}
	
	function imageCenterSelectionPop(divId) {
		if(divId=="close"){
			document.getElementById("centerImagePop").style.display="none";
			document.getElementById("grayBG").style.display="none";
		}else{
			document.getElementById("centerImagePop").style.display="block";
			document.getElementById("grayBG").style.display="block";
		}
	}
	
/*My Changes end here*/
function serComponentVisible(compNum){
	   var curr_compDiv = "compDiv_"+compNum[1];
	   var labelSpanDiv = "labelSpan"+compNum[1];
	   jQuery("#"+curr_compDiv).css("visibility", "visible");
	   jQuery("#"+labelSpanDiv).css("visibility", "visible");
}
function serComponentHidden(compNum){
	   var curr_compDiv = "compDiv_"+compNum[1];
	   var labelSpanDiv = "labelSpan"+compNum[1];
	   jQuery("#"+curr_compDiv).css("visibility", "hidden");
	   jQuery("#"+labelSpanDiv).css("visibility", "hidden");
}

//Close All Div()
function closeAllDiv(){
	jQuery("#lbox").empty();
	jQuery("#lbox104").empty();
        jQuery("#lbox105").empty();
	jQuery("#leftBox").empty();
	jQuery("#bottomLeftBox").empty();
	jQuery("#bottomRightBox").empty();
	jQuery("#bottomLastBox").empty();
	jQuery("#repositoryBox").empty();
	jQuery("#bottomMidBox").val("");
	jQuery("#bottomCenterBox").val("");
}

//Clear File uploader
function clearFileUploader(){
	jQuery("#localImage").val("");
	jQuery("#leftImage").val("");
	jQuery("#bottomLeftImage").val("");
	jQuery("#bottomRightImage").val("");
	jQuery("#bottomLastImage").val("");
	jQuery("#bottomMidImage").val("");
	jQuery("#bottomCenterImage").val("");
}
function saveEditText(){
	var data = jQuery("#editText").val();
	jQuery("#headerText").html(data);
	jQuery("#headingText").val(data);
	jQuery("#headerText").show();
	jQuery("#editText").hide();
	jQuery("#okHeading").hide();
	jQuery("#editHeading").show();
}

function editCmpt(cm,pos,selectNum){
	var edit = "edit"+cm+"_"+selectNum[1];
	var ok = "ok"+cm+"_"+selectNum[1];
	var delete1 = "delete"+cm+"_"+selectNum[1];
	jQuery("#"+pos+"_"+selectNum[1]).hide();
	jQuery("#Component_"+selectNum[1]).show();
	jQuery("#Component_"+selectNum[1]).focus();
	jQuery("#"+ok).show();
	jQuery("#"+delete1).hide();
	jQuery("#"+edit).hide();
}

function editCompHeading(cm,pos,selectNum){
	var edit = "edit"+cm+"_"+selectNum[1];
	var ok = "ok"+cm+"_"+selectNum[1];
	var delete1 = "delete"+cm+"_"+selectNum[1];
	jQuery("#"+pos+"_"+selectNum[1]).hide();
	jQuery("#Component_"+selectNum[1]).show();
	jQuery("#Component_"+selectNum[1]).focus();
	jQuery("#"+ok).show();
	jQuery("#"+edit).hide();
}

function okCmpt(cm,pos,selectNum){
	var edit = "edit"+cm+"_"+selectNum[1];
	var ok = "ok"+cm+"_"+selectNum[1];
	var delete1 = "delete"+cm+"_"+selectNum[1];
	var delClass = "delete"+cm;
	var data = jQuery("#Component_"+selectNum[1]).val();
	jQuery("#"+pos+"_"+selectNum[1]).html(data);
	jQuery("#"+pos+"_"+selectNum[1]).show();
	if(pos == "topText"){
		jQuery("#"+pos+"_"+selectNum[1]).css("font-weight","bold");
		jQuery("#"+pos+"_"+selectNum[1]).css("font-family","'MyriadProBold', Arial");
		jQuery("#"+pos+"_"+selectNum[1]).css("font-size","11pt");
	}
	jQuery("#Component_"+selectNum[1]).hide();			
	jQuery("#"+ok).hide();
	jQuery("#"+edit).show();
	if(data=="")
	{
		jQuery("#"+delete1).hide();
	}
	else
	{
		/*jQuery("#"+pos+"_"+selectNum[1]).append(jQuery('<a href="javascript:void(0);" id="'+delete1+'" class="'+delClass+'"><img src="../images/iDelete.png" width="16px" height="17px"></a>').click(function()
		{
			var r = confirm("Are you sure you want to delete?");
			if (r==true)
			{
				var selectId = jQuery(this).attr("id");
				var selectNum = selectId.split("_");
				deleteComp(cm,pos,selectNum);
			}
			else{}
		}));*/
	}
}

function okTopCmpt(cm,pos,selectNum){
	var edit = "edit"+cm+"_"+selectNum[1];
	var ok = "ok"+cm+"_"+selectNum[1];
	var delete1 = "delete"+cm+"_"+selectNum[1];
	var delClass = "delete"+cm;
	var data = jQuery("#Component_"+selectNum[1]).val();
	jQuery("#"+pos+"_"+selectNum[1]).html(data);
	jQuery("#"+pos+"_"+selectNum[1]).show();
	if(pos == "topText"){
		jQuery("#"+pos+"_"+selectNum[1]).css("font-weight","bold");
		jQuery("#"+pos+"_"+selectNum[1]).css("font-family","'MyriadProBold', Arial");
		jQuery("#"+pos+"_"+selectNum[1]).css("font-size","11pt");
	}
	jQuery("#Component_"+selectNum[1]).hide();			
	jQuery("#"+ok).hide();
	jQuery("#"+edit).show();
	if(data=="")
	{
		jQuery("#"+delete1).hide();
	}
}

function deleteComp(cm,pos,selectNum){
	var delete1 = "delete"+cm+"_"+selectNum[1];
	jQuery("#"+pos+"_"+selectNum[1]).hide();			
	jQuery("#Component_"+selectNum[1]).val("");
	jQuery("#"+delete1).hide();
}

//checkStringLength
function checkStringLength(id, event)
{
	var currNum = id.split("_");
	var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
	var maxWidth = 770;
	if(is_chrome == true){
		maxWidth = 720;
	}
	jQuery("#temp1").empty();
	jQuery("#temp1").text(jQuery("#"+id).val());
	var matches = jQuery("#"+id).val();
	var pattern=/_/g;
    var v=matches.match(pattern);
    var width = 0;
    jQuery("#temp1").hide();
    if(v!=null)
    {
    	width=jQuery("#temp1").width()-((matches.match(pattern).length/2)*.5);
    }
    else
    {
    	 width=jQuery("#temp1").width();
    } 
	if(width>=maxWidth && (event.keyCode != 8 && event.keyCode != 46))
	{
		jQuery("#"+id).attr('maxlength',jQuery("#"+id).val().length);
		alert("Cannot add more characters.");
		if(id != "editComp_"+currNum[1]+"_1"){
			  jQuery("#editComp_"+currNum[1]+"_1").focus();
    	}else{
    		//jQuery(".okDetailHeading").click(function(){
    			okDetailHeading103(".okDetailHeading");
    		//});
    	}
		return true;
	}
	else if(event.keyCode == 13 ){
    	if(id != "editComp_"+currNum[1]+"_1"){
    		 jQuery("#editComp_"+currNum[1]+"_1").focus();
    	}
    	else{
    		okDetailHeading103(".okDetailHeading");
    	}
    }
}
function comp1FocusOut(){
	
}
function comp2FocusOut(){
	
}
function okDetailHeading103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	jQuery("#editComp_"+selectNum[1]+"_"+1).css("display","none");
	jQuery("#editComp_"+selectNum[1]+"_"+2).css("display","none");
	jQuery("#okDetailHeading_"+selectNum[1]).hide();
	jQuery("#editDetailHeading_"+selectNum[1]).show();
	if(jQuery("#editComp_"+selectNum[1]+"_"+1).val().length==0)
	{
		jQuery("#editComp_"+selectNum[1]+"_"+1).val(" ");
	
	}
	if(jQuery("#editComp_"+selectNum[1]+"_"+2).val().length==0)
	{
		jQuery("#editComp_"+selectNum[1]+"_"+2).val(" ");
	}
	jQuery("#cust1").html(jQuery("#editComp_"+selectNum[1]+"_"+1).val());
	jQuery("#cust2").html(jQuery("#editComp_"+selectNum[1]+"_"+2).val());
	jQuery("#detailText_"+selectNum[1]).val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
	jQuery(".detailCmpt").val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
	jQuery("#cust1").show();
	jQuery("#cust2").show();
	if((jQuery.trim((jQuery("#editComp_"+selectNum[1]+"_"+1).val())).length == 0) && (jQuery.trim((jQuery("#editComp_"+selectNum[1]+"_"+1).val())).length == 0)){
		jQuery("#customer").css("border","1px solid white");
	}
	else{
		jQuery("#customer").css("border","1px solid black");
	}
}

function okTop103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	if(jQuery("#Component_"+selectNum[1]).val()==""){
		alert("This field can not be blank.");
		jQuery(".focusOutSaveTop").focus();
	}
	else {
		okTopCmpt("TopHeading","topText",selectNum);				
	}
}

function topLeft103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	if(jQuery("#Component_"+selectNum[1]).val()==""){
		alert(headingAlert);
		jQuery(this).focus();
	}
	else {
		okTopCmpt("TopLeftHeading","topLeft",selectNum);
	}
}	

function topLeftS103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	okCmpt("TopLeft","topLeft",selectNum);
}

function topRight103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	okCmpt("TopRight","topRight",selectNum);
}

function bottomLeft103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	okCmpt("BottomLeft","bottomLeft",selectNum);
}

function topRightHeading103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	if(jQuery("#Component_"+selectNum[1]).val()==""){
		alert(headingAlert);
	}
	else {
		okTopCmpt("TopRightHeading","topRight",selectNum);
	}
	jQuery(".focusOutSaveRightHeading").focus();
}

function bottomLeftHeading103(thisId){
	var selectId = jQuery(thisId).attr("id");
	var selectNum = selectId.split("_");
	if(jQuery("#Component_"+selectNum[1]).val()==""){
		alert(headingAlert);
	}
	else {
		okTopCmpt("BottomLeftHeading","bottomLeft",selectNum);	
	}
	jQuery(".focusOutSaveBLeftHeading").focus();
}


function mouseOut103_1(){
		jQuery("#editComp_377_2").focus();
	
}
function mouseOut103_2(){
		jQuery("#editComp_377_1").css("display","none");
		jQuery("#editComp_377_2").css("display","none");
		jQuery("#okDetailHeading_377").hide();
		jQuery("#editDetailHeading_377").show();
		if(jQuery("#editComp_377_1").val().length==0)
		{
			jQuery("#editComp_377_1").val(" ");
		
		}
		if(jQuery("#editComp_377_2").val().length==0)
		{
			jQuery("#editComp_377_2").val(" ");
		}
		jQuery("#cust1").html(jQuery("#editComp_377_1").val());
		jQuery("#cust2").html(jQuery("#editComp_377_2").val());
		jQuery("#detailText_377").val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
		jQuery(".detailCmpt").val(jQuery("#cust1").text()+"~"+jQuery("#cust2").text());
		jQuery("#cust1").show();
		jQuery("#cust2").show();
		if((jQuery.trim((jQuery("#editComp_377_1").val())).length == 0) && (jQuery.trim((jQuery("#editComp_377_1").val())).length == 0)){
			jQuery("#customer").css("border","1px solid white");
		}
		else{
			jQuery("#customer").css("border","1px solid black");
		}
	}

