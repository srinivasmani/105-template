jQuery(document).ready(function(){
	jQuery(".editTopRightHeading").click(function(){
		jQuery(".focusOutBottomRight").focus();
	});
	jQuery(".focusOutBottomRight").focusout(function(){
		removeMultipleEdit('component38',25,31);
	});
	jQuery(".focusOutBottomRight").keydown(function(e){
		if(e.keyCode==13){
			removeMultipleEdit('component38',25,31);
		}
	});
	jQuery("#formName").keyup(function(){
		jQuery("#formNameTab").text(jQuery("#formName").val());
	});
});
function editMultipleText(id,num){
	$("#editButton"+id.substring(9)).hide();
	$("#okButton"+id.substring(9)).show();
	
	for(var i=0;i<num;i++){
		var imageId = id.substring(9);
        imageId = parseInt(imageId) + parseInt(i);
		$("#comp_"+imageId).hide();
		$("#editComp_"+imageId).css("display","inline");
	}
}

function removeMultipleEdit(id , maxChar , num){
	if(jQuery("#editComp_38").val()==""){
		alert("This field can't be blank.");
	}
	else if(jQuery("#editComp_72").val()==""){
		alert("This field can't be blank.");
	}
	else if(jQuery("#editComp_73").val()==""){
		alert("This field can't be blank.");
	}
	else if(jQuery("#editComp_74").val()==""){
		alert("This field can't be blank.");
	}
	else {
		var val = $.trim($("#"+id).val());
		for(var i=0;i<num;i++){
			var imageId = id.substring(9);
			imageId = parseInt(imageId) + parseInt(i);
			$("#comp_"+imageId).show();
			$("#editComp_"+imageId).css("display","none");
			$("#editButton"+imageId).show();
			$("#okButton"+imageId).hide();
			$("#comp_"+imageId).html($("#editComp_"+imageId).val());
		}
	}
}