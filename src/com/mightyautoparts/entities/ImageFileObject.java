package com.mightyautoparts.entities;

import java.io.File;

public class ImageFileObject
{
  private String fileName;
  private File uploadImageFile;
  
  public ImageFileObject(String fileName, File uploadImageFile)
  {
    this.fileName = fileName;
    this.uploadImageFile = uploadImageFile;
  }
  
  public String getFileName()
  {
    return this.fileName;
  }
  
  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }
  
  public File getUploadImageFile()
  {
    return this.uploadImageFile;
  }
  
  public void setUploadImageFile(File uploadImageFile)
  {
    this.uploadImageFile = uploadImageFile;
  }
}
