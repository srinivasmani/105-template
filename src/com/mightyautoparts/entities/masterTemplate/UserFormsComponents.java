package com.mightyautoparts.entities.masterTemplate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

@Entity
@Table(name="USER_FORMS_COMPONENTS")
public class UserFormsComponents
  implements Comparable<Position>
{
  @ManyToOne
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  @JoinColumn(name="USER_FORM_ID")
  private UserForms userForms;
  @OneToOne
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  @JoinColumn(name="COMPONENT_ID")
  private Components component;
  @OneToOne
  @JoinColumn(name="POSITION_ID")
  private Position position;
  @OneToOne
  @JoinColumn(name="STATUS_ID")
  private Status status;
  @ManyToOne
  @JoinColumn(name="DOCUMENT_ID")
  private Document document;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="USER_FORMS_COMPONENT_ID")
  private Long userFormsComponentId;
  @Column(name="COLOR_PREFERENCE")
  private String colorPreference;
  @Column(name="USER_ID")
  private Long userId;
  @Column(name="PARENT_COMPONENT_ID")
  private Long parentComponentId;
  
  public UserFormsComponents(Components component, Position position, Status status, Long userId, Long parentComponentId, UserForms userForms)
  {
    this.component = component;
    this.position = position;
    this.status = status;
    this.userId = userId;
    this.parentComponentId = parentComponentId;
    this.userForms = userForms;
  }
  
  public UserFormsComponents(Long id, Long componentId, Long userFormId)
  {
    this.userFormsComponentId = id;
    this.component = new Components(componentId);
    this.userForms = new UserForms(userFormId);
  }
  
  public Long getParentComponentId()
  {
    return this.parentComponentId;
  }
  
  public void setParentComponentId(Long parentComponentId)
  {
    this.parentComponentId = parentComponentId;
  }
  
  public UserFormsComponents() {}
  
  public UserForms getUserForms()
  {
    return this.userForms;
  }
  
  public void setUserForms(UserForms userForms)
  {
    this.userForms = userForms;
  }
  
  public Components getComponent()
  {
    return this.component;
  }
  
  public void setComponent(Components component)
  {
    this.component = component;
  }
  
  public Position getPosition()
  {
    return this.position;
  }
  
  public void setPosition(Position position)
  {
    this.position = position;
  }
  
  public Status getStatus()
  {
    return this.status;
  }
  
  public void setStatus(Status status)
  {
    this.status = status;
  }
  
  public Document getDocument()
  {
    return this.document;
  }
  
  public void setDocument(Document document)
  {
    this.document = document;
  }
  
  public Long getUserFormsComponentId()
  {
    return this.userFormsComponentId;
  }
  
  public void setUserFormsComponentId(Long userFormsComponentId)
  {
    this.userFormsComponentId = userFormsComponentId;
  }
  
  public String getColorPreference()
  {
    return this.colorPreference;
  }
  
  public void setColorPreference(String colorPreference)
  {
    this.colorPreference = colorPreference;
  }
  
  public Long getUserId()
  {
    return this.userId;
  }
  
  public void setUserId(Long userId)
  {
    this.userId = userId;
  }
  
  public int compareTo(Position o)
  {
    return this.position.getPositionId().compareTo(o.getPositionId());
  }
}
