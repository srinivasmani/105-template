package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="COMPONENTS")
public class Components
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE)
  @Column(name="COMPONENT_ID", nullable=false)
  private Long componentId;
  @Column(name="COMPONENT_NAME", length=50, nullable=true)
  private String componentName;
  @Column(name="CREATED_DATE", nullable=true)
  private Date createdDate;
  @Column(name="CREATED_BY", length=50, nullable=true)
  private String createdBy;
  @Column(name="UPDATED_DATE", nullable=false)
  private Date updatedDate;
  @Column(name="UPDATED_BY", length=50, nullable=true)
  private String updatedBy;
  @OneToOne(fetch=FetchType.EAGER, optional=true, mappedBy="component")
  private UserFormsComponents userFormsComponents;
  @OneToOne(fetch=FetchType.EAGER)
  @JoinTable(name="SUB_COMPONENTS", joinColumns={@javax.persistence.JoinColumn(name="COMPONENT_ID")}, inverseJoinColumns={@javax.persistence.JoinColumn(name="SUB_COMPONENT_ID")})
  private SubComponents subComponents;
  @OneToOne(fetch=FetchType.EAGER)
  @JoinTable(name="TEMPLATE_COMPONENTS", joinColumns={@javax.persistence.JoinColumn(name="COMPONENT_ID")}, inverseJoinColumns={@javax.persistence.JoinColumn(name="TEMPLATE_COMPONENTS_ID")})
  private TemplateComponent templateComponent;
  
  public Components() {}
  
  public Components(String componentName, Date createdDate, String createdBy, Date updatedDate, String updatedBy)
  {
    this.componentName = componentName;
    this.createdDate = createdDate;
    this.createdBy = createdBy;
    this.updatedDate = updatedDate;
    this.updatedBy = updatedBy;
  }
  
  public Components(Long componentId2)
  {
    this.componentId = componentId2;
  }
  
  public Long getComponentId()
  {
    return this.componentId;
  }
  
  public void setComponentId(Long componentId)
  {
    this.componentId = componentId;
  }
  
  public String getComponentName()
  {
    return this.componentName;
  }
  
  public void setComponentName(String componentName)
  {
    this.componentName = componentName;
  }
  
  public Date getCreatedDate()
  {
    return this.createdDate;
  }
  
  public void setCreatedDate(Date createdDate)
  {
    this.createdDate = createdDate;
  }
  
  public String getCreatedBy()
  {
    return this.createdBy;
  }
  
  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }
  
  public Date getUpdatedDate()
  {
    return this.updatedDate;
  }
  
  public void setUpdatedDate(Date updatedDate)
  {
    this.updatedDate = updatedDate;
  }
  
  public String getUpdatedBy()
  {
    return this.updatedBy;
  }
  
  public void setUpdatedBy(String updatedBy)
  {
    this.updatedBy = updatedBy;
  }
  
  public SubComponents getSubComponents()
  {
    return this.subComponents;
  }
  
  public void setSubComponents(SubComponents subComponents)
  {
    this.subComponents = subComponents;
  }
  
  public TemplateComponent getTemplateComponent()
  {
    return this.templateComponent;
  }
  
  public UserFormsComponents getUserFormsComponents()
  {
    return this.userFormsComponents;
  }
  
  public void setUserFormsComponents(UserFormsComponents userFormsComponents)
  {
    this.userFormsComponents = userFormsComponents;
  }
  
  public void setTemplateComponent(TemplateComponent templateComponent)
  {
    this.templateComponent = templateComponent;
  }
}
