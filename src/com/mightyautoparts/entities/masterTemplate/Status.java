package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STATUS")
public class Status
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="STATUS_ID", nullable=false)
  private Long statusId;
  @Column(name="STATUS_DESCRIPTION", length=50, nullable=true)
  private String statusDescription;
  
  public Long getStatusId()
  {
    return this.statusId;
  }
  
  public void setStatusId(Long statusId)
  {
    this.statusId = statusId;
  }
  
  public String getStatusDescription()
  {
    return this.statusDescription;
  }
  
  public void setStatusDescription(String statusDescription)
  {
    this.statusDescription = statusDescription;
  }
}
