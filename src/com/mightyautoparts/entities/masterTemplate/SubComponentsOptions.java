package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="SUB_COMPONENTS_OPTIONS")
public class SubComponentsOptions
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @ManyToOne
  @JoinColumn(name="SUB_COMPONENT_ID")
  private SubComponents subComponents;
  @OneToOne
  @JoinColumn(name="OPTION_ID")
  private Options options;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="SUB_COMPONENTS_OPTIONS_ID", nullable=false)
  private Long subComponentOptionsId;
  @Column(name="STATUS")
  private String status;
  @Column(name="OPTION_TYPE")
  private String optionType;
  
  public SubComponentsOptions(String status, String optionType, Options options)
  {
    this.status = status;
    this.optionType = optionType;
    this.options = options;
  }
  
  public SubComponentsOptions(String status, String optionType, Options options, SubComponents subComponents)
  {
    this.status = status;
    this.optionType = optionType;
    this.options = options;
    this.subComponents = subComponents;
  }
  
  public String getOptionType()
  {
    return this.optionType;
  }
  
  public void setOptionType(String optionType)
  {
    this.optionType = optionType;
  }
  
  public SubComponentsOptions() {}
  
  public Options getOptions()
  {
    return this.options;
  }
  
  public void setOptions(Options options)
  {
    this.options = options;
  }
  
  public SubComponents getSubComponents()
  {
    return this.subComponents;
  }
  
  public void setSubComponents(SubComponents subComponents)
  {
    this.subComponents = subComponents;
  }
  
  public Long getSubComponentOptionsId()
  {
    return this.subComponentOptionsId;
  }
  
  public void setSubComponentOptionsId(Long subComponentOptionsId)
  {
    this.subComponentOptionsId = subComponentOptionsId;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
}
