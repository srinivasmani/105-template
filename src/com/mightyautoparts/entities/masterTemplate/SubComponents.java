package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="SUB_COMPONENTS")
public class SubComponents
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @OneToMany(mappedBy="subComponents", fetch=FetchType.EAGER)
  private List<SubComponentsOptions> subComponentsOptions = new ArrayList();
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="SUB_COMPONENT_ID", nullable=false)
  private Long subComponentId;
  @Column(name="COMPONENT_ID", nullable=false)
  private Long componentId;
  @Column(name="LABEL", length=50, nullable=true)
  private String label;
  @Column(name="MODIFIED_DATE")
  private Date modifiedDate;
  @Column(name="MODIFIED_BY")
  private String modifiedBy;
  @Column(name="CREATED_DATE")
  private Date createddate;
  @Column(name="CREATED_BY")
  private String createdBy;
  
  public List<SubComponentsOptions> getSubComponentsOptions()
  {
    return this.subComponentsOptions;
  }
  
  public void setSubComponentsOptions(List<SubComponentsOptions> subComponentsOptions)
  {
    this.subComponentsOptions = subComponentsOptions;
  }
  
  public SubComponents() {}
  
  public SubComponents(Long componentId, String label, Date modifiedDate, String modifiedBy, Date createddate, String createdBy)
  {
    this.componentId = componentId;
    this.label = label;
    this.modifiedDate = modifiedDate;
    this.modifiedBy = modifiedBy;
    this.createddate = createddate;
  }
  
  public Long getSubComponentId()
  {
    return this.subComponentId;
  }
  
  public void setSubComponentId(Long subComponentId)
  {
    this.subComponentId = subComponentId;
  }
  
  public Long getComponentId()
  {
    return this.componentId;
  }
  
  public void setComponentId(Long componentId)
  {
    this.componentId = componentId;
  }
  
  public String getLabel()
  {
    return this.label;
  }
  
  public void setLabel(String label)
  {
    this.label = label;
  }
  
  public Date getModifiedDate()
  {
    return this.modifiedDate;
  }
  
  public void setModifiedDate(Date modifiedDate)
  {
    this.modifiedDate = modifiedDate;
  }
  
  public String getModifiedBy()
  {
    return this.modifiedBy;
  }
  
  public void setModifiedBy(String modifiedBy)
  {
    this.modifiedBy = modifiedBy;
  }
  
  public Date getCreateddate()
  {
    return this.createddate;
  }
  
  public void setCreateddate(Date createddate)
  {
    this.createddate = createddate;
  }
  
  public String getCreatedBy()
  {
    return this.createdBy;
  }
  
  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }
}
