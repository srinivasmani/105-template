package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="OPTIONS")
public class Options
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="OPTION_ID", nullable=false)
  private Long optionId;
  @Column(name="OPTIONS_NAME", length=50, nullable=false)
  private String optionName;
  @Column(name="OPTION_DESCRIPTION", length=500, nullable=true)
  private String optionDescription;
  
  public Options() {}
  
  public Options(String optionName, String optionDescription)
  {
    this.optionName = optionName;
    this.optionDescription = optionDescription;
  }
  
  public Long getOptionId()
  {
    return this.optionId;
  }
  
  public void setOptionId(Long optionId)
  {
    this.optionId = optionId;
  }
  
  public String getOptionName()
  {
    return this.optionName;
  }
  
  public void setOptionName(String optionName)
  {
    this.optionName = optionName;
  }
  
  public String getOptionDescription()
  {
    return this.optionDescription;
  }
  
  public void setOptionDescription(String optionDescription)
  {
    this.optionDescription = optionDescription;
  }
}
