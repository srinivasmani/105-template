package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TEMPLATE_COMPONENTS")
public class TemplateComponent
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @ManyToOne
  @JoinColumn(name="POSITION_ID")
  private Position position;
  @ManyToOne
  @JoinColumn(name="STATUS_ID")
  private Status status;
  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE)
  @Column(name="TEMPLATE_COMPONENTS_ID", nullable=false)
  private Long templateComponentsId;
  @Column(name="TEMPLATE_ID", nullable=false)
  private Long templateId;
  @Column(name="COMPONENT_ID", nullable=false)
  private Long componentId;
  
  public TemplateComponent(Long templateId, Long componentId, Position position, Status status)
  {
    this.templateId = templateId;
    this.componentId = componentId;
    this.status = status;
    this.position = position;
  }
  
  public TemplateComponent() {}
  
  public Long getTemplateId()
  {
    return this.templateId;
  }
  
  public void setTemplateId(Long templateId)
  {
    this.templateId = templateId;
  }
  
  public Long getComponentId()
  {
    return this.componentId;
  }
  
  public void setComponentId(Long componentId)
  {
    this.componentId = componentId;
  }
  
  public Position getPosition()
  {
    return this.position;
  }
  
  public void setPosition(Position position)
  {
    this.position = position;
  }
  
  public Status getStatus()
  {
    return this.status;
  }
  
  public void setStatus(Status status)
  {
    this.status = status;
  }
  
  public Long getTemplateComponentsId()
  {
    return this.templateComponentsId;
  }
  
  public void setTemplateComponentsId(Long templateComponentsId)
  {
    this.templateComponentsId = templateComponentsId;
  }
}
