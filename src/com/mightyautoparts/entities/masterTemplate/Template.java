package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="TEMPLATE")
public class Template
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @OneToMany
  @JoinTable(name="USER_FORMS", joinColumns={@javax.persistence.JoinColumn(name="TEMPLATE_ID")}, inverseJoinColumns={@javax.persistence.JoinColumn(name="USER_FORM_ID")})
  private List<UserForms> userForms = new ArrayList();
  @ManyToMany(fetch=FetchType.LAZY)
  @JoinTable(name="TEMPLATE_COMPONENTS", joinColumns={@javax.persistence.JoinColumn(name="TEMPLATE_ID")}, inverseJoinColumns={@javax.persistence.JoinColumn(name="COMPONENT_ID")})
  List<Components> components;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="TEMPLATE_ID", nullable=false)
  private Long templateId;
  @Column(name="TEMPLATE_NAME", length=50, nullable=false)
  private String templateName;
  @Column(name="CREATED_DATE", nullable=false)
  private Date createdDate;
  @Column(name="CREATED_BY", length=50, nullable=false)
  private String createdBy;
  @Column(name="UPDATED_DATE", nullable=false)
  private Date updatedDate;
  @Column(name="UPDATED_BY", length=50, nullable=false)
  private String updatedBy;
  @Column(name="HEADER_TEXT", length=50, nullable=true)
  private String headerText;
  @Column(name="HEADING_COLOR", length=50, nullable=true)
  private String headingColor;
  @Column(name="TEMPLATE_IMAGE", length=200, nullable=true)
  private String templateImage;
  @Column(name="TEMPLATE_LEFT_IMAGE", length=200, nullable=true)
  private String templateLeftImage;
  @Column(name="TEMPLATE_RIGHT_IMAGE", length=200, nullable=true)
  private String templateRightImage;
  @Column(name="IMAGE_TYPE", length=20, nullable=true)
  private String imageType;
  @Column(name="LOGO_TEXT")
  private String logoText;
  
  public String getLogoText()
  {
    return this.logoText;
  }
  
  public void setLogoText(String logoText)
  {
    this.logoText = logoText;
  }
  
  public List<Components> getComponents()
  {
    return this.components;
  }
  
  public void setComponents(List<Components> components)
  {
    this.components = components;
  }
  
  public Long getTemplateId()
  {
    return this.templateId;
  }
  
  public void setTemplateId(Long templateId)
  {
    this.templateId = templateId;
  }
  
  public String getTemplateName()
  {
    return this.templateName;
  }
  
  public void setTemplateName(String templateName)
  {
    this.templateName = templateName;
  }
  
  public Date getCreatedDate()
  {
    return this.createdDate;
  }
  
  public void setCreatedDate(Date createdDate)
  {
    this.createdDate = createdDate;
  }
  
  public String getCreatedBy()
  {
    return this.createdBy;
  }
  
  public void setCreatedBy(String createdBy)
  {
    this.createdBy = createdBy;
  }
  
  public Date getUpdatedDate()
  {
    return this.updatedDate;
  }
  
  public void setUpdatedDate(Date updatedDate)
  {
    this.updatedDate = updatedDate;
  }
  
  public String getUpdatedBy()
  {
    return this.updatedBy;
  }
  
  public void setUpdatedBy(String updatedBy)
  {
    this.updatedBy = updatedBy;
  }
  
  public List<UserForms> getUserForms()
  {
    return this.userForms;
  }
  
  public void setUserForms(List<UserForms> userForms)
  {
    this.userForms = userForms;
  }
  
  public String getHeaderText()
  {
    return this.headerText;
  }
  
  public void setHeaderText(String headerText)
  {
    this.headerText = headerText;
  }
  
  public String getHeadingColor()
  {
    return this.headingColor;
  }
  
  public void setHeadingColor(String headingColor)
  {
    this.headingColor = headingColor;
  }
  
  public String getTemplateImage()
  {
    return this.templateImage;
  }
  
  public void setTemplateImage(String templateImage)
  {
    this.templateImage = templateImage;
  }
  
  public void setTemplateLeftImage(String templateLeftImage)
  {
    this.templateLeftImage = templateLeftImage;
  }
  
  public String getTemplateLeftImage()
  {
    return this.templateLeftImage;
  }
  
  public void setTemplateRightImage(String templateRightImage)
  {
    this.templateRightImage = templateRightImage;
  }
  
  public String getTemplateRightImage()
  {
    return this.templateRightImage;
  }
  
  public void setImageType(String imageType)
  {
    this.imageType = imageType;
  }
  
  public String getImageType()
  {
    return this.imageType;
  }
}
