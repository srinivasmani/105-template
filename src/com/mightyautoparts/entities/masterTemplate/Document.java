package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="DOCUMENT")
public class Document
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="DOCUMENT_ID", nullable=false)
  private Long documentId;
  @Column(name="DOCUMENT_NAME", length=100, nullable=false)
  private String documentName;
  @Column(name="DOCUMENT_DESCRIPTION", length=2000, nullable=true)
  private String documentDescription;
  @Column(name="DOCUMENT_FILE", nullable=true)
  private byte[] documentFile;
  @Column(name="DOCUMENT_TYPE", length=2000, nullable=true)
  private String documentType;
  @Column(name="DOCUMENT_ROLE", length=2000, nullable=true)
  private String documentRole;
  @OneToMany(mappedBy="document")
  private List<UserFormsComponents> userFormsComponents = new ArrayList();
  
  public Long getDocumentId()
  {
    return this.documentId;
  }
  
  public void setDocumentId(Long documentId)
  {
    this.documentId = documentId;
  }
  
  public String getDocumentName()
  {
    return this.documentName;
  }
  
  public void setDocumentName(String documentName)
  {
    this.documentName = documentName;
  }
  
  public String getDocumentDescription()
  {
    return this.documentDescription;
  }
  
  public void setDocumentDescription(String documentDescription)
  {
    this.documentDescription = documentDescription;
  }
  
  public byte[] getDocumentFile()
  {
    return this.documentFile;
  }
  
  public void setDocumentFile(byte[] documentFile)
  {
    this.documentFile = documentFile;
  }
  
  public List<UserFormsComponents> getUserFormsComponents()
  {
    return this.userFormsComponents;
  }
  
  public void setUserFormsComponents(List<UserFormsComponents> userFormsComponents)
  {
    this.userFormsComponents = userFormsComponents;
  }
  
  public String getDocumentType()
  {
    return this.documentType;
  }
  
  public void setDocumentType(String documentType)
  {
    this.documentType = documentType;
  }
  
  public String getDocumentRole()
  {
    return this.documentRole;
  }
  
  public void setDocumentRole(String documentRole)
  {
    this.documentRole = documentRole;
  }
}
