package com.mightyautoparts.entities.masterTemplate;

import java.util.Comparator;

class UserForms$1
  implements Comparator<UserFormsComponents>
{
  UserForms$1(UserForms paramUserForms) {}
  
  public int compare(UserFormsComponents ufc1, UserFormsComponents ufc2)
  {
    return ufc1.getPosition().getPositionId().compareTo(ufc2.getPosition().getPositionId());
  }
}
