package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="USER_IFS")
public class UserIfs
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @OneToMany(mappedBy="userIfs", fetch=FetchType.EAGER)
  private List<UserForms> userForms = new ArrayList();
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="USER_ID", nullable=false)
  private Long userId;
  @Column(name="USER_NAME")
  private String userName;
  @Column(name="USER_ROLE")
  private String userRole;
  @Column(name="SESS_USER_ID")
  private Long sessUserId;
  
  public UserIfs() {}
  
  public UserIfs(String userName, String userRole, Long sessUserId)
  {
    this.userName = userName;
    this.userRole = userRole;
    this.sessUserId = sessUserId;
  }
  
  public Long getUserId()
  {
    return this.userId;
  }
  
  public void setUserId(Long userId)
  {
    this.userId = userId;
  }
  
  public String getUserName()
  {
    return this.userName;
  }
  
  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  
  public String getUserRole()
  {
    return this.userRole;
  }
  
  public void setUserRole(String userRole)
  {
    this.userRole = userRole;
  }
  
  public List<UserForms> getUserForms()
  {
    return this.userForms;
  }
  
  public void setUserForms(List<UserForms> userForms)
  {
    this.userForms = userForms;
  }
  
  public Long getSessUserId()
  {
    return this.sessUserId;
  }
  
  public void setSessUserId(Long sessUserId)
  {
    this.sessUserId = sessUserId;
  }
}
