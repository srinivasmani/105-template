package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

@Entity
@Table(name="USER_FORMS")
public class UserForms
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @OneToMany(mappedBy="userForms")
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  private List<UserFormsComponents> userFormComponents = new ArrayList();
  @ManyToOne
  @JoinColumn(name="USER_ID")
  private UserIfs userIfs;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="USER_FORM_ID", nullable=false)
  private Long userFormId;
  @Column(name="FORM_NAME", nullable=false)
  private String formName;
  @Column(name="STATUS", nullable=false)
  private String status;
  @Column(name="TEMPLATE_ID", nullable=false)
  private Long templateId;
  @Column(name="HEADER_TEXT", length=50, nullable=true)
  private String headerText;
  @Column(name="HEADING_COLOR", length=50, nullable=true)
  private String headingColor;
  @Column(name="FORM_IMAGE", length=200, nullable=true)
  private String formImage;
  @Column(name="FORM_LEFT_IMAGE", length=200, nullable=true)
  private String formLeftImage;
  @Column(name="FORM_RIGHT_IMAGE", length=200, nullable=true)
  private String formRightImage;
  @Column(name="IMAGE_TYPE", length=20, nullable=true)
  private String formImgType;
  @Column(name="CREATED_TIME")
  private Date createdTime;
  @Column(name="LOGO_TEXT")
  private String logoText;
  
  public UserForms() {}
  
  public UserForms(String formName, String status, Long templateId, String headerText, String headingColor, String formImage, UserIfs userIfs, String formImgType, String formLeftImage, String formRightImage, Date createdTime)
  {
    this.formName = formName;
    this.status = status;
    this.templateId = templateId;
    this.headerText = headerText;
    this.headingColor = headingColor;
    this.formImage = formImage;
    this.userIfs = userIfs;
    this.formImgType = formImgType;
    this.formLeftImage = formLeftImage;
    this.formRightImage = formRightImage;
    this.createdTime = createdTime;
  }
  
  public UserForms(Long userFormId2)
  {
    this.userFormId = userFormId2;
  }
  
  public Date getCreatedTime()
  {
    return this.createdTime;
  }
  
  public void setCreatedTime(Date createdTime)
  {
    this.createdTime = createdTime;
  }
  
  public List<UserFormsComponents> getUserFormComponents()
  {
    Collections.sort(this.userFormComponents, new Comparator<UserFormsComponents>()
    {
      public int compare(UserFormsComponents ufc1, UserFormsComponents ufc2)
      {
        return ufc1.getPosition().getPositionId().compareTo(ufc2.getPosition().getPositionId());
      }
    });
    return this.userFormComponents;
  }
  
  public String getLogoText()
  {
    return this.logoText;
  }
  
  public void setLogoText(String logoText)
  {
    this.logoText = logoText;
  }
  
  public void setUserFormComponents(List<UserFormsComponents> userFormComponents)
  {
    this.userFormComponents = userFormComponents;
  }
  
  public Long getUserFormId()
  {
    return this.userFormId;
  }
  
  public void setUserFormId(Long userFormId)
  {
    this.userFormId = userFormId;
  }
  
  public String getFormName()
  {
    return this.formName;
  }
  
  public void setFormName(String formName)
  {
    this.formName = formName;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
  
  public Long getTemplateId()
  {
    return this.templateId;
  }
  
  public void setTemplateId(Long templateId)
  {
    this.templateId = templateId;
  }
  
  public UserIfs getUserIfs()
  {
    return this.userIfs;
  }
  
  public void setUserIfs(UserIfs userIfs)
  {
    this.userIfs = userIfs;
  }
  
  public String getHeaderText()
  {
    return this.headerText;
  }
  
  public void setHeaderText(String headerText)
  {
    this.headerText = headerText;
  }
  
  public String getHeadingColor()
  {
    return this.headingColor;
  }
  
  public void setHeadingColor(String headingColor)
  {
    this.headingColor = headingColor;
  }
  
  public String getFormImage()
  {
    return this.formImage;
  }
  
  public void setFormImage(String formImage)
  {
    this.formImage = formImage;
  }
  
  public void setFormLeftImage(String formLeftImage)
  {
    this.formLeftImage = formLeftImage;
  }
  
  public String getFormLeftImage()
  {
    return this.formLeftImage;
  }
  
  public void setFormRightImage(String formRightImage)
  {
    this.formRightImage = formRightImage;
  }
  
  public String getFormRightImage()
  {
    return this.formRightImage;
  } 
  public void setFormImgType(String formImgType)
  {
    this.formImgType = formImgType;
  }
  
  public String getFormImgType()
  {
    return this.formImgType;
  }
}
