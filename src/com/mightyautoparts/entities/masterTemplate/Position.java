package com.mightyautoparts.entities.masterTemplate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="POSITION")
public class Position
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="POSITION_ID", nullable=false)
  private Long positionId;
  @Column(name="POSITION_DESCRIPTION", nullable=true)
  private String positionDescription;
  @Column(name="POSITION_NAME", length=100, nullable=false)
  private String positionName;
  
  public Long getPositionId()
  {
    return this.positionId;
  }
  
  public void setPositionId(Long positionId)
  {
    this.positionId = positionId;
  }
  
  public String getPositionDescription()
  {
    return this.positionDescription;
  }
  
  public void setPositionDescription(String positionDescription)
  {
    this.positionDescription = positionDescription;
  }
  
  public String getPositionName()
  {
    return this.positionName;
  }
  
  public void setPositionName(String positionName)
  {
    this.positionName = positionName;
  }
}
