package com.mightyautoparts.imageLibrary;

import com.mightyautoparts.dao.masterTemplate.DocumentDao;
import com.mightyautoparts.entities.masterTemplate.Document;
import com.mightyautoparts.util.MightyProperties;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ImageLibrary
  extends QuartzJobBean
{
  private static final Logger log = Logger.getLogger(ImageLibrary.class);
  private DocumentDao documentDao;
  
  public boolean createTempImageFolder(String directoryName)
  {
    boolean success = new File(directoryName).mkdir();
    return success;
  }
  
  @Autowired
  public void setDocumentDao(DocumentDao documentDao)
  {
    this.documentDao = documentDao;
  }
  
  protected void executeInternal(JobExecutionContext ctx)
    throws JobExecutionException
  {
    String path = MightyProperties.getImageLibraryPath();
    path = path != null ? path : MightyProperties.getProperty("file.path");
    File f = null;
    try
    {
      f = new File(path);
      if (!f.exists()) {
        f.mkdir();
      }
    }
    catch (Exception e)
    {
      log.info("Folder ImageLibrary is not created :" + e);
    }
    log.info("Inside executeInternal.....");
    try
    {
      if (f != null)
      {
        List<Document> items = this.documentDao.getAllDocumentItem();
       String filename = path;
        for (Document document : items)
        {
          log.info("Image saving as :" + document.getDocumentName());
          byte[] bDocument = document.getDocumentFile();
          String imgName = document.getDocumentName();
          try
          {
            if (bDocument.length > 0)
            {
              FileOutputStream fos = new FileOutputStream(filename + "" + MightyProperties.getProperty("file.separator") + "" + imgName);
              fos.write(bDocument);
              fos.flush();
              fos.close();
            }
          }
          catch (Exception e)
          {
            e.printStackTrace();
          }
        }
      }
    }
    catch (Exception e1)
    {
      String filename;
      log.info("Image is not saved :" + e1);
      e1.printStackTrace();
    }
  }
}
