package com.mightyautoparts.pdfutils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;

public class Template104
{
  private static final Logger LOGGER = Logger.getLogger(Template104.class);
  List<String> firstReactangeComonents = new ArrayList();
  List<String> thirdReactangeComonents = new ArrayList();
  List<String> sixthLastRightComonents = new ArrayList();
  List<String> firstThreeTiresInfo = new ArrayList();
  Map<Long, String> sixthRightComonents = new LinkedHashMap();
  Map<Long, String[]> linkedHashMap = new LinkedHashMap();
  Map<Long, String> headinglinkedHashMap = new LinkedHashMap();
  Map<Long, String> rightSubheadings = new LinkedHashMap();
  Map<Long, String[]> rightHashMap = new LinkedHashMap();
  List<String> lfrfLists = new ArrayList();
  List<String> secondLfrfLists = new ArrayList();
  List<String> lastTiresComponents = new ArrayList();
  List<String> firstHeading = new ArrayList();
  List<String> rightFirstRectlastTwoSubHeading = new ArrayList();
  List<String> imageList = new ArrayList();
  List<String> rightSecondLastTexts = new ArrayList();
  List<String> dateAndInspectedBy = new ArrayList();
  Map<Long, String> underVichileConditions = new LinkedHashMap();
  String sixthComponentString = "";
  String firstComponentString = "";
  String lastTiresHeading = "";
  String fourtyTiresHeading = "";
  static String circleOrReactangle = "";
  String twelvethPositionString = "";
  String fourtyfifthString = "";
  String comments = "";
  String path = "";
  BaseFont baseF;
  private Document document;
  
  public Template104(String path)
  {
    this.path = path;
    FontFactory.register(path + "myriad" + "/impact.ttf", "impactreg");
    FontFactory.register(path + "myriad" + "/6762.ttf", "mariodbolditalic");
    FontFactory.register(path + "myriad" + "/MYRIADPRO-BOLD.OTF", "myriadprobold");
    
    FontFactory.register(path + "myriad" + "/Helvetica LT 67 Medium Condensed-1.ttf", "helveticacondensed");
    
    //FontFactory.register(path + "myriad" + "/MYRIADPRO-REGULAR.OTF", "myriadproreg");
    
    FontFactory.register(path + "myriad" + "/MyriadPro-Bold_Italic.ttf", "myriadproitalicbold");
    
    FontFactory.register(path + "myriad" + "/MyriadPro-Italic.ttf", "myriadproitalic");
    try
    {
      this.baseF = BaseFont.createFont(path + "myriad" + "/Helvetica-BoldOblique.afm", "Cp1252", false);
    }
    catch (DocumentException e)
    {
      e.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    this.document = new Document(new Rectangle(0.0F, 0.0F, 612.0F, 792.0F));
  }
  
  public Document getDocument()
  {
    return this.document;
  }
  
  public void setDocument(Document document)
  {
    this.document = document;
  }
  
  public String generatePdf(List<Components> components, String[] images, String headerText, String path, String pdfName, String imageType)
    throws DocumentException, MalformedURLException, IOException
  {
    circleOrReactangle = imageType;
    Template104 htmlToPdf = new Template104(path);
    PdfWriter pdfWriter = PdfWriter.getInstance(htmlToPdf.getDocument(), new FileOutputStream(path + "pdf/" + pdfName + ".pdf"));
    for (int temp = 0; temp < components.size(); temp++)
    {
      Components component = (Components)components.get(temp);
      String positionIdbyString = component.getTemplateComponent() != null ? component.getTemplateComponent().getPosition().getPositionName() : component.getUserFormsComponents().getPosition().getPositionName();
      
      Long statusId = component.getTemplateComponent() != null ? component.getTemplateComponent().getStatus().getStatusId() : component.getUserFormsComponents().getStatus().getStatusId();
      
      Long positionId = Long.valueOf(Long.parseLong(positionIdbyString));
      if ((positionId.longValue() == 1L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.firstComponentString = component.getComponentName();
      }
      if ((positionId.longValue() >= 2L) && (positionId.longValue() <= 4L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.firstHeading.add(component.getComponentName());
      }
      if ((positionId.longValue() == 6L) && ((statusId.longValue() == 1L) || (!component.getComponentName().equals("")))) {
        this.sixthComponentString = component.getComponentName();
      }
      if ((positionId.longValue() >= 7L) && (positionId.longValue() <= 10L)) {
        if (statusId.longValue() != 1L) {
          this.firstReactangeComonents.add("");
        } else {
          this.firstReactangeComonents.add(component.getComponentName());
        }
      }
      if (positionId.longValue() == 12L) {
        if (statusId.longValue() != 1L) {
          this.twelvethPositionString = "";
        } else {
          this.twelvethPositionString = component.getComponentName();
        }
      }
      if ((positionId.longValue() >= 14L) && (positionId.longValue() <= 20L)) {
        if (statusId.longValue() != 1L) {
          this.thirdReactangeComonents.add("");
        } else {
          this.thirdReactangeComonents.add(component.getComponentName());
        }
      }
      if (((positionId.longValue() >= 5L) || (positionId.longValue() <= 11L) || (positionId.longValue() >= 13L) || (positionId.longValue() <= 21L) || (positionId.longValue() <= 27L)) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.headinglinkedHashMap.put(positionId, component.getComponentName());
      }
      if ((positionId.longValue() >= 22L) && (positionId.longValue() <= 26L)) {
        if (statusId.longValue() != 1L) {
          this.underVichileConditions.put(positionId, " ");
        } else {
          this.underVichileConditions.put(positionId, component.getComponentName());
        }
      }
      if ((positionId.longValue() >= 29L) && (positionId.longValue() <= 31L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.firstThreeTiresInfo.add(component.getComponentName());
      }
      if ((positionId.longValue() == 32L) || (positionId.longValue() == 36L))
      {
        long tempstatusId1 = (((Components)components.get(temp)).getTemplateComponent() != null ? ((Components)components.get(temp)).getTemplateComponent().getStatus().getStatusId() : ((Components)components.get(temp)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        
        long tempstatusId2 = (((Components)components.get(temp + 1)).getTemplateComponent() != null ? ((Components)components.get(temp + 1)).getTemplateComponent().getStatus().getStatusId() : ((Components)components.get(temp + 1)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        if ((tempstatusId1 == 1L) && (tempstatusId2 == 1L)) {
          this.linkedHashMap.put(component.getComponentId(), new String[] { component.getComponentName(), ((Components)components.get(temp + 1)).getComponentName() });
        } else {
          this.linkedHashMap.put(component.getComponentId(), new String[] { "", "" });
        }
      }
      if ((positionId.longValue() == 34L) || (positionId.longValue() == 38L))
      {
        long tempstatusId1 = (((Components)components.get(temp)).getTemplateComponent() != null ? ((Components)components.get(temp)).getTemplateComponent().getStatus().getStatusId() : ((Components)components.get(temp)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        
        long tempstatusId2 = (((Components)components.get(temp + 1)).getTemplateComponent() != null ? ((Components)components.get(temp + 1)).getTemplateComponent().getStatus().getStatusId() : ((Components)components.get(temp + 1)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        if ((tempstatusId1 == 1L) && (tempstatusId2 == 1L)) {
          this.rightHashMap.put(component.getComponentId(), new String[] { component.getComponentName(), ((Components)components.get(temp + 1)).getComponentName() });
        } else {
          this.rightHashMap.put(component.getComponentId(), new String[] { "", "" });
        }
      }
      if ((positionId.longValue() >= 41L) && (positionId.longValue() <= 44L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.lfrfLists.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 46L) && (positionId.longValue() <= 47L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.rightFirstRectlastTwoSubHeading.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 48L) && (positionId.longValue() <= 51L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.secondLfrfLists.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 53L) && (positionId.longValue() <= 56L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.lastTiresComponents.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 59L) && (positionId.longValue() <= 61L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.sixthRightComonents.put(positionId, component.getComponentName());
      }
      if ((positionId.longValue() >= 62L) && (positionId.longValue() <= 65L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.sixthLastRightComonents.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 28L) && (positionId.longValue() <= 58L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.rightSubheadings.put(positionId, component.getComponentName());
      }
      if ((positionId.longValue() == 68L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comments = component.getComponentName();
      }
      if ((positionId.longValue() == 52L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.lastTiresHeading = component.getComponentName();
      }
      if ((positionId.longValue() == 45L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.fourtyfifthString = component.getComponentName();
      }
      if ((positionId.longValue() == 40L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.fourtyTiresHeading = component.getComponentName();
      }
      if ((positionId.longValue() >= 71L) && (positionId.longValue() <= 76L)) {
        this.imageList.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 66L) && (positionId.longValue() <= 67L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.rightSecondLastTexts.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 69L) && (positionId.longValue() <= 70L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.dateAndInspectedBy.add(component.getComponentName());
      }
    }
    htmlToPdf.getDocument().open();
    htmlToPdf.setHeadingReactangle(pdfWriter, this.headinglinkedHashMap);
    htmlToPdf.setLeftFirstReactangle(pdfWriter, this.firstReactangeComonents.size());
    
    htmlToPdf.setLeftSecondReactangle(pdfWriter, this.twelvethPositionString);
    htmlToPdf.setLeftThirdReactangle(pdfWriter, this.thirdReactangeComonents.size());
    
    htmlToPdf.setLeftFourReactangle(pdfWriter, this.underVichileConditions);
    htmlToPdf.setRightFirstReactangle(pdfWriter);
    htmlToPdf.setRightSecondReactangle(pdfWriter);
    htmlToPdf.setRightThirdReactangle(pdfWriter, this.comments);
    htmlToPdf.setImages(pdfWriter, this.imageList);
    htmlToPdf.setfirstReactangeComonents(pdfWriter, this.firstReactangeComonents);
    htmlToPdf.setThirdReactangeComonents(pdfWriter, this.thirdReactangeComonents);
    htmlToPdf.setFourthReactangeComonents(pdfWriter, this.underVichileConditions);
    htmlToPdf.setSixthLastRightRComonents(pdfWriter, this.sixthLastRightComonents);
    htmlToPdf.setTireFirstThirdComponents(pdfWriter, this.firstThreeTiresInfo);
    htmlToPdf.setTiresLastComponents(pdfWriter, this.lastTiresComponents);
    htmlToPdf.setRightLFRFcomponents(pdfWriter, this.linkedHashMap, this.rightHashMap);
    htmlToPdf.setTyreFirstlfrf(pdfWriter, this.lfrfLists);
    htmlToPdf.setUniqueComponent(pdfWriter, this.firstComponentString);
    htmlToPdf.setTyreSecondlfrf(pdfWriter, this.secondLfrfLists);
    htmlToPdf.setRightSubHeadings(pdfWriter, this.rightSubheadings);
    htmlToPdf.setHeadingImage(pdfWriter, images);
    htmlToPdf.setCondtionInfo(pdfWriter, this.firstHeading);
    htmlToPdf.setSixthComponent(pdfWriter, this.sixthComponentString);
    htmlToPdf.setLastTwoSubHeadings(pdfWriter, this.rightFirstRectlastTwoSubHeading);
    htmlToPdf.setLastHeadings(pdfWriter, this.lastTiresHeading, this.fourtyfifthString, this.fourtyTiresHeading);
    htmlToPdf.setRightLastTwoTexts(pdfWriter, this.rightSecondLastTexts);
    htmlToPdf.dateAndInspectedBy(pdfWriter, this.dateAndInspectedBy);
    htmlToPdf.setTitle(pdfWriter, headerText);
    htmlToPdf.setSixthRightRComonents(pdfWriter, this.sixthRightComonents);
    htmlToPdf.getDocument().close();
    LOGGER.info("generated pdf file namme:template104 .pdf");
    return pdfName + ".pdf";
  }
  
  public void dateAndInspectedBy(PdfWriter pdfWriter, List<String> rightSecondLastTexts2)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 11.0F);
    canvas.showTextAligned(0, (String)rightSecondLastTexts2.get(0), 302.0F, 30.0F, 0.0F);
    
    canvas.showTextAligned(0, (String)rightSecondLastTexts2.get(1), 450.0F, 30.0F, 0.0F);
    
    canvas.setFontAndSize(bf_helv, 5.0F);
    canvas.showTextAligned(0, "Form  #104", 530.0F, 18.0F, 0.0F);
    canvas.endText();
    canvas.setLineWidth(1.0F);
    canvas.moveTo(307.0F + bf_helv.getWidthPoint(WordUtils.capitalize((String)rightSecondLastTexts2.get(0)), 11.0F), 29.0F);
    
    canvas.lineTo(447.0F, 29.0F);
    canvas.moveTo(453.0F + bf_helv.getWidthPoint(WordUtils.capitalize((String)rightSecondLastTexts2.get(1)), 11.0F), 29.0F);
    
    canvas.lineTo(558.0F, 29.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setRightLastTwoTexts(PdfWriter pdfWriter, List<String> rightSecondLastTexts2)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 11.0F);
    for (int temp = 0; temp < rightSecondLastTexts2.size(); temp++) {
      canvas.showTextAligned(0, (String)rightSecondLastTexts2.get(temp), 320 + temp * 155, 180.0F, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setImages(PdfWriter pdfWriter, List<String> images)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    try
    {
      Image img1 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(0));
      
      img1.scaleAbsoluteHeight(img1.getHeight() * 0.52F);
      img1.scaleAbsoluteWidth(img1.getWidth() * 0.52F);
      img1.setAlignment(1);
      img1.setAbsolutePosition(90.0F + (130.0F - img1.getWidth() * 0.52F) / 2.0F, 470.0F + (96.0F - img1.getHeight() * 0.52F) / 2.0F);
      canvas.addImage(img1);
    }
    catch (Exception e) {}
    try
    {
      Image img2 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(1));
      
      img2.scaleAbsoluteHeight(img2.getHeight() * 0.6F);
      img2.scaleAbsoluteWidth(img2.getWidth() * 0.6F);
      img2.setAlignment(1);
      img2.setAbsolutePosition(205.0F + (55.0F - img2.getWidth() * 0.6F) / 2.0F, 309.0F + (55.0F - img2.getHeight() * 0.6F) / 2.0F);
      
      canvas.addImage(img2);
    }
    catch (Exception e) {}
    try
    {
      Image img3 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(2));
      
      img3.scaleAbsoluteHeight(img3.getHeight() * 0.65F);
      img3.scaleAbsoluteWidth(img3.getWidth() * 0.6F);
      img3.setAlignment(1);
      img3.setAbsolutePosition(307.0F + (64.0F - img3.getWidth() * 0.6F) / 2.0F, 402.0F + (110.0F - img3.getHeight() * 0.65F) / 2.0F);
      canvas.addImage(img3);
    }
    catch (Exception e) {}
    try
    {
      Image img4 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(3));
      
      img4.scaleAbsoluteHeight(img4.getHeight() * 0.55F);
      img4.scaleAbsoluteWidth(img4.getWidth() * 0.55F);
      img4.setAlignment(1);
      img4.setAbsolutePosition(463.0F + (19.0F - img4.getWidth() * 0.55F) / 2.0F, 465.0F);
      
      canvas.addImage(img4);
    }
    catch (Exception e) {}
    try
    {
      Image img5 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(4));
      
      img5.scaleAbsoluteHeight(img5.getHeight() * 0.6545F);
      img5.scaleAbsoluteWidth(img5.getWidth() * 0.52F);
      img5.setAlignment(1);
      img5.setAbsolutePosition(448.0F + (100.0F - img5.getWidth() * 0.52F) / 2.0F, 200.0F + (143.0F - img5.getHeight() * 0.6545F) / 2.0F);
      canvas.addImage(img5);
    }
    catch (Exception e) {}
    try
    {
      Image img6 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(5));
      
      img6.scaleAbsoluteHeight(img6.getHeight() * 0.58F);
      img6.scaleAbsoluteWidth(img6.getWidth() * 0.61F);
      img6.setAlignment(1);
      img6.setAbsolutePosition(375.0F + (100.0F - img6.getWidth() * 0.6F) / 2.0F, 173.0F);
      
      canvas.addImage(img6);
    }
    catch (Exception e) {}
    canvas.saveState();
    canvas.setLineWidth(0.5F);
    if (circleOrReactangle.equalsIgnoreCase("square")) {
      canvas.rectangle(448.0F, 470.0F, 9.0F, 9.0F);
    } else {
      canvas.circle(453.0F, 473.0F, 5.0F);
    }
    canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
    canvas.fillStroke();
    canvas.restoreState();
  }
  
  public void setFooter(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    canvas.showTextAligned(1, "Comments", 50.0F, 85.0F, 0.0F);
    canvas.showTextAligned(1, "inspected", 55.0F, 30.0F, 0.0F);
    canvas.showTextAligned(1, "date", 250.0F, 30.0F, 0.0F);
    canvas.setFontAndSize(bf_helv, 5.0F);
    canvas.showTextAligned(1, "Form#103", 360.0F, 20.0F, 0.0F);
    canvas.endText();
    canvas.setLineWidth(0.75F);
    canvas.moveTo(75.0F, 85.0F);
    canvas.lineTo(375.0F, 85.0F);
    canvas.moveTo(30.0F, 70.0F);
    canvas.lineTo(375.0F, 70.0F);
    canvas.moveTo(30.0F, 55.0F);
    canvas.lineTo(375.0F, 55.0F);
    canvas.moveTo(85.0F, 35.0F);
    canvas.lineTo(230.0F, 35.0F);
    canvas.moveTo(265.0F, 35.0F);
    canvas.lineTo(375.0F, 35.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setHeadingReactangle(PdfWriter pdfWriter, Map<Long, String> headingMap)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("impactreg");
    BaseFont baseFont = impact.getBaseFont();
    canvas.saveState();
    canvas.rectangle(36.0F, 585.0F, 253.0F, 18.0F);
    canvas.rectangle(36.0F, 365.0F, 253.0F, 18.0F);
    canvas.rectangle(36.0F, 284.0F, 253.0F, 18.0F);
    canvas.rectangle(36.0F, 135.0F, 253.0F, 18.0F);
    canvas.rectangle(302.0F, 585.0F, 256.0F, 18.0F);
    canvas.rectangle(302.0F, 365.0F, 256.0F, 18.0F);
    canvas.rectangle(302.0F, 146.0F, 256.0F, 18.0F);
    canvas.setColorFill(new BaseColor(Color.decode("#DDDDDF")));
    canvas.fillStroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 12.0F);
    canvas.setColorFill(new BaseColor(Color.decode("#000000")));
    for (Map.Entry<Long, String> entSet : headingMap.entrySet())
    {
      if (((Long)entSet.getKey()).longValue() == 5L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 165.0F, 590.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 11L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 165.0F, 370.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 13L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 165.0F, 288.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 21L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 165.0F, 140.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 27L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 415.0F, 590.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 57L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 415.0F, 370.0F, 0.0F);
      }
    }
    canvas.endText();
    canvas.restoreState();
  }
  
  public void setInteriorImage(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.moveTo(36.0F, 560.0F);
    canvas.lineTo(36.0F, 435.0F);
    canvas.moveTo(289.0F, 560.0F);
    canvas.lineTo(289.0F, 435.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLeftFirstReactangle(PdfWriter pdfWriter, int size)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(36.0F, 460.0F, 253.0F, 125.0F);
    canvas.rectangle(36.0F, 460 - 18 * size, 253.0F, 18 * size);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < size; temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.75F);
      canvas.moveTo(36.0F, 460 - 18 * (temp + 1));
      canvas.lineTo(289.0F, 460 - 18 * (temp + 1));
      canvas.stroke();
      canvas.restoreState();
    }
  }
  
  public void setLeftSecondReactangle(PdfWriter pdfWriter, String twelvePositionString)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("helveticacondensed");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(36.0F, 307.0F, 253.0F, 58.0F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 12.0F);
    
    canvas.showTextAligned(0, twelvePositionString, 44.0F, 340.0F, 0.0F);
    
    canvas.stroke();
    canvas.endText();
    canvas.restoreState();
    if (!twelvePositionString.equals(""))
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(60.0F, 320.0F, 13.0F, 13.0F);
      } else {
        canvas.circle(65.0F, 326.0F, 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(80.0F, 320.0F, 13.0F, 13.0F);
      } else {
        canvas.circle(85.0F, 326.0F, 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(100.0F, 320.0F, 13.0F, 13.0F);
      } else {
        canvas.circle(105.0F, 326.0F, 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setLeftThirdReactangle(PdfWriter pdfWriter, int size)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(36.0F, 284 - 18 * size, 253.0F, 18 * size);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < size; temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.75F);
      canvas.moveTo(36.0F, 284 - 18 * (temp + 1));
      canvas.lineTo(289.0F, 284 - 18 * (temp + 1));
      canvas.stroke();
      canvas.restoreState();
    }
  }
  
  public void setLeftFourReactangle(PdfWriter pdfWriter, Map<Long, String> underVichileConditions)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    if ((underVichileConditions.size() == 5) && (((Long)((Map.Entry)underVichileConditions.entrySet().iterator().next()).getKey()).longValue() == 22L)) {
      canvas.rectangle(36.0F, 134.5F - 18.5F * (underVichileConditions.size() + 1), 253.0F, 18.5F * (underVichileConditions.size() + 1));
    } else {
      canvas.rectangle(36.0F, 134.5F - 18.5F * underVichileConditions.size(), 253.0F, 18.5F * underVichileConditions.size());
    }
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    float rectHeight = 134.5F;
    for (Map.Entry<Long, String> entSet : underVichileConditions.entrySet())
    {
      canvas.saveState();
      canvas.setLineWidth(0.75F);
      if (((Long)entSet.getKey()).longValue() == 22L)
      {
        rectHeight -= 36.0F;
      }
      else
      {
        canvas.moveTo(36.0F, rectHeight);
        canvas.lineTo(289.0F, rectHeight);
        rectHeight = (float)(rectHeight - 18.5D);
      }
      canvas.stroke();
      canvas.restoreState();
    }
  }
  
  public void setRightFirstReactangle(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(302.0F, 389.0F, 256.0F, 196.0F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.rectangle(302.0F, 389.0F, 256.0F, 115.0F);
    canvas.setLineWidth(-0.0F);
    canvas.setColorFill(new BaseColor(Color.decode("#DDDDDF")));
    canvas.fillStroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.moveTo(302.0F, 307.0F);
    canvas.lineTo(558.0F, 307.0F);
    canvas.setColorStroke(new BaseColor(Color.decode("#FFFFFF")));
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setColorStroke(new BaseColor(Color.decode("#000000")));
    canvas.setLineWidth(1.0F);
    canvas.moveTo(430.0F, 389.0F);
    canvas.lineTo(430.0F, 503.0F);
    canvas.moveTo(497.0F, 389.0F);
    canvas.lineTo(497.0F, 503.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setRightSecondReactangle(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(302.0F, 169.0F, 256.0F, 196.0F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.rectangle(302.0F, 169.0F, 256.0F, 115.0F);
    canvas.setLineWidth(-0.0F);
    canvas.setColorFill(new BaseColor(Color.decode("#DDDDDF")));
    canvas.fillStroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.moveTo(302.0F, 284.0F);
    canvas.lineTo(558.0F, 284.0F);
    canvas.setColorStroke(new BaseColor(Color.decode("#FFFFFF")));
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setRightThirdReactangle(PdfWriter pdfWriter, String comments)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(302.0F, 52.0F, 256.0F, 112.0F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    Font impact = FontFactory.getFont("impactreg");
    BaseFont baseFont = impact.getBaseFont();
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 12.0F);
    
    canvas.showTextAligned(1, comments, 425.0F, 150.0F, 0.0F);
    canvas.endText();
    canvas.setLineWidth(0.5F);
    for (int temp = 0; temp < 5; temp++)
    {
      canvas.moveTo(302.0F, 146.0F - 18.5F * temp);
      canvas.lineTo(558.0F, 146.0F - 18.5F * temp);
    }
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setfirstReactangeComonents(PdfWriter pdfWriter, List<String> firstReactangeComonents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("helveticacondensed");
    BaseFont baseFont = impact.getBaseFont();
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 12.0F);
    baseFont.getFullFontName();
    for (int temp = 0; temp < firstReactangeComonents.size(); temp++) {
      canvas.showTextAligned(0, (String)firstReactangeComonents.get(temp), 40.0F, 465 - 18 * (temp + 1), 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < firstReactangeComonents.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(235.0F, 462.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(240.0F, 468.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(253.0F, 462.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(258.0F, 468.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(271.0F, 462.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(276.0F, 468.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setThirdReactangeComonents(PdfWriter pdfWriter, List<String> ThirdReactangeComonents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("helveticacondensed");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 12.0F);
    for (int temp = 0; temp < ThirdReactangeComonents.size(); temp++) {
      canvas.showTextAligned(0, (String)ThirdReactangeComonents.get(temp), 40.0F, 288 - 18 * (temp + 1), 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < ThirdReactangeComonents.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(235.0F, 286.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(240.0F, 292.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(253.0F, 286.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(258.0F, 292.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(271.0F, 286.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(276.0F, 292.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setFourthReactangeComonents(PdfWriter pdfWriter, Map<Long, String> underVichileConditions)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("helveticacondensed");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 12.0F);
    float stringHeight = 139.0F;
    for (Map.Entry<Long, String> entSet : underVichileConditions.entrySet()) {
      if (((Long)entSet.getKey()).longValue() == 22L)
      {
        if (((String)entSet.getValue()).length() > 27)
        {
          canvas.showTextAligned(0, ((String)entSet.getValue()).substring(0, 27), 40.0F, stringHeight - 18.0F, 0.0F);
          
          canvas.showTextAligned(0, ((String)entSet.getValue()).substring(27, ((String)entSet.getValue()).length() - 1), 40.0F, stringHeight - 34.0F, 0.0F);
        }
        else
        {
          canvas.showTextAligned(0, ((String)entSet.getValue()).substring(0, ((String)entSet.getValue()).length() - 1), 40.0F, stringHeight - 18.0F, 0.0F);
        }
        stringHeight -= 34.0F;
      }
      else
      {
        canvas.showTextAligned(0, (String)entSet.getValue(), 40.0F, stringHeight - 18.7F, 0.0F);
        
        stringHeight -= 18.0F;
      }
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    float stringRectHeight = 136.5F;
    for (Map.Entry<Long, String> entSet : underVichileConditions.entrySet()) {
      if (((Long)entSet.getKey()).longValue() == 22L)
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(235.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(240.0F, stringRectHeight - 12.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(253.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(258.0F, stringRectHeight - 12.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(271.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(276.0F, stringRectHeight - 12.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
        stringRectHeight -= 36.0F;
      }
      else
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(235.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(240.0F, stringRectHeight - 12.0F, 6.8F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(253.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(258.0F, stringRectHeight - 12.0F, 6.8F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(271.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(276.0F, stringRectHeight - 12.0F, 6.8F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
        stringRectHeight -= 18.0F;
      }
    }
  }
  
  public void setSixthLastRightRComonents(PdfWriter pdfWriter, List<String> sixthLastComponents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 11.0F);
    for (int temp = 0; temp < sixthLastComponents.size(); temp++) {
      canvas.showTextAligned(0, (String)sixthLastComponents.get(temp), 343.0F, 260.0F - 18.5F * temp, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int newTemp = 0; newTemp < sixthLastComponents.size(); newTemp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(363.0F, 258.0F - 18.5F * newTemp, 13.0F, 13.0F);
      } else {
        canvas.circle(368.0F, 264.0F - 18.5F * newTemp, 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(382.0F, 258.0F - 18.5F * newTemp, 13.0F, 13.0F);
      } else {
        canvas.circle(387.0F, 264.0F - 18.5F * newTemp, 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(400.0F, 258.0F - 18.5F * newTemp, 13.0F, 13.0F);
      } else {
        canvas.circle(405.0F, 264.0F - 18.5F * newTemp, 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setSixthRightRComonents(PdfWriter pdfWriter, Map<Long, String> sixthLastComponents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font font = new Font(this.baseF, 10.0F, 3);
    BaseFont baseFont = font.getBaseFont();
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 9.7F);
    
    int temp = 0;
    for (Map.Entry<Long, String> entSet : sixthLastComponents.entrySet())
    {
      if (((Long)entSet.getKey()).longValue() == 59L) {
        if (baseFont.getWidthPoint((String)entSet.getValue(), 10.0F) > 85.0F)
        {
          StringBuffer firstLine = new StringBuffer("");
          for (int charCnt = 0; charCnt < ((String)entSet.getValue()).length(); charCnt++) {
            if (baseFont.getWidthPoint(new String(firstLine), 10.0F) <= 85.0F) {
              firstLine.append(((String)entSet.getValue()).charAt(charCnt));
            }
          }
          if (firstLine.lastIndexOf(" ", firstLine.length() - 1) != -1)
          {
            canvas.showTextAligned(0, ((String)entSet.getValue()).substring(0, firstLine.lastIndexOf(" ", firstLine.length() - 1)), 343.0F, 343.0F - 23.0F * temp, 0.0F);
            if (((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length() - 1), ((String)entSet.getValue()).length() - 1) != null) {
              canvas.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length()), ((String)entSet.getValue()).length()), 343.0F, 333.0F - 23.0F * temp, 0.0F);
            }
          }
          else
          {
            canvas.showTextAligned(0, new String(firstLine), 343.0F, 343.0F - 23.0F * temp, 0.0F);
            
            canvas.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.length(), ((String)entSet.getValue()).length()), 343.0F, 333.0F - 23.0F * temp, 0.0F);
          }
        }
        else
        {
          canvas.showTextAligned(0, (String)entSet.getValue(), 343.0F, 340.0F - 23.0F * temp, 0.0F);
        }
      }
      if (((Long)entSet.getKey()).longValue() == 60L) {
        if (baseFont.getWidthPoint((String)entSet.getValue(), 10.0F) > 85.0F)
        {
          StringBuffer firstLine = new StringBuffer("");
          for (int charCnt = 0; charCnt < ((String)entSet.getValue()).length(); charCnt++) {
            if (baseFont.getWidthPoint(new String(firstLine), 10.0F) <= 70.0F) {
              firstLine.append(((String)entSet.getValue()).charAt(charCnt));
            }
          }
          if (firstLine.lastIndexOf(" ", firstLine.length() - 1) != -1)
          {
            canvas.showTextAligned(0, ((String)entSet.getValue()).substring(0, firstLine.lastIndexOf(" ", firstLine.length() - 1)), 343.0F, 343.0F - 23.0F * temp, 0.0F);
            if (((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length() - 1), ((String)entSet.getValue()).length() - 1) != null) {
              canvas.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length()), ((String)entSet.getValue()).length()), 343.0F, 333.0F - 23.0F * temp, 0.0F);
            }
          }
          else
          {
            canvas.showTextAligned(0, new String(firstLine), 343.0F, 343.0F - 23.0F * temp, 0.0F);
            
            canvas.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.length(), ((String)entSet.getValue()).length()), 343.0F, 333.0F - 23.0F * temp, 0.0F);
          }
        }
        else
        {
          canvas.showTextAligned(0, (String)entSet.getValue(), 343.0F, 340.0F - 23.0F * temp, 0.0F);
        }
      }
      if (((Long)entSet.getKey()).longValue() == 61L) {
        if (baseFont.getWidthPoint((String)entSet.getValue(), 10.0F) > 85.0F)
        {
          StringBuffer firstLine = new StringBuffer("");
          for (int charCnt = 0; charCnt < ((String)entSet.getValue()).length(); charCnt++) {
            if (baseFont.getWidthPoint(new String(firstLine), 10.0F) <= 115.0F) {
              firstLine.append(((String)entSet.getValue()).charAt(charCnt));
            }
          }
          if (firstLine.lastIndexOf(" ", firstLine.length() - 1) != -1)
          {
            canvas.showTextAligned(0, ((String)entSet.getValue()).substring(0, firstLine.lastIndexOf(" ", firstLine.length() - 1)), 343.0F, 343.0F - 23.0F * temp, 0.0F);
            if (((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length() - 1), ((String)entSet.getValue()).length() - 1) != null) {
              canvas.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length()), ((String)entSet.getValue()).length()), 343.0F, 333.0F - 23.0F * temp, 0.0F);
            }
          }
          else
          {
            canvas.showTextAligned(0, new String(firstLine), 343.0F, 343.0F - 23.0F * temp, 0.0F);
            
            canvas.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.length(), ((String)entSet.getValue()).length()), 343.0F, 333.0F - 23.0F * temp, 0.0F);
          }
        }
        else
        {
          canvas.showTextAligned(0, (String)entSet.getValue(), 343.0F, 340.0F - 23.0F * temp, 0.0F);
        }
      }
      temp++;
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    int newtemp = 0;
    for (Map.Entry<Long, String> entSet : sixthLastComponents.entrySet())
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(325.0F, 335.0F - 23.0F * newtemp, 13.0F, 13.0F);
      } else {
        canvas.circle(330.0F, 341.0F - 23.0F * newtemp, 7.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 59L) {
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      }
      if (((Long)entSet.getKey()).longValue() == 60L) {
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      }
      if (((Long)entSet.getKey()).longValue() == 61L) {
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      }
      canvas.fillStroke();
      canvas.restoreState();
      newtemp++;
    }
  }
  
  public void setTireFirstThirdComponents(PdfWriter pdfWriter, List<String> componentNames)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 9.0F);
    float wordlength = 0.0F;
    for (int temp = 0; temp < componentNames.size(); temp++)
    {
      if (temp == 0) {
        canvas.showTextAligned(0, (String)componentNames.get(temp), 325.0F, 557.0F, 0.0F);
      }
      if (temp == 1) {
        canvas.showTextAligned(0, (String)componentNames.get(temp), 410.0F, 557.0F, 0.0F);
      }
      if (temp == 2) {
        canvas.showTextAligned(0, (String)componentNames.get(temp), 490.0F, 557.0F, 0.0F);
      }
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < componentNames.size(); temp++)
    {
      if (temp == 0)
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(310.0F, 554.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(315.0F, 560.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      if (temp == 1)
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(395.0F, 554.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(400.0F, 560.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      if (temp == 2)
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(475.0F, 554.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(480.0F, 560.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
      }
    }
  }
  
  public void setRightLFRFcomponents(PdfWriter pdfWriter, Map<Long, String[]> leftHashMap, Map<Long, String[]> rightHashMap)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 10.0F);
    
    int temp = 0;
    for (Map.Entry<Long, String[]> entSet : leftHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.showTextAligned(0, ((String[])entSet.getValue())[0], 338.0F, 540 - temp * 18, 0.0F);
        
        canvas.showTextAligned(0, ((String[])entSet.getValue())[1], 388.0F, 540 - temp * 18, 0.0F);
      }
      temp++;
    }
    int temp1 = 0;
    for (Map.Entry<Long, String[]> entSet : rightHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.showTextAligned(0, ((String[])entSet.getValue())[0], 437.0F, 540 - temp1 * 18, 0.0F);
        
        canvas.showTextAligned(0, ((String[])entSet.getValue())[1], 489.0F, 540 - temp1 * 18, 0.0F);
      }
      temp1++;
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    int newTemp = 0;
    for (Map.Entry<Long, String[]> entSet : leftHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(353.0F, 556 - 18 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(358.0F, 562 - 18 * (newTemp + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(365.0F, 556 - 18 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(370.0F, 562 - 18 * (newTemp + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(377.0F, 556 - 18 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(382.0F, 562 - 18 * (newTemp + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      newTemp++;
    }
    int newindex = 0;
    for (Map.Entry<Long, String[]> entSet : rightHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(452.0F, 558 - 18 * (newindex + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(457.0F, 562 - 18 * (newindex + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(464.0F, 558 - 18 * (newindex + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(469.0F, 562 - 18 * (newindex + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(476.0F, 558 - 18 * (newindex + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(481.0F, 562 - 18 * (newindex + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      newindex++;
    }
  }
  
  public void setTyreFirstlfrf(PdfWriter pdfWriter, List<String> lfrfLists)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 11.0F);
    for (int temp = 0; temp < lfrfLists.size(); temp++) {
      canvas.showTextAligned(0, (String)lfrfLists.get(temp), 375.0F, 454 - 17 * temp, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int newTemp = 0; newTemp < lfrfLists.size(); newTemp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(390.0F, 472 - 17 * (newTemp + 1), 9.0F, 9.0F);
      } else {
        canvas.circle(395.0F, 474 - 17 * (newTemp + 1), 5.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(402.0F, 472 - 17 * (newTemp + 1), 9.0F, 9.0F);
      } else {
        canvas.circle(407.0F, 474 - 17 * (newTemp + 1), 5.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(414.0F, 472 - 17 * (newTemp + 1), 9.0F, 9.0F);
      } else {
        canvas.circle(419.0F, 474 - 17 * (newTemp + 1), 5.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setTyreSecondlfrf(PdfWriter pdfWriter, List<String> secondLfrfLists)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 8.0F);
    for (int temp = 0; temp < secondLfrfLists.size(); temp++) {
      canvas.showTextAligned(0, (String)secondLfrfLists.get(temp), 435.0F, 445 - 14 * temp, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int newTemp = 0; newTemp < secondLfrfLists.size(); newTemp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      canvas.rectangle(447.0F, 457 - 14 * (newTemp + 1), 19.0F, 10.0F);
      canvas.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
      canvas.fillStroke();
      canvas.restoreState();
    }
    canvas.saveState();
    canvas.setLineWidth(0.5F);
    canvas.rectangle(470.0F, 429.5F, 19.0F, 23.5F);
    canvas.rectangle(470.0F, 401.0F, 19.0F, 23.5F);
    canvas.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
    canvas.fillStroke();
    canvas.restoreState();
  }
  
  public void setTiresLastComponents(PdfWriter pdfWriter, List<String> lastComponents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 8.0F);
    for (int temp = 0; temp < lastComponents.size(); temp++) {
      canvas.showTextAligned(0, (String)lastComponents.get(temp), 512.0F, 449 - 14 * temp, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int newTemp = 0; newTemp < lastComponents.size(); newTemp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      canvas.rectangle(500.0F, 462 - 14 * (newTemp + 1), 9.0F, 9.0F);
      canvas.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setRightSubHeadings(PdfWriter pdfWriter, Map<Long, String> subheadings)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 9.0F);
    for (Map.Entry<Long, String> entSet : subheadings.entrySet())
    {
      if (((Long)entSet.getKey()).longValue() == 28L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 415.0F, 575.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 58L) {
        canvas.showTextAligned(1, (String)entSet.getValue(), 425.0F, 353.0F, 0.0F);
      }
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setCondtionInfo(PdfWriter pdfWriter, List<String> components)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.beginText();
    Font myriadbold = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont tempMariad = myriadbold.getCalculatedBaseFont(false);
    canvas.setFontAndSize(tempMariad, 10.0F);
    for (int temp = 0; temp < components.size(); temp++)
    {
      if (temp == 0) {
        canvas.showTextAligned(0, (String)components.get(temp), 53.0F, 612.0F, 0.0F);
      }
      if (temp == 1) {
        canvas.showTextAligned(0, (String)components.get(temp), 222.0F, 612.0F, 0.0F);
      }
      if (temp == 2) {
        canvas.showTextAligned(0, (String)components.get(temp), 404.0F, 612.0F, 0.0F);
      }
    }
    canvas.endText();
    canvas.restoreState();
    for (int temp = 0; temp < components.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (temp == 0)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(36.0F, 609.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(41.0F, 615.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      }
      if (temp == 1)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(205.0F, 609.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(210.0F, 615.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      }
      if (temp == 2)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(387.0F, 609.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(392.0F, 615.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      }
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setHeadingImage(PdfWriter writer, String[] image)
    throws MalformedURLException, IOException, DocumentException
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + image[0]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.67F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.654545F);
      img.setAlignment(1);
      img.setAbsolutePosition(154.0F + (288.0F - img.getWidth() * 0.654545F) / 2.0F, 702.0F + (52.0F - img.getHeight() * 0.67F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e)
    {
      pdfCont.saveState();
      pdfCont.setLineWidth(0.75F);
      Font myriadpro = FontFactory.getFont("myriadproreg");
      BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
      pdfCont.beginText();
      pdfCont.setFontAndSize(imp_temp, 10.0F);
      pdfCont.setCharacterSpacing(0.0F);
      pdfCont.showTextAligned(1, "(Your logo here )", 293.0F, 736.0F, 0.0F);
      
      pdfCont.endText();
      pdfCont.rectangle(154.0F, 702.0F, 288.0F, 52.0F);
      pdfCont.stroke();
      pdfCont.restoreState();
    }
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + image[1]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.67F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.67F);
      img.setAlignment(1);
      img.setAbsolutePosition(36.0F + (102.0F - img.getWidth() * 0.654545F) / 2.0F, 684.0F + (70.0F - img.getHeight() * 0.67F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + image[2]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.67F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.67F);
      img.setAlignment(1);
      img.setAbsolutePosition(457.0F + (102.0F - img.getWidth() * 0.67F) / 2.0F, 684.0F + (70.0F - img.getHeight() * 0.67F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
  }
  
  public void setTitle(PdfWriter pdfWriter, String title)
  {
    PdfContentByte pdfCont = pdfWriter.getDirectContent();
    Font myriadpro = FontFactory.getFont("impactreg");
    BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
    pdfCont.saveState();
    pdfCont.beginText();
    pdfCont.setFontAndSize(imp_temp, 18.0F);
    pdfCont.setCharacterSpacing(0.0F);
    pdfCont.showTextAligned(1, title.toUpperCase(), 306.0F, 680.0F, 0.0F);
    pdfCont.endText();
    pdfCont.restoreState();
  }
  
  public void setSixthComponent(PdfWriter pdfWriter, String posSixth)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 7.0F);
    canvas.showTextAligned(1, posSixth, 150.0F, 575.0F, 0.0F);
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.moveTo(36.0F, 573.0F);
    canvas.lineTo(289.0F, 573.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLastTwoSubHeadings(PdfWriter pdfWriter, List<String> lastTwoComponents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 4.5F);
    for (int temp = 0; temp < lastTwoComponents.size(); temp++) {
      canvas.showTextAligned(1, (String)lastTwoComponents.get(temp), 455 + temp * 24, 458.0F, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLastHeadings(PdfWriter pdfWriter, String lastHeading, String fourthfifthString, String fourtythString)
  {
    PdfPTable lastHeadingtable = new PdfPTable(1);
    lastHeadingtable.getDefaultCell().setBorder(0);
    Paragraph paragraph = new Paragraph(lastHeading, FontFactory.getFont("myriadproitalicbold", 9.5F));
    lastHeadingtable.addCell(paragraph);
    lastHeadingtable.setTotalWidth(51.0F);
    lastHeadingtable.writeSelectedRows(0, -1, 500.0F, 496.0F, pdfWriter.getDirectContent());
    PdfPTable fourtythString1table = new PdfPTable(1);
    fourtythString1table.getDefaultCell().setBorder(0);
    Paragraph fourtythStringparagraph = new Paragraph(fourtythString, FontFactory.getFont("myriadproitalicbold", 9.5F));
    fourtythStringparagraph.setLeading(9.0F, 10.0F);
    fourtythString1table.addCell(fourtythStringparagraph);
    fourtythString1table.setTotalWidth(63.0F);
    fourtythString1table.setWidthPercentage(80.0F);
    fourtythString1table.setHorizontalAlignment(1);
    fourtythString1table.writeSelectedRows(0, -1, 369.0F, 496.0F, pdfWriter.getDirectContent());
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont baseFont = impact.getBaseFont();
    canvas.setFontAndSize(baseFont, 9.5F);
    canvas.saveState();
    canvas.beginText();
    if (!fourthfifthString.equals("")) {
      canvas.showTextAligned(1, fourthfifthString, 460.0F, 485.0F, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setUniqueComponent(PdfWriter pdfWriter, String firstComponent)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    
    String[] splittesString = firstComponent.split("~");
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 10.0F);
    canvas.showTextAligned(0, splittesString[0], 35.0F, 665.0F, 0.0F);
    
    canvas.showTextAligned(0, splittesString[1], 35.0F, 645.0F, 0.0F);
    
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
}
