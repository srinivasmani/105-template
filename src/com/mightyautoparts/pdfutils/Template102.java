package com.mightyautoparts.pdfutils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.mightyautoparts.comparator.ComponentsComparator;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;

public class Template102
{
  private static final Logger LOGGER = Logger.getLogger(Template102.class);
  private String path = "";
  private Document document;
  private float commentHeight = 556.0F;
  
  public Template102(String folderName)
  {
    this.path = folderName;
    FontFactory.register(folderName + "myriad" + "/impact.ttf", "impact");
    FontFactory.register(folderName + "myriad" + "/MYRIADPRO-BOLD.OTF", "myriadprobold");
    
   // FontFactory.register(folderName + "myriad" + "/MYRIADPRO-REGULAR.OTF", "myriadproreg");
    
    this.document = new Document(new Rectangle(0.0F, 0.0F, 612.0F, 792.0F));
  }
  
  public String generatePdf(List<Components> components, String folderName, String imageType, String pdfName, String[] image, String type, String title)
    throws DocumentException, MalformedURLException, IOException
  {
    PdfWriter pdfWriter = PdfWriter.getInstance(getDocument(), new FileOutputStream(folderName + "pdf" + "/" + pdfName + ".pdf"));
    
    getDocument().open();
    if (type.equals("template")) {
      Collections.sort(components, new ComponentsComparator());
    }
    List<Components> componentsList = new ArrayList();
    for (int temp = 6; temp < components.size(); temp++)
    {
      Components compTemp = (Components)components.get(temp);
      String positionId = compTemp.getTemplateComponent() != null ? compTemp.getTemplateComponent().getPosition().getPositionName() : compTemp.getUserFormsComponents().getPosition().getPositionName();
      
      Long statusId = compTemp.getTemplateComponent() != null ? compTemp.getTemplateComponent().getStatus().getStatusId() : compTemp.getUserFormsComponents().getStatus().getStatusId();
      if ((compTemp.getComponentName() != null) && (!compTemp.getComponentName().equals(" ")) && (statusId.longValue() == 1L) && 
        (!positionId.equals("22")) && (!positionId.equals("23"))) {
        componentsList.add(components.get(temp));
      }
    }
    int size = componentsList.size() % 2 == 0 ? componentsList.size() / 2 : componentsList.size() / 2 + 1;
    
    setLeftElement(pdfWriter, componentsList, size, imageType);
    setRightElement(pdfWriter, componentsList, componentsList.size(), imageType);
    
    setFirstComponent(pdfWriter, ((Components)components.get(0)).getComponentName());
    setHeadingImage(pdfWriter, folderName, image);
    setTitle(pdfWriter, title);
    setCondtionInfo(pdfWriter, imageType, components);
    setRightHeadings(pdfWriter, ((Components)components.get(20)).getComponentName(), ((Components)components.get(21)).getComponentName());
    
    setLeftHeadings(pdfWriter, ((Components)components.get(4)).getComponentName(), ((Components)components.get(5)).getComponentName());
    
    setLeftReactangale(pdfWriter, size);
    setRightReactangle(pdfWriter, componentsList.size());
    setFooter(pdfWriter);
    
    getDocument().close();
    LOGGER.info("generated pdf file namme:" + pdfName + ".pdf");
    return pdfName + ".pdf";
  }
  
  public void setTitle(PdfWriter pdfWriter, String title)
  {
    PdfContentByte pdfCont = pdfWriter.getDirectContent();
    Font myriadpro = FontFactory.getFont("myriadprobold");
    BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
    pdfCont.saveState();
    pdfCont.beginText();
    pdfCont.setFontAndSize(imp_temp, 18.0F);
    pdfCont.setCharacterSpacing(0.0F);
    pdfCont.showTextAligned(1, title.toUpperCase(), 306.0F, 652.0F, 0.0F);
    
    pdfCont.endText();
    pdfCont.restoreState();
  }
  
  public void setborderToPage(PdfWriter pdfWriter)
  {
    PdfContentByte pdfCont = pdfWriter.getDirectContent();
    pdfCont.saveState();
    pdfCont.setLineWidth(0.75F);
    pdfCont.rectangle(0.0F, 0.0F, 611.0F, 791.0F);
    pdfCont.stroke();
    pdfCont.restoreState();
  }
  
  private void setHeadingImage(PdfWriter writer, String folderName, String[] image)
    throws MalformedURLException, IOException, DocumentException
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    try
    {
      Image img = Image.getInstance(folderName + "..//ImageLibrary/" + image[0]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.654545F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.654545F);
      img.setAlignment(1);
      img.setAbsolutePosition(162.0F + (289.0F - img.getWidth() * 0.654545F) / 2.0F, 684.0F + (72.0F - img.getHeight() * 0.654545F) / 2.0F);
      
      pdfCont.addImage(img);
    }
    catch (Exception e)
    {
      pdfCont.saveState();
      pdfCont.setLineWidth(0.75F);
      Font myriadpro = FontFactory.getFont("myriadproreg");
      BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
      pdfCont.beginText();
      pdfCont.setFontAndSize(imp_temp, 10.0F);
      pdfCont.setCharacterSpacing(0.0F);
      pdfCont.showTextAligned(1, "(Your logo here )", 300.0F, 720.0F, 0.0F);
      
      pdfCont.endText();
      pdfCont.rectangle(162.0F, 684.0F, 289.0F, 72.0F);
      pdfCont.stroke();
      pdfCont.restoreState();
    }
    try
    {
      Image img = Image.getInstance(folderName + "..//ImageLibrary/" + image[1]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.654545F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.654545F);
      img.setAlignment(1);
      img.setAbsolutePosition(36.0F + (72.0F - img.getWidth() * 0.654545F) / 2.0F, 684.0F + (72.0F - img.getHeight() * 0.654545F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
    try
    {
      Image img = Image.getInstance(folderName + "..//ImageLibrary/" + image[2]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.654545F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.654545F);
      img.setAlignment(1);
      img.setAbsolutePosition(497.0F + (72.0F - img.getWidth() * 0.654545F) / 2.0F, 684.0F + (72.0F - img.getHeight() * 0.654545F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
  }
  
  public Document getDocument()
  {
    return this.document;
  }
  
  public void setDocument(Document document)
  {
    this.document = document;
  }
  
  public void setHeader(PdfWriter writer)
    throws DocumentException
  {
    PdfContentByte canvas = writer.getDirectContent();
    canvas.saveState();
    
    Font Impact = FontFactory.getFont("impact");
    BaseFont imp_temp = Impact.getCalculatedBaseFont(false);
    canvas.beginText();
    canvas.setFontAndSize(imp_temp, 24.0F);
    canvas.setCharacterSpacing(0.0F);
    canvas.showTextAligned(0, "jhfksdjfklds", 70.0F, 565.0F, 0.0F);
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setMileage(PdfWriter writer)
    throws DocumentException
  {
    PdfContentByte canvas = writer.getDirectContent();
    
    Font myriadpro = FontFactory.getFont("myriadproreg");
    BaseFont tempMariad = myriadpro.getCalculatedBaseFont(true);
    canvas.beginText();
    canvas.setFontAndSize(tempMariad, 10.0F);
    canvas.showTextAligned(0, "Mileage", 135.0F, 482.0F, 0.0F);
    canvas.endText();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.moveTo(170.0F, 482.0F);
    canvas.lineTo(260.0F, 482.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLeftHeadings(PdfWriter writer, String Items, String conditions)
  {
    PdfContentByte canvas = writer.getDirectContent();
    canvas.saveState();
    canvas.beginText();
    Font myriadbold = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont tempMariad = myriadbold.getCalculatedBaseFont(false);
    canvas.setFontAndSize(tempMariad, 10.0F);
    canvas.showTextAligned(1, WordUtils.capitalize(Items), 117.0F, 537.0F, 0.0F);
    
    canvas.showTextAligned(1, WordUtils.capitalize(conditions), 248.0F, 537.0F, 0.0F);
    
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setRightHeadings(PdfWriter writer, String Items, String conditions)
  {
    PdfContentByte canvas = writer.getDirectContent();
    canvas.saveState();
    canvas.beginText();
    Font myriadbold = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont tempMariad = myriadbold.getCalculatedBaseFont(false);
    canvas.setFontAndSize(tempMariad, 10.0F);
    canvas.showTextAligned(1, WordUtils.capitalize(Items), 405.0F, 537.0F, 0.0F);
    
    canvas.showTextAligned(1, WordUtils.capitalize(conditions), 536.0F, 537.0F, 0.0F);
    
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLeftElement(PdfWriter writer, List<Components> components, int size, String conditionType)
    throws DocumentException
  {
    PdfContentByte canvas = writer.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    for (int temp = 0; temp < size; temp++)
    {
      canvas.saveState();
      canvas.beginText();
      canvas.setFontAndSize(bf_helv, 10.0F);
      canvas.showTextAligned(0, WordUtils.capitalize(((Components)components.get(temp)).getComponentName()), 45.0F, 565 - 27 * (temp + 2), 0.0F);
      
      canvas.endText();
      canvas.stroke();
      canvas.restoreState();
      if (conditionType.equals("circle"))
      {
        canvas.saveState();
        canvas.setLineWidth(4.0F);
        canvas.circle(220.0F, 569 - 27 * (temp + 2), 9.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#00A651")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(4.0F);
        canvas.circle(245.0F, 569 - 27 * (temp + 2), 9.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#FFD503")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(4.0F);
        canvas.circle(270.0F, 569 - 27 * (temp + 2), 9.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#ED1C24")));
        canvas.stroke();
        canvas.restoreState();
      }
      else
      {
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(215.0F, 560 - 27 * (temp + 2), 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#00A651")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(240.0F, 560 - 27 * (temp + 2), 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#FFD503")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(265.0F, 560 - 27 * (temp + 2), 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#ED1C24")));
        canvas.stroke();
        canvas.restoreState();
      }
    }
  }
  
  public void setRightElement(PdfWriter writer, List<Components> components, int size, String imageType)
    throws DocumentException
  {
    int tempSize = size % 2 == 0 ? size / 2 : size / 2 + 1;
    PdfContentByte canvas = writer.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    int temp1 = 0;
    for (int temp = tempSize; temp < size; temp++)
    {
      canvas.saveState();
      canvas.beginText();
      canvas.setFontAndSize(bf_helv, 10.0F);
      canvas.showTextAligned(0, WordUtils.capitalize(((Components)components.get(temp)).getComponentName()), 328.0F, 565 - 27 * (temp1 + 2), 0.0F);
      
      canvas.endText();
      canvas.stroke();
      canvas.restoreState();
      if (imageType.equals("circle"))
      {
        canvas.saveState();
        canvas.setLineWidth(4.0F);
        canvas.circle(510.0F, 569 - 27 * (temp1 + 2), 9.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#00A651")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(4.0F);
        canvas.circle(535.0F, 569 - 27 * (temp1 + 2), 9.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#FFD503")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(4.0F);
        canvas.circle(560.0F, 569 - 27 * (temp1 + 2), 9.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#ED1C24")));
        canvas.stroke();
        canvas.restoreState();
      }
      else
      {
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(500.0F, 560 - 27 * (temp1 + 2), 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#00A651")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(525.0F, 560 - 27 * (temp1 + 2), 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#FFD503")));
        canvas.stroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(550.0F, 560 - 27 * (temp1 + 2), 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#ED1C24")));
        canvas.stroke();
        canvas.restoreState();
      }
      temp1++;
    }
  }
  
  public void setFooter(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    canvas.showTextAligned(0, "Comments: ", 35.0F, this.commentHeight - 30.0F, 0.0F);
    
    canvas.showTextAligned(0, "Inspected by:", 35.0F, this.commentHeight - 114.5F, 0.0F);
    
    canvas.showTextAligned(0, "Date:", 397.0F, this.commentHeight - 114.5F, 0.0F);
    
    canvas.setFontAndSize(bf_helv, 5.0F);
    canvas.showTextAligned(1, "Form #102", 550.0F, this.commentHeight - 128.0F, 0.0F);
    canvas.endText();
    canvas.setLineWidth(0.75F);
    canvas.moveTo(90.0F, this.commentHeight - 30.0F);
    canvas.lineTo(575.0F, this.commentHeight - 30.0F);
    canvas.moveTo(35.0F, this.commentHeight - 56.0F);
    canvas.lineTo(575.0F, this.commentHeight - 56.0F);
    canvas.moveTo(35.0F, this.commentHeight - 81.0F);
    canvas.lineTo(575.0F, this.commentHeight - 81.0F);
    canvas.moveTo(93.0F, this.commentHeight - 115.0F);
    canvas.lineTo(392.0F, this.commentHeight - 115.0F);
    canvas.moveTo(425.0F, this.commentHeight - 115.0F);
    canvas.lineTo(575.0F, this.commentHeight - 115.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setCondtionInfo(PdfWriter pdfWriter, String conditionType, List<Components> components)
    throws MalformedURLException, IOException, DocumentException
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.beginText();
    Font myriadbold = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont tempMariad = myriadbold.getCalculatedBaseFont(false);
    canvas.setFontAndSize(tempMariad, 10.0F);
    canvas.showTextAligned(0, ((Components)components.get(1)).getComponentName(), 75.0F, 580.0F, 0.0F);
    
    canvas.showTextAligned(0, ((Components)components.get(2)).getComponentName(), 245.0F, 580.0F, 0.0F);
    
    canvas.showTextAligned(0, ((Components)components.get(3)).getComponentName(), 455.0F, 580.0F, 0.0F);
    
    canvas.endText();
    canvas.restoreState();
    if (conditionType.equals("circle"))
    {
      canvas.saveState();
      canvas.setLineWidth(4.0F);
      if (!((Components)components.get(1)).getComponentName().equals("")) {
        canvas.circle(60.0F, 585.0F, 9.0F);
      }
      canvas.setColorStroke(new BaseColor(Color.decode("#00A651")));
      canvas.stroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(4.0F);
      if (!((Components)components.get(2)).getComponentName().equals("")) {
        canvas.circle(230.0F, 585.0F, 9.0F);
      }
      canvas.setColorStroke(new BaseColor(Color.decode("#FFD503")));
      canvas.stroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(4.0F);
      if (!((Components)components.get(3)).getComponentName().equals("")) {
        canvas.circle(438.0F, 585.0F, 9.0F);
      }
      canvas.setColorStroke(new BaseColor(Color.decode("#ED1C24")));
      canvas.stroke();
      canvas.restoreState();
    }
    else
    {
      if (!((Components)components.get(1)).getComponentName().equals(""))
      {
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(55.0F, 575.0F, 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#00A651")));
        canvas.stroke();
        canvas.restoreState();
      }
      if (!((Components)components.get(2)).getComponentName().equals(" "))
      {
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(225.0F, 575.0F, 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#FFD503")));
        canvas.stroke();
        canvas.restoreState();
      }
      if (!((Components)components.get(3)).getComponentName().equals(""))
      {
        canvas.saveState();
        canvas.setLineWidth(3.0F);
        canvas.rectangle(433.0F, 575.0F, 16.0F, 16.0F);
        canvas.setColorStroke(new BaseColor(Color.decode("#ED1C24")));
        canvas.stroke();
        canvas.restoreState();
      }
    }
  }
  
  public void setLeftReactangale(PdfWriter pdfWriter, int size)
  {
    PdfContentByte pdfCont = pdfWriter.getDirectContent();
    pdfCont.saveState();
    pdfCont.setLineWidth(1.0F);
    if (this.commentHeight >= 556 - 27 * (size + 1)) {
      this.commentHeight = (556 - 27 * (size + 1));
    }
    pdfCont.rectangle(36.0F, 556 - 27 * (size + 1), 253.0F, 27 * (size + 1));
    pdfCont.stroke();
    pdfCont.restoreState();
    pdfCont.saveState();
    pdfCont.setLineWidth(0.75F);
    pdfCont.moveTo(206.0F, 556.0F);
    pdfCont.lineTo(206.0F, 556 - 27 * (size + 1));
    pdfCont.stroke();
    pdfCont.restoreState();
    for (int temp = 0; temp <= size; temp++)
    {
      pdfCont.saveState();
      pdfCont.setLineWidth(0.75F);
      pdfCont.moveTo(36.0F, 556 - 27 * (temp + 1));
      pdfCont.lineTo(289.0F, 556 - 27 * (temp + 1));
      pdfCont.stroke();
      pdfCont.restoreState();
    }
  }
  
  public void setRightReactangle(PdfWriter pdfWriter, int size)
  {
    int tempSize = size % 2 == 0 ? size / 2 : size / 2 + 1;
    PdfContentByte pdfCont = pdfWriter.getDirectContent();
    pdfCont.saveState();
    pdfCont.setLineWidth(1.0F);
    if (this.commentHeight >= 556 - 27 * (size - tempSize + 1)) {
      this.commentHeight = (556 - 27 * (size - tempSize + 1));
    }
    pdfCont.rectangle(322.0F, 556 - 27 * (size - tempSize + 1), 253.0F, 27 * (size - tempSize + 1));
    
    pdfCont.stroke();
    pdfCont.restoreState();
    pdfCont.saveState();
    pdfCont.setLineWidth(0.75F);
    pdfCont.moveTo(492.0F, 556.0F);
    pdfCont.lineTo(492.0F, 556 - 27 * (size - tempSize + 1));
    pdfCont.stroke();
    pdfCont.restoreState();
    for (int temp = 0; temp <= size - tempSize; temp++)
    {
      pdfCont.saveState();
      pdfCont.setLineWidth(0.75F);
      pdfCont.moveTo(322.0F, 556 - 27 * (temp + 1));
      pdfCont.lineTo(575.0F, 556 - 27 * (temp + 1));
      pdfCont.stroke();
      pdfCont.restoreState();
    }
  }
  
  public void setFirstComponent(PdfWriter pdfWriter, String firstComponent)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    StringBuffer firstLine = new StringBuffer("");
    StringBuffer secondLine = new StringBuffer("");
    
    String[] splittesString = firstComponent.split("~");
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 11.0F);
    canvas.showTextAligned(0, splittesString[0], 35.0F, 630.0F, 0.0F);
    
    canvas.showTextAligned(0, splittesString[1], 35.0F, 610.0F, 0.0F);
    
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
}
