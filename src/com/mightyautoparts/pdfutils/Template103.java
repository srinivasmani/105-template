package com.mightyautoparts.pdfutils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;

public class Template103
{
  private static final Logger LOGGER = Logger.getLogger(Template103.class);
  String path;
  List<String> leftFirstLists = new ArrayList();
  List<String> leftSecondLists = new ArrayList();
  List<String> leftThirdLists = new ArrayList();
  List<String> rightFirstLists = new ArrayList();
  List<String> rightSecondLists = new ArrayList();
  List<String> rightThirdFirstLists = new ArrayList();
  Map<Long, String[]> linkedHashMap = new LinkedHashMap();
  Map<Long, String[]> rightHashMap = new LinkedHashMap();
  List<String> lfrfLists = new ArrayList();
  List<String> secondLfrfLists = new ArrayList();
  List<String> lastComponents = new ArrayList();
  List<String> lastTwoSubHeading = new ArrayList();
  List<String> firstTwoSubHeading = new ArrayList();
  List<String> firstHeading = new ArrayList();
  List<String> imagesList = new ArrayList();
  List<String> header = new ArrayList();
  List<String> bottonContents = new ArrayList();
  String lastHeading = "";
  String firstComponentString = "";
  String sixthComponentString = "";
  String fifthSeventhString = "";
  String fifthEightyhString = "";
  String fourtyComponentString = "";
  String pos52 = "";
  static String circleOrReactangle = "";
  private Document document;
  
  public Template103(String path)
  {
    this.path = path;
    FontFactory.register(path + "myriad" + "/MYRIADPRO-BOLD.OTF", "myriadprobold");
    
   // FontFactory.register(path + "myriad" + "/MYRIADPRO-REGULAR.OTF", "myriadproreg");
    
    FontFactory.register(path + "myriad" + "/MyriadPro-Bold_Italic.ttf", "myriadproitalicbold");
    
    FontFactory.register(path + "myriad" + "/MyriadPro-Italic.ttf", "myriadproitalic");
    
    this.document = new Document(new Rectangle(0.0F, 0.0F, 612.0F, 792.0F));
  }
  
  public Document getDocument()
  {
    return this.document;
  }
  
  public void setDocument(Document document)
  {
    this.document = document;
  }
  
  public String generatePdf(List<Components> components, String[] images, String templateHeading, String templateName, String path, String imageType)
    throws DocumentException, MalformedURLException, IOException
  {
    Template103 htmlToPdf = new Template103(path);
    try
    {
      circleOrReactangle = imageType;
      PdfWriter pdfWriter = PdfWriter.getInstance(htmlToPdf.getDocument(), new FileOutputStream(path + "pdf/" + templateName + ".pdf"));
      for (int temp = 0; temp < components.size(); temp++)
      {
        Components component = (Components)components.get(temp);
        String positionIdbyString = component.getTemplateComponent() != null ? component.getTemplateComponent().getPosition().getPositionName() : component.getUserFormsComponents().getPosition().getPositionName();
        
        Long statusId = component.getTemplateComponent() != null ? component.getTemplateComponent().getStatus().getStatusId() : component.getUserFormsComponents().getStatus().getStatusId();
        
        Long positionId = Long.valueOf(Long.parseLong(positionIdbyString));
        if ((positionId.longValue() == 1L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
          this.firstComponentString = component.getComponentName();
        }
        if ((positionId.longValue() >= 2L) && (positionId.longValue() <= 4L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
          this.firstHeading.add(component.getComponentName());
        }
        if ((positionId.longValue() >= 7L) && (positionId.longValue() <= 14L)) {
          if (statusId.longValue() != 1L) {
            this.leftFirstLists.add("");
          } else {
            this.leftFirstLists.add(component.getComponentName());
          }
        }
        if ((positionId.longValue() >= 16L) && (positionId.longValue() <= 26L)) {
          if (statusId.longValue() != 1L) {
            this.leftSecondLists.add("");
          } else {
            this.leftSecondLists.add(component.getComponentName());
          }
        }
        if ((positionId.longValue() >= 27L) && (positionId.longValue() <= 29L)) {
          if (statusId.longValue() != 1L) {
            this.leftThirdLists.add("");
          } else {
            this.leftThirdLists.add(component.getComponentName());
          }
        }
        if ((positionId.longValue() >= 31L) && (positionId.longValue() <= 38L)) {
          if (statusId.longValue() != 1L) {
            this.rightFirstLists.add("");
          } else {
            this.rightFirstLists.add(component.getComponentName());
          }
        }
        if ((positionId.longValue() >= 41L) && (positionId.longValue() <= 43L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
          this.rightThirdFirstLists.add(component.getComponentName());
        }
        if ((positionId.longValue() == 44L) || (positionId.longValue() == 46L)) {
          if ((!component.getComponentName().equals("")) && (!((Components)components.get(temp + 4)).getComponentName().equals(""))) {
            this.linkedHashMap.put(component.getComponentId(), new String[] { component.getComponentName(), ((Components)components.get(temp + 4)).getComponentName() });
          } else {
            this.linkedHashMap.put(component.getComponentId(), new String[] { "", "" });
          }
        }
        if ((positionId.longValue() == 45L) || (positionId.longValue() == 47L)) {
          if ((!component.getComponentName().equals("")) && (!((Components)components.get(temp + 4)).getComponentName().equals(""))) {
            this.rightHashMap.put(component.getComponentId(), new String[] { component.getComponentName(), ((Components)components.get(temp + 4)).getComponentName() });
          } else {
            this.rightHashMap.put(component.getComponentId(), new String[] { "", "" });
          }
        }
        if ((positionId.longValue() >= 53L) && (positionId.longValue() <= 56L) && (!component.getComponentName().equals(""))) {
          this.lfrfLists.add(component.getComponentName());
        }
        if ((positionId.longValue() >= 61L) && (positionId.longValue() <= 64L) && (!component.getComponentName().equals(""))) {
          this.secondLfrfLists.add(component.getComponentName());
        }
        if ((positionId.longValue() >= 66L) && (positionId.longValue() <= 69L) && (!component.getComponentName().equals(""))) {
          this.lastComponents.add(component.getComponentName());
        }
        if ((positionId.longValue() == 52L) && (!component.getComponentName().equals(""))) {
          this.pos52 = component.getComponentName();
        }
        if ((positionId.longValue() >= 59L) && (positionId.longValue() <= 60L) && (!component.getComponentName().equals(""))) {
          this.lastTwoSubHeading.add(component.getComponentName());
        }
        if ((positionId.longValue() == 57L) && (!component.getComponentName().equals(""))) {
          this.fifthSeventhString = component.getComponentName();
        }
        if ((positionId.longValue() == 58L) && (!component.getComponentName().equals(""))) {
          this.fifthEightyhString = component.getComponentName();
        }
        if ((positionId.longValue() == 65L) && (!component.getComponentName().equals(""))) {
          this.lastHeading = component.getComponentName();
        }
        if ((positionId.longValue() >= 70L) && (positionId.longValue() <= 72L)) {
          this.imagesList.add(component.getComponentName());
        }
        if ((positionId.longValue() == 5L) || (positionId.longValue() == 15L) || (positionId.longValue() == 30L) || (positionId.longValue() == 39L)) {
          if (statusId.longValue() == 1L) {
            this.header.add(component.getComponentName());
          } else {
            this.header.add("");
          }
        }
        if ((positionId.longValue() == 6L) && ((statusId.longValue() == 1L) || (!component.getComponentName().equals("")))) {
          this.sixthComponentString = component.getComponentName();
        }
        if ((positionId.longValue() == 40L) && ((statusId.longValue() == 1L) || (!component.getComponentName().equals("")))) {
          this.fourtyComponentString = component.getComponentName();
        }
        if ((positionId.longValue() >= 73L) && (positionId.longValue() <= 75L) && (!component.getComponentName().equals(""))) {
          this.bottonContents.add(component.getComponentName());
        }
      }
      htmlToPdf.getDocument().open();
      htmlToPdf.setHeadingReactangle(pdfWriter, this.header);
      htmlToPdf.setInteriorImage(pdfWriter);
      htmlToPdf.setLeftFirstReactangle(pdfWriter, this.leftFirstLists.size());
      htmlToPdf.setTitle(pdfWriter, templateHeading);
      htmlToPdf.setLeftFirstcomponents(pdfWriter, this.leftFirstLists);
      htmlToPdf.setSixthComponent(pdfWriter, this.sixthComponentString);
      htmlToPdf.setFourtyComponent(pdfWriter, this.fourtyComponentString);
      htmlToPdf.setUniqueComponent(pdfWriter, this.firstComponentString);
      htmlToPdf.setLeftSecondcomponents(pdfWriter, this.leftSecondLists, this.leftThirdLists, (String)this.imagesList.get(1));
      
      htmlToPdf.setRightFirstscomponents(pdfWriter, this.rightFirstLists);
      htmlToPdf.setRightThirdFirstscomponents(pdfWriter, this.rightThirdFirstLists);
      
      htmlToPdf.setRightLFRFcomponents(pdfWriter, this.linkedHashMap, this.rightHashMap);
      
      htmlToPdf.setTyreFirstlfrf(pdfWriter, this.lfrfLists);
      htmlToPdf.setLeftSecondReactangle(pdfWriter, this.leftSecondLists.size(), this.leftThirdLists.size());
      
      htmlToPdf.setRightFirstReactangle(pdfWriter, this.rightFirstLists.size());
      
      htmlToPdf.setRightSecondReactangle(pdfWriter);
      htmlToPdf.setTyreFirstlfrf(pdfWriter, this.lfrfLists);
      htmlToPdf.setTyreSecondlfrf(pdfWriter, this.secondLfrfLists);
      htmlToPdf.setLastComponents(pdfWriter, this.lastComponents);
      
      htmlToPdf.setLastTwoSubHeadings(pdfWriter, this.lastTwoSubHeading);
      htmlToPdf.setFastTwoSubHeadings(pdfWriter, this.fifthSeventhString, this.fifthEightyhString);
      htmlToPdf.setLastHeadings(pdfWriter, this.lastHeading, this.pos52);
      htmlToPdf.setHeadingImage(pdfWriter, images);
      htmlToPdf.setCondtionInfo(pdfWriter, this.firstHeading);
      htmlToPdf.setImage(pdfWriter, this.imagesList);
      htmlToPdf.setFooter(pdfWriter, this.bottonContents);
      htmlToPdf.getDocument().close();
    }
    catch (Exception e)
    {
      LOGGER.info(e);
    }
    LOGGER.info("generated pdf file namme:" + templateName + ".pdf");
    return templateName + ".pdf";
  }
  
  public void setFooter(PdfWriter pdfWriter, List<String> bottonContents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    canvas.showTextAligned(0, (String)bottonContents.get(0), 305.0F, 168.0F, 0.0F);
    canvas.showTextAligned(0, (String)bottonContents.get(1), 305.0F, 55.0F, 0.0F);
    canvas.showTextAligned(0, (String)bottonContents.get(2), 450.0F, 55.0F, 0.0F);
    canvas.setFontAndSize(bf_helv, 5.0F);
    canvas.showTextAligned(0, "Form  #103", 530.0F, 43.0F, 0.0F);
    canvas.endText();
    canvas.setLineWidth(0.75F);
    canvas.moveTo(307.0F + bf_helv.getWidthPoint(WordUtils.capitalize((String)bottonContents.get(0)), 10.0F), 167.0F);
    
    canvas.lineTo(555.0F, 167.0F);
    for (int temp = 0; temp < 5; temp++)
    {
      canvas.moveTo(555.0F, 166 - (temp + 1) * 18);
      canvas.lineTo(305.0F, 166 - (temp + 1) * 18);
    }
    canvas.moveTo(307.5F + bf_helv.getWidthPoint(WordUtils.capitalize((String)bottonContents.get(1)), 10.0F), 54.0F);
    
    canvas.lineTo(447.0F, 54.0F);
    canvas.moveTo(453.5F + bf_helv.getWidthPoint(WordUtils.capitalize((String)bottonContents.get(2)), 10.0F), 54.0F);
    
    canvas.lineTo(555.0F, 54.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setHeadingReactangle(PdfWriter pdfWriter, List<String> headers)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.rectangle(36.0F, 560.0F, 253.0F, 24.0F);
    canvas.rectangle(36.0F, 298.0F, 253.0F, 24.0F);
    canvas.rectangle(302.0F, 560.0F, 253.0F, 24.0F);
    canvas.rectangle(302.0F, 387.0F, 253.0F, 24.0F);
    
    canvas.setColorFill(new BaseColor(Color.decode("#005AAB")));
    canvas.fillStroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    canvas.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
    canvas.showTextAligned(1, (String)headers.get(0), 165.0F, 567.0F, 0.0F);
    
    canvas.showTextAligned(1, (String)headers.get(1), 165.0F, 308.0F, 0.0F);
    
    canvas.showTextAligned(1, (String)headers.get(2), 415.0F, 568.0F, 0.0F);
    
    canvas.showTextAligned(1, (String)headers.get(3), 415.0F, 393.0F, 0.0F);
    
    canvas.endText();
    canvas.restoreState();
  }
  
  public void setFirstBlock(PdfWriter pdfWriter) {}
  
  public void setSecondBlock(PdfWriter pdfWriter) {}
  
  public void setThirdBlock(PdfWriter pdfWriter) {}
  
  public void setInteriorImage(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.moveTo(36.0F, 560.0F);
    canvas.lineTo(36.0F, 477.0F);
    canvas.moveTo(289.0F, 560.0F);
    canvas.lineTo(289.0F, 477.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLeftFirstReactangle(PdfWriter pdfWriter, int size)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(36.0F, 477 - 18 * size, 253.0F, 18 * size);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < size; temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(1.0F);
      canvas.moveTo(36.0F, 477 - 18 * (temp + 1));
      canvas.lineTo(289.0F, 477 - 18 * (temp + 1));
      canvas.stroke();
      canvas.restoreState();
    }
  }
  
  public void setLeftSecondReactangle(PdfWriter pdfWriter, int size, int baterySize)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    if (baterySize != 0) {
      canvas.rectangle(36.0F, 298 - 18 * (size + 4), 253.0F, 18 * (size + 4));
    } else {
      canvas.rectangle(36.0F, 298 - 18 * size, 253.0F, 18 * size);
    }
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 1; temp <= size; temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(1.0F);
      canvas.moveTo(36.0F, 299.0F - 18.0F * temp);
      canvas.lineTo(289.0F, 299.0F - 18.0F * temp);
      canvas.stroke();
      canvas.restoreState();
    }
  }
  
  public void setRightFirstReactangle(PdfWriter pdfWriter, int size)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(302.0F, 560 - 18 * size, 253.0F, 18 * size);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < size; temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(1.0F);
      canvas.moveTo(302.0F, 560 - 18 * (temp + 1));
      canvas.lineTo(555.0F, 560 - 18 * (temp + 1));
      canvas.stroke();
      canvas.restoreState();
    }
  }
  
  public void setRightSecondReactangle(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.rectangle(302.0F, 188.0F, 253.0F, 119.0F);
    canvas.setLineWidth(-0.0F);
    canvas.setColorFill(new BaseColor(Color.decode("#DADAEE")));
    canvas.fillStroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.0F);
    canvas.moveTo(302.0F, 307.0F);
    canvas.lineTo(555.0F, 307.0F);
    canvas.setColorStroke(new BaseColor(Color.decode("#FFFFFF")));
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.rectangle(302.0F, 188.0F, 253.0F, 198.0F);
    canvas.setLineWidth(1.0F);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(1.5F);
    canvas.setColorStroke(new BaseColor(Color.decode("#005AAB")));
    canvas.moveTo(430.0F, 188.0F);
    canvas.lineTo(430.0F, 307.0F);
    canvas.moveTo(497.0F, 188.0F);
    canvas.lineTo(497.0F, 307.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLeftFirstcomponents(PdfWriter pdfWriter, List<String> componentNames)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    for (int temp = 0; temp < componentNames.size(); temp++) {
      canvas.showTextAligned(0, (String)componentNames.get(temp), 100.0F, 481 - 18 * (temp + 1), 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < componentNames.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(45.0F, 480 - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(51.0F, 486.0F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(63.0F, 480 - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(68.0F, 486.0F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(81.0F, 480 - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(86.0F, 486.0F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setLeftSecondcomponents(PdfWriter pdfWriter, List<String> componentNames, List<String> bottomComponents, String image)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    for (int temp = 0; temp < componentNames.size(); temp++) {
      canvas.showTextAligned(0, (String)componentNames.get(temp), 100.0F, 303 - 18 * (temp + 1), 0.0F);
    }
    for (int temp = 0; temp < bottomComponents.size(); temp++) {
      canvas.showTextAligned(0, (String)bottomComponents.get(temp), 100.0F, 299 - 18 * (temp + componentNames.size() + 1), 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < componentNames.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(45.0F, 301 - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(51.0F, 307.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(63.0F, 301 - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(68.0F, 307.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(81.0F, 301 - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(86.0F, 307.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
    for (int temp = 0; temp < bottomComponents.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(45.0F, 297 - 18 * (temp + componentNames.size() + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(51.0F, 304.0F - 18 * (temp + componentNames.size() + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(63.0F, 297 - 18 * (temp + componentNames.size() + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(68.0F, 304.0F - 18 * (temp + componentNames.size() + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(81.0F, 297 - 18 * (temp + componentNames.size() + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(86.0F, 304.0F - 18 * (temp + componentNames.size() + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
    if (bottomComponents.size() != 0) {
      try
      {
        Image img = Image.getInstance(this.path + "..//ImageLibrary/" + image);
        
        img.scaleAbsoluteHeight(img.getHeight() * 0.55F);
        img.scaleAbsoluteWidth(img.getWidth() * 0.55F);
        img.setAlignment(1);
        img.setAbsolutePosition(197.0F + (65.0F - img.getWidth() * 0.55F) / 2.0F, 40.0F + (53.0F - img.getHeight() * 0.55F) / 2.0F);
        canvas.addImage(img);
      }
      catch (Exception e)
      {
        LOGGER.error("Insufficient Data" + e);
      }
    }
  }
  
  public void setRightFirstscomponents(PdfWriter pdfWriter, List<String> componentNames)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    for (int temp = 0; temp < componentNames.size(); temp++) {
      canvas.showTextAligned(0, (String)componentNames.get(temp), 365.0F, 564 - 18 * (temp + 1), 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < componentNames.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(310.0F, 563.0F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(315.0F, 568.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(328.0F, 563.0F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(333.0F, 568.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(346.0F, 563.0F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        canvas.circle(351.0F, 568.5F - 18 * (temp + 1), 7.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setRightThirdFirstscomponents(PdfWriter pdfWriter, List<String> componentNames)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 10.0F);
    float wordlength = 0.0F;
    for (int temp = 0; temp < componentNames.size(); temp++)
    {
      if (temp == 0) {
        canvas.showTextAligned(0, (String)componentNames.get(temp), 325.0F, 358.0F, 0.0F);
      }
      if (temp == 1) {
        canvas.showTextAligned(0, (String)componentNames.get(temp), 415.0F, 358.0F, 0.0F);
      }
      if (temp == 2) {
        canvas.showTextAligned(0, (String)componentNames.get(temp), 495.0F, 358.0F, 0.0F);
      }
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < componentNames.size(); temp++)
    {
      if (temp == 0)
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(310.0F, 355.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(315.0F, 360.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      if (temp == 1)
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(400.0F, 355.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(405.0F, 360.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      if (temp == 2)
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(480.0F, 355.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(485.0F, 360.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
      }
    }
  }
  
  public void setRightLFRFcomponents(PdfWriter pdfWriter, Map<Long, String[]> leftHashMap, Map<Long, String[]> rightHashMap)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 10.0F);
    int temp = 0;
    for (Map.Entry<Long, String[]> entSet : leftHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.showTextAligned(0, ((String[])entSet.getValue())[0], 338.0F, 340 - temp * 15, 0.0F);
        
        canvas.showTextAligned(0, ((String[])entSet.getValue())[1], 388.0F, 340 - temp * 15, 0.0F);
      }
      temp++;
    }
    int temp1 = 0;
    for (Map.Entry<Long, String[]> entSet : rightHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.showTextAligned(0, ((String[])entSet.getValue())[0], 437.0F, 340 - temp1 * 15, 0.0F);
        
        canvas.showTextAligned(0, ((String[])entSet.getValue())[1], 489.0F, 340 - temp1 * 15, 0.0F);
      }
      temp1++;
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    int newindex = 0;
    for (Map.Entry<Long, String[]> entSet : leftHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(353.0F, 355 - 15 * (newindex + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(358.0F, 358 - 15 * (newindex + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(365.0F, 355 - 15 * (newindex + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(370.0F, 358 - 15 * (newindex + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(377.0F, 355 - 15 * (newindex + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(382.0F, 358 - 15 * (newindex + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      newindex++;
    }
    int newTemp = 0;
    for (Map.Entry<Long, String[]> entSet : rightHashMap.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(452.0F, 355 - 15 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(457.0F, 358 - 15 * (newTemp + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(464.0F, 355 - 15 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(469.0F, 358 - 15 * (newTemp + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
        canvas.fillStroke();
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(476.0F, 355 - 15 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          canvas.circle(481.0F, 358 - 15 * (newTemp + 1), 5.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        canvas.fillStroke();
        canvas.restoreState();
      }
      newTemp++;
    }
  }
  
  public void setTyreFirstlfrf(PdfWriter pdfWriter, List<String> lfrfLists)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 11.0F);
    for (int temp = 0; temp < lfrfLists.size(); temp++) {
      canvas.showTextAligned(0, (String)lfrfLists.get(temp), 375.0F, 250 - 17 * temp, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int newTemp = 0; newTemp < lfrfLists.size(); newTemp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(390.0F, 267 - 17 * (newTemp + 1), 9.0F, 9.0F);
      } else {
        canvas.circle(395.0F, 271 - 17 * (newTemp + 1), 5.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(402.0F, 267 - 17 * (newTemp + 1), 9.0F, 9.0F);
      } else {
        canvas.circle(407.0F, 271 - 17 * (newTemp + 1), 5.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      canvas.fillStroke();
      canvas.restoreState();
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(414.0F, 267 - 17 * (newTemp + 1), 9.0F, 9.0F);
      } else {
        canvas.circle(419.0F, 271 - 17 * (newTemp + 1), 5.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setTyreSecondlfrf(PdfWriter pdfWriter, List<String> secondLfrfLists)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadprobold");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 8.0F);
    for (int temp = 0; temp < secondLfrfLists.size(); temp++) {
      canvas.showTextAligned(0, (String)secondLfrfLists.get(temp), 435.0F, 239 - 14 * temp, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int newTemp = 0; newTemp < secondLfrfLists.size(); newTemp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      canvas.rectangle(447.0F, 249 - 14 * (newTemp + 1), 19.0F, 10.0F);
      canvas.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setLastComponents(PdfWriter pdfWriter, List<String> lastComponents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 8.0F);
    for (int temp = 0; temp < lastComponents.size(); temp++) {
      canvas.showTextAligned(0, (String)lastComponents.get(temp), 512.0F, 243 - 14 * temp, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    for (int newTemp = 0; newTemp < lastComponents.size(); newTemp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      canvas.rectangle(500.0F, 255 - 14 * (newTemp + 1), 9.0F, 9.0F);
      canvas.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setLastTwoSubHeadings(PdfWriter pdfWriter, List<String> lastTwoComponents)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(true);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(basefont, 5.0F);
    for (int temp = 0; temp < lastTwoComponents.size(); temp++) {
      canvas.showTextAligned(1, (String)lastTwoComponents.get(temp), 455 + temp * 24, 248.0F, 0.0F);
    }
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    if (lastTwoComponents.size() == 2)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      canvas.rectangle(470.0F, 221.5F, 19.0F, 23.5F);
      canvas.rectangle(470.0F, 192.9F, 19.0F, 23.5F);
      canvas.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setFastTwoSubHeadings(PdfWriter pdfWriter, String fifthSeventhString, String fiftheightthString)
  {
    PdfPTable lastHeadingtable = new PdfPTable(1);
    lastHeadingtable.getDefaultCell().setBorder(0);
    Paragraph paragraph = new Paragraph(fiftheightthString, FontFactory.getFont("myriadproitalicbold", 7.5F));
    lastHeadingtable.addCell(paragraph);
    lastHeadingtable.setTotalWidth(55.0F);
    lastHeadingtable.writeSelectedRows(0, -1, 445.0F, 281.0F, pdfWriter.getDirectContent());
    
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont baseFont = impact.getBaseFont();
    canvas.setFontAndSize(baseFont, 9.7F);
    if ((!fifthSeventhString.equals("")) && (fifthSeventhString != null))
    {
      canvas.saveState();
      canvas.beginText();
      canvas.showTextAligned(1, fifthSeventhString, 463.0F, 285.0F, 0.0F);
      
      canvas.endText();
      canvas.stroke();
      canvas.restoreState();
    }
    if (!fiftheightthString.equals(""))
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        canvas.rectangle(435.0F, 269.0F, 9.0F, 9.0F);
      } else {
        canvas.circle(439.3F, 273.0F, 5.0F);
      }
      canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setLastHeadings(PdfWriter pdfWriter, String lastHeading, String fourtythString)
  {
    PdfPTable lastHeadingtable = new PdfPTable(1);
    lastHeadingtable.getDefaultCell().setBorder(0);
    Paragraph paragraph = new Paragraph(lastHeading, FontFactory.getFont("myriadproitalicbold", 9.7F));
    lastHeadingtable.addCell(paragraph);
    lastHeadingtable.setTotalWidth(51.0F);
    lastHeadingtable.writeSelectedRows(0, -1, 500.0F, 297.0F, pdfWriter.getDirectContent());
    PdfPTable fourtythString1table = new PdfPTable(1);
    fourtythString1table.getDefaultCell().setBorder(0);
    Paragraph fourtythStringparagraph = new Paragraph(fourtythString, FontFactory.getFont("myriadproitalicbold", 9.7F));
    fourtythStringparagraph.setLeading(9.0F, 10.0F);
    fourtythString1table.addCell(fourtythStringparagraph);
    fourtythString1table.setTotalWidth(63.0F);
    fourtythString1table.setWidthPercentage(80.0F);
    fourtythString1table.setHorizontalAlignment(1);
    fourtythString1table.writeSelectedRows(0, -1, 367.0F, 297.0F, pdfWriter.getDirectContent());
  }
  
  public void setHeadingImage(PdfWriter writer, String[] image)
    throws MalformedURLException, IOException, DocumentException
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + image[0]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.68F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.7F);
      img.setAlignment(1);
      img.setAbsolutePosition(147.0F + (289.0F - img.getWidth() * 0.654545F) / 2.0F, 684.0F + (72.0F - img.getHeight() * 0.654545F) / 2.0F);
      
      pdfCont.addImage(img);
    }
    catch (Exception e)
    {
      pdfCont.saveState();
      pdfCont.setLineWidth(0.75F);
      Font myriadpro = FontFactory.getFont("myriadproreg");
      BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
      pdfCont.beginText();
      pdfCont.setFontAndSize(imp_temp, 10.0F);
      pdfCont.setCharacterSpacing(0.0F);
      pdfCont.showTextAligned(1, "(Your logo here )", 285.0F, 726.0F, 0.0F);
      
      pdfCont.endText();
      pdfCont.rectangle(147.0F, 690.0F, 289.0F, 72.0F);
      pdfCont.stroke();
      pdfCont.restoreState();
    }
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + image[1]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.68F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.68F);
      img.setAlignment(1);
      img.setAbsolutePosition(36.0F + (72.0F - img.getWidth() * 0.67F) / 2.0F, 684.0F + (72.0F - img.getHeight() * 0.654545F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + image[2]);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.68F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.678F);
      img.setAlignment(1);
      img.setAbsolutePosition(480.0F + (72.0F - img.getWidth() * 0.67F) / 2.0F, 684.0F + (72.0F - img.getHeight() * 0.654545F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
  }
  
  public void setCondtionInfo(PdfWriter pdfWriter, List<String> components)
    throws MalformedURLException, IOException, DocumentException
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.beginText();
    Font myriadbold = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont tempMariad = myriadbold.getCalculatedBaseFont(false);
    canvas.setFontAndSize(tempMariad, 10.0F);
    for (int temp = 0; temp < components.size(); temp++)
    {
      if (temp == 0) {
        canvas.showTextAligned(0, (String)components.get(temp), 53.0F, 593.0F, 0.0F);
      }
      if (temp == 1) {
        canvas.showTextAligned(0, (String)components.get(temp), 222.0F, 593.0F, 0.0F);
      }
      if (temp == 2) {
        canvas.showTextAligned(0, (String)components.get(temp), 404.0F, 593.0F, 0.0F);
      }
    }
    canvas.endText();
    canvas.restoreState();
    for (int temp = 0; temp < components.size(); temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.5F);
      if (temp == 0)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(36.0F, 591.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(41.0F, 595.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#00A651")));
      }
      if (temp == 1)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(205.0F, 591.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(210.0F, 595.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#FFD503")));
      }
      if (temp == 2)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          canvas.rectangle(387.0F, 591.0F, 13.0F, 13.0F);
        } else {
          canvas.circle(392.0F, 595.0F, 7.0F);
        }
        canvas.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      }
      canvas.fillStroke();
      canvas.restoreState();
    }
  }
  
  public void setUniqueComponent(PdfWriter pdfWriter, String firstComponent)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    String[] splittesString = firstComponent.split("~");
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 10.0F);
    canvas.showTextAligned(0, splittesString[0], 35.0F, 635.0F, 0.0F);
    
    canvas.showTextAligned(0, splittesString[1], 35.0F, 615.0F, 0.0F);
    
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setTitle(PdfWriter pdfWriter, String title)
  {
    PdfContentByte pdfCont = pdfWriter.getDirectContent();
    Font myriadpro = FontFactory.getFont("myriadprobold");
    BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
    pdfCont.saveState();
    pdfCont.beginText();
    pdfCont.setFontAndSize(imp_temp, 18.0F);
    pdfCont.setCharacterSpacing(0.0F);
    pdfCont.showTextAligned(1, title.toUpperCase(), 302.0F, 657.0F, 0.0F);
    pdfCont.endText();
    pdfCont.restoreState();
  }
  
  public void setImage(PdfWriter writer, List<String> images)
    throws MalformedURLException, IOException, DocumentException
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(0));
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.52F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.52F);
      img.setAlignment(1);
      img.setAbsolutePosition(80.0F + (148.0F - img.getWidth() * 0.51F) / 2.0F, 484.0F + (59.0F - img.getHeight() * 0.52F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(2));
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.6F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.6F);
      img.setAlignment(1);
      img.setAbsolutePosition(305.0F + (60.0F - img.getWidth() * 0.6F) / 2.0F, 200.0F + (115.0F - img.getHeight() * 0.6F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e) {}
  }
  
  public void setSixthComponent(PdfWriter pdfWriter, String posSixth)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 7.0F);
    canvas.showTextAligned(1, posSixth, 150.0F, 551.5F, 0.0F);
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.moveTo(36.0F, 548.0F);
    canvas.lineTo(289.0F, 548.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setFourtyComponent(PdfWriter pdfWriter, String string)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(baseFont, 10.0F);
    canvas.showTextAligned(1, string, 420.0F, 374.0F, 0.0F);
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
}
