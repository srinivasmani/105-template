package com.mightyautoparts.pdfutils;

import com.itextpdf.text.DocumentException;
import com.mightyautoparts.comparator.ComponentsComparator;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.log4j.Logger;

public class PdfGenerateFactory
{
  private static final Logger LOGGER = Logger.getLogger(PdfGenerateFactory.class);
  public String file = "";
  
  public String generatePdf(Object object, String type, String folderName)
  {
    if (type.equals("template"))
    {
      Template template = (Template)object;
      List<Components> tempList = template.getComponents();
      List<Components> components = new ArrayList();
      Collections.sort(tempList, new ComponentsComparator());
      for (Components component : tempList) {
        if ((component.getTemplateComponent().getStatus().getStatusId().longValue() == 1L) && (Long.parseLong(component.getTemplateComponent().getPosition().getPositionName()) <= 15L)) {
          components.add(component);
        }
      }
      try
      {
        this.file = new Template101(folderName, template.getHeadingColor()).generatePdf(components, folderName, template.getHeaderText(), template.getTemplateName(), template.getTemplateImage());
      }
      catch (FileNotFoundException e)
      {
        LOGGER.debug(e);
      }
      catch (DocumentException e)
      {
        LOGGER.debug(e);
      }
    }
    else if (type.equals("form"))
    {
      UserForms userForms = (UserForms)object;
      List<UserFormsComponents> tempList = userForms.getUserFormComponents();
      
      List<Components> componentsList = new ArrayList();
      for (UserFormsComponents formsComponents : tempList) {
        if ((formsComponents.getStatus().getStatusId().longValue() == 1L) && (Long.parseLong(formsComponents.getPosition().getPositionName()) <= 15L)) {
          componentsList.add(formsComponents.getComponent());
        }
      }
      try
      {
        this.file = new Template101(folderName, userForms.getHeadingColor()).generatePdf(componentsList, folderName, userForms.getHeaderText(), userForms.getFormName(), userForms.getFormImage());
      }
      catch (FileNotFoundException e)
      {
        LOGGER.debug(e);
      }
      catch (DocumentException e)
      {
        LOGGER.debug(e);
      }
    }
    return this.file;
  }
  
  public String generatePdfTemplate2(Object object, String type, String folderName)
  {
    Template template = (Template)object;
    try
    {
      String[] images = { template.getTemplateImage(), template.getTemplateLeftImage(), template.getTemplateRightImage() };
      
      return new Template102(folderName).generatePdf(template.getComponents(), folderName, template.getImageType(), template.getTemplateName(), images, "template", template.getHeaderText());
    }
    catch (MalformedURLException e)
    {
      LOGGER.debug(e);
    }
    catch (DocumentException e)
    {
      LOGGER.debug(e);
    }
    catch (IOException e)
    {
      LOGGER.debug(e);
    }
    return null;
  }
  
  public String generatePdfTemplate2ByForm(Object object, String folderName)
  {
    UserForms userForms = (UserForms)object;
    try
    {
      String[] images = { userForms.getFormImage(), userForms.getFormLeftImage(), userForms.getFormRightImage() };
      
      List<UserFormsComponents> components = userForms.getUserFormComponents();
      
      Collections.sort(components, new Comparator<UserFormsComponents>()
      {
        public int compare(UserFormsComponents userComponents1, UserFormsComponents userComponents2)
        {
          Long temp1 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          Long temp2 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          return temp1.compareTo(temp2);
        }
      });
      List<Components> componentsList = new ArrayList();
      for (UserFormsComponents formsComponents : components) {
        componentsList.add(formsComponents.getComponent());
      }
      return new Template102(folderName).generatePdf(componentsList, folderName, userForms.getFormImgType(), userForms.getFormName(), images, "form", userForms.getHeaderText());
    }
    catch (MalformedURLException e)
    {
      LOGGER.debug(e);
    }
    catch (DocumentException e)
    {
      LOGGER.debug(e);
    }
    catch (IOException e)
    {
      LOGGER.debug(e);
    }
    return null;
  }
  
  public String generatePdfTemplate3(Object object, String type, String path)
  {
    Template template = (Template)object;
    String filename = "";
    try
    {
      String[] images = { template.getTemplateImage(), template.getTemplateLeftImage(), template.getTemplateRightImage() };
      
      List<Components> components = template.getComponents();
      Collections.sort(components, new ComponentsComparator());
      filename = new Template103(path).generatePdf(components, images, template.getHeaderText(), template.getTemplateName(), path, template.getImageType());
    }
    catch (Exception e)
    {
      LOGGER.debug(e);
    }
    return filename;
  }
  
  public String generatePdfTemplate3ByForm(Object object, String path)
  {
    UserForms userForms = (UserForms)object;
    try
    {
      String[] images = { userForms.getFormImage(), userForms.getFormLeftImage(), userForms.getFormRightImage() };
      
      List<UserFormsComponents> components = userForms.getUserFormComponents();
      
      Collections.sort(components, new Comparator<UserFormsComponents>()
      {
        public int compare(UserFormsComponents userComponents1, UserFormsComponents userComponents2)
        {
          Long temp1 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          Long temp2 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          return temp1.compareTo(temp2);
        }
      });
      List<Components> componentsList = new ArrayList();
      for (UserFormsComponents formsComponents : components) {
        componentsList.add(formsComponents.getComponent());
      }
      return new Template103(path).generatePdf(componentsList, images, userForms.getHeaderText(), userForms.getFormName(), path, userForms.getFormImgType());
    }
    catch (MalformedURLException e)
    {
      LOGGER.debug(e);
    }
    catch (DocumentException e)
    {
      LOGGER.debug(e);
    }
    catch (IOException e)
    {
      LOGGER.debug(e);
    }
    return null;
  }
  
  public String generatePdfTemplate4(Object object, String type, String folderName)
  {
    Template template = (Template)object;
    try
    {
      String[] images = { template.getTemplateImage(), template.getTemplateLeftImage(), template.getTemplateRightImage() };
      
      List<Components> components = template.getComponents();
      Collections.sort(components, new ComponentsComparator());
      return new Template104(folderName).generatePdf(components, images, template.getHeaderText(), folderName, template.getTemplateName(), template.getImageType());
    }
    catch (FileNotFoundException e)
    {
      LOGGER.debug(e);
    }
    catch (DocumentException e)
    {
      LOGGER.debug(e);
      e.printStackTrace();
    }
    catch (MalformedURLException e)
    {
      LOGGER.debug(e);
    }
    catch (IOException e)
    {
      LOGGER.debug(e);
    }
    return null;
  }
  
  public String generatePdfTemplate4ByForm(Object object, String folderName)
  {
    UserForms userForms = (UserForms)object;
    try
    {
      String[] images = { userForms.getFormImage(), userForms.getFormLeftImage(), userForms.getFormRightImage() };
      
      List<UserFormsComponents> components = userForms.getUserFormComponents();
      
      Collections.sort(components, new Comparator<UserFormsComponents>()
      {
        public int compare(UserFormsComponents userComponents1, UserFormsComponents userComponents2)
        {
          Long temp1 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          Long temp2 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          return temp1.compareTo(temp2);
        }
      });
      List<Components> componentsList = new ArrayList();
      for (UserFormsComponents formsComponents : components) {
        componentsList.add(formsComponents.getComponent());
      }
      return new Template104(folderName).generatePdf(componentsList, images, userForms.getHeaderText(), folderName, userForms.getFormName(), userForms.getFormImgType());
    }
    catch (MalformedURLException e)
    {
      LOGGER.debug(e);
    }
    catch (DocumentException e)
    {
      LOGGER.debug(e);
    }
    catch (IOException e)
    {
      LOGGER.debug(e);
    }
    return null;
  }
  
  public String generatePdfTemplate5(Object object, String type, String folderName, String baseURL)
  {
    Template template = (Template)object;
    try
    {
      String templateImage = template.getTemplateImage();
      List<Components> components = template.getComponents();
      Collections.sort(components, new ComponentsComparator());
      return new Template105(folderName, baseURL).generatePdf(components, templateImage, template.getHeaderText(), folderName, baseURL, template.getTemplateName(), template.getImageType(), template.getLogoText(), template.getTemplateId());
    }
    catch (FileNotFoundException e)
    {
      LOGGER.debug(e);
    }
    catch (DocumentException e)
    {
      LOGGER.debug(e);
      e.printStackTrace();
    }
    catch (MalformedURLException e)
    {
      LOGGER.debug(e);
    }
    catch (IOException e)
    {
      LOGGER.debug(e);
    }
    return null;
  }
  
  public String generatePdfTemplate5ByForm(Object object, String folderName, String baseURL)
  {
    UserForms userForms = (UserForms)object;
    try
    {
      String templateImage = userForms.getFormImage();
      List<UserFormsComponents> components = userForms.getUserFormComponents();
      Collections.sort(components, new Comparator<UserFormsComponents>()
      {
        public int compare(UserFormsComponents userComponents1, UserFormsComponents userComponents2)
        {
          Long temp1 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          Long temp2 = Long.valueOf(Long.parseLong(userComponents1.getPosition().getPositionName()));
          
          return temp1.compareTo(temp2);
        }
      });
      List<Components> componentsList = new ArrayList();
      for (UserFormsComponents formsComponents : components) {
        componentsList.add(formsComponents.getComponent());
      }
      return new Template105(folderName, baseURL).generatePdf(componentsList, templateImage, userForms.getHeaderText(), folderName, baseURL, userForms.getFormName(), userForms.getFormImgType(), userForms.getLogoText(), userForms.getUserFormId());
    }
    catch (MalformedURLException e)
    {
      LOGGER.debug(e);
    }
    catch (DocumentException e)
    {
      LOGGER.debug(e);
    }
    catch (IOException e)
    {
      LOGGER.debug(e);
    }
    return null;
  }
}
