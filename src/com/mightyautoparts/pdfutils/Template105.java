package com.mightyautoparts.pdfutils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import gui.ava.html.image.generator.HtmlImageGenerator;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document.OutputSettings;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.select.Elements;

public class Template105
{
  private static final Logger LOGGER = Logger.getLogger(Template105.class);
  static Long compId;
  BaseFont baseF;
  private com.itextpdf.text.Document document;
  
  public Template105(String path, String baseURL)
  {
    this.path = path;
    this.baseURL = baseURL;
    
    FontFactory.register(path + "myriad" + "/impact.ttf", "impactreg");
    FontFactory.register(path + "myriad" + "/6762.ttf", "mariodbolditalic");
    FontFactory.register(path + "myriad" + "/MYRIADPRO-BOLD.OTF", "myriadprobold");
    FontFactory.register(path + "myriad" + "/Helvetica LT 67 Medium Condensed-1.ttf", "helveticacondensed");
    //FontFactory.register(path + "myriad" + "/MYRIADPRO-REGULAR.OTF", "myriadproreg");
    FontFactory.register(path + "myriad" + "/MyriadPro-Bold_Italic.ttf", "myriadproitalicbold");
    FontFactory.register(path + "myriad" + "/MyriadPro-Italic.ttf", "myriadproitalic");
    try
    {
      this.baseF = BaseFont.createFont(path + "myriad" + "/Helvetica-BoldOblique.afm", "Cp1252", false);
    }
    catch (DocumentException e)
    {
      e.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    this.document = new com.itextpdf.text.Document(new Rectangle(0.0F, 0.0F, 612.0F, 792.0F));
  }
  
  static String circleOrReactangle = "";
  String path = "";
  String baseURL = "";
  Map<Long, String> CompHeadingsMap = new LinkedHashMap();
  String compName1Name = "";
  List<String> compName2to4checkedFutImm = new ArrayList();
  String compName6Note = "";
  List<String> comp7to10Light = new ArrayList();
  List<String> comp12to18Oilcondition = new ArrayList();
  List<String> compName6970dateAndInspectedBy = new ArrayList();
  Map<Long, String> comp20to24Map = new LinkedHashMap();
  String comp26Tread = "";
  String comp56Breakpad = "";
  List<String> comp27to29List = new ArrayList();
  Map<Long, String[]> comp3031or3435Map = new LinkedHashMap();
  Map<Long, String[]> comp3233or3637Map = new LinkedHashMap();
  String comp38Wear = "";
  String comp43Air = "";
  String comp50Based = "";
  List<String> comp39to42List = new ArrayList();
  List<String> comp44_45List = new ArrayList();
  List<String> comp46to49List = new ArrayList();
  List<String> comp51to54List = new ArrayList();
  Map<Long, String> comp57to59Map = new LinkedHashMap();
  List<String> comp60to63List = new ArrayList();
  List<String> comp64to65List = new ArrayList();
  String comp67See = "";
  String comp68Comments = "";
  List<String> imageList = new ArrayList();
  public static final String NBSP_IN_UTF8 = "�";
  
  public String generatePdf(List<Components> componentsList, String templateImage, String headerText, String path, String baseURL, String pdfName, String imageType, String logoText, Long id)
    throws DocumentException, MalformedURLException, IOException
  {
    compId = id;
    
    circleOrReactangle = imageType;
    
    Template105 htmlToPdf = new Template105(path, baseURL);
    
    PdfWriter pdfWriter = PdfWriter.getInstance(htmlToPdf.getDocument(), new FileOutputStream(path + "pdf/" + pdfName + ".pdf"));
    for (int i = 0; i < componentsList.size(); i++)
    {
      Components component = (Components)componentsList.get(i);
      Long statusId = component.getTemplateComponent() != null ? component.getTemplateComponent().getStatus().getStatusId() : component.getUserFormsComponents().getStatus().getStatusId();
      
      Long positionId = component.getTemplateComponent() != null ? component.getTemplateComponent().getPosition().getPositionId() : component.getUserFormsComponents().getPosition().getPositionId();
      if (((positionId.longValue() == 5L) || (positionId.longValue() == 11L) || (positionId.longValue() == 19L) || (positionId.longValue() == 25L) || (positionId.longValue() == 55L) || (positionId.longValue() == 66L)) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.CompHeadingsMap.put(positionId, component.getComponentName());
      }
      if ((positionId.longValue() == 1L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.compName1Name = component.getComponentName();
      }
      if (((positionId.longValue() == 69L) || (positionId.longValue() == 70L)) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.compName6970dateAndInspectedBy.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 2L) && (positionId.longValue() <= 4L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.compName2to4checkedFutImm.add(component.getComponentName());
      }
      if ((positionId.longValue() == 6L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.compName6Note = component.getComponentName();
      }
      if ((positionId.longValue() >= 7L) && (positionId.longValue() <= 10L)) {
        if (statusId.longValue() != 1L) {
          this.comp7to10Light.add("");
        } else {
          this.comp7to10Light.add(component.getComponentName());
        }
      }
      if ((positionId.longValue() >= 12L) && (positionId.longValue() <= 18L)) {
        if (statusId.longValue() != 1L) {
          this.comp12to18Oilcondition.add("");
        } else {
          this.comp12to18Oilcondition.add(component.getComponentName());
        }
      }
      if ((positionId.longValue() >= 20L) && (positionId.longValue() <= 24L)) {
        if (statusId.longValue() != 1L) {
          this.comp20to24Map.put(positionId, " ");
        } else {
          this.comp20to24Map.put(positionId, component.getComponentName());
        }
      }
      if ((positionId.longValue() == 26L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp26Tread = component.getComponentName();
      }
      if ((positionId.longValue() >= 27L) && (positionId.longValue() <= 29L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp27to29List.add(component.getComponentName());
      }
      if ((positionId.longValue() == 30L) || (positionId.longValue() == 34L))
      {
        long compStatusId1 = (((Components)componentsList.get(i)).getTemplateComponent() != null ? ((Components)componentsList.get(i)).getTemplateComponent().getStatus().getStatusId() : ((Components)componentsList.get(i)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        
        long nextCompstatusId2 = (((Components)componentsList.get(i + 1)).getTemplateComponent() != null ? ((Components)componentsList.get(i + 1)).getTemplateComponent().getStatus().getStatusId() : ((Components)componentsList.get(i + 1)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        if ((compStatusId1 == 1L) && (nextCompstatusId2 == 1L)) {
          this.comp3031or3435Map.put(component.getComponentId(), new String[] { component.getComponentName(), ((Components)componentsList.get(i + 1)).getComponentName() });
        } else {
          this.comp3031or3435Map.put(component.getComponentId(), new String[] { "", "" });
        }
      }
      if ((positionId.longValue() == 32L) || (positionId.longValue() == 36L))
      {
        long tempstatusId1 = (((Components)componentsList.get(i)).getTemplateComponent() != null ? ((Components)componentsList.get(i)).getTemplateComponent().getStatus().getStatusId() : ((Components)componentsList.get(i)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        
        long tempstatusId2 = (((Components)componentsList.get(i + 1)).getTemplateComponent() != null ? ((Components)componentsList.get(i + 1)).getTemplateComponent().getStatus().getStatusId() : ((Components)componentsList.get(i + 1)).getUserFormsComponents().getStatus().getStatusId()).longValue();
        if ((tempstatusId1 == 1L) && (tempstatusId2 == 1L)) {
          this.comp3233or3637Map.put(component.getComponentId(), new String[] { component.getComponentName(), ((Components)componentsList.get(i + 1)).getComponentName() });
        } else {
          this.comp3233or3637Map.put(component.getComponentId(), new String[] { "", "" });
        }
      }
      if ((positionId.longValue() == 38L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp38Wear = component.getComponentName();
      }
      if ((positionId.longValue() >= 39L) && (positionId.longValue() <= 42L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp39to42List.add(component.getComponentName());
      }
      if ((positionId.longValue() == 43L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp43Air = component.getComponentName();
      }
      if (((positionId.longValue() == 44L) || (positionId.longValue() == 45L)) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp44_45List.add(component.getComponentName());
      }
      if ((positionId.longValue() >= 46L) && (positionId.longValue() <= 49L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp46to49List.add(component.getComponentName());
      }
      if ((positionId.longValue() == 50L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp50Based = component.getComponentName();
      }
      if ((positionId.longValue() >= 51L) && (positionId.longValue() <= 54L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp51to54List.add(component.getComponentName());
      }
      if ((positionId.longValue() <= 56L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp56Breakpad = component.getComponentName();
      }
      if ((positionId.longValue() >= 57L) && (positionId.longValue() <= 59L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp57to59Map.put(positionId, component.getComponentName());
      }
      if ((positionId.longValue() >= 60L) && (positionId.longValue() <= 63L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp60to63List.add(component.getComponentName());
      }
      if (((positionId.longValue() == 64L) || (positionId.longValue() == 65L)) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp64to65List.add(component.getComponentName());
      }
      if (positionId.longValue() == 67L) {
        if (statusId.longValue() != 1L) {
          this.comp67See = "";
        } else {
          this.comp67See = component.getComponentName();
        }
      }
      if ((positionId.longValue() == 68L) && (statusId.longValue() == 1L) && (!component.getComponentName().equals(""))) {
        this.comp68Comments = component.getComponentName();
      }
      if ((positionId.longValue() >= 71L) && (positionId.longValue() <= 75L)) {
        this.imageList.add(component.getComponentName());
      }
    }
    htmlToPdf.getDocument().open();
    
    htmlToPdf.setHeaderToPdf(pdfWriter, headerText);
    
    htmlToPdf.setLogoImage(pdfWriter, logoText);
    htmlToPdf.setComp1ToPdf(pdfWriter, this.compName1Name);
    htmlToPdf.setComp2to4ToPdf(pdfWriter, this.compName2to4checkedFutImm);
    
    htmlToPdf.setAllCompHeadingsToPdf(pdfWriter, this.CompHeadingsMap);
    
    htmlToPdf.setInteriorExteriorReactangle(pdfWriter, this.comp7to10Light.size());
    htmlToPdf.setComp6ToPdf(pdfWriter, this.compName6Note);
    htmlToPdf.setComp7to10ToPdf(pdfWriter, this.comp7to10Light);
    
    htmlToPdf.setUnderhoodReactangle(pdfWriter, this.comp12to18Oilcondition.size());
    htmlToPdf.setComp12to18ToPdf(pdfWriter, this.comp12to18Oilcondition);
    
    htmlToPdf.setUnderVehicleReactangle(pdfWriter, this.comp20to24Map);
    htmlToPdf.setComp20to24ToPdf(pdfWriter, this.comp20to24Map);
    
    htmlToPdf.setTiresReactangle(pdfWriter);
    htmlToPdf.setComp26ToPdf(pdfWriter, this.comp26Tread);
    htmlToPdf.setComp27to29ToPdf(pdfWriter, this.comp27to29List);
    htmlToPdf.setComp30to37ToPdf(pdfWriter, this.comp3031or3435Map, this.comp3233or3637Map);
    htmlToPdf.setComp38_43_50ToPdf(pdfWriter, this.comp50Based, this.comp43Air, this.comp38Wear);
    htmlToPdf.setComp39to42ToPdf(pdfWriter, this.comp39to42List);
    htmlToPdf.setcomp44_45ToPdf(pdfWriter, this.comp44_45List);
    htmlToPdf.setComp46to49ToPdf(pdfWriter, this.comp46to49List);
    htmlToPdf.setComp51to54ToPdf(pdfWriter, this.comp51to54List);
    
    htmlToPdf.setBrakesReactangle(pdfWriter);
    htmlToPdf.setComp56ToPdf(pdfWriter, this.comp56Breakpad);
    htmlToPdf.setComp57to59ToPdf(pdfWriter, this.comp57to59Map);
    htmlToPdf.setComp60to63ToPdf(pdfWriter, this.comp60to63List);
    htmlToPdf.setComp64to65ToPdf(pdfWriter, this.comp64to65List);
    
    htmlToPdf.setBatteryRectWithcomp67ToPdf(pdfWriter, this.comp67See);
    htmlToPdf.setComp68ToPdf(pdfWriter, this.comp68Comments);
    htmlToPdf.setComp6970ToPdf(pdfWriter, this.compName6970dateAndInspectedBy);
    
    htmlToPdf.setImages(pdfWriter, this.imageList);
    
    htmlToPdf.getDocument().close();
    LOGGER.info("generated pdf file namme:template105.pdf");
    return pdfName + ".pdf";
  }
  
  public void setImages(PdfWriter pdfWriter, List<String> images)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    try
    {
      Image img1 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(0));
      img1.scaleAbsoluteHeight(img1.getHeight() * 0.52F);
      img1.scaleAbsoluteWidth(img1.getWidth() * 0.52F);
      img1.setAlignment(1);
      img1.setAbsolutePosition(90.0F + (130.0F - img1.getWidth() * 0.52F) / 2.0F, 390.0F + (96.0F - img1.getHeight() * 0.52F) / 2.0F);
      pcb.addImage(img1);
    }
    catch (Exception e) {}
    try
    {
      Image img2 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(1));
      img2.scaleAbsoluteHeight(img2.getHeight() * 0.48F);
      img2.scaleAbsoluteWidth(img2.getWidth() * 0.48F);
      img2.setAlignment(1);
      img2.setAbsolutePosition(320.0F + (55.0F - img2.getWidth() * 0.6F) / 2.0F, 350.0F + (55.0F - img2.getHeight() * 0.6F) / 2.0F);
      pcb.addImage(img2);
    }
    catch (Exception e) {}
    try
    {
      Image img3 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(2));
      img3.scaleAbsoluteHeight(img3.getHeight() * 0.65F);
      img3.scaleAbsoluteWidth(img3.getWidth() * 0.6F);
      img3.setAlignment(1);
      img3.setAbsolutePosition(448.0F + (64.0F - img3.getWidth() * 0.6F) / 2.0F, 340.0F + (110.0F - img3.getHeight() * 0.65F) / 2.0F);
      pcb.addImage(img3);
    }
    catch (Exception e) {}
    try
    {
      Image img6 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(3));
      img6.scaleAbsoluteHeight(img6.getHeight() * 0.52F);
      img6.scaleAbsoluteWidth(img6.getWidth() * 0.6F);
      img6.setAlignment(1);
      img6.setAbsolutePosition(375.0F + (100.0F - img6.getWidth() * 0.6F) / 2.0F, 179.0F);
      pcb.addImage(img6);
    }
    catch (Exception e) {}
    try
    {
      Image img5 = Image.getInstance(this.path + "..//ImageLibrary/" + (String)images.get(4));
      img5.scaleAbsoluteHeight(img5.getHeight() * 0.5F);
      img5.scaleAbsoluteWidth(img5.getWidth() * 0.5F);
      img5.setAlignment(1);
      img5.setAbsolutePosition(480.0F + (55.0F - img5.getWidth() * 0.5F) / 2.0F, 100.0F + (55.0F - img5.getHeight() * 0.5F) / 2.0F);
      pcb.addImage(img5);
    }
    catch (Exception e) {}
    pcb.saveState();
    pcb.setLineWidth(0.5F);
    if (circleOrReactangle.equalsIgnoreCase("square")) {
      pcb.rectangle(448.0F, 390.0F, 9.0F, 9.0F);
    } else {
      pcb.circle(453.0F, 390.0F, 5.0F);
    }
    pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
    pcb.fillStroke();
    pcb.restoreState();
  }
  
  public void setComp68ToPdf(PdfWriter pdfWriter, String comp68Comment)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.rectangle(302.0F, 42.0F, 256.0F, 50.0F);
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setLineWidth(0.75F);
    pcb.stroke();
    pcb.restoreState();
    BaseFont basefont = FontFactory.getFont("helveticacondensed").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 12.0F);
    pcb.showTextAligned(0, comp68Comment, 307.0F, 82.0F, 0.0F);
    pcb.endText();
    pcb.setLineWidth(0.5F);
    
    pcb.moveTo(302.0F, 78.0F);
    pcb.lineTo(558.0F, 78.0F);
    pcb.moveTo(302.0F, 59.5F);
    pcb.lineTo(558.0F, 59.5F);
    
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setBatteryRectWithcomp67ToPdf(PdfWriter pdfWriter, String comp67)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("helveticacondensed");
    BaseFont baseFont = impact.getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.rectangle(302.0F, 97.0F, 256.0F, 58.0F);
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 12.0F);
    pcb.showTextAligned(0, comp67, 310.0F, 130.0F, 0.0F);
    pcb.stroke();
    pcb.endText();
    pcb.restoreState();
    if (!comp67.equals(""))
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(320.0F, 110.0F, 13.0F, 13.0F);
      } else {
        pcb.circle(325.0F, 116.0F, 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(335.0F, 110.0F, 13.0F, 13.0F);
      } else {
        pcb.circle(345.0F, 116.0F, 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(350.0F, 110.0F, 13.0F, 13.0F);
      } else {
        pcb.circle(365.0F, 116.0F, 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      pcb.fillStroke();
      pcb.restoreState();
    }
  }
  
  public void setComp64to65ToPdf(PdfWriter pdfWriter, List<String> comp64to65list)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont basefont = FontFactory.getFont("myriadproitalicbold", 11.0F, 3).getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 11.0F);
    for (int i = 0; i < comp64to65list.size(); i++) {
      pcb.showTextAligned(0, (String)comp64to65list.get(i), 315 + i * 165, 185.0F, 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setComp60to63ToPdf(PdfWriter pdfWriter, List<String> comp60to63list)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont bf_helv = FontFactory.getFont("myriadprobold").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(bf_helv, 11.0F);
    int x = 470;
    int y = 262;
    for (int i = 0; i < comp60to63list.size(); i++) {
      pcb.showTextAligned(0, (String)comp60to63list.get(i), x, y - 18.5F * i, 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    for (int i = 0; i < comp60to63list.size(); i++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(x + 20, y - 2 - 18.5F * i, 13.0F, 13.0F);
      } else {
        pcb.circle(x + 25, y + 4 - 18.5F * i, 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(x + 39, y - 2 - 18.5F * i, 13.0F, 13.0F);
      } else {
        pcb.circle(x + 44, y + 4 - 18.5F * i, 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(x + 57, y - 2 - 18.5F * i, 13.0F, 13.0F);
      } else {
        pcb.circle(x + 62, y + 4 - 18.5F * i, 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      pcb.fillStroke();
      pcb.restoreState();
    }
  }
  
  public void setComp57to59ToPdf(PdfWriter pdfWriter, Map<Long, String> sixthLastComponents)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    Font font = new Font(this.baseF, 10.0F, 3);
    BaseFont baseFont = font.getBaseFont();
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 9.7F);
    int temp = 0;
    int y = 262;
    for (Map.Entry<Long, String> entSet : sixthLastComponents.entrySet())
    {
      if (((Long)entSet.getKey()).longValue() == 57L) {
        if (baseFont.getWidthPoint((String)entSet.getValue(), 10.0F) > 85.0F)
        {
          StringBuffer firstLine = new StringBuffer("");
          for (int charCnt = 0; charCnt < ((String)entSet.getValue()).length(); charCnt++) {
            if (baseFont.getWidthPoint(new String(firstLine), 10.0F) <= 85.0F) {
              firstLine.append(((String)entSet.getValue()).charAt(charCnt));
            }
          }
          if (firstLine.lastIndexOf(" ", firstLine.length() - 1) != -1)
          {
            pcb.showTextAligned(0, ((String)entSet.getValue()).substring(0, firstLine.lastIndexOf(" ", firstLine.length() - 1)), 343.0F, y - 23.0F * temp, 0.0F);
            if (((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length() - 1), ((String)entSet.getValue()).length() - 1) != null) {
              pcb.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length()), ((String)entSet.getValue()).length()), 343.0F, y - 10 - 23.0F * temp, 0.0F);
            }
          }
          else
          {
            pcb.showTextAligned(0, new String(firstLine), 343.0F, y - 23.0F * temp, 0.0F);
            
            pcb.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.length(), ((String)entSet.getValue()).length()), 343.0F, y - 10 - 23.0F * temp, 0.0F);
          }
        }
        else
        {
          pcb.showTextAligned(0, (String)entSet.getValue(), 343.0F, y - 3 - 23.0F * temp, 0.0F);
        }
      }
      if (((Long)entSet.getKey()).longValue() == 58L) {
        if (baseFont.getWidthPoint((String)entSet.getValue(), 10.0F) > 85.0F)
        {
          StringBuffer firstLine = new StringBuffer("");
          for (int charCnt = 0; charCnt < ((String)entSet.getValue()).length(); charCnt++) {
            if (baseFont.getWidthPoint(new String(firstLine), 10.0F) <= 70.0F) {
              firstLine.append(((String)entSet.getValue()).charAt(charCnt));
            }
          }
          if (firstLine.lastIndexOf(" ", firstLine.length() - 1) != -1)
          {
            pcb.showTextAligned(0, ((String)entSet.getValue()).substring(0, firstLine.lastIndexOf(" ", firstLine.length() - 1)), 343.0F, y - 23.0F * temp, 0.0F);
            if (((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length() - 1), ((String)entSet.getValue()).length() - 1) != null) {
              pcb.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length()), ((String)entSet.getValue()).length()), 343.0F, y - 10 - 23.0F * temp, 0.0F);
            }
          }
          else
          {
            pcb.showTextAligned(0, new String(firstLine), 343.0F, y - 23.0F * temp, 0.0F);
            
            pcb.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.length(), ((String)entSet.getValue()).length()), 343.0F, y - 10 - 23.0F * temp, 0.0F);
          }
        }
        else
        {
          pcb.showTextAligned(0, (String)entSet.getValue(), 343.0F, y - 3 - 23.0F * temp, 0.0F);
        }
      }
      if (((Long)entSet.getKey()).longValue() == 59L) {
        if (baseFont.getWidthPoint((String)entSet.getValue(), 10.0F) > 85.0F)
        {
          StringBuffer firstLine = new StringBuffer("");
          for (int charCnt = 0; charCnt < ((String)entSet.getValue()).length(); charCnt++) {
            if (baseFont.getWidthPoint(new String(firstLine), 10.0F) <= 115.0F) {
              firstLine.append(((String)entSet.getValue()).charAt(charCnt));
            }
          }
          if (firstLine.lastIndexOf(" ", firstLine.length() - 1) != -1)
          {
            pcb.showTextAligned(0, ((String)entSet.getValue()).substring(0, firstLine.lastIndexOf(" ", firstLine.length() - 1)), 343.0F, y - 23.0F * temp, 0.0F);
            if (((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length() - 1), ((String)entSet.getValue()).length() - 1) != null) {
              pcb.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.lastIndexOf(" ", firstLine.length()), ((String)entSet.getValue()).length()), 343.0F, y - 10 - 23.0F * temp, 0.0F);
            }
          }
          else
          {
            pcb.showTextAligned(0, new String(firstLine), 343.0F, y - 23.0F * temp, 0.0F);
            
            pcb.showTextAligned(0, ((String)entSet.getValue()).substring(firstLine.length(), ((String)entSet.getValue()).length()), 343.0F, y - 10 - 23.0F * temp, 0.0F);
          }
        }
        else
        {
          pcb.showTextAligned(0, (String)entSet.getValue(), 343.0F, y - 3 - 23.0F * temp, 0.0F);
        }
      }
      temp++;
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    int newtemp = 0;
    for (Map.Entry<Long, String> entSet : sixthLastComponents.entrySet())
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(325.0F, y - 5 - 23.0F * newtemp, 13.0F, 13.0F);
      } else {
        pcb.circle(330.0F, y + 1 - 23.0F * newtemp, 7.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 57L) {
        pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
      }
      if (((Long)entSet.getKey()).longValue() == 58L) {
        pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
      }
      if (((Long)entSet.getKey()).longValue() == 59L) {
        pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      }
      pcb.fillStroke();
      pcb.restoreState();
      newtemp++;
    }
  }
  
  public void setComp51to54ToPdf(PdfWriter pdfWriter, List<String> comp51to54list)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont basefont = FontFactory.getFont("myriadproitalicbold").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 8.0F);
    for (int i = 0; i < comp51to54list.size(); i++) {
      pcb.showTextAligned(0, (String)comp51to54list.get(i), 512.0F, 377 - 14 * i, 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    for (int i = 0; i < comp51to54list.size(); i++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      pcb.rectangle(500.0F, 390 - 14 * (i + 1), 9.0F, 9.0F);
      pcb.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
      pcb.fillStroke();
      pcb.restoreState();
    }
  }
  
  public void setComp46to49ToPdf(PdfWriter pdfWriter, List<String> comp46to49list)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont bf_helv = FontFactory.getFont("myriadprobold").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(bf_helv, 8.0F);
    for (int i = 0; i < comp46to49list.size(); i++) {
      pcb.showTextAligned(0, (String)comp46to49list.get(i), 435.0F, 360 - 14 * i, 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    for (int i = 0; i < comp46to49list.size(); i++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      pcb.rectangle(447.0F, 372 - 14 * (i + 1), 19.0F, 10.0F);
      pcb.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
      pcb.fillStroke();
      pcb.restoreState();
    }
    pcb.saveState();
    pcb.setLineWidth(0.5F);
    pcb.rectangle(470.0F, 345.0F, 19.0F, 23.5F);
    pcb.rectangle(470.0F, 315.5F, 19.0F, 23.5F);
    pcb.setColorFill(new BaseColor(Color.decode("#FFFFFF")));
    pcb.fillStroke();
    pcb.restoreState();
  }
  
  public void setcomp44_45ToPdf(PdfWriter pdfWriter, List<String> comp44_45list)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproitalicbold");
    BaseFont basefont = impact.getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 4.5F);
    for (int i = 0; i < comp44_45list.size(); i++) {
      pcb.showTextAligned(1, (String)comp44_45list.get(i), 455 + i * 24, 376.0F, 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setComp39to42ToPdf(PdfWriter pdfWriter, List<String> comp39to42list)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("myriadprobold").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 11.0F);
    for (int i = 0; i < comp39to42list.size(); i++) {
      pcb.showTextAligned(0, (String)comp39to42list.get(i), 375.0F, 384 - 17 * i, 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    for (int i = 0; i < comp39to42list.size(); i++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(390.0F, 402 - 17 * (i + 1), 9.0F, 9.0F);
      } else {
        pcb.circle(395.0F, 404 - 17 * (i + 1), 5.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(402.0F, 402 - 17 * (i + 1), 9.0F, 9.0F);
      } else {
        pcb.circle(407.0F, 404 - 17 * (i + 1), 5.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(414.0F, 402 - 17 * (i + 1), 9.0F, 9.0F);
      } else {
        pcb.circle(419.0F, 404 - 17 * (i + 1), 5.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      pcb.fillStroke();
      pcb.restoreState();
    }
  }
  
  public void setComp38_43_50ToPdf(PdfWriter pdfWriter, String comp50Based, String comp43Air, String comp38Wear)
  {
    PdfPTable comp50BasedTable = new PdfPTable(1);
    comp50BasedTable.getDefaultCell().setBorder(0);
    Paragraph paragraph = new Paragraph(comp50Based, FontFactory.getFont("myriadproitalicbold", 9.5F));
    comp50BasedTable.addCell(paragraph);
    comp50BasedTable.setTotalWidth(51.0F);
    comp50BasedTable.writeSelectedRows(0, -1, 500.0F, 420.0F, pdfWriter.getDirectContent());
    
    PdfPTable comp38WearTable = new PdfPTable(1);
    comp38WearTable.getDefaultCell().setBorder(0);
    Paragraph comp38WearParagraph = new Paragraph(comp38Wear, FontFactory.getFont("myriadproitalicbold", 9.5F));
    comp38WearParagraph.setLeading(9.0F, 10.0F);
    comp38WearTable.addCell(comp38WearParagraph);
    comp38WearTable.setTotalWidth(63.0F);
    comp38WearTable.setWidthPercentage(80.0F);
    comp38WearTable.setHorizontalAlignment(1);
    comp38WearTable.writeSelectedRows(0, -1, 369.0F, 420.0F, pdfWriter.getDirectContent());
    
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("myriadproitalicbold").getBaseFont();
    pcb.setFontAndSize(baseFont, 9.5F);
    pcb.saveState();
    pcb.beginText();
    if (!comp43Air.equals("")) {
      pcb.showTextAligned(1, comp43Air, 460.0F, 410.0F, 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setComp30to37ToPdf(PdfWriter pdfWriter, Map<Long, String[]> comp3031or3435map, Map<Long, String[]> comp3233or3637map)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("myriadprobold").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 10.0F);
    int temp = 0;
    for (Map.Entry<Long, String[]> entSet : comp3031or3435map.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        pcb.showTextAligned(0, ((String[])entSet.getValue())[0], 338.0F, 460 - temp * 18, 0.0F);
        pcb.showTextAligned(0, ((String[])entSet.getValue())[1], 388.0F, 460 - temp * 18, 0.0F);
      }
      temp++;
    }
    int temp1 = 0;
    for (Map.Entry<Long, String[]> entSet : comp3233or3637map.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        pcb.showTextAligned(0, ((String[])entSet.getValue())[0], 437.0F, 460 - temp1 * 18, 0.0F);
        pcb.showTextAligned(0, ((String[])entSet.getValue())[1], 489.0F, 460 - temp1 * 18, 0.0F);
      }
      temp1++;
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    int newTemp = 0;
    for (Map.Entry<Long, String[]> entSet : comp3031or3435map.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(353.0F, 476 - 18 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          pcb.circle(358.0F, 482 - 18 * (newTemp + 1), 5.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(365.0F, 476 - 18 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          pcb.circle(370.0F, 482 - 18 * (newTemp + 1), 5.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(377.0F, 476 - 18 * (newTemp + 1), 9.0F, 9.0F);
        } else {
          pcb.circle(382.0F, 482 - 18 * (newTemp + 1), 5.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        pcb.fillStroke();
        pcb.restoreState();
      }
      newTemp++;
    }
    int newindex = 0;
    for (Map.Entry<Long, String[]> entSet : comp3233or3637map.entrySet())
    {
      if ((!((String[])entSet.getValue())[0].equals("")) && (!((String[])entSet.getValue())[1].equals("")))
      {
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(452.0F, 476 - 18 * (newindex + 1), 9.0F, 9.0F);
        } else {
          pcb.circle(457.0F, 482 - 18 * (newindex + 1), 5.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(464.0F, 476 - 18 * (newindex + 1), 9.0F, 9.0F);
        } else {
          pcb.circle(469.0F, 482 - 18 * (newindex + 1), 5.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(476.0F, 476 - 18 * (newindex + 1), 9.0F, 9.0F);
        } else {
          pcb.circle(481.0F, 482 - 18 * (newindex + 1), 5.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        pcb.fillStroke();
        pcb.restoreState();
      }
      newindex++;
    }
  }
  
  public void setComp1ToPdf(PdfWriter pdfWriter, String componentName1)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    Font font = FontFactory.getFont("myriadproreg");
    BaseFont baseFont = font.getCalculatedBaseFont(false);
    
    String[] splittesString = componentName1.split("_");
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 10.0F);
    int j = 700;
    for (int i = 0; i < splittesString.length; i++)
    {
      pcb.showTextAligned(0, splittesString[i], 320.0F, j, 0.0F);
      pcb.setLineWidth(1.0F);
      pcb.moveTo(320.0F, j - 4);
      pcb.lineTo(555.0F, j - 4);
      j -= 20;
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setComp2to4ToPdf(PdfWriter pdfWriter, List<String> compName2to4)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    pcb.saveState();
    
    pcb.beginText();
    BaseFont baseFont = FontFactory.getFont("myriadprobold", 1.0F).getCalculatedBaseFont(false);
    pcb.setFontAndSize(baseFont, 10.0F);
    for (int temp = 0; temp < compName2to4.size(); temp++)
    {
      if (temp == 0) {
        pcb.showTextAligned(0, (String)compName2to4.get(temp), 53.0F, 532.0F, 0.0F);
      }
      if (temp == 1) {
        pcb.showTextAligned(0, (String)compName2to4.get(temp), 222.0F, 532.0F, 0.0F);
      }
      if (temp == 2) {
        pcb.showTextAligned(0, (String)compName2to4.get(temp), 404.0F, 532.0F, 0.0F);
      }
    }
    pcb.endText();
    pcb.restoreState();
    for (int temp = 0; temp < compName2to4.size(); temp++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (temp == 0)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(36.0F, 529.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(41.0F, 535.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
      }
      if (temp == 1)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(205.0F, 529.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(210.0F, 535.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
      }
      if (temp == 2)
      {
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(387.0F, 529.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(392.0F, 535.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      }
      pcb.fillStroke();
      pcb.restoreState();
    }
  }
  
  public void setComp6ToPdf(PdfWriter pdfWriter, String compName6)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("myriadproreg").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 7.0F);
    pcb.showTextAligned(1, compName6, 165.0F, 498.0F, 0.0F);
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.moveTo(36.0F, 496.0F);
    pcb.lineTo(289.0F, 496.0F);
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setComp7to10ToPdf(PdfWriter pdfWriter, List<String> comp7to10)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("helveticacondensed").getBaseFont();
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 12.0F);
    baseFont.getFullFontName();
    for (int temp = 0; temp < comp7to10.size(); temp++) {
      pcb.showTextAligned(0, (String)comp7to10.get(temp), 40.0F, 384 - 18 * (temp + 1), 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    for (int temp = 0; temp < comp7to10.size(); temp++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(235.0F, 381.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        pcb.circle(240.0F, 389 - 18 * (temp + 1), 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(253.0F, 381.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        pcb.circle(258.0F, 389 - 18 * (temp + 1), 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(271.0F, 381.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        pcb.circle(276.0F, 389 - 18 * (temp + 1), 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      pcb.fillStroke();
      pcb.restoreState();
    }
  }
  
  public void setComp26ToPdf(PdfWriter pdfWriter, String comp26)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont basefont = FontFactory.getFont("myriadprobold", 1.0F).getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 9.0F);
    pcb.showTextAligned(1, comp26, 430.0F, 495.0F, 0.0F);
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setComp27to29ToPdf(PdfWriter pdfWriter, List<String> comp27to29NameList)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont basefont = FontFactory.getFont("myriadproitalicbold").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 9.0F);
    for (int temp = 0; temp < comp27to29NameList.size(); temp++)
    {
      if (temp == 0) {
        pcb.showTextAligned(0, (String)comp27to29NameList.get(temp), 325.0F, 480.0F, 0.0F);
      }
      if (temp == 1) {
        pcb.showTextAligned(0, (String)comp27to29NameList.get(temp), 410.0F, 480.0F, 0.0F);
      }
      if (temp == 2) {
        pcb.showTextAligned(0, (String)comp27to29NameList.get(temp), 490.0F, 480.0F, 0.0F);
      }
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    for (int temp = 0; temp < comp27to29NameList.size(); temp++)
    {
      if (temp == 0)
      {
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(310.0F, 476.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(315.0F, 483.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
        pcb.fillStroke();
        pcb.restoreState();
      }
      if (temp == 1)
      {
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(395.0F, 476.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(400.0F, 483.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
        pcb.fillStroke();
        pcb.restoreState();
      }
      if (temp == 2)
      {
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(475.0F, 476.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(480.0F, 483.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        pcb.fillStroke();
        pcb.restoreState();
      }
    }
  }
  
  public void setComp56ToPdf(PdfWriter pdfWriter, String comp56)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont basefont = FontFactory.getFont("myriadprobold", 1.0F).getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 9.0F);
    pcb.showTextAligned(1, comp56, 425.0F, 275.0F, 0.0F);
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setComp6970ToPdf(PdfWriter pdfWriter, List<String> comp6970)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("helveticacondensed").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 12.0F);
    pcb.showTextAligned(0, (String)comp6970.get(0), 302.0F, 25.0F, 0.0F);
    pcb.showTextAligned(0, (String)comp6970.get(1), 450.0F, 25.0F, 0.0F);
    pcb.setFontAndSize(baseFont, 5.0F);
    pcb.showTextAligned(0, "Form  #105", 530.0F, 17.0F, 0.0F);
    pcb.endText();
    pcb.setLineWidth(1.0F);
    pcb.moveTo(305.0F + baseFont.getWidthPoint(WordUtils.capitalize((String)comp6970.get(0)), 11.0F), 25.0F);
    pcb.lineTo(447.0F, 25.0F);
    pcb.moveTo(452.0F + baseFont.getWidthPoint(WordUtils.capitalize((String)comp6970.get(1)), 11.0F), 25.0F);
    pcb.lineTo(558.0F, 25.0F);
    pcb.stroke();
    pcb.restoreState();
  }
  
  public com.itextpdf.text.Document getDocument()
  {
    return this.document;
  }
  
  public void setDocument(com.itextpdf.text.Document document)
  {
    this.document = document;
  }
  
  public void setInteriorExteriorReactangle(PdfWriter pdfWriter, int size)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.rectangle(36.0F, 380.0F, 253.0F, 125.0F);
    pcb.rectangle(36.0F, 380 - 18 * size, 253.0F, 18 * size);
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setLineWidth(0.75F);
    pcb.stroke();
    pcb.restoreState();
    for (int temp = 0; temp < size; temp++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.75F);
      pcb.moveTo(36.0F, 380 - 18 * (temp + 1));
      pcb.lineTo(289.0F, 380 - 18 * (temp + 1));
      pcb.stroke();
      pcb.restoreState();
    }
  }
  
  public void setInteriorExteriorImage(PdfWriter pdfWriter)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.moveTo(36.0F, 600.0F);
    pcb.lineTo(36.0F, 435.0F);
    pcb.moveTo(289.0F, 600.0F);
    pcb.lineTo(289.0F, 435.0F);
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setUnderhoodReactangle(PdfWriter pdfWriter, int size)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    canvas.saveState();
    canvas.setLineWidth(1.0F);
    canvas.rectangle(36.0F, 284 - 18 * size, 253.0F, 18 * size);
    canvas.stroke();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.stroke();
    canvas.restoreState();
    for (int temp = 0; temp < size; temp++)
    {
      canvas.saveState();
      canvas.setLineWidth(0.75F);
      canvas.moveTo(36.0F, 284 - 18 * (temp + 1));
      canvas.lineTo(289.0F, 284 - 18 * (temp + 1));
      canvas.stroke();
      canvas.restoreState();
    }
  }
  
  public void setComp12to18ToPdf(PdfWriter pdfWriter, List<String> comp12to18)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("helveticacondensed").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 12.0F);
    for (int temp = 0; temp < comp12to18.size(); temp++) {
      pcb.showTextAligned(0, (String)comp12to18.get(temp), 40.0F, 288 - 18 * (temp + 1), 0.0F);
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    for (int temp = 0; temp < comp12to18.size(); temp++)
    {
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(235.0F, 286.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        pcb.circle(240.0F, 292 - 18 * (temp + 1), 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(253.0F, 286.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        pcb.circle(258.0F, 292.5F - 18 * (temp + 1), 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
      pcb.fillStroke();
      pcb.restoreState();
      pcb.saveState();
      pcb.setLineWidth(0.5F);
      if (circleOrReactangle.equalsIgnoreCase("square")) {
        pcb.rectangle(271.0F, 286.5F - 18 * (temp + 1), 13.0F, 13.0F);
      } else {
        pcb.circle(276.0F, 292.5F - 18 * (temp + 1), 7.0F);
      }
      pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
      pcb.fillStroke();
      pcb.restoreState();
    }
  }
  
  public void setUnderVehicleReactangle(PdfWriter pdfWriter, Map<Long, String> comp20to24map)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    if ((comp20to24map.size() == 5) && (((Long)((Map.Entry)comp20to24map.entrySet().iterator().next()).getKey()).longValue() == 20L)) {
      pcb.rectangle(36.0F, 134.5F - 18.5F * (comp20to24map.size() + 1), 253.0F, 18.5F * (comp20to24map.size() + 1));
    } else {
      pcb.rectangle(36.0F, 134.5F - 18.5F * comp20to24map.size(), 253.0F, 18.5F * comp20to24map.size());
    }
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setLineWidth(0.75F);
    pcb.stroke();
    pcb.restoreState();
    float rectHeight = 134.5F;
    for (Map.Entry<Long, String> entSet : comp20to24map.entrySet())
    {
      pcb.saveState();
      pcb.setLineWidth(0.75F);
      if (((Long)entSet.getKey()).longValue() == 20L)
      {
        rectHeight -= 36.0F;
      }
      else
      {
        pcb.moveTo(36.0F, rectHeight);
        pcb.lineTo(289.0F, rectHeight);
        rectHeight = (float)(rectHeight - 18.5D);
      }
      pcb.stroke();
      pcb.restoreState();
    }
  }
  
  public void setComp20to24ToPdf(PdfWriter pdfWriter, Map<Long, String> comp20to24map)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont basefont = FontFactory.getFont("helveticacondensed").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(basefont, 12.0F);
    float stringHeight = 139.0F;
    for (Map.Entry<Long, String> entSet : comp20to24map.entrySet()) {
      if (((Long)entSet.getKey()).longValue() == 20L)
      {
        if (((String)entSet.getValue()).length() > 27)
        {
          pcb.showTextAligned(0, ((String)entSet.getValue()).substring(0, 27), 40.0F, stringHeight - 18.0F, 0.0F);
          
          pcb.showTextAligned(0, ((String)entSet.getValue()).substring(27, ((String)entSet.getValue()).length() - 1), 40.0F, stringHeight - 34.0F, 0.0F);
        }
        else
        {
          pcb.showTextAligned(0, ((String)entSet.getValue()).substring(0, ((String)entSet.getValue()).length() - 1), 40.0F, stringHeight - 18.0F, 0.0F);
        }
        stringHeight -= 34.0F;
      }
      else
      {
        pcb.showTextAligned(0, (String)entSet.getValue(), 40.0F, stringHeight - 18.7F, 0.0F);
        stringHeight -= 18.0F;
      }
    }
    pcb.endText();
    pcb.stroke();
    pcb.restoreState();
    float stringRectHeight = 136.5F;
    for (Map.Entry<Long, String> entSet : comp20to24map.entrySet()) {
      if (((Long)entSet.getKey()).longValue() == 20L)
      {
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(235.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(240.0F, stringRectHeight - 12.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(253.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(258.0F, stringRectHeight - 12.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(271.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(276.0F, stringRectHeight - 12.0F, 7.0F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        pcb.fillStroke();
        pcb.restoreState();
        stringRectHeight -= 36.0F;
      }
      else
      {
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(235.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(240.0F, stringRectHeight - 12.0F, 6.8F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#00A651")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(253.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(258.0F, stringRectHeight - 12.0F, 6.8F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#FFD503")));
        pcb.fillStroke();
        pcb.restoreState();
        pcb.saveState();
        pcb.setLineWidth(0.5F);
        if (circleOrReactangle.equalsIgnoreCase("square")) {
          pcb.rectangle(271.0F, stringRectHeight - 18.0F, 13.0F, 13.0F);
        } else {
          pcb.circle(276.0F, stringRectHeight - 12.0F, 6.8F);
        }
        pcb.setColorFill(new BaseColor(Color.decode("#ED1C24")));
        pcb.fillStroke();
        pcb.restoreState();
        stringRectHeight -= 18.0F;
      }
    }
  }
  
  public void setBrakesReactangle(PdfWriter pdfWriter)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.rectangle(302.0F, 178.0F, 256.0F, 107.0F);
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setTiresReactangle(PdfWriter pdfWriter)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.rectangle(302.0F, 309.0F, 256.0F, 196.0F);
    pcb.rectangle(302.0F, 309.0F, 256.0F, 115.0F);
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setLineWidth(0.75F);
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setLineWidth(1.0F);
    pcb.moveTo(302.0F, 307.0F);
    pcb.lineTo(558.0F, 307.0F);
    pcb.setColorStroke(new BaseColor(Color.decode("#FFFFFF")));
    pcb.stroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.setColorStroke(new BaseColor(Color.decode("#000000")));
    pcb.setLineWidth(1.0F);
    pcb.moveTo(430.0F, 309.0F);
    pcb.lineTo(430.0F, 423.0F);
    pcb.moveTo(497.0F, 309.0F);
    pcb.lineTo(497.0F, 423.0F);
    pcb.stroke();
    pcb.restoreState();
  }
  
  public void setAllCompHeadingsToPdf(PdfWriter pdfWriter, Map<Long, String> headingMap)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("impactreg").getBaseFont();
    pcb.saveState();
    pcb.rectangle(36.0F, 505.0F, 253.0F, 18.0F);
    pcb.rectangle(36.0F, 284.0F, 253.0F, 18.0F);
    pcb.rectangle(36.0F, 134.5F, 253.0F, 18.0F);
    
    pcb.rectangle(302.0F, 505.0F, 256.0F, 18.0F);
    pcb.rectangle(302.0F, 285.0F, 256.0F, 18.0F);
    pcb.rectangle(302.0F, 155.0F, 256.0F, 18.0F);
    pcb.setColorFill(new BaseColor(Color.decode("#DDDDDF")));
    pcb.fillStroke();
    pcb.restoreState();
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 12.0F);
    pcb.setColorFill(new BaseColor(Color.decode("#000000")));
    for (Map.Entry<Long, String> entSet : headingMap.entrySet())
    {
      if (((Long)entSet.getKey()).longValue() == 5L) {
        pcb.showTextAligned(1, (String)entSet.getValue(), 165.0F, 510.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 11L) {
        pcb.showTextAligned(1, (String)entSet.getValue(), 165.0F, 288.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 19L) {
        pcb.showTextAligned(1, (String)entSet.getValue(), 165.0F, 139.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 25L) {
        pcb.showTextAligned(1, (String)entSet.getValue(), 430.0F, 510.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 55L) {
        pcb.showTextAligned(1, (String)entSet.getValue(), 430.0F, 290.0F, 0.0F);
      }
      if (((Long)entSet.getKey()).longValue() == 66L) {
        pcb.showTextAligned(1, (String)entSet.getValue(), 430.0F, 160.0F, 0.0F);
      }
    }
    pcb.endText();
    pcb.restoreState();
  }
  
  public void setLogoImage(PdfWriter writer, String logoText)
    throws MalformedURLException, IOException, DocumentException
  {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PdfContentByte pdfCont = writer.getDirectContent();
    if ((logoText != null) && (!"".equals(logoText.trim())))
    {
      pdfCont.saveState();
      
      pdfCont.restoreState();
      org.jsoup.nodes.Document doms = Jsoup.parseBodyFragment(logoText, this.baseURL);
//      doms.outputSettings().escapeMode(entities.EscapeMode.xhtml);
      doms.outputSettings().charset("UTF-16");
      
      LOGGER.info(doms.baseUri());
      Elements els = doms.select("img");
      String imgVal = "";
      for (Element el : els)
      {
        imgVal = el.attr("src");
        el.attr("src", doms.baseUri() + imgVal);
        LOGGER.info("Element src attributes::" + el.attr("src"));
      }
      Elements all = doms.select("*");
      for (Element el : all)
      {
        if ((el.tagName().equalsIgnoreCase("p")) && (el.text().equals("�")))
        {
          LOGGER.info("NBSP Found");
          el.text(el.text().replaceAll("�", ""));
          el.html("<br>");
        }
        LOGGER.info("Element Name::" + el.tagName());
        if (("div,dl,dt,dd,h1,h2,h3,h4,h5,h6,pre,label,fieldset,input,p,blockquote".contains(el.tagName())) && 
          (el.attr("style") != null) && (!el.attr("style").contains("margin"))) {
          el.attr("style", "margin-bottom:0px;margin-top:1px;" + el.attr("style"));
        }
      }
      Elements tableElements = doms.select("table");
      for (Element table : tableElements)
      {
        String tableBorder = table.attr("border");
        String tableStyle = table.attr("style");
        
        LOGGER.info("TableBorder::" + tableBorder + " tableWidth::" + tableStyle);
        if (!tableBorder.equalsIgnoreCase("0"))
        {
          String tableWidth = fetchStyleAttribute(tableStyle, "width");
          if (tableWidth.equalsIgnoreCase("100%"))
          {
            table.attr("style", modifyStyleAttribute(tableStyle, "width", "280px"));
          }
          else
          {
            float adjTableWidth = Float.parseFloat(tableWidth.replace("px", ""));
            table.attr("style", modifyStyleAttribute(tableStyle, "width", (int)(adjTableWidth / 380.0F * 100.0F) + "%"));
          }
        }
      }
      LOGGER.info("doms::" + doms.toString());
      HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
      imageGenerator.loadHtml(doms.toString());
      
      String imgPath = this.path + "..//ImageLibrary/" + compId + "logo.png";
      Image imgLogo = Image.getInstance(imgPath);
      LOGGER.info("Image path::" + imgPath);
      
      LOGGER.info("Setting image size");
      
      imgLogo.scaleAbsolute(imgLogo.getWidth() * 0.68F, imgLogo.getHeight() * 0.68F);
      
      imgLogo.setAbsolutePosition(36.0F, 550.0F);
      pdfCont.addImage(imgLogo);
    }
    else
    {
      pdfCont.saveState();
      pdfCont.setLineWidth(0.75F);
      BaseFont imp_temp = FontFactory.getFont("myriadproreg").getCalculatedBaseFont(false);
      pdfCont.beginText();
      pdfCont.setFontAndSize(imp_temp, 10.0F);
      pdfCont.setCharacterSpacing(0.0F);
      pdfCont.showTextAligned(1, "(Your logo here )", 170.0F, 630.0F, 0.0F);
      pdfCont.endText();
      pdfCont.rectangle(36.0F, 550.0F, 253.0F, 175.0F);
      pdfCont.stroke();
      pdfCont.restoreState();
    }
  }
  
  private String fetchStyleAttribute(String style, String attribute)
  {
    String val = "";
    style = StringUtils.deleteWhitespace(style);
    String[] attributes = style.split(";");
    for (String attr : attributes) {
      if ((attr.indexOf(":") > 0) && (attr.split(":")[0].equalsIgnoreCase(attribute))) {
        val = val + attr.split(":")[1];
      }
    }
    return val;
  }
  
  private String modifyStyleAttribute(String style, String attribute, String newAttrVal)
  {
    String newStyle = "";
    style = StringUtils.deleteWhitespace(style);
    String[] attributes = style.split(";");
    for (String attr : attributes) {
      if ((attr.indexOf(":") > 0) && (attr.split(":")[0].equalsIgnoreCase(attribute))) {
        newStyle = newStyle + attr.split(":")[0] + ":" + newAttrVal + ";";
      } else {
        newStyle = newStyle + attr + ";";
      }
    }
    return newStyle;
  }
  
  public void setLogoImage(PdfWriter writer, String templateImage, String logoText)
    throws MalformedURLException, IOException, DocumentException
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    try
    {
      Image img = Image.getInstance(this.path + "..//ImageLibrary/" + templateImage);
      img.scaleAbsoluteHeight(img.getHeight() * 0.7F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.722857F);
      img.setAlignment(1);
      img.setAbsolutePosition(32.0F + (288.0F - img.getWidth() * 0.722857F) / 2.0F, 620.0F + (52.0F - img.getHeight() * 0.7F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e)
    {
      if ((logoText != null) && (!"".equals(logoText.trim())))
      {
        pdfCont.saveState();
        pdfCont.rectangle(36.0F, 550.0F, 260.0F, 175.0F);
        pdfCont.restoreState();
        
        HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
        imageGenerator.loadHtml(logoText);
        imageGenerator.saveAsImage(this.path + "..//ImageLibrary/" + compId + "logo.png");
        Image imgLogo = Image.getInstance(this.path + "..//ImageLibrary/" + compId + "logo.png");
        imgLogo.setAlignment(1);
        imgLogo.scaleAbsoluteHeight(187.5F);
        imgLogo.scaleAbsoluteWidth(259.0F);
        imgLogo.setAbsolutePosition(37.0F, 550.5F);
        
        pdfCont.addImage(imgLogo);
      }
      else
      {
        pdfCont.saveState();
        pdfCont.setLineWidth(0.75F);
        BaseFont imp_temp = FontFactory.getFont("myriadproreg").getCalculatedBaseFont(false);
        pdfCont.beginText();
        pdfCont.setFontAndSize(imp_temp, 10.0F);
        pdfCont.setCharacterSpacing(0.0F);
        pdfCont.showTextAligned(1, "(Your logo here )", 170.0F, 630.0F, 0.0F);
        pdfCont.endText();
        pdfCont.rectangle(36.0F, 550.0F, 253.0F, 175.0F);
        pdfCont.stroke();
        pdfCont.restoreState();
      }
    }
  }
  
  public void setHeaderToPdf(PdfWriter pdfWriter, String title)
  {
    PdfContentByte pcb = pdfWriter.getDirectContent();
    BaseFont baseFont = FontFactory.getFont("impactreg").getCalculatedBaseFont(false);
    pcb.saveState();
    pcb.beginText();
    pcb.setFontAndSize(baseFont, 18.0F);
    pcb.setCharacterSpacing(0.0F);
    pcb.showTextAligned(1, title.toUpperCase(), 306.0F, 755.0F, 0.0F);
    pcb.endText();
    pcb.restoreState();
  }
}
