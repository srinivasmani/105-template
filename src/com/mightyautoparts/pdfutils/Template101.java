package com.mightyautoparts.pdfutils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;

public class Template101
{
  private static final Logger LOGGER = Logger.getLogger(Template101.class);
  private float tempXHeight = 452.0F;
  private float tempYHeight = 452.0F;
  private String colorCode = "";
  private Document document;
  
  public Template101(String folderName, String colorCode)
  {
    FontFactory.register(folderName + "myriad" + "/impact.ttf", "impact");
    FontFactory.register(folderName + "myriad" + "/MYRIADPRO-BOLD.OTF", "myriadprobold");
    
    //FontFactory.register(folderName + "myriad" + "/MYRIADPRO-REGULAR.OTF", "myriadproreg");
    
    this.colorCode = colorCode;
    this.document = new Document(new Rectangle(0.0F, 0.0F, 396.0F, 612.0F));
  }
  
  public String generatePdf(List<Components> components, String folderName, String titleString, String fileName, String imageName)
    throws FileNotFoundException, DocumentException
  {
    PdfWriter pdfWriter = PdfWriter.getInstance(getDocument(), new FileOutputStream(folderName + "pdf/" + fileName + ".pdf"));
    
    getDocument().open();
    setHeader(pdfWriter, titleString);
    if (imageName != null) {
      setImage(pdfWriter, folderName + "..//ImageLibrary/" + imageName);
    } else {
      setDefaultImage(pdfWriter);
    }
    String optionType = "";
    setMileage(pdfWriter);
    
    int temp = 0;
    int size = components.size() % 2 == 0 ? components.size() / 2 : components.size() / 2 + 1;
    for (int temp1 = 0; temp1 < components.size(); temp1++)
    {
      Components component = (Components)components.get(temp1);
      Long positionId = Long.valueOf(component.getTemplateComponent() == null ? Long.parseLong(component.getUserFormsComponents().getPosition().getPositionName()) : Long.parseLong(component.getTemplateComponent().getPosition().getPositionName()));
      
      Options option = null;
      for (SubComponentsOptions componentsOptions : component.getSubComponents().getSubComponentsOptions()) {
        if (componentsOptions.getStatus().equals("true"))
        {
          optionType = componentsOptions.getOptionType();
          option = componentsOptions.getOptions();
          long tempOptionId = componentsOptions.getOptions().getOptionId().longValue();
          
          temp = (int)tempOptionId;
        }
      }
      if (positionId.longValue() <= 8L) {
        setLeftElement(pdfWriter, temp, component, option, optionType);
      } else {
        setRightElement(pdfWriter, temp, component, option, optionType);
      }
    }
    setFooter(pdfWriter);
    setborderToPage(pdfWriter);
    getDocument().close();
    LOGGER.info("generated pdf file namme:" + fileName + ".pdf");
    return fileName + ".pdf";
  }
  
  public void setborderToPage(PdfWriter pdfWriter)
  {
    PdfContentByte pdfCont = pdfWriter.getDirectContent();
    pdfCont.saveState();
    pdfCont.setLineWidth(0.75F);
    pdfCont.rectangle(0.0F, 0.0F, 396.0F, 612.0F);
    pdfCont.stroke();
    pdfCont.restoreState();
  }
  
  public void setImage(PdfWriter writer, String imageName)
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    try
    {
      Image img = Image.getInstance(imageName);
      
      img.scaleAbsoluteHeight(img.getHeight() * 0.656717F);
      img.scaleAbsoluteWidth(img.getWidth() * 0.656717F);
      img.setAlignment(1);
      img.setAbsolutePosition(75.0F + (255.0F - img.getWidth() * 0.656717F) / 2.0F, 504.0F + (55.0F - img.getHeight() * 0.656717F) / 2.0F);
      pdfCont.addImage(img);
    }
    catch (Exception e)
    {
      pdfCont.saveState();
      pdfCont.setLineWidth(0.75F);
      Font myriadpro = FontFactory.getFont("myriadpro");
      BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
      pdfCont.beginText();
      pdfCont.setFontAndSize(imp_temp, 10.0F);
      pdfCont.setCharacterSpacing(0.0F);
      pdfCont.showTextAligned(1, "(Your logo here )", 200.0F, 530.0F, 0.0F);
      
      pdfCont.endText();
      pdfCont.rectangle(71.0F, 502.0F, 255.0F, 53.0F);
      
      pdfCont.stroke();
      pdfCont.restoreState();
    }
  }
  
  void setDefaultImage(PdfWriter writer)
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    pdfCont.saveState();
    pdfCont.setLineWidth(0.75F);
    Font myriadpro = FontFactory.getFont("myriadpro");
    BaseFont imp_temp = myriadpro.getCalculatedBaseFont(false);
    pdfCont.beginText();
    pdfCont.setFontAndSize(imp_temp, 10.0F);
    pdfCont.setCharacterSpacing(0.0F);
    pdfCont.showTextAligned(1, "(Your logo here )", 200.0F, 530.0F, 0.0F);
    
    pdfCont.endText();
    pdfCont.rectangle(71.0F, 502.0F, 255.0F, 53.0F);
    
    pdfCont.stroke();
    pdfCont.restoreState();
  }
  
  public Document getDocument()
  {
    return this.document;
  }
  
  public void setDocument(Document document)
  {
    this.document = document;
  }
  
  public void setHeader(PdfWriter writer, String title)
    throws DocumentException
  {
    PdfContentByte canvas = writer.getDirectContent();
    canvas.saveState();
    
    Font Impact = FontFactory.getFont("Impact");
    BaseFont imp_temp = Impact.getCalculatedBaseFont(false);
    canvas.beginText();
    canvas.setFontAndSize(imp_temp, 24.0F);
    canvas.setCharacterSpacing(0.0F);
    canvas.showTextAligned(1, title.toUpperCase(), 202.0F, 565.0F, 0.0F);
    
    canvas.endText();
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setMileage(PdfWriter writer)
    throws DocumentException
  {
    PdfContentByte canvas = writer.getDirectContent();
    
    Font myriadpro = FontFactory.getFont("myriadpro");
    BaseFont tempMariad = myriadpro.getCalculatedBaseFont(true);
    canvas.beginText();
    canvas.setFontAndSize(tempMariad, 10.0F);
    canvas.showTextAligned(0, "Mileage", 135.0F, 482.0F, 0.0F);
    
    canvas.endText();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.moveTo(177.0F, 482.0F);
    canvas.lineTo(260.0F, 482.0F);
    canvas.stroke();
    canvas.restoreState();
  }
  
  public void setLeftElement(PdfWriter writer, int temp, Components components, Options option, String optionType)
    throws DocumentException
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    
    Font myriadbold = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont tempMariad = myriadbold.getCalculatedBaseFont(false);
    Font myriadreg = FontFactory.getFont("myriadproreg", 0.0F);
    BaseFont basechckboxFont = myriadreg.getCalculatedBaseFont(false);
    String[] optionStrings = new String[0];
    if (option != null) {
      optionStrings = option.getOptionDescription().replace('^', '~').split("~");
    }
    Long positionId = Long.valueOf(components.getTemplateComponent() == null ? Long.parseLong(components.getUserFormsComponents().getPosition().getPositionName()) : Long.parseLong(components.getTemplateComponent().getPosition().getPositionName()));
    
    pdfCont.saveState();
    pdfCont.beginText();
    pdfCont.setFontAndSize(tempMariad, 12.0F);
    Color bColor = Color.decode(this.colorCode);
    pdfCont.setColorFill(new BaseColor(bColor));
    pdfCont.showTextAligned(0, components.getComponentName().toUpperCase(), 25.0F, this.tempYHeight, 0.0F);
    
    pdfCont.setColorFill(BaseColor.BLACK);
    if ((components.getSubComponents() != null) && (!components.getSubComponents().getLabel().equals("")))
    {
      pdfCont.showTextAligned(0, components.getSubComponents().getLabel(), 25.0F, this.tempYHeight - 14.0F, 0.0F);
      
      this.tempYHeight -= 14.0F;
    }
    pdfCont.endText();
    pdfCont.restoreState();
    pdfCont.saveState();
    pdfCont.beginText();
    pdfCont.setFontAndSize(tempMariad, 12.0F);
    pdfCont.setCMYKColorFillF(1.0F, 0.47F, 0.0F, 0.33F);
    pdfCont.setColorFill(BaseColor.BLACK);
    pdfCont.setFontAndSize(basechckboxFont, 10.0F);
    float len = 43.0F;
    float stringWidth = 0.0F;
    if (option != null) {
      for (int temp1 = 0; temp1 < optionStrings.length; temp1++)
      {
        pdfCont.showTextAligned(0, optionStrings[temp1], len + stringWidth, this.tempYHeight - 13.0F, 0.0F);
        
        stringWidth += basechckboxFont.getWidthPoint(optionStrings[temp1], 10.0F) + basechckboxFont.getWidthPoint(WordUtils.capitalize("     "), 12.0F);
      }
    }
    pdfCont.endText();
    pdfCont.restoreState();
    pdfCont.stroke();
    pdfCont.saveState();
    pdfCont.restoreState();
    pdfCont.saveState();
    pdfCont.setLineWidth(0.5F);
    float rectLen = 34.0F;
    float optionWidth = 0.0F;
    if ((option != null) && (!option.getOptionDescription().equals("")))
    {
      for (int temp1 = 0; temp1 < optionStrings.length; temp1++)
      {
        if (optionType.equals("Circle"))
        {
          pdfCont.circle(rectLen + optionWidth + 3.0F, this.tempYHeight - 9.0F, 4.0F);
          
          optionWidth += basechckboxFont.getWidthPoint(optionStrings[temp1], 10.0F) + 12.0F;
        }
        if (optionType.equals("Square"))
        {
          pdfCont.rectangle(rectLen + optionWidth, this.tempYHeight - 13.0F, 7.0F, 7.0F);
          
          optionWidth += basechckboxFont.getWidthPoint(optionStrings[temp1], 10.0F) + 12.0F;
        }
      }
      this.tempYHeight -= 34.0F;
    }
    else
    {
      this.tempYHeight -= 20.0F;
    }
    pdfCont.stroke();
    pdfCont.restoreState();
  }
  
  public void setRightElement(PdfWriter writer, int temp, Components components, Options option, String optionType)
    throws DocumentException
  {
    PdfContentByte pdfCont = writer.getDirectContent();
    
    Font myriadbold = FontFactory.getFont("myriadprobold", 1.0F);
    BaseFont tempMariad = myriadbold.getCalculatedBaseFont(false);
    Font myriadreg = FontFactory.getFont("myriadproreg", 0.0F);
    BaseFont basechckboxFont = myriadreg.getCalculatedBaseFont(false);
    String[] optionStrings = new String[0];
    if (option != null) {
      optionStrings = option.getOptionDescription().replace('^', '~').split("~");
    }
    Long positionId = components.getTemplateComponent() == null ? components.getUserFormsComponents().getPosition().getPositionId() : components.getTemplateComponent().getPosition().getPositionId();
    if (positionId.longValue() < 15L)
    {
      pdfCont.saveState();
      pdfCont.beginText();
      pdfCont.setFontAndSize(tempMariad, 12.0F);
      Color bColor = Color.decode(this.colorCode);
      pdfCont.setColorFill(new BaseColor(bColor));
      pdfCont.showTextAligned(0, components.getComponentName().toUpperCase(), 210.0F, this.tempXHeight, 0.0F);
      
      pdfCont.setColorFill(BaseColor.BLACK);
      if ((components.getSubComponents() != null) && (!components.getSubComponents().getLabel().equals("")))
      {
        pdfCont.showTextAligned(0, components.getSubComponents().getLabel(), 210.0F, this.tempXHeight - 14.0F, 0.0F);
        
        this.tempXHeight -= 14.0F;
      }
      pdfCont.endText();
      pdfCont.restoreState();
      
      pdfCont.saveState();
      pdfCont.beginText();
      pdfCont.setFontAndSize(tempMariad, 12.0F);
      pdfCont.setCMYKColorFillF(1.0F, 0.47F, 0.0F, 0.33F);
      pdfCont.setColorFill(BaseColor.BLACK);
      pdfCont.setFontAndSize(basechckboxFont, 10.0F);
      float len = 226.0F;
      float StringWidth = 0.0F;
      for (int temp1 = 0; temp1 < optionStrings.length; temp1++)
      {
        pdfCont.showTextAligned(0, optionStrings[temp1], len + StringWidth, this.tempXHeight - 13.0F, 0.0F);
        
        StringWidth += basechckboxFont.getWidthPoint(optionStrings[temp1], 10.0F) + basechckboxFont.getWidthPoint(WordUtils.capitalize("     "), 12.0F);
      }
      pdfCont.endText();
      pdfCont.restoreState();
      pdfCont.stroke();
      pdfCont.saveState();
      pdfCont.restoreState();
      float rectLen = 217.0F;
      float optionWidth = 0.0F;
      if ((option != null) && (!option.getOptionDescription().equals("")))
      {
        for (int temp1 = 0; temp1 < optionStrings.length; temp1++)
        {
          pdfCont.saveState();
          pdfCont.setLineWidth(0.5F);
          if (optionType.equals("Circle"))
          {
            pdfCont.circle(rectLen + optionWidth + 3.0F, this.tempXHeight - 9.0F, 4.0F);
            
            optionWidth += basechckboxFont.getWidthPoint(optionStrings[temp1], 10.0F) + 12.0F;
          }
          if (optionType.equals("Square"))
          {
            pdfCont.rectangle(rectLen + optionWidth, this.tempXHeight - 13.0F, 7.0F, 7.0F);
            
            optionWidth += basechckboxFont.getWidthPoint(optionStrings[temp1], 10.0F) + 12.0F;
          }
          pdfCont.stroke();
          pdfCont.restoreState();
        }
        this.tempXHeight -= 34.0F;
      }
      else
      {
        this.tempXHeight -= 20.0F;
      }
    }
    else
    {
      pdfCont.saveState();
      pdfCont.beginText();
      pdfCont.setFontAndSize(tempMariad, 12.0F);
      Color bColor = Color.decode(this.colorCode);
      pdfCont.setColorFill(new BaseColor(bColor));
      pdfCont.showTextAligned(0, components.getComponentName().toUpperCase(), 210.0F, this.tempXHeight, 0.0F);
      
      pdfCont.setColorFill(BaseColor.BLACK);
      pdfCont.showTextAligned(0, components.getSubComponents().getLabel(), 210.0F, this.tempXHeight - 13.0F, 0.0F);
      
      pdfCont.endText();
      pdfCont.restoreState();
      pdfCont.saveState();
      pdfCont.beginText();
      pdfCont.setFontAndSize(tempMariad, 12.0F);
      pdfCont.setCMYKColorFillF(1.0F, 0.47F, 0.0F, 0.33F);
      pdfCont.setColorFill(BaseColor.BLACK);
      pdfCont.setFontAndSize(basechckboxFont, 10.0F);
      float height = this.tempXHeight - 15.0F;
      for (int temp1 = 0; temp1 < optionStrings.length; temp1++)
      {
        height -= 13.0F;
        pdfCont.showTextAligned(0, optionStrings[temp1], 232.0F, height, 0.0F);
      }
      pdfCont.endText();
      pdfCont.restoreState();
      pdfCont.stroke();
      pdfCont.saveState();
      pdfCont.restoreState();
      pdfCont.saveState();
      pdfCont.setLineWidth(0.5F);
      float rectheight = this.tempXHeight - 15.0F;
      if ((option != null) && (!option.getOptionDescription().equals(""))) {
        for (int temp1 = 0; temp1 < optionStrings.length; temp1++)
        {
          if (optionType.equals("Circle"))
          {
            rectheight -= 10.0F;
            pdfCont.circle(222.0F, rectheight, 4.0F);
          }
          if (optionType.equals("Square"))
          {
            rectheight -= 13.0F;
            pdfCont.rectangle(222.0F, rectheight, 7.0F, 7.0F);
          }
          if (optionType.equals("None"))
          {
            rectheight -= 13.0F;
            pdfCont.showTextAligned(0, "   ", 222.0F, rectheight, 0.0F);
          }
        }
      }
      pdfCont.stroke();
      pdfCont.restoreState();
      this.tempXHeight -= 66.0F;
    }
  }
  
  public void setFooter(PdfWriter pdfWriter)
  {
    PdfContentByte canvas = pdfWriter.getDirectContent();
    Font impact = FontFactory.getFont("myriadproreg");
    BaseFont bf_helv = impact.getCalculatedBaseFont(false);
    float footerheight = this.tempXHeight;
    if (footerheight > this.tempYHeight) {
      footerheight = this.tempYHeight;
    }
    canvas.saveState();
    canvas.beginText();
    canvas.setFontAndSize(bf_helv, 10.0F);
    canvas.showTextAligned(1, "Comments: ", 50.0F, footerheight - 10.0F, 0.0F);
    
    canvas.showTextAligned(1, "Inspected by:", 52.0F, footerheight - 60.0F, 0.0F);
    
    canvas.showTextAligned(1, "Date:", 250.0F, footerheight - 60.0F, 0.0F);
    
    canvas.setFontAndSize(bf_helv, 5.0F);
    canvas.showTextAligned(1, "Form #101", 360.0F, footerheight - 75.0F, 0.0F);
    
    canvas.endText();
    canvas.restoreState();
    canvas.saveState();
    canvas.setLineWidth(0.75F);
    canvas.moveTo(75.0F, footerheight - 10.0F);
    canvas.lineTo(375.0F, footerheight - 10.0F);
    canvas.moveTo(30.0F, footerheight - 25.0F);
    canvas.lineTo(375.0F, footerheight - 25.0F);
    canvas.moveTo(30.0F, footerheight - 40.0F);
    canvas.lineTo(375.0F, footerheight - 40.0F);
    canvas.moveTo(81.0F, footerheight - 60.0F);
    canvas.lineTo(238.0F, footerheight - 60.0F);
    canvas.moveTo(265.0F, footerheight - 60.0F);
    canvas.lineTo(375.0F, footerheight - 60.0F);
    canvas.stroke();
    canvas.restoreState();
  }
}
