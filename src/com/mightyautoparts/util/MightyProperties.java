package com.mightyautoparts.util;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;

public class MightyProperties
{
  private static String default_config = "/autoparts.properties";
  private static Properties mighty;
  private static String imageLibraryPath = null;
  private static final Logger LOGGER = Logger.getLogger(MightyProperties.class);
  
  public static String getImageLibraryPath()
  {
    Map localMap = System.getenv();
    
    String str = System.getProperty("catalina.base");
    if ((str != null) && (!str.isEmpty()))
    {
      str = str.replace("'", getProperty("file.separator"));
      str = str + getProperty("file.separator") + "webapps" + getProperty("file.separator") + "ImageLibrary";
      imageLibraryPath = str;
      setProperty("file.path", str);
    }
    else
    {
      imageLibraryPath = getProperty("file.path") + getProperty("file.separator") + "webapps" + getProperty("file.separator") + "ImageLibrary";
    }
    return imageLibraryPath;
  }
  
  public static void setImageLibraryPath(String paramString)
  {
    paramString = paramString;
  }
  
  public static String getProperty(String paramString)
  {
    return mighty.getProperty(paramString);
  }
  
  public static int getIntProperty(String paramString)
  {
    return Integer.parseInt(mighty.getProperty(paramString));
  }
  
  public static boolean getBooleanProperty(String paramString)
  {
    return getBooleanProperty(paramString, false);
  }
  
  public static boolean getBooleanProperty(String paramString, boolean paramBoolean)
  {
    String str = getProperty(paramString);
    if (str == null) {
      return paramBoolean;
    }
    return new Boolean(str).booleanValue();
  }
  
  public static Enumeration keys()
  {
    return mighty.keys();
  }
  
  public static String setProperty(String paramString1, String paramString2)
  {
    return (String)mighty.setProperty(paramString1, paramString2);
  }
  
  static
  {
    mighty = new Properties();
    try
    {
      Class localClass = Class.forName("com.mightyautoparts.util.MightyProperties");
      
      InputStream localInputStream = localClass.getResourceAsStream(default_config);
      //mighty.load(localInputStream);
      LOGGER.info("successfully loaded default properties.");
      if (LOGGER.isDebugEnabled())
      {
        String str = null;
        Enumeration localEnumeration = mighty.keys();
        while (localEnumeration.hasMoreElements())
        {
          str = (String)localEnumeration.nextElement();
          LOGGER.info(str + "=" + mighty.getProperty(str));
        }
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
}
