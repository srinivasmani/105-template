package com.mightyautoparts.util;

import com.itextpdf.text.Image;
import org.apache.log4j.Logger;

public class ImageResizer
{
  private boolean isEqual;
  private boolean smallDimensions;
  private static Logger log = Logger.getLogger(ImageResizer.class);
  
  public String resizeImage(String OriginalImage, String ConvertedImage, String xWidth, String yWidth, String type, String OutputFolderPath, String stretch)
  {
    try
    {
      log.info("\n\n\n ##### Conversion Failed.......aa");
      String command = new String();
      log.info("\n\n\n ##### Conversion Failed.......aa");
      if ("no".equals(stretch))
      {
        if ((type.equalsIgnoreCase("jpeg")) || (type.equalsIgnoreCase("jpg"))) {
          command = "convert " + OutputFolderPath + OriginalImage + " -resize " + xWidth + "x" + yWidth + ">! " + OutputFolderPath + ConvertedImage;
        } else {
          command = "convert " + OutputFolderPath + OriginalImage + " -resize " + xWidth + "x" + yWidth + ">! " + OutputFolderPath + ConvertedImage;
        }
      }
      else if ((type.equalsIgnoreCase("jpeg")) || (type.equalsIgnoreCase("jpg"))) {
        command = "convert " + OutputFolderPath + OriginalImage + " -resize " + xWidth + "x" + yWidth + "! " + OutputFolderPath + ConvertedImage;
      } else {
        command = "convert " + OutputFolderPath + OriginalImage + " -resize " + xWidth + "x" + yWidth + "! " + OutputFolderPath + ConvertedImage;
      }
      Process p = Runtime.getRuntime().exec(command);
      
      StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR");
      
      StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT");
      
      errorGobbler.start();
      outputGobbler.start();
      p.waitFor();
      return "Success";
    }
    catch (Exception e)
    {
      log.info("\n\n\n ##### Conversion Failed.......");
    }
    return "Failed";
  }
  
  public boolean isSmall(String OriginalImage, String imageHeight, String imageWidth, String OutputFolderPath)
  {
    boolean flag = false;
    try
    {
      float maxH = 0.0F;
      float maxW = 0.0F;
      if ((imageHeight != null) && (imageWidth != null))
      {
        maxH = Float.parseFloat(imageHeight);
        maxW = Float.parseFloat(imageWidth);
      }
      Image img = Image.getInstance(OutputFolderPath + OriginalImage);
      float height = img.getHeight();
      float width = img.getWidth();
      if ((maxH > height) && (maxW > width))
      {
        flag = true;
        setSmallDimensions(true);
      }
      else if ((maxH > height) || (maxW > width))
      {
        flag = true;
      }
      else if ((maxH == height) && (maxW == width))
      {
        setEqual(true);
      }
    }
    catch (Exception e)
    {
      log.info("\n\n\n ......." + e.getMessage());
    }
    return flag;
  }
  
  public static void main(String[] args)
  {
    ImageResizer resizeImage = new ImageResizer();
    resizeImage.resizeImage("C:\\test\\car3.jpg", "C:\\test\\car.jpg", "100", "100", "jpg", "C:\\test\\car100.jpg", "no");
  }
  
  public boolean isEqual()
  {
    return this.isEqual;
  }
  
  public void setEqual(boolean isEqual)
  {
    this.isEqual = isEqual;
  }
  
  public boolean isSmallDimensions()
  {
    return this.smallDimensions;
  }
  
  public void setSmallDimensions(boolean smallDimensions)
  {
    this.smallDimensions = smallDimensions;
  }
}
