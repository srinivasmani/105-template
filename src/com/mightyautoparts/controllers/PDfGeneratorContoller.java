package com.mightyautoparts.controllers;

import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.pdfutils.PdfGenerateFactory;
import com.mightyautoparts.services.masterTemplate.TemplateService;
import com.mightyautoparts.services.masterTemplate.UserFormsService;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PDfGeneratorContoller
{
  private static final Logger LOGGER = Logger.getLogger(PDfGeneratorContoller.class);
  @Resource(name="templateService")
  private TemplateService templateService;
  @Resource(name="userFormsService")
  private UserFormsService userFormsService;
  
  @RequestMapping(value={"/admin/GenerateTemplate1Pdf.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView download(HttpServletRequest request, HttpServletResponse response)
    throws IOException
  {
    Long templateId = Long.valueOf(Long.parseLong(request.getParameter("templateId")));
    ModelAndView view = new ModelAndView("mastertemplatePdf");
    ServletContext context = request.getSession().getServletContext();
    String path = context.getRealPath(System.getProperty("file.separator"));
    path = path.substring(0, path.length() - 1) + System.getProperty("file.separator");
    
    LOGGER.info("This is the method for pdf generation template1 starts");
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    
    view.addObject("pdfName", new PdfGenerateFactory().generatePdf(template, "template", path));
    
    LOGGER.info("This is the method for pdf generation template1 ends");
    return view;
  }
  
  @RequestMapping(value={"/user/GenerateUserFormPdf.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView generateUserPdf(HttpServletRequest request, HttpServletResponse response)
    throws IOException
  {
    ModelAndView view = new ModelAndView("mastertemplatePdf");
    try
    {
      ServletContext context = request.getSession().getServletContext();
      String path = context.getRealPath(System.getProperty("file.separator"));
      path = path.substring(0, path.length() - 1) + System.getProperty("file.separator");
      
      String baseURL = getBaseUrl(request);
      Long userFormId = Long.valueOf(Long.parseLong(request.getParameter("userFormId")));
      UserForms userForm = userFormId != null ? this.userFormsService.getUserFormsById(userFormId) : null;
      if (userForm.getTemplateId().longValue() == 1L) {
        view.addObject("pdfName", new PdfGenerateFactory().generatePdf(userForm, "form", path));
      }
      if (userForm.getTemplateId().longValue() == 2L) {
        view.addObject("pdfName", new PdfGenerateFactory().generatePdfTemplate2ByForm(userForm, path));
      }
      if (userForm.getTemplateId().longValue() == 3L) {
        view.addObject("pdfName", new PdfGenerateFactory().generatePdfTemplate3ByForm(userForm, path));
      }
      if (userForm.getTemplateId().longValue() == 4L) {
        view.addObject("pdfName", new PdfGenerateFactory().generatePdfTemplate4ByForm(userForm, path));
      }
      if (userForm.getTemplateId().longValue() == 5L) {
        view.addObject("pdfName", new PdfGenerateFactory().generatePdfTemplate5ByForm(userForm, path, baseURL));
      }
    }
    catch (Exception e)
    {
      view = new ModelAndView("ErrorPage");
      LOGGER.debug(e);
    }
    return view;
  }
  
  @RequestMapping(value={"/admin/GenerateTemplate2Pdf.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView generatePdfTemplate2(HttpServletRequest request, HttpServletResponse response)
    throws IOException
  {
    Long templateId = Long.valueOf(Long.parseLong(request.getParameter("templateId")));
    ModelAndView view = new ModelAndView("mastertemplatePdf");
    ServletContext context = request.getSession().getServletContext();
    String path = context.getRealPath(System.getProperty("file.separator"));
    path = path.substring(0, path.length() - 1) + System.getProperty("file.separator");
    
    LOGGER.info("This is the method for pdf generation template2 starts");
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    String fileName = new PdfGenerateFactory().generatePdfTemplate2(template, "template", path);
    
    view.addObject("pdfName", fileName);
    LOGGER.info("This is the method for pdf generation template2 ends");
    return view;
  }
  
  @RequestMapping(value={"/admin/GenerateTemplate3Pdf.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView generatePdfTemplate3(HttpServletRequest request, HttpServletResponse response)
    throws IOException
  {
    Long templateId = Long.valueOf(Long.parseLong(request.getParameter("templateId")));
    ModelAndView view = new ModelAndView("mastertemplatePdf");
    ServletContext context = request.getSession().getServletContext();
    String path = context.getRealPath(System.getProperty("file.separator"));
    path = path.substring(0, path.length() - 1) + System.getProperty("file.separator");
    
    LOGGER.info("This is the method for pdf generation template3 starts");
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    String fileName = new PdfGenerateFactory().generatePdfTemplate3(template, "template", path);
    view.addObject("pdfName", fileName);
    LOGGER.info("This is the method for pdf generation template3 starts");
    return view;
  }
  
  @RequestMapping(value={"/admin/GenerateTemplate4Pdf.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView generatePdfTemplate4(HttpServletRequest request, HttpServletResponse response)
    throws IOException
  {
    Long templateId = Long.valueOf(Long.parseLong(request.getParameter("templateId")));
    ModelAndView view = new ModelAndView("mastertemplatePdf");
    
    ServletContext context = request.getSession().getServletContext();
    String path = context.getRealPath(System.getProperty("file.separator"));
    path = path.substring(0, path.length() - 1) + System.getProperty("file.separator");
    
    LOGGER.info("This is the method for pdf generation template4 starts");
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    String fileName = new PdfGenerateFactory().generatePdfTemplate4(template, "template", path);
    
    view.addObject("pdfName", fileName);
    LOGGER.info("This is the method for pdf generation template4 starts");
    return view;
  }
  
  @RequestMapping(value={"/admin/GenerateTemplate5Pdf.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView generatePdfTemplate5(HttpServletRequest request, HttpServletResponse response)
    throws IOException
  {
    Long templateId = Long.valueOf(Long.parseLong(request.getParameter("templateId")));
    ModelAndView view = new ModelAndView("mastertemplatePdf");
    
    ServletContext context = request.getSession().getServletContext();
    String path = context.getRealPath(System.getProperty("file.separator"));
    path = path.substring(0, path.length() - 1) + System.getProperty("file.separator");
    LOGGER.info("This is the method for pdf generation template5 starts");
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    String fileName = new PdfGenerateFactory().generatePdfTemplate5(template, "template", path, getBaseUrl(request));
    view.addObject("pdfName", fileName);
    LOGGER.info("This is the method for pdf generation template5 starts");
    return view;
  }
  
  private static String getBaseUrl(HttpServletRequest request)
  {
    if ((request.getServerPort() == 80) || (request.getServerPort() == 443)) {
      return "http://" + request.getServerName();
    }
    return "http://" + request.getServerName() + ":" + request.getServerPort();
  }
}
