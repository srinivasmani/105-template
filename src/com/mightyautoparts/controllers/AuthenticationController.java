package com.mightyautoparts.controllers;

import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import com.mightyautoparts.services.masterTemplate.UserIfsService;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthenticationController
{
  @Resource(name="userIfsService")
  private UserIfsService userIfsService;
  
  @RequestMapping(value={"/Authentication/SignIn.do"}, headers={"accept=*/*"})
  public ModelAndView doLoginPost(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = null;
    paramHttpServletRequest.getSession().setAttribute("fname", "Chinmayee");
    paramHttpServletRequest.getSession().setAttribute("lname", "Seth");
    paramHttpServletRequest.getSession().setAttribute("emailaddress", "chinmayee100@neevtech.com");
    paramHttpServletRequest.getSession().setAttribute("franno", "0");
    paramHttpServletRequest.getSession().setAttribute("accessid", "1");
    paramHttpServletRequest.getSession().setAttribute("userid", "1000");
    
    String str1 = (String)paramHttpServletRequest.getSession().getAttribute("fname") + " " + (String)paramHttpServletRequest.getSession().getAttribute("lname");
    String str2 = (String)paramHttpServletRequest.getSession().getAttribute("emailaddress");
    String str3 = ((String)paramHttpServletRequest.getSession().getAttribute("franno")).split(",")[0];
    String str4 = (String)paramHttpServletRequest.getSession().getAttribute("accessid");
    String str5 = (String)paramHttpServletRequest.getSession().getAttribute("userid");
    try
    {
      if ((str2 != null) && (str3 != null) && (str4 != null) && (str5 != null))
      {
        UserIfs localUserIfs = this.userIfsService.getUserInSession(str1, Long.valueOf(Long.parseLong(str3)), Long.valueOf(Long.parseLong(str4)), Long.valueOf(Long.parseLong(str5)));
        if (localUserIfs != null)
        {
          ArrayList localArrayList = new ArrayList();
          if (localUserIfs.getUserForms() != null) {
            for (UserForms localUserForms : localUserIfs.getUserForms()) {
              if (localUserForms.getStatus().equalsIgnoreCase("active")) {
                localArrayList.add(localUserForms);
              }
            }
          } else {
            localArrayList = null;
          }
          paramHttpServletRequest.getSession().setAttribute("user", localUserIfs);
          
          localModelAndView = new ModelAndView("UserLogin");
          localModelAndView.addObject("user", localUserIfs);
          localModelAndView.addObject("userForms", localArrayList);
          localModelAndView.addObject("sessionUser", "sessionUser");
        }
        else
        {
          localModelAndView = new ModelAndView("UserLogin");
          localModelAndView.addObject("sessionUser", "noSessionUser");
        }
      }
      else
      {
        localModelAndView = new ModelAndView("UserLogin");
        localModelAndView.addObject("sessionUser", "noSessionUser");
      }
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("UserLogin");
      localModelAndView.addObject("sessionUser", "noSessionUser");
    }
    return localModelAndView;
  }
}
