package com.mightyautoparts.controllers;

import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.codec.Base64;
import com.mightyautoparts.constants.TemplateConstant;
import com.mightyautoparts.dao.Exception.AppDAOException;
import com.mightyautoparts.dto.ComponentDTO;
import com.mightyautoparts.dto.ComponentObjDTO;
import com.mightyautoparts.dto.SubComponentOptionsDTO;
import com.mightyautoparts.dto.TemplateDTO;
import com.mightyautoparts.entities.ImageFileObject;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import com.mightyautoparts.services.masterTemplate.ComponentsService;
import com.mightyautoparts.services.masterTemplate.DocumentService;
import com.mightyautoparts.services.masterTemplate.OptionsService;
import com.mightyautoparts.services.masterTemplate.SubComponentsOptionsService;
import com.mightyautoparts.services.masterTemplate.TemplateService;
import com.mightyautoparts.services.masterTemplate.UserFormsComponentsService;
import com.mightyautoparts.services.masterTemplate.UserFormsService;
import com.mightyautoparts.services.masterTemplate.UserIfsService;
import com.mightyautoparts.util.ImageResizer;
import com.mightyautoparts.util.MightyProperties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@MultipartConfig(fileSizeThreshold=2097152, maxFileSize=52428800L, maxRequestSize=83886080L)
@Controller
public class UserController
{
  private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UserController.class);
  private String errorMessage;
  @Resource(name="componentsService")
  private ComponentsService componentsService;
  @Resource(name="templateService")
  private TemplateService templateService;
  @Resource(name="userFormsService")
  private UserFormsService userFormsService;
  @Resource(name="userIfsService")
  private UserIfsService userIfsService;
  @Resource(name="documentService")
  private DocumentService documentService;
  @Resource(name="subComponentsOptions")
  private SubComponentsOptionsService subOptionService;
  @Resource(name="userFormsComponentsService")
  private UserFormsComponentsService userFormCompService;
  @Resource(name="optionsService")
  private OptionsService optionsService;
  @Resource(name="subComponentsOptions")
  private SubComponentsOptionsService subCompOptService;
  @Resource(name="userFormsComponentsService")
  private UserFormsComponentsService userFormsComponentsService;
  String pageId1;
  private Object localList;
  String selectedUserId1;
  private static final String PATH = MightyProperties.getProperty("file.path");
  private static final String FILE_SEPARATOR = MightyProperties.getProperty("file.separator");
  private static final String IMAGE_LIBRARY_PATH = "";//MightyProperties.getImageLibraryPath() + FILE_SEPARATOR;
  
  public UserController()
  {
    this.pageId1 = "1";
    this.selectedUserId1 = "2";
    
//    FontFactory.register(PATH + "Mighty_Inspection_Form/WebContent/myriad" + "MYRIADPRO-REGULAR.OTF", "myriadproreg");
  }
  
  @RequestMapping(value={"/admin/navigateTemplate104.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateTemplate104Get(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = null;
    Long localLong = TemplateConstant.TEMPLATE_104;
    try
    {
      Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
      LinkedHashMap localLinkedHashMap;
      if (localTemplate != null)
      {
        localModelAndView = new ModelAndView("Template104");
        paramHttpServletRequest.setAttribute("imgType", localTemplate.getImageType());
        localModelAndView.addObject("template", localTemplate);
        List<Components> localList = localTemplate.getComponents();
        localLinkedHashMap = new LinkedHashMap(5);
        Collections.sort(localList, new Comparator <Components>()
        {
          public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
          {
            return paramAnonymousComponents1.getComponentId().compareTo(paramAnonymousComponents2.getComponentId());
          }
        });
        for (Components localComponents : localList) {
          localLinkedHashMap.put(localComponents.getComponentId(), localComponents.getComponentName());
        }
      }
      else
      {
        localModelAndView = new ModelAndView("ErrorPage");
      }
    }
    catch (Exception localException)
    {
      log.error("error while getting tempalte 104 data " + localException);
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  @RequestMapping(value={"/admin/navigateTemplate105.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateTemplate105Get(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = null;
    Long localLong = TemplateConstant.TEMPLATE_105;
    try
    {
      Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
      Object localObject;
      if (localTemplate != null)
      {
        localModelAndView = new ModelAndView("Template105");
        paramHttpServletRequest.setAttribute("imgType", localTemplate.getImageType());
        localModelAndView.addObject("template", localTemplate);
        List<Components> localList = localTemplate.getComponents();
        
        Iterator localIterator1 = localList.iterator();
        while (localIterator1.hasNext()) {
        				localObject = (Components)localIterator1.next();
        }
        				localObject = new LinkedHashMap(5);
        Collections.sort(localList, new Comparator<Components>()
        {
          public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
          {
            return paramAnonymousComponents1.getComponentId().compareTo(paramAnonymousComponents2.getComponentId());
          }
        });
        for (Components localComponents : localList) {
          ((Map)localObject).put(localComponents.getComponentId(), localComponents.getComponentName());
        }
      }
      else
      {
        localModelAndView = new ModelAndView("ErrorPage");
      }
    }
    catch (Exception localException)
    {
      log.error("error while getting tempalte 104 data " + localException);
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/addImageToLibrary.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView addImage(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = null;
    try
    {
      localModelAndView = new ModelAndView("addImage");
    }
    catch (Exception localException)
    {
      log.error("error while getting addImage data " + localException);
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/navigateTemplate103.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateTemplate3Get(HttpServletRequest paramHttpServletRequest)
  {
    Long localLong = Long.valueOf(3L);
    ModelAndView localModelAndView = new ModelAndView("Template103");
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);   
    List<Components> localList = localTemplate.getComponents();
    Collections.sort(localList, new Comparator <Components>()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getTemplateComponent().getPosition().getPositionId().compareTo(paramAnonymousComponents2.getTemplateComponent().getPosition().getPositionId());
      }
    });
    divideComponentAdmin3(localList, localModelAndView);
    paramHttpServletRequest.setAttribute("imgType", localTemplate.getImageType());
    localModelAndView.addObject("components", localList);
    localModelAndView.addObject("template", localTemplate);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/browseImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView browseImage(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("RepositoryImages");
    String str = paramHttpServletRequest.getParameter("type");
    List localList = this.documentService.getDocumentItem();
    paramHttpServletResponse.setContentType("text/html");
    localModelAndView.addObject("type", str);
    localModelAndView.addObject("items", localList);
    return localModelAndView;
  } 
  @RequestMapping(value={"/admin/browseImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView browseImageGet(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("RepositoryImages");
    String str = paramHttpServletRequest.getParameter("type");
    List localList = this.documentService.getDocumentItem();
    paramHttpServletResponse.setContentType("text/html");
    localModelAndView.addObject("type", str);
    localModelAndView.addObject("items", localList);
    return localModelAndView;
  }
  @RequestMapping(value={"/user/browseImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView usreBrowseImage(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("RepositoryImages");
    String str = paramHttpServletRequest.getParameter("type");
    List localList = this.documentService.getDocumentItem();
    paramHttpServletResponse.setContentType("text/html");
    localModelAndView.addObject("type", str);
    localModelAndView.addObject("items", localList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/browseImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView usreBrowseImageGet(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("RepositoryImages");
    String str = paramHttpServletRequest.getParameter("type");
    List localList = this.documentService.getDocumentItem();
    paramHttpServletResponse.setContentType("text/html");
    localModelAndView.addObject("type", str);
    localModelAndView.addObject("items", localList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/showRepositoryImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView showRepositoryImage(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getParameter("imgName");
    String str2 = paramHttpServletRequest.getParameter("type");
    ModelAndView localModelAndView = new ModelAndView("image");
    
    String str3 = paramHttpServletRequest.getParameter("imageHeight");
    String str4 = paramHttpServletRequest.getParameter("imageWidth");
    
    String str5 = IMAGE_LIBRARY_PATH;
    String str6 = paramHttpServletRequest.getParameter("stretch");
    ImageResizer localImageResizer = new ImageResizer();
    String str7 = "lbi" + System.currentTimeMillis();
    String str8 = str1.split("\\.")[(str1.split("\\.").length - 1)];
    if (str8.length() <= 4) {
      str7 = str7 + "." + str8;
    }
    if (!localImageResizer.isSmall(str1, str3, str4, str5))
    {
      if (!localImageResizer.isEqual()) {
        localImageResizer.resizeImage(str1, str7, str4, str3, "", str5, "no");
      } else {
        str7 = str1;
      }
    }
    else if ((str6 != null) && (!"".equals(str6)))
    {
      if (("yes".equals(str6)) || (!localImageResizer.isSmallDimensions())) {
        localImageResizer.resizeImage(str1, str7, str4, str3, "", str5, str6);
      } else {
        str7 = str1;
      }
    }
    else
    {
      localModelAndView.addObject("smallImg", "yes");
      return localModelAndView;
    }
    localModelAndView.addObject("type", str2);
    localModelAndView.addObject("logoName", str7);
    paramHttpServletResponse.setContentType("text/html");
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/showUserRepositoryImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView showUserRepositoryImage(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getParameter("imgName");
    String str2 = paramHttpServletRequest.getParameter("type");
    ModelAndView localModelAndView = new ModelAndView("image");
    
    String str3 = paramHttpServletRequest.getParameter("imageHeight");
    String str4 = paramHttpServletRequest.getParameter("imageWidth");
    
    String str5 = IMAGE_LIBRARY_PATH;
    String str6 = paramHttpServletRequest.getParameter("stretch");
    ImageResizer localImageResizer = new ImageResizer();
    String str7 = "lbi" + System.currentTimeMillis();
    String str8 = str1.split("\\.")[(str1.split("\\.").length - 1)];
    if (str8.length() <= 4) {
      str7 = str7 + "." + str8;
    }
    if (!localImageResizer.isSmall(str1, str3, str4, str5))
    {
      if (!localImageResizer.isEqual()) {
        localImageResizer.resizeImage(str1, str7, str4, str3, "", str5, "no");
      } else {
        str7 = str1;
      }
    }
    else if ((str6 != null) && (!"".equals(str6)))
    {
      if (("yes".equals(str6)) || (!localImageResizer.isSmallDimensions())) {
        localImageResizer.resizeImage(str1, str7, str4, str3, "", str5, str6);
      } else {
        str7 = str1;
      }
    }
    else
    {
      localModelAndView.addObject("smallImg", "yes");
      return localModelAndView;
    }
    localModelAndView.addObject("type", str2);
    localModelAndView.addObject("logoName", str7);
    paramHttpServletResponse.setContentType("text/html");
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/lightBoxController.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView userLightBoxController(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    log.info("Entered into userFormEditGet()......");
    ModelAndView localModelAndView = null;
    Long localLong = (paramHttpServletRequest.getParameter("userFormId") != null) || (paramHttpServletRequest.getParameter("userFormId") != "") ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("userFormId"))) : null;
    
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    try
    {
      Object localObject1 = localLong != null ? this.userFormsService.getUserFormsById(localLong) : null;
      
      Object localObject2 = new ArrayList();
      ArrayList localArrayList1 = new ArrayList();
      ArrayList localArrayList2 = new ArrayList();
      if (localObject1 != null)
      {
        localModelAndView = new ModelAndView("UserFormEdit");
        localObject2 = ((UserForms)localObject1).getUserFormComponents();
        int i = 1;
        if (((List)localObject2).size() > 15) {
          i = 15;
        } else {
          i = ((List)localObject2).size();
        }
        for (int j = 0; j < i; j++) {
          if (j <= 7) {
            localArrayList1.add(((List)localObject2).get(j));
          } else {
            localArrayList2.add(((List)localObject2).get(j));
          }
        }
        HashMap localHashMap = getUserComponetHeadingList((List)localObject2);
        List localList = this.userIfsService.getUserForm();
        localModelAndView.addObject("userFormList", localList);
        
        localModelAndView.addObject("userForm", localObject1);
        localModelAndView.addObject("leftUserFormComponents", localArrayList1);
        
        localModelAndView.addObject("rightUserFormsComponents", localArrayList2);
        
        localModelAndView.addObject("templateId", ((UserForms)localObject1).getTemplateId());
        localModelAndView.addObject("compDrp", localHashMap);
      }
      else
      {
        localModelAndView = new ModelAndView("Welcome");
        localModelAndView.addObject("user", localUserIfs);
      }
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/lightBoxController.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView lightBoxController(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    Long localLong = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("imageDelete")));
    Boolean localBoolean = this.documentService.removeImage(localLong);
    ModelAndView localModelAndView = new ModelAndView("RepositoryImages");
    String str = paramHttpServletRequest.getParameter("type");
    List localList = this.documentService.getDocumentItem();
    paramHttpServletResponse.setContentType("text/html");
    localModelAndView.addObject("type", str);
    localModelAndView.addObject("items", localList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/saveLogoImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView saveLogoImage(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("SaveImage");
    String str1 = paramHttpServletRequest.getParameter("saveLogo");
    String str2 = "repository";
    String str3 = "admin";
    String str4 = "Your image successfully saved.";
    log.info("file path:" + IMAGE_LIBRARY_PATH);
    File localFile = new File(IMAGE_LIBRARY_PATH + str1);
    byte[] arrayOfByte = new byte[(int)localFile.length()];
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(localFile);
      localFileInputStream.read(arrayOfByte);
      localFileInputStream.close();
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    List localList = this.documentService.getDocumentItem();
    
    this.documentService.saveImage(str1, arrayOfByte, str2, str3);
    
    localModelAndView.addObject("msg", str4);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/editTemplate2.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView editTemplate2(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("image");
    String str1 = paramHttpServletRequest.getParameter("type");
    String str2 = paramHttpServletRequest.getParameter("imageHeight");
    String str3 = paramHttpServletRequest.getParameter("imageWidth");
    ImageFileObject localImageFileObject = getImageFile(paramHttpServletRequest, paramHttpServletResponse, "", str2, str3);
    if (localImageFileObject != null)
    {
      localModelAndView.addObject("type", str1);
      localModelAndView.addObject("url", localImageFileObject.getUploadImageFile().getPath());
      localModelAndView.addObject("logoImage", localImageFileObject.getUploadImageFile());
      localModelAndView.addObject("logoName", localImageFileObject.getFileName());
      paramHttpServletResponse.setContentType("text/html");
      return localModelAndView;
    }
    localModelAndView.addObject("smallImg", "yes");
    return localModelAndView;
  }
  
  public String resizeImage(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    try
    {
      String str = "";
      if ((paramString5.equalsIgnoreCase("JPEG")) || (paramString5.equalsIgnoreCase("JPG")) || (paramString5.equalsIgnoreCase("GIF")) || (paramString5.equalsIgnoreCase("PNG"))) {
        str = "convert " + paramString6 + paramString1 + " -resize " + paramString3 + "x" + paramString4 + "! -strip -interlace Plane " + paramString6 + paramString2;
      } else {
        str = "convert " + paramString6 + paramString1 + " -resize " + paramString3 + "x" + paramString4 + "! " + paramString6 + paramString2;
      }
      return "Success";
    }
    catch (Exception localException)
    {
      log.info("\n\n\n ##### Conversion Failed.......");
    }
    return "Failed";
  }  
  private String getFileName(Part paramPart)
  {
    String str1 = paramPart.getHeader("content-disposition");
    String[] arrayOfString1 = str1.split(";");
    for (String str2 : arrayOfString1) {
      if (str2.trim().startsWith("filename")) {
        return str2.substring(str2.indexOf("=") + 2, str2.length() - 1);
      }
    }
    return "";
  }
  public ImageFileObject getImageFile(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse, String paramString1, String paramString2, String paramString3)
  {
    String str1 = IMAGE_LIBRARY_PATH;
    String str2 = paramHttpServletRequest.getParameter("stretch");
    String str3 = paramHttpServletRequest.getParameter("lbimage");
    ImageResizer localImageResizer = new ImageResizer();
    Random localRandom = new Random();
    File localFile1 = null;
    String str4 = null;
    try
    {
      File localFile2 = new File(str1);
      if (!localFile2.exists()) {
        localFile2.mkdir();
      }
    }
    catch (Exception localException1)
    {
      localException1.printStackTrace();
    }
    try
    {
      for (Part localPart : paramHttpServletRequest.getParts())
      {
        str4 = getFileName(localPart);
        if ((str4 != null) && (!str4.isEmpty()))
        {
          if (str4 != null) {
            str4 = FilenameUtils.getName(str4);
          }
          String[] arrayOfString = { "#", "+", "$", "@", " ", "!", "%", "^", "&", "*" };
          for (int i = 0; i < arrayOfString.length; i++) {
            str4 = StringUtils.replace(str4, arrayOfString[i], localRandom.nextInt(10) + "");
          }
          String str5 = str4.split("\\.")[(str4.split("\\.").length - 1)];
          
          str5 = str5.toUpperCase();
          if ((!str5.equals("JPEG")) && (!str5.equals("JPG")) && (!str5.equals("GIF")) && (!str5.equals("PNG")))
          {
            this.errorMessage = "imgFormatError";
            return null;
          }
          str4 = str4.substring(0, 5);
          str4 = str4 + localRandom.nextInt(100000) + "." + str5;
          paramHttpServletRequest.setAttribute("imageName", str4);
          
          localPart.write(str1 + str4);
          localFile1 = new File(str1, str4);
        }
      }
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    catch (Exception localException2)
    {
      localException2.printStackTrace();
    }
    System.out.println(str4);
    return new ImageFileObject(str4, localFile1);
  }  
  @RequestMapping(value={"/admin/navigateTemplateView.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateTemplateViewGet(HttpServletRequest paramHttpServletRequest)
  {
    Long localLong = Long.valueOf(1L);
    ModelAndView localModelAndView;
    if (paramHttpServletRequest.getSession().getAttribute("user") != null)
    {
      localModelAndView = new ModelAndView("TemplateView");
      Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
      if (localTemplate != null)
      {
        List localList1 = localTemplate.getComponents();
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        int i = 1;
        Collections.sort(localList1, new Comparator<Components>()
        {
          public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
          {
            return paramAnonymousComponents1.getTemplateComponent().getPosition().getPositionId().compareTo(paramAnonymousComponents2.getTemplateComponent().getPosition().getPositionId());
          }
        });
        if (localList1.size() > 15) {
          i = 15;
        } else {
          i = localList1.size();
        }
        for (int j = 0; j < i; j++) 
        {
          if (j <= 7) {
            localArrayList1.add(localList1.get(j));
          } else {
            localArrayList2.add(localList1.get(j));
          }
        }
        List<Components> localList2 = localTemplate.getComponents();
        LinkedHashMap localLinkedHashMap = new LinkedHashMap();
        Collections.sort(localList2, new Comparator<Components>()
        {
          public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
          {
            return paramAnonymousComponents1.getComponentName().compareTo(paramAnonymousComponents2.getComponentName());
          }
        });
        for (Components localComponents : localList2) {
          localLinkedHashMap.put(localComponents.getComponentId(), localComponents.getComponentName());
        }
        localModelAndView.addObject("components", localList1);
        localModelAndView.addObject("template", localTemplate);
        localModelAndView.addObject("leftComponents", localArrayList1);
        localModelAndView.addObject("rightComponents", localArrayList2);
      }
      else
      {
        //localModelAndView = new ModelAndView("ErrorPage");
      }
    }
    else
    {
      localModelAndView = new ModelAndView("SessionExpired");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/editTemplate3.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView editTemplate3Get(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    Long localLong = Long.valueOf(3L);
    ModelAndView localModelAndView = new ModelAndView("EditTemplate3");
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
    List localList = localTemplate.getComponents();
    Collections.sort(localList, new Comparator<Components>()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getTemplateComponent().getPosition().getPositionId().compareTo(paramAnonymousComponents2.getTemplateComponent().getPosition().getPositionId());
      }
    });
    divideComponentAdmin3(localList, localModelAndView);
    paramHttpServletRequest.setAttribute("imgType", localTemplate.getImageType());
    localModelAndView.addObject("components", localList);
    localModelAndView.addObject("template", localTemplate);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/editTemplate2.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView editTemplate2Get(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    Long localLong = Long.valueOf(1L);
    ModelAndView localModelAndView = new ModelAndView("EditTemplate2");
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
    
    List localList = localTemplate.getComponents();
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    int i = 1;
    Collections.sort(localList, new Comparator<Components>()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getTemplateComponent().getPosition().getPositionId().compareTo(paramAnonymousComponents2.getTemplateComponent().getPosition().getPositionId());
      }
    });
    if (localList.size() > 15) {
      i = 15;
    } else {
      i = localList.size();
    }
    for (int j = 0; j < i; j++) {
      if (j <= 7) {
        localArrayList1.add(localList.get(j));
      } else {
        localArrayList2.add(localList.get(j));
      }
    }
    Map localMap = getComponetHeadingList(localList);
    localModelAndView.addObject("components", localList);
    localModelAndView.addObject("componentsSize", Integer.valueOf(localList.size()));
    localModelAndView.addObject("template", localTemplate);
    localModelAndView.addObject("compDrp", localMap);
    localModelAndView.addObject("leftComponents", localArrayList1);
    localModelAndView.addObject("rightComponents", localArrayList2);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/navigateUserDefinedTemplate.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateUserDefinedTemplateGet(HttpServletRequest paramHttpServletRequest)
  {
    if (paramHttpServletRequest.getParameter("d-4037936-p") != null) {
      this.pageId1 = paramHttpServletRequest.getParameter("d-4037936-p");
    }
    ModelAndView localModelAndView = new ModelAndView("UserDefinedTemplate");
    List localList1 = this.userIfsService.getUsers();
    List localList2 = this.userIfsService.getUserOrderByUserName(Integer.parseInt(this.pageId1));
    Collections.sort(localList2, new Comparator<UserForms>()
    {
      public int compare(UserForms paramAnonymousUserForms1, UserForms paramAnonymousUserForms2)
      {
        return paramAnonymousUserForms1.getCreatedTime().compareTo(paramAnonymousUserForms2.getCreatedTime());
      }
    });
    localModelAndView.addObject("users", localList1);
    localModelAndView.addObject("pageId1", this.pageId1);
    localModelAndView.addObject("userId", this.userIfsService.getIdOrderByUserName());
    localModelAndView.addObject("userForms", localList2);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/navigateUserCreatedTemplate.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=text/html"})
  public ModelAndView navigateUserCreatedTemplate(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    if (paramHttpServletRequest.getParameter("d-4037936-p") != null) {
      this.pageId1 = paramHttpServletRequest.getParameter("d-4037936-p");
    }
    if (paramHttpServletRequest.getParameter("id") != null) {
      this.selectedUserId1 = paramHttpServletRequest.getParameter("id");
    }
    ModelAndView localModelAndView = new ModelAndView("UserFormsList");
    long l = (paramHttpServletRequest.getParameter("id") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("id"))) : null).longValue();
    paramHttpServletRequest.getSession().setAttribute("userId", Long.valueOf(l));
    List localList1 = this.userIfsService.getUserForm(Long.valueOf(l));
    List localList2 = this.userIfsService.getUsers();
    localModelAndView.addObject("users", localList2);
    localModelAndView.addObject("pageId1", this.pageId1);
    localModelAndView.addObject("userId", this.selectedUserId1);
    localModelAndView.addObject("userForms", localList1);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/navigateUserTemplate.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateUserTemplateGet(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = new ModelAndView("UserTemplate");
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/navigateTemplateView.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public String navigateUserTemplateViewGet(HttpServletRequest paramHttpServletRequest)
  {
    log.info("Entered into .... navigateUserTemplateViewGet");
    Long localLong = Long.valueOf(paramHttpServletRequest.getParameter("templateId") != null ? Long.parseLong(paramHttpServletRequest.getParameter("templateId")) : 1L);
    
    log.info("Inside navigateUserTemplateViewGet .. TemplateId :" + localLong);
    
    UserForms localUserForms = null;
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    localUserForms = this.userFormsService.getUserDeFaultForm(localUserIfs.getUserId(), localTemplate);
    
    return "redirect:/user/userFormView.do?formid=" + localUserForms.getUserFormId();
  }
  
  @RequestMapping(value={"/user/userFormEdit.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView userFormEdit(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getParameter("type");
    String str2 = paramHttpServletRequest.getParameter("imageHeight");
    String str3 = paramHttpServletRequest.getParameter("imageWidth");
    ImageFileObject localImageFileObject = getImageFile(paramHttpServletRequest, paramHttpServletResponse, "", str2, str3);
    if (localImageFileObject != null)
    {
      ModelAndView localModelAndView = new ModelAndView("image");
      localModelAndView.addObject("type", str1);
      localModelAndView.addObject("url", localImageFileObject.getUploadImageFile().getPath());
      localModelAndView.addObject("logoImage", localImageFileObject.getUploadImageFile());
      localModelAndView.addObject("logoName", localImageFileObject.getFileName());
      paramHttpServletResponse.setContentType("text/html");
      return localModelAndView;
    }
    return null;
  }  
  @RequestMapping(value={"/user/userFormEdit.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView userFormEditGet(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    log.info("Entered into userFormEditGet()......");
    ModelAndView localModelAndView = null;
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    try
    {
      Long localLong = (paramHttpServletRequest.getParameter("userFormId") != null) || (paramHttpServletRequest.getParameter("userFormId") != "") ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("userFormId"))) : null;
      
      Object localObject1 = localLong != null ? this.userFormsService.getUserFormsById(localLong) : null;
      
      Object localObject2 = new ArrayList();
      ArrayList localArrayList1 = new ArrayList();
      ArrayList localArrayList2 = new ArrayList();
      int i = 1;
      Template localTemplate = this.templateService.getCompnentsByTemplateId(((UserForms)localObject1).getTemplateId());
      if (localObject1 != null)
      {
        localModelAndView = new ModelAndView("UserFormEdit");
        localObject2 = ((UserForms)localObject1).getUserFormComponents();
        int j = 1;
        if (((List)localObject2).size() > 15) {
          j = 15;
        } else {
          j = ((List)localObject2).size();
        }
        for (int k = 0; k < j; k++) {
          if (k <= 7) {
            localArrayList1.add(((List)localObject2).get(k));
          } else {
            localArrayList2.add(((List)localObject2).get(k));
          }
        }
        HashMap localHashMap = getUserComponetHeadingList((List)localObject2);
        List localList = null;
        if (localUserIfs != null) {
          localList = this.userFormsService.getUserActiveForms(localUserIfs);
        }
        if (localList != null) {
          i = localList.size();
        }
        i++;       
        localModelAndView.addObject("count", Integer.valueOf(i));
        localModelAndView.addObject("userForm", localObject1);
        localModelAndView.addObject("template", localTemplate);
        localModelAndView.addObject("leftUserFormComponents", localArrayList1);
        
        localModelAndView.addObject("rightUserFormsComponents", localArrayList2);
        
        localModelAndView.addObject("templateId", ((UserForms)localObject1).getTemplateId());
        localModelAndView.addObject("compDrp", localHashMap);
      }
      else
      {
        localModelAndView = new ModelAndView("Welcome");
        localModelAndView.addObject("user", localUserIfs);
      }
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/userFormEdit103.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView userFormEdit103Get(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    log.info("Entered into userFormEditGet()......");
    ModelAndView localModelAndView = null;
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    Long localLong = (paramHttpServletRequest.getParameter("userFormId") != null) || (paramHttpServletRequest.getParameter("userFormId") != "") ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("userFormId"))) : null;
    try
    {
      Object localObject1 = localLong != null ? this.userFormsService.getUserFormsById(localLong) : null;
      
      Object localObject2 = new ArrayList();
      
      Template localTemplate = this.templateService.getCompnentsByTemplateId(((UserForms)localObject1).getTemplateId());
      if (localObject1 != null)
      {
        localModelAndView = new ModelAndView("UserEditTemplate103");
        localObject2 = ((UserForms)localObject1).getUserFormComponents();
        Collections.sort((List)localObject2, new Comparator<UserFormsComponents>()
        {
          public int compare(UserFormsComponents paramAnonymousUserFormsComponents1, UserFormsComponents paramAnonymousUserFormsComponents2)
          {
            return paramAnonymousUserFormsComponents1.getPosition().getPositionId().compareTo(paramAnonymousUserFormsComponents2.getPosition().getPositionId());
          }
        });
        divideComponentUser3((List)localObject2, localModelAndView);
        paramHttpServletRequest.setAttribute("imgType", ((UserForms)localObject1).getFormImgType());
        localModelAndView.addObject("userForm", localObject1);
        localModelAndView.addObject("user", localUserIfs);
        localModelAndView.addObject("template", localTemplate);
        localModelAndView.addObject("userFormComponents", localObject2);
      }
      else
      {
        localModelAndView = new ModelAndView("Welcome");
        localModelAndView.addObject("user", localUserIfs);
      }
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/updateForm.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView updateUserForm(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    log.info("Entering into updateUserForm() ...... ");
    ModelAndView localModelAndView = new ModelAndView("UserFormView");
    try
    {
      String str1 = paramHttpServletRequest.getParameter("headingText");
      String str2 = paramHttpServletRequest.getParameter("saveLogo");
      String str3 = str2 != null ? paramHttpServletRequest.getParameter("saveLogo") : paramHttpServletRequest.getParameter("saveImage");
      ArrayList localArrayList = new ArrayList();
      UserForms localUserForms = null;
      
      Long localLong3 = null;
      
      ComponentDTO localComponentDTO = null;
      Long localLong4 = null;
      Long localLong5 = null;
      String str7 = null;
      String str8 = null;
      Long localLong6 = paramHttpServletRequest.getParameter("userFormId") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("userFormId"))) : null;
      String str9 = paramHttpServletRequest.getParameter("formName") != null ? paramHttpServletRequest.getParameter("formName") : null;
      String str10 = paramHttpServletRequest.getParameter("headerColor") != null ? paramHttpServletRequest.getParameter("headerColor") : "#045fb4";
      Object localObject4;
      if (str3 != null)
      {
        String str11 = "form";
        Object localObject2 = "user";
        log.info("file path:" + IMAGE_LIBRARY_PATH);
        Object localObject3 = new File(IMAGE_LIBRARY_PATH + str2);
        localObject4 = new byte[(int)((File)localObject3).length()];
        try
        {
          FileInputStream localFileInputStream = new FileInputStream((File)localObject3);
          localFileInputStream.read((byte[])localObject4);
          localFileInputStream.close();
        }
        catch (Exception localException2)
        {
          localException2.printStackTrace();
        }
        this.documentService.saveImage(str3, (byte[])localObject4, str11, (String)localObject2);
      }
      for (int i = 1; i <= 15; i++)
      {
    	  Object localObject2 = "viewPosition" + i;
    	  Object localObject3 = "templatePosition" + i;
        localObject4 = "componentName" + i;
        String str12 = "label" + i;
        String str13 = "scoptionId" + i;
        String str14 = "optionValue" + i;
        String str15 = "templateComponent" + i;
        String str16 = "viewComponent" + i;
        String str17 = "visibility" + i;
        String str18 = "optType" + i;
        Long localLong1 = paramHttpServletRequest.getParameter((String)localObject2) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter((String)localObject2))) : null;
        Long localLong2 = Long.valueOf(i);
        String str4 = paramHttpServletRequest.getParameter((String)localObject4) != null ? paramHttpServletRequest.getParameter((String)localObject4) : null;
        String str5 = paramHttpServletRequest.getParameter(str12) != null ? paramHttpServletRequest.getParameter(str12) : " ";
        String str6 = paramHttpServletRequest.getParameter(str14) != null ? paramHttpServletRequest.getParameter(str14) : " ";
        localLong4 = paramHttpServletRequest.getParameter(str15) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str15))) : null;
        localLong5 = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
        str7 = paramHttpServletRequest.getParameter(str17) != null ? paramHttpServletRequest.getParameter(str17) : " ";
        str8 = paramHttpServletRequest.getParameter(str18) != null ? paramHttpServletRequest.getParameter(str18) : " ";
        if (paramHttpServletRequest.getParameter(str14) != null) {
          try
          {
            localLong3 = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str13)));
          }
          catch (Exception localException3)
          {
            localLong3 = null;
          }
        }
        if ((localLong5 != null) && (localLong4 != null))
        {
          localComponentDTO = new ComponentDTO(localLong1, localLong2, str4, str5, localLong3, str6, localLong4, localLong5, str7, str8);
          
          localArrayList.add(localComponentDTO);
        }
      }
      if (localLong6 != null) {
        localUserForms = this.userFormsService.updateUserFormService(localArrayList, localLong6, str10, str9, str1, str3);
      }
      localUserForms = this.userFormsService.getUserFormsById(localLong6);
      Object localObject1 = new ArrayList();
      Object localObject2 = new ArrayList();
      Object localObject3 = new ArrayList();
      if (localUserForms != null)
      {
        localObject1 = localUserForms.getUserFormComponents();
        int j = 1;
        if (((List)localObject1).size() > 15) {
          j = 15;
        } else {
          j = ((List)localObject1).size();
        }
        for (int k = 0; k < j; k++) {
          if (k <= 7) {
            ((List)localObject2).add(((List)localObject1).get(k));
          } else {
            ((List)localObject3).add(((List)localObject1).get(k));
          }
        }
      }
      UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
      localModelAndView.addObject("user", localUserIfs);
      localModelAndView.addObject("userForm", localUserForms);
      localModelAndView.addObject("leftUserFormComponents", localObject2);
      localModelAndView.addObject("rightUserFormsComponents", localObject3);
      localModelAndView.addObject("templateId", localUserForms.getTemplateId());
    }
    catch (Exception localException1)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/updateUserForm103.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView updateUserForm103(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getParameter("userFormId");
    String str2 = paramHttpServletRequest.getParameter("formName");
    String str3 = str2 != null ? str2 : null;
    String str4 = paramHttpServletRequest.getParameter("imgType");
    Long localLong = str1 != null ? Long.valueOf(Long.parseLong(str1)) : null;
    String str5 = "form";
    String str6 = "user";
    ModelAndView localModelAndView = new ModelAndView("UserTemplate103");
    TreeMap localTreeMap = new TreeMap();
    UserForms localUserForms = this.userFormsService.getUserComponents(localLong);
    Object localObject1 = paramHttpServletRequest.getParameter("saveImage");
    Object localObject2 = paramHttpServletRequest.getParameter("saveLogo");
    localObject2 = localObject2 != null ? localObject2 : localObject1;
    String str7 = paramHttpServletRequest.getParameter("headingText");
    
    String str8 = paramHttpServletRequest.getParameter("saveLogoLeft1");
    str8 = str8 != null ? str8 : paramHttpServletRequest.getParameter("saveLeftImage");
    String str9 = paramHttpServletRequest.getParameter("saveLogoRight");
    str9 = str9 != null ? str9 : paramHttpServletRequest.getParameter("saveRightImage");
    String str10 = paramHttpServletRequest.getParameter("saveLogoLeft");
    String str11 = paramHttpServletRequest.getParameter("saveLogoBottomLeft");
    String str12 = paramHttpServletRequest.getParameter("saveLogoBottomRight");
    
    ArrayList localArrayList = new ArrayList();
    List localList1 = localUserForms.getUserFormComponents();
    try
    {
      if ((str8 == "") || (str8 == null)) {
        str8 = paramHttpServletRequest.getParameter("saveLogoLeftImage");
      }
      if ((str9 == "") || (str9 == null)) {
        str9 = paramHttpServletRequest.getParameter("saveLogoRightImage");
      }
      if ((localObject2 == "") || (localObject2 == null)) {
        localObject2 = localObject1;
      }
      if ((localObject1 == null) || (localObject1 == ""))
      {
        localObject1 = localObject2;
        if (localObject2 == localObject1) {
          localObject2 = localObject1;
        }
      }
    }
    catch (NullPointerException localNullPointerException)
    {
      localNullPointerException.printStackTrace();
    }
    localArrayList.add(localObject2);
    localArrayList.add(str10);
    localArrayList.add(str8);
    localArrayList.add(str9);
    localArrayList.add(str11);
    localArrayList.add(str12);
    Collections.sort(localList1, new Comparator<UserFormsComponents>()
    {
      public int compare(UserFormsComponents paramAnonymousUserFormsComponents1, UserFormsComponents paramAnonymousUserFormsComponents2)
      {
        return paramAnonymousUserFormsComponents1.getPosition().getPositionId().compareTo(paramAnonymousUserFormsComponents2.getPosition().getPositionId());
      }
    });
    log.info("file path:" + IMAGE_LIBRARY_PATH);
    saveImages(localArrayList, str5, str6);
    for (int i = 0; i < localList1.size(); i++)
    {
      long l = ((UserFormsComponents)localList1.get(i)).getComponent().getComponentId().longValue();
      String str13 = "Component_" + l;
      String str14 = paramHttpServletRequest.getParameter(str13);
      if (i == 69) {
        str14 = str10;
      }
      if (i == 70) {
        str14 = str11;
      }
      if (i == 71) {
        str14 = str12;
      }
      if ((str14 == null) && (i >= 38) && (i <= 68)) {
        str14 = paramHttpServletRequest.getParameter("editComp_" + i);
      }
      if ((str14 == null) && (i >= 72) && (i <= 74)) {
        str14 = paramHttpServletRequest.getParameter("editComp_" + i);
      }
      if (str14 != null) {
        localTreeMap.put(Long.valueOf(l), str14);
      }
    }
    localUserForms = this.userFormsService.getUserComponents(localLong);
    this.userFormsService.updateUserForm103Service(localTreeMap, localUserForms, str3, str7, (String)localObject2, str8, str9, str4);
    List localList2 = localUserForms.getUserFormComponents();
    Collections.sort(localList2, new Comparator()
    {
      public int compare(UserFormsComponents paramAnonymousUserFormsComponents1, UserFormsComponents paramAnonymousUserFormsComponents2)
      {
        return paramAnonymousUserFormsComponents1.getPosition().getPositionId().compareTo(paramAnonymousUserFormsComponents2.getPosition().getPositionId());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    divideComponentUser3(localList2, localModelAndView);
    paramHttpServletRequest.setAttribute("imgType", localUserForms.getFormImgType());
    localModelAndView.addObject("userForm", localUserForms);
    localModelAndView.addObject("userFormComponents", localList2);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/navigateShowUserView.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView showUserTemplate(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    long l = Long.parseLong(paramHttpServletRequest.getParameter("formid"));
    ModelAndView localModelAndView = new ModelAndView("UserForm");
    Long localLong = paramHttpServletRequest.getParameter("formid") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("formid"))) : null;
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    ArrayList localArrayList1 = new ArrayList();
    Object localObject = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    UserForms localUserForms1 = null;
    try
    {
      if (localLong != null)
      {
        localUserForms1 = this.userFormsService.getUserFormsById(localLong);
        if (localUserForms1 != null)
        {
          localObject = localUserForms1.getUserFormComponents();
          int i = 1;
          if (((List)localObject).size() > 15) {
            i = 15;
          } else {
            i = ((List)localObject).size();
          }
          for (int j = 0; j < i; j++) {
            if (j <= 7) {
              localArrayList2.add(((List)localObject).get(j));
            } else {
              localArrayList3.add(((List)localObject).get(j));
            }
          }
        }
      }
      if (localUserIfs.getUserForms() != null) {
        for (UserForms localUserForms2 : localUserIfs.getUserForms()) {
          if (localUserForms2.getStatus().equalsIgnoreCase("active")) {
            localArrayList1.add(localUserForms2);
          }
        }
      }
      localModelAndView.addObject("userForm", localUserForms1);
      localModelAndView.addObject("user", localUserIfs);
      localModelAndView.addObject("templateId", localUserForms1.getTemplateId());
      localModelAndView.addObject("leftUserFormComponents", localArrayList2);
      
      localModelAndView.addObject("rightUserFormsComponents", localArrayList3);
      
      localModelAndView.addObject("userForms", localArrayList1);
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/navigateShowUserDelete.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView deleteUserTemplate(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    long l1 = Long.parseLong(paramHttpServletRequest.getParameter("formid"));
    long l2 = Long.parseLong(paramHttpServletRequest.getParameter("userid"));
    ModelAndView localModelAndView = new ModelAndView("UserFormsList");
    boolean bool = this.userFormsService.setStatusInactive(Long.valueOf(l1));
    List localList1 = this.userIfsService.getUsers();
    List localList2 = this.userIfsService.getUserForm(Long.valueOf(l2));
    Collections.sort(localList2, new Comparator()
    {
      public int compare(UserForms paramAnonymousUserForms1, UserForms paramAnonymousUserForms2)
      {
        return paramAnonymousUserForms1.getCreatedTime().compareTo(paramAnonymousUserForms2.getCreatedTime());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    localModelAndView.addObject("users", localList1);
    localModelAndView.addObject("userForms", localList2);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/getAdminEditComponent.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getAdminEditComponent(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("EditComponent");
    Long localLong = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("ufsId")));
    Components localComponents = this.componentsService.getComponentByCid(localLong);
    
    String str1 = "";
    String str2 = "";
    for (Object localObject = localComponents.getSubComponents().getSubComponentsOptions().iterator(); ((Iterator)localObject).hasNext();)
    {
      SubComponentsOptions localSubComponentsOptions = (SubComponentsOptions)((Iterator)localObject).next();
      if (localSubComponentsOptions.getStatus().equals("true"))
      {
        str1 = localSubComponentsOptions.getOptions().getOptionDescription();
        str2 = localSubComponentsOptions.getOptionType();
      }
    }
    Object localObject = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    localModelAndView.addObject("user", localObject);
    localModelAndView.addObject("selectOptType", str2);
    localModelAndView.addObject("selectedComp", localComponents);
    localModelAndView.addObject("selectDesc", str1);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/getEditComponent.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getEditComponent(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("EditComponent");
    Long localLong = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("ufsId")));
    UserFormsComponents localUserFormsComponents = this.userFormsService.getComponentByUfId(localLong);
    
    Components localComponents = localUserFormsComponents.getComponent();
    String str1 = "";
    String str2 = "";
    for (Object localObject = localComponents.getSubComponents().getSubComponentsOptions().iterator(); ((Iterator)localObject).hasNext();)
    {
      SubComponentsOptions localSubComponentsOptions = (SubComponentsOptions)((Iterator)localObject).next();
      if (localSubComponentsOptions.getStatus().equals("true"))
      {
        str2 = localSubComponentsOptions.getOptionType();
        str1 = localSubComponentsOptions.getOptions().getOptionDescription();
      }
    }
    Object localObject = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    localModelAndView.addObject("user", localObject);
    localModelAndView.addObject("selectOptType", str2);
    localModelAndView.addObject("selectedComp", localComponents);
    localModelAndView.addObject("selectDesc", str1);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/getOptions.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getOptions(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("EditOption");
    Long localLong = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("optId")));
    SubComponentsOptions localSubComponentsOptions = this.subOptionService.getSubCompnentOption(localLong);
    
    String str = localSubComponentsOptions.getOptions().getOptionDescription();
    localModelAndView.addObject("optionDesc", str);
    localModelAndView.addObject("optionType", localSubComponentsOptions.getOptionType());
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/getOptionsDropdown.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getOptionsDropdown(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("OptionDropdown");
    try
    {
      Long localLong1 = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("cid")));
      List localList = this.userFormCompService.getSubOptionsById(localLong1);
      
      UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
      Long localLong2 = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("slectOPtId")));
      localModelAndView.addObject("user", localUserIfs);
      localModelAndView.addObject("selectId", localLong2);
      localModelAndView.addObject("subOptions", localList);
      
      return localModelAndView;
    }
    catch (Exception localException)
    {
      localException = 
      
        localException;log.info(" UserController.java ,getOptionsDropdown .." + localException);return localModelAndView;
    }
    finally {}
//    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/userFormView.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView userFormViewGet(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = new ModelAndView("UserFormView");
    log.info("Entered into .... navigateUserTemplateViewGet");
    Long localLong = paramHttpServletRequest.getParameter("formid") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("formid"))) : null;
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    try
    {
      ArrayList localArrayList1 = new ArrayList();
      Object localObject = new ArrayList();
      ArrayList localArrayList2 = new ArrayList();
      ArrayList localArrayList3 = new ArrayList();
      UserForms localUserForms1 = null;
      if (localLong != null)
      {
        localUserForms1 = this.userFormsService.getUserFormsById(localLong);
        if (localUserForms1 != null)
        {
          localObject = localUserForms1.getUserFormComponents();
          int i = 1;
          if (((List)localObject).size() > 15) {
            i = 15;
          } else {
            i = ((List)localObject).size();
          }
          for (int j = 0; j < i; j++) {
            if (j <= 7) {
              localArrayList2.add(((List)localObject).get(j));
            } else {
              localArrayList3.add(((List)localObject).get(j));
            }
          }
        }
      }
      if (localUserIfs.getUserForms() != null) {
        for (UserForms localUserForms2 : localUserIfs.getUserForms()) {
          if (localUserForms2.getStatus().equalsIgnoreCase("active")) {
            localArrayList1.add(localUserForms2);
          }
        }
      }
      localModelAndView.addObject("userForm", localUserForms1);
      localModelAndView.addObject("user", localUserIfs);
      localModelAndView.addObject("templateId", localUserForms1.getTemplateId());
      localModelAndView.addObject("leftUserFormComponents", localArrayList2);
      localModelAndView.addObject("rightUserFormsComponents", localArrayList3);
      localModelAndView.addObject("userForms", localArrayList1);
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/updateTemplate3.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView updateTemplate3(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    Long localLong = Long.valueOf(3L);
    String str1 = "form";
    String str2 = "admin";
    String str3 = paramHttpServletRequest.getParameter("imgType");
    ModelAndView localModelAndView = new ModelAndView("Template103");
    TreeMap localTreeMap = new TreeMap();
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
    
    String str4 = paramHttpServletRequest.getParameter("saveLogo");
    String str5 = paramHttpServletRequest.getParameter("saveImage");
    str5 = str4 != null ? str4 : str5;
    String str6 = paramHttpServletRequest.getParameter("headingText");
    String str7 = paramHttpServletRequest.getParameter("saveLogoLeft");
    String str8 = paramHttpServletRequest.getParameter("saveLogoBottomLeft");
    String str9 = paramHttpServletRequest.getParameter("saveLogoBottomRight");
    
    ArrayList localArrayList = new ArrayList();
    List localList1 = localTemplate.getComponents();
    localArrayList.add(str5);
    localArrayList.add(str7);
    localArrayList.add(str8);
    localArrayList.add(str9);
    
    Collections.sort(localList1, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getTemplateComponent().getPosition().getPositionId().compareTo(paramAnonymousComponents2.getTemplateComponent().getPosition().getPositionId());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    log.info("file path:" + IMAGE_LIBRARY_PATH);
    for (int i = 0; i < localList1.size(); i++)
    {
      long l = ((Components)localList1.get(i)).getComponentId().longValue();
      String str10 = "Component_" + l;
      String str11 = paramHttpServletRequest.getParameter(str10);
      if ((i == 69) && 
        (str7 != null) && (str11 != str7)) {
        str11 = str7;
      }
      if ((i == 70) && 
        (str8 != null) && (str11 != str8)) {
        str11 = str8;
      }
      if ((i == 71) && 
        (str9 != null) && (str11 != str9)) {
        str11 = str9;
      }
      if ((str11 == null) && (i >= 38) && (i <= 68)) {
        str11 = paramHttpServletRequest.getParameter("editComp_" + i);
      }
      if ((str11 == null) && (i >= 72) && (i <= 74)) {
        str11 = paramHttpServletRequest.getParameter("editComp_" + i);
      }
      if ((str11 != null) || (str11 != "")) {
        localTreeMap.put(Long.valueOf(l), str11);
      }
    }
    try
    {
      this.templateService.updateTemplate3Service(localTreeMap, localTemplate, str5, str6, str3);
      saveImages(localArrayList, str1, str2);
    }
    catch (Exception localException)
    {
      log.info("error while updating master template 103 component " + localException);
    }
    List localList2 = localTemplate.getComponents();
    Collections.sort(localList2, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getTemplateComponent().getPosition().getPositionId().compareTo(paramAnonymousComponents2.getTemplateComponent().getPosition().getPositionId());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    divideComponentAdmin3(localList2, localModelAndView);
    paramHttpServletRequest.setAttribute("imgType", localTemplate.getImageType());
    localModelAndView.addObject("components", localList1);
    localModelAndView.addObject("template", localTemplate);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/updateTemplate.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView updateTemplate(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    log.info("Entering into updateUserForm() ...... ");
    ModelAndView localModelAndView = new ModelAndView("TemplateView");
    String str1 = paramHttpServletRequest.getParameter("headingText");
    String str2 = paramHttpServletRequest.getParameter("saveLogo");
    String str3 = str2 != null ? paramHttpServletRequest.getParameter("saveLogo") : paramHttpServletRequest.getParameter("saveImage");
    
    Template localTemplate = null;
    ArrayList localArrayList = new ArrayList();
    
    Long localLong3 = null;
    
    ComponentDTO localComponentDTO = null;
    Long localLong4 = null;
    Long localLong5 = null;
    String str7 = null;
    String str8 = null;
    Long localLong6 = paramHttpServletRequest.getParameter("templateId") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("templateId"))) : null;
    
    String str9 = paramHttpServletRequest.getParameter("headerColor") != null ? paramHttpServletRequest.getParameter("headerColor") : "#045fb4";
    Object localObject3;
    String localObject1;
	File localObject2;
	if (str3 != null)
    {
      String str10 = "form";
      localObject1 = "admin";
      log.info("file path:" + IMAGE_LIBRARY_PATH);
      localObject2 = new File(IMAGE_LIBRARY_PATH + str2);
      localObject3 = new byte[(int)((File)localObject2).length()];
      try
      {
        FileInputStream localFileInputStream = new FileInputStream((File)localObject2);
        localFileInputStream.read((byte[])localObject3);
        localFileInputStream.close();
      }
      catch (Exception localException1)
      {
        log.info("Error occured while getting folder inside UserController.java : updateTemplate() ..");
        localException1.printStackTrace();
      }
      this.documentService.saveImage(str3, (byte[])localObject3, str10, (String)localObject1);
    }
    Object localObject6;
    for (int i = 1; i <= 15; i++)
    {
      localObject1 = "viewPosition" + i;
//      localObject2 = "templatePosition" + i;
      localObject3 = "componentName" + i;
      String str11 = "label" + i;
//      localObject4 = "scoptionId" + i;
//      localObject5 = "optionValue" + i;
      localObject6 = "templateComponent" + i;
      String str12 = "viewComponent" + i;
      String str13 = "visibility" + i;
      String str14 = "optType" + i;
      Long localLong1 = paramHttpServletRequest.getParameter((String)localObject1) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter((String)localObject1))) : null;
      
      Long localLong2 = Long.valueOf(i);
      String str4 = paramHttpServletRequest.getParameter((String)localObject3) != null ? paramHttpServletRequest.getParameter((String)localObject3) : null;
      
      String str5 = paramHttpServletRequest.getParameter(str11) != null ? paramHttpServletRequest.getParameter(str11) : " ";
      
//      	String str6 = paramHttpServletRequest.getParameter((String)localObject5) != null ? paramHttpServletRequest.getParameter((String)localObject5) : " ";
      
      localLong4 = paramHttpServletRequest.getParameter((String)localObject6) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter((String)localObject6))) : null;
      
      localLong5 = paramHttpServletRequest.getParameter(str12) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str12))) : null;
      
      str7 = paramHttpServletRequest.getParameter(str13) != null ? paramHttpServletRequest.getParameter(str13) : " ";
      
      str8 = paramHttpServletRequest.getParameter(str14) != null ? paramHttpServletRequest.getParameter(str14) : " ";
//      if (paramHttpServletRequest.getParameter((String)localObject5) != null) {
//        try
//        {
//          String localObject4;
//		localLong3 = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter((String)localObject4)));
//        }
//        catch (Exception localException2)
//        {
//          localLong3 = null;
//        }
//      }
      if ((localLong5 != null) && (localLong4 != null))
      {
//        localComponentDTO = new ComponentDTO(localLong1, localLong2, str4, str5, localLong3, str6, localLong4, localLong5, str7, str8);
        localArrayList.add(localComponentDTO);
      }
    }
    if (localLong6 != null)
    {
      localTemplate = this.templateService.getCompnentsByTemplateId(localLong6);
      this.templateService.updateTemplateService(localArrayList, localTemplate, str9, str1, str3);
    }
    else
    {
      localTemplate = this.templateService.getCompnentsByTemplateId(Long.valueOf(1L));
    }
    List localList1 = localTemplate.getComponents();
//    Object localObject1 = new ArrayList();
//    Object localObject2 = new ArrayList();
    Collections.sort(localList1, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getTemplateComponent().getPosition().getPositionId().compareTo(paramAnonymousComponents2.getTemplateComponent().getPosition().getPositionId());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    int j = 1;
    if (localList1.size() > 15) {
      j = 15;
    } else {
      j = localList1.size();
    }
    for (int k = 0; k < j; k++) {
      if (k <= 7) {
//        ((List)localObject1).add(localList1.get(k));
      } else {
//        ((List)localObject2).add(localList1.get(k));
      }
    }
    List localList2 = localTemplate.getComponents();
    Object localObject4 = new LinkedHashMap();
    Collections.sort(localList2, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentName().compareTo(paramAnonymousComponents2.getComponentName());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    for (Object localObject5 = localList2.iterator(); ((Iterator)localObject5).hasNext();)
    {
      localObject6 = (Components)((Iterator)localObject5).next();
      ((Map)localObject4).put(((Components)localObject6).getComponentId(), ((Components)localObject6).getComponentName());
    }
    Object localObject5 = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    localModelAndView.addObject("user", localObject5);
    localModelAndView.addObject("template", localTemplate);
    localModelAndView.addObject("compDrp", localObject4);
//    localModelAndView.addObject("leftComponents", localObject1);
//    localModelAndView.addObject("rightComponents", localObject2);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/getOptionsDropdown.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getOptionsDropdownAdmin(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("OptionDropdown");
    Long localLong1 = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("cid")));
    List localList = this.componentsService.getSubOptionsByComponentId(localLong1);
    
    Long localLong2 = null;
    try
    {
      localLong2 = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("slectOPtId")));
    }
    catch (Exception localException)
    {
      log.info("slectOPtId is not an id");
    }
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("selectId", localLong2);
    localModelAndView.addObject("subOptions", localList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/getOptions.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getComponentOptions(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("EditOption");
    Long localLong = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("optId")));
    SubComponentsOptions localSubComponentsOptions = this.subOptionService.getSubCompnentOption(localLong);
    
    String str = localSubComponentsOptions.getOptions().getOptionDescription();
    localModelAndView.addObject("optionType", localSubComponentsOptions.getOptionType());
    localModelAndView.addObject("optionDesc", str);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/adminCreateComponent.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView adminCreateComponent(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("CreateComponent");
    return localModelAndView;
  } 
  @RequestMapping(value={"/admin/adminAddCreateComponent.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView addComponents(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    ModelAndView localModelAndView = new ModelAndView("AddCreateComponent");
    Long localLong = Long.valueOf(paramHttpServletRequest.getParameter("templateId") != null ? Long.parseLong(paramHttpServletRequest.getParameter("templateId")) : 1L);
    
    String str1 = paramHttpServletRequest.getParameter("heading");
    String str2 = paramHttpServletRequest.getParameter("subHeading");
    String str3 = paramHttpServletRequest.getParameter("optionNameFirst") != null ? paramHttpServletRequest.getParameter("optionNameFirst") : "First";
    
    String str4 = paramHttpServletRequest.getParameter("optionNameSecond") != null ? paramHttpServletRequest.getParameter("optionNameSecond") : "Second";
    
    String str5 = paramHttpServletRequest.getParameter("optionNameThird") != null ? paramHttpServletRequest.getParameter("optionNameThird") : "Third";
    
    String str6 = paramHttpServletRequest.getParameter("optiontypeFirst") != null ? paramHttpServletRequest.getParameter("optiontypeFirst") : "square";
    
    String str7 = paramHttpServletRequest.getParameter("optiontypeSecond") != null ? paramHttpServletRequest.getParameter("optiontypeSecond") : "square";
    
    String str8 = paramHttpServletRequest.getParameter("optiontypeThird") != null ? paramHttpServletRequest.getParameter("optiontypeThird") : "square";
    
    ArrayList localArrayList = new ArrayList(1);
    String str9 = paramHttpServletRequest.getParameter("optionDescriptionFirst") != null ? paramHttpServletRequest.getParameter("optionDescriptionFirst") : null;
    if ((str9 != null) && (!str9.isEmpty())) {
      localArrayList.add(new SubComponentOptionsDTO(this.optionsService.createOption(str3, str9), str6));
    }
    String str10 = paramHttpServletRequest.getParameter("optionDescriptionSecond") != null ? paramHttpServletRequest.getParameter("optionDescriptionSecond") : null;
    if ((str10 != null) && (!str10.isEmpty())) {
      localArrayList.add(new SubComponentOptionsDTO(this.optionsService.createOption(str4, str10), str7));
    }
    String str11 = paramHttpServletRequest.getParameter("optionDescriptionThird") != null ? paramHttpServletRequest.getParameter("optionDescriptionThird") : null;
    if ((str11 != null) && (!str11.isEmpty())) {
      localArrayList.add(new SubComponentOptionsDTO(this.optionsService.createOption(str5, str11), str8));
    }
    Components localComponents1 = this.componentsService.createCompnent(str1, str2, localArrayList);
    
    this.componentsService.addComponentsToUserForms(localComponents1);
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
    
    List localList = localTemplate.getComponents();
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    Collections.sort(localList, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentName().compareTo(paramAnonymousComponents2.getComponentName());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    localLinkedHashMap.put(Long.valueOf(Long.parseLong("0")), "--Select--");
//    for (Components localComponents2 : localList) {
//      if (localComponents2.getTemplateComponent().getStatus().getStatusId().longValue() != 3L) {
//        localLinkedHashMap.put(localComponents2.getComponentId(), localComponents2.getComponentName());
//      }
//    }
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("compDrp", localLinkedHashMap);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/adminAddComponentOptions.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView showAddComponentOptions(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("AddComponentsOptions");
    String str = paramHttpServletRequest.getParameter("opt") != null ? paramHttpServletRequest.getParameter("opt") : "First";
    
    localModelAndView.addObject("optionSelect", str);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/adminCreateOption.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView adminCreateOption(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("CreateOption");
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/adminAddCreateOption.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView adminAddCreateOption(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    SubComponentsOptions localSubComponentsOptions = null;
    Components localComponents = null;
    ModelAndView localModelAndView = new ModelAndView("AddCreateOption");
    Long localLong = (paramHttpServletRequest.getParameter("createCompOptId") != null) && (!paramHttpServletRequest.getParameter("createCompOptId").isEmpty()) ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("createCompOptId"))) : null;
    
    String str1 = (paramHttpServletRequest.getParameter("optionName") != null) && (!paramHttpServletRequest.getParameter("optionName").isEmpty()) ? paramHttpServletRequest.getParameter("optionName") : null;
    
    String str2 = (paramHttpServletRequest.getParameter("optionDescription") != null) && (!paramHttpServletRequest.getParameter("optionDescription").isEmpty()) ? paramHttpServletRequest.getParameter("optionDescription") : null;
    
    String str3 = (paramHttpServletRequest.getParameter("optionType") != null) && (!paramHttpServletRequest.getParameter("optionType").isEmpty()) ? paramHttpServletRequest.getParameter("optionType") : null;
    if ((localLong != null) && (str1 != null) && (str2 != null) && (str3 != null))
    {
      localSubComponentsOptions = this.subCompOptService.CreateSubComponentOptions(localLong.longValue(), str1, str2, str3);
      
      localComponents = this.userFormsService.createUserFormsOption(localLong, localSubComponentsOptions);
    }
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("component", localComponents);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/navigateTemplateIfs.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateTemplateIfs(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
    throws IndexOutOfBoundsException
  {
    ModelAndView localModelAndView = new ModelAndView("templateIfs");
    Long localLong = Long.valueOf(2L);
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong);
    paramHttpServletRequest.setAttribute("imgType", localTemplate.getImageType());
    localModelAndView.addObject("template", localTemplate);
    List localList = localTemplate.getComponents();
    Collections.sort(localList, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentId().compareTo(paramAnonymousComponents2.getComponentId());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    resizeAdminForm(localList, localModelAndView);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/getFreeIns.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getFreeIns(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("templateIfs");
    String str1 = paramHttpServletRequest.getParameter("headingText102");
    String str2 = paramHttpServletRequest.getParameter("saveLogo");
    String str3 = str2 != null ? str2 : paramHttpServletRequest.getParameter("saveImage");
    String str4 = paramHttpServletRequest.getParameter("saveLftOptionalLogo");
    String str5 = paramHttpServletRequest.getParameter("saveRghtOptionalLogo");
    String str6 = paramHttpServletRequest.getParameter("imgType");
    ArrayList localArrayList = new ArrayList();
    ComponentObjDTO localComponentObjDTO = null;
    
    Long localLong = null;
    Template localTemplate = null;
    int i = 42;
    for (int j = 6; j < i; j++)
    {
    	Object localObject2 = "editComp_" + j;
    	Object localObject3 = "componentId" + j;
      String str7 = paramHttpServletRequest.getParameter((String)localObject2) != null ? paramHttpServletRequest.getParameter((String)localObject2) : null;
      
      localLong = paramHttpServletRequest.getParameter((String)localObject3) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter((String)localObject3))) : null;
      
      localComponentObjDTO = new ComponentObjDTO(str7, localLong);
      localArrayList.add(localComponentObjDTO);
    }
    Object localObject4;
    if (str3 != null)
    {
     Object localObject1 = "form";
     Object localObject2 = "admin";
      log.info("file path:" + IMAGE_LIBRARY_PATH);
      Object localObject3 = new File(IMAGE_LIBRARY_PATH + str3);
      localObject4 = new byte[(int)((File)localObject3).length()];
      try
      {
        FileInputStream localFileInputStream = new FileInputStream((File)localObject3);
        localFileInputStream.read((byte[])localObject4);
        localFileInputStream.close();
      }
      catch (Exception localException)
      {
        log.info("Error occured while getting folder inside UserController.java : updateTemplate() ..");
        localException.printStackTrace();
      }
      this.documentService.saveImage(str3, (byte[])localObject4, (String)localObject1, (String)localObject2);
    }
    localTemplate = this.templateService.getCompnentsByTemplateId(Long.valueOf(2L));
    this.templateService.updateComponentService(localArrayList, localTemplate, str1, str3, str4, str5, str6);
    
    paramHttpServletRequest.setAttribute("imgType", localTemplate.getImageType());
    localModelAndView.addObject("template", localTemplate);
    Object localObject1 = localTemplate.getComponents();
    Collections.sort((List)localObject1, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentId().compareTo(paramAnonymousComponents2.getComponentId());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    resizeAdminForm((List)localObject1, localModelAndView);
    Object localObject2 = new LinkedHashMap();
    Collections.sort((List)localObject1, new Comparator<Components>()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentId().compareTo(paramAnonymousComponents2.getComponentId());
      }
    });
    for (Object localObject3 = ((List)localObject1).iterator(); ((Iterator)localObject3).hasNext();)
    {
      localObject4 = (Components)((Iterator)localObject3).next();
      ((Map)localObject2).put(((Components)localObject4).getComponentId(), ((Components)localObject4).getComponentName());
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/uploadRhtLftImg.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView uploadRhtLftLogo(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getSession().getServletContext().getRealPath(System.getProperty("file.separator"));
    
    str1 = str1.substring(0, str1.length() - 1);
    String str2 = str1 + System.getProperty("file.separator") + "ImageLibrary";
    
    String str3 = paramHttpServletRequest.getParameter("type");
    String str4 = paramHttpServletRequest.getParameter("imageHeight");
    String str5 = paramHttpServletRequest.getParameter("imageWidth");
    ImageFileObject localImageFileObject = getImageFile(paramHttpServletRequest, paramHttpServletResponse, "", str4, str5);
    if (localImageFileObject != null)
    {
      ModelAndView localModelAndView = new ModelAndView("optionalLogo");
      
      localModelAndView.addObject("url", localImageFileObject.getUploadImageFile().getPath());
      localModelAndView.addObject("logoImage", localImageFileObject.getUploadImageFile());
      localModelAndView.addObject("logoName", localImageFileObject.getFileName());
      localModelAndView.addObject("type", str3);
      paramHttpServletResponse.setContentType("text/html");
      return localModelAndView;
    }
    return null;
  }
  
  @RequestMapping(value={"/user/getUserComponentIfs.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserComponentIfs(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("userTemplateIfs");
    Object localObject = new ArrayList();
    ArrayList localArrayList = new ArrayList();
    UserForms localUserForms = null;
    Template localTemplate = this.templateService.getCompnentsByTemplateId(Long.valueOf(2L));
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    localUserForms = this.userFormsService.getUserDeFaultIfsForm(localUserIfs.getUserId(), localTemplate);
    
    int i = 1;
    if (localUserForms != null)
    {
      localObject = localUserForms.getUserFormComponents();
      i = ((List)localObject).size();
      for (int j = 0; j < i; j++) {
        localArrayList.add(((List)localObject).get(j));
      }
    }
    localModelAndView.addObject("count", Integer.valueOf(i));
    localModelAndView.addObject("userForm", localUserForms);
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("templateId", Long.valueOf(2L));
    localModelAndView.addObject("template", localTemplate);
    paramHttpServletRequest.setAttribute("imgType", "circle");
    paramHttpServletRequest.setAttribute("imgType", localUserForms.getFormImgType());
    resizeUserForm(localArrayList, localModelAndView);
    localModelAndView.addObject("newUserFormComponents", localArrayList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/getUserFormView103.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserFormView103(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    Long localLong1 = Long.valueOf(System.currentTimeMillis());
    ModelAndView localModelAndView = new ModelAndView("UserTemplate103");
    Long localLong2 = Long.valueOf(paramHttpServletRequest.getParameter("templateId") != null ? Long.parseLong(paramHttpServletRequest.getParameter("templateId")) : 3L);
    Object localObject = new ArrayList();
    UserForms localUserForms = null;
    Template localTemplate = this.templateService.getCompnentsByTemplateId(localLong2);
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    
    localUserForms = this.userFormsService.getUserDeFaultIfsFormFor103(localUserIfs.getUserId(), localTemplate);
    if (localUserForms != null)
    {
      localObject = localUserForms.getUserFormComponents();
      divideComponentUser3((List)localObject, localModelAndView);
    }
    localModelAndView.addObject("userForm", localUserForms);
    paramHttpServletRequest.setAttribute("imgType", localUserForms.getFormImgType());
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("templateId", localLong2);
    localModelAndView.addObject("template", localTemplate);
    localModelAndView.addObject("userFormComponents", localObject);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/getUserComponent103.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserComponent103(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
    throws AppDAOException
  {
    ModelAndView localModelAndView = new ModelAndView("UserTemplate103");
    Long localLong = paramHttpServletRequest.getParameter("formid") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("formid"))) : null;
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    ArrayList localArrayList1 = new ArrayList();
    Object localObject = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    ArrayList localArrayList4 = new ArrayList();
    ArrayList localArrayList5 = new ArrayList();
    ArrayList localArrayList6 = new ArrayList();
    ArrayList localArrayList7 = new ArrayList();
    ArrayList localArrayList8 = new ArrayList();
    UserForms localUserForms1 = null;
    if (localLong != null)
    {
      localUserForms1 = this.userFormsService.getUserFormsById(localLong);
      if (localUserForms1 != null)
      {
        localObject = localUserForms1.getUserFormComponents();
        int i = 1;
        if (((List)localObject).size() > 73) {
          i = 73;
        } else {
          i = ((List)localObject).size();
        }
        for (int j = 0; j < i; j++) {
          if (j == 0) {
            localArrayList2.add(((List)localObject).get(j));
          } else if ((j > 0) && (j <= 3)) {
            localArrayList3.add(((List)localObject).get(j));
          } else if ((j > 3) && (j <= 13)) {
            localArrayList4.add(((List)localObject).get(j));
          } else if ((j > 13) && (j <= 28)) {
            localArrayList6.add(((List)localObject).get(j));
          } else if ((j > 28) && (j <= 37)) {
            localArrayList5.add(((List)localObject).get(j));
          } else if ((j > 37) && (j <= 68)) {
            localArrayList7.add(((List)localObject).get(j));
          } else if ((j > 68) && (j <= 72)) {
            localArrayList8.add(((List)localObject).get(j));
          }
        }
        Collections.sort((List)localObject, new Comparator()
        {
          public int compare(UserFormsComponents paramAnonymousUserFormsComponents1, UserFormsComponents paramAnonymousUserFormsComponents2)
          {
            return paramAnonymousUserFormsComponents1.getPosition().getPositionId().compareTo(paramAnonymousUserFormsComponents2.getPosition().getPositionId());
          }

		@Override
		public int compare(Object o1, Object o2) {
			// TODO Auto-generated method stub
			return 0;
		}
        });
      }
    }
    if (localUserIfs.getUserForms() != null) {
      for (UserForms localUserForms2 : localUserIfs.getUserForms()) {
        if (localUserForms2.getStatus().equalsIgnoreCase("active")) {
          localArrayList1.add(localUserForms2);
        }
      }
    }
    paramHttpServletRequest.setAttribute("imgType", localUserForms1.getFormImgType());
    localModelAndView.addObject("userForm", localUserForms1);
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("userForms", localArrayList1);
    localModelAndView.addObject("templateId", localUserForms1.getTemplateId());
    localModelAndView.addObject("userFormComponents", localObject);
    localModelAndView.addObject("detailUserFormComponent", localArrayList2);
    localModelAndView.addObject("topUserFormComponents", localArrayList3);
    localModelAndView.addObject("topLeftUserFormComponents", localArrayList4);
    localModelAndView.addObject("topRightUserFormComponents", localArrayList5);
    localModelAndView.addObject("bottomLeftUserFormComponents", localArrayList6);
    localModelAndView.addObject("bottomRightUserFormComponents", localArrayList7);
    localModelAndView.addObject("imageUserFormComponents", localArrayList8);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/getUserViewComponentIfs.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserViewComponentIfs(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("userTemplateIfs");
    try
    {
      Long localLong = paramHttpServletRequest.getParameter("formid") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("formid"))) : null;
      
      Object localObject1 = new ArrayList();
      ArrayList localArrayList1 = new ArrayList();
      ArrayList localArrayList2 = new ArrayList();
      UserForms localUserForms = null;
      UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
      if (localLong != null)
      {
        localUserForms = this.userFormsService.getUserFormsById(localLong);
        if (localUserForms != null)
        {
          localObject1 = localUserForms.getUserFormComponents();
          int i = ((List)localObject1).size();
          for (int j = 0; j < i; j++) {
            localArrayList1.add(((List)localObject1).get(j));
          }
        }
      }
      if (localUserIfs.getUserForms() != null) {
        for (Object localObject2 = localUserIfs.getUserForms().iterator(); ((Iterator)localObject2).hasNext();)
        {
          Object localObject3 = (UserForms)((Iterator)localObject2).next();
          if (((UserForms)localObject3).getStatus().equalsIgnoreCase("active")) {
            localArrayList2.add(localObject3);
          }
        }
      }
      localModelAndView.addObject("userForm", localUserForms);
      localModelAndView.addObject("user", localUserIfs);
      paramHttpServletRequest.setAttribute("imgType", "circle");
      paramHttpServletRequest.setAttribute("imgType", localUserForms.getFormImgType());
      localModelAndView.addObject("newUserFormComponents", localArrayList1);
      Object localObject2 = localUserForms.getUserFormComponents();
      Object localObject3 = new LinkedHashMap();
      
      Collections.sort((List)localObject2, new Comparator()
      {
        public int compare(UserFormsComponents paramAnonymousUserFormsComponents1, UserFormsComponents paramAnonymousUserFormsComponents2)
        {
          return paramAnonymousUserFormsComponents1.getComponent().getComponentId().compareTo(paramAnonymousUserFormsComponents2.getComponent().getComponentId());
        }

		@Override
		public int compare(Object o1, Object o2) {
			// TODO Auto-generated method stub
			return 0;
		}
      });
//      for (UserFormsComponents localUserFormsComponents :localObject2) {
//        ((Map)localObject3).put(localUserFormsComponents.getComponent().getComponentId(), localUserFormsComponents.getComponent().getComponentName());
//      }
      resizeUserForm(localArrayList1, localModelAndView);
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("Errorpage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/updateUserFormIfs.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView updateUserFormIfs(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("userTemplateIfs");
    try
    {
      String str1 = paramHttpServletRequest.getParameter("headingText102");
      String str2 = paramHttpServletRequest.getParameter("imgType");
      ArrayList localArrayList = new ArrayList();
      Object localObject1 = new ArrayList();
      ComponentObjDTO localComponentObjDTO = null;
      
      String str4 = paramHttpServletRequest.getParameter("userFormId");
      Long localLong = null;
      UserForms localUserForms = null;
      String str5 = paramHttpServletRequest.getParameter("saveLogo");
      str5 = str5 != null ? str5 : paramHttpServletRequest.getParameter("saveImage");
      String str6 = paramHttpServletRequest.getParameter("saveLogoLeft1");
      str6 = str6 != null ? str6 : paramHttpServletRequest.getParameter("saveLeftImage");
      String str7 = paramHttpServletRequest.getParameter("saveLogoRight");
      str7 = str7 != null ? str7 : paramHttpServletRequest.getParameter("saveRightImage");
      String str8 = paramHttpServletRequest.getParameter("formName") != null ? paramHttpServletRequest.getParameter("formName") : null;
      int i = 42;
      for (int j = 6; j < i; j++)
      {
       String str10 = "editComp_" + j;
        Object localObject2 = "componentId" + j;
        String str3 = paramHttpServletRequest.getParameter(str10) != null ? paramHttpServletRequest.getParameter(str10) : null;
        
        localLong = paramHttpServletRequest.getParameter((String)localObject2) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter((String)localObject2))) : null;
        
        localComponentObjDTO = new ComponentObjDTO(str3, localLong);
        localArrayList.add(localComponentObjDTO);
      }
      String str9 = "form";
      String str10 = "user";
      Object localObject2 = new ArrayList();
      ((List)localObject2).add(str5);
      ((List)localObject2).add(str6);
      ((List)localObject2).add(str7);
      saveImages((List)localObject2, str9, str10);
      localUserForms = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(str4)));
      this.userFormsService.updateUserComponentServiceIfs(localArrayList, localUserForms, str1, str5, str6, str7, str2, str8);
      
      paramHttpServletRequest.setAttribute("imgType", localUserForms.getFormImgType());
      localObject1 = localUserForms.getUserFormComponents();
      resizeUserForm((List)localObject1, localModelAndView);
      localModelAndView.addObject("newUserFormComponents", localObject1);
      localModelAndView.addObject("userForm", localUserForms);
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/uploadRhtLftImg.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView uploadUserRhtLftLogo(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getSession().getServletContext().getRealPath(System.getProperty("file.separator"));
    
    str1 = str1.substring(0, str1.length() - 1);
    String str2 = str1 + System.getProperty("file.separator") + "ImageLibrary";
    
    String str3 = paramHttpServletRequest.getParameter("type");
    String str4 = paramHttpServletRequest.getParameter("imageHeight");
    String str5 = paramHttpServletRequest.getParameter("imageWidth");
    ImageFileObject localImageFileObject = getImageFile(paramHttpServletRequest, paramHttpServletResponse, "", str4, str5);
    if (localImageFileObject != null)
    {
      ModelAndView localModelAndView = new ModelAndView("optionalLogo");
      localModelAndView.addObject("url", localImageFileObject.getUploadImageFile().getPath());
      
      localModelAndView.addObject("logoImage", localImageFileObject.getUploadImageFile());
      localModelAndView.addObject("logoName", localImageFileObject.getFileName());
      localModelAndView.addObject("type", str3);
      paramHttpServletResponse.setContentType("text/html");
      return localModelAndView;
    }
    return null;
  }
  
  @RequestMapping(value={"/user/saveLogoImage.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public void saveUserLogoImage(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getParameter("saveLogo");
    String str2 = "form";
    String str3 = "user";
    log.info("file path:" + IMAGE_LIBRARY_PATH);
    File localFile = new File(IMAGE_LIBRARY_PATH + str1);
    byte[] arrayOfByte = new byte[(int)localFile.length()];
    try
    {
      FileInputStream localFileInputStream = new FileInputStream(localFile);
      
      localFileInputStream.read(arrayOfByte);
      localFileInputStream.close();
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    this.documentService.saveImage(str1, arrayOfByte, str2, str3);
  }
  
  @RequestMapping(value={"/user/editTemplate2.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getUserLogo(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getParameter("type");
    System.out.println("type: " + str1);
    String str2 = paramHttpServletRequest.getParameter("imageHeight");
    String str3 = paramHttpServletRequest.getParameter("imageWidth");
    ImageFileObject localImageFileObject = getImageFile(paramHttpServletRequest, paramHttpServletResponse, "", str2, str3);
    if (localImageFileObject != null)
    {
    	ModelAndView localModelAndView = new ModelAndView("image");
      localModelAndView.addObject("type", str1);
      localModelAndView.addObject("url", localImageFileObject.getUploadImageFile().getPath());
      localModelAndView.addObject("logoImage", localImageFileObject.getUploadImageFile());
      localModelAndView.addObject("logoName", localImageFileObject.getFileName());
      paramHttpServletResponse.setContentType("text/html");
      return localModelAndView;
    }
    ModelAndView localModelAndView = new ModelAndView("image");
    localModelAndView.addObject("smallImg", "yes");
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/userFormDelete.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView userFormDeleteGet(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = new ModelAndView("UserCreatedForms");
    try
    {
      Long localLong1 = paramHttpServletRequest.getParameter("formId") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("formId"))) : null;
      
      Long localLong2 = paramHttpServletRequest.getParameter("userId") != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("userId"))) : null;
      if (localLong1 != null)
      {
        UserIfs localUserIfs = this.userIfsService.getUserById(localLong2);
        List localList = this.userFormsService.deleteUserForm(localLong1, localUserIfs);
        
        Collections.sort(localList, new Comparator()
        {
          public int compare(UserForms paramAnonymousUserForms1, UserForms paramAnonymousUserForms2)
          {
            return paramAnonymousUserForms1.getCreatedTime().compareTo(paramAnonymousUserForms2.getCreatedTime());
          }

		@Override
		public int compare(Object o1, Object o2) {
			// TODO Auto-generated method stub
			return 0;
		}
        });
        localModelAndView.addObject("user", localUserIfs);
        localModelAndView.addObject("userForms", localList);
      }
    }
    catch (Exception localException)
    {
      localModelAndView = new ModelAndView("ErrorPage");
    }
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/uploadImageToRep.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView uploadImageToRep(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getSession().getServletContext().getRealPath(System.getProperty("file.separator"));
    
    str1 = str1.substring(0, str1.length() - 1);
    String str2 = str1 + System.getProperty("file.separator") + "ImageLibrary";
    
    String str3 = paramHttpServletRequest.getParameter("imageHeight");
    String str4 = paramHttpServletRequest.getParameter("imageWidth");
    ImageFileObject localImageFileObject = getImageFile(paramHttpServletRequest, paramHttpServletResponse, "", str3, str4);
    if (localImageFileObject != null)
    {
      ModelAndView localModelAndView = new ModelAndView("imageRepository");
      localModelAndView.addObject("url", localImageFileObject.getUploadImageFile().getPath());
      
      localModelAndView.addObject("logoImage", localImageFileObject.getUploadImageFile());
      localModelAndView.addObject("logoName", localImageFileObject.getFileName());
      paramHttpServletResponse.setContentType("text/html");
      return localModelAndView;
    }
    return null;
  }
  
  @RequestMapping(value={"/user/uploadImageToRep.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView uploadUserImageToRep(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    String str1 = paramHttpServletRequest.getSession().getServletContext().getRealPath(System.getProperty("file.separator"));
    
    str1 = str1.substring(0, str1.length() - 1);
    String str2 = str1 + System.getProperty("file.separator") + "ImageLibrary";
    
    String str3 = paramHttpServletRequest.getParameter("imageHeight");
    String str4 = paramHttpServletRequest.getParameter("imageWidth");
    ImageFileObject localImageFileObject = getImageFile(paramHttpServletRequest, paramHttpServletResponse, "", str3, str4);
    if (localImageFileObject != null)
    {
      ModelAndView localModelAndView = new ModelAndView("imageRepository");
      localModelAndView.addObject("url", localImageFileObject.getUploadImageFile().getPath());
      
      localModelAndView.addObject("logoImage", localImageFileObject.getUploadImageFile());
      localModelAndView.addObject("logoName", localImageFileObject.getFileName());
      paramHttpServletResponse.setContentType("text/html");
      return localModelAndView;
    }
    return null;
  }  
  @RequestMapping(value={"/user/userFormList.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserFormList(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("UserHomePage");
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    List localList = null;
    if (localUserIfs != null) {
      localList = this.userFormsService.getUserActiveForms(localUserIfs);
    }
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("userForms", localList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/updateUserFormByFormId104.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public String updateUserFormByFormId104(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ArrayList localArrayList1 = new ArrayList();
    ComponentObjDTO localComponentObjDTO = null;
    String str1 = null;
    UserForms localUserForms = new UserForms();
    Long localLong = null;
    String str2 = null;   
    String str3 = "form";
    String str4 = "user";
    TreeMap localTreeMap = new TreeMap();  
    String str5 = paramHttpServletRequest.getParameter("saveLogo");
    String str6 = paramHttpServletRequest.getParameter("saveImage");
    str6 = str5 != null ? str5 : str6;
    
    String str7 = paramHttpServletRequest.getParameter("saveLogoLeft1") != null ? paramHttpServletRequest.getParameter("saveLogoLeft1") : paramHttpServletRequest.getParameter("saveLeftImage");
    String str8 = paramHttpServletRequest.getParameter("saveLogoRight") != null ? paramHttpServletRequest.getParameter("saveLogoRight") : paramHttpServletRequest.getParameter("saveRightImage");
    String str9 = paramHttpServletRequest.getParameter("saveLogoLeft");
    String str10 = paramHttpServletRequest.getParameter("saveLogoBottomLeft");
    String str11 = paramHttpServletRequest.getParameter("saveLogoBottomRight");
    String str12 = paramHttpServletRequest.getParameter("saveLogoBottomMid");
    String str13 = paramHttpServletRequest.getParameter("saveLogoBottomCenter");
    String str14 = paramHttpServletRequest.getParameter("saveLogoBottomLast");
    
    String str15 = paramHttpServletRequest.getParameter("html2canvasImg");
    log.info("html2CanvasImg::" + str15);
    
    ArrayList localArrayList2 = new ArrayList();
    
    localArrayList2.add(str6);
    localArrayList2.add(str7);
    localArrayList2.add(str8);
    localArrayList2.add(str9);
    localArrayList2.add(str10);
    localArrayList2.add(str11);
    localArrayList2.add(str12);
    localArrayList2.add(str13);
    localArrayList2.add(str14);
    
    log.info("file path:" + IMAGE_LIBRARY_PATH);
    saveImages(localArrayList2, str3, str4);
    
    String str16 = paramHttpServletRequest.getParameter("logoText");
    
    String str17 = "";
    int i = 70;
    for (int j = 0; j < i; j++) {
      try
      {
        String str19 = "component" + j;
        str17 = "componentId" + j;
        str1 = paramHttpServletRequest.getParameter(str19) != null ? paramHttpServletRequest.getParameter(str19) : null;
        localLong = paramHttpServletRequest.getParameter(str17) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str17))) : null;
        localComponentObjDTO = new ComponentObjDTO(str1, localLong);
        localArrayList1.add(localComponentObjDTO);
      }
      catch (Exception localException3)
      {
        log.error("Error while getting form data with component Id::" + j);
      }
    }
    try
    {
      str17 = "componentId70";
      localLong = paramHttpServletRequest.getParameter(str17) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str17))) : null;
      localComponentObjDTO = new ComponentObjDTO(str9, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str17 = "componentId71";
      localLong = paramHttpServletRequest.getParameter(str17) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str17))) : null;
      localComponentObjDTO = new ComponentObjDTO(str10, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str17 = "componentId72";
      localLong = paramHttpServletRequest.getParameter(str17) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str17))) : null;
      localComponentObjDTO = new ComponentObjDTO(str11, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str17 = "componentId73";
      localLong = paramHttpServletRequest.getParameter(str17) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str17))) : null;
      localComponentObjDTO = new ComponentObjDTO(str12, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str17 = "componentId74";
      localLong = paramHttpServletRequest.getParameter(str17) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str17))) : null;
      localComponentObjDTO = new ComponentObjDTO(str13, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str17 = "componentId75";
      localLong = paramHttpServletRequest.getParameter(str17) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str17))) : null;
      localComponentObjDTO = new ComponentObjDTO(str14, localLong);
      localArrayList1.add(localComponentObjDTO);
    }
    catch (Exception localException1)
    {
      log.error("Error while getting form data with component Id::");
    }
    try
    {
      String str18 = paramHttpServletRequest.getParameter("headerText");
      String str20 = paramHttpServletRequest.getParameter("imgType");
      str2 = paramHttpServletRequest.getParameter("formId");
      String str21 = paramHttpServletRequest.getParameter("formName") != null ? paramHttpServletRequest.getParameter("formName") : "Sample Form";
      localUserForms = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(str2)));
      this.userFormsService.updateUserComponentService(localArrayList1, localUserForms, str18, str6, str7, str8, str20, str21, str16);
    }
    catch (Exception localException2)
    {
      log.error("error while updating the compoenent");
    }
    saveHtml2CanvasImage(str15, str2);
    
    return "redirect:/user/getUserFormByFormId104.do?formId=" + str2;
  }
  
  @RequestMapping(value={"/user/getUserFormByFormId104.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserFormByFormId104(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
	 
    ModelAndView localModelAndView = new ModelAndView("UserTemplate104");
    Object localObject = new ArrayList();
    UserForms localUserForms = new UserForms();
    String str = null;
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    try
    {
      str = paramHttpServletRequest.getParameter("formId");
      localUserForms = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(str)));
      paramHttpServletRequest.setAttribute("imgType", localUserForms.getFormImgType());
      localObject = this.userFormsComponentsService.getUserFormComponentListByUserFormId(Long.valueOf(Long.parseLong(str)));
    }
    catch (Exception localException)
    {
      log.error("error while getting the tempalte 104 compoenent");
      localModelAndView = new ModelAndView("ErrorPage");
    }
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("newUserFormComponents", localObject);
    localModelAndView.addObject("userForm", localUserForms);
    return localModelAndView;
  }
  @RequestMapping(value={"/user/userUpdateFormList.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserUpdateFormList(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("UserWelcomeHomePage");
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    List localList = null;
    if (localUserIfs != null) {
      localList = this.userFormsService.getUserActiveForms(localUserIfs);
    }
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("userForms", localList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/updateAdminTemplate104.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String updateAdminTemplate104(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("Template104");
    String str1 = paramHttpServletRequest.getParameter("headerText");
    String str2 = "form";
    String str3 = "admin";
    String str4 = null;
    String str5 = null;
    
    ArrayList localArrayList1 = new ArrayList();
    String str6 = paramHttpServletRequest.getParameter("saveLogo");
    String str7 = paramHttpServletRequest.getParameter("saveImage");
    str7 = str6 != null ? str6 : str7;
    String str8 = paramHttpServletRequest.getParameter("saveLogoLeft");
    String str9 = paramHttpServletRequest.getParameter("saveLogoBottomLeft");
    String str10 = paramHttpServletRequest.getParameter("saveLogoBottomRight");
    String str11 = paramHttpServletRequest.getParameter("saveLogoBottomMid");
    String str12 = paramHttpServletRequest.getParameter("saveLogoBottomCenter");
    String str13 = paramHttpServletRequest.getParameter("saveLogoBottomLast");    
    localArrayList1.add(str7);
    localArrayList1.add(str8);
    localArrayList1.add(str9);
    localArrayList1.add(str10);
    localArrayList1.add(str11);
    localArrayList1.add(str12);
    localArrayList1.add(str13);
    
    log.info("file path:" + IMAGE_LIBRARY_PATH);
    saveImages(localArrayList1, str2, str3);
    String str14 = paramHttpServletRequest.getParameter("imgType");
    ArrayList localArrayList2 = new ArrayList();
    ComponentObjDTO localComponentObjDTO = null;
    
    Long localLong = null;
    Template localTemplate = null;
    String str16 = null;
    int i = 70;
    for (int j = 0; j < i; j++)
    {
      String str17 = "component" + j;
      str16 = "componentId" + j;
      String str15 = paramHttpServletRequest.getParameter(str17) != null ? paramHttpServletRequest.getParameter(str17) : null;
      localLong = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
      localComponentObjDTO = new ComponentObjDTO(str15, localLong);
      localArrayList2.add(localComponentObjDTO);
    }
    str16 = "componentId70";
    localLong = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
    localComponentObjDTO = new ComponentObjDTO(str8, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str16 = "componentId71";
    localLong = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
    localComponentObjDTO = new ComponentObjDTO(str9, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str16 = "componentId72";
    localLong = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
    localComponentObjDTO = new ComponentObjDTO(str10, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str16 = "componentId73";
    localLong = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
    localComponentObjDTO = new ComponentObjDTO(str11, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str16 = "componentId74";
    localLong = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
    localComponentObjDTO = new ComponentObjDTO(str12, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str16 = "componentId75";
    localLong = paramHttpServletRequest.getParameter(str16) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str16))) : null;
    localComponentObjDTO = new ComponentObjDTO(str13, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    localTemplate = this.templateService.getCompnentsByTemplateId(TemplateConstant.TEMPLATE_104);
    this.templateService.updateTemplateComponentService(localArrayList2, localTemplate, str1, str7, str4, str5, str14);
    
    return "redirect:/admin/navigateTemplate104.do";
  }
  
  public void saveHtml2CanvasImage(String paramString1, String paramString2)
  {
    log.info("compID::" + paramString2);
    FileOutputStream localFileOutputStream = null;
    try
    {
      if ((paramString1 != null) && (paramString1.length() > 0))
      {
        byte[] arrayOfByte = Base64.decode(paramString1.substring(22));
        String str = IMAGE_LIBRARY_PATH + paramString2 + "logo.png";
        log.info(str);
        localFileOutputStream = new FileOutputStream(str);
        localFileOutputStream.write(arrayOfByte);
      }
      return;
    }
    catch (Exception localException)
    {
      log.info("Error occurred while uploading the image: " + localException);
    }
    finally
    {
      try
      {
        if (localFileOutputStream != null) {
          localFileOutputStream.close();
        }
      }
      catch (IOException localIOException3)
      {
        java.util.logging.Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, localIOException3);
      }
    }
  }  
  public void saveImages(List<String> paramList, String paramString1, String paramString2)
  {
    try
    {
      for (String str : paramList) {
        if ((str != "") && (str != null))
        {
          File localFile = new File(IMAGE_LIBRARY_PATH + str);
          byte[] arrayOfByte = new byte[(int)localFile.length()];
          try
          {
            FileInputStream localFileInputStream = new FileInputStream(localFile);
            localFileInputStream.read(arrayOfByte);
            localFileInputStream.close();
          }
          catch (Exception localException2)
          {
            log.info("Error occurred : " + localException2);
            localException2.printStackTrace();
          }
          if ((!this.documentService.isImageExist(str).booleanValue()) && (str != null) && (str != "")) {
            this.documentService.saveImage(str, arrayOfByte, paramString1, paramString2);
          }
        }
      }
    }
    catch (Exception localException1)
    {
      log.info("Error occurred while uploading the image: " + localException1);
    }
  }  
  public void resizeUserForm(List<UserFormsComponents> paramList, ModelAndView paramModelAndView)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    ArrayList localArrayList4 = new ArrayList();
    ArrayList localArrayList5 = new ArrayList();
    int i = 1;
    if (paramList.size() > 36) {
      i = 36;
    } else {
      i = paramList.size();
    }
    for (int j = 0; j < i; j++) {
      if ((j >= 4) && (j < 6)) {
        localArrayList1.add(paramList.get(j));
      } else if ((j >= 20) && (j < 22)) {
        localArrayList2.add(paramList.get(j));
      } else if (((j >= 6) && (j < 20)) || ((j >= 22) && (j < 36) && 
        (((UserFormsComponents)paramList.get(j)).getComponent().getComponentName().length() > 1))) {
        localArrayList3.add(paramList.get(j));
      }
    }
    float j = 1;
    if (localArrayList3.size() > 28) {
      j = 28;
    } else {
      j = localArrayList3.size();
    }
    for (int k = 0; k < j; k++) {
      if (j % 2 == 0)
      {
        if ((k >= 0) && (k < j / 2)) {
          localArrayList4.add(localArrayList3.get(k));
        } else if ((k >= j / 2) && (k < j)) {
          localArrayList5.add(localArrayList3.get(k));
        }
      }
      else if ((k >= 0) && (k <= j / 2)) {
        localArrayList4.add(localArrayList3.get(k));
      } else if ((k > j / 2) && (k < j)) {
        localArrayList5.add(localArrayList3.get(k));
      }
    }
    paramModelAndView.addObject("leftList", localArrayList4);
    paramModelAndView.addObject("rightList", localArrayList5);
    paramModelAndView.addObject("leftheader", localArrayList1);
    paramModelAndView.addObject("rightheader", localArrayList2);
  }
  
  public void resizeAdminForm(List<Components> paramList, ModelAndView paramModelAndView)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    ArrayList localArrayList4 = new ArrayList();
    ArrayList localArrayList5 = new ArrayList();
    int i = 1;
    if (paramList.size() > 36) {
      i = 36;
    } else {
      i = paramList.size();
    }
    for (int j = 0; j < i; j++) {
      if ((j >= 4) && (j < 6)) {
        localArrayList1.add(paramList.get(j));
      } else if ((j >= 20) && (j < 22)) {
        localArrayList2.add(paramList.get(j));
      } else if (((j >= 6) && (j < 20)) || ((j >= 22) && (j < 36) && 
        (((Components)paramList.get(j)).getComponentName().length() > 1))) {
        localArrayList3.add(paramList.get(j));
      }
    }
    int j = 1;
    if (localArrayList3.size() > 28) {
      j = 28;
    } else {
      j = localArrayList3.size();
    }
    for (int k = 0; k < j; k++) {
      if (j % 2 == 0)
      {
        if ((k >= 0) && (k < j / 2)) {
          localArrayList4.add(localArrayList3.get(k));
        } else if ((k >= j / 2) && (k < j)) {
          localArrayList5.add(localArrayList3.get(k));
        }
      }
      else if ((k >= 0) && (k <= j / 2)) {
        localArrayList4.add(localArrayList3.get(k));
      } else if ((k > j / 2) && (k < j)) {
        localArrayList5.add(localArrayList3.get(k));
      }
    }
    paramModelAndView.addObject("leftList", localArrayList4);
    paramModelAndView.addObject("rightList", localArrayList5);
    paramModelAndView.addObject("leftheader", localArrayList1);
    paramModelAndView.addObject("rightheader", localArrayList2);
  }
  
  public void divideComponentAdmin3(List<Components> paramList, ModelAndView paramModelAndView)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    ArrayList localArrayList4 = new ArrayList();
    ArrayList localArrayList5 = new ArrayList();
    ArrayList localArrayList6 = new ArrayList();
    ArrayList localArrayList7 = new ArrayList();
    int i = 1;
    if (paramList.size() > 73) {
      i = 73;
    } else {
      i = paramList.size();
    }
    for (int j = 0; j < i; j++) {
      if (j == 0) {
        localArrayList1.add(paramList.get(j));
      } else if ((j > 0) && (j <= 3)) {
        localArrayList2.add(paramList.get(j));
      } else if ((j > 3) && (j <= 13)) {
        localArrayList3.add(paramList.get(j));
      } else if ((j > 13) && (j <= 28)) {
        localArrayList5.add(paramList.get(j));
      } else if ((j > 28) && (j <= 37)) {
        localArrayList4.add(paramList.get(j));
      } else if ((j > 37) && (j <= 68)) {
        localArrayList6.add(paramList.get(j));
      } else if ((j > 68) && (j <= 72)) {
        localArrayList7.add(paramList.get(j));
      }
    }
    paramModelAndView.addObject("detailComponent", localArrayList1);
    paramModelAndView.addObject("topComponents", localArrayList2);
    paramModelAndView.addObject("topLeftComponents", localArrayList3);
    paramModelAndView.addObject("topRightComponents", localArrayList4);
    paramModelAndView.addObject("bottomLeftComponents", localArrayList5);
    paramModelAndView.addObject("bottomRightComponents", localArrayList6);
    paramModelAndView.addObject("imageComponents", localArrayList7);
  }
  
  public void divideComponentUser3(List<UserFormsComponents> paramList, ModelAndView paramModelAndView)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    ArrayList localArrayList4 = new ArrayList();
    ArrayList localArrayList5 = new ArrayList();
    ArrayList localArrayList6 = new ArrayList();
    ArrayList localArrayList7 = new ArrayList();
    int i = 1;
    if (paramList.size() > 73) {
      i = 73;
    } else {
      i = paramList.size();
    }
    for (int j = 0; j < i; j++) {
      if (j == 0) {
        localArrayList1.add(paramList.get(j));
      } else if ((j > 0) && (j <= 3)) {
        localArrayList2.add(paramList.get(j));
      } else if ((j > 3) && (j <= 13)) {
        localArrayList3.add(paramList.get(j));
      } else if ((j > 13) && (j <= 28)) {
        localArrayList5.add(paramList.get(j));
      } else if ((j > 28) && (j <= 37)) {
        localArrayList4.add(paramList.get(j));
      } else if ((j > 37) && (j <= 68)) {
        localArrayList6.add(paramList.get(j));
      } else if ((j > 68) && (j <= 72)) {
        localArrayList7.add(paramList.get(j));
      }
    }
    paramModelAndView.addObject("detailUserFormComponent", localArrayList1);
    paramModelAndView.addObject("topUserFormComponents", localArrayList2);
    paramModelAndView.addObject("topLeftUserFormComponents", localArrayList3);
    paramModelAndView.addObject("topRightUserFormComponents", localArrayList4);
    paramModelAndView.addObject("bottomLeftUserFormComponents", localArrayList5);
    paramModelAndView.addObject("bottomRightUserFormComponents", localArrayList6);
    paramModelAndView.addObject("imageUserFormComponents", localArrayList7);
  }
  
  @RequestMapping(value={"/admin/getComponents.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView getComponents(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("ShowComponents");
    Long localLong = Long.valueOf(1L);
    List localList = this.templateService.getComponents(localLong);
    ArrayList localArrayList = new ArrayList();
    Collections.sort(localList, new Comparator<Components>()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentName().compareTo(paramAnonymousComponents2.getComponentName());
      }
    });
//    for (Components localComponents : localList) {
//      if (localComponents.getTemplateComponent().getStatus().getStatusId().longValue() != 3L) {
//        localArrayList.add(localComponents);
//      }
//    }
    localModelAndView.addObject("componentsList", localArrayList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/deleteComponent.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView deleteComponent(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ModelAndView localModelAndView = new ModelAndView("ShowComponents");
    Long localLong = Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter("cid")));
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    List localList = this.componentsService.deleteTemplateComponent(localLong, Long.valueOf(1L), localUserIfs);
    ArrayList localArrayList = new ArrayList();
    Collections.sort(localList, new Comparator()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentName().compareTo(paramAnonymousComponents2.getComponentName());
      }

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
//    for (Components localComponents : localList) {
//      if (localComponents.getTemplateComponent().getStatus().getStatusId().longValue() != 3L) {
//        localArrayList.add(localComponents);
//      }
//    }
    localModelAndView.addObject("componentsList", localArrayList);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/user/navigateSearchUser.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateSearchUser(HttpServletRequest paramHttpServletRequest)
  {
    String str1 = "1";
    if (paramHttpServletRequest.getParameter("d-4037936-p") != null) {
      str1 = paramHttpServletRequest.getParameter("d-4037936-p");
    }
    String str2 = paramHttpServletRequest.getParameter("searchString");
    ModelAndView localModelAndView = new ModelAndView("UserDefinedTemplate");
    List localList1 = this.userIfsService.searchUsersByName(str2);
    long l = (Long.valueOf(str1).longValue() - 1L) * 12L;
    List localList2 = null;
    if (!localList1.isEmpty()) {
      localList2 = this.userIfsService.getUserForm(((UserIfs)localList1.get((int)l)).getUserId());
    }
    localModelAndView.addObject("searchString", str2);
    localModelAndView.addObject("users", localList1);
    localModelAndView.addObject("pageId1", str1);
    localModelAndView.addObject("userId", this.userIfsService.getIdOrderByUserName());
    localModelAndView.addObject("userForms", localList2);
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/checkCustomerMileageText.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView checkCustomerMileageText(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = new ModelAndView("CustomerMileage");
    String str = paramHttpServletRequest.getParameter("checkString");
    Font localFont = FontFactory.getFont("myriadproreg");
    BaseFont localBaseFont = localFont.getCalculatedBaseFont(false);
    localModelAndView.addObject("checkString", Float.valueOf(localBaseFont.getWidthPoint(str, 11.0F)));
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/checkCustomerMileageTextFor103.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView checkCustomerMileageTextforTemplate103(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = new ModelAndView("CustomerMileage");
    String str = paramHttpServletRequest.getParameter("checkString");
    Font localFont = FontFactory.getFont("myriadproreg");
    BaseFont localBaseFont = localFont.getCalculatedBaseFont(false);
    localModelAndView.addObject("checkString", Float.valueOf(localBaseFont.getWidthPoint(str, 11.0F)));
    return localModelAndView;
  }
  
  @RequestMapping(value={"/admin/checkCustomerMileageTextFor104.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView checkCustomerMileageTextforTemplate104(HttpServletRequest paramHttpServletRequest)
  {
    ModelAndView localModelAndView = new ModelAndView("CustomerMileage");
    String str = paramHttpServletRequest.getParameter("checkString");
    Font localFont = FontFactory.getFont("myriadproreg");
    BaseFont localBaseFont = localFont.getCalculatedBaseFont(false);
    System.out.println(localBaseFont.getWidthPoint("W", 11.0F));
    System.out.println(localBaseFont.getWidthPoint("_", 11.0F));
    if (localBaseFont.getWidthPoint(str, 11.0F) <= 575.0D) {
      localModelAndView.addObject("checkString", "yes");
    } else {
      localModelAndView.addObject("checkString", "no");
    }
    return localModelAndView;
  }
  
  public Map<Long, String> getComponetHeadingList(List<Components> paramList)
  {
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    Collections.sort(paramList, new Comparator<Components>()
    {
      public int compare(Components paramAnonymousComponents1, Components paramAnonymousComponents2)
      {
        return paramAnonymousComponents1.getComponentName().compareTo(paramAnonymousComponents2.getComponentName());
      }
    });
    for (Components localComponents : paramList) {
      if (localComponents.getTemplateComponent().getStatus().getStatusId().longValue() != 3L) {
        localLinkedHashMap.put(localComponents.getComponentId(), localComponents.getComponentName());
      }
    }
    return localLinkedHashMap;
  }
  
  public HashMap<Long, String> getUserComponetHeadingList(List<UserFormsComponents> paramList)
  {
    HashMap localHashMap = new HashMap();
    for (UserFormsComponents localUserFormsComponents : paramList) {
      if (localUserFormsComponents.getStatus().getStatusId().longValue() != 3L) {
        localHashMap.put(localUserFormsComponents.getUserFormsComponentId(), localUserFormsComponents.getComponent().getComponentName());
      }
    }
    return localHashMap;
  }
  
  @RequestMapping(value={"/admin/updateAdminTemplate105.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String updateAdminTemplate105(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    TemplateDTO localTemplateDTO = new TemplateDTO();
    localTemplateDTO.setHeaderText(paramHttpServletRequest.getParameter("headerText"));
    
    String str1 = paramHttpServletRequest.getParameter("saveLogo");
    String str2 = str1 != null ? str1 : paramHttpServletRequest.getParameter("saveImage");
    localTemplateDTO.setTemplateImage(str2);
    localTemplateDTO.setImageType(paramHttpServletRequest.getParameter("imgType"));
    
    String str3 = paramHttpServletRequest.getParameter("saveLogoLeft");
    String str4 = paramHttpServletRequest.getParameter("saveLogoBottomRight");
    String str5 = paramHttpServletRequest.getParameter("saveLogoBottomMid");
    String str6 = paramHttpServletRequest.getParameter("saveLogoBottomLast");
    String str7 = paramHttpServletRequest.getParameter("saveLogoBottomLeft");
    
    ArrayList localArrayList1 = new ArrayList();
    localArrayList1.add(str2);
    localArrayList1.add(str3);
    localArrayList1.add(str4);
    localArrayList1.add(str5);
    localArrayList1.add(str6);
    localArrayList1.add(str7);
    
    saveImages(localArrayList1, "form", "admin");
    
    localTemplateDTO.setLogoText(paramHttpServletRequest.getParameter("logoText"));
    
    ArrayList localArrayList2 = new ArrayList();
    ComponentObjDTO localComponentObjDTO = null;
    
    Long localLong = null;
    Template localTemplate = null;
    String str9 = null;
    for (int i = 0; i < 70; i++)
    {
      String str10 = "component" + i;
      str9 = "componentId" + i;
      String str8 = paramHttpServletRequest.getParameter(str10) != null ? paramHttpServletRequest.getParameter(str10) : null;
      localLong = paramHttpServletRequest.getParameter(str9) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str9))) : null;
      localComponentObjDTO = new ComponentObjDTO(str8, localLong);
      localArrayList2.add(localComponentObjDTO);
    }
    str9 = "componentId70";
    localLong = paramHttpServletRequest.getParameter(str9) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str9))) : null;
    localComponentObjDTO = new ComponentObjDTO(str3, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str9 = "componentId71";
    localLong = paramHttpServletRequest.getParameter(str9) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str9))) : null;
    localComponentObjDTO = new ComponentObjDTO(str4, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str9 = "componentId72";
    localLong = paramHttpServletRequest.getParameter(str9) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str9))) : null;
    localComponentObjDTO = new ComponentObjDTO(str5, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str9 = "componentId73";
    localLong = paramHttpServletRequest.getParameter(str9) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str9))) : null;
    localComponentObjDTO = new ComponentObjDTO(str6, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    str9 = "componentId74";
    localLong = paramHttpServletRequest.getParameter(str9) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str9))) : null;
    localComponentObjDTO = new ComponentObjDTO(str7, localLong);
    localArrayList2.add(localComponentObjDTO);
    
    localTemplate = this.templateService.getCompnentsByTemplateId(TemplateConstant.TEMPLATE_105);
    this.templateService.updateTemplate105ComponentService(localArrayList2, localTemplate, localTemplateDTO);
    
    ModelAndView localModelAndView = new ModelAndView("Template105");
    localModelAndView.addObject("template", localTemplate);
    
    return "redirect:/admin/navigateTemplate105.do";
  }
  
  @RequestMapping(value={"/user/updateUserFormByFormId105.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public String updateUserFormByFormId105(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
    ArrayList localArrayList1 = new ArrayList();
    ComponentObjDTO localComponentObjDTO = null;
    String str1 = null;
    UserForms localUserForms = new UserForms();
    Long localLong = null;
    String str2 = null;   
    String str3 = "form";
    String str4 = "user";
    
    String str5 = paramHttpServletRequest.getParameter("saveLogo");
    String str6 = paramHttpServletRequest.getParameter("saveImage");
    str6 = str5 != null ? str5 : str6;
    
    String str7 = paramHttpServletRequest.getParameter("saveLogoLeft");
    String str8 = paramHttpServletRequest.getParameter("saveLogoBottomRight");
    String str9 = paramHttpServletRequest.getParameter("saveLogoBottomMid");
    String str10 = paramHttpServletRequest.getParameter("saveLogoBottomLast");
    String str11 = paramHttpServletRequest.getParameter("saveLogoBottomLeft");
    
    String str12 = paramHttpServletRequest.getParameter("html2canvasImg");
    log.info("html2CanvasImg::" + str12);
    
    ArrayList localArrayList2 = new ArrayList();
    localArrayList2.add(str6);
    localArrayList2.add(str7);
    localArrayList2.add(str8);
    localArrayList2.add(str9);
    localArrayList2.add(str10);
    localArrayList2.add(str11);
    
    log.info("file path:" + IMAGE_LIBRARY_PATH);
    saveImages(localArrayList2, str3, str4);
    
    String str13 = paramHttpServletRequest.getParameter("logoText");
    
    String str14 = "";
    int i = 75;
    for (int j = 0; j < 70; j++) {
      try
      {
        String str16 = "component" + j;
        str14 = "componentId" + j;
        str1 = paramHttpServletRequest.getParameter(str16) != null ? paramHttpServletRequest.getParameter(str16) : null;
        localLong = paramHttpServletRequest.getParameter(str14) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str14))) : null;
        localComponentObjDTO = new ComponentObjDTO(str1, localLong);
        localArrayList1.add(localComponentObjDTO);
      }
      catch (Exception localException3)
      {
        log.error("Error while getting form data with component Id::" + j);
      }
    }
    try
    {
      str14 = "componentId70";
      localLong = paramHttpServletRequest.getParameter(str14) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str14))) : null;
      localComponentObjDTO = new ComponentObjDTO(str7, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str14 = "componentId71";
      localLong = paramHttpServletRequest.getParameter(str14) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str14))) : null;
      localComponentObjDTO = new ComponentObjDTO(str8, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str14 = "componentId72";
      localLong = paramHttpServletRequest.getParameter(str14) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str14))) : null;
      localComponentObjDTO = new ComponentObjDTO(str9, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str14 = "componentId73";
      localLong = paramHttpServletRequest.getParameter(str14) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str14))) : null;
      localComponentObjDTO = new ComponentObjDTO(str10, localLong);
      localArrayList1.add(localComponentObjDTO);
      
      str14 = "componentId74";
      localLong = paramHttpServletRequest.getParameter(str14) != null ? Long.valueOf(Long.parseLong(paramHttpServletRequest.getParameter(str14))) : null;
      localComponentObjDTO = new ComponentObjDTO(str11, localLong);
      localArrayList1.add(localComponentObjDTO);
    }
    catch (Exception localException1)
    {
      log.error("Error while getting form data with component Id::");
    }
    try
    {
      String str15 = paramHttpServletRequest.getParameter("headerText");
      String str17 = paramHttpServletRequest.getParameter("imgType");
      str2 = paramHttpServletRequest.getParameter("formId");
      
      String str18 = paramHttpServletRequest.getParameter("formName") != null ? paramHttpServletRequest.getParameter("formName") : "Sample Form";
      localUserForms = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(str2)));
      
      this.userFormsService.updateUserComponentService(localArrayList1, localUserForms, str15, str6, null, null, str17, str18, str13);
    }
    catch (Exception localException2)
    {
      log.error("error while updating the compoenent");
    }
    saveHtml2CanvasImage(str12, str2);
    
    return "redirect:/user/getUserFormByFormId105.do?formId=" + str2;
  }
  
  @RequestMapping(value={"/user/getUserFormByFormId105.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView getUserFormByFormId105(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
  {
	  
	 
    ModelAndView localModelAndView = new ModelAndView("UserTemplate105");
    Object localObject = new ArrayList();
    UserForms localUserForms = new UserForms();
    String str = null;
    UserIfs localUserIfs = (UserIfs)paramHttpServletRequest.getSession().getAttribute("user");
    try
    {
      str = paramHttpServletRequest.getParameter("formId");
      localUserForms = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(str)));
      paramHttpServletRequest.setAttribute("imgType", localUserForms.getFormImgType());
      localObject = this.userFormsComponentsService.getUserFormComponentListByUserFormId(Long.valueOf(Long.parseLong(str)));
    }
    catch (Exception localException)
    {
      log.error("error while getting the tempalte 104 compoenent");
      localModelAndView = new ModelAndView("ErrorPage");
    }
    localModelAndView.addObject("user", localUserIfs);
    localModelAndView.addObject("newUserFormComponents", localObject);
    localModelAndView.addObject("userForm", localUserForms);
    return localModelAndView;
  }
}
