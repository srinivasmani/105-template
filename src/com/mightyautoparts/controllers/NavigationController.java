package com.mightyautoparts.controllers;

import com.mightyautoparts.constants.TemplateConstant;

import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import com.mightyautoparts.services.masterTemplate.TemplateService;
import com.mightyautoparts.services.masterTemplate.UserFormsComponentsService;
import com.mightyautoparts.services.masterTemplate.UserFormsService;
import com.mightyautoparts.services.masterTemplate.UserIfsService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class NavigationController
{
  @Resource(name="userIfsService")
  private UserIfsService userIfsService;
  @Resource(name="userFormsService")
  private UserFormsService userFormsService;
  @Resource(name="templateService")
  private TemplateService templateService;
  @Resource(name="userFormsComponentsService")
  private UserFormsComponentsService userFormsComponentsService;
  private static final Logger log = Logger.getLogger(NavigationController.class);
  
  @RequestMapping(value={"/Login.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView doLoginGet(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView modelAndView = new ModelAndView("Welcome");
    UserIfs user = (UserIfs)request.getSession().getAttribute("user");
    if (user != null)
    {
      List<UserForms> userForms = this.userFormsService.getUserActiveForms(user);
      
      Collections.sort(userForms, new Comparator<UserForms>()
      {
    	public int compare(UserForms userForm1, UserForms userForm2)
        {
          return userForm1.getCreatedTime().compareTo(userForm2.getCreatedTime());
        }
      });
      modelAndView.addObject("user", user);
      modelAndView.addObject("userForms", userForms);
    }
    else
    {
      modelAndView = new ModelAndView("UserLoginGet");
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/Login.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST}, headers={"accept=*/*"})
  public ModelAndView doLogin(HttpServletRequest request, HttpServletResponse response, String username, String role)
  {
    if (log.isDebugEnabled()) {
      log.info("Entering doLogin(..)");
    }
    ModelAndView modelAndView = null;
    UserIfs user = null;
    if (request.getSession().getAttribute("user") != null)
    {
      user = (UserIfs)request.getSession().getAttribute("user");
    }
    else
    {
      user = this.userIfsService.getUserIdByName(username, role);
      request.getSession().setAttribute("user", user);
    }
    List<UserForms> userForms = new ArrayList();
    if (user != null)
    {
      userForms = this.userFormsService.getUserActiveForms(user);
      Collections.sort(userForms, new Comparator<UserForms>()
      {
        public int compare(UserForms userForm1, UserForms userForm2)
        {
          return userForm1.getCreatedTime().compareTo(userForm2.getCreatedTime());
        }
      });
      modelAndView = new ModelAndView("Welcome");
      modelAndView.addObject("user", user);
      modelAndView.addObject("userForms", userForms);
    }
    else
    {
      modelAndView = new ModelAndView("UserLoginGet");
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/admin/editNavTemplateIfs.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateTemplate3(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView modelAndView = new ModelAndView("editIfsTemplate");
    Long templateId = Long.valueOf(2L);
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    request.setAttribute("imgType", template.getImageType());
    modelAndView.addObject("template", template);
    List<Components> sorted = template.getComponents();
    Map<Long, String> compDrp = new LinkedHashMap();
    Collections.sort(sorted, new Comparator<Components>()
    {
      public int compare(Components c1, Components c2)
      {
        return c1.getComponentId().compareTo(c2.getComponentId());
      }
    });
    for (Components component : sorted) {
      compDrp.put(component.getComponentId(), component.getComponentName());
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/user/userEditTemplateIfs.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateEditUserTemplateIfs(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView modelAndView = new ModelAndView("editUserIfsTemplate");
    Long templateId = Long.valueOf(2L);
    UserForms userForm = null;
    int count = 1;
    try
    {
      List<UserFormsComponents> newUserFormComponents = new ArrayList();
      Template template = this.templateService.getCompnentsByTemplateId(templateId);
      String userFormId = request.getParameter("userFormId");
      
      userForm = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(userFormId)));
      newUserFormComponents = userForm.getUserFormComponents();
      Map<Long, String> compDrp = new LinkedHashMap();
      for (UserFormsComponents component : newUserFormComponents) {
        compDrp.put(component.getComponent().getComponentId(), component.getComponent().getComponentName());
      }
      if (newUserFormComponents != null) {
        count = newUserFormComponents.size();
      }
      count++;
      modelAndView.addObject("count", Integer.valueOf(count));
      modelAndView.addObject("template", template);
      modelAndView.addObject("userForm", userForm);
      request.setAttribute("imgType", userForm.getFormImgType());
      modelAndView.addObject("newUserFormComponents", newUserFormComponents);
    }
    catch (Exception e)
    {
      modelAndView = new ModelAndView("ErrorPage");
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/user/userTemplateIfs.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView navigateUserTemplateIfs(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView modelAndView = new ModelAndView("userTemplateIfs");
    Long templateId = Long.valueOf(2L);
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    request.setAttribute("imgType", "circle");
    modelAndView.addObject("template", template);
    
    return modelAndView;
  }
  
  @RequestMapping(value={"/admin/editTemplate104.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView editTemplate104(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView modelAndView = new ModelAndView("editTemplate104");
    Long templateId = TemplateConstant.TEMPLATE_104;
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    request.setAttribute("imgType", template.getImageType());
    modelAndView.addObject("template", template);
    List<Components> sorted = template.getComponents();
    Map<Long, String> compDrp = new LinkedHashMap();
    Collections.sort(sorted, new Comparator <Components>()
    {
      public int compare(Components c1, Components c2)
      {
        return c1.getComponentId().compareTo(c2.getComponentId());
      }
    });
    for (Components component : sorted) {
      compDrp.put(component.getComponentId(), component.getComponentName());
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/admin/editTemplate105.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView editTemplate105(HttpServletRequest request, HttpServletResponse response)
  {
	 
    ModelAndView modelAndView = new ModelAndView("editTemplate105");
    Long templateId = TemplateConstant.TEMPLATE_105;
    Template template = this.templateService.getCompnentsByTemplateId(templateId);
    request.setAttribute("imgType", template.getImageType());
    modelAndView.addObject("template", template);
    List<Components> sorted = template.getComponents();
    Map<Long, String> compDrp = new LinkedHashMap();
    Collections.sort(sorted, new Comparator <Components>()
    {
      public int compare(Components c1, Components c2)
      {
        return c1.getComponentId().compareTo(c2.getComponentId());
      }
    });
    for (Components component : sorted) {
      compDrp.put(component.getComponentId(), component.getComponentName());
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/Logout.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView doLogOut(HttpServletRequest request, HttpServletResponse response)
  {
    request.getSession().removeAttribute("user");
    ModelAndView mav = new ModelAndView("UserLoginGet");
    return mav;
  }
  
  @RequestMapping(value={"/user/userEditTemplate104ByFormId.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView userEditTemplateByFormId(HttpServletRequest request, HttpServletResponse response)
  {
	  
    ModelAndView modelAndView = new ModelAndView("editUserTemplate104");
    Long templateId = TemplateConstant.TEMPLATE_104;
    UserForms userForm = null;
    List<UserFormsComponents> newUserFormComponents = new ArrayList();
    String userFormId = request.getParameter("formId");
    try
    {
      userForm = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(userFormId)));
      newUserFormComponents = this.userFormsComponentsService.getUserFormComponentListByUserFormId(Long.valueOf(Long.parseLong(userFormId)));
    }
    catch (Exception e)
    {
      log.error("error while getting the user component data of template 104");
    }
    modelAndView.addObject("userForm", userForm);
    request.setAttribute("imgType", userForm.getFormImgType());
    modelAndView.addObject("newUserFormComponents", newUserFormComponents);
    return modelAndView;
  }
  
  @RequestMapping(value={"/user/createUserComponentForm104.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView createUserComponentForm104(HttpServletRequest request, HttpServletResponse response)
  {
	
    ModelAndView modelAndView = new ModelAndView("UserTemplate104");
    List<UserFormsComponents> userFormComponents = new ArrayList();
    List<UserFormsComponents> newUserFormComponents = new ArrayList();
    UserForms userForm = null;
    try
    {
      Template template = this.templateService.getCompnentsByTemplateId(TemplateConstant.TEMPLATE_104);
      UserIfs user = (UserIfs)request.getSession().getAttribute("user");
      userForm = this.userFormsService.getUserDeFaultIfsFormFor104(user.getUserId(), template);
      if (userForm != null)
      {
        userFormComponents = userForm.getUserFormComponents();
        int count = userFormComponents.size();
        for (int i = 0; i < count; i++) {
          newUserFormComponents.add(userFormComponents.get(i));
        }
      }
      modelAndView.addObject("userForm", userForm);
      modelAndView.addObject("user", user);
      modelAndView.addObject("templateId", TemplateConstant.TEMPLATE_104);
      modelAndView.addObject("template", template);
      request.setAttribute("imgType", "circle");
      request.setAttribute("imgType", userForm.getFormImgType());
      modelAndView.addObject("newUserFormComponents", newUserFormComponents);
    }
    catch (Exception e)
    {
      log.error("error while creating the form 104 ::" + e);
      modelAndView = new ModelAndView("ErrorPage");
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/user/createUserComponentForm105.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView createUserComponentForm105(HttpServletRequest request, HttpServletResponse response)
  {
//	  return null;
	  
    ModelAndView modelAndView = new ModelAndView("UserTemplate105");
    List<UserFormsComponents> userFormComponents = new ArrayList();
    List<UserFormsComponents> newUserFormComponents = new ArrayList();
    UserForms userForm = null;
    try
    {
      Template template = this.templateService.getCompnentsByTemplateId(TemplateConstant.TEMPLATE_105);
      UserIfs user = (UserIfs)request.getSession().getAttribute("user");
      userForm = this.userFormsService.getUserDeFaultIfsFormFor105(user.getUserId(), template);
      if (userForm != null)
      {
        userFormComponents = userForm.getUserFormComponents();
        int count = userFormComponents.size();
        for (int i = 0; i < count; i++) {
          newUserFormComponents.add(userFormComponents.get(i));
        }
      }
      modelAndView.addObject("userForm", userForm);
      modelAndView.addObject("user", user);
      modelAndView.addObject("templateId", TemplateConstant.TEMPLATE_105);
      modelAndView.addObject("template", template);
      request.setAttribute("imgType", "circle");
      request.setAttribute("imgType", userForm.getFormImgType());
      modelAndView.addObject("newUserFormComponents", newUserFormComponents);
    }
    catch (Exception e)
    {
      log.error("error while creating the form 105 ::" + e);
      modelAndView = new ModelAndView("ErrorPage");
    }
    return modelAndView;
  }
  
  @RequestMapping(value={"/user/userEditTemplate105ByFormId.do"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, headers={"accept=*/*"})
  public ModelAndView userEditTemplate105ByFormId(HttpServletRequest request, HttpServletResponse response)
  {
	 
    ModelAndView modelAndView = new ModelAndView("editUserTemplate105");
    UserForms userForm = null;
    List<UserFormsComponents> newUserFormComponents = new ArrayList();
    String userFormId = request.getParameter("formId");
    try
    {
      userForm = this.userFormsService.getUserFormsById(Long.valueOf(Long.parseLong(userFormId)));
      newUserFormComponents = this.userFormsComponentsService.getUserFormComponentListByUserFormId(Long.valueOf(Long.parseLong(userFormId)));
    }
    catch (Exception e)
    {
      log.error("error while getting the user component data of template 105");
    }
    modelAndView.addObject("userForm", userForm);
    request.setAttribute("imgType", userForm.getFormImgType());
    modelAndView.addObject("newUserFormComponents", newUserFormComponents);
    return modelAndView;
  }
}
