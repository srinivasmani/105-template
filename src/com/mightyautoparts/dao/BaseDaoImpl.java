package com.mightyautoparts.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class BaseDaoImpl
  extends HibernateDaoSupport
  implements BaseDao
{
  @Autowired
  public void init(SessionFactory factory)
  {
    setSessionFactory(factory);
  }
  
  public void save(Object obj)
  {
    getHibernateTemplate().save(obj);
  }
  
  public void update(Object obj)
  {
    getHibernateTemplate().saveOrUpdate(obj);
  }
  
  public void find(String queryString, Class clazz)
  {
    getHibernateTemplate().find(queryString);
  }
  
  public void updateObject(Object obj)
  {
    getHibernateTemplate().update(obj);
  }
}
