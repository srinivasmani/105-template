package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.DocumentDao;
import com.mightyautoparts.dto.DocumentDTO;
import com.mightyautoparts.entities.masterTemplate.Document;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("documentDao")
public class DocumentDaoImpl
  extends BaseDaoImpl
  implements DocumentDao
{
  private static final Logger log = Logger.getLogger(DocumentDaoImpl.class);
  
  public List<DocumentDTO> getDocumentItem()
  {
    Criteria criteria = getSession().createCriteria(Document.class);
    criteria.add(Restrictions.eq("documentType", "repository"));
    criteria.add(Restrictions.eq("documentDescription", "active"));
    criteria.add(Restrictions.ne("documentName", "undefined"));
    criteria.setProjection(Projections.projectionList().add(Projections.property("documentName")).add(Projections.property("documentId")));
    List<DocumentDTO> list = new ArrayList();
    list = criteria.list();
    return list;
  }
  
  public List<Document> getAllDocumentItem()
  {
    Criteria criteria = getSession().createCriteria(Document.class);
    List<Document> list = criteria.list();
    return list;
  }
  
  public Boolean removeImageDB(Long imageId)
  {
    Session session = getSession();
    Document document = null;
    try
    {
      document = (Document)session.createCriteria(Document.class).add(Restrictions.eq("documentId", imageId)).uniqueResult();
      session.delete(document);
    }
    catch (HibernateException e)
    {
      log.info("document could not be updated..");
      log.info("Error occured while setting document description in DocumentDaoImpl.java:" + e);
      e.printStackTrace();
    }
    return Boolean.valueOf(true);
  }
  
  public void saveImageDB(Document document)
  {
    try
    {
      save(document);
    }
    catch (Exception e)
    {
      log.info("document could not be saved..");
      log.info("Error occured while saving DocumentDaoImpl.java :" + e);
    }
  }
  
  public Boolean isRepoImageExist(String image)
  {
    Boolean isExists = Boolean.valueOf(false);
    Document document = null;
    Criteria criteria = getSession().createCriteria(Document.class);
    criteria.add(Restrictions.eq("documentType", "repository"));
    criteria.add(Restrictions.eq("documentName", image));
    try
    {
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      document = (Document)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("No data with image name" + image + " and type repository..");
      log.info("Error thrown as :" + e + " inDocumentDaoImpl.java");
      document = null;
    }
    if (document != null) {
      isExists = Boolean.valueOf(true);
    }
    return isExists;
  }
}
