package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.ComponentDao;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("componentDao")
public class ComponentDaoImpl
  extends BaseDaoImpl
  implements ComponentDao
{
  private static final Logger log = Logger.getLogger(ComponentDaoImpl.class);
  
  public Components saveComponent(Components component)
  {
    try
    {
      save(component);
    }
    catch (Exception e)
    {
      log.info("component could not be saved..");
      log.info("Error occured while saving component in ComponentDaoImpl.java: " + e);
    }
    return component;
  }
  
  public Components getComponentById(Long componentId)
  {
    Components component = null;
    try
    {
      Criteria criteria = getSession().createCriteria(Components.class);
      
      criteria.add(Restrictions.idEq(componentId));
      component = (Components)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      if ((e instanceof RuntimeException))
      {
        log.info("RuntimeException .." + e + " message:" + e.getMessage());
        Throwable ne = e;
        while (ne != null) {
          if ((ne instanceof SQLException))
          {
            SQLException sqle = (SQLException)ne;
            ne = sqle.getNextException();
            log.info("SQLException .." + sqle);
            log.info("Nested SQLException .." + ne);
            log.info("SQLException Message .." + sqle.getMessage());
          }
          else
          {
            ne = ne.getCause();
          }
        }
      }
      log.info("component could not be retrieved..");
      log.info("Error occured while retrieving component by id in ComponentDaoImpl.java: " + e);
    }
    return component;
  }
  
  public Components updateComponent(Components currentComponents)
  {
    try
    {
      return (Components)getSession().merge(currentComponents);
    }
    catch (Exception e)
    {
      log.info("component could not be updated through merge..");
      log.info("Error occured while updating component using merge in ComponentDaoImpl.java: " + e);
    }
    return currentComponents;
  }
  
  public void deleteComponent(Long componentId)
  {
    Components component = getComponentById(componentId);
    if (component != null)
    {
      Long position = Long.valueOf(Long.parseLong(component.getTemplateComponent().getPosition().getPositionName()));
      if (position.longValue() > 24L)
      {
        if (component.getSubComponents() != null)
        {
          List<SubComponentsOptions> subComponentsOptions = component.getSubComponents().getSubComponentsOptions();
          for (SubComponentsOptions subComponentsOption : subComponentsOptions)
          {
            String hql = "delete from SubComponentsOptions where subComponentOptionsId = :subComponentOptionsId";
            Query query = getSession().createQuery(hql);
            query.setLong("subComponentOptionsId", subComponentsOption.getSubComponentOptionsId().longValue()).executeUpdate();
            
            hql = "delete from Options where optionId = :optionId";
            query = getSession().createQuery(hql);
            query.setLong("optionId", subComponentsOption.getOptions().getOptionId().longValue()).executeUpdate();
            
            hql = "delete from SubComponents where subComponentId = :subComponentId";
            query = getSession().createQuery(hql);
            query.setLong("subComponentId", subComponentsOption.getSubComponents().getComponentId().longValue()).executeUpdate();
          }
        }
        getSession().delete(component);
      }
      else
      {
        component.setComponentName("");
        if (component.getSubComponents() != null)
        {
          component.getSubComponents().setLabel("");
          List<SubComponentsOptions> subComponentsOptions = component.getSubComponents().getSubComponentsOptions();
          for (SubComponentsOptions subComponentsOption : subComponentsOptions)
          {
            String hql = "delete from SubComponentsOptions where subComponentOptionsId = :subComponentOptionsId";
            Query query = getSession().createQuery(hql);
            query.setLong("subComponentOptionsId", subComponentsOption.getSubComponentOptionsId().longValue()).executeUpdate();
            
            hql = "delete from Options where optionId = :optionId";
            query = getSession().createQuery(hql);
            query.setLong("optionId", subComponentsOption.getOptions().getOptionId().longValue()).executeUpdate();
          }
        }
        Criteria criteria = getSession().createCriteria(Status.class);
        criteria.add(Restrictions.idEq(Long.valueOf(3L)));
        Status status = (Status)criteria.uniqueResult();
        component.getTemplateComponent().setStatus(status);
        update(component);
      }
    }
  }
}
