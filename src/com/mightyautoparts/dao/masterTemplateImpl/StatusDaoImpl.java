package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.StatusDao;
import com.mightyautoparts.entities.masterTemplate.Status;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository("statusDao")
public class StatusDaoImpl
  extends BaseDaoImpl
  implements StatusDao
{
  public Status getStatusById(Long statusId)
  {
    Status status = (Status)getSession().load(Status.class, statusId);
    return status;
  }
}
