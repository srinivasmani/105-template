package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.PositionDao;
import com.mightyautoparts.entities.masterTemplate.Position;
import org.springframework.stereotype.Repository;

@Repository("positionDao")
public class PositionDaoImpl
  extends BaseDaoImpl
  implements PositionDao
{
  public Position createPosition(Long positionNum)
  {
    Position position = new Position();
    position.setPositionDescription(positionNum.toString());
    position.setPositionName(positionNum.toString());
    save(position);
    return position;
  }
}
