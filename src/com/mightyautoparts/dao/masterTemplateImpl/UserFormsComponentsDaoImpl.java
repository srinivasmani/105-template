package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.UserFormsComponentsDao;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("userFormsComponentsDao")
@Transactional
public class UserFormsComponentsDaoImpl
  extends BaseDaoImpl
  implements UserFormsComponentsDao
{
  private static final Logger log = Logger.getLogger(UserFormsComponentsDaoImpl.class);
  
  public UserFormsComponents getUserFormComponentById(Long formComponentId)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserFormsComponents.class);
      criteria.add(Restrictions.idEq(formComponentId));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      return (UserFormsComponents)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("UserFormsComponents could not be retrieved..");
      log.info("Error occured while retrieving UserFormsComponents in UserFormsComponentsDaoImpl.java:" + e);
      log.info("returning null UserFormsComponentsDaoImpl.java, getUserFormComponentById():" + e);
    }
    return null;
  }
  
  public UserFormsComponents getUserFormCompByParent(Long componentId, Long userFormId)
  {
    UserFormsComponents userFormComp = null;
    try
    {
      Query query = getSession().createQuery("from UserFormsComponents ufc where ufc.parentComponentId = :componentID and ufc.userForms.userFormId = :formID");
      
      query.setLong("componentID", componentId.longValue());
      query.setLong("formID", userFormId.longValue());
      List<UserFormsComponents> userFormsComponents = query.list();
      if (userFormsComponents.size() > 0) {}
      return (UserFormsComponents)userFormsComponents.get(0);
    }
    catch (Exception e)
    {
      log.info("UserFormsComponents could not be retrieved..");
      log.info("Error occured while retrieving UserFormsComponents in UserFormsComponentsDaoImpl.java:" + e);
      
      log.info("returning null UserFormsComponentsDaoImpl.java, getUserFormCompByParent():" + e);
    }
    return null;
  }
  
  public List<UserFormsComponents> getUserFormComponentListByUserFormId(Long userFormId)
  {
    try
    {
      return getSession().createCriteria(UserFormsComponents.class).addOrder(Order.asc("position")).add(Restrictions.eq("userForms.id", userFormId)).list();
    }
    catch (Exception e)
    {
      log.error("Error while getting userFormComponents by UserformId");
    }
    return null;
  }
  
  public void deleteChildComponent(Long componentId, Long userFormId)
  {
    Criteria criteria = getSession().createCriteria(UserFormsComponents.class);
    criteria.add(Restrictions.eq("parentComponentId", componentId));
    criteria.add(Restrictions.eq("userForms.userFormId", userFormId));
    UserFormsComponents userFormComp = (UserFormsComponents)criteria.uniqueResult();
    Components component = userFormComp.getComponent();
    if (userFormComp != null) {
      try
      {
        Long position = Long.valueOf(Long.parseLong(userFormComp.getPosition().getPositionName()));
        if (position.longValue() > 24L)
        {
          if (component.getSubComponents() != null)
          {
            List<SubComponentsOptions> subComponentsOptions = component.getSubComponents().getSubComponentsOptions();
            for (SubComponentsOptions subComponentsOption : subComponentsOptions)
            {
              String hql = "delete from SubComponentsOptions where subComponentOptionsId = :subComponentOptionsId";
              Query query = getSession().createQuery(hql);
              query.setLong("subComponentOptionsId", subComponentsOption.getSubComponentOptionsId().longValue()).executeUpdate();
              
              hql = "delete from Options where optionId = :optionId";
              query = getSession().createQuery(hql);
              query.setLong("optionId", subComponentsOption.getOptions().getOptionId().longValue()).executeUpdate();
              
              hql = "delete from SubComponents where subComponentId = :subComponentId";
              query = getSession().createQuery(hql);
              query.setLong("subComponentId", subComponentsOption.getSubComponents().getComponentId().longValue()).executeUpdate();
            }
          }
          getSession().delete(component);
        }
        else
        {
          component.setComponentName("");
          if (component.getSubComponents() != null)
          {
            component.getSubComponents().setLabel("");
            List<SubComponentsOptions> subComponentsOptions = component.getSubComponents().getSubComponentsOptions();
            for (SubComponentsOptions subComponentsOption : subComponentsOptions)
            {
              String hql = "delete from SubComponentsOptions where subComponentOptionsId = :subComponentOptionsId";
              Query query = getSession().createQuery(hql);
              query.setLong("subComponentOptionsId", subComponentsOption.getSubComponentOptionsId().longValue()).executeUpdate();
              
              hql = "delete from Options where optionId = :optionId";
              query = getSession().createQuery(hql);
              query.setLong("optionId", subComponentsOption.getOptions().getOptionId().longValue()).executeUpdate();
            }
          }
          Criteria criteriaStatus = getSession().createCriteria(Status.class);
          criteriaStatus.add(Restrictions.idEq(Long.valueOf(3L)));
          Status status = (Status)criteria.uniqueResult();
          component.getTemplateComponent().setStatus(status);
          update(component);
        }
      }
      catch (Exception e)
      {
        log.info("Error occured while deleting UserForms in UserFormsDaoImpl.java:" + e);
        e.printStackTrace();
      }
    }
  }
}
