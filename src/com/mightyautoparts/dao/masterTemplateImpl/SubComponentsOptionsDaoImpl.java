package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.SubComponentsOptionsDao;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("subComponentsOptionsDao")
public class SubComponentsOptionsDaoImpl
  extends BaseDaoImpl
  implements SubComponentsOptionsDao
{
  private static final Logger log = Logger.getLogger(SubComponentsOptionsDaoImpl.class);
  
  public SubComponentsOptions getSubComponentOptionsById(Long subOptionId)
  {
    SubComponentsOptions subOption = null;
    try
    {
      Criteria criteria = getSession().createCriteria(SubComponentsOptions.class);
      criteria.add(Restrictions.idEq(subOptionId));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      subOption = (SubComponentsOptions)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("SubComponentsOptions could not be retrieved..");
      log.info("Error occured while retrieving subcomponent in SubComponentsOptionsDaoImpl.java:" + e);
      log.info("returning null SubComponentsOptionsDaoImpl.java, getSubComponentOptionsById():" + e);
    }
    return subOption;
  }
}
