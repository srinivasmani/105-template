package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.UserDao;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl
  extends BaseDaoImpl
  implements UserDao
{
  private static final Logger log = Logger.getLogger(UserDaoImpl.class);
  
  public List<UserIfs> getAllUsers()
  {
    try
    {
      Criteria crit = getSession().createCriteria(UserIfs.class);
      crit.add(Restrictions.eq("userRole", "user"));
      crit.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      
      return crit.list();
    }
    catch (Exception e)
    {
      log.info("User list could not be retrieved..");
      log.info("Error occured while retrieving User list  in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getAllUsers():" + e);
    }
    return null;
  }
  
  public UserIfs getUser(Long userId)
  {
    try
    {
      return (UserIfs)getSession().load(UserIfs.class, userId);
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUser():" + e);
    }
    return null;
  }
  
  public List<UserForms> getUserForms()
  {
    try
    {
      Criteria crit = getSession().createCriteria(UserIfs.class);
      return crit.list();
    }
    catch (Exception e)
    {
      log.info("User list could not be retrieved..");
      log.info("Error occured while retrieving User list  in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUserForms():" + e);
    }
    return null;
  }
  
  public List<UserForms> getUserForms(Long userId)
  {
    try
    {
      UserIfs user = (UserIfs)getSession().load(UserIfs.class, userId);
      
      return user != null ? user.getUserForms() : null;
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUserForms():" + e);
    }
    return null;
  }
  
  public List<UserForms> getUserOrderByUserName(int id)
  {
    try
    {
      Criteria crit = getSession().createCriteria(UserIfs.class);
      crit.addOrder(Order.asc("userName"));
      crit.add(Restrictions.eq("userRole", "user"));
      ProjectionList proList = Projections.projectionList();
      proList.add(Projections.property("userId"));
      crit.setProjection(proList);
      crit.setMaxResults(id * 12);
      List<Integer> userIds = crit.list();
      UserIfs user = (UserIfs)getSession().load(UserIfs.class, (Serializable)userIds.get((id - 1) * 12));
      
      return user != null ? user.getUserForms() : null;
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUserForms():" + e);
    }
    return null;
  }
  
  public Long getIdOrderByUserName()
  {
    try
    {
      Criteria crit = getSession().createCriteria(UserIfs.class);
      crit.addOrder(Order.asc("userName"));
      ProjectionList proList = Projections.projectionList();
      proList.add(Projections.property("userId"));
      crit.setProjection(proList);
      crit.setMaxResults(1);
      return (Long)crit.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUserForms():" + e);
    }
    return null;
  }
  
  public UserIfs getUserIdByName(String username, String userRole)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserIfs.class);
      criteria.add(Restrictions.ilike("userName", username));
      criteria.add(Restrictions.ilike("userRole", userRole));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      
      return (UserIfs)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUserIdByName():" + e);
    }
    return null;
  }
  
  public UserIfs getUserByUserId(Long userId)
  {
    UserIfs user = null;
    Criteria criteria = getSession().createCriteria(UserIfs.class);
    criteria.add(Restrictions.idEq(userId));
    try
    {
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      
      user = (UserIfs)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUserIdByName():" + e);
    }
    return user;
  }
  
  public UserIfs getUserBySessUserId(Long sessUserId)
  {
    try
    {
      Criteria crit = getSession().createCriteria(UserIfs.class);
      crit.add(Restrictions.eq("sessUserId", sessUserId));
      crit.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      
      return (UserIfs)crit.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserDaoImpl.java:" + e);
      
      log.info("returning null UserDaoImpl.java, getUserIdByName():" + e);
    }
    return null;
  }
  
  public List<UserIfs> searchUsersByName(String searchString)
  {
    ArrayList<UserIfs> userIfs = new ArrayList(5);
    ArrayList list = new ArrayList();
    try
    {
      String hql = "select  userId from UserIfs where lower(userName) like '%" + searchString.toLowerCase() + "%' ORDER BY userName ASC";
      Query query = getSession().createQuery(hql);
      list = (ArrayList)query.list();
      Iterator itr = list.iterator();
      while (itr.hasNext()) {
        userIfs.add(getUserByUserId((Long)itr.next()));
      }
    }
    catch (Exception e)
    {
      log.info("problem in search..... User could not be retrieved with string :" + searchString);
    }
    return userIfs;
  }
}
