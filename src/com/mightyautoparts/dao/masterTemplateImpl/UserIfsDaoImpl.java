package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.UserIfsDao;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("userIfsDao")
public class UserIfsDaoImpl
  extends BaseDaoImpl
  implements UserIfsDao
{
  private static final Logger log = Logger.getLogger(UserIfsDaoImpl.class);
  
  public UserIfs getUserById(Long userId)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserIfs.class);
      criteria.add(Restrictions.idEq(userId));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      return (UserIfs)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("User could not be retrieved..");
      log.info("Error occured while retrieving User in UserIfsDaoImpl.java:" + e);
      log.info("returning null UserIfsDaoImpl.java, getUserById():" + e);
    }
    return null;
  }
}
