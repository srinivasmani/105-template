package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.SubComponentsDao;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository("subComponentsDao")
public class SubComponentsDaoImpl
  extends BaseDaoImpl
  implements SubComponentsDao
{
  private static final Logger log = Logger.getLogger(SubComponentsDaoImpl.class);
  
  public SubComponents saveSubComponent(SubComponents formSubComponent)
  {
    try
    {
      save(formSubComponent);
      return (SubComponents)getSession().load(SubComponents.class, formSubComponent.getSubComponentId());
    }
    catch (Exception e)
    {
      log.info("subcomponent could not be saved..");
      log.info("Error occured while saving subcomponent in SubComponentsDaoImpl.java:" + e);
    }
    return formSubComponent;
  }
  
  public SubComponents getSubComponentById(Long subComponentId)
  {
    try
    {
      return (SubComponents)getSession().load(SubComponents.class, subComponentId);
    }
    catch (Exception e)
    {
      log.info("subcomponent could not be retrieved..");
      log.info("Error occured while retrieving subcomponent in SubComponentsDaoImpl.java:" + e);
      log.info("returning null SubComponentsDaoImpl.java getSubComponentById():" + e);
    }
    return null;
  }
  
  public SubComponents updateSubComponent(SubComponents currentSubComponent)
  {
    try
    {
      return (SubComponents)getSession().merge(currentSubComponent);
    }
    catch (Exception e)
    {
      log.info("subcomponent could not be updated..");
      log.info("Error occured while retrieving subcomponent in SubComponentsDaoImpl.java:" + e);
      log.info("returning @param currentSubComponent SubComponentsDaoImpl.java updateSubComponent():" + e);
    }
    return currentSubComponent;
  }
}
