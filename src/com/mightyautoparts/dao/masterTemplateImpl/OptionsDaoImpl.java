package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.OptionsDao;
import com.mightyautoparts.entities.masterTemplate.Options;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository("optionsDao")
public class OptionsDaoImpl
  extends BaseDaoImpl
  implements OptionsDao
{
  private static final Logger log = Logger.getLogger(OptionsDaoImpl.class);
  
  public Options saveAndGet(Options formOption)
  {
    log.info("inside OptionsDaoImpl.java saveAndGet()");
    save(formOption);
    Options currentOption = (Options)getSession().load(Options.class, formOption.getOptionId());
    return currentOption;
  }
}
