package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.TemplateComponentDao;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("templateComponentDao")
public class TemplateComponentDaoImpl
  extends BaseDaoImpl
  implements TemplateComponentDao
{
  private static final Logger log = Logger.getLogger(TemplateComponentDaoImpl.class);
  
  public TemplateComponent getCurrentTemplateComponent(Long templateId, Long componentId)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(TemplateComponent.class);
      criteria.add(Restrictions.eq("templateId", templateId));
      criteria.add(Restrictions.eq("componentId", componentId));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      return (TemplateComponent)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("TemplateComponent could not be retrieved..");
      log.info("Error occured while retrieving TemplateComponent in TemplateComponentDaoImpl.java:" + e);
      log.info("returning null TemplateComponentDaoImpl.java, getCurrentTemplateComponent():" + e);
    }
    return null;
  }
  
  public List<TemplateComponent> getTemplateComponents(Long templateId)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(TemplateComponent.class);
      criteria.add(Restrictions.eq("templateId", templateId));
      return criteria.list();
    }
    catch (Exception e)
    {
      log.info("TemplateComponent list could not be retrieved..");
      log.info("Error occured while retrieving TemplateComponentList in TemplateComponentDaoImpl.java:" + e);
      log.info("returning null TemplateComponentDaoImpl.java, getTemplateComponents():" + e);
    }
    return null;
  }
  
  public List<TemplateComponent> getComponentListByTemplateId(Long templateId)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(TemplateComponent.class);
      criteria.add(Restrictions.eq("templateId", templateId));
      return criteria.list();
    }
    catch (Exception e)
    {
      log.info("TemplateComponent could not be retrieved..");
      log.info("Error occured while retrieving TemplateComponent in TemplateComponentDaoImpl.java:" + e);
      log.info("returning null TemplateComponentDaoImpl.java, getComponentListByTemplateId():" + e);
    }
    return null;
  }
}
