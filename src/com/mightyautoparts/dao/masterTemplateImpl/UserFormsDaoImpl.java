package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.Exception.AppDAOException;
import com.mightyautoparts.dao.masterTemplate.UserFormsDao;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository("userFormsDao")
public class UserFormsDaoImpl
  extends BaseDaoImpl
  implements UserFormsDao
{
  private static final Logger log = Logger.getLogger(UserFormsDaoImpl.class);
  
  public UserForms getDefaultUserForm(UserIfs userIfs, Template template)
  {
    UserForms userForm = null;
    Criteria crit = getSession().createCriteria(UserForms.class);
    crit.add(Restrictions.eq("status", "default"));
    crit.add(Restrictions.eq("userIfs", userIfs));
    crit.add(Restrictions.eq("templateId", template.getTemplateId()));
    try
    {
      userForm = (UserForms)crit.list().get(0);
    }
    catch (Exception e)
    {
      e = e;log.info("UserForms could not be retrieved..");log.info("Error occured while retrieving UserForms in UserFormsDaoImpl.java:" + e);log.info("returning null UserFormsDaoImpl.java, getDefaultUserForm():" + e);userForm = null;
    }
    finally {}
    return userForm;
  }
  
  public UserForms saveUserForm(UserForms userForm)
  {
    save(userForm);
    UserForms currentserForm = (UserForms)getSession().load(UserForms.class, userForm.getUserFormId());
    
    return currentserForm;
  }
  
  public UserForms getUserFormsById(Long userFormId)
    throws AppDAOException
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserForms.class);
      criteria.add(Restrictions.idEq(userFormId));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      
      return (UserForms)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      if ((e instanceof RuntimeException))
      {
        log.info("RuntimeException .." + e + "message :" + e.getMessage());
        Throwable ne = e;
        while (ne != null)
        {
          if ((ne instanceof SQLException))
          {
            SQLException sqle = (SQLException)ne;
            ne = sqle.getNextException();
            log.info("SQLException .." + sqle);
            log.info("Nested SQLException .." + ne);
            log.info("SQLException Message .." + sqle.getMessage());
            throw new AppDAOException(sqle.getMessage());
          }
          ne = ne.getCause();
        }
      }
      else
      {
        throw new AppDAOException(e.getMessage());
      }
    }
    
    log.info("UserForms could not be retrieved..");
    log.info("Error occured while retrieving UserForms in UserFormsDaoImpl.java:");
    log.info("returning null UserFormsDaoImpl.java, getUserFormsById():");
    return null;
  }
  
  public UserForms getUserForm(Long formId)
  {
    UserForms currentserForm = (UserForms)getSession().load(UserForms.class, formId);
    
    return currentserForm;
  }
  
  public boolean setFormStatus(Long formId)
  {
    UserForms userForms = null;
    try
    {
      userForms = getUserFormsById(formId);
      List<UserFormsComponents> userFormsComponents = userForms.getUserFormComponents();
      for (UserFormsComponents userfromcomponent : userFormsComponents)
      {
        Components component = userfromcomponent.getComponent();
        if (component.getSubComponents() != null)
        {
          List<SubComponentsOptions> subComponentsOptions = component.getSubComponents().getSubComponentsOptions();
          for (SubComponentsOptions subComponentsOption : subComponentsOptions)
          {
            String hql = "delete from SubComponentsOptions where subComponentOptionsId = :subComponentOptionsId";
            Query query = getSession().createQuery(hql);
            query.setLong("subComponentOptionsId", subComponentsOption.getSubComponentOptionsId().longValue()).executeUpdate();
            
            hql = "delete from Options where optionId = :optionId";
            query = getSession().createQuery(hql);
            query.setLong("optionId", subComponentsOption.getOptions().getOptionId().longValue()).executeUpdate();
            
            hql = "delete from SubComponents where subComponentId = :subComponentId";
            query = getSession().createQuery(hql);
            query.setLong("subComponentId", subComponentsOption.getSubComponents().getComponentId().longValue()).executeUpdate();
          }
        }
      }
      getSession().delete(userForms);
    }
    catch (Exception e)
    {
      log.info("UserForms could not be deleting with Id:" + formId);
      log.info("Error occured while deleting UserForms in UserFormsDaoImpl.java:" + e);
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  public UserForms updateUserForm(UserForms userForm)
  {
    try
    {
      getHibernateTemplate().merge(userForm);
      return userForm;
    }
    catch (Exception e)
    {
      log.info("UserForms could not be updated..");
      log.info("Error occured while updating UserForms in UserFormsDaoImpl.java:" + e);
      log.info("returning null UserFormsDaoImpl.java, setFormStatus():" + e);
    }
    return null;
  }
  
  public List<UserForms> getActiveUserForms(UserIfs userIfs)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserForms.class);
      criteria.add(Restrictions.eq("userIfs", userIfs));
      criteria.add(Restrictions.eq("status", "active"));
      return criteria.list();
    }
    catch (Exception e)
    {
      log.info("UserForms list could not be updated..");
      log.info("Error occured while updating UserForms list in UserFormsDaoImpl.java:" + e);
      log.info("returning null UserFormsDaoImpl.java, getActiveUserForms():" + e);
    }
    return null;
  }
  
  public List<UserForms> getAllUserFormsByTemplateId(Long templateId)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserForms.class);
      criteria.add(Restrictions.eq("templateId", templateId));
      criteria.add(Restrictions.eq("status", "active"));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      return criteria.list();
    }
    catch (Exception e)
    {
      log.info("UserForms list could not be updated..");
      log.info("Error occured while updating UserForms list in UserFormsDaoImpl.java:" + e);
      log.info("returning null UserFormsDaoImpl.java, getAllUserFormsByTemplateId():" + e);
    }
    return null;
  }
  
  public List<UserForms> getAllActiveUserForms()
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserForms.class);
      criteria.add(Restrictions.eq("status", "active"));
      criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
      return criteria.list();
    }
    catch (Exception e)
    {
      log.info("UserForms list could not be updated..");
      log.info("Error occured while updating UserForms list in UserFormsDaoImpl.java:" + e);
      log.info("returning null UserFormsDaoImpl.java, getAllActiveUserForms():" + e);
    }
    return null;
  }
  
  public List<UserForms> getActiveUserFormsByTemplateId(UserIfs userIfs, Template template)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(UserForms.class);
      criteria.add(Restrictions.eq("userIfs", userIfs));
      criteria.add(Restrictions.eq("templateId", template.getTemplateId()));
      criteria.add(Restrictions.eq("status", "active"));
      return criteria.list();
    }
    catch (Exception e)
    {
      log.info("UserForms list could not be get..");
      log.info("Error occured while getting UserForms list by template id in UserFormsDaoImpl.java:" + e);
    }
    return null;
  }
}
