package com.mightyautoparts.dao.masterTemplateImpl;

import com.mightyautoparts.dao.BaseDaoImpl;
import com.mightyautoparts.dao.masterTemplate.TemplateDao;
import com.mightyautoparts.entities.masterTemplate.Template;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("templateDao")
public class TemplateDaoImpl
  extends BaseDaoImpl
  implements TemplateDao
{
  private static final Logger log = Logger.getLogger(TemplateDaoImpl.class);
  
  public Template getTemplateById(Long templateId)
  {
    try
    {
      Criteria criteria = getSession().createCriteria(Template.class);
      criteria.add(Restrictions.idEq(templateId));
      
      return (Template)criteria.uniqueResult();
    }
    catch (Exception e)
    {
      log.info("Template could not be retrieved..");
      log.info("Error occured while retrieving TemplateComponent in TemplateDaoImpl.java:" + e);
      log.info("returning null TemplateDaoImpl.java, getTemplateById():" + e);
    }
    return null;
  }
  
  public void updateTemplate(Template template)
  {
    try
    {
      getSession().merge(template);
    }
    catch (Exception e)
    {
      log.info("Template could not be retrieved..");
      log.info("Error occured while retrieving TemplateComponent in TemplateDaoImpl.java:" + e);
      log.info("returning null TemplateDaoImpl.java, getTemplateById():" + e);
    }
  }
}
