package com.mightyautoparts.dao.Exception;

public class AppDAOException
  extends Exception
{
  private static final long serialVersionUID = 1L;
  private String errorMessage;
  
  public AppDAOException(String errorMessage)
  {
    this.errorMessage = errorMessage;
  }
  
  public String getErrorMessage()
  {
    return this.errorMessage;
  }
}
