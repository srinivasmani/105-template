package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.Position;

public abstract interface PositionDao
  extends BaseDao
{
  public abstract Position createPosition(Long paramLong);
}
