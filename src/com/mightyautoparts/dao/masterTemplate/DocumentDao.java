package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.dto.DocumentDTO;
import com.mightyautoparts.entities.masterTemplate.Document;
import java.util.List;

public abstract interface DocumentDao
  extends BaseDao
{
  public abstract List<DocumentDTO> getDocumentItem();
  
  public abstract List<Document> getAllDocumentItem();
  
  public abstract Boolean removeImageDB(Long paramLong);
  
  public abstract void saveImageDB(Document paramDocument);
  
  public abstract Boolean isRepoImageExist(String paramString);
}
