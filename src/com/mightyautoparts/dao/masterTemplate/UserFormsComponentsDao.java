package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.util.List;

public abstract interface UserFormsComponentsDao
  extends BaseDao
{
  public abstract UserFormsComponents getUserFormComponentById(Long paramLong);
  
  public abstract UserFormsComponents getUserFormCompByParent(Long paramLong1, Long paramLong2);
  
  public abstract List<UserFormsComponents> getUserFormComponentListByUserFormId(Long paramLong);
  
  public abstract void deleteChildComponent(Long paramLong1, Long paramLong2);
}
