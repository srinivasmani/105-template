package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.SubComponents;

public abstract interface SubComponentsDao
  extends BaseDao
{
  public abstract SubComponents saveSubComponent(SubComponents paramSubComponents);
  
  public abstract SubComponents getSubComponentById(Long paramLong);
  
  public abstract SubComponents updateSubComponent(SubComponents paramSubComponents);
}
