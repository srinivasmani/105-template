package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import java.util.List;

public abstract interface UserDao
  extends BaseDao
{
  public abstract List<UserIfs> getAllUsers();
  
  public abstract UserIfs getUser(Long paramLong);
  
  public abstract List<UserForms> getUserForms();
  
  public abstract List<UserForms> getUserForms(Long paramLong);
  
  public abstract UserIfs getUserIdByName(String paramString1, String paramString2);
  
  public abstract UserIfs getUserByUserId(Long paramLong);
  
  public abstract UserIfs getUserBySessUserId(Long paramLong);
  
  public abstract List<UserForms> getUserOrderByUserName(int paramInt);
  
  public abstract Long getIdOrderByUserName();
  
  public abstract List<UserIfs> searchUsersByName(String paramString);
}
