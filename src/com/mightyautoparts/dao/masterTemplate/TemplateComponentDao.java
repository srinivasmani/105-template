package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import java.util.List;

public abstract interface TemplateComponentDao
  extends BaseDao
{
  public abstract TemplateComponent getCurrentTemplateComponent(Long paramLong1, Long paramLong2);
  
  public abstract List<TemplateComponent> getTemplateComponents(Long paramLong);
  
  public abstract List<TemplateComponent> getComponentListByTemplateId(Long paramLong);
}
