package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;

public abstract interface SubComponentsOptionsDao
  extends BaseDao
{
  public abstract SubComponentsOptions getSubComponentOptionsById(Long paramLong);
}
