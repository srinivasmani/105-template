package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.Template;

public abstract interface TemplateDao
  extends BaseDao
{
  public abstract Template getTemplateById(Long paramLong);
  
  public abstract void updateTemplate(Template paramTemplate);
}
