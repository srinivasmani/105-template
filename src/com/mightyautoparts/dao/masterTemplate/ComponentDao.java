package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.Components;

public abstract interface ComponentDao
  extends BaseDao
{
  public abstract Components getComponentById(Long paramLong);
  
  public abstract Components saveComponent(Components paramComponents);
  
  public abstract Components updateComponent(Components paramComponents);
  
  public abstract void deleteComponent(Long paramLong);
}
