package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.dao.Exception.AppDAOException;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import java.util.List;

public abstract interface UserFormsDao
  extends BaseDao
{
  public abstract UserForms getDefaultUserForm(UserIfs paramUserIfs, Template paramTemplate);
  
  public abstract UserForms saveUserForm(UserForms paramUserForms);
  
  public abstract UserForms getUserFormsById(Long paramLong)
    throws AppDAOException;
  
  public abstract UserForms getUserForm(Long paramLong);
  
  public abstract boolean setFormStatus(Long paramLong);
  
  public abstract UserForms updateUserForm(UserForms paramUserForms);
  
  public abstract List<UserForms> getActiveUserForms(UserIfs paramUserIfs);
  
  public abstract List<UserForms> getAllUserFormsByTemplateId(Long paramLong);
  
  public abstract List<UserForms> getAllActiveUserForms();
  
  public abstract List<UserForms> getActiveUserFormsByTemplateId(UserIfs paramUserIfs, Template paramTemplate);
}
