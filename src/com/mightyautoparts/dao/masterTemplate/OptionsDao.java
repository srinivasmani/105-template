package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.Options;

public abstract interface OptionsDao
  extends BaseDao
{
  public abstract Options saveAndGet(Options paramOptions);
}
