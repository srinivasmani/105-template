package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.Status;

public abstract interface StatusDao
  extends BaseDao
{
  public abstract Status getStatusById(Long paramLong);
}
