package com.mightyautoparts.dao.masterTemplate;

import com.mightyautoparts.dao.BaseDao;
import com.mightyautoparts.entities.masterTemplate.UserIfs;

public abstract interface UserIfsDao
  extends BaseDao
{
  public abstract UserIfs getUserById(Long paramLong);
}
