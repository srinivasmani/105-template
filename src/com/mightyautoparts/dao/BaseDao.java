package com.mightyautoparts.dao;

public abstract interface BaseDao
{
  public abstract void save(Object paramObject);
  
  public abstract void update(Object paramObject);
  
  public abstract void updateObject(Object paramObject);
  
  public abstract void find(String paramString, Class paramClass);
}
