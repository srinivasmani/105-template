package com.mightyautoparts.dto;

public class DocumentDTO
{
  private Long docId;
  private String docName;
  
  public Long getDocId()
  {
    return this.docId;
  }
  
  public void setDocId(Long docId)
  {
    this.docId = docId;
  }
  
  public String getDocName()
  {
    return this.docName;
  }
  
  public void setDocName(String docName)
  {
    this.docName = docName;
  }
}
