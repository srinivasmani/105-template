package com.mightyautoparts.dto;

public class ComponentDTO
{
  private Long viewPosition;
  private Long templatePosition;
  private String componentName;
  private String label;
  private Long scoptionId;
  private String optionValue;
  private Long templateComponent;
  private Long viewComponent;
  private String visibility;
  private String optionType;
  
  public ComponentDTO(Long viewPosition, Long templatePosition, String componentName, String label, Long scoptionId, String optionValue, Long templateComponent, Long viewComponent, String visibility, String optionType)
  {
    this.viewPosition = viewPosition;
    this.templatePosition = templatePosition;
    this.componentName = componentName;
    this.label = label;
    this.scoptionId = scoptionId;
    this.optionValue = optionValue;
    this.templateComponent = templateComponent;
    this.viewComponent = viewComponent;
    this.visibility = visibility;
    this.optionType = optionType;
  }
  
  public Long getViewPosition()
  {
    return this.viewPosition;
  }
  
  public void setViewPosition(Long viewPosition)
  {
    this.viewPosition = viewPosition;
  }
  
  public Long getTemplatePosition()
  {
    return this.templatePosition;
  }
  
  public void setTemplatePosition(Long templatePosition)
  {
    this.templatePosition = templatePosition;
  }
  
  public String getComponentName()
  {
    return this.componentName;
  }
  
  public void setComponentName(String componentName)
  {
    this.componentName = componentName;
  }
  
  public String getLabel()
  {
    return this.label;
  }
  
  public void setLabel(String label)
  {
    this.label = label;
  }
  
  public Long getScoptionId()
  {
    return this.scoptionId;
  }
  
  public void setScoptionId(Long scoptionId)
  {
    this.scoptionId = scoptionId;
  }
  
  public String getOptionValue()
  {
    return this.optionValue;
  }
  
  public void setOptionValue(String optionValue)
  {
    this.optionValue = optionValue;
  }
  
  public Long getTemplateComponent()
  {
    return this.templateComponent;
  }
  
  public void setTemplateComponent(Long templateComponent)
  {
    this.templateComponent = templateComponent;
  }
  
  public Long getViewComponent()
  {
    return this.viewComponent;
  }
  
  public void setViewComponent(Long viewComponent)
  {
    this.viewComponent = viewComponent;
  }
  
  public String getVisibility()
  {
    return this.visibility;
  }
  
  public void setVisibility(String visibility)
  {
    this.visibility = visibility;
  }
  
  public String getOptionType()
  {
    return this.optionType;
  }
  
  public void setOptionType(String optionType)
  {
    this.optionType = optionType;
  }
}
