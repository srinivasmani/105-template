package com.mightyautoparts.dto;

import java.util.Map;

public class ResponseDTO
{
  private String status;
  private String errorMessage;
  private Map<String, String> fieldErrors;
  private Object responsePayload;
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
  
  public String getErrorMessage()
  {
    return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage)
  {
    this.errorMessage = errorMessage;
  }
  
  public Map<String, String> getFieldErrors()
  {
    return this.fieldErrors;
  }
  
  public void setFieldErrors(Map<String, String> fieldErrors)
  {
    this.fieldErrors = fieldErrors;
  }
  
  public Object getResponsePayload()
  {
    return this.responsePayload;
  }
  
  public void setResponsePayload(Object responsePayload)
  {
    this.responsePayload = responsePayload;
  }
}
