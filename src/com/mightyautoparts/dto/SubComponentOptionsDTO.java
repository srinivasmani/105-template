package com.mightyautoparts.dto;

import com.mightyautoparts.entities.masterTemplate.Options;

public class SubComponentOptionsDTO
{
  private Options option;
  private String optiontype;
  
  public SubComponentOptionsDTO(Options option, String optiontype)
  {
    this.option = option;
    this.optiontype = optiontype;
  }
  
  public Options getOption()
  {
    return this.option;
  }
  
  public void setOption(Options option)
  {
    this.option = option;
  }
  
  public String getOptiontype()
  {
    return this.optiontype;
  }
  
  public void setOptiontype(String optiontype)
  {
    this.optiontype = optiontype;
  }
}
