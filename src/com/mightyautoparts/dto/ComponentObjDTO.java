package com.mightyautoparts.dto;

public class ComponentObjDTO
{
  private String componentName;
  private Long componentId;
  
  public ComponentObjDTO(String componentName)
  {
    this.componentName = componentName;
  }
  
  public ComponentObjDTO(String componentName, Long componentId)
  {
    this.componentName = componentName;
    this.componentId = componentId;
  }
  
  public void setComponentName(String componentName)
  {
    this.componentName = componentName;
  }
  
  public String getComponentName()
  {
    return this.componentName;
  }
  
  public void setComponentId(Long componentId)
  {
    this.componentId = componentId;
  }
  
  public Long getComponentId()
  {
    return this.componentId;
  }
}
