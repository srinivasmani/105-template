package com.mightyautoparts.dto;

import java.util.ArrayList;

public class ValidationErrorDTO
{
  private boolean valid = true;
  private ArrayList<String> errorFields = new ArrayList();
  private ArrayList<String> errorMessages = new ArrayList();
  
  public ValidationErrorDTO() {}
  
  public ValidationErrorDTO(boolean valid, ArrayList<String> errorFields, ArrayList<String> errorMessages)
  {
    setValid(valid);
    setErrorFields(errorFields);
    setErrorMessages(errorMessages);
  }
  
  public void addInvalidField(String errorField, String errorMessage)
  {
    ArrayList<String> ef = getErrorFields();
    ArrayList<String> em = getErrorMessages();
    
    ef.add(errorField);
    em.add(errorMessage);
    
    setErrorFields(ef);
    setErrorMessages(em);
  }
  
  public boolean isValid()
  {
    return this.valid;
  }
  
  public void setValid(boolean valid)
  {
    this.valid = valid;
  }
  
  public ArrayList<String> getErrorFields()
  {
    return this.errorFields;
  }
  
  public void setErrorFields(ArrayList<String> errorFields)
  {
    this.errorFields = errorFields;
  }
  
  public ArrayList<String> getErrorMessages()
  {
    return this.errorMessages;
  }
  
  public void setErrorMessages(ArrayList<String> errorMessages)
  {
    this.errorMessages = errorMessages;
  }
}
