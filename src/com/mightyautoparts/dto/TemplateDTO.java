package com.mightyautoparts.dto;

import java.util.Date;

public class TemplateDTO
{
  private Long templateId;
  private String headerText;
  private String imageType;
  private String templateImage;
  private String templateName;
  private String updatedBy;
  private Date updatedDate;
  private String logoText;
  
  public Long getTemplateId()
  {
    return this.templateId;
  }
  
  public void setTemplateId(Long templateId)
  {
    this.templateId = templateId;
  }
  
  public String getHeaderText()
  {
    return this.headerText;
  }
  
  public void setHeaderText(String headerText)
  {
    this.headerText = headerText;
  }
  
  public String getImageType()
  {
    return this.imageType;
  }
  
  public void setImageType(String imageType)
  {
    this.imageType = imageType;
  }
  
  public String getTemplateImage()
  {
    return this.templateImage;
  }
  
  public void setTemplateImage(String templateImage)
  {
    this.templateImage = templateImage;
  }
  
  public String getTemplateName()
  {
    return this.templateName;
  }
  
  public void setTemplateName(String templateName)
  {
    this.templateName = templateName;
  }
  
  public String getUpdatedBy()
  {
    return this.updatedBy;
  }
  
  public void setUpdatedBy(String updatedBy)
  {
    this.updatedBy = updatedBy;
  }
  
  public Date getUpdatedDate()
  {
    return this.updatedDate;
  }
  
  public void setUpdatedDate(Date updatedDate)
  {
    this.updatedDate = updatedDate;
  }
  
  public String getLogoText()
  {
    return this.logoText;
  }
  
  public void setLogoText(String logoText)
  {
    this.logoText = logoText;
  }
}
