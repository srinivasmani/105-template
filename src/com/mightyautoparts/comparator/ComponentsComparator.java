package com.mightyautoparts.comparator;

import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import java.util.Comparator;

public class ComponentsComparator
  implements Comparator<Components>
{
  public int compare(Components component1, Components component2)
  {
    Long temp1 = Long.valueOf(Long.parseLong(component1.getTemplateComponent().getPosition().getPositionName()));
    Long temp2 = Long.valueOf(Long.parseLong(component2.getTemplateComponent().getPosition().getPositionName()));
    return temp1.compareTo(temp2);
  }
}
