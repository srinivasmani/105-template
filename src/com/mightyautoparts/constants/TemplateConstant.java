package com.mightyautoparts.constants;

public class TemplateConstant
{
  public static final Long TEMPLATE_104 = Long.valueOf(4L);
  public static final int TOTAL_COMPONENT_TEMPLATE104 = 70;
  public static final Long TEMPLATE_105 = Long.valueOf(5L);
  public static final int TOTAL_COMPONENT_TEMPLATE105 = 75;
  public static final int PAGE_SIZE = 12;
}
