package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dto.ComponentDTO;
import java.util.Comparator;

class TemplateService$1
  implements Comparator<ComponentDTO>
{
  TemplateService$1(TemplateService this$0) {}
  
  public int compare(ComponentDTO c1, ComponentDTO c2)
  {
    return c1.getTemplatePosition().compareTo(c2.getTemplatePosition());
  }
}
