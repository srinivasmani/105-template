package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.TemplateComponentDao;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("templateComponentService")
public class TemplateComponentService
{
  @Autowired
  private TemplateComponentDao templateComponentDao;
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<TemplateComponent> getComponentListByTemplateId(Long templateId)
  {
    return this.templateComponentDao.getComponentListByTemplateId(templateId);
  }
}
