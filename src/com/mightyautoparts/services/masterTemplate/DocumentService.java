package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.DocumentDao;
import com.mightyautoparts.dto.DocumentDTO;
import com.mightyautoparts.entities.masterTemplate.Document;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("documentService")
public class DocumentService
{
  private static final Logger logger = Logger.getLogger(DocumentService.class);
  @Autowired
  private DocumentDao documentDao;
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<DocumentDTO> getDocumentItem()
  {
    List<DocumentDTO> document = this.documentDao.getDocumentItem();
    return document;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Boolean isImageExist(String image)
  {
    return this.documentDao.isRepoImageExist(image);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Boolean removeImage(Long imageId)
  {
    return this.documentDao.removeImageDB(imageId);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void saveImage(String fileName, byte[] image, String type, String role)
  {
    Document document = new Document();
    document.setDocumentDescription("active");
    document.setDocumentFile(image);
    document.setDocumentName(fileName);
    document.setDocumentRole(role);
    document.setDocumentType(type);
    this.documentDao.saveImageDB(document);
  }
}
