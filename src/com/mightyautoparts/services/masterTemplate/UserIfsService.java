package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.UserDao;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("userIfsService")
public class UserIfsService
{
  @Autowired
  private UserDao userDao;
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<UserIfs> getUsers()
  {
    return this.userDao.getAllUsers();
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<UserForms> getUserForm()
  {
    return this.userDao.getUserForms();
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<UserForms> getUserForm(Long userId)
  {
    return this.userDao.getUserForms(userId);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserIfs getUserIdByName(String userName, String userRole)
  {
    return this.userDao.getUserIdByName(userName, userRole);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserIfs getUserById(Long userId)
  {
    return this.userDao.getUserByUserId(userId);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserIfs getUserInSession(String userName, Long franno, Long accessid, Long sessUserId)
  {
    String userRole = "user";
    if ((franno.longValue() == 0L) && (accessid.longValue() == 1L)) {
      userRole = "admin";
    }
    UserIfs user = this.userDao.getUserBySessUserId(sessUserId);
    if (user == null)
    {
      user = new UserIfs(userName, userRole, sessUserId);
      this.userDao.save(user);
    }
    return user;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<UserForms> getUserOrderByUserName(int id)
  {
    return this.userDao.getUserOrderByUserName(id);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Long getIdOrderByUserName()
  {
    return this.userDao.getIdOrderByUserName();
  }
  
  public List<UserIfs> searchUsersByName(String searchString)
  {
    return this.userDao.searchUsersByName(searchString);
  }
}
