package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.ComponentDao;
import com.mightyautoparts.dao.masterTemplate.OptionsDao;
import com.mightyautoparts.dao.masterTemplate.PositionDao;
import com.mightyautoparts.dao.masterTemplate.StatusDao;
import com.mightyautoparts.dao.masterTemplate.SubComponentsDao;
import com.mightyautoparts.dao.masterTemplate.SubComponentsOptionsDao;
import com.mightyautoparts.dao.masterTemplate.TemplateComponentDao;
import com.mightyautoparts.dao.masterTemplate.TemplateDao;
import com.mightyautoparts.dao.masterTemplate.UserFormsComponentsDao;
import com.mightyautoparts.dao.masterTemplate.UserFormsDao;
import com.mightyautoparts.dto.SubComponentOptionsDTO;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("componentsService")
public class ComponentsService
{
  @Autowired
  private ComponentDao componentDao;
  @Autowired
  private StatusDao statusDao;
  @Autowired
  private PositionDao positionDao;
  @Autowired
  private TemplateComponentDao templateComponentDao;
  @Autowired
  private SubComponentsDao subComponentDao;
  @Autowired
  private OptionsDao optionDao;
  @Autowired
  private SubComponentsOptionsDao subOptionDao;
  @Autowired
  private UserFormsComponentsDao userFormsComponentsDao;
  @Autowired
  private UserFormsDao userFormsDao;
  @Autowired
  private TemplateDao templateDao;
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Components getComponentByCid(Long componentId)
  {
    Components component = this.componentDao.getComponentById(componentId);
    return component;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<SubComponentsOptions> getSubOptionsByComponentId(Long componentId)
  {
    Components component = this.componentDao.getComponentById(componentId);
    List<SubComponentsOptions> subOpt = component.getSubComponents().getSubComponentsOptions();
    return subOpt;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void addComponentsToUserForms(Components component)
  {
    List<UserForms> userFormList = this.userFormsDao.getAllUserFormsByTemplateId(Long.valueOf(1L));
    for (UserForms userForm : userFormList) {
      createUserFormComponent(component, userForm);
    }
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Components createCompnent(String heading, String subHeading, List<SubComponentOptionsDTO> subCompDTOList)
  {
    Components component = new Components(heading, new Date(2012, 1, 1), "admin", new Date(2012, 1, 1), "admin");
    this.componentDao.save(component);
    List<TemplateComponent> templateComponentList = this.templateComponentDao.getTemplateComponents(Long.valueOf(1L));
    SubComponents subComponent = new SubComponents(component.getComponentId(), subHeading, component.getUpdatedDate(), component.getUpdatedBy(), component.getCreatedDate(), component.getCreatedBy());
    this.subComponentDao.save(subComponent);
    List<SubComponentsOptions> subComponentsOptions = new ArrayList();
    String status = "true";
    for (SubComponentOptionsDTO subCompDTO : subCompDTOList)
    {
      SubComponentsOptions userSubCompOpt = new SubComponentsOptions(status, subCompDTO.getOptiontype(), subCompDTO.getOption(), subComponent);
      this.subOptionDao.save(userSubCompOpt);
      subComponentsOptions.add(userSubCompOpt);
      status = "false";
    }
    subComponent.setSubComponentsOptions(subComponentsOptions);
    this.subComponentDao.update(subComponent);
    
    Long size = Long.valueOf(templateComponentList.size());
    Position tempCompPostion = this.positionDao.createPosition(Long.valueOf(size.longValue() + 1L));
    Status tempCompStatus = this.statusDao.getStatusById(Long.valueOf(1L));
    TemplateComponent templateComponent = new TemplateComponent(Long.valueOf(1L), component.getComponentId(), tempCompPostion, tempCompStatus);
    this.templateComponentDao.save(templateComponent);
    
    this.componentDao.update(component);
    return component;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void createUserFormComponent(Components component, UserForms userForm)
  {
    if (component.getSubComponents() != null)
    {
      Components userComp = new Components(component.getComponentName(), component.getCreatedDate(), component.getCreatedBy(), component.getUpdatedDate(), component.getUpdatedBy());
      this.componentDao.save(userComp);
      SubComponents userSubComp = new SubComponents(userComp.getComponentId(), component.getSubComponents().getLabel(), component.getSubComponents().getModifiedDate(), component.getSubComponents().getModifiedBy(), component.getSubComponents().getCreateddate(), component.getSubComponents().getCreatedBy());
      this.subComponentDao.save(userSubComp);
      List<SubComponentsOptions> subOptionsList = component.getSubComponents().getSubComponentsOptions();
      List<SubComponentsOptions> userSubOptionsList = new ArrayList();
      for (SubComponentsOptions subOpt : subOptionsList)
      {
        Options userOpt = new Options(subOpt.getOptions().getOptionName(), subOpt.getOptions().getOptionDescription());
        this.optionDao.save(userOpt);
        SubComponentsOptions userSubCompOpt = new SubComponentsOptions(subOpt.getStatus(), subOpt.getOptionType(), userOpt, userSubComp);
        this.subOptionDao.save(userSubCompOpt);
        userSubOptionsList.add(userSubCompOpt);
      }
      userSubComp.setSubComponentsOptions(userSubOptionsList);
      this.subComponentDao.update(userSubComp);
      userComp.setSubComponents(userSubComp);
      this.componentDao.update(userComp);
      UserFormsComponents userFormComp = new UserFormsComponents(userComp, component.getTemplateComponent().getPosition(), component.getTemplateComponent().getStatus(), userForm.getUserIfs().getUserId(), component.getComponentId(), userForm);
      this.userFormsComponentsDao.save(userFormComp);
    }
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<Components> deleteTemplateComponent(Long componentId, Long templateId, UserIfs userIfs)
  {
    this.componentDao.deleteComponent(componentId);
    UserForms userForm = this.userFormsDao.getDefaultUserForm(userIfs, this.templateDao.getTemplateById(templateId));
    if (userForm != null) {
      this.userFormsComponentsDao.deleteChildComponent(componentId, userForm.getUserFormId());
    }
    List<Components> componentList = this.templateDao.getTemplateById(templateId).getComponents();
    return componentList;
  }
}
