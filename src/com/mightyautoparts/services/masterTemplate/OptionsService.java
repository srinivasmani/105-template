package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.OptionsDao;
import com.mightyautoparts.entities.masterTemplate.Options;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("optionsService")
public class OptionsService
{
  @Autowired
  private OptionsDao optionsDao;
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Options createOption(String optionName, String optionDescription)
  {
    Options option = new Options(optionName, optionDescription);
    this.optionsDao.save(option);
    return option;
  }
}
