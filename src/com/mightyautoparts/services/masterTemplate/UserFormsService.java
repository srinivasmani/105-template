package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.Exception.AppDAOException;
import com.mightyautoparts.dao.masterTemplate.ComponentDao;
import com.mightyautoparts.dao.masterTemplate.OptionsDao;
import com.mightyautoparts.dao.masterTemplate.StatusDao;
import com.mightyautoparts.dao.masterTemplate.SubComponentsDao;
import com.mightyautoparts.dao.masterTemplate.SubComponentsOptionsDao;
import com.mightyautoparts.dao.masterTemplate.TemplateComponentDao;
import com.mightyautoparts.dao.masterTemplate.UserFormsComponentsDao;
import com.mightyautoparts.dao.masterTemplate.UserFormsDao;
import com.mightyautoparts.dao.masterTemplate.UserIfsDao;
import com.mightyautoparts.dto.ComponentDTO;
import com.mightyautoparts.dto.ComponentObjDTO;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import com.mightyautoparts.entities.masterTemplate.UserForms;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import com.mightyautoparts.entities.masterTemplate.UserIfs;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("userFormsService")
public class UserFormsService
{
  @Autowired
  private ComponentDao componentDao;
  @Autowired
  private UserFormsComponentsDao userFormsComponentsDao;
  @Autowired
  private UserFormsDao userFormsDao;
  @Autowired
  private SubComponentsDao subComponentsDao;
  @Autowired
  private OptionsDao optionsDao;
  @Autowired
  private SubComponentsOptionsDao subComponentsOptionsDao;
  @Autowired
  private TemplateComponentDao templateComponentDao;
  @Autowired
  private SubComponentsOptionsDao subOptionsDao;
  @Autowired
  private UserIfsDao userIfsDao;
  @Autowired
  private StatusDao statusDao;
  @Resource(name="templateService")
  private TemplateService templateService;
  private static final String FORMPREFIX = "10";
  private static final String FORMSFFIX = " Form ";
  private static final Logger log = Logger.getLogger(UserFormsService.class);
  
  @Transactional(propagation=Propagation.REQUIRED)
  public boolean setStatusInactive(Long formId)
  {
    return this.userFormsDao.setFormStatus(formId);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserComponents(Long formId)
  {
    return this.userFormsDao.getUserForm(formId);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserDeFaultForm(Long userId, Template template)
  {
    UserIfs userIfs = this.userIfsDao.getUserById(userId);
    if (userIfs != null)
    {
      UserForms userForm = this.userFormsDao.getDefaultUserForm(userIfs, template);
      List<Components> componentList = template.getComponents();
      String userFormName;
      List<UserFormsComponents> userFormCompList;
      if (userForm == null)
      {
        int userFormNo = 0;
        userFormNo = this.userFormsDao.getActiveUserFormsByTemplateId(userIfs, template).size() + 1;
        userFormName = null;
        userFormName = "10" + template.getTemplateId() + " Form " + userFormNo;
        userForm = new UserForms(userFormName, "default", template.getTemplateId(), template.getHeaderText(), template.getHeadingColor(), template.getTemplateImage(), userIfs, template.getImageType(), template.getTemplateLeftImage(), template.getTemplateRightImage(), new java.util.Date());
        userFormCompList = new ArrayList();
        this.userFormsDao.save(userForm);
        for (Components component : componentList) {
          if ((component.getSubComponents() != null) && (component.getTemplateComponent() != null))
          {
            Components userFormComp = createUserCompnent(component);
            userFormCompList.add(createUserFormComponent(component, userFormComp, userForm));
          }
        }
        userForm.setUserFormComponents(userFormCompList);
        this.userFormsDao.update(userForm);
      }
      else
      {
        userForm.setCreatedTime(new java.util.Date());
        userFormCompList = new ArrayList();
        for (Components component : componentList)
        {
          UserFormsComponents userFormComp = this.userFormsComponentsDao.getUserFormCompByParent(component.getComponentId(), userForm.getUserFormId());
          log.info("Entering into updating all properties of the default form if it exists ...... ");
          if ((userFormComp != null) && (userFormComp.getComponent().getSubComponents() != null))
          {
            userFormCompList.add(updateUserDefaultFormComponents(component, userFormComp));
          }
          else if ((userFormComp == null) && 
            (component.getSubComponents() != null) && (component.getTemplateComponent() != null))
          {
            Components addUserFormComp = createUserCompnent(component);
            userFormCompList.add(createUserFormComponent(component, addUserFormComp, userForm));
          }
          updateUserDefaultform(userForm, template, userFormCompList);
        }
      }
      return userForm;
    }
    return null;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserFormsById(Long userFormId)
    throws AppDAOException
  {
    UserForms userForm = this.userFormsDao.getUserFormsById(userFormId);
    return userForm;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms updateUserFormService(List<ComponentDTO> componentDTOList, Long userFormId, String headingColor, String formName, String headingText, String templateImage)
    throws AppDAOException
  {
    SubComponents currentSubComponent = null;
    SubComponentsOptions subComponentsOption = null;
    Position currentPosition = null;
    Position viewPosition = null;
    String optionDesc = "";
    UserFormsComponents viewUserFormsComponents = null;
    UserFormsComponents currUserFormsComponents = null;
    Collections.sort(componentDTOList, new Comparator()
    {
      public int compare(ComponentDTO c1, ComponentDTO c2)
      {
        return c1.getTemplatePosition().compareTo(c2.getTemplatePosition());
      }

	@Override
	public int compare(Object arg0, Object arg1) {
		// TODO Auto-generated method stub
		return 0;
	}
    });
    for (int count = 0; count < componentDTOList.size(); count++)
    {
      ComponentDTO componentDTO = (ComponentDTO)componentDTOList.get(count);
      viewUserFormsComponents = this.userFormsComponentsDao.getUserFormComponentById(componentDTO.getViewComponent());
      currUserFormsComponents = this.userFormsComponentsDao.getUserFormComponentById(componentDTO.getTemplateComponent());
      viewUserFormsComponents.getComponent().setComponentName(componentDTO.getComponentName());
      viewUserFormsComponents.getComponent().getSubComponents().setLabel(componentDTO.getLabel());
      if (componentDTO.getScoptionId() != null)
      {
        subComponentsOption = this.subOptionsDao.getSubComponentOptionsById(componentDTO.getScoptionId());
        currentSubComponent = viewUserFormsComponents.getComponent().getSubComponents();
        optionDesc = componentDTO.getOptionValue();
        for (SubComponentsOptions subOption : currentSubComponent.getSubComponentsOptions()) {
          if (subOption.getSubComponentOptionsId().equals(componentDTO.getScoptionId()))
          {
            subOption.setStatus("true");
            subOption.setOptionType(componentDTO.getOptionType());
            subOption.getOptions().setOptionDescription(optionDesc);
            this.subOptionsDao.update(subOption);
          }
          else
          {
            subOption.setStatus("false");
          }
        }
      }
      currentPosition = viewUserFormsComponents.getPosition();
      viewPosition = currUserFormsComponents.getPosition();
      Status viewStatus = this.statusDao.getStatusById(Long.valueOf(1L));
      Status currentStatus = this.statusDao.getStatusById(Long.valueOf(1L));
      ComponentDTO componentDto = null;
      try
      {
        componentDto = (ComponentDTO)componentDTOList.get(Integer.parseInt(viewPosition.getPositionName()) - 1);
        if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("hidden"))) {
          viewStatus = this.statusDao.getStatusById(Long.valueOf(2L));
        } else if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("deleted"))) {
          viewStatus = this.statusDao.getStatusById(Long.valueOf(3L));
        }
      }
      catch (Exception e)
      {
        log.info("error in TemplateService while getting componentDTO : updateTemplate." + e);
      }
      try
      {
        componentDto = (ComponentDTO)componentDTOList.get(Integer.parseInt(currentPosition.getPositionName()) - 1);
        if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("hidden"))) {
          currentStatus = this.statusDao.getStatusById(Long.valueOf(2L));
        } else if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("deleted"))) {
          currentStatus = this.statusDao.getStatusById(Long.valueOf(3L));
        }
      }
      catch (Exception e)
      {
        log.info("error in TemplateService while getting componentDTO : updateTemplate." + e);
      }
      if (viewPosition.getPositionId() != currentPosition.getPositionId())
      {
        viewUserFormsComponents.setPosition(viewPosition);
        currUserFormsComponents.setPosition(currentPosition);
        viewUserFormsComponents.setStatus(viewStatus);
        currUserFormsComponents.setStatus(currentStatus);
      }
      else
      {
        viewUserFormsComponents.setStatus(currentStatus);
        currUserFormsComponents.setStatus(viewStatus);
        log.info("viewComponent " + viewPosition + "viewStatus " + viewStatus);
        log.info("currComponent " + currentPosition + " " + currentStatus);
      }
      this.userFormsComponentsDao.update(viewUserFormsComponents);
      this.userFormsComponentsDao.update(currUserFormsComponents);
    }
    UserForms userForms = this.userFormsDao.getUserFormsById(userFormId);
    if (userForms.getStatus().equalsIgnoreCase("default"))
    {
      userForms.setStatus("active");
      userForms.setFormName(formName);
      userForms.setHeadingColor(headingColor);
      userForms.setHeaderText(headingText);
      userForms.setFormImage(templateImage);
      
      this.userFormsDao.update(userForms);
    }
    else
    {
      userForms.setFormName(formName);
      userForms.setHeadingColor(headingColor);
      userForms.setHeaderText(headingText);
      userForms.setFormImage(templateImage);
      
      this.userFormsDao.update(userForms);
    }
    return userForms;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateUserForm103Service(Map<Long, String> userComponentIDName, UserForms userForms, String formName, String headerText, String formImage, String leftLogoImge, String rightLogoImage, String imgType)
  {
    Components viewComponent = null;
    
    userForms.setHeaderText(headerText);
    userForms.setFormName(formName);
    userForms.setFormImage(formImage);
    userForms.setFormLeftImage(leftLogoImge);
    userForms.setFormRightImage(rightLogoImage);
    userForms.setFormImgType(imgType);
    userForms.setStatus("active");
    
    this.userFormsDao.updateUserForm(userForms);
    for (Map.Entry<Long, String> entry : userComponentIDName.entrySet())
    {
      viewComponent = this.componentDao.getComponentById((Long)entry.getKey());
      viewComponent.setComponentName((String)entry.getValue());
      Long status;
      if ((((String)entry.getValue()).equals("")) || (((String)entry.getValue()).equals(null))) {
        status = Long.valueOf(2L);
      } else {
        status = Long.valueOf(1L);
      }
      viewComponent.getUserFormsComponents().setStatus(this.statusDao.getStatusById(status));
      this.componentDao.update(viewComponent);
    }
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserFormsComponents getComponentByUfId(Long ComponentId)
  {
    UserFormsComponents userFormComp = this.userFormsComponentsDao.getUserFormComponentById(ComponentId);
    return userFormComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<UserForms> deleteUserForm(Long formId, UserIfs user)
    throws AppDAOException
  {
    try
    {
      boolean isdeleted = this.userFormsDao.setFormStatus(formId);
      return this.userFormsDao.getActiveUserForms(user);
    }
    catch (Exception e)
    {
      log.error("pronlem in deleting and getting the form with id:" + formId);
    }
    return null;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserFormsComponents createUserFormComponent(Components parentComponent, Components component, UserForms userForm)
  {
    UserFormsComponents userFormComp = new UserFormsComponents(component, parentComponent.getTemplateComponent().getPosition(), parentComponent.getTemplateComponent().getStatus(), userForm.getUserIfs().getUserId(), parentComponent.getComponentId(), userForm);
    this.userFormsComponentsDao.save(userFormComp);
    return userFormComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Components createUserCompnent(Components component)
  {
    Components userComp = new Components(component.getComponentName(), component.getCreatedDate(), component.getCreatedBy(), component.getUpdatedDate(), component.getUpdatedBy());
    this.componentDao.save(userComp);
    SubComponents userSubComp = new SubComponents(userComp.getComponentId(), component.getSubComponents().getLabel(), component.getUpdatedDate(), component.getUpdatedBy(), component.getCreatedDate(), component.getCreatedBy());
    this.subComponentsDao.save(userSubComp);
    List<SubComponentsOptions> userSubOptionsList = new ArrayList();
    List<SubComponentsOptions> subOptionList = component.getSubComponents().getSubComponentsOptions();
    if (subOptionList != null) {
      for (SubComponentsOptions subOpt : subOptionList) {
        if ((subOpt != null) && (subOpt.getOptions() != null))
        {
          Options option = new Options(subOpt.getOptions().getOptionName(), subOpt.getOptions().getOptionDescription());
          this.optionsDao.save(option);
          SubComponentsOptions userSubCompOpt = new SubComponentsOptions(subOpt.getStatus(), subOpt.getOptionType(), option, userSubComp);
          this.subComponentsOptionsDao.save(userSubCompOpt);
          userSubOptionsList.add(userSubCompOpt);
        }
      }
    }
    userSubComp.setSubComponentsOptions(userSubOptionsList);
    this.subComponentsDao.update(userSubComp);
    
    this.componentDao.update(userComp);
    return userComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms updateUserDefaultform(UserForms userForm, Template template, List<UserFormsComponents> userFormCompList)
  {
    userForm.setFormImage(template.getTemplateImage());
    userForm.setHeaderText(template.getHeaderText());
    userForm.setHeadingColor(template.getHeadingColor());
    userForm.setUserFormComponents(userFormCompList);
    this.userFormsDao.update(userForm);
    return userForm;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserFormsComponents updateUserDefaultFormComponents(Components component, UserFormsComponents userFormComp)
  {
    userFormComp.getComponent().setComponentName(component.getComponentName());
    userFormComp.getComponent().getSubComponents().setLabel(component.getSubComponents().getLabel());
    List<SubComponentsOptions> userFomSubCompOpt = userFormComp.getComponent().getSubComponents().getSubComponentsOptions();
    int index = 0;
    for (SubComponentsOptions subopt : component.getSubComponents().getSubComponentsOptions())
    {
      if (index < userFomSubCompOpt.size())
      {
        SubComponentsOptions currSubOpt = this.subComponentsOptionsDao.getSubComponentOptionsById(((SubComponentsOptions)userFomSubCompOpt.get(index)).getSubComponentOptionsId());
        currSubOpt.getOptions().setOptionDescription(subopt.getOptions().getOptionDescription());
        currSubOpt.getOptions().setOptionName(subopt.getOptions().getOptionName());
        currSubOpt.setOptionType(subopt.getOptionType());
        this.optionsDao.update(currSubOpt.getOptions());
        currSubOpt.setStatus(subopt.getStatus());
        this.subComponentsOptionsDao.update(currSubOpt);
      }
      else
      {
        Options formOption = new Options(subopt.getOptions().getOptionName(), subopt.getOptions().getOptionDescription());
        this.optionsDao.save(formOption);
        SubComponentsOptions formSubComponentsOption = new SubComponentsOptions(subopt.getStatus(), subopt.getOptionType(), formOption, userFormComp.getComponent().getSubComponents());
        this.subComponentsOptionsDao.save(formSubComponentsOption);
        userFomSubCompOpt.add(formSubComponentsOption);
      }
      index++;
    }
    userFormComp.getComponent().getSubComponents().setSubComponentsOptions(userFomSubCompOpt);
    this.componentDao.update(userFormComp.getComponent());
    this.subComponentsDao.update(userFormComp.getComponent().getSubComponents());
    userFormComp.setStatus(component.getTemplateComponent().getStatus());
    userFormComp.setPosition(component.getTemplateComponent().getPosition());
    this.userFormsComponentsDao.update(userFormComp);
    return userFormComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Components createUserFormsOption(Long componentId, SubComponentsOptions subCompOpt)
  {
    Components component = this.componentDao.getComponentById(componentId);
    List<UserForms> userFormList = this.userFormsDao.getAllActiveUserForms();
    for (UserForms userForm : userFormList) {
      createUserFormOption(component, this.userFormsComponentsDao.getUserFormCompByParent(componentId, userForm.getUserFormId()), userForm, subCompOpt);
    }
    return component;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void createUserFormOption(Components component, UserFormsComponents userFormComp, UserForms userForm, SubComponentsOptions subCompOpt)
  {
    List<SubComponentsOptions> subOptList = null;
    if (userFormComp != null)
    {
      if (userFormComp.getComponent().getSubComponents() != null) {
        subOptList = userFormComp.getComponent().getSubComponents().getSubComponentsOptions();
      } else {
        subOptList = new ArrayList();
      }
      Options option = new Options(subCompOpt.getOptions().getOptionName(), subCompOpt.getOptions().getOptionDescription());
      this.optionsDao.save(option);
      SubComponentsOptions createSubCompOpt = new SubComponentsOptions(subCompOpt.getStatus(), subCompOpt.getOptionType(), option, userFormComp.getComponent().getSubComponents());
      this.subComponentsOptionsDao.save(createSubCompOpt);
      subOptList.add(createSubCompOpt);
      userFormComp.getComponent().getSubComponents().setSubComponentsOptions(subOptList);
      this.componentDao.update(userFormComp.getComponent());
    }
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserDeFault103Form(Long userId, Template template)
  {
    UserIfs userIfs = this.userIfsDao.getUserById(userId);
    UserForms userForm = this.userFormsDao.getDefaultUserForm(userIfs, template);
    List<Components> componentList = template.getComponents();
    String userFormName;
    if (userForm == null)
    {
      int userFormNo = 0;
      userFormNo = this.userFormsDao.getActiveUserFormsByTemplateId(userIfs, template).size() + 1;
      userFormName = null;
      userFormName = "10" + template.getTemplateId() + " Form " + userFormNo;
      
      userForm = new UserForms(userFormName, "default", template.getTemplateId(), template.getHeaderText(), template.getHeadingColor(), template.getTemplateImage(), userIfs, template.getImageType(), template.getTemplateLeftImage(), template.getTemplateRightImage(), new java.util.Date());
      List<UserFormsComponents> userFormCompList = new ArrayList();
      this.userFormsDao.save(userForm);
      for (Components component : componentList)
      {
        Components userFormComp = createUserCompnentIfs(component);
        userFormCompList.add(createUserFormComponentIfs(component, userFormComp, userForm));
      }
      userForm.setUserFormComponents(userFormCompList);
      this.userFormsDao.update(userForm);
    }
    else
    {
      userForm.setCreatedTime(new java.util.Date());
      List<UserFormsComponents> userFormCompList = new ArrayList();
      for (Components component : componentList)
      {
        UserFormsComponents userFormComp = this.userFormsComponentsDao.getUserFormCompByParent(component.getComponentId(), userForm.getUserFormId());
        if (userFormComp != null) {
          userFormCompList.add(updateUserDefaultIfsFormComponents(component, userFormComp));
        }
      }
      updateUserDefaultIfsform(userForm, template, userFormCompList);
    }
    return userForm;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserDeFaultIfsForm(Long userId, Template template)
  {
    UserIfs userIfs = this.userIfsDao.getUserById(userId);
    UserForms userForm = this.userFormsDao.getDefaultUserForm(userIfs, template);
    List<Components> componentList = template.getComponents();
    String userFormName;
    if (userForm == null)
    {
      int userFormNo = 0;
      userFormNo = this.userFormsDao.getActiveUserFormsByTemplateId(userIfs, template).size() + 1;
      userFormName = null;
      userFormName = "10" + template.getTemplateId() + " Form " + userFormNo;
      
      userForm = new UserForms(userFormName, "default", template.getTemplateId(), template.getHeaderText(), template.getHeadingColor(), template.getTemplateImage(), userIfs, template.getImageType(), template.getTemplateLeftImage(), template.getTemplateRightImage(), new java.util.Date());
      List<UserFormsComponents> userFormCompList = new ArrayList();
      this.userFormsDao.save(userForm);
      for (Components component : componentList)
      {
        Components userFormComp = createUserCompnentIfs(component);
        userFormCompList.add(createUserFormComponentIfs(component, userFormComp, userForm));
      }
      userForm.setUserFormComponents(userFormCompList);
      this.userFormsDao.update(userForm);
    }
    else
    {
      userForm.setCreatedTime(new java.util.Date());
      List<UserFormsComponents> userFormCompList = new ArrayList();
      for (Components component : componentList)
      {
        UserFormsComponents userFormComp = this.userFormsComponentsDao.getUserFormCompByParent(component.getComponentId(), userForm.getUserFormId());
        if (userFormComp != null)
        {
          userFormCompList.add(updateUserDefaultIfsFormComponents(component, userFormComp));
        }
        else
        {
          Components newUserFormComp = createUserCompnentIfs(component);
          userFormCompList.add(createUserFormComponentIfs(component, newUserFormComp, userForm));
        }
      }
      updateUserDefaultIfsform(userForm, template, userFormCompList);
    }
    return userForm;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Components createUserCompnentIfs(Components component)
  {
    Components userComp = new Components(component.getComponentName(), component.getCreatedDate(), component.getCreatedBy(), component.getUpdatedDate(), component.getUpdatedBy());
    this.componentDao.save(userComp);
    this.componentDao.update(userComp);
    return userComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserFormsComponents createUserFormComponentIfs(Components parentComponent, Components component, UserForms userForm)
  {
    UserFormsComponents userFormComp = new UserFormsComponents(component, parentComponent.getTemplateComponent().getPosition(), parentComponent.getTemplateComponent().getStatus(), userForm.getUserIfs().getUserId(), parentComponent.getComponentId(), userForm);
    this.userFormsComponentsDao.save(userFormComp);
    return userFormComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserFormsComponents updateUserDefaultIfsFormComponents(Components component, UserFormsComponents userFormComp)
  {
    userFormComp.getComponent().setComponentName(component.getComponentName());
    this.componentDao.update(userFormComp.getComponent());
    userFormComp.setStatus(component.getTemplateComponent().getStatus());
    this.userFormsComponentsDao.update(userFormComp);
    return userFormComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms updateUserDefaultIfsform(UserForms userForm, Template template, List<UserFormsComponents> userFormCompList)
  {
    userForm.setFormImage(template.getTemplateImage());
    userForm.setFormImgType(template.getImageType());
    userForm.setHeaderText(template.getHeaderText());
    userForm.setHeadingColor(template.getHeadingColor());
    userForm.setUserFormComponents(userFormCompList);
    this.userFormsDao.update(userForm);
    return userForm;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateUserComponentServiceIfs(List<ComponentObjDTO> componentObjDTOList, UserForms userForm, String headingText, String formImage, String formLeftImage, String formRightImage, String imgType, String formName)
  {
    Components component = null;
    
    userForm.setHeaderText(headingText);
    userForm.setFormImage(formImage);
    userForm.setFormImgType(imgType);
    userForm.setFormLeftImage(formLeftImage);
    userForm.setFormRightImage(formRightImage);
    userForm.setFormName(formName);
    userForm.setStatus("active");
    
    java.sql.Date updatedDate = new java.sql.Date(2012, 24, 1);
    for (int count = 0; count < componentObjDTOList.size(); count++) {
      try
      {
        ComponentObjDTO componentTDTO = (ComponentObjDTO)componentObjDTOList.get(count);
        component = this.componentDao.getComponentById(componentTDTO.getComponentId());
        if ((componentTDTO.getComponentName() != null) && (componentTDTO.getComponentName() != "")) {
          component.setComponentName(componentTDTO.getComponentName());
        } else {
          component.setComponentName(" ");
        }
        component.setUpdatedDate(updatedDate);
        this.componentDao.update(component);
      }
      catch (Exception e)
      {
        log.error("Error whiel updating the  component with Id::" + count);
      }
    }
    try
    {
      this.userFormsDao.updateUserForm(userForm);
    }
    catch (Exception e)
    {
      log.error("Error while updating the user form with form id::" + userForm.getUserFormId());
    }
    log.error("succefully updated the user form with form id::" + userForm.getUserFormId());
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateUserComponentService(List<ComponentObjDTO> componentObjDTOList, UserForms userForm, String headingText, String formImage, String formLeftImage, String formRightImage, String imgType, String formName, String logoText)
  {
    Components component = null;
    if ((headingText != null) && (headingText != "")) {
      userForm.setHeaderText(headingText);
    } else {
      userForm.setHeaderText(userForm.getHeaderText());
    }
    userForm.setFormImage(formImage);
    
    userForm.setFormLeftImage(formLeftImage);
    
    userForm.setFormRightImage(formRightImage);
    
    userForm.setFormImgType(imgType);
    userForm.setFormName(formName);
    userForm.setStatus("active");
    
    userForm.setLogoText(logoText);
    
    java.sql.Date updatedDate = new java.sql.Date(2012, 24, 1);
    for (int count = 0; count < componentObjDTOList.size(); count++) {
      try
      {
        ComponentObjDTO componentTDTO = (ComponentObjDTO)componentObjDTOList.get(count);
        component = this.componentDao.getComponentById(componentTDTO.getComponentId());
        if ((componentTDTO.getComponentName() != null) && (componentTDTO.getComponentName() != ""))
        {
          component.setComponentName(componentTDTO.getComponentName());
          component.getUserFormsComponents().setStatus(this.statusDao.getStatusById(Long.valueOf(1L)));
        }
        else
        {
          component.setComponentName(component.getComponentName());
          component.getUserFormsComponents().setStatus(this.statusDao.getStatusById(Long.valueOf(2L)));
        }
        component.setUpdatedDate(updatedDate);
        this.componentDao.update(component);
      }
      catch (Exception e)
      {
        log.error("Error while updating the  component with Id::" + count);
      }
    }
    try
    {
      this.userFormsDao.updateUserForm(userForm);
    }
    catch (Exception e)
    {
      log.error("Error while updating the user form with form id::" + userForm.getUserFormId());
    }
    log.error("succefully updated the user form with form id::" + userForm.getUserFormId());
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<UserForms> getUserActiveForms(UserIfs user)
  {
    List<UserForms> userForms = this.userFormsDao.getActiveUserForms(user);
    if (userForms != null) {
      Collections.sort(userForms, new Comparator()
      {
        public int compare(UserForms c1, UserForms c2)
        {
          return c1.getUserFormId().compareTo(c2.getUserFormId());
        }

		@Override
		public int compare(Object o1, Object o2) {
			// TODO Auto-generated method stub
			return 0;
		}
      });
    }
    return userForms;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserDeFaultIfsFormFor103(Long userId, Template template)
  {
    UserIfs userIfs = this.userIfsDao.getUserById(userId);
    UserForms userForm = this.userFormsDao.getDefaultUserForm(userIfs, template);
    List<Components> componentList = template.getComponents();
    String userFormName;
    Components component;
    if (userForm == null)
    {
      int userFormNo = 0;
      
      userFormNo = this.userFormsDao.getActiveUserFormsByTemplateId(userIfs, template).size() + 1;
      
      userFormName = null;
      userFormName = "10" + template.getTemplateId() + " Form " + userFormNo;
      
      userForm = new UserForms(userFormName, "default", template.getTemplateId(), template.getHeaderText(), template.getHeadingColor(), template.getTemplateImage(), userIfs, template.getImageType(), template.getTemplateLeftImage(), template.getTemplateRightImage(), new java.util.Date());
      List<UserFormsComponents> userFormCompList = new ArrayList();
      for (Iterator localIterator = componentList.iterator(); localIterator.hasNext();)
      {
        component = (Components)localIterator.next();
        Components userFormComp = createUserCompnentIfs103n104(component);
        userFormCompList.add(createUserFormComponentIfs103n104(component, userFormComp, userForm));
      }
      userForm.setUserFormComponents(userFormCompList);
      this.userFormsDao.save(userForm);
    }
    else
    {
      userForm.setCreatedTime(new java.util.Date());
      
      List<UserFormsComponents> userFormCompList = userForm.getUserFormComponents();
//      for (userFormName = userFormCompList.iterator().toString();
//    		  userForm.hasNext();)
//      {
//        for (Components tempCom : componentList) {
//          TemplateComponent userFormsComponents;
//		if (Long.parseLong(tempCom.getTemplateComponent().getPosition().getPositionName()) == Long.parseLong(userFormsComponents.getPosition()
//            .getPositionName())) {
//            ComponentDTO components;
//			components.setComponentName(tempCom.getComponentName());
//          }
//        }
//      }
      UserFormsComponents userFormsComponents;
      Components components;
      userForm.setUserFormComponents(userFormCompList);
      userForm.setFormImage(template.getTemplateImage());
      userForm.setFormImgType(template.getImageType());
      userForm.setHeaderText(template.getHeaderText());
      userForm.setHeadingColor(template.getHeadingColor());
      this.userFormsDao.updateObject(userForm);
    }
    return userForm;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserDeFaultIfsFormFor104(Long userId, Template template)
  {
    UserIfs userIfs = this.userIfsDao.getUserById(userId);
    UserForms userForm = this.userFormsDao.getDefaultUserForm(userIfs, template);
    List<Components> componentList = template.getComponents();
    String userFormName;
    Components component;
    if (userForm == null)
    {
      int userFormNo = 0;
      
      userFormNo = this.userFormsDao.getActiveUserFormsByTemplateId(userIfs, template).size() + 1;
      
      userFormName = null;
      userFormName = "10" + template.getTemplateId() + " Form " + userFormNo;
      
      userForm = new UserForms(userFormName, "default", template.getTemplateId(), template.getHeaderText(), template.getHeadingColor(), template.getTemplateImage(), userIfs, template.getImageType(), template.getTemplateLeftImage(), template.getTemplateRightImage(), new java.util.Date());
      List<UserFormsComponents> userFormCompList = new ArrayList();
      for (Iterator localIterator = componentList.iterator(); localIterator.hasNext();)
      {
        component = (Components)localIterator.next();
        Components userFormComp = createUserCompnentIfs103n104(component);
        userFormCompList.add(createUserFormComponentIfs103n104(component, userFormComp, userForm));
      }
      userForm.setUserFormComponents(userFormCompList);
      this.userFormsDao.save(userForm);
    }
    else
    {
      userForm.setCreatedTime(new java.util.Date());
      
      List<UserFormsComponents> userFormCompList = userForm.getUserFormComponents();
//      for (userFormName = userFormCompList.iterator(); userFormName.hasNext();)
//      {
//        userFormsComponents  components= (UserFormsComponents)userFormName.next();
//        components = userFormsComponents.getComponent();
//        for (Components tempCom : componentList) {
//          if (Long.parseLong(tempCom.getTemplateComponent().getPosition().getPositionName()) == Long.parseLong(userFormsComponents.getPosition()
//            .getPositionName())) {
//            components.setComponentName(tempCom.getComponentName());
//          }
//        }
//      }
      UserFormsComponents userFormsComponents;
      Components components;
      userForm.setUserFormComponents(userFormCompList);
      userForm.setFormImage(template.getTemplateImage());
      userForm.setFormImgType(template.getImageType());
      userForm.setHeaderText(template.getHeaderText());
      userForm.setHeadingColor(template.getHeadingColor());
      this.userFormsDao.updateObject(userForm);
    }
    return userForm;
  }
  
  public Components createUserCompnentIfs103n104(Components component)
  {
    Components userComp = new Components(component.getComponentName(), component.getCreatedDate(), component.getCreatedBy(), component.getUpdatedDate(), component.getUpdatedBy());
    
    return userComp;
  }
  
  public UserFormsComponents createUserFormComponentIfs103n104(Components parentComponent, Components component, UserForms userForm)
  {
    UserFormsComponents userFormComp = new UserFormsComponents(component, parentComponent.getTemplateComponent().getPosition(), parentComponent.getTemplateComponent().getStatus(), userForm.getUserIfs().getUserId(), parentComponent.getComponentId(), userForm);
    return userFormComp;
  }
  
  public Components createUserCompnentIfs105(Components component)
  {
    Components userComp = new Components(component.getComponentName(), component.getCreatedDate(), component.getCreatedBy(), component.getUpdatedDate(), component.getUpdatedBy());
    
    return userComp;
  }
  
  public UserFormsComponents createUserFormComponentIfs105(Components parentComponent, Components component, UserForms userForm)
  {
    UserFormsComponents userFormComp = new UserFormsComponents(component, parentComponent.getTemplateComponent().getPosition(), parentComponent.getTemplateComponent().getStatus(), userForm.getUserIfs().getUserId(), parentComponent.getComponentId(), userForm);
    return userFormComp;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public UserForms getUserDeFaultIfsFormFor105(Long userId, Template template)
  {
    UserIfs userIfs = this.userIfsDao.getUserById(userId);
    UserForms userForm = this.userFormsDao.getDefaultUserForm(userIfs, template);
    List<Components> componentList = template.getComponents();
    String userFormName;
    Components component;
    if (userForm == null)
    {
      int userFormNo = 0;
      
      userFormNo = this.userFormsDao.getActiveUserFormsByTemplateId(userIfs, template).size() + 1;
      
      userFormName = null;
      userFormName = "10" + template.getTemplateId() + " Form " + userFormNo;
      
      userForm = new UserForms(userFormName, "default", template.getTemplateId(), template.getHeaderText(), template.getHeadingColor(), template.getTemplateImage(), userIfs, template.getImageType(), template.getTemplateLeftImage(), template.getTemplateRightImage(), new java.util.Date());
      List<UserFormsComponents> userFormCompList = new ArrayList();
      for (Iterator localIterator = componentList.iterator(); localIterator.hasNext();)
      {
        component = (Components)localIterator.next();
        Components userFormComp = createUserCompnentIfs105(component);
        userFormCompList.add(createUserFormComponentIfs105(component, userFormComp, userForm));
      }
      userForm.setUserFormComponents(userFormCompList);
      this.userFormsDao.save(userForm);
    }
    else
    {
      userForm.setCreatedTime(new java.util.Date());
      List<UserFormsComponents> userFormCompList = userForm.getUserFormComponents();
//      for (userFormName = userFormCompList.iterator(); userFormName.hasNext();)
//      {
//        userFormsComponents = (UserFormsComponents)userFormName.next();
//        components = userFormsComponents.getComponent();
//        for (Components tempCom : componentList) {
//          if (Long.parseLong(tempCom.getTemplateComponent().getPosition().getPositionName()) == Long.parseLong(userFormsComponents.getPosition()
//            .getPositionName())) {
//            components.setComponentName(tempCom.getComponentName());
//          }
//        }
//      }
      UserFormsComponents userFormsComponents;
      Components components;
      userForm.setUserFormComponents(userFormCompList);
      userForm.setFormImage(template.getTemplateImage());
      userForm.setFormImgType(template.getImageType());
      userForm.setHeaderText(template.getHeaderText());
      userForm.setHeadingColor(template.getHeadingColor());
      this.userFormsDao.updateObject(userForm);
    }
    return userForm;
  }
}
