package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.entities.masterTemplate.UserForms;
import java.util.Comparator;

class UserFormsService$2
  implements Comparator<UserForms>
{
  UserFormsService$2(UserFormsService this$0) {}
  
  public int compare(UserForms c1, UserForms c2)
  {
    return c1.getUserFormId().compareTo(c2.getUserFormId());
  }
}
