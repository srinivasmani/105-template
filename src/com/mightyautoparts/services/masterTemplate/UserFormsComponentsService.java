package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.UserFormsComponentsDao;
import com.mightyautoparts.dao.masterTemplate.UserFormsDao;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.UserFormsComponents;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("userFormsComponentsService")
public class UserFormsComponentsService
{
  @Autowired
  private UserFormsComponentsDao userFormsComponentsDao;
  @Autowired
  private UserFormsDao userFormsDao;
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<SubComponentsOptions> getSubOptionsById(Long ufcomponentId)
  {
    UserFormsComponents userFormComp = this.userFormsComponentsDao.getUserFormComponentById(ufcomponentId);
    List<SubComponentsOptions> subOpt = userFormComp.getComponent().getSubComponents().getSubComponentsOptions();
    return subOpt;
  }
  @Transactional(propagation=Propagation.REQUIRED)
  public List<UserFormsComponents> getUserFormComponentListByUserFormId(Long userFormId)
  {
    return this.userFormsComponentsDao.getUserFormComponentListByUserFormId(userFormId);
  }
}
