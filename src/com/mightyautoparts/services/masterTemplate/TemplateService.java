package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.ComponentDao;
import com.mightyautoparts.dao.masterTemplate.StatusDao;
import com.mightyautoparts.dao.masterTemplate.SubComponentsOptionsDao;
import com.mightyautoparts.dao.masterTemplate.TemplateDao;
import com.mightyautoparts.dto.ComponentDTO;
import com.mightyautoparts.dto.ComponentObjDTO;
import com.mightyautoparts.dto.TemplateDTO;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.Position;
import com.mightyautoparts.entities.masterTemplate.Status;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import com.mightyautoparts.entities.masterTemplate.Template;
import com.mightyautoparts.entities.masterTemplate.TemplateComponent;
import java.sql.Date;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("templateService")
public class TemplateService
{
  @Autowired
  private TemplateDao templateDao;
  @Autowired
  private ComponentDao componentDao;
  @Autowired
  private StatusDao statusDao;
  @Autowired
  private SubComponentsOptionsDao subOptionsDao;
  private static final Logger log = Logger.getLogger(TemplateService.class);
  
  @Transactional(propagation=Propagation.REQUIRED)
  public Template getCompnentsByTemplateId(Long templateId)
  {
    return this.templateDao.getTemplateById(templateId);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateTemplateService(List<ComponentDTO> componentDTOList, Template template, String headingColor, String headingText, String templateImage)
  {
    SubComponents currentSubComponent = null;
    SubComponentsOptions subComponentsOption = null;
    Position currentPosition = null;
    Position viewPosition = null;
    String optionDesc = "";
    Components viewComponent = null;
    Components currComponent = null;
    template.setHeaderText(headingText);
    template.setTemplateImage(templateImage);
    template.setHeadingColor(headingColor);
    this.templateDao.updateTemplate(template);
    Collections.sort(componentDTOList, new Comparator<ComponentDTO>()
    {
      public int compare(ComponentDTO c1, ComponentDTO c2)
      {
        return c1.getTemplatePosition().compareTo(c2.getTemplatePosition());
      }
    });
    for (int count = 0; count < componentDTOList.size(); count++)
    {
      ComponentDTO componentDTO = (ComponentDTO)componentDTOList.get(count);
      viewComponent = this.componentDao.getComponentById(componentDTO.getViewComponent());
      currComponent = this.componentDao.getComponentById(componentDTO.getTemplateComponent());
      viewComponent.setComponentName(componentDTO.getComponentName());
      viewComponent.getSubComponents().setLabel(componentDTO.getLabel());
      if (componentDTO.getScoptionId() != null)
      {
        subComponentsOption = this.subOptionsDao.getSubComponentOptionsById(componentDTO.getScoptionId());
        currentSubComponent = viewComponent.getSubComponents();
        optionDesc = componentDTO.getOptionValue();
        for (SubComponentsOptions subOption : currentSubComponent.getSubComponentsOptions()) {
          if (subOption.getSubComponentOptionsId().equals(componentDTO.getScoptionId()))
          {
            subOption.setStatus("true");
            subOption.setOptionType(componentDTO.getOptionType());
            subOption.getOptions().setOptionDescription(optionDesc);
            this.subOptionsDao.update(subOption);
          }
          else
          {
            subOption.setStatus("false");
            this.subOptionsDao.update(subOption);
          }
        }
      }
      currentPosition = viewComponent.getTemplateComponent().getPosition();
      viewPosition = currComponent.getTemplateComponent().getPosition();
      Status viewStatus = this.statusDao.getStatusById(Long.valueOf(1L));
      Status currentStatus = this.statusDao.getStatusById(Long.valueOf(1L));
      ComponentDTO componentDto = null;
      try
      {
        componentDto = (ComponentDTO)componentDTOList.get(Integer.parseInt(viewPosition.getPositionName()) - 1);
        if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("hidden"))) {
          viewStatus = this.statusDao.getStatusById(Long.valueOf(2L));
        } else if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("deleted"))) {
          viewStatus = this.statusDao.getStatusById(Long.valueOf(3L));
        }
      }
      catch (Exception e)
      {
        log.info("error in TemplateService while getting componentDTO : updateTemplate." + e);
      }
      try
      {
        componentDto = (ComponentDTO)componentDTOList.get(Integer.parseInt(currentPosition.getPositionName()) - 1);
        if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("hidden"))) {
          currentStatus = this.statusDao.getStatusById(Long.valueOf(2L));
        } else if ((componentDto != null) && (componentDto.getVisibility().equalsIgnoreCase("deleted"))) {
          currentStatus = this.statusDao.getStatusById(Long.valueOf(3L));
        }
      }
      catch (Exception e)
      {
        log.info("error in TemplateService while getting componentDTO : updateTemplate." + e);
      }
      if (viewPosition.getPositionId() != currentPosition.getPositionId())
      {
        viewComponent.getTemplateComponent().setPosition(viewPosition);
        currComponent.getTemplateComponent().setPosition(currentPosition);
        viewComponent.getTemplateComponent().setStatus(viewStatus);
        currComponent.getTemplateComponent().setStatus(currentStatus);
        log.info("viewComponent " + viewPosition + "viewStatus " + viewStatus);
        log.info("currComponent " + currentPosition + " " + currentStatus);
      }
      else
      {
        viewComponent.getTemplateComponent().setStatus(currentStatus);
        currComponent.getTemplateComponent().setStatus(viewStatus);
      }
      this.componentDao.update(viewComponent);
      this.componentDao.update(currComponent);
    }
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateComponentService(List<ComponentObjDTO> componentObjDTOList, Template template, String headingText, String templateImage, String templateLeftImage, String templateRightImage, String imgType)
  {
    Components component = null;
    template.setHeaderText(headingText);
    template.setTemplateImage(templateImage);
    template.setImageType(imgType);
    Date updatedDate = new Date(2012, 24, 1);
    for (int count = 0; count < componentObjDTOList.size(); count++)
    {
      ComponentObjDTO componentTDTO = (ComponentObjDTO)componentObjDTOList.get(count);
      component = this.componentDao.getComponentById(componentTDTO.getComponentId());
      if (component != null)
      {
        if ((componentTDTO.getComponentName() != null) && (componentTDTO.getComponentName() != ""))
        {
          component.getTemplateComponent().setStatus(this.statusDao.getStatusById(Long.valueOf(1L)));
          component.setComponentName(componentTDTO.getComponentName());
        }
        else
        {
          component.getTemplateComponent().setStatus(this.statusDao.getStatusById(Long.valueOf(2L)));
          component.setComponentName(" ");
        }
        component.setUpdatedDate(updatedDate);
        this.componentDao.update(component);
      }
    }
    this.templateDao.updateTemplate(template);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateTemplateComponentService(List<ComponentObjDTO> componentObjDTOList, Template template, String headingText, String templateImage, String templateLeftImage, String templateRightImage, String imgType)
  {
    Components component = null;
    if ((headingText != null) && (headingText != "")) {
      template.setHeaderText(headingText);
    } else {
      template.setHeaderText(template.getHeaderText());
    }
    template.setTemplateImage(templateImage);
    if ((templateLeftImage != null) && (templateLeftImage != "")) {
      template.setTemplateLeftImage(templateLeftImage);
    }
    if ((templateRightImage != null) && (templateRightImage != "")) {
      template.setTemplateRightImage(templateRightImage);
    }
    template.setImageType(imgType);
    Date updatedDate = new Date(2012, 24, 1);
    for (int count = 0; count < componentObjDTOList.size(); count++) {
      try
      {
        ComponentObjDTO componentTDTO = (ComponentObjDTO)componentObjDTOList.get(count);
        
        component = this.componentDao.getComponentById(componentTDTO.getComponentId());
        if ((componentTDTO.getComponentName().equalsIgnoreCase("null")) && (componentTDTO.getComponentName() != ""))
        {
          component.setComponentName(component.getComponentName());
        }
        else if (componentTDTO.getComponentName() == "")
        {
          component.getTemplateComponent().setStatus(this.statusDao.getStatusById(Long.valueOf(2L)));
        }
        else
        {
          component.setComponentName(componentTDTO.getComponentName());
          component.getTemplateComponent().setStatus(this.statusDao.getStatusById(Long.valueOf(1L)));
        }
        component.setUpdatedDate(updatedDate);
        this.componentDao.update(component);
      }
      catch (Exception e)
      {
        log.error("error while updating the admin component with Id::" + count);
      }
    }
    this.templateDao.updateTemplate(template);
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateTemplate3Service(Map<Long, String> componentIDName, Template template, String templateImage, String headerText, String imgType)
  {
    Components viewComponent = null;
    
    template.setHeaderText(headerText);
    template.setTemplateImage(templateImage);
    template.setImageType(imgType);
    this.templateDao.updateTemplate(template);
    for (Map.Entry<Long, String> entry : componentIDName.entrySet())
    {
      viewComponent = this.componentDao.getComponentById((Long)entry.getKey());
      viewComponent.setComponentName((String)entry.getValue());
      Long status;
      Long status1;
      if ((((String)entry.getValue()).equals("")) || (((String)entry.getValue()).equals(null))) {
        status = Long.valueOf(2L);
      } else {
        status = Long.valueOf(1L);
      }
      viewComponent.getTemplateComponent().setStatus(this.statusDao.getStatusById(status));
      this.componentDao.updateComponent(viewComponent);
    }
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public List<Components> getComponents(Long templateId)
  {
    Template template = this.templateDao.getTemplateById(templateId);
    List<Components> componentsList = template.getComponents();
    return componentsList;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public void updateTemplate105ComponentService(List<ComponentObjDTO> componentObjDTOList, Template template, TemplateDTO templateDTO)
  {
    if ((templateDTO.getHeaderText() != null) && (templateDTO.getHeaderText() != "")) {
      template.setHeaderText(templateDTO.getHeaderText());
    } else {
      template.setHeaderText(template.getHeaderText());
    }
    template.setTemplateImage(templateDTO.getTemplateImage());
    template.setLogoText(templateDTO.getLogoText());
    template.setImageType(templateDTO.getImageType());
    
    Components component = null;
    for (int count = 0; count < componentObjDTOList.size(); count++) {
      try
      {
        ComponentObjDTO componentTDTO = (ComponentObjDTO)componentObjDTOList.get(count);
        component = this.componentDao.getComponentById(componentTDTO.getComponentId());
        if ((componentTDTO.getComponentName().equalsIgnoreCase("null")) && (componentTDTO.getComponentName() != ""))
        {
          component.setComponentName(component.getComponentName());
        }
        else if (componentTDTO.getComponentName() == "")
        {
          component.getTemplateComponent().setStatus(this.statusDao.getStatusById(Long.valueOf(2L)));
        }
        else
        {
          component.setComponentName(componentTDTO.getComponentName());
          component.getTemplateComponent().setStatus(this.statusDao.getStatusById(Long.valueOf(1L)));
        }
        this.componentDao.update(component);
      }
      catch (Exception e)
      {
        log.error("error while updating the admin component with Id:" + count);
      }
    }
    this.templateDao.updateTemplate(template);
  }
}
