package com.mightyautoparts.services.masterTemplate;

import com.mightyautoparts.dao.masterTemplate.ComponentDao;
import com.mightyautoparts.dao.masterTemplate.OptionsDao;
import com.mightyautoparts.dao.masterTemplate.SubComponentsOptionsDao;
import com.mightyautoparts.entities.masterTemplate.Components;
import com.mightyautoparts.entities.masterTemplate.Options;
import com.mightyautoparts.entities.masterTemplate.SubComponents;
import com.mightyautoparts.entities.masterTemplate.SubComponentsOptions;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("subComponentsOptions")
public class SubComponentsOptionsService
{
  @Autowired
  private SubComponentsOptionsDao subOptionsDao;
  @Autowired
  private OptionsDao optionDao;
  @Autowired
  private ComponentDao compDao;
  
  @Transactional(propagation=Propagation.REQUIRED)
  public SubComponentsOptions getSubCompnentOption(Long subOptionsId)
  {
    SubComponentsOptions suboption = this.subOptionsDao.getSubComponentOptionsById(subOptionsId);
    return suboption;
  }
  
  @Transactional(propagation=Propagation.REQUIRED)
  public SubComponentsOptions CreateSubComponentOptions(long componentId, String optionName, String optionDesc, String optionType)
  {
    Components component = this.compDao.getComponentById(Long.valueOf(componentId));
    Options option = new Options(optionName, optionDesc);
    this.optionDao.save(option);
    List<SubComponentsOptions> subCompOptlist = component.getSubComponents().getSubComponentsOptions();
    SubComponentsOptions subCompOpt = new SubComponentsOptions("false", optionType, option, component.getSubComponents());
    this.subOptionsDao.save(subCompOpt);
    subCompOptlist.add(subCompOpt);
    component.getSubComponents().setSubComponentsOptions(subCompOptlist);
    this.compDao.update(component);
    return subCompOpt;
  }
}
