package mty;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

@WebServlet({"/FileUploadServlet"})
public class FileUploadServlet
  extends HttpServlet
{
  private static final long serialVersionUID = 1L;
  
  protected void doGet(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
    throws ServletException, IOException
  {
    System.out.println("Get! Params: " + paramHttpServletRequest.getParameterMap());
    paramHttpServletResponse.setContentType("text/plain");
    PrintWriter localPrintWriter = paramHttpServletResponse.getWriter();
    localPrintWriter.append("Hello from server!");
  }
  
  protected void doPost(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
    throws IOException
  {
    System.out.println("Post!");
    
    DiskFileItemFactory localDiskFileItemFactory = new DiskFileItemFactory();
    
    ServletFileUpload localServletFileUpload = new ServletFileUpload(localDiskFileItemFactory);
    try
    {
      List localList = localServletFileUpload.parseRequest(new ServletRequestContext(paramHttpServletRequest));
//      for (FileItem localFileItem :localList) {
//        if (localFileItem.isFormField())
//        {
//          System.out.print("Got normal form field " + localFileItem.getFieldName() + "=" + localFileItem.getString());
//        }
//        else
//        {
//          System.out.print("Got file: " + localFileItem.getName());
//          
//          String str = "/";//opt/liferay/instances/mapps-02/tomcat-home/webapps/Inspection_Form/test";
//          File localFile = new File(str, localFileItem.getName());
//          localFile.setWritable(true);
//          if (localFile.createNewFile()) {
//            try
//            {
//              localFileItem.write(localFile);
//            }
//            catch (Exception localException)
//            {
//              localException.printStackTrace();
//            }
//          }
//        }
//      }
    }
    catch (FileUploadException localFileUploadException)
    {
      try
      {
        throw new ServletException("File processing failed", localFileUploadException);
      }
      catch (ServletException localServletException)
      {
        localServletException.printStackTrace();
      }
    }
  }
  
  private void doRequest(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse)
    throws IOException
  {}
}
